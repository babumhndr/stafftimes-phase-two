<script type="text/javascript">
$(document).ready(function(){
        $('#f').click(function() {
    $("#e").toggle();
});
    }); 
</script>
<style type="text/css">

.dropbtn {
    /*background-color: #4CAF50;*/
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #000;
    min-width: 250px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    left: -70px;
    top: 66px;
    border-radius: 5px;
}

.dropdown-content a {
    color: white;
    padding: 12px 16px;
    text-decoration: none;
    margin-right: 0px;
    text-align: left;
    display: block;
}

/*.dropdown-content a:hover {background-color: #ddd}*/

.dropdown:hover .dropdown-content {
    display: block;
}

ul.dropdown-content:after {
    position: absolute;
    left: 39%;
    top: -19px;
    width: 0;
    height: 0;
    content: '';
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
    border-bottom: 20px solid #000000;
}

/*.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}*/
  
</style>

        <div class="col-md-12 hidden-xs heading">
            <div class="col-md-3 logo">
            <a href="<?php echo route('/')?>" class="logo1">
                <img src="<?php echo url('image/webapp.png')?>" width="120" alt="" />
                </a>
               <!-- <p style="color: red !important;font-size: 9px;margin: 0px;">Version 0.16</p> -->
            </div>
            <div class="col-md-9 header">

                <a href="<?php echo route('/')?>"><?php echo trans('main_page_header_nav.home');?></a>
                <a href="<?php echo route('price')?>"><?php echo trans('main_page_header_nav.price');?></a>
                <a href="<?php echo route('faq')?>"><?php echo trans('main_page_header_nav.faq');?></a>
                <a href="<?php echo route('blog')?>"><?php echo trans('main_page_header_nav.blog');?></a>
               
                <div class="dropdown">
                  <a class="dropbtn"><?php echo trans('main_page_header_nav.login')?></a>
                  <ul class="dropdown-content">
                    <li><a href="#" style="color:blue;text-transform: lowercase;cursor: none;">I am a ..</a></li>
                    <li><a href="<?php echo route('login')?>">Business Owner</a></li>
                    <li><a href="login_ca">Charted Accountant</a></li>
                  </ul>
                </div>
                <a href="<?php echo url('/#register')?>"><?php echo trans('main_page_header_nav.register');?></a>

                <div class="dropdown" style="float:right; font-style:italic;">
                <?php
                $codeOfLanguage=App::getLocale();
                        if ($codeOfLanguage=="en") {
                            $codeOfLanguage="de";
                        }
                        else
                        {
                             $codeOfLanguage="en";
                        }  
                ?>
                     <a class="lang_tag lang_a_tag" href="<?php echo route('lang.switch', $codeOfLanguage) ?>">
                    <?= Config::get('languages')[$codeOfLanguage] ?></a>
                <!--     <div id="e" style="display:none;position:absolute; left: 89%; background-color: #3a98d8;">
                     <ul style=" margin-bottom: 0px;list-style: none; ">
                  <?php foreach (Config::get('languages') as $lang => $language):?>
                                                   <?php if ($lang != $codeOfLanguage):?>
                                                        <li>
                                                            <a class="lang_tag" href="<?php echo route('lang.switch', $lang) ?>" style=" margin: auto; padding: 16px; "><?php echo $language ?></a>
                                                        </li>
                                                    <?php endif; ?>
                                               <?php endforeach;?>
                                                </ul>
                    </div> -->
                  </div>

            </div>
        </div>
        <div class="col-md-12 mobile_heading">
            <div class="col-md-4 mobile_logo">
                <h4>Staff Times</h4>
            </div>
            <div class="col-md-8 mobile_nav">
                <span id="menu_navigation"><a href="#"><i class="fa fa-bars"></i></a></span>
            </div>
        </div>
        <div class="col-md-12 drop_down_menu">
            <ul class="menu_expand">
                <li class="mobile_tabs"><a href="<?php echo route('/')?>"><?php echo trans('main_page_header_nav.home');?></a></li>
                <li class="mobile_tabs"><a href="<?php echo route('tour')?>"><?php echo trans('main_page_header_nav.tour');?></a></li>
                <li class="mobile_tabs"><a href="<?php echo route('faq')?>"><?php echo trans('main_page_header_nav.faq');?></a></li>
                <li class="mobile_tabs"><a href="<?php echo route('blog')?>"><?php echo trans('main_page_header_nav.blog');?></a></li>
                <li class="mobile_tabs"><a href="<?php echo route('login')?>"><?php echo trans('main_page_header_nav.login');?></a></li>
                <li class="mobile_tabs"><a href="<?php echo url('/#register')?>"><?php echo trans('main_page_header_nav.register');?></a></li>
                <li class="mobile_tabs"><a href="<?php echo route('lang.switch', $codeOfLanguage) ?>">
                    <?= Config::get('languages')[$codeOfLanguage] ?></a></li>
            </ul>
        </div>
