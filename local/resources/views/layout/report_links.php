<div class="col-md-12 col-sm-12 col-xs-12 first">
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants start hvr-pop" id="reports_1">
                        <a href="<?php echo url('reports_1/'.$data['employee_details']['employee_id'])?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p><?php echo trans ('report_links.one_line_one')?></p>
                                    <p><?php echo trans ('report_links.one_line_two')?></p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
<div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="reports">
                        <a href="<?php echo url('reports/'.$data['employee_details']['employee_id'])?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p><?php echo trans ('report_links.two_line_one')?></p>
                                    <p><?php echo trans ('report_links.two_line_two')?></p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot1.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('stats_reports/'.$data['employee_details']['employee_id'])?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p><?php echo trans ('report_links.three_line_one')?></p>
                                    <p><?php echo trans ('report_links.three_line_two')?></p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="graph_report">
                            <a href="<?php echo url('graph_report/'.$data['employee_details']['employee_id'])?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p><?php echo trans ('report_links.four_line_one')?></p>
                                    <p><?php echo trans ('report_links.four_line_two')?></p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot3.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop end" id="activity_graph_report">
                        <a href="<?php echo url('activity_graph_report/'.$data['employee_details']['employee_id'])?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p><?php echo trans ('report_links.five_line_one')?></p>
                                    <p><?php echo trans ('report_links.five_line_two')?></p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot5.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop end" id="allowance_report">
                        <a href="<?php echo url('allowance_report/'.$data['employee_details']['employee_id'])?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p><?php echo trans ('report_links.six_line_one')?></p>
                                    <p><?php echo trans ('report_links.six_line_two')?></p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot5.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>
                    <script>
            $('document').ready(function(){
            var url=window.location.href.split( '/' );
           var last_part=url[ url.length - 2 ];
           console.log(last_part);
           $('#'+last_part).addClass('hover_color');

            });
           
            </script>
<?php
$res=Auth::user();
$path = $res['profile_image'];
if ($path==''||$path==null) {
    $path = 'https://www.stafftimes.com/image/webapp.png';
}
$b64_url = 'php://filter/read=convert.base64-encode/resource='.$path;
$b64_img = file_get_contents($b64_url);
$type = pathinfo($path, PATHINFO_EXTENSION);
$base64 = 'data:image/' . $type . ';base64,' . $b64_img;
?>
<script type="text/javascript">
    var image64="<?=$base64;?>";
</script>