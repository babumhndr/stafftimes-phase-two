   <!DOCTYPE html>
<html>
    <head>
        <title><?php echo $metaTitle; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="<?php if(!empty($metaKeyword)){echo $metaKeyword;}?>" />
 <meta name="description" content=" <?php echo $metaDescription; ?>">
 <meta name="author" content="">
  <meta property="og:site_name" content="Staff Times" />
    <meta property="og:title" content="<?php if(!empty($metaTitle)){echo $metaTitle;}?>" />
    <meta property="og:description" content="<?php if(!empty($metaDescription)){echo $metaDescription;}?>" />
    <meta property="og:url" content="<?php if(!empty($metaUrl)){echo $metaUrl;}?>" />
    <meta property="og:type" content="website" />
    <link rel="canonical" href="https://stafftimes.com/">
    <meta property="og:image" content="<?php echo url("/image/stafftimes_logo.png")?>">
        <link rel="shortcut icon" href="<?php echo url('image/new-favicon-logo1.png')?>">
        <link rel="stylesheet" href="<?php echo url('/css/font-awesome.min.css')?>">
        <link rel="stylesheet" href="<?php echo url('/css/bootstrap.css')?>">
        <link href="<?php echo url('/css/rgen_min.css')?>" rel="stylesheet">
        <link href="<?php echo url('/css/custom.css')?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo url('/assets/css/sweet-alert.css')?>">
        <script src="<?php echo url('/js/jquery-2.2.0.min.js')?>"></script> 
        <script src="<?php echo url('/assets/js/sweet-alert.js')?>"></script>     
    </head>
  <style type="text/css">
  ::-webkit-scrollbar {
width: 6px;
}
::-webkit-scrollbar-thumb {
background-color: #67B0E1;
}
::-webkit-scrollbar-track {
background-color: #D4D4D4;
}
/*.lang_tag:hover
{
    background: #3b99d9 !important;

}*/

</style>
<!-- <script type="text/javascript">
var userLang = navigator.language || navigator.userLanguage;
var userLanguages = userLang.substring(0, 2);
     $.ajax({
     url: "langChangeForBrowser/"+userLanguages,
     success: function(data) {

     },
     async:false
  });
</script> -->