<div class="col-md-12 col-sm-12 col-xs-12 first">
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants start hvr-pop" id="reports_1">
                        <a href="<?php echo url('reports_1')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>MY</p>
                                    <p>OVERTIME</p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
<div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="reports">
                        <a href="<?php echo url('reports')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>DETAILED</p>
                                    <p>REPORT</p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot1.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('stats_reports')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>OVERTIME</p>
                                    <p>STATISTICS</p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="graph_report">
                            <a href="<?php echo url('graph_report')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>OVERTIME</p>
                                    <p>GRAPH</p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot3.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop end" id="activity_graph_report">
                        <a href="<?php echo url('activity_graph_report')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>ACTIVITY</p>
                                    <p>GRAPH REPORT</p>
                                </div>
                                <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot5.png')?>') no-repeat;">
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>
                    <script>
            $('document').ready(function(){
            var url=window.location.href;
           var last_part=url.substr(url.lastIndexOf('/') + 1);
           $('#'+last_part).addClass('hover_color');

            });
           
            </script>