<!DOCTYPE html>
<html>
    <head>
        <title>Staff Times - <?php echo trans('welcome.para1'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="time tracking, time, tracking, tracker, working day, worklog, log, working hours, offset tracking, flextime, flexitime, hours tracker, work, time & attendance, attendance, time and attendance, timer,time and attendance software,time & attendance software" />
        <meta name="keywords" content="Zeiterfassung, Arbeitszeit, Arbeitstag, Sollzeit, Gleitzeit, Flextime, Zeit, Zeiterfassen, Stundenabrechnung, Betriebsstunden, Arbeit, Präsenzzeiten, Präsenzzeit, Kontaktzeiten, Timer, zeiterfassung app, zeiterfassung schweiz, zeiterfassung excel, time tracking app, time tracking excel, time tracking software, time tracker app, time tracker software" />
        <meta name="keywords" content="Winterthur , winterthur , london , zurich , zürich , bangalore , bengaluru , bern , basel , luzern , zug , st gallen , chur , thun , biel , stuttgart , münchen , berlin , hamburg , frankfurt , bonn , amsterdam , stockholm , oslo , köln , düsseldorf , bochum , wien , innsbruck , bregenz , schweiz , deutschland , österreich , sverige , norge , danmark , kobnhavn , nederland" />
 <meta name="description" content=" <?php echo trans('welcome.para2'); ?>">
 <meta name="author" content="">
 <?php 
 $langType=trans('translate.lang');
 if ($langType =="en") {
  $blogtitle=$data['title_en']; 
  $blogdata=strip_tags($data['body_en']);
  }
  else if ($langType =="de")
  {
    $blogtitle=$data['title_de']; 
    $blogdata=strip_tags($data['body_de']);
  }

 $id = $data['_id'];
 $blogimage=$data['blog_image'];
 ?>
	<meta property="fb:app_id" content="667873140022627" />
  <meta property="og:site_name" content="Staff Times" />
    <meta property="og:title" content="<?php echo $blogtitle ?>" />
    <meta property="og:description" content="qwertyui" />  
	<meta property="og:url" content="https://stafftimes.com/bloglanding/<?php echo $id ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?php echo $blogimage ?>">
        <link rel="shortcut icon" href="<?php echo url('image/new-favicon-logo1.png')?>">
        <link rel="stylesheet" href="<?php echo url('/css/font-awesome.min.css')?>">
        <link rel="stylesheet" href="<?php echo url('/css/bootstrap.css')?>">
        <link href="<?php echo url('/css/rgen_min.css')?>" rel="stylesheet">
        <link href="<?php echo url('/css/custom.css')?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo url('/assets/css/sweet-alert.css')?>">
        <script src="<?php echo url('/js/jquery-2.2.0.min.js')?>"></script> 
        <script src="<?php echo url('/assets/js/sweet-alert.js')?>"></script>     
    </head>
  <style type="text/css">
  ::-webkit-scrollbar {
width: 6px;
}
::-webkit-scrollbar-thumb {
background-color: #67B0E1;
}
::-webkit-scrollbar-track {
background-color: #D4D4D4;
}
/*.lang_tag:hover
{
    background: #3b99d9 !important;

}*/

</style>
<!-- <script type="text/javascript">
var userLang = navigator.language || navigator.userLanguage;
var userLanguages = userLang.substring(0, 2);
     $.ajax({
     url: "langChangeForBrowser/"+userLanguages,
     success: function(data) {

     },
     async:false
  });
 </script> -->
