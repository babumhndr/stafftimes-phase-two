<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo url('image/new-favicon-logo1.png')?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="author" content="" />
	<meta http-equiv="refresh" content="3600;url=<?php echo url('logout1'); ?>" />
	<title>Staff Times</title>
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/css.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/hover.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/fonts/linecons/css/linecons.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('css/fonts/fontawesome/css/font-awesome.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('css/font-awesome.min.css')?>">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/bootstrap.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/bootstrap-social.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/xenon-core.css')?>">
 	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/xenon-components.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/header.css')?>">
	<link rel="stylesheet" type='text/css' href="<?php echo url('/assets/css/sweet-alert.css')?>">
	<script src="<?php echo url('assets/js/jquery-1.11.1.min.js')?>"></script>	
	<script src="<?php echo url('assets/js/date.format.js')?>"></script>
	<script src="<?php echo url('assets/js/geolocator.js')?>"></script>
    <script src="<?php echo url('assets/js/sweet-alert.js')?>"></script>
      <script src="<?php echo url('js/moment-with-locales.js')?>"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <script type="text/javascript">
    $(window).load(function()
    {
      $.ajax({
      url: '<?= url('').'/checkAuth' ?>',
      type:'post',
      success:function(response){
        console.log(response);
        if (response=="false") 
        {
            window.location.href = '<?php echo url('redirectToLogin'); ?>';
        }
      }
    });
    });
    </script>
</head>
<style>
.about {
    margin: auto;
    padding: 24px;
    background-color: #fff;
    width: 540px;
    min-width: 540px;
    border-radius: 4px;
    border: 1px solid #c9d7df;
    -webkit-transform-origin: top left;
    transform-origin: top left;
}

.layout__box.o__has-columns {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
}

.about__header {
    text-align: center;
}
.layout__box {
    -webkit-flex: 0 0 auto;
    -ms-flex: 0 0 auto;
    flex: 0 0 auto;
    min-width: 0;
    min-height: 0;
}
.about .about__product-icon {
    height: 72px;
    width: 72px;
}
.product-icon {
    line-height: 0;
    position: relative;
}
.about__title {
    margin-top: auto;
    margin-bottom: auto;

}
.plan-label.o__pro {
    background-color: #1f8ded;
}
.plan-label {
    border-radius: 3px;
    font-weight: 700;
    text-transform: uppercase;
    padding: 4px;
    color: #fff;
    display: inline-block;
    font-size: 10px;
    vertical-align: baseline;
    margin-left: 8px;
    margin-top: .2em;
}
.plan-label, .team__dropdown__item-meta {
    line-height: 1;
}
.about__h2 {
    font-size: 20px;
    line-height: 28px;
    font-weight: 500;
    color: #37474f;
}
.about__h3 {
    font-weight: 400;
    text-align: left;
}
.t__h3 {
    font-size: 14px;
    line-height: 20px;
    font-weight: 100;
    color: #37474f;
}
.about__continue-container {
    margin-left: auto;
    margin-top: auto;
    margin-bottom: auto;
}
.sp__2 {
    height: 20px;
}
.tutorial-groups__header__hr.o__no-margin {
    margin: 0 -24px;
}
.sp__hr.o__no-margin {
    margin: 0;
}
.sp__hr {
    height: 0;
    margin: 20px 0;
    border: 0;
    border-top: 1px solid #e1e7ea;
    background-color: transparent;
}
hr {
    box-sizing: content-box;
    display: block;
    height: 1px;
    border-top: 1px solid #ccc;
    margin: 1em 0;
}
body, fieldset, hr {
    padding: 0;
}
fieldset, hr {
    border: 0;
    padding: 0;
}
.sp__1 {
    height: 10px;
}
.take-a-tutorial__card {
    height: 50px;
}
.card {
    background-color: #fff;
    border: 1px solid #c9d7df;
    border-radius: 3px;
    box-shadow: 0 0 0 transparent;
    transition: box-shadow linear 40ms,border-color linear .2s;
    position: relative;
}
.btn, .card, .kv__key, .m__team-member-selector__content, .mixpanel-steps .step-instructions, .tbl__fixed__wrapper {
    box-sizing: border-box;
}
.take-a-tutorial__tutorial-content {
    cursor: pointer;
    height: 40px;
    width: 95%;
}
.u__center-item {
    left: 50%!important;
    -webkit-transform: translate(-50%,-50%)!important;
    transform: translate(-50%,-50%)!important;
}
.u__center-item, .u__m__0 {
    margin: 0!important;
}
.u__center-item, .u__center-item-vertically {
    top: 50%!important;
    position: absolute!important;
}
.u__center-item-vertically {
    -webkit-transform: translate(0,-50%)!important;
    transform: translate(0,-50%)!important;
    margin: 0!important;
}

.u__center-item, .u__center-item-vertically {
    top: 50%!important;
    position: absolute!important;
}
.take-a-tutorial__tutorial-icon-wrapper {
    margin-right: 5px;
}
.take-a-tutorial__tutorial-icon-wrapper, .take-a-tutorial__tutorial-text {
    display: inline-block;
    vertical-align: middle;
}
.tutorial__tutorial-icon {
    background-color: #1f8ded;
    background-repeat: no-repeat;
    background-size: 24px;
    border-radius: 50%;
    height: 24px;
    width: 24px;
}
.take-a-tutorial__tutorial-description {
    font-weight: 700;
    text-transform: lowercase;
}
.c__light {
    color: #6e8593;
}
.take-a-tutorial__tutorial-status {
    color: #038bcf;
    cursor: pointer;
    right: 0;
}
.tutorial__tutorial-text {
    display: inline-block;
    vertical-align: middle;
}
.about__section.o__info {
    -webkit-align-self: center;
    -ms-flex-item-align: center;
    -ms-grid-row-align: center;
    align-self: center;
    background-color: #f7fafc;
    padding: 24px;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}
.modal .modal-dialog .modal-content {
	font-family:"Open Sans";
}
</style>
<style type="text/css">
.langmenu
{
    position: absolute;
    left: 64%;
    top: 30px;
}
	  ::-webkit-scrollbar {
width: 6px;
}
::-webkit-scrollbar-thumb {
background-color: #67B0E1;
}
::-webkit-scrollbar-track {
background-color: #D4D4D4;
}
	.notifications {
		left: auto;
		right: 0;
	}
	.user-info-navbar .user-info-menu > li .dropdown-menu.user-profile-menu {
		right: 0px;
	}
	.open > .notifications:after {
		content: '';
position: absolute;
border-style: solid;
border-width: 0 15px 15px;
border-color: #FFFFFF transparent;
display: block;
width: 0;
z-index: 1;
top: -15px;
left: auto;
right: 15px;
	}
	.open > .user-profile-menu:after {
		content: '';
position: absolute;
border-style: solid;
border-width: 0 15px 15px;
border-color: #FFFFFF transparent;
display: block;
width: 0;
z-index: 1;
top: -15px;
left: auto;
right: 15px;
	}
	.tooltip {
		font-family: 'Open Sans';
	}
.logo1 img {
    -webkit-transition-duration: .8s;
    -moz-transition-duration: .8s;
    -o-transition-duration: .8s;
    transition-duration: .8s;
    -webkit-transition-property: -webkit-transform;
    -moz-transition-property: -moz-transform;
    -o-transition-property: -o-transform;
    transition-property: transform;
    overflow: hidden;
}

.logo1 img:hover {
    -webkit-transform: rotateX(0deg) rotateY(-360deg);
    -moz-transform: rotateX(0deg) rotateY(-360deg);
    -o-transform: rotateX(0deg) rotateY(-360deg);
}
.notification-no{
    text-align: center;
    padding: 20px 0px 20px 0px;
    font-size: 15px;
}
.trail_day_msg
{
	position: relative;
	top: 34px;
	color:#fff;
    font-weight: 900;
}
.hover_color {
    color: #fff;
}
.template_link {
        color: #2392ec;
    }
    .template_link a {
        color: #2392ec;
    }
</style>
<!-- for logo base64 encode for report-->
<?php
$user=Auth::user();
$id=$user['_id'];
$email=$user['email'];
$name=$user['company_name'];
$country_name=$user['country_name'];
$language_name=$user['language_name'];
$company_unique_id=$user['company_unique_id'];
/*$userLogo=$user['profile_image'];*/
/*$userLogo=url('image/stafftimes.png');
$logo=convertImageToBase64($userLogo);
function convertImageToBase64($path)
{
$type =pathinfo($path, PATHINFO_EXTENSION);
$data =file_get_contents($path);
$data = Image::make($data)->resize(60, 60)->encode('data-url');
return $data;
}*/
?>
<script type="text/javascript">
    var companyName="<?php echo $user['company_name'];?>";
   /* alert(logoForReport);*/
</script>
<!-- end-->
<script type="text/javascript">
$(document).ready(function(){
        $('#f').click(function() {
    $("#e").toggle();
});
    });
</script>
<body class="page-body">
<!-- hour format coverter for php -->
<?php
function time_in_24_hour_format($time){
$time_in_24_hour_format  = date("H:i", strtotime($time));
return $time_in_24_hour_format;
} 
function time_in_12_hour_format($time){
$time_in_12_hour_format  = date("g:i a", strtotime($time));
return $time_in_12_hour_format;
} 
?>
<?php 
use App\Http\Controllers\CompanyController;
$getDetail=CompanyController::getTrialDays(); 
$days=$getDetail['days'];
if ($days>=31) 
	{
		$days=30;
	}
$trail=30-$days;
$trailMsg='';
if ($trail==0) 
{
	$trailMsg=trans('translate.trial_warning3');	
}
else
{
	$trailMsg=trans('translate.trial_warning1').' '.$trail.' '.trans('translate.trial_warning2');	
}
?>
	<!-- <div class="settings-pane">
			
		<a href="#" data-toggle="settings-pane" data-animate="true">
			&times;
		</a>
		
		<div class="settings-pane-inner">
			
			<div class="row">
				
				<div class="col-md-4">
					
					<div class="user-info">
						
						<div class="user-image">
							<a href="extra-profile.html">
								<img src="assets/images/user-2.png" class="img-responsive img-circle" />
							</a>
						</div>
						
						<div class="user-details">
							
							<h3>
								<a href="extra-profile.html">John Smith</a>
								
								
								<span class="user-status is-online"></span>
							</h3>
							
							<p class="user-title">Web Developer</p>
							
							<div class="user-links">
								<a href="extra-profile.html" class="btn btn-primary">Edit Profile</a>
								<a href="extra-profile.html" class="btn btn-success">Upgrade</a>
							</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div class="col-md-8 link-blocks-env">
					
					<div class="links-block left-sep">
						<h4>
							<span>Notifications</span>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk1" />
								<label for="sp-chk1">Messages</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk2" />
								<label for="sp-chk2">Events</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk3" />
								<label for="sp-chk3">Updates</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk4" />
								<label for="sp-chk4">Server Uptime</label>
							</li>
						</ul>
					</div>
					
					<div class="links-block left-sep">
						<h4>
							<a href="#">
								<span>Help Desk</span>
							</a>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Support Center
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Submit a Ticket
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Domains Protocol
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Terms of Service
								</a>
							</li>
						</ul>
					</div>
					
				</div>
				
			</div>
		
		</div>
		
	</div> -->
	<div class="modal fade" id="mymodal">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" id="close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" style="text-align:center"><img src="<?php echo url('image/webapp.png')?>" width="50" alt="">&nbsp;&nbsp;&nbsp;<?php echo trans ('header.setup_guide')?></h4>
                </div>
                
                <div class="modal-body">
                
                    <div class="about  ">
    <div class="about__header layout__box o__has-columns">

      <div class="about__title">
        <div class="layout__box o__has-columns u__center-layout">
          <div class="about__h2"><?php echo trans ('header.welcome_stafftimes')?></div>


<!---->        </div>

        <div class="t__h3 about__h3"><?php echo trans ('header.prepared_guide')?></div>
      </div>

      <div class="about__continue-container">
<!---->      </div>
    </div>

    <div class="sp__2"></div>

    <!-- <div class="about__body layout__box o__has-columns">
      <div class="about__section o__info">
        <div class="about__h2">Onboard new users, make announcements, and re-engage users with targeted messages, triggered by time or behavior</div>
      </div>

    </div> -->

    <div id="ember1916" class="ember-view">  <div class="sp__2"></div>
    <hr class="tutorial-groups__header__hr sp__hr o__no-margin">
  <div class="sp__1"></div>

    <div class="sp__1"></div>
    <div id="ember1927" class="ember-view"><div class="card take-a-tutorial__card">
  <div class="take-a-tutorial__tutorial-content u__center-item" data-ember-action="1928">
    <div class="u__center-item-vertically">
      <!-- <div class="take-a-tutorial__tutorial-icon-wrapper">
        <div id="ember1937" class="ember-view"><div class="take-a-tutorial__tutorial-icon u__center-item-vertically;" style="background-image: url(https://static.intercomassets.com/ember/assets/images/guide/icons/arrow_filled-2c63ff0b3ae817028dc88ae64c389f7d.png);">
</div>

</div>
      </div> -->

      <div class="take-a-tutorial__tutorial-text">
        <span class="c__light"><?php echo trans ('header.setup_organization')?></span> <!-- <span class="take-a-tutorial__tutorial-description">Send an auto message</span> -->
      </div>
    </div>

    <div class="take-a-tutorial__tutorial-status u__center-item-vertically">
        <a href="<?php echo trans ('translate.link1')?>" target="_blank" ><?php echo trans ('header.start_tutorial')?></a>
    </div>
  </div>
</div>
</div>
    <div class="sp__1"></div>
    <div id="ember1943" class="ember-view"><div class="card take-a-tutorial__card">
  <div class="take-a-tutorial__tutorial-content u__center-item" data-ember-action="1944">
    <div class="u__center-item-vertically">
      <!-- <div class="take-a-tutorial__tutorial-icon-wrapper">
        <div id="ember1945" class="ember-view"><div class="take-a-tutorial__tutorial-icon u__center-item-vertically;" style="background-image: url(https://static.intercomassets.com/ember/assets/images/guide/icons/arrow_filled-2c63ff0b3ae817028dc88ae64c389f7d.png);">
</div>

</div>
      </div> -->

      <div class="take-a-tutorial__tutorial-text">
        <span class="c__light"><?php echo trans ('header.start_tracking')?></span><!--  <span class="take-a-tutorial__tutorial-description">Get product feedback</span> -->
      </div>
    </div>

    <div class="take-a-tutorial__tutorial-status u__center-item-vertically">
        <a href="<?php echo trans ('translate.link2')?>" target="_blank" ><?php echo trans ('header.start_tutorial')?></a>
    </div>
  </div>
</div>
</div>

  <div class="sp__2"></div>
  <p class="c__light">
    <a id="ember1947" class="ember-view"><?php echo trans ('header.documentation')?></a>
  </p>
  <div class="sp__1"></div>
  	<ul> 

  		<li><a href="" target="_blank" id="pdfLoc"><?php echo trans ('header.guide_timesheets')?><span style="float:right;">></span></a></li>
        <script>
            var pdfLoc = window.location.protocol+'//'+window.location.hostname+'/'+'<?php echo trans('header.pdf_link')?>';
     
            $('#pdfLoc').attr("href", pdfLoc);
        </script>
  		<div class="sp__1"></div>
  		<li><a href="<?php echo route('faq')?>" target="_blank"><?php echo trans ('header.guide_faq')?><span style="float:right;">></span></a></li>
  		<div class="sp__1"></div>
  		<li><a href="<?php echo route('blog')?>" target="_blank"><?php echo trans ('header.guide_blog')?><span style="float:right;">></span></a></li>
  		<div class="sp__1"></div>
        <li><a href="<?php echo trans ('header.user_roles')?>" target="_blank"><?php echo trans ('header.features_user')?><span style="float:right;">></span></a></li>
        <div class="sp__1"></div>
  	</ul>
</div>
  </div>
                 </div>  
                 <div class="modal-footer">
                 <p style="float: left;margin-top: 8px;font-weight: bolder;">V 1.03</p>
					<button type="button" class="btn btn-white" data-dismiss="modal"><?php echo trans ('header.guide_close')?></button>
				</div> 
                </div>
                </div>
                </div>
	
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			
		<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
		<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
		<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
		<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="<?php echo url('dashboard')?>" class="logo-expanded logo1">
							<img src="<?php echo url('image/webapp.png')?>" width="75" alt="" />
							
						</a>
						<!--  <p style="color: red !important;font-size: 9px;margin: 0px;">Version 0.17</p> -->
						<a href="#" class="logo-collapsed">
							<img src="<?php echo url('image/stafftimes2.png')?>" alt="" style=" width: 53px; height: 32px; margin-left: -16px; margin-top: -4px; "/>
							<!-- <p class="logo">MOT</p> -->
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
                        <ul class="user-info-menu right-links list-inline list-unstyled">
                       <!--  <li class="dropdown">
						<a href="#" data-toggle="dropdown">
                            <i class="fa-bell-o"></i>
                            <span class="badge badge-purple notif" id="notification_list_count_bell"></span>
                        </a>
                            
                        <ul class="dropdown-menu notifications">
                            <li class="top">
                                <p class="small">
                                    <a href="#" class="pull-right" onclick="makeAllRead()"><?php echo trans('translate.mark_all_read')?></a>
                                    <?php echo trans('translate.you_have')?> <strong id="notification_list_count"></strong> <?php echo trans('translate.new_notifications')?>.
                                </p>
                            </li>
                            
                            <li id="notification_list_no">
                                <ul class="dropdown-menu-list list-unstyled ps-scrollbar" id="notification_list">
                                    
                                    
                                </ul>
                            </li> -->
                            
                            
                        </ul>
                    </li> 
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa fa-bars"></i>
						</a>

                      <li class="dropdown user-profile">
                        <a href="#" data-toggle="dropdown">
                            <img src="<?php 
                            $res=Auth::user();
                            if (!empty($res['profile_image']))
                            {
                                echo $res['profile_image'];
                            }
                            else{
                                echo url("image/".trans('translate.lang').".jpg");
                            }
                            ?>" alt="user-image" class="img-inline userpic-32" width="32"style=" height: 56px; width: 56px; margin-top:-30px;" />
                            <span class="user_name">
                            <?php 
                            $res=Auth::user();
                            if (!empty($res))
                            {
                                echo $res['company_name']; 
                            }
                            else{
                                echo 'User&nbsp';
                            }
                            ?>
                                <i class="fa fa-caret-down"></i>
                            </span>
                        </a>
                        
                        <ul class="dropdown-menu user-profile-menu list-unstyled" style="z-index:9999;">
                            <li>
                            
                                <a href="<?php  $res=Auth::user(); echo url('profile/'.$res['id']); ?>">
                                    <i class="fa-user"></i>
                                    <?php echo trans('translate.my_profile')?>
                                </a>
                            </li>
                            <li class="last">
                                <a href="<?php echo route('logout')?>">
                                    <i class="fa-lock"></i>
                                    <?php echo trans('translate.logout')?>
                                </a>
                            </li>
                        </ul>
                    </li>  
                    </ul>
					</div>
					
								
				</header>
						
				
						
				<ul id="main-menu" class="main-menu auto-inherit-active-class">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<?php echo url('dashboard')?>">
							<img src="<?php echo url('assets/images/icon.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title" id="dashboard"><?php echo trans ('header.dashboard')?></span>
						</a>
					</li>
					<li>
						<a href="<?php echo url('#')?>">
							<img src="<?php echo url('assets/images/two.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title"><?php echo trans ('header.Employee')?></span>
						</a>
						<!-- <ul> -->
							<li class="sub_link">
								<a href="<?php echo url('add_employee')?>">
									<span class="title" id="add_employee"><?php echo trans ('header.Add_Employee')?></span>
								</a>
							</li>
                            <li class="sub_link">
                                <a href="add_role">
                                    <span class="title" id="add_employee">Manage roles</span>
                                </a>
                            </li>
                            <li class="sub_link">
                                <a href="manage_groups">
                                    <span class="title" id="add_employee">Manage groups</span>
                                </a>
                            </li>
							<li class="sub_link">
								<a href="<?php echo url('listing')?>">
									<span class="title" id="listing"><?php echo trans ('header.Listing')?></span>
								</a>
							</li>
                            <li class="sub_link">
                                <a href="<?php echo url('planner_report')?>">
                                    <span class="title" id="planner_report"><?php echo trans ('header.planner_report')?></span>
                                </a>
                            </li>
						<!-- </ul> -->
					</li>
					<li>
						<a href="#">
							<img src="<?php echo url('assets/images/icon.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title"><?php echo trans ('header.Book_your_time')?></span>
						</a>
						<!-- <ul> -->
							<li class="sub_link">
								<a href="<?php echo url('manage_template')?>">
									<span class="title" id="manage_template"><?php echo trans ('header.template')?></span>
								</a>
							</li>
							<li class="sub_link">
								<a href="<?php echo url('manage_activity')?>">
									<span class="title" id="manage_activity"><?php echo trans ('header.activity')?></span>
								</a>
							</li>
						<!-- </ul> -->
					</li>
					<li>
						<a href="<?php echo url('backupPage')?>">
							<img src="<?php echo url('assets/images/database.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title" id="backup"><?php echo trans ('header.Backup')?></span>
						</a>
					</li>
					<li>
						<a href="<?php echo url('settings')?>" id="setting-onclick">
							<img src="<?php echo url('assets/images/icon (1).png')?>" class="a">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title" id="settings"><?php echo trans ('header.Settings')?></span>
						</a>
					</li>
					<!-- <li>
						<a href="<?php echo url('plansandprice')?>">
							<img src="<?php echo url('assets/images/icon.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title"><?php echo trans ('header.Plans')?></span>
						</a>
					</li> -->
                    <li>
                        <a href="#">
                            <img src="<?php echo url('assets/images/icon.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="title"><?php echo trans ('header.Plans_Main')?></span>
                        </a>
                        <!-- <ul> -->
                            <li class="sub_link">
                                <a href="<?php echo url('plansandprice')?>">
                                    <span class="title" id="plansandprice"><?php echo trans ('header.Plans')?></span>
                                </a>
                            </li>
                            <li class="sub_link">
                                <a href="<?php echo url('subscribedusers')?>">
                                    <span class="title" id="subscribedemployees"><?php echo trans ('header.subscribed_user')?></span>
                                </a>
                            </li>
                        <!-- </ul> -->
                    </li>
				</ul>
						
			</div>
			
		</div>
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled col-md-4">

					<li class="hidden-xs">
						<!-- <a href="#" id="modal_box_display" onclick="jQuery('#mymodal').modal('show', {backdrop: 'static'});">
							<i class="fa fa-question-circle" aria-hidden="true"></i>
						</a> -->
                        <div class="mot_dashboard_profile_inner">
                            <ul class="profile_time_list">
                                <li class="current_stats"><i class="fa fa-clock-o" aria-hidden="true">&nbsp&nbsp
                                    <span class="current_stats_font" id="time_now1"></span></i>
                                </li>
                            </ul>
                            <a href="javascript:;" id="modal_box_display" onclick="jQuery('#mymodal').modal('show', {backdrop: 'static'});">
                            <i class="fa fa-question-circle" aria-hidden="true" style="font-size:19px;"></i>
                        </a>
                        </div>
					</li>
				</ul>
				<ul class="hidden-sm hidden-xs mot_dashboard_profile_time col-md-4">
					<h2 style=" margin: 0px !important; text-align: center !important; color: #fff !important; "><?php $res=Auth::user();if (!empty($res)){echo $res['company_name'];}else{echo 'User&nbsp';}?></h2>
    			</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled ">
					<li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="" >
						<span class="trail_day_msg">
							<marquee class="marquee"><?php 
							if ($getDetail['payment_status']=="pending") {
								echo $trailMsg;
							}
							  ?>
                              </marquee>
						</span>
					</li>

					<li class="dropdown">
						<a href="#" data-toggle="dropdown">
							<i class="fa-bell-o"></i>
							<span class="badge badge-purple notif" id="notification_list_count_bell"></span>
						</a>
							
						<ul class="dropdown-menu notifications">
							<li class="top">
								<p class="small">
									<a href="#" class="pull-right" onclick="makeAllRead()"><?php echo trans('translate.mark_all_read')?></a>
									<?php echo trans('translate.you_have')?> <strong id="notification_list_count"></strong> <?php echo trans('translate.new_notifications')?>.
								</p>
							</li>
							
							<li id="notification_list_no">
								<ul class="dropdown-menu-list list-unstyled ps-scrollbar" id="notification_list">
	
								</ul>
							</li>
						</ul>
					</li>
					<li class="visible-sm visible-md visible-lg dropdown user-profile">
						<a href="#" data-toggle="dropdown" class="logo_a" style="padding: 25px 20px !important;">
							<img src="<?php 
							$res=Auth::user();
							if (!empty($res['profile_image']))
							{
								echo $res['profile_image'];
							}
							else{
								echo url("image/".trans('translate.lang').".jpg");
							}
							?>" alt="user-image" class="img-inline userpic-32" width="32"style=" height: 56px; width: 56px; margin-top:-20px; border-radius:50%" />
							<span class="user_name">
							<!-- <?php 
							$res=Auth::user();
							if (!empty($res))
							{
								echo $res['company_name']; 
							}
							else{
								echo 'User&nbsp';
							}
							?> -->
								<i class="fa fa-caret-down"></i>
							</span>
						</a>
						
						<ul class="dropdown-menu user-profile-menu list-unstyled" style="z-index:9999;">
							<li>
							
								<a href="<?php  $res=Auth::user(); echo url('profile/'.$res['id']); ?>">
									<i class="fa-user"></i>
									<?php echo trans('translate.my_profile')?>
								</a>
							</li>
							<li class="last">
								<a href="<?php echo route('logout')?>">
									<i class="fa-lock"></i>
									<?php echo trans('translate.logout')?>
								</a>
							</li>
						</ul>
					</li>
					
					<!-- <li>
						<a href="#" data-toggle="chat">
							<i class="fa-comments-o"></i>
						</a>
					</li>
					 -->
				</ul>
			<!-- 	<div class="langmenu" >
    <a id="f" style="cursor:pointer;"><?= Config::get('languages')[App::getLocale()] ?></a>
    <div id="e" style="display:none; background-color: #3a98d8;">
     <ul style="margin-bottom: 0px;list-style: none;padding: 0px;margin-top: 4px;">
  <?php foreach (Config::get('languages') as $lang => $language):?>
                                   <?php if ($lang != App::getLocale()):?>
                                        <li>
                                            <a class="lang_tag" href="<?php echo route('lang.switch', $lang) ?>" style="margin: auto;padding: 16px;padding-left: 0px;padding-right: 0px;"><?php echo $language ?></a>
                                        </li>
                                    <?php endif; ?>
                               <?php endforeach;?>
                                </ul>
    </div>
  </div> -->
			</nav>
		<!-- 	<script>
					var now= new Date();
				document.getElementById("date_now").innerHTML = now.format('ddd dd mmm, yyyy');
			</script> -->
<script>
	$(document).ready(function(){
		$(".tooltip").css("width", "130px");
		$(".tooltip").css("left", "-35px !important");
	});
</script>
			<script type="text/javascript">
			var userLang = navigator.language || navigator.userLanguage;
        	var userLanguages = userLang.substring(0, 2);
        	var time_of_mot= '<?php if (!empty($details['general_setting']['response']['0']['time_style'])) {echo $details['general_setting']['response']['0']['time_style'];}?>';
        	/*var offset_time= '<?php if (!empty($details['general_setting']['response']['0']['set_working_hours'])) {echo $details['general_setting']['response']['0']['set_working_hours'];}?>';*/
    //The callback function executed when the location is fetched successfully.
 /*   function onGeoSuccess(location) {
       // console.log(location);
        //console.log(location.coords.latitude);
        //console.log(location.coords.longitude);

        var lat=location.coords.latitude;
        var lng=location.coords.longitude;
    $.ajax({
        

        type : 'GET',  

        url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&sensor=true',

        success: function(response)   

        {
        	console.log(response);
        	var loc1=response.results[3].formatted_address;
        	console.log(loc1);
        	loc1=loc1.split(",");
        	count=loc1.length;
        	var loc =loc1[count-2]+','+loc1[count-1];
        	$('#place_temp').html(loc+', ');
		}
			});
        $.ajax({
        

        type : 'GET',  

        url: 'http://api.openweathermap.org/data/2.5/weather?lat='+lat+'&lon='+lng+'&appid=827f6ed0cf00a21e2e0b6531f74fea89',

        success: function(response)   

        {
        	$('#place_temp').append((Math.round((response.main.temp-273.15) * 10 ) / 10)+'\u00B0C');

		}
			});
    }
    //The callback function executed when the location could not be fetched.
    function onGeoError(error) {
        console.log(error);
    }

    window.onload = function () {
        //geolocator.locateByIP(onGeoSuccess, onGeoError, 2, 'map-canvas');
        var html5Options = { enableHighAccuracy: true, timeout: 6000, maximumAge: 0 };
        geolocator.locate(onGeoSuccess, onGeoError, true, html5Options, 'map-canvas');
    };*/

</script>
<script>
         $('document').ready(function(){
            var url=window.location.href.split( '/' );
           var last_part=url[ url.length - 1 ];
           console.log(last_part);
           $('#'+last_part).addClass('hover_color');

            });        
</script>
<script>
function myNotification()
{
var id="<?php echo $user['_id'];?>";
var deployed_time = moment().format('Y-MM-DD H:mm:ss'); 
var res=[]; 
var posted_time = [];
var noNewNotificationTranslation="<?php echo trans('popup.no_new_notification_translation')?>";
			$.ajax({
					method: 'POST',
					url: '<?= url('').'/myNotification' ?>',
					data:{"id":id},
					success: function(response) 
					{   
						console.log(response['notification_list']);
                        if(response['notification_list'].length > 0){
                        for(var i=0; i<response['notification_list'].length; i++){
						   posted_time[i] = moment(response["notification_list"][i]["notification_sent_at"]).startOf("minute").fromNow();
						   console.log(posted_time[i]);
						   var content=response["notification_list"][i]["notification_content"];
						   var displayMsg='';
						   var ahref='';
						   if (content=="deployed new activity.") 
						   {
						   	displayMsg="<?php echo trans('translate.deployed_new_activity')?>";
						   	ahref="<?php echo url('manage_activity')?>";
						   }
						   else if (content=="deployed new template.") 
						   {
						   	displayMsg="<?php echo trans('translate.deployed_new_template')?>";
						   	ahref="<?php echo url('manage_template')?>";
						   }
						   res[i] = '<li class="active notification-success"><a href="#"  onclick="changeReadStatus(\''+response["notification_list"][i]['_id']+'\',\''+ahref+'\')"><i class="fa-user"></i><span class="line"><strong>'+response["notification_list"][i]["sender_info"]["company_name"]+' '+displayMsg+'</strong></span><span class="line small time">'+posted_time[i]+'</span></a></li>';
						  
						}
						$('#notification_list_count').html(response['notification_list'].length);
                        $('#notification_list_count_bell').html(response['notification_list'].length);
						$('#notification_list').html(res);
                        //$('#notification_list_no').html('');
                        }
						else{
                        $('#notification_list_count').html("0");
                        $('#notification_list_count_bell').html("0");
                        var res1 = '<li class="active notification-no"><strong>'+noNewNotificationTranslation+'</strong></li>'; 
                        $('#notification_list').html(res1);
						//$('#notification_list_no').html(res1);
						}
					}

				}); 
}
myNotification();
</script>
<script>
function makeAllRead()
{
swal({   
	title: "<?php echo trans('popup.you_sure')?>",   /*
	text: "You will not be able to recover this template!",*/   
	type: "warning",   
	showCancelButton: true,   
	confirmButtonColor: "#2acd84",   
	confirmButtonText: "<?php echo trans('popup.yes_make_it_read')?>",  
    cancelButtonText:"<?php echo trans('popup.cancel');?>",   
	closeOnConfirm: false 
	}, 
	function()
	{ 
var id="<?php echo $user['_id'];?>";
var deployed_time = moment().format('Y-MM-DD H:mm:ss'); 
			$.ajax({
					method: 'POST',
					url: '<?= url('').'/makeItRead' ?>',
					data:{"company_id":id},
					success: function(response) 
					{   
						if(response['status'] == 'success')
						{
							 swal({  
                         title: "<?php echo trans('popup.success')?>", 
                         text: "<?php echo trans('popup.mark_read_successfully')?>",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            location.reload();
  
                        });
						}
						else{
							 swal({  
                         title: "<?php echo trans('popup.error_')?>", 
                         text: "<?php echo trans('popup.error_occurred')?>",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            location.reload();
  
                        });
						} 
					}

				}); 
	});
}

function changeReadStatus(notificationId,aHref)
{ 
	var id="<?php echo $user['_id'];?>";
			$.ajax({
					method: 'POST',
					url: '<?= url('').'/makeClikedRead' ?>',
					data:{"company_id":id,"notification_id":notificationId},
					success: function(response) 
					{ 
					console.log(response);  
						if(response['status'] == 'success')
						{
							window.location.href=aHref;
						}
						else{
							window.location.href=aHref;
						} 
					}

				});
}
$('#setting-onclick').click(function(){
localStorage.setItem("value", "");
});
/*$('#sub1').addClass('expanded');
$('#sub2').addClass('expanded');
$("#subul1").css("display", "block");
$("#subul2").css("display", "block");
$('#sub3').addClass('expanded');
$("#subul3").css("display", "block");*/
</script>
</body>
</html>
