<style>
#cookie-bar {background:#111111; opacity:0.9; height:auto; line-height:24px; color:#eeeeee; text-align:center; padding:20px 10px;position: fixed;width: 100%;bottom: 0;z-index: 9999;}
#cookie-bar.fixed {position:fixed; top:0; left:0; width:100%;}
#cookie-bar.fixed.bottom {bottom:0; top:auto;}
#cookie-bar p {margin:0; padding:0;}
#cookie-bar a {color:#ffffff; display:inline-block; border-radius:3px; text-decoration:none; padding:0 6px; margin-left:8px;}
#cookie-bar .cb-enable {background:#007700;}
#cookie-bar .cb-enable:hover {background:#009900;}
#cookie-bar .cb-disable {background:#990000;}
#cookie-bar .cb-disable:hover {background:#bb0000;}
#cookie-bar .cb-policy {background:#2392ec;}
#cookie-bar .cb-policy:hover {background:#2392ec;}
</style>
<div class="horizontal_line"></div>
      <div class="row mot_social_networking" >
        <div class="container">
            <div class="col-md-12 sixth-part">
                <div class="col-md-6 col-sm-6 col-xs-12 social-left-part">
                    <h5 class="sixth-part-heading"><?php echo trans('main_page_footer.social') ?></h5>
                    <h2 class="sixth-part-sub-heading"><?php echo trans('main_page_footer.join_us') ?></h2>
                    <?php 
                    $language=trans("translate.lang");
                    $fbLink="";
                    if ($language=="en") {
                      $fbLink="https://www.facebook.com/stafftimesglobal";
                    }
                    else
                    {
                      $fbLink="https://www.facebook.com/Staff-Times-Deutsch-1827217914174420/?fref=ts";
                    }
                    ?>
                    <div class="icons icons_facebook"><a href="<?= $fbLink ?>" target="_blank"><i class="fa fa-facebook fo" ></i></a></div>
                    <div class="icons icons_youtube"><a href="https://www.youtube.com/channel/UCmOsaY5ip9NEJjNnPpZc3lA" target="_blank"><i class="fa fa-youtube-play fo"></i></a></div>
                    <div class="icons icons_linkedin"><a href="https://www.linkedin.com/company/staff-times?trk=ppro_cprof" target="_blank"><i class="fa fa-linkedin fo"></i></a></div>
                    <!-- <div class="icons"><a href='#'><i class="fa fa-instagram fo"></i></a></div> -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 social-right-part">
                    <h5 class="sixth-part-heading"><?php echo trans('main_page_footer.newsletter') ?></h5>
                    <h2 class="sixth-part-sub-heading"><?php echo trans('main_page_footer.newsletter_body') ?></h2>
                    <form id="home-search-form" action="ccc">
                     <div class="input-group home-input">
                    <input type="text" class="form-control" id="mail" placeholder="<?php echo trans('main_page_footer.newsletter_email') ?>">
                    <span class="input-group-btn">
                    <button class="mail_btn" type="button" id="newsletterBtn"><i class="fa fa-envelope-o"></i></button>
                    </span>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row mot_footer">
        <div class="container">
          <div class="col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
            <h6 class="footer-heading"><?php echo trans('main_page_footer.products') ?></h6>
            <ul class="list-unstyled">
            <li class="address_list"><a href="<?php echo url('/#register')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.register');?></a></li>
            <li class="address_list"><a href="<?php echo route('price')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.price');?></a></li>
            <li class="address_list"><a href="<?php echo route('tour')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.tour');?></a></li>
             <li class="address_list"><a href="<?php echo route('termsandcondition')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.terms_conditions');?></a></li>
              <li class="address_list"><a href="<?php echo route('privacy')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.privacy');?></a></li>
          </ul>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
            <h6 class="footer-heading"><?php echo trans('main_page_footer.help') ?></h6>
            <ul class="list-unstyled">
            <li class="address_list"><a href="<?php echo route('contactus')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.contactus');?></a></li>
            <li class="address_list"><a href="<?php echo route('faq')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.faq');?></a></li>
            <li class="address_list"><a href="<?php echo route('blog')?>" style="color: #969696;"><?php echo trans('main_page_header_nav.blog');?></a></li>
          </ul>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="text-align: left;" >
            <div class="logo_div">
            <a href="#" class="logo">
                <img src="<?php echo url('image/webapp.png')?>" width="55" alt="" />
                </a>
                <h3 class="footer-heading1" ><?php echo trans('main_page_footer.myovertime_name') ?></h3>
            </div>
            
            <!--<div class="col-md-12 col-sm-12 col-xs-12 footer-address" style="float:left;padding-bottom:10px;">
             <div class="col-md-6 col-sm-6 col-xs-6" style="text-align:right;float:left;">
              
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6" style="text-align:left;float:left;">
              
            </div> 
          </div>-->
            <p class="footer-address"><?php echo trans('main_page_footer.myovertime_address') ?></p>
           <!--  <hr class="line-styling-3"> -->
            <p class="footer-address"><?php echo trans('main_page_footer.myovertime_copyright') ?>&nbsp&nbsp<i class="fa fa-copyright"></i>&nbsp&nbsp <?php echo trans('main_page_footer.myovertime_right') ?></p>
          </div>
        </div>
    </div> 
   </body>
  <style>
  .linker {
    text-decoration: none !important;
    font-size: 16px;
  }
  .address_list {
                padding: 10px 0px 0px 0px;
            }
  .footer-heading {
                color: #fff;
                padding-bottom: 10px;
            }
            .footer-address {
                color: #969696;
                float: left;
            }
   .logo {/*
                text-align: center;*/
                color: white;
                padding-left: 0px;
                float: left;
                top: -30px;
                left: 30px;
            }
    .footer-heading1 {
                padding-left: 20px;
                    float: left;
                color: #fff;
            }
  </style>
<script>
 $('#newsletterBtn').click(function() {
/*$('#home-search-form').ajaxForm({
    beforeSend: function () {
    var mail = $('#mail').val();
    var isValid = true;
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(mail == null || mail == '' ){
        isValid = false;
        swal("Error!", "Please fill all the fields", "error");
    }
    else if(regex.test(mail) == false){
        isValid = false;
        swal("Error!", "Please check the Email address", "error");
    }
  },
    success: function(data) {
        console.log(data);
         if(data.status=='success')
         {
            swal("Success!", "NewsLetter sent!", "success");
            $("#form")[0].reset();
             $('#loader').hide();
             $('#btn2').show();
         }
    },
}); */

var mail = $('#mail').val();
$.ajax({
                method: 'POST',
                url: '<?= url('').'/newsletterSend' ?>',
                data:{"mail":mail},
                  beforeSend: function () {
                  var isValid = true;
                  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                  if(mail == null || mail == '' ){
                  isValid = false;
                  swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.all_fields');?>", "error");
                  return false;
                  }
                  else if(regex.test(mail) == false){
                  isValid = false;
                  swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.check_email');?>", "error");
                  return false;
                  }
                  },
                success:function(response){ 
                  console.log(response);
                  if (response['status']=='success') 
                    { 
                    swal({  
                         title:"<?php echo trans('popup.success');?>", 
                         text:  "<?php echo trans('popup.mail_sent');?>",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){
                          $('#mail').val($('#mail').html() + '');
                        });
                    }  
                  else    
                    {
                    swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.error');?>", "error"); 
                    /*window.location.reload(); */
                    }     
                }
              });
});
</script>
<script type="text/javascript">
    //get the current unix time 
   $(function(){
    function getCurrentTime(){
   // Get the current time as Unix time
            var currentUnixTime = Math.round((new Date()).getTime() / 1000);
            return timeConverter(currentUnixTime);

   function timeConverter(UNIX_timestamp){
              var a = new Date(UNIX_timestamp * 1000);
              var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
              var year = a.getFullYear();
              var month = months[a.getMonth()];
              var date = ("0" + a.getDate()).slice(-2);
              var hour = a.getHours();
              var min = a.getMinutes();
              var sec = a.getSeconds();
              var time = date + '-' + month + '-' + year + ' ' + hour + ':' + min + ':' + sec ;
              return time;
            }

  }
  var res=getCurrentTime();
  $('#time').val(res);});
</script>
<script >
 var message="<?php echo trans('popup.message')?>";
 var agree= "<?php echo trans('popup.agree')?>";
 var privacy="<?php echo trans('popup.privacy_policy')?>";
 var privay_link ="<?php echo route('privacy')?>";
 message = message+'<a href='+ privay_link +' target="_blank" style="text-decoration:underline;margin-left:0px;">'+ privacy + '</a>';
 var disable_cookie ="<?php echo trans('popup.disable_cookie')?>"
 console.log(message);
 </script>
 <script type="text/javascript" src="<?php echo url('js/jquery.cookiebar.js')?>"></script>
 <script src="<?php echo url('js/bootstrap.min.js')?>"></script>
    <script src="<?php echo url('js/bootstrap.js')?>"></script>
     <script>
    $("#menu_navigation").click(function(){
    $(".drop_down_menu").slideToggle();
    });
    </script>
    <script>
    $(".mobile_tabs").click(function(){
    $(".drop_down_menu").slideUp("slow");
    });
    </script>
    <script>
    $(".mobile_click").click(function(){
    $(".drop_down_menu").slideUp("slow");
    });

    $.cookieBar({message,agree,privacy});

    </script> 
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90176824-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Hotjar Tracking Code for www.stafftimes.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:386161,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
  
  
</html>
  

