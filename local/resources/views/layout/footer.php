	<!-- Bottom Scripts -->
	 <!-- for fetching time from frontend-->
<script type="text/javascript">
    //get the current unix time 
   $(function(){
    function getCurrentTime(){
   // Get the current time as Unix time
            var currentUnixTime = Math.round((new Date()).getTime() / 1000);
            return timeConverter(currentUnixTime);

   function timeConverter(UNIX_timestamp){
              var a = new Date(UNIX_timestamp * 1000);
              var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
              var year = a.getFullYear();
              var month = months[a.getMonth()];
              var date = ("0" + a.getDate()).slice(-2);
              var hour = a.getHours();
              var min = a.getMinutes();
              var sec = a.getSeconds();
              var time = date + '-' + month + '-' + year + ' ' + hour + ':' + min + ':' + sec ;
              return time;
            }

  }
  var res=getCurrentTime();
  $('#time').val(res);
   $('.time').val(res);
});
</script>
	<script src="<?php echo url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo url('assets/js/TweenMax.min.js')?>"></script>
	<script src="<?php echo url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo url('assets/js/xenon-toggles.js')?>"></script>

	<script src="<?php echo url('assets/js/xenon-widgets.js')?>"></script>
	<script src="<?php echo url('assets/js/devexpress-web-14.1/js/globalize.min.js')?>"></script>
	<script src="<?php echo url('assets/js/devexpress-web-14.1/js/dx.chartjs.js')?>"></script>
	<script src="<?php echo url('assets/js/toastr/toastr.min.js')?>"></script>
  <script src="<?php echo url('assets/js/sweet-alert.js')?>"></script>
	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>

  <script type="text/javascript">
  // set the desired language
moment.locale(userLang);

// use one of the localized format strings
var s = moment().format('llll');
/*console.log(s);*/
 document.getElementById("time_now1").innerHTML = s; 
</script>
<script>
  window.intercomSettings = {
    app_id: "yigrmccn",
    name: '<?php echo $name ?>', // Full name
    email: '<?php echo $email ?>', // Email address
    country_name: '<?php echo $country_name ?>',
    language_name: '<?php echo $language_name ?>',
    company_unique_id: '<?php echo $company_unique_id ?>',
    build: 'staging',
    type:'Company',
    created_at: 1312182000 // Signup date as a Unix timestamp
  };
</script>
<script>
(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/j6wr3wx8';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90176824-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Hotjar Tracking Code for www.stafftimes.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:386161,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>