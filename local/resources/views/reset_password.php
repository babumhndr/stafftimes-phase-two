<html>
<head>
        <title>Staff Times</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo url('/assets/css/css.css')?>">
        <link rel="stylesheet" href="<?php echo url('css/font-awesome.min.css')?>">
        <link rel="stylesheet" href="<?php echo url('css/bootstrap.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo url('css/login.css')?>">
        <script src="<?php echo url('assets/js/jquery-1.11.1.min.js')?>"></script>
        <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-core.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-forms.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-components.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-skins.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/sweet-alert.css')?>">
   </head>
   <style>
   	.mot_login_form {
   		margin-left: auto;
   		margin-right: auto;
   	}
   </style>
<body>
<script src="<?php echo url('js/jquery.min.js')?>"></script>

    	<div class="row mot_login">
    		<div id="container" class="container">
    			<div class="mot_login_form">
    				<form method="POST" action="updatePassword" id="passwordReset">
    				<div class="col-md-12">
    				<p class="mot_login_heading"><?php echo trans ('login.forgot_password')?></p>
    				<div class="right-inner-addon ">
    					<input type="text" class="form-control" id="field-1" name="email" readonly value="<?php echo $data;?>">
    				</div>
    				<div class="right-inner-addon ">
    					<input type="password" class="form-control" id="field-2" name="password" placeholder="<?php echo trans ('login.placeholder_new_password')?>" autofocus="autofocus">
    				</div>
    				<div class="right-inner-addon ">
    					<input type="password" class="form-control" id="field-3" name="cpassword" placeholder="<?php echo trans ('login.placeholder_confirm_password')?>" >
    				</div>
  					<div class="form-group" style="text-align:center;">
                                <button type="submit" class="form-control" id="mail_send"><?php echo trans ('login.confirm_button')?></button>
                                <img id="loader" src="<?php echo url('image/hourglass.gif')?>" style="display:none; width:40px; height: 40px; text-align: center"/>
                       </div> 
					</form>
				</div>
    		</div>
    	</div>
                    <script src="<?php echo url('js/jquery.form.js')?>"></script>
                    <script>
 (function() {
$('#passwordReset').ajaxForm({
    beforeSend: function () {
        var password = $('#field-2').val();
        var cpassword = $('#field-3').val();
        var pass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    console.log(pass.test(password));
          var isValid = true;
             if(password == null ||cpassword == '')
              {
                     swal({  
                         title: "<?php echo trans('popup.error_');?>", 
                         text: "<?php echo trans('popup.all_fields');?>",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });
                     isValid= false;
              }
              else if(pass.test(password) == false)
              {
                isValid = false;
                swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.password_minimum');?>", "error");
              }
              else if(password != cpassword)
              {
        		isValid = false;
         		swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.not_matching');?>", "error");
    		  }
            if(isValid == false) {
                return false;
            }
            else{
        $('#mail_send').hide();
        $('#loader').show();
    }

    
 },
    success: function(data) {
        /*console.log(data);*/
 if(data.status=='success')
 {
  swal({  
                         title: "<?php echo trans('popup.success');?>", 
                         text: data.response,   
                         type: "success",   
                         confirmButtonText : "Login"
                         },
                     		function(){
                     	window.location.href = "<?php echo url('login')?>";
                     	$('#mail_send').show();
        $('#loader').hide();
                        });

 }
 else if(data.status=='failure'){
swal({  
                         title: "Failure", 
                         text: data.response,   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });

 }
}
}); 

})();
</script>

<script src="<?php echo url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo url('assets/js/TweenMax.min.js')?>"></script>
	<script src="<?php echo url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo url('assets/js/xenon-toggles.js')?>"></script>

	<script src="<?php echo url('assets/js/xenon-widgets.js')?>"></script>
	<script src="<?php echo url('assets/js/devexpress-web-14.1/js/globalize.min.js')?>"></script>
	<script src="<?php echo url('assets/js/devexpress-web-14.1/js/dx.chartjs.js')?>"></script>
	<script src="<?php echo url('assets/js/toastr/toastr.min.js')?>"></script>
  <script src="<?php echo url('assets/js/sweet-alert.js')?>"></script>
</body>
</html>