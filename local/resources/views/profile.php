<?php  include 'layout/header.php';?>
<?php
$timesheet=$data['timesheet'];
$finalTotalHours=0;
$finalBalanceHours=0;
/*for ($i=0; $i < sizeof($timesheet) ; $i++) 
{ 
	$offset=$timesheet[$i][0]['offset_min'];
	$totalHours=0;
	$balanceHours=0;
	for ($j=0; $j <sizeof($timesheet[$i]) ; $j++) { 
		$totalHours=$totalHours+$timesheet[$i][$j]['total_hours_min'];
	}
	$balanceHours=$totalHours-$offset;
	$finalTotalHours=$finalTotalHours+$totalHours;
	$finalBalanceHours=$finalBalanceHours+$balanceHours;
}*/
$totalHours=0;
$balanceHours=0;
if (sizeof($timesheet)>0) {
	for ($i=0; $i < sizeof($timesheet) ; $i++) 
		{ 
			$finalTotalHours=$finalTotalHours+$timesheet[$i][0]['TSHrsTotal'];
			$finalBalanceHours=$finalBalanceHours+$timesheet[$i][0]['TSDailyBalnce'];
		}
}
/*echo $finalTotalHours.'fffgfd'.$finalBalanceHours;*/
function MinToHours($Minutes)
	{
    	/*return  date('G.i', mktime(0,$time));*/
    $s='';
	    if ($Minutes < 0)
	    {
	        $Min = Abs($Minutes);
            $s='-';
	    }
	    else
	    {
	        $Min = $Minutes;
	    }
	    $iHours = Floor($Min / 60);
	    $Minutes = ($Min - ($iHours * 60)) / 100;
	    $tHours = $iHours + $Minutes;
	    if ($Minutes < 0)
	    {
	        $tHours = $tHours * (-1);
	    }
	    $aHours = explode(".", $tHours);
	    $iHours = $aHours[0];
	    if (empty($aHours[1]))
	    {
	        $aHours[1] = "00";
	    }
	    $Minutes = $aHours[1];
	    if (strlen($Minutes) < 2)
	    {
	        $Minutes = $Minutes ."0";
	    }
	    $tHours = $s.$iHours .":". $Minutes;
	    return $tHours;
	}
?>
    <link rel="stylesheet" href="<?php echo url('assets/css/mot_profile.css')?>">
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row mot_profile_content">
			 <div class="col-md-12 link">
                    <p> 
                    <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('listing')?>" ><?php echo trans ('header.Listing')?></a></span> /
                   <a ><?php echo trans ('translate.employee')?></a>
                    </p>
                </div>
				<div class="col-md-3 col-xs-12" style="padding-right:5px;">
					<div class="fill">
						<div class="col-md-12 col-sm-12 col-xs-12 employee_picture">
							<div class="profile_picture" style="background: url('<?php echo $data['employee']['profile_image'];?>') no-repeat center #fff;">
							</div>
							<div class="profile_name">
							<p style=" margin-bottom: 0px; margin-top: 9px; color: #2392ec; "><?php echo $data['employee']['name'];?></p>
							<p style="font-size: 12px;">
							<?php 
							$department_data=$data['employee']['department'];
							if (!empty($department_data))
							{
								echo $department_data;
							}
							else{
								echo "--";
							}
							?>
							</p></div>
						</div>
						
					<div class="col-md-12 col-sm-12 col-xs-12 employee_whole">
					<?php 
						if ($data['employee']['status']=='active') {
							$baseUrl=url('reports_1/'.$data['employee']['_id']);
							/*echo $baseUrl;*/
							echo "<div class='listing_link_div'>
								  <a class='listing_link' href='".$baseUrl."'>".trans('listing.view_report')."</a>
						          <a class='listing_link' id='employeeArchive' href='#'>".trans('translate.archive')."</a>
						          </div>";
						}
						else if ($data['employee']['status']=='archive')
						{
							echo "<div class='listing_link_div'>
						          <a class='listing_link' id='employeeUnArchive' href='#'>".trans('translate.unarchive')."</a>
						          </div>";
						}
						else 
						{
							echo "";
						}
					?>
					</div>
					
					</div>
				</div>
				<div class="col-md-9 tabs" style="padding-left:5px;">
					<div class="fill">
					<div class="col-md-12 col-sm-12 col-xs-12 ">
						<div class="col-md-6 employee_left">
							<div class="xe-widget xe-counter">
								<div class="xe-icon icon_padding">
									<img src="<?php echo url('assets/images/image.png')?>" width="55">
								</div>
							<div class="xe-label">
								<strong class="num">
									<?php 
											$hours2=$finalBalanceHours;
											$hours2=MinToHours($hours2);
											if (!empty($hours2))
												{
													echo $hours2;
												}
											else{
													echo "0";
												}
										?>
								</strong>
								<span><?php echo trans('translate.total_number_of_overtime_hours')?></span>
							</div>
							</div>
						</div>
						<div class="col-md-6 employee_right">
							<div class="xe-widget xe-counter">
								<div class="xe-icon icon_padding">
									<img src="<?php echo url('assets/images/clock2 75x75png.png')?>" width="55">
								</div>
							<div class="xe-label">
								<strong class="num">
											<?php 
											$hours1=$finalTotalHours;
											$hours1=MinToHours($hours1);
											if (!empty($hours1))
												{
													echo $hours1;
												}
											else{
													echo "0";
												}
										?>
										</strong>
								<span><?php echo trans('translate.total_number_of_hours_worked')?></span>
							</div>
							</div>
						</div>
						<!-- <div class="col-md-3 employee_right">
							<div class="xe-widget xe-counter">
								<div class="xe-icon icon_padding">
									<img src="<?php echo url('assets/images/clock2 75x75png.png')?>" width="55">
								</div>
								<div class="xe-label">
								<strong class="num">470</strong>
								<span>Total number of Overtime hours</span>
								</div>
							</div>
						</div>
						<div class="col-md-3 employee_right">
							<div class="xe-widget xe-counter icon_padding">
								<div class="xe-icon">
									<img src="<?php echo url('assets/images/clock2 75x75png.png')?>" width="55">
								</div>
								<div class="xe-label">
								<strong class="num">470</strong>
								<span>Total number of Overtime hours</span>
								</div>
							</div>
						</div> -->
						</div>
						<!-- <div class="col-md-9 employee_left">
							<div class="xe-widget xe-status-update">
						<div class="xe-header">
							<p class="about_user">About <?php echo $data['employee']['name'];?></p>
						</div>
						<div class="xe-body">
							<p class="user_info">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</div>
						</div>
						</div> -->
						<!-- <div class="col-md-2 col-xs-12 employee_right">
							<div class="xe-widget xe-vertical-counter xe-vertical-counter-white">
						<div class="xe-icon">
							<img src="<?php echo url('assets/images/clock2 75x75png.png')?>">
						</div>
						
						<div class="xe-label">
							<strong class="num">00</strong>
							<span>Day balance</span>
						</div>
					</div>
						</div> -->
						<div class="col-md-8 col-sm-4 col-xs-12 employee_left">
						<div class="xe-widget xe-conversations basic_information">		
							<div class="xe-label basic_info grey">
								<p><?php echo trans('translate.basic_information')?></p>
							</div>
						<div class="xe-body basic_info_body">
							<ul class="list-unstyled">
						<li>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p><?php echo trans('employer.email')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$email_data=$data['employee']['email'];
											if (!empty($email_data))
												{
													echo $email_data;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list grey">
								<div class="basic_info_title">
									<p><?php echo trans('employer.phone')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$phone_data=$data['employee']['phone_number'];
											if (!empty($phone_data))
												{
													echo $phone_data;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p>Employee ID</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$employee_id=$data['employee']['employee_id'];
											if (!empty($employee_id))
												{
													echo $employee_id;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list end">
								<div class="basic_info_title">
									<p>Token</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$token=$data['employee']['token'];
											if (!empty($token))
												{
													echo $token;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list grey">
								<div class="basic_info_title">
									<p><?php echo trans('employer.country')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$address=$data['employee']['address'];
											if (!empty($address))
												{
													echo $address;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
					</ul>
				</div>
			</div>
						</div>
<!-- 						
						<div class="col-md-4 employee_center">
							<div class="xe-widget xe-conversations activity_body">
							<div class="activity grey">		
							<div class="xe-label activity_title">
								<p>Last Day Activities</p>
							</div>
							<p class="date" id="date_dive"></p>
							</div>
						<div class="xe-body">
							<div class="time_left">
								<p class="medium">Offset Hours</p>
								<p class="grey large">8</p>
								<p class="break">Break     1hr</p>
							</div>
							<div class="time_right">
								<p class="medium center">At Office</p>
								<p class="para grey"><span class="activity_left">Checked In</span><span class="activity_right">10 AM</span></p>
								<p class="para"><span class="activity_left">Checked Out</span><span class="activity_right">8 PM</span></p>
								<p class="para break grey"><span class="activity_left">Break</span><span class="activity_right">1 hour</span></p>
							</div>
						</div>
							</div>
						</div> -->
						<div class="col-md-4 col-sm-4 col-xs-12 employee_right">
							<div class="xe-widget xe-conversations profile_data">	
								<div class="xe-label basic_info grey">
									<p><?php echo trans('translate.weekly_overtime_overview')?></p>
								</div>
							<div class="xe-body profile_body">
								<ul class="list-unstyled">
									<li>
										<div class="mot_day_stats">
											<div class="day_info_title">
												<p class="day"><?php echo trans('translate.Monday')?></p>
											</div>
											<div class="day_info_value">
												<p class="hours" id="monday">--</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats grey">
											<div class="day_info_title">
												<p class="day"><?php echo trans('translate.Tuesday')?></p>
											</div>
											<div class="day_info_value">
												<p class="hours" id="tuesday">--</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats">
											<div class="day_info_title">
												<p class="day"><?php echo trans('translate.Wednesday')?></p>
											</div>
											<div class="day_info_value">
												<p class="hours" id="wednesday">--</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats grey">
											<div class="day_info_title">
												<p class="day"><?php echo trans('translate.Thursday')?></p>
											</div>
											<div class="day_info_value">
												<p class="hours" id="thursday">--</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats end">
											<div class="day_info_title">
												<p class="day"><?php echo trans('translate.Friday')?></p>
											</div>
											<div class="day_info_value">
												<p class="hours" id="friday">--</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
						<!-- <div class="col-md-5 employee_center">
							<div class="xe-widget xe-conversations address_information">		
								<div class="xe-label address_info grey">
									<p>Address</p>
								</div>
								<div class="xe-body address_info_body">
									<ul class="list-unstyled">
										<li>
										<div class="address_info_list">
											<div class="basic_info_title">
												<p>Flat no</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>#007</p>
												</a>
											</div>	
										</div>
										</li>
										<li>
										<div class="address_info_list grey">
											<div class="basic_info_title">
												<p>Street</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>Domlur</p>
												</a>
											</div>	
										</div>
										</li>
										<li>
										<div class="address_info_list">
											<div class="basic_info_title">
												<p>City</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>Bangalore</p>
												</a>
											</div>	
										</div>
										</li>
										<li>
										<div class="address_info_list end grey">
											<div class="basic_info_title">
												<p>State</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>Karnataka</p>
												</a>
											</div>	
										</div>
										</li>
									</ul>
								</div>
							</div>
						</div> -->

						<!-- <div class="col-md-2 col-xs-12 employee_right">
							<div class="xe-widget xe-vertical-counter xe-vertical-counter-white monthly_balance">
						<div class="xe-icon">
							<img src="<?php echo url('assets/images/clock2 75x75png.png')?>">
						</div>
						
						<div class="xe-label">
							<strong class="num">00</strong>
							<span class="center">Monthly balance</span>
						</div>
					</div>
						</div> -->
					</div>
				</div>
			</div>

			<div class="row mot_time_graph">
				<div class="col-sm-12">
						<div class="panel graph_heading">
<!-- 							<div class="mot_graph_heading">
								<div class="panel-heading graph_header">
									<p>Normal hours vs Overtime</p>
								</div>
								<div class="graph_attributes">
									<a href="#">
										<div class="aqua_blue"></div><span class="graph_normal_hours">Normal Hours</span>
									</a>
									<a href="#">
										<div class="purple"></div><span class="graph_overtime">Overtime</span>
									</a>
								</div>
							</div> -->
				<div class="panel-body">	
					<div id="container" style="height: 310px; width: 100%;"></div>
							<script src="https://code.highcharts.com/highcharts.js"></script>
							<script src="https://code.highcharts.com/modules/exporting.js"></script>
					</div>
					</div>
						
				</div>
			</div>
		</div>
		

	</div>

		<script>
		$(".left-links li a").click(function(){
    	$(this).find('i').toggleClass('fa-indent fa-outdent')});

    	$('#employeeArchive').click(function()
    	{

    		swal({   
    			title: "<?php echo trans('popup.you_sure');?>?",   
    			text: "<?php echo trans('popup.retrieve_employee');?>",   
    			type: "warning",   
    			showCancelButton: true,   
    			confirmButtonColor: "#DD6B55",   
    			confirmButtonText: "<?php echo trans('popup.archive_it');?>?",   
    			cancelButtonText: "<?php echo trans('popup.cancel_it');?>",   
    			closeOnConfirm: false,   
    			closeOnCancel: false }, 
    			function(isConfirm){   
    				if (isConfirm) 
    					{     
							$.ajax({
								method: 'POST',
								url: '<?= url('').'/employeeArchive' ?>',
								data:{"id":'<?= $data['employee']['id']?>'},
								success:function(response){ 
									console.log(response);
									if (response.status=='success') 
										{
										swal("<?php echo trans('popup.archived');?>", "<?php echo trans('popup.employee_archived');?>", "success"); 
										window.location.reload();  
										}  
									else    
										{
										swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.error_occurred');?>", "error"); 
										window.location.reload(); 
										}    	
								}
							});  
    					} 
    				else 
    					{     
    						swal("<?php echo trans('popup.cancelled');?>", "<?php echo trans('popup.employee_not_archived');?>", "error");   
    					} 
    			});
    	});

    $('#employeeUnArchive').click(function()
    	{

    		swal({   
    			title: "<?php echo trans('popup.you_sure');?>?",   
    			text: "<?php echo trans('popup.unarchive_msg');?>?",   
    			type: "warning",   
    			showCancelButton: true,   
    			confirmButtonColor: "#DD6B55",   
    			confirmButtonText: "<?php echo trans('popup.unarchive_msg1');?>?",   
    			cancelButtonText: "<?php echo trans('popup.cancel_it');?>",   
    			closeOnConfirm: false,   
    			closeOnCancel: false }, 
    			function(isConfirm){   
    				if (isConfirm) 
    					{     
							$.ajax({
								method: 'POST',
								url: '<?= url('').'/employeeUnArchive' ?>',
								data:{"id":'<?= $data['employee']['id']?>'},
								success:function(response){ 
									console.log(response);
									if (response.status=='success') 
										{
										swal("success", "<?php echo trans('popup.unarchive_msg2');?>?", "success"); 
										window.location.reload();  
										}  
									else    
										{
										swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.error_occurred');?>", "error"); 
										window.location.reload(); 
										}    	
								}
							});  
    					} 
    				else 
    					{     
    						swal("<?php echo trans('popup.cancelled');?>", "<?php echo trans('popup.employee_not_archived');?>", "error");   
    					} 
    			});
    	});
	</script>
	 <?php  include 'layout/footer.php';?>
	 	<script type="text/javascript">
	$( window ).load(function()
		{ 
		var date=moment().subtract(1, 'days').format("YYYY-MM-DD");
		var dateDisplay=moment().subtract(1, 'days').format("DD MMM YYYY");
		var toDate=moment().add(1, 'days').format("YYYY-MM-DD");
		var start_date=moment().startOf('isoweek').format("YYYY-MM-DD");
		var end_date=moment().startOf('isoweek').add(4, 'days').format("YYYY-MM-DD");
		var hoursTranslate="<?php echo trans("translate.hours")?>";
		$('#date_dive').html(dateDisplay);
		$.ajax({
				method: 'POST',
				url: '<?= url('').'/getPreviousDayTemplate' ?>',
				data:{"id":'<?= $data['employee']['id']?>',"date":date,"start_date":start_date,"end_date":end_date},
				success:function(response){
				console.log(response);
					if (response.status=='success') 
						{ 
							for (var i = 0; i < response['data']['week'].length; i++) 
							{
								if (response['data']['week'][i]['date']=="Mon") 
								{
									var weekyData=parseInt(response['data']['week'][i]['overtime']);
										weekyData=minToHrs(weekyData)+' '+hoursTranslate;
									$('#monday').html(weekyData);
									console.log(response['data']['week'][i]['overtime']);
								}
								else if (response['data']['week'][i]['date']=="Tue") 
								{	var weekyData=parseInt(response['data']['week'][i]['overtime']);
										weekyData=minToHrs(weekyData)+' '+hoursTranslate;
									$('#tuesday').html(weekyData);
									console.log(response['data']['week'][i]['overtime']);
								}
								else if (response['data']['week'][i]['date']=="Wed") 
								{
									var weekyData=parseInt(response['data']['week'][i]['overtime']);
										weekyData=minToHrs(weekyData)+' '+hoursTranslate;
									$('#wednesday').html(weekyData);
								}
								else if (response['data']['week'][i]['date']=="Thu") 
								{
									var weekyData=parseInt(response['data']['week'][i]['overtime']);
										weekyData=minToHrs(weekyData)+' '+hoursTranslate;
									$('#thursday').html(weekyData);
								}
								else if (response['data']['week'][i]['date']=="Fri") 
								{
									var weekyData=parseInt(response['data']['week'][i]['overtime']);
										weekyData=minToHrs(weekyData)+' '+hoursTranslate;
									$('#friday').html(weekyData);
								}
							}
						}     	
				}
			});
	});
	</script>
	<script>
	var titleTranslation="<?php echo trans('popup.dashboard_graph_title');?>";
	var timeTranslation="<?php echo trans('popup.dashboard_graph_time');?>";
	var overtimeTranslation="<?php echo trans('popup.dashboard_graph_overtime');?>";
	var avergeOffsetTranslation="<?php echo trans('popup.dashboard_graph_averge_offset');?>";
	$(function () {
		var categories = <?php echo json_encode($data["chart_data"]["date"]); ?>;
		var overtime = <?php echo json_encode($data["chart_data"]["total_overtime"]); ?>;
		var offset = <?php echo json_encode($data["chart_data"]["avg_offset"]); ?>;
    Highcharts.chart('container', {
        title: {
            text: titleTranslation,
            //x: -20 //center
        },
        subtitle: {
            text: '',
            //x: -20
        },
        xAxis: {
            categories:categories
        },
        yAxis: {
            title: {
                text: timeTranslation
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'hr'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        credits: {
      enabled: false
  		},
  		exporting: {
         enabled: false
		},
        series: [ {
            name: overtimeTranslation,
            data: overtime
        }, {
            name: avergeOffsetTranslation,
            data: offset
        }]
    });
});

	function minToHrs(minutes) 
	{
		var sign ='';
		if(minutes < 0){
		sign = '-';
		}

		var hours = Math.floor(Math.abs(minutes) / 60);
		var minutes = leftPad(Math.abs(minutes) % 60);

		return sign + hours +':'+minutes;
	}
	function leftPad(number) {  
         return ((number < 10 && number >= 0) ? '0' : '') + number;
    }

	</script>

