
<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/add_employee.css')?>">
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row invite">
				<div class="col-md-12 link">
					<p> 
					<span class="template_link">
					<a href="<?php echo url('dashboard')?>"><?php echo trans('add_employee.home')?></a></span>  /
                    <a ><?php echo trans('add_employee.Add_Employee')?></a>
					</p>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="panel">
                    <div class="invite_people_icon">
                        <img src="<?php echo url('assets/images/invite_main.png')?>"class="mail_box">
                    </div>
                    <div class="invite_people_heading">
                        <p><?php echo trans('dashboard.invite')?></p>
                    </div>
                    <div class="invite_people_sub_heading">
                        <p><?php echo trans('dashboard.invite_msg')?></p>
                    </div>
                    <div class="submit_invite">
                    <form id="invite" method="post" action="invitePeople">
                        <input type="text" class="invite_mail_id" id="name"name="name" placeholder="<?php echo trans('dashboard.invite_name')?>"></input>
						<input type="text" class="invite_mail_id" id="email"name="email" placeholder="<?php echo trans('dashboard.invite_mail')?>"></input>
                         <input type="hidden" id="time" name="time"></input>
                        <button class="send_invite" type="submit" id="send_invite_btn"><?php echo trans('dashboard.invite_btn')?></button>
                    </form>
                    </div>
                </div>
				</div>
		  </div>
				
				

</div>

		
			
		<!-- start: Chat Section -->
		<!-- <div id="chat" class="fixed">
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					Chat
					<span class="badge badge-success is-hidden">0</span>
				</h2>
				
				<script type="text/javascript">
				// Here is just a sample how to open chat conversation box
				jQuery(document).ready(function($)
				{
					var $chat_conversation = $(".chat-conversation");
					
					$(".chat-group a").on('click', function(ev)
					{
						ev.preventDefault();
						
						$chat_conversation.toggleClass('is-open');
						
						$(".chat-conversation textarea").trigger('autosize.resize').focus();
					});
					
					$(".conversation-close").on('click', function(ev)
					{
						ev.preventDefault();
						$chat_conversation.removeClass('is-open');
					});
				});
				</script>
				
				
				<div class="chat-group">
					<strong>Favorites</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Work</strong>
					
					<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Other</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
				</div>
			
			</div>
			
 -->			<!-- conversation template -->
			<!-- <div class="chat-conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					
					<span class="user-status is-online"></span>
					<span class="display-name">Arlind Nushi</span> 
					<small>Online</small>
				</div>
				
				<ul class="conversation-body">	
					<li>
						<span class="user">Arlind Nushi</span>
						<span class="time">09:00</span>
						<p>Are you here?</p>
					</li>
					<li class="odd">
						<span class="user">Brandon S. Young</span>
						<span class="time">09:25</span>
						<p>This message is pre-queued.</p>
					</li>
					<li>
						<span class="user">Brandon S. Young</span>
						<span class="time">09:26</span>
						<p>Whohoo!</p>
					</li>
					<li class="odd">
						<span class="user">Arlind Nushi</span>
						<span class="time">09:27</span>
						<p>Do you like it?</p>
					</li>
				</ul>
				
				<div class="chat-textarea">
					<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
				</div>
				
			</div>
			
		</div> -->
		<!-- end: Chat Section -->
		
		
	</div>

 <script src="js/jquery.form.js"></script>
 <script>
 	(function() {
		$('form').ajaxForm({
    		beforeSend: function () {
                     var name = $('#name').val();
                    var email = $('#email').val();
                    var trailStatus='<?php echo $data['trail_status'];?>';
                    var paymentStatus='<?php echo $data['payment_status'];?>';
                    var isValid = true;
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(name == null || email == null || name == '' || email == '')
                    {
                    	isValid = false;
                     swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.all_fields');?>", "error");
                   }
                    else if(regex.test(email) == false)
                    {
                    	isValid = false;
        				swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.check_email');?>", "error");
    				}
    				else if (paymentStatus=="pending") 
   					{
   						if (trailStatus=="expired")
   						 	{
   						 		isValid = false;
								swal({  
                     				title: "<?php echo trans('popup.error_');?>", 
                     				text: "<?php echo trans('popup.expired_trail');?>",   
                     				type: "error",   
                     				confirmButtonText : "<?php echo trans('popup.go_to_plans');?>"
                    				},
                    				function(){
                         				window.location.href = '<?php echo url('plansandprice'); ?>';

                   					 });
   						 	}
   					}
   				else if (paymentStatus=="expired")  
   					{
   						isValid = false;
   								swal({  
                     				title: "<?php echo trans('popup.error_');?>", 
                     				text: "<?php echo trans('popup.expired_subscription');?>",   
                     				type: "error",   
                     				confirmButtonText : "<?php echo trans('popup.go_to_plans');?>"
                    				},
                    				function(){
                         				window.location.href = '<?php echo url('plansandprice'); ?>';

                   					 });
   					}
    	        if(isValid == false)
    	        {
        			return false;
      			}
 				},
    		uploadProgress: function() {
      
    			},
    		success: function(data) {

    			 if(data.status=='success')
                   {
                   	swal("<?php echo trans('popup.great');?>", "<?php echo trans('popup.invite_employee');?>", "success");
                   	$("#invite")[0].reset();
                   }
                  else if(data.status=='failure'){	
                  	swal("<?php echo trans('popup.oops');?>", "<?php echo trans('popup.already_registered');?>", "error");
                  }	
    			},		
    		complete: function() {
    
   				 }
			}); 

		})();	
	</script>
	<script>
		$(".left-links li a").click(function(){
    	$(this).find('i').toggleClass('fa-indent fa-outdent');
});
	</script>
	<!-- Imported styles on this page -->

 <?php  include 'layout/footer.php';?>


