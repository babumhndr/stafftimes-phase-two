<?php  include 'layout/header.php';?>
<style type="text/css">
    #ui-datepicker-div
    {
        z-index:55 !important;
    }
      #fakeLoader{
    display: none;
    top: inherit !important;
    left: inherit !important;
    background-color: rgb(238, 238, 238)!important;
    }
    .spinner2
    {
        left: 40% !important;
        top: 40% !important;
        color: #000;
    } 
    .container1 > div, .container2 > div, .container3 > div {
    background-color: #000 !important;
    }
    @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
    .main-content {
        width: 100%;
    }
}
</style>
<?php 
$employeeName='';
if (!empty($data['employee'][0]['name']))
    {
        $employeeName=$data['employee'][0]['name'];
    }
    $employeeBaseUrl=url('employee_profile/'.$data['employee_details']['employee_id']);
 $employeeProfileLink='<a href="'.$employeeBaseUrl.'" style="color:#2392ec">'.$employeeName.'</a>';
?>
<?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    if (languageType=="de") 
    {
        printChart1="Grafik drucken";
        downloadPNG1="Als PNG Datei herunterladen";
        downloadJPEG1="Als JPG Datei herunterladen";
        downloadPDF1="Als PDF Dokument herunterladen";
        downloadSVG1="Als SVG Vector-Datei herunterladen";
    }
    else
    {
        printChart1="Print chart";
        downloadPNG1="Download PNG image";
        downloadJPEG1="Download JPEG image";
        downloadPDF1="Download PDF image";
        downloadSVG1="Download SVG image";
    }
</script>
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<link rel="stylesheet" href="<?php echo url('assets/css/activity_graph_report.css')?>">
<link href="<?php echo url('css/fakeLoader.css')?>" rel="stylesheet">
            <!-- <h3>Here starts everything&hellip;</h3> -->
           
            <div class="row reports">
                <div class="col-md-12 link">
                    <p> 
                    <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('listing')?>" ><?php echo trans ('header.Listing')?></a></span> /
                   <a><?php echo trans ('report_links.title')." - ".$employeeProfileLink?></a>
                    </p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="fill">
                    <?php include 'layout/report_links.php' ?> 
                    <div class="col-md-12 col-sm-12 col-xs-12 second">
                     <div class="fakeloader" id="fakeLoader" ></div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-9 col-sm-12 col-xs-12 statsrespad">
                        <div class="fill">
                        <form role="form" action="#">                       
                        <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <div class="fill">
                                <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.from')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="start_date" class="form-control ">         
                            </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.to')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="end_date" class="form-control">         
                            </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <div class="form-group">
                          <!--   <label class="radio-inline">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="radio-1" checked>
                                    Daily
                            </label> -->
                            <label class="radio-inline radio_range">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="weekly">
                                    <?php echo trans ('report_links.weekly')?>
                            </label>
                            <!-- <label class="radio-inline radio_range">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="monthly">
                                    <?php echo trans ('report_links.monthly')?>
                            </label>
                            <label class="radio-inline radio_range">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="yearly">
                                    <?php echo trans ('report_links.yearly')?>
                            </label> -->
                        </div>
                        </div>
                        </form>
                        </div>
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 fourth" style="background: #fff; margin-bottom: 15px;">
                   <div class="panel-body"> 
                           <!--  <script type="text/javascript">
                            jQuery(document).ready(function($)
                                {
                                    var dataSource=[{day:"baxbin",balance: 0.00},{day:"bdabin",balance: 2.30},{day:"badbin",balance: 4.00},{day:"badbin",balance: 2.00},{day:"babidn",balance: 1.00}];
                             $("#bar-1").dxChart({
                                        dataSource:dataSource,
                                     
                                        series: {
                                            argumentField: "day",
                                            valueField: "balance",
                                            name: "Balance",
                                            type: "bar",
                                            color: '#669999'
                                        }
                                    });  
                             });
                            </script> -->
                            <div id="bar-1" style="height: 450px; width: 100%;"></div>
                        </div>
                        
                </div>
                    </div>
                </div>
            </div>   
    </div>
    
    <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script> 
      <script type="text/javascript" src="<?php echo url('js/datepicker-de.js')?>"></script>
    <script src="<?php echo url('js/highcharts.js')?>"></script>
    <script src="<?php echo url('js/exporting.js')?>"></script>
   <!--  <script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'Overtime Graph', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Overtime Graph</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }

</script> -->
    <script>
        $(".left-links li a").click(function(){
        $(this).find('i').toggleClass('fa-indent fa-outdent');
});

//check weekly onload
$('#weekly').click();
/*
 //get todays date
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear(); 
        if(dd<10){
            dd='0'+dd
            } 
        if(mm<10){
            mm='0'+mm
            } 
        var onload_startdate = dd+'-'+mm+'-'+yyyy;
        var d = mm+'/'+dd+'/'+yyyy;
       var onload_enddate=getdate(d);
function getdate(d) {
    var tt = d

    var date = new Date(tt);
    var newdate = new Date(date);

    newdate.setDate(newdate.getDate() + 6);
    
    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    var someFormattedDate = dd+ '-' +mm+ '-' + y;
    return someFormattedDate;
}*/
 /* $('#start_date').html($('#start_date').val(today));//onload with date printed on range
   $('#end_date').html($('#end_date').val(today));//onload with date printed on range*/


        //date range ajax
var dateToday = '';
var employee_id="<?=$data['employee_details']['employee_id']?>";
var company_id="<?=$data['employee_details']['company_id']?>";
/*console.log(employee_id);
console.log(company_id);*/
var dates = $("#start_date, #end_date").datepicker({
    defaultDate: "+1w",
    firstDay: 1,
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    dateFormat:"dd-mm-yy",
    onSelect: function(selectedDate) {
        var option = this.id == "start_date" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            $.datepicker.regional[languageType];
        dates.not(this).datepicker("option", option, date);
        var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
        localStorage.start_date=start_date;
        localStorage.end_date=end_date;
       /* console.log(start_date+'//'+end_date+'//');*/
        if(start_date!=''&&end_date!='')
        {
            $('#fakeLoader').show();
        if($('#weekly').prop("checked") == true)
        {
        
            //dataGraph(company_id,employee_id,start_date,end_date);

             $.ajax({
                    url:"<?php echo url('listingTimesheetWithRangeForGraph/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    dataType: "json",
                    success: function(response)
                    {  
                     console.log(response);
                        var graph=[];
                       for (var i = 0; i < response.length; i++) 
                       {
                            var day_balance=Number(minToHrs(response[i][0]['range_balance']));
                            graph.push({"day":response[i]['1'],"balance":day_balance});
                        }
                            console.log(graph);
                            dataGraph(graph);
                    
                     }
                    });
        }
        //for month radio button 
            if($('#monthly').prop("checked") == true){
                var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetForGraphMonths/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    success: function(response)
                     {  
                     console.log(response);
                        var graph=[];
                       for (var i = 0; i < response.length; i++) 
                       {
                            var day_balance=Number(minToHrs(response[i][0]['range_balance']));
                            graph.push({"day":response[i]['1'],"balance":day_balance});
                        }
                            console.log(graph);
                            dataGraph(graph);
                    
                     }
                }); 
            }

              //for month radio button 
            if($('#yearly').prop("checked") == true){
                var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetForGraphYearly/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    success: function(response)
                     {  
                     console.log(response);
                        var graph=[];
                       for (var i = 0; i < response.length; i++) 
                       {
                            var day_balance=Number(minToHrs(response[i][0]['range_balance']));
                            graph.push({"day":response[i]['1'],"balance":day_balance});
                        }
                            console.log(graph);
                            dataGraph(graph);
                    
                     }
                }); 
            }

        }
    }
});
//on click radio button
$('#weekly').click(function(){
var start_date1=$('#start_date').val();
var end_date1=$('#end_date').val();
if(start_date1!=''&&end_date1!='')
        {
            $('#fakeLoader').show();
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
        if($('#weekly').prop("checked") == true){
            $.ajax({
                    url:"<?php echo url('listingTimesheetWithRangeForGraph/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                   success: function(response)
                     {  
                     console.log(response);
                        var graph=[];
                       for (var i = 0; i < response.length; i++) 
                       {
                            var day_balance=Number(minToHrs(response[i][0]['range_balance']));
                            graph.push({"day":response[i]['1'],"balance":day_balance});
                        }
                            console.log(graph);
                            dataGraph(graph);
                    
                     }
                });
                }

        }

});

$('#monthly').click(function(){
var start_date1=$('#start_date').val();
var end_date1=$('#end_date').val();
if(start_date1!=''&&end_date1!='')
        {
            $('#fakeLoader').show();
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
         //for month radio button 
            if($('#monthly').prop("checked") == true){
            $.ajax({
                    url:"<?php echo url('listingTimesheetForGraphMonths/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                    success: function(response)
                     {  
                     console.log(response);
                        var graph=[];
                       for (var i = 0; i < response.length; i++) 
                       {
                            var day_balance=Number(minToHrs(response[i][0]['range_balance']));
                            graph.push({"day":response[i]['1'],"balance":day_balance});
                        }
                            console.log(graph);
                            dataGraph(graph);
                    
                     }
                }); 
            }

        }

});
$('#yearly').click(function(){
var start_date1=$('#start_date').val();
var end_date1=$('#end_date').val();
if(start_date1!=''&&end_date1!='')
        {
            $('#fakeLoader').show();
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
        //for month radio button 
            if($('#yearly').prop("checked") == true){
            $.ajax({
                    url:"<?php echo url('listingTimesheetForGraphYearly/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                   success: function(response)
                     {  
                     console.log(response);
                        var graph=[];
                       for (var i = 0; i < response.length; i++) 
                       {
                            var day_balance=Number(minToHrs(response[i][0]['range_balance']));
                            graph.push({"day":response[i]['1'],"balance":day_balance});
                        }
                            console.log(graph);
                            dataGraph(graph);
                    
                     }
                }); 
            }
        }

});

//on load 
$( window ).load(function(){
var today = new Date();
var dayOfWeekStartingSundayZeroIndexBased = today.getDay(); // 0 : Sunday ,1 : Monday,2,3,4,5,6 : Saturday
var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()-6);
var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+7);
var start_date1 =moment(mondayOfWeek).format('D-M-YYYY');
var end_date1 =moment(sundayOfWeek).format('D-M-YYYY');
if (typeof(localStorage.start_date)=='undefined') { localStorage.start_date=start_date1}
if (typeof(localStorage.end_date)=='undefined') { localStorage.end_date=end_date1}
$('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
$('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));
start_date1 =localStorage.getItem("start_date");
end_date1 =localStorage.getItem("end_date");
if(start_date1!=''&&end_date1!='')
        {
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
            $.ajax({
                    url:"<?php echo url('listingTimesheetWithRangeForGraph/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                    success: function(response)
                     {  
                     console.log(response);
                        var graph=[];
                       for (var i = 0; i < response.length; i++) 
                       {
                            var day_balance=Number(minToHrs(response[i][0]['range_balance']));
                            graph.push({"day":response[i]['1'],"balance":day_balance});
                        }
                            console.log(graph);
                            dataGraph(graph);
                    
                     }
                });

        }
});

                                function dataGraph(dataSource){
                                  var start_date=$('#start_date').val();
                                  var end_date=$('#end_date').val();
                                  var message="<?php echo trans ('reports.date_range')?>: "+start_date+" <?php echo trans ('report_links.to')?> "+end_date;
                                  var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
                                  var employeeName="<?= $employeeName?>";
                                  var textFinal=employeeName+'<br>'+companyOf+'<br>'+message;
                                  var titleOf=toTitleCase("<?php echo trans ('report_links.four_line_one')?> <?php echo trans ('report_links.four_line_two')?>");
                                  var valuesTranslation="<?php echo trans ('translate.values')?>";
                                  var balanceTranslation="<?php echo trans ('reports-1.balance')?>";
                                  console.log('sd');
                                  var categories=[];
                                  var data=[];
                                  for (var i = 0; i < dataSource.length; i++) {
                                   categories.push(dataSource[i]['day']);
                                   data.push(dataSource[i]['balance']);
                                  }
                                  console.log(categories);
                                  console.log(data);
                                  Highcharts.chart('bar-1', {
                                  chart: {
                                  type: 'column',
                                  },
                                    lang: {
                                    printChart:printChart1,
                                    downloadPNG:downloadPNG1,
                                    downloadJPEG:downloadJPEG1,
                                    downloadPDF:downloadPDF1,
                                    downloadSVG:downloadSVG1
                                    },
                                  title: {
                                  align:"left",
                                  text:textFinal,
                                  style:{color:"#333333",fontSize:"12px"},
                                  },
                                  xAxis: {
                                  categories: categories
                                  },
                                  yAxis: {
                                  title: {
                                  text: valuesTranslation
                                  }
                                  },
                                  credits: {
                                  enabled: false,
                                  },
                                  exporting: {
                                  filename: titleOf,
                                  },
                                  series: [{
                                  name: balanceTranslation,
                                  data:data
                                  }]
                                  });
                                  $('#fakeLoader').hide();
                                } 

    </script>
    <script src="<?php echo url('js/fakeLoader.min.js')?>"></script>
    <script type="text/javascript">
    $(".fakeloader").fakeLoader({
    timeToHide:15000, //Time in milliseconds for fakeLoader disappear
    zIndex:"999",//Default zIndex
    spinner:"spinner2",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
    bgColor:"#2392ec", //Hex, RGB or RGBA colors
    });

    //change caps to title case
    function toTitleCase(str)
    {
     return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
    function minToHrs(minutes) {
        var sign ='';
        if(minutes < 0){
        sign = '-';
        }

        var hours = Math.floor(Math.abs(minutes) / 60);
        var minutes = leftPad(Math.abs(minutes) % 60);

        return sign + hours +'.'+minutes;

        }

        function leftPad(number) {  
        return ((number < 10 && number >= 0) ? '0' : '') + number;
        }
    </script>
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="<?php echo url('assets/js/datatables/dataTables.bootstrap.css')?>">
    <!-- Bottom Scripts -->
    <script src="<?php echo url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>


    <!-- Imported scripts on this page -->
    <script src="<?php echo url('assets/js/datatables/dataTables.bootstrap.js')?>"></script>
    <script src="<?php echo url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js')?>"></script>
    <script src="<?php echo url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
    <script src="<?php echo url('assets/js/timepicker/bootstrap-timepicker.min.js')?>"></script>

    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
    
 <?php  include 'layout/footer.php';?>

