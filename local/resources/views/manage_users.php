
<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/add_employee.css')?>">
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row invite">
				<div class="col-md-12 link">
					
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="panel">
                    <div class="row">
                    	<div class="col-md-3">
                    		<h3>Manage users</h3>
                    		<p>Manage employees of the company</p>
                    	</div>
                    	<div class="col-md-6">
                    		<br>
                    		<input type="text" class="invite_mail_id" placeholder="Search Employee by name, joining date etc"></input>
                    	</div>
                    	<div class="col-md-3">
                    		<br>
                    		<a href="add_staff"><button class="btn btn-info">Add an employee</button></a>
                    	</div>
                    </div>
                    <div class="row">
                    	<table class="table table-striped">
                    		<thead style="background: #edf7ff">
                    			<th>USER NAME</th>
                    			<th>TERM</th>
                    			<th>FROM DATE</th>
                    			<th>TO DATE</th>
                    			<th>AVERAGE % </th>
                    			<th>DAILY OFFSET</th>
                    			<th>VACATION</th>
                    			<th>HIRED QUOTA</th>
                    			<th></th>
                    		</thead>
                    		<tr>
                    			<td>Pruthvi Raj Karur</td>
                    			<td>Full time</td>
                    			<td>01-01-2018</td>
                    			<td>01-12-2018</td>
                    			<td>100</td>
                    			<td>10.00</td>
                    			<td>10</td>
                    			<td>50%</td>
                    			<td><button class="btn btn-info">Expire</button></td>
                    		</tr>
                    		<tr>
                    			<td>NItin Ganjigatti</td>
                    			<td>Full time</td>
                    			<td>01-01-2018</td>
                    			<td>01-12-2018</td>
                    			<td>100</td>
                    			<td>10.00</td>
                    			<td>10</td>
                    			<td>50%</td>
                    			<td><button class="btn btn-info">Expire</button></td>
                    		</tr>
                    		<tr>
                    			<td>NItin </td>
                    			<td>Full time</td>
                    			<td>01-01-2018</td>
                    			<td>01-12-2018</td>
                    			<td>100</td>
                    			<td>10.00</td>
                    			<td>10</td>
                    			<td>50%</td>
                    			<td><button class="btn btn-info">Expire</button></td>
                    		</tr>
                    		<tr>
                    			<td>Pruthvi Raj Karur</td>
                    			<td>Full time</td>
                    			<td>01-01-2018</td>
                    			<td>01-12-2018</td>
                    			<td>100</td>
                    			<td>10.00</td>
                    			<td>10</td>
                    			<td>50%</td>
                    			<td><button class="btn btn-info">Expire</button></td>
                    		</tr>
                    		<tr>
                    			<td> Ganjigatti</td>
                    			<td>Full time</td>
                    			<td>01-01-2018</td>
                    			<td>01-12-2018</td>
                    			<td>100</td>
                    			<td>10.00</td>
                    			<td>10</td>
                    			<td>50%</td>
                    			<td><button class="btn btn-info">Expire</button></td>
                    		</tr>
                    		<tr>
                    			<td>NItin Ganjigatti</td>
                    			<td>Full time</td>
                    			<td>01-01-2018</td>
                    			<td>01-12-2018</td>
                    			<td>100</td>
                    			<td>10.00</td>
                    			<td>10</td>
                    			<td>50%</td>
                    			<td><button class="btn btn-info">Expire</button></td>
                    		</tr>
                    		
                    	</table>
                    </div>
                </div>
				</div>
		  </div>
				
				

</div>

	
		
		
	</div>

 <script src="js/jquery.form.js"></script>


 <?php  include 'layout/footer.php';?>


