<?php  include 'admin_header.php';?>
<link rel="stylesheet" href="<?php echo url('css/settings.css')?>">
<link rel="stylesheet" href="<?php echo url('css/timedropper.css')?>">
<link href="<?php echo url('css/jquery.timeentry.css')?>" rel="stylesheet">
<script src="<?php echo url('js/jquery.spinner.js')?>"></script> 
  <?php 
 $general_setting=$details['general_setting'];
 $set_annual_allowance=$details['set_annual_allowance'];
?>
 <script>
  var offset='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['set_working_hours'];} ?>';
 /* alert(offset);*/
 </script>  
      <div class="row mot_template">
        <div class="col-md-12 link">
          <p> 
          <a href="dashboard"><?php echo trans ('header.dashboard')?></a>&nbsp
          <span class="template_link">>
          <a href="settings">&nbsp<?php echo trans('settings.settings'); ?></a></span> 
          </p>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 content">
          <ul class="nav nav-tabs">
            <li id="general_settings_id" class="top_list">
              <a href="#general_settings" class="top_anchor" data-toggle="tab">
                <span class="visible-xs"><i class="fa-home"></i></span>
                <span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('settings.tooltip_general_settings'); ?></p>"><?php echo trans('settings.general_settings'); ?></span></span>
              </a>
            </li>
            <li id="set_annual_id" class="top_list">
              <a href="#set_annual" class="top_anchor" data-toggle="tab">
                <span class="visible-xs"><i class="fa-envelope-o"></i></span>
                <span class="hidden-xs"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('settings.tooltip_set_annual'); ?></p>"><?php echo trans('settings.annual_allowance'); ?></span></span>
              </a>
            </li>
          </ul>

<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////--> 
          <div class="tab-content custom" >
            <div class="tab-pane " id="general_settings" style="padding-left: 70px;">
            <div class="col-sm-12 no_padding column">
                <form id="general_setting" action="<?php
                  if($general_setting['status']=='success')
                  {
                    echo "updateGeneralSettingInAdmin";
                  }
                  else
                  {
                     echo "setGeneralSettingInAdmin";
                  }
                  ?>" method="post">
                            <div class="col-md-12 div_space">
                              <label class="col-md-4 col-sm-6 control-label label_padding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_working_one'); ?></li> <li><?php echo trans('settings.tooltip_working_two'); ?></li> <li><?php echo trans('settings.tooltip_working_three'); ?></li> </ul>"><?php echo trans('settings.working_hours'); ?></span></label>
                            <div class="col-md-8 col-sm-6 no_padding form-group">
                            <label class="radio-inline">
                               <span><?php echo trans('settings.formula'); ?>: 1 <?php echo trans('settings.day'); ?> =  </span> <input type="text" name="set_working_hours" id="set_working_hours" value="<?php if (!empty($general_setting['response'])) { echo $general_setting['response']['0']['set_working_hours']; } else { echo "00:00"; } ?>"><span>  <?php echo trans('settings.hours'); ?></span><br>
                                <label style=" margin-top: 4px; ">
                                <input type="checkbox" class="cbr cbr-info" name="update_annual_allowance" id="update_annual_allowance" value="true">
                                <?php echo trans('settings.update'); ?> ?
                                </label>
                            </label>
                            </div>
                            </div>
                         <div class="col-md-12 t_align buttons">
                          <button class="btn btn-info btn_class general_setting_btn"><!-- <?php
                  if($general_setting['status']=='success')
                  {
                    echo "EDIT";
                  }
                  else
                  {
                     echo "SAVE";
                  }
                  ?> --><?php echo trans('settings.save_button'); ?></button>
                         </div>
                         </form>
            </div>
            </div>
<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
            <div class="tab-pane" id="set_annual">
              <form id="set_annual_allowance_form" action="<?php
                  if($set_annual_allowance['status']=='success')
                  {
                    echo "updateSetAnnualAllowanceInAdmin";
                  }
                  else
                  {
                     echo "setSetAnnualAllowanceInAdmin";
                  }
                  ?>" method="post">
                <div class="col-md-12 no_padding column">
                  <div class="col-md-3 no_padding ">
                  <label class="col-md-12 col-sm-12 control-label label_padding"><?php echo trans('settings.annual_allowance'); ?></label>
                  </div>
                  <div class="col-md-9 no_padding ">
                  <div class="col-md-12 no_padding space">
                  <label class="col-md-1 col-sm-1 control-label col_3 "><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_activityannual_one'); ?></li> <li><?php echo trans('settings.tooltip_activityannual_two'); ?></li> <li><?php echo trans('settings.tooltip_activityannual_three'); ?></li> </ul>"><?php echo trans('settings.activity'); ?></span></label>
                  <div class="col-md-11 col-sm-11">
                    <div class="arrow_background3">
                     <?php
                            for($i=0;$i<count($details['activity_list']);$i++)
                            {
                              if (!empty($details['activity_list']))
                              {
                                echo '<input type="hidden" id="day'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['allowance_bank_days'].'"><input type="hidden" id="hour'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['allowance_bank_hours'].'">';
                              }
                              else
                              {
                              echo '';
                              }
                            }
                      ?>
                    <!-- <select name="set_annual_allowance_activity">
                      <option value="Public Holiday" selected>Public Holiday</option>
                      <option value="Unpaid Leave">Unpaid Leave</option>
                    </select> -->
                      <select name="set_annual_allowance_activity" class="setting_selection1" id="setting_selection">
                            <?php
                            for($i=0;$i<count($details['activity_list']);$i++)
                            {
                              if (!empty($details['activity_list']))
                              {
                                if($i==0)
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['_id'].'" '.$res.'>'.$details['activity_list'][$i]['activity_name'].'</option>';
                              }
                              else
                              {
                              echo '';
                              }
                            }
                          ?>
                      </select>
                    </div>
                  </div>
                  </div>
                  <div class="col-md-12 no_padding space">
                  <label class="col-md-1 col-sm-1 control-label col_3 "><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_totalin_one'); ?></li> <li><?php echo trans('settings.tooltip_totalin_two'); ?></li> <li><?php echo trans('settings.tooltip_totalin_three'); ?></li> </ul>"><?php echo trans('settings.total_in'); ?></span></label>
                  <div class="col-md-2 col-sm-2">
                    <div class="arrow_background4">
                    <select name="total_in_type" id="total_in_type">
                      <option value="Hours"  <?php if (!empty($set_annual_allowance['response']))
                       {
                        if ($set_annual_allowance['response']['0']['total_in_type']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }
                        ?>>Hours</option>
                      <option value="Days"<?php if (!empty($set_annual_allowance['response']))
                       {
                        if ($set_annual_allowance['response']['0']['total_in_type']=="Days"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>>Days</option>
                    </select>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2">
                    <div class="input-group"> 
                    <input name='total_in_value' id='total_in_value' class='form-control t_align input_radius decimal onchange_val' value='00' />
                    <input type='text' id='total_in_value1' name='total_in_value1' class='form-control inputdiv hours_val onchange_val' value='0:00'>
                  </div>
                    </div>
                  </div>
                  <div class="col-md-12 no_padding space">
                  <!-- <label class="col-md-1 col-sm-1 control-label col_3 "></label> -->
                  <label class="col-md-2 col-sm-2 control-label col_3 "><span style="float: left;" id="type_display"></span></label>
                  <label class="col-md-1 col-sm-1 control-label equalpostion">=</label>
                  <div class="col-md-2 col-sm-2">
                            <div class="input-group">
                      <input type="text" name="total_in_calculated_value" id="total_in_calculated_value" class="form-control t_align input_radius onchange_result" value="" readonly/>
                    </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                      <span class="btn btn-info" id="go"><?php echo trans('settings.go'); ?></span>
                    </div>
                  </div>
                  </div>
                </div>
                <div class="col-md-12 t_align buttons">
                  <button class="btn btn-info btn_class set_annual_allowance_btn"><!-- <?php
                  if($set_annual_allowance['status']=='success')
                  {
                    echo "EDIT";
                  }
                  else
                  {
                     echo "SAVE";
                  }
                  ?> --><?php echo trans('settings.save_button'); ?></button>
                 <!--  <button class="btn btn-white btn_class" id="cancel_btn">CANCEL</button>     -->            
              </div>
              </form>
            </div>
          </div>
          </div>

<script src="<?php echo url('js/timedropper.js')?>"></script>   
<script src="<?php echo url('js/jquery.form.js')?>"></script>
<script src="<?php echo url('js/jquery.plugin.min.js')?>"></script>
<script src="<?php echo url('js/jquery.timeentry.js')?>"></script>
<script src="<?php echo url('js/admin_settings.js')?>"></script>
<link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">
<script src="assets/js/timepicker/bootstrap-timepicker.min.js"></script>
<script src="assets/js/daterangepicker/daterangepicker.js"></script>
<script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
<?php include 'admin_footer.php';?>
<script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.culture.de-DE.js')?>"></script>
<script src="<?php echo url('js/jquery.mousewheel.js')?>"></script>
