<?php  include 'admin_header.php';?>
<?php include 'cms_links.php' ?>
<div class="row" style="margin:0px">
 <div class="col-md-12">
  <select id="language" style=" float: right; ">
        <option value="en">ENGLISH</option>
        <option value="ge">DEUTSCH</option>
    </select>
 </div>
 <div class="col-md-12">
  <div class="col-sm-6">
   <form >
    <label >Email Field Placeholder</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Password Field Placeholder</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Sign in Button</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='signin'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Can't access your account</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cant_access'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Reset Password</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='reset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Send Button</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='send'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

    <hr>
    <label >Forgot Password title</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='forgot_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Placeholder to enter the New Password</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_new_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Placeholder to enter the Confirm password</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_confirm_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Confirm Password button</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='confirm_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />



   </form>
  </div>
  <div class="col-sm-6" id="englishdiv">
   <form id="cms_form" enctype="multipart/form-data">
    <label >Email Field Placeholder</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Password Field Placeholder</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Sign in Button</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='signin'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='signin'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Can't access your account</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cant_access'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cant_access'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Reset Password</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='reset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='reset'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Send Button</label><br />
    <textarea rows="2" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='send'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='send'){ echo $details['Translation'][$i]['_id'];}}?>">
    <hr>
    <label >Forgot Password title</label><br />
    <textarea rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='forgot_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='forgot_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Placeholder to enter the New Password</label><br />
    <textarea rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_new_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_new_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Placeholder to enter the Confirm password</label><br />
    <textarea rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_confirm_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_confirm_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Confirm Password button</label><br />
    <textarea rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='confirm_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='confirm_button'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang" name="languag">
    <input id="submit_button" type="button" value="Update" />
   </form>
  </div>
  <div class="col-sm-6" id="germandiv" style="display:none;">
   <form id="cms_form1" enctype="multipart/form-data">
    <label >Email Field Placeholder</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Password Field Placeholder</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Sign in Button</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='signin'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='signin'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Can't access your account</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cant_access'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cant_access'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Reset Password</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='reset'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='reset'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Send Button</label><br />
    <textarea rows="2" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='send'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='send'){ echo $details['Translation'][$i]['_id'];}}?>">
    <hr>
    <label >Forgot Password title</label><br />
    <textarea rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='forgot_password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='forgot_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Placeholder to enter the New Password</label><br />
    <textarea rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_new_password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_new_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Placeholder to enter the Confirm password</label><br />
    <textarea rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_confirm_password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_confirm_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Confirm Password button</label><br />
    <textarea rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='confirm_button'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='confirm_button'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang1" name="languag">
    <input id="submit_button1" type="button" value="Update" />
   </form>
  </div>
 </div>
</div>
<script src="js/jquery.form.js"></script>
<script>
                              
$('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendLogin',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminLoginCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendLogin',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminLoginCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>

var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$("#language").change(function () {
   if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
</script>
<?php  include 'admin_footer.php';?>