<?php  include 'admin_header.php';?>
<style>
 .panel .panel-body {
    color: #575757;
}
input[type="search"] {
    border: 1px solid #eee;
    padding: 5px;
}
.table.table-bordered > tbody > tr > td{
	font-size: 14px;
}
#myTable th{
	font-size: 14px;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
	background: #fff;
	border-color:#3597D3; 
	color: #3597D3 !important; 
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{
	background: #3597D3;
	color: #fff!important; 
	border-color:#3597D3; 
}
.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
	background: #3597D3;
	color: #fff!important; 
	border-color:#3597D3; 
}
</style>
 <script>
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
<div class="col-sm-12" style=" padding-top: 15px; padding-bottom: 15px; ">
		<!-- Basic Setup -->
			<div class="panel panel-default" style=" font-family: sans-serif;width: 100%;">
				<div class="panel-heading">
					<h3 class="panel-title">Employee List</h3>
<!-- 					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a href="#" data-toggle="remove">
							&times;
						</a>
					</div> -->
				</div>
				<div class="panel-body" style="padding-top: 25px;">
					
					<table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%" style="border-bottom-color: #eee;">
						<thead>
							<tr>
								<th>Employee name</th>
								<th>Email address</th>
								<th>Status</th>
								<th>Payment Status</th>
								<th>Subscription Start Date</th>
								<th>Subscription End Date</th>
							</tr>
						</thead>
					
						<tbody>
						<?php
							for($i=0;$i<sizeof($data);$i++) 
								{	
									$subscriptionStart='--';
									$subscriptionEnd='--';
									if($data[$i]['status'] == 'pending')
										{
											$status ='Pending';
										}
									else if($data[$i]['status'] == 'active')
										{
											$status ='Active';
										}
									else{
											$status ='Archive';
										}

									if($data[$i]['payment_status'] == 'pending')
										{
											$paymentStatus ='Pending';
										}
									else if($data[$i]['payment_status'] == 'active')
										{
											$paymentStatus ='Active';
											$subscriptionStart=date_create($data[$i]['subscription_start_date']);
											$subscriptionStart=date_format($subscriptionStart,"d-m-Y");
											$subscriptionEnd=date_create($data[$i]['subscription_end_date']);
											$subscriptionEnd=date_format($subscriptionEnd,"d-m-Y");
										}

									echo '<tr>
								          <td><a href="companyEmployeeProfile/'.$data[$i]['_id'].'"style=" color: #03A9F4; ">'.$data[$i]['name'].'</a></td>
										  <td>'.$data[$i]['email'].'</td>
										  <td>'.$status.'</td>
										  <td>'.$paymentStatus.'</td>
										  <td>'.$subscriptionStart.'</td>
										  <td>'.$subscriptionEnd.'</td>
							         	  </tr>';
								}
						?>
							<!-- <tr>
								<td>Tiger Nixon</td>
								<td>System Architect</td>
								<td>Edinburgh</td>
							</tr> -->
						</tbody>
					</table>
					
				</div>
			</div>	
</div>
<?php  include 'admin_footer.php';?>