<?php  include 'admin_header.php';?>
<link rel="stylesheet" type="text/css" href="assets/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="assets/slick/slick-theme.css"/>	
<link rel="stylesheet" href="<?php echo url('assets/css/manage_template.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/timedropper.css')?>">
<script src="<?php echo url('js/replace.js')?>"></script>
<link href="<?php echo url('css/jquery.timeentry.css')?>" rel="stylesheet">
<style type="text/css">
	.float_button
	{
		position: absolute;
		left: 89%;
		top: -4px;
	}
</style>
  <?php 
 $general_setting=$details['general_setting'];
 $separator='dot';
 $valueFormat='';
 $timeOfMot='';
 $separatorValue='';
 if (!empty($general_setting['response'])) {$valueFormat=$general_setting['response']['0']['value_format'];} 
 if (!empty($general_setting['response'])) {$timeOfMot=$general_setting['response']['0']['time_style'];} 
  if (!empty($separator['data'])) {$separatorValue=$separator['data'];}
   $valueFormat='8:15';
    $timeOfMot='24 hours';
function replaceTo($valueFormat,$timeOfMot, $separatorValue,$str)
{
 if($timeOfMot=='am/pm')
	{
		if($valueFormat=='8.15')
		{
			 return str_replace(':','.',$str);
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace(':',',',$str);
			}
			else
			{
				return str_replace(':','.',$str);
			}
		}
	}
else if($timeOfMot=='24 hours')
	{
		if($valueFormat=='8.15')
		{
			return str_replace(':','.',$str);
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace(':',',',$str);
			}
			else
			{
				return str_replace(':','.',$str);
			}
		}
	}
else if ($timeOfMot=='Industrial')
	{
		if($valueFormat=='8.15')
		{
			return str_replace(':','.',$str);
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace(':',',',$str);
			}
			else
			{
				return str_replace(':','.',$str);
			}
		}
	}
	else
	{
		return $str;
	}
}
function replaceToDot($valueFormat,$timeOfMot, $separatorValue,$str)
{
 if($timeOfMot=='am/pm')
	{
		if($valueFormat=='8.15')
		{
			 return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
else if($timeOfMot=='24 hours')
	{
		if($valueFormat=='8.15')
		{
			return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
else if ($timeOfMot=='Industrial')
	{
		if($valueFormat=='8.15')
		{
			return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
	else
	{
		return $str;
	}
}
 ?>   
 <script>
  var value_format='8.15';
  var separator='dot';
  var offset='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['set_working_hours'];} ?>';
  var time_of_mot='24 hours';
 /* alert(offset);*/
 </script>  
	<div class="row mot_template">
		<div class="col-md-12 link">
					<div style="float:left;">
					<p> 

					<a href="dashboard"><?php echo trans('manage_template.Dashboard')?></a>&nbsp
					<span class="template_link">>
					<a href="manage_template">&nbsp<?php echo trans('manage_template.Manage_Template')?></a></span> 
					</p>
				</div>
				
				
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
					<ul class="nav nav-tabs">
						<li class="top_list active" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_template.tooltip_tab'); ?></p> ">
							<a href="#home" class="top_anchor" data-toggle="tab" id="company_template">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"><?php echo trans('manage_template.company_template')?></span>
							</a>
						</li>
				<!-- 		<li class="top_list">
							<a href="#profile" class="top_anchor" data-toggle="tab" id="mot_template">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs"><?php echo trans('manage_template.overtime_template')?></span>
							</a>
						</li> -->
						<!-- <li class="top_list">
							<a href="#messages" class="top_anchor" data-toggle="tab">
								<span class="visible-xs"><i class="fa-envelope-o"></i></span>
								<span class="hidden-xs">Most Used Template</span>
							</a>
						</li> -->
					</ul>
						
							
					<div class="tab-content custom">
						<div class="tab-pane active" id="home">
							
							<div class="col-md-12 col-sm-12 col-xs-12 center">
								<div class="xe-widget xe-conversations temp_title">		
						<div class="xe-body basic_info_body">
							
		
	
			
			<!-- Basic Setup -->
			<div class="">
				
				<div class="panel-body">
					
					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-1").dataTable({
							aLengthMenu: [
								[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
							]
						});
					});
					</script>
					<?php
						if($details['mot_template_list']['status'] == 'success'){
							$tableDisplay = "display:block";
							$noItem = "display:none";
						}
						else{
							$tableDisplay = "display:none";
							$noItem = "display:block";
						}
					?>
					<div style="<?=$tableDisplay?>">
						<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%" >
						<thead>
							<tr>
								<th>Template</th>
								<th>Template In German</th>
								<th>Offset Time</th>
								<th>Activity</th>
								<th>Flat Hours</th>
								<th>Amount</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Break</th>
								<th>Deploy Status</th>
								
									
							</tr>
						</thead>
					<tbody>

						<?php
							for($i=0;$i<count($details['mot_template_list']['response']);$i++){
								$templateId = '"'.$details['mot_template_list']['response'][$i]['_id'].'"';
								$template_id=$details['mot_template_list']['response'][$i]['_id'];
								
										$div='';
										$div1='';
										$div2='';
										$div3='';
										$div4='';
										$div5='';
										$div6='';
							if ($details['mot_template_list']['response'][$i]['deploy_status']=="pending") 
								{
									$div6="<button class='deploy_btn' onclick ='changeStatusDeploy(".$templateId.");' >Deploy</button>
										<button class='delete_btn' onclick ='deleteTemplate(".$templateId.");' >Delete</button>";
								}
							else
								{
									$div6='<p style=" color: #2acd84; ">Deployed</p>';
								}
							for($j=0;$j<count($details['mot_template_list']['response'][$i]['activity']);$j++)
  								{
  								$activity_start  =$details['mot_template_list']['response'][$i]['activity'][$j]['activity_start'];
								$activity_end  = $details['mot_template_list']['response'][$i]['activity'][$j]['activity_end'];
								if($activity_start!=00 && $activity_end!=00)
								{
								if (!empty($details['general_setting']['response']['0']['time_style'])) 
										{
											if($details['general_setting']['response']['0']['time_style']=='am/pm')
											{
												$activity_start= time_in_12_hour_format($details['mot_template_list']['response'][$i]['activity'][$j]['activity_start']);
											}
											else
											{
												$activity_start= time_in_24_hour_format($details['mot_template_list']['response'][$i]['activity'][$j]['activity_start']);
											}
											if($details['general_setting']['response']['0']['time_style']=='am/pm')
											{
												$activity_end= time_in_12_hour_format($details['mot_template_list']['response'][$i]['activity'][$j]['activity_end']);
											}
											else
											{
												$activity_end= time_in_24_hour_format($details['mot_template_list']['response'][$i]['activity'][$j]['activity_end']);
											}
										}
										else
										{
											$activity_start= time_in_24_hour_format($details['mot_template_list']['response'][$i]['activity'][$j]['activity_start']);
											$activity_end= time_in_24_hour_format($details['mot_template_list']['response'][$i]['activity'][$j]['activity_end']);
										}
									}
									else
									{
										$activity_start='--';
										$activity_end='--';
									}
  									$res = $details['mot_template_list']['response'][$i]['activity'][$j]['activityDetails']['_id'];
  									$activity_flat_hours=$details['mot_template_list']['response'][$i]['activity'][$j]['activity_flat_hours'];
  									$activity_amount=$details['mot_template_list']['response'][$i]['activity'][$j]['activity_amount'];
  									$activity_break=$details['mot_template_list']['response'][$i]['activity'][$j]['activity_break'];
  									if($activity_flat_hours == 0 )
  									{
  										$activity_flat_hours='--';
  									}
  									if($activity_amount == 0 )
  									{
  										$activity_amount='--';
  									}
  									if($activity_break == 0 )
  									{
  										$activity_break='--';
  									}
  									$div .="<option value=".$res.">".$details['mot_template_list']['response'][$i]['activity'][$j]['activityDetails']['activity_name']."</option>";
  									$div1 .="<div style='display:none' class='activity_table ".$template_id.$j." ".'activity_table'.$j." ".'activity_table'.$template_id."'>".$activity_flat_hours."</div>";
  									$div2 .="<div style='display:none' class='activity_table ".$template_id.$j." ".'activity_table'.$j." ".'activity_table'.$template_id."'>".$activity_amount."</div>";
  									$div3 .="<div style='display:none' class='activity_table ".$template_id.$j." ".'activity_table'.$j." ".'activity_table'.$template_id."'>".$activity_start."</div>";
  									$div4 .="<div style='display:none' class='activity_table ".$template_id.$j." ".'activity_table'.$j." ".'activity_table'.$template_id."'>".$activity_end."</div>";
  									$div5 .="<div style='display:none' class='activity_table ".$template_id.$j." ".'activity_table'.$j." ".'activity_table'.$template_id."'>".$activity_break."</div>";
  								}
								echo "<tr>
								<td onclick = 'viewTemplate(".$templateId.");' style='color:#2392ec;cursor:pointer;'>".$details['mot_template_list']['response'][$i]['template_name']."</td>
								<td>".$details['mot_template_list']['response'][$i]['template_name_german']."</td>
								<td>".$details['mot_template_list']['response'][$i]['template_offset_time']."</td>";
								echo "
								<td><select class='form-control select_box'id=".$template_id." onChange='activityChange(this,".$templateId.");' >".$div."</select></td>
								<td>".$div1."</td>
								<td>".$div2."</td>
								<td>".$div3."</td>
								<td>".$div4."</td>
								<td>".$div5."</td>
								<td>".$div6."</td>";
								
								
							echo "</tr>";
							}
						?>
					
						
							
							
							
						</tbody>
					</table>
				</div>
					<div class="col-md-12 error_msg" style="<?=$noItem?>"><p><?php echo trans('manage_template.data'); ?></p></div>
					
				</div>
			</div>
			
			
	
				</div>
			</div>								
	
								
							</div>
							
						</div>

						<div class="float_button" id="float_button">
							<button onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"  type="submit" class="circular_button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							<p class="add_temp_text"><?php echo trans('manage_template.add')?></p>
							<p class="add_temp_text"><?php echo trans('manage_template.template')?></p></button>
						</div>
					</div>
				</div>
			</div>
			</div>
			</div>
			<div class="modal fade" id="modal-6">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_template.Add_Template')?></h5>
				</div>
			<div class="modal-content custom-content">
				<form method="post" action="addDefaultTemplate" id="form1">
				<div class="modal-body custom-body">
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-1" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="">Name of the template in English</label>
								<input type="text" id="template_name" name="template_name" class="form-control templateform" id="field-1" placeholder="Template name" value="" maxlength="25">
								<label for="field-1" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="" style=" margin-top: 6px; ">Name of the template in German</label>
								<input type="text" id="template_name_german" name="template_name_german" class="form-control templateform" id="field-1" placeholder="Template name" value="" maxlength="25">
							</div>		
							
						</div>
						
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-2" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_template.offset')?></label>
								
								<input type="text" id="template_offset_time" name="template_offset_time" class="form-control inputdiv" value="">
							</div>	
						
						</div>
					</div>
				
				<div class="optionBox">
 	
</div>
	<input type="hidden" name="number_of_activity" id="number_of_activity" value="1">
	 <input type="hidden" id="time" name="time"></input>
				<div class="modal-footer custom-footer">
				<div id="addbtn"> 
					<button type="submit" class="btn btn-info" id="save_btn"><?php echo trans('manage_template.save')?></button>
					</div>
					<div id="addloader" style="display:none">
						<img src="<?=url('image/mot_loader.gif')?>">
					</div>
				</div>
				 </div>
				</form>
				<div class="slider_btn">
				<button id="add_btn" class="js-add-slide add_btn"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="modal-61">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_template.Edit_Template')?></h5>
				</div>
			<div class="modal-content custom-content">
				<form method="post" action="updateDefaultTemplate" id="form2">
				<div class="modal-body custom-body">
				    <div class="row"> 
			<div class="col-md-12">
				<div class="form-group"> 
 					<label for="field-1" class="control-label">Name of the template in English</label> 
 					<input type="text" id="template_name1" name="template_name" class="form-control templateform " id="field-1" placeholder="Template name" value="" maxlength="25">
 					<label for="field-1" class="control-label" style=" margin-top: 6px; ">Name of the template in German</label> 
 					<input type="text" id="template_name_german1" name="template_name_german" class="form-control templateform " id="field-1" placeholder="Template name" value="" maxlength="25"> 
 				</div>
 			</div> 
		<div class="col-md-12"> 
 		<div class="form-group"> 
 			<label for="field-2" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_template.offset')?></label> 
 			<input type="text" id="template_offset_time1" name="template_offset_time" class="form-control inputdiv " id="field-2" value=""> 
 		</div> 
 	</div> 
 </div>
    <div class="optionBox3" id="optionBox3"> 

</div>
<!-- <span class="add1" style="color:green">Add </span> -->
<!-- <div style=" text-align: right;margin-bottom: 6px;"><i class="fa fa-plus-circle" aria-hidden="true"></i><span class="add1" style="color:green;    padding-left: 4px;margin-right: 5px;">Add </span></div> -->
<!--  <div class="optionBox2"> 
 <div class="block1">
</div>
 </div> -->
 <input type="hidden" name="number_of_activity" id="number_of_activity1">
 <input type="hidden" id="time1" class="time" name="time1" value=""></input>
 <input type="hidden" id="template_id" name="template_id" value=""></input> 
 <div class="modal-footer custom-footer"> 
 	<div id="addbtn1"> 
					<button type="submit" class="btn btn-info" id="save_btn"><?php echo trans('manage_template.save')?></button>
					</div>
					<div id="addloader1" style="display:none">
						<img src="<?=url('image/mot_loader.gif')?>">
					</div>
 </div>
		 </div>
		 </form>
		<div class="slider_btn">
				<button class="js-add-slide1 add_btn"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
			</div>
			</div>
		</div>
	</div>
<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
	<div class="modal fade" id="modal-611">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" id="modal-611-close"class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_template.View_Template')?></h5>
				</div>
			<div class="modal-content custom-content">
				<div class="modal-body custom-body" id="modal-body61">
				<div class="row"> 
			<div class="col-md-12">
				<div class="form-group"> 
 					<label for="field-1" class="control-label">Name of the template in English</label> 
 					<input type="text" id="template_name11" name="template_name1" class="form-control templateform notallowed" id="field-1" placeholder="Peter" value="" style=" background-color: #fff; " readonly> 
 					<label for="field-1" class="control-label" style=" margin-top: 6px; ">Name of the template in German</label> 
 					<input type="text" id="template_name_german11" name="template_name_german1" class="form-control templateform notallowed" id="field-1" placeholder="Peter" value="" style=" background-color: #fff; " readonly> 		
 				</div>  
 			</div> 
		<div class="col-md-12"> 
 		<div class="form-group"> 
 			<label for="field-2" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_template.offset')?></label> 
 			<input type="text" id="template_offset_time11" name="template_offset_time1" class="form-control inputdiv notallowed" id="field-2" value="" readonly> 
 		</div> 
 	</div> 
 </div> 
 <div class="optionBox1" id="optionBox1">

 </div>
 <input type="hidden" name="number_of_activity" id="number_of_activity">
 	<div class="modal-footer custom-footer" id="btned">
				</div>
		     </div>
			</div>
		</div>
	</div>
	<script src="<?php echo url('js/timedropper.js')?>"></script>
	<script type="text/javascript">
	var userLanguage="en";
		$( "#template_name" ).on('input', function() {
    if ($(this).val().length>=25) {
        /*swal('you have reached a limit of 25');  */
        swal("You have reached a limit of 25!.","Due to limited space on mobile app screens", "warning")     
    }
});
		$( "#template_name1" ).on('input', function() {
    if ($(this).val().length>=25) {
     /*   swal('you have reached a limit of 25'); */  
        swal("You have reached a limit of 25!.","Due to limited space on mobile app screens", "warning")    
    }
});
</script>
<script type="text/javascript" src="<?php echo url('assets/slick/slick.min.js')?>"></script>
<!-- script for flat hours -->
<script src="<?php echo url('js/jquery.plugin.min.js')?>"></script>
<script src="<?php echo url('js/jquery.timeentry.js')?>"></script>
<script>
var offsetValue="<?php if (!empty($details['general_setting']['response']['0']['set_working_hours'])) {echo $details['general_setting']['response']['0']['set_working_hours'];}else{echo "00:00";}?>";
var slideIndex=0;
var nextSlideIndex=1;
var i=1;
var j=0;
var remove_btn_div='';
$('.optionBox').slick({
  slidesToShow: 1,
   dots: true,
   swipeToSlide:false,
});
$('.optionBox').slick('slickAdd',"<div class='block wid' ><div><button type='button'  class='delete_btn1'><i class='fa fa-trash-o' aria-hidden='true'></i></button></div> <div class='row'> <div class='col-md-12'> <div class='form-group sort' style='float: left;''> <label for='field-3' class='control-label'><?php echo trans('manage_template.Activity')?></label> <div class='arrow_background'> <select class='form-control send_select_box0' id='template_activity' name='activity_id_0' onChange='changeOfActivity(this,1);' required><option></option><?php for($i=0;$i<count($details['activity_list']['response']);$i++){ echo "<option id='1".$details['activity_list']['response'][$i]['_id']."' value='".$details['activity_list']['response'][$i]['_id']."'>".$details['activity_list']['response'][$i]['activity_name']."</option>"; } ?></select> </div> </div> </div> </div> <div class='row div-flat-time-mode1' id='div-flat-time-mode'> <div class='col-md-12'> <div class='form-group'> <label for='field-4' class='control-label'><?php echo trans('manage_template.flat')?></label> <input type='text' id='template_flat_hours1' name='activity_flat_hours_0' class='form-control inputdiv' id='field-4' value=''> </div> </div> </div> <div class='row div-amount1' id='div-amount'> <div class='col-md-12'> <div class='form-group'> <label for='field-' class='control-label'><?php echo trans('manage_template.amount')?>&nbsp&nbsp&nbsp</label> <input  id='template_amount' name='activity_amount_0' class='form-control no_border' id='field-5' placeholder='200' value='00'> </div> </div> </div> <div class='row div-start-end-break1' id='div-start-end-break' > <div class='col-md-4'> <div class='form-group'> <label for='field-6' class='control-label'><?php echo trans('manage_template.start')?></label> <input type='text' id='template_start1' name='activity_start_0' class='form-control pointer' value='0' readonly> </div> </div> <div class='col-md-4'> <div class='form-group'> <label for='field-7' class='control-label'><?php echo trans('manage_template.end')?></label> <input type='text' id='template_end1' name='activity_end_0' class='form-control pointer' value='0' readonly> </div> </div> <div class='col-md-4 div-break-deduction1' id='div-break-deduction'> <div class='form-group'> <label for='field-8' class='control-label'><?php echo trans('manage_template.break')?></label> <input type='text' id='template_break1' name='activity_break_0' class='form-control inputdiv' id='field-8' value='00:00'> </div> </div> </div> <p id='pagination0'>1/1</p> </div>");
$('.js-add-slide').on('click', function() {
	$('#delete_btn').show();
	if(slideIndex==nextSlideIndex-1)
	{
		  remove_btn_div='<div id="add_remove'+nextSlideIndex+'"></div>';
	}
  i=i+1;
  j=j+1;
  $('.optionBox').slick('slickAdd',"<div class='block wid'>"+remove_btn_div+"<div class='row'><div class='col-md-12'> <div class='form-group sort' style='float: left;''> <label for='field-3' class='control-label'><?php echo trans('manage_template.Activity')?></label> <div class='arrow_background'> <select class='form-control send_select_box"+j+"' id='template_activity5' name='activity_id_"+j+"' onChange='changeOfActivity(this,"+i+");' required><option></option><?php for($i=0;$i<count($details['activity_list']['response']);$i++){ echo "<option id='".$details['activity_list']['response'][$i]['_id']."' value='".$details['activity_list']['response'][$i]['_id']."'>".$details['activity_list']['response'][$i]['activity_name']."</option>"; } ?> </select> </div> </div> </div> </div> <div class='row div-flat-time-mode"+i+"' id='div-flat-time-mode'> <div class='col-md-12'> <div class='form-group'> <label for='field-4' class='control-label'><?php echo trans('manage_template.flat')?></label> <input type='text' id='template_flat_hours"+i+"' name='activity_flat_hours_"+j+"'class='form-control inputdiv' id='field-4' value=''> </div> </div> </div> <div class='row div-amount"+i+"' id='div-amount'> <div class='col-md-12'> <div class='form-group'> <label for='field-5' class='control-label'><?php echo trans('manage_template.amount')?>&nbsp&nbsp&nbsp</label> <input id='template_amount"+i+"' name='activity_amount_"+j+"'class='form-control no_border' id='field-5' placeholder='200' value='00'> </div> </div> </div> <div class='row div-start-end-break"+i+"'id='div-start-end-break' > <div class='col-md-4'> <div class='form-group'> <label for='field-6'class='control-label'><?php echo trans('manage_template.start')?></label> <input type='text' id='template_start"+i+"' name='activity_start_"+j+"' class='form-control pointer' value='0' readonly> </div> </div> <div class='col-md-4'> <div class='form-group'> <label for='field-7' class='control-label'><?php echo trans('manage_template.end')?></label> <input type='text' id='template_end"+i+"' name='activity_end_"+j+"' class='form-control pointer' value='0' readonly> </div> </div> <div class='col-md-4 div-break-deduction"+i+"' id='div-break-deduction'> <div class='form-group'> <label for='field-8' class='control-label'><?php echo trans('manage_template.break')?></label> <input type='text' id='template_break"+i+"' name='activity_break_"+j+"' class='form-control inputdiv' id='field-8' value='00:00'> </div> </div> </div><p id='pagination"+j+"'>"+i+"/"+i+"</p>");
   $('.optionBox').slick('slickNext');
   /*var page =j-1;
   console.log("page"+page);
   $("#pagination"+page).html(+i-1+"/"+i);*/
   for(var a=0; a<=j;a++)
 	{
	 	var page =a;
	 	var end_page=j+1;
	   console.log("page"+page);
	   $("#pagination"+page).html(+a+1+"/"+end_page);
	}
   	  $('#add_remove'+nextSlideIndex).html('<button type="button" onclick="remove_div_action();"id="delete_btn" class="js-remove-slide delete_btn"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
		  $('#add_remove'+slideIndex).html('<button type="button"  class="delete_btn1"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
  slideIndex++;
  nextSlideIndex++;
      $('#number_of_activity').val($('#number_of_activity').html() + i);
if(time_of_mot=='')
{
	if(userLanguage=='en')
		{
			$("#template_start"+i).timeDropper();
			$("#template_end"+i).timeDropper();
		}
	else
		{
			$("#template_start"+i).timeDropper5();
			$("#template_end"+i).timeDropper5();
		}
}
else if(time_of_mot=='am/pm')
{
	$("#template_start"+i).timeDropper();
	$("#template_end"+i).timeDropper();
}
else if(time_of_mot=='24 hours')
{
	$("#template_start"+i).timeDropper5();
	$("#template_end"+i).timeDropper5();
}
else if (time_of_mot=='Industrial')
{
	$("#template_start"+i).timeDropper5();
	$("#template_end"+i).timeDropper5();
}
/*$("#template_offset_time").timeDropper1();
// $("#template_flat_hours"+i).timeDropper4();
	$(function () {
	$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
	});
$("#template_break"+i).timeDropper1();*/
if(time_of_mot=='am/pm')
	{
		if(value_format=='8.15')
		{
			/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});			
		}
		else if(value_format=='8:15')
		{
		/*	$("#template_offset_time").timeDropper1();*/
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00,00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i).timeDropper111({format:'HH,mm',});;
				$( function() {
				Globalize.culture('de');
				$('#template_amount'+i).html($('#template_amount'+i).val("0,00"));
				$("#template_amount"+i).spinner({
				step: 0.01,
				numberFormat: "n",
				});
				});		
			}
			else
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
				$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});		
			}
		}
	}
else if(time_of_mot=='24 hours')
	{
		if(value_format=='8.15')
		{
			/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i).timeDropper11({format:'HH.mm',});;		
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});			
		}
		else if(value_format=='8:15')
		{
			/*$("#template_offset_time").timeDropper1();*/
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();   
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00,00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i).timeDropper111({format:'HH,mm',});;	
				$( function() {
			Globalize.culture('de');
			$('#template_amount'+i).html($('#template_amount'+i).val("0,00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
			else
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
				$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
		}
	}
else if (time_of_mot=='Industrial')
	{
		if(value_format=='8.15')
		{
			/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i).timeDropper11({format:'HH.mm',});;		
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});			
		}
		else if(value_format=='8:15')
		{
			/*$("#template_offset_time").timeDropper1();*/
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00,00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i).timeDropper111({format:'HH,mm',});;	
				$( function() {
			Globalize.culture('de');
			$('#template_amount'+i).html($('#template_amount'+i).val("0,00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
			else
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});;*/
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
				$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
		}
	}
	else
		{
			/*$("#template_offset_time").timeDropper1();*/
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
onLoadActivity(0,i);
});  


function remove_div_action() {
	if(slideIndex==1)
	{
		$('#delete_btn').hide();
	}
	if(slideIndex>0)
	{
  $('.optionBox').slick('slickRemove',slideIndex);
  if (slideIndex !== 0){
/*  	console.log(slideIndex);
    console.log(nextSlideIndex);*/
    slideIndex--;
    nextSlideIndex--;
     $('#add_remove'+slideIndex).html('<button type="button" onclick="remove_div_action();"id="delete_btn" class="js-remove-slide delete_btn"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
    i=i-1;
 	j=j-1;
 	for(var a=0; a<=j;a++)
 	{
	 	var page =a;
	 	var end_page=j+1;
	   console.log("page"+page);
	   $("#pagination"+page).html(+a+1+"/"+end_page);
	}
 	$('.optionBox').slick('slickGoTo',slideIndex);
  }
  $('#number_of_activity').val($('#number_of_activity').html() + i);
}
}
	
</script>
<script type="text/javascript">

//changeOfActivity
var changeOfActivity = function(e,i){
	/*console.log(i);*/
	var index = e.selectedIndex-1;
	console.log(index);
	//if select back to none
	if (index == -1) 
	{
		$(".div-start-end-break"+i).hide();
	$(".div-flat-time-mode"+i).hide();
	$(".div-amount"+i).hide(); 
	}
	//var activityId = e.value;
	var activityDetails = [];
	activityDetails = <?php echo json_encode($details["activity_list"]["response"]);?>;
	/*console.log(activityDetails[index]);*/
	activityDetails = activityDetails[index];
   console.log(activityDetails['_id']);
	//hide or show flat_break_deduction
	if(activityDetails['flat_break_deduction'] == false){
	/*	$('.div-break-deduction').hide();*/
		$('.div-break-deduction'+i).hide();
	}
	else{
		$('.div-break-deduction').show();
		$('.div-break-deduction'+i).show();
	}

	//hide or show flat_time_mode
	if(activityDetails['flat_time_mode'] == false){
		/*$('.div-start-end-break').show();
		$('.div-flat-time-mode').hide();*/
		$('.div-start-end-break'+i).show();
		$('.div-flat-time-mode'+i).hide();
	/*	$('input[name="template_start_"'+i+']').val('');  
		$('input[name="template_end_"'+i+']').val('0'); */
		
	}
	else{
		/*$('.div-start-end-break').hide();
		$('.div-flat-time-mode').show();*/
		$('.div-start-end-break'+i).hide();
		$('.div-flat-time-mode'+i).show();
		var r=i-1;
		$('input[name="activity_start_'+r+'"]').val('00:00');  
		$('input[name="activity_end_'+r+'"]').val('00:00'); 
/*		alert($('input[name="activity_start_'+r+'"]').val());
alert(r);*/

	}
	//hide or show amount field
	if(activityDetails['amount'] == false){

		/*$('.div-amount').hide();*/
		$('.div-amount'+i).hide();
		/*alert('false');*/
	}else{
		console.log('true');
		/*$('.div-amount').show();*/
		$('.div-amount'+i).show();
		/*alert('true');*/
	}
}
var changeOfActivity1 = function(e,i){
	/*console.log(i);*/
	var index = e.selectedIndex;
	console.log(index);
	//if select back to none
	if (index == -1) 
	{
		$(".div-start-end-break"+i).hide();
	$(".div-flat-time-mode"+i).hide();
	$(".div-amount"+i).hide(); 
	}
	//var activityId = e.value;
	var activityDetails = [];
	activityDetails = <?php echo json_encode($details["activity_list"]["response"]);?>;
	/*console.log(activityDetails[index]);*/
	activityDetails = activityDetails[index];
   console.log(activityDetails['_id']);
	//hide or show flat_break_deduction
	if(activityDetails['flat_break_deduction'] == false){
	/*	$('.div-break-deduction').hide();*/
		$('.div-break-deduction'+i).hide();
	}
	else{
		$('.div-break-deduction').show();
		$('.div-break-deduction'+i).show();
	}

	//hide or show flat_time_mode
	if(activityDetails['flat_time_mode'] == false){
		/*$('.div-start-end-break').show();
		$('.div-flat-time-mode').hide();*/
		$('.div-start-end-break'+i).show();
		$('.div-flat-time-mode'+i).hide();
	/*	$('input[name="template_start_"'+i+']').val('');  
		$('input[name="template_end_"'+i+']').val('0'); */
		
	}
	else{
		/*$('.div-start-end-break').hide();
		$('.div-flat-time-mode').show();*/
		$('.div-start-end-break'+i).hide();
		$('.div-flat-time-mode'+i).show();
		var r=i-1;
		$('input[name="activity_start_'+r+'"]').val('00:00');  
		$('input[name="activity_end_'+r+'"]').val('00:00'); 
/*		alert($('input[name="activity_start_'+r+'"]').val());
alert(r);*/

	}
	//hide or show amount field
	if(activityDetails['amount'] == false){

		/*$('.div-amount').hide();*/
		$('.div-amount'+i).hide();
		/*alert('false');*/
	}else{
		console.log('true');
		/*$('.div-amount').show();*/
		$('.div-amount'+i).show();
		/*alert('true');*/
	}
}
//changeOfActivity
var onLoadActivity = function(index,i){
	/*var activityDetails = [];
	activityDetails = <?php echo json_encode($details["activity_list"]["response"]);?>;
	console.log(activityDetails[index]);
	activityDetails = activityDetails[index];
	$('input[name="template_start"]').val('');  
		$('input[name="template_end"]').val('');
	
	
	//onclick load
	//hide or show amount field
	if(activityDetails['amount'] == false){
		$(".div-amount"+i).hide();
	}else{
		$(".div-amount"+i).show();
	}
	//hide or show flat_break_deduction
	if(activityDetails['flat_break_deduction'] == false){
		$(".div-break-deduction"+i).hide();
	}
	else{
		$(".div-break-deduction"+i).show();
	}

	//hide or show flat_time_mode
	if(activityDetails['flat_time_mode'] == false){
		$(".div-start-end-break"+i).show();
		$(".div-flat-time-mode"+i).hide(); 
	}
	else{
		$(".div-start-end-break"+i).hide();
		$(".div-flat-time-mode"+i).show();
	}
	

*/
$(".div-start-end-break"+i).hide();
	$(".div-flat-time-mode"+i).hide();
	$(".div-amount"+i).hide(); 
/*	var r=i-1;
		$('input[name="activity_start_'+r+'"]').val('00');  
		$('input[name="activity_end_'+r+'"]').val('00');*/

}
var onLoadActivity1 = function(index){
/*var activityDetails = [];
	activityDetails = <?php echo json_encode($details["activity_list"]["response"]);?>;
	console.log(activityDetails[index]);
	activityDetails = activityDetails[index];*/
	//hide or show amount field
/*	if(activityDetails['amount'] == false){
		$(".div-amount1").hide();
	}else{
		$(".div-amount1").show();
	}
	//hide or show flat_break_deduction
	if(activityDetails['flat_break_deduction'] == false){
		$(".div-break-deduction1").hide();
	}
	else{
		$(".div-break-deduction1").show();
	}

	//hide or show flat_time_mode
	if(activityDetails['flat_time_mode'] == false){
		$(".div-start-end-break1").show();
		$(".div-flat-time-mode1").hide(); 
	}
	else{
		$(".div-start-end-break1").hide();
		$(".div-flat-time-mode1").show();
	}*/
	$(".div-start-end-break1").hide();
	$(".div-flat-time-mode1").hide();
	$(".div-amount1").hide();  
}

onLoadActivity1(0);

</script>
<script type="text/javascript">
var res2=''; 
var k=0;
var l=0;
var remove_div='';
 var slideIndex1=0;
 var nextSlideIndex1=0;
 var remove_btn_div1='';
 var count_for_remove='';
	function editTemplate(id)
			{	
				k=0;
				l=0;
				slideIndex1=0;
				nextSlideIndex1=1;
				$('#modal-611-close').click();
				$('#modal-61').modal('show', {backdrop: 'static'});
				$.ajax({
					url: "mot_admin_template_edit/"+id,
					type:'post',
					success: function(response)
					{
					setTimeout(function(){						
						var number_of_activity=response['0']['activity'];
						console.log(number_of_activity);
						/*console.log(response);
						console.log(response['0']['_id']);*/
						$('#template_id').val($('#template_id').html() + response['0']['_id']);
					 $('#template_name1').val($('#template_name1').html() + response[0]['template_name']);
					 $('#template_name_german1').val($('#template_name_german1').html() + response[0]['template_name_german']);
					/*$('#template_offset_time1').val($('#template_offset_time1').html() +response[0]['template_offset_time']);*/
				     if(res2 != '')
						{
							$('#optionBox3').append();
						
						}
						else
						{
							var new_number_of_activity=number_of_activity.length;
				     for(i=0;i<new_number_of_activity;i++)
				     {
				     	/*alert(i);*/
				     		l=l+1;
				     		/*console.log('r'+slideIndex1);*/
				     	/*console.log(response['0']['activity'][i]['activity_details']['_id']);*/
				     	 count_for_remove=i+1;
				     	res2 += "<div class='wid'><div id='add_remove1"+count_for_remove+"'></div><div class='row'>  <div class='col-md-12'> <div class='form-group sort' style='float: left;'> <label for='field-3' class='control-label'><?php echo trans('manage_template.Activity')?></label> <div class='arrow_background'> <select class='form-control' id='template_activity"+i+"' name='activity_id_"+k+"' onChange='changeOfActivity1(this,"+i+");'> <?php for($i=0;$i<count($details['activity_list']['response']);$i++){ echo "<option id='".$details['activity_list']['response'][$i]['_id']."' value='".$details['activity_list']['response'][$i]['_id']."'>".$details['activity_list']['response'][$i]['activity_name']."</option>"; } ?></select> </div> </div> </div> </div> <div class='row div-flat-time-mode"+i+"' id='div-flat-time-mode1' > <div class='col-md-12'> <div class='form-group'> <label for='field-4' class='control-label'><?php echo trans('manage_template.flat')?></label> <input type='text' id='template_flat_hours"+i+i+"' name='activity_flat_hours_"+k+"' class='form-control inputdiv' id='field-4' value='' > </div> </div> </div> <div class='row div-amount"+i+"' id='div-amount1'> <div class='col-md-12'> <div class='form-group'> <label for='field-5' class='control-label'><?php echo trans('manage_template.amount')?>&nbsp&nbsp&nbsp</label> <input id='template_amount"+i+i+"' name='activity_amount_"+k+"' class='form-control no_border' id='field-5' value='"+replaceToComma(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_amount'])+"' style='background-color: #fff;' > </div> </div> </div> <div class='row div-start-end-break"+i+"' id='div-start-end-break1'> <div class='col-md-4'> <div class='form-group'> <label for='field-6' class='control-label'><?php echo trans('manage_template.start')?></label> <input type='text' id='template_start"+i+i+"' name='activity_start_"+k+"' class='form-control pointer' style='cursor: pointer; background: #fff;' placeholder='00:00' value='"+response['0']['activity'][i]['activity_start']+"'> </div> </div> <div class='col-md-4'> <div class='form-group'> <label for='field-7' class='control-label'><?php echo trans('manage_template.end')?></label> <input type='text' id='template_end"+i+i+"' name='activity_end_"+k+"' class='form-control pointer' placeholder='00:00' style='cursor: pointer;background: #fff; ' value='"+response['0']['activity'][i]['activity_end']+"' > </div> </div> <div class='col-md-4 div-break-deduction"+i+"' id='div-break-deduction1'> <div class='form-group'> <label for='field-8' class='control-label'><?php echo trans('manage_template.break')?></label> <input type='text' id='template_break"+i+i+"' name='activity_break_"+k+"' class='form-control inputdiv template_break' id='field-8' placeholder='1:00' value='"+response['0']['activity'][i]['activity_break']+"'' > </div> </div> </div><p id='paginationedit"+k+"'></p></div>";
				     	k=k+1; 	 
				     	count_for_remove=0; 
				 /*    	console.log(response['0']['activity'][i]['activity_break']);
				     	console.log(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_break']));*/
				     	//flat tim picker
				 /*    	$(function () {
									 $("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage:'',defaultTime:''});
									});	*/
				     }
				     $('#optionBox3').html(res2);
				     for(var a=0; a<=k;a++)
					 	{
						 	var page =a;
						 	var end_page=k;
						  /* console.log("page"+page);*/
						   $("#paginationedit"+page).html(+a+1+"/"+end_page);
						}
						res2='';
				     $('.optionBox3').slick({
  						slidesToShow: 1,
   						dots: true,
						});
						if(time_of_mot=='')
						{
							if(userLanguage=='en')
							{
								/*$("#template_offset_time1").timeDropper1();*/
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ''});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
							}
							else
							{
								/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});*/
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
							}
						}
						else if(time_of_mot=='am/pm')
						{
							if(value_format=='8.15')
							{
								/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});*/	
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));	
							}
							else if(value_format=='8:15')
							{
								/*$("#template_offset_time1").timeDropper1();	*/
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ''});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
							}
							else if(value_format=="Device region")
							{
								if(separator=="comma")
								{
									/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});	*/
									$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
								}
								else
								{
									/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});*/
									$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
								}
							}
						}
						else if(time_of_mot=='24 hours')
						{
							if(value_format=='8.15')
							{
								/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});*/	
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));				
							}
							else if(value_format=='8:15')
							{
								/*$("#template_offset_time1").timeDropper1();	*/	
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ''});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
							}
							else if(value_format=="Device region")
							{
								if(separator=="comma")
								{
									/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});*/	
									$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));	
								}
								else
								{
									/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});	*/	
									$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
								}
							}
						}
						else if (time_of_mot=='Industrial')
						{
							if(value_format=='8.15')
							{
								/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});	*/	
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));			
							}
							else if(value_format=='8:15')
							{
								/*$("#template_offset_time1").timeDropper1();*/	
								$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ''});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
							}
							else if(value_format=="Device region")
							{
								if(separator=="comma")
								{
									/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});	*/	
									$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
								}
								else
								{
									/*$("#template_offset_time1").timeDropper1({format:'HH,mm',});	*/	
									$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
								}
							}
						}
						else 
						{
							/*$("#template_offset_time1").timeDropper1();	*/
							$("#template_offset_time1").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ''});
								$('#template_offset_time1').val($('#template_offset_time1').html()+replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
						}
					for(i=0;i<new_number_of_activity;i++)
						{
							count_for_remove=i+1;
							slideIndex1++;
							nextSlideIndex1++;
							if(i>0)
							{
								if(i==number_of_activity.length-1 )
								{
									$('#add_remove1'+count_for_remove).html('<button type="button" onclick="remove_div_action1();"id="delete_btn" class="js-remove-slide delete_btn"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
								}
							}
							else
							{
								$('#add_remove1'+count_for_remove).html('<button type="button"  class="delete_btn1"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
							}

							$("#template_activity"+i).val(response['0']['activity'][i]['activity_details']['_id']);
							activityDetails = response[0]['activity'][i]['activity_details'];
							//hide or show amount field
							if(activityDetails['amount'] == false)
							{
								$(".div-amount"+i).hide();
							}
							else
							{
								$(".div-amount"+i).show();
							}
							//hide or show flat_break_deduction
							if(activityDetails['flat_break_deduction'] == false)
							{
								$(".div-break-deduction"+i).hide();
							}
							else
							{
								$(".div-break-deduction"+i).show();
							}

							//hide or show flat_time_mode
							if(activityDetails['flat_time_mode'] == false)
							{
								$(".div-start-end-break"+i).show();
								$(".div-flat-time-mode"+i).hide(); 
								$('input[name="template_start_'+i+'"]').val('');  
								$('input[name="template_end_'+i+'"]').val(''); 
							}
							else
							{
								$(".div-start-end-break"+i).hide();
								$(".div-flat-time-mode"+i).show();
							}
							$('#number_of_activity1').val($('#number_of_activity1').html()+l);				
							if(time_of_mot=='')
							{
								if(userLanguage=='en')
								{
									$("#template_start"+i+i).timeDropper2();
									$("#template_end"+i+i).timeDropper2();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper1();
									$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});
								}
								else
								{
									$("#template_start"+i+i).timeDropper6();
									$("#template_end"+i+i).timeDropper6();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper111({format:'HH,mm',});
									$( function() {Globalize.culture('de');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});
								}
							}
							else if(time_of_mot=='am/pm')
							{
								if(value_format=='8.15')
								{
									$("#template_start"+i+i).timeDropper2();
									$("#template_end"+i+i).timeDropper2();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper11({format:'HH.mm',});	
									$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});			
								}
								else if(value_format=='8:15')
								{
									$("#template_start"+i+i).timeDropper2();
									$("#template_end"+i+i).timeDropper2();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper1();
									$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});	
								}
								else if(value_format=="Device region")
								{
									if(separator=="comma")
									{
										$("#template_start"+i+i).timeDropper2();
										$("#template_end"+i+i).timeDropper2();
										$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
										$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
										$("#template_break"+i+i).timeDropper111({format:'HH,mm',});
										$( function() {Globalize.culture('de');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
									}
									else
									{
										$("#template_start"+i+i).timeDropper2();
										$("#template_end"+i+i).timeDropper2();
										$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
										$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
										$("#template_break"+i+i).timeDropper11({format:'HH.mm',});	
										$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
									}
								}
							}
							else if(time_of_mot=='24 hours')
							{
								if(value_format=='8.15')
								{
									$("#template_start"+i+i).timeDropper6();
									$("#template_end"+i+i).timeDropper6();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper11({format:'HH.mm',});
									$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});						
								}
								else if(value_format=='8:15')
								{
									$("#template_start"+i+i).timeDropper6();
									$("#template_end"+i+i).timeDropper6();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper1();
									$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
								}
								else if(value_format=="Device region")
								{
									if(separator=="comma")
									{
										$("#template_start"+i+i).timeDropper6();
										$("#template_end"+i+i).timeDropper6();
										$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
										$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
										$("#template_break"+i+i).timeDropper111({format:'HH,mm',});
										$( function() {Globalize.culture('de');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
									}
									else
									{
										$("#template_start"+i+i).timeDropper6();
										$("#template_end"+i+i).timeDropper6();
										$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
										$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
										$("#template_break"+i+i).timeDropper11({format:'HH.mm',});
										$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
									}
								}
							}
							else if (time_of_mot=='Industrial')
							{
								if(value_format=='8.15')
								{
									$("#template_start"+i+i).timeDropper6();
									$("#template_end"+i+i).timeDropper6();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper11({format:'HH.mm',});	
									$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});					
								}
								else if(value_format=='8:15')
								{
									$("#template_start"+i+i).timeDropper6();
									$("#template_end"+i+i).timeDropper6();
									$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
									$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
									$("#template_break"+i+i).timeDropper1();
									$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
								}
								else if(value_format=="Device region")
								{
									if(separator=="comma")
									{
										$("#template_start"+i+i).timeDropper6();
										$("#template_end"+i+i).timeDropper6();
										$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
										$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
										$("#template_break"+i+i).timeDropper111({format:'HH,mm',});	
										$( function() {Globalize.culture('de');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
									}
									else
									{
										$("#template_start"+i+i).timeDropper6();
										$("#template_end"+i+i).timeDropper6();
										$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
										$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
										$("#template_break"+i+i).timeDropper11({format:'HH.mm',});	
										$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
									}
								}
							}
							else 
							{
								$("#template_start"+i+i).timeDropper1();
								$("#template_end"+i+i).timeDropper1();
								$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
								$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val(replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])));
								$("#template_break"+i+i).timeDropper1();
								$( function() {Globalize.culture('en');$("#template_amount"+i+i).spinner({step: 0.01,numberFormat: "n"});});		
							}
						}
				}
				},100);
			}
		});
$('.optionBox3').slick('unslick');
}
			$('.js-add-slide1').on('click', function() {
				if(slideIndex1==nextSlideIndex1-1)
				{
					var res= slideIndex1-1;
		  			remove_btn_div1='<div id="add_remove1'+nextSlideIndex1+'"></div>';
		  			  $('#add_remove1'+res).html('<button type="button"  class="delete_btn1"><i class="fa fa-trash-o" aria-hidden="true"></i></button>')
				}

  i=i+1;
  $('.optionBox3').slick('slickAdd',"<div class='wid'>"+remove_btn_div1+"<div><button type='button' class='delete_btn1'><i class='fa fa-trash-o' aria-hidden='true'></i></button></div> <div class='row'> <div class='col-md-12'> <div class='form-group sort' style='float: left;'> <label for='field-3' class='control-label'><?php echo trans('manage_template.Activity')?></label> <div class='arrow_background'> <select class='form-control' id='template_activity' name='activity_id_"+k+"' onChange='changeOfActivity(this,"+i+");' required> <option></option><?php for($i=0;$i<count($details['activity_list']['response']);$i++){ echo "<option id='".$details['activity_list']['response'][$i]['_id']."' value='".$details['activity_list']['response'][$i]['_id']."'>".$details['activity_list']['response'][$i]['activity_name']."</option>"; } ?></select> </div> </div> </div> </div> <div class='row div-flat-time-mode"+i+"' id='div-flat-time-mode'> <div class='col-md-12'> <div class='form-group'> <label for='field-4' class='control-label'><?php echo trans('manage_template.flat')?></label> <input type='text' id='template_flat_hours"+i+i+"' name='activity_flat_hours_"+k+"' class='form-control inputdiv' value='00:00'> </div> </div> </div> <div class='row div-amount"+i+"' id='div-amount'> <div class='col-md-12'> <div class='form-group'> <label for='field-5' class='control-label'><?php echo trans('manage_template.amount')?> &nbsp&nbsp&nbsp</label> <input id='template_amount"+i+"' name='activity_amount_"+k+"' class='form-control no_border' id='field-5' placeholder='200' value='00'> </div> </div> </div> <div class='row div-start-end-break"+i+"' id='div-start-end-break' > <div class='col-md-4'> <div class='form-group'> <label for='field-6' class='control-label'><?php echo trans('manage_template.start')?></label> <input type='text' id='template_start"+i+i+"' name='activity_start_"+k+"' class='form-control pointer' value='0' > </div> </div> <div class='col-md-4'> <div class='form-group'> <label for='field-7' class='control-label'><?php echo trans('manage_template.end')?></label> <input type='text' id='template_end"+i+i+"' name='activity_end_"+k+"' class='form-control pointer' value='0' > </div> </div> <div class='col-md-4 div-break-deduction"+i+"' id='div-break-deduction'> <div class='form-group'> <label for='field-8' class='control-label'><?php echo trans('manage_template.break')?></label> <input type='text' id='template_break"+i+i+"' name='activity_break_"+k+"' class='form-control inputdiv' id='field-8' value='00:00'> </div> </div> </div><p id='paginationedit"+k+"'></p></div>");
  	console.log(slideIndex1);
  	$('#add_remove1'+nextSlideIndex1).html('<button type="button" onclick="remove_div_action1();"id="delete_btn" class="js-remove-slide delete_btn"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
		  $('#add_remove1'+slideIndex1).html('<button type="button"  class="delete_btn1"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
  nextSlideIndex1++;
  	  slideIndex1++;
  	  //add pagination
  	  for(var a=0; a<=k;a++)
					 	{
						 	var page =a;
						 	var end_page=k+1;
						   console.log("page"+page);
						   $("#paginationedit"+page).html(+a+1+"/"+end_page);
						}
    $('.optionBox3').slick('slickGoTo',slideIndex1-1);
      k=k+1;
    l=l+1;
    $('#number_of_activity1').val($('#number_of_activity1').html() + l);
    if(time_of_mot=='')
{
if(userLanguage=='en')
{
$("#template_start"+i+i).timeDropper();
$("#template_end"+i+i).timeDropper();
}
else
{
$("#template_start"+i+i).timeDropper5();
$("#template_end"+i+i).timeDropper5();
}
}
else if(time_of_mot=='am/pm')
{
  $("#template_start"+i+i).timeDropper();
$("#template_end"+i+i).timeDropper();
}
else if(time_of_mot=='24 hours')
{
  $("#template_start"+i+i).timeDropper5();
$("#template_end"+i+i).timeDropper5();
}
else
{
	$("#template_start"+i+i).timeDropper5();
	$("#template_end"+i+i).timeDropper5();
}
/*$("#template_offset_time1").timeDropper1();
$(function () {
 $("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
});
$("#template_break"+i+i).timeDropper1();*/
if(time_of_mot=='am/pm')
	{
		if(value_format=='8.15')
		{
			// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
			$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00.00'));
			$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i+i).timeDropper11({format:'HH.mm',});;			
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=='8:15')
		{
			// $("#template_offset_time1").timeDropper1();
			$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00:00'));
			$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
				$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00,00'));
				$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i+i).timeDropper111({format:'HH,mm',});;	
				$( function() {
			Globalize.culture('de');
			$('#template_amount'+i).html($('#template_amount'+i).val("0,00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
			else
			{
				// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
				$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00.00'));
				$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i+i).timeDropper11({format:'HH.mm',});;	$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});		
			}
		}
	}
else if(time_of_mot=='24 hours')
	{
		if(value_format=='8.15')
		{
			// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
			$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00.00'));
			$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i+i).timeDropper11({format:'HH.mm',});;		
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});			
		}
		else if(value_format=='8:15')
		{
			// $("#template_offset_time1").timeDropper1();
			$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00:00'));
			$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
				$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00,00'));
				$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i+i).timeDropper111({format:'HH,mm',});
				$( function() {
			Globalize.culture('de');
			$('#template_amount'+i).html($('#template_amount'+i).val("0,00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
			else
			{
				// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
				$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00.00'));
				$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i+i).timeDropper11({format:'HH.mm',});	
				$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
		}
	}
else if (time_of_mot=='Industrial')
	{
		if(value_format=='8.15')
		{
			// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
			$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00.00'));
			$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i+i).timeDropper11({format:'HH.mm',});
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});				
		}
		else if(value_format=='8:15')
		{
			// $("#template_offset_time1").timeDropper1();
			$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00:00'));
			$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
				$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00,00'));
				$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i+i).timeDropper111({format:'HH,mm',});
				$( function() {
			Globalize.culture('de');
			$('#template_amount'+i).html($('#template_amount'+i).val("0,00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
			else
			{
				// $("#template_offset_time1").timeDropper1({format:'HH,mm',});;
				$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00.00'));
				$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i+i).timeDropper11({format:'HH.mm',});
				$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});		
			}
		}
	}
	else
		{
			// $("#template_offset_time1").timeDropper1();
			$("#template_flat_hours"+i+i).html($("#template_flat_hours"+i+i).val('00:00'));
			$("#template_flat_hours"+i+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount'+i).html($('#template_amount'+i).val("0.00"));
			$("#template_amount"+i).spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
onLoadActivity(0,i);

});  

console.log('r'+slideIndex1);
 function remove_div_action1() {

  if(slideIndex1 > 1)
  {
  	  $('.optionBox3').slick('slickRemove',slideIndex1,true);
  	  if (slideIndex1 !== 0){
  	console.log(slideIndex1);
      slideIndex1--;
    nextSlideIndex1--;
     console.log('dsds'+slideIndex1);
     if(slideIndex1!=1)
     {
     $('#add_remove1'+slideIndex1).html('<button type="button" onclick="remove_div_action1();"id="delete_btn" class="js-remove-slide delete_btn"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
 	}
    i=i-1;
  k=k-1;
  l=l-1;
  for(var a=0; a<=k;a++)
 	{
	 	var page =a;
	 	var end_page=k;
	   console.log("page"+page);
	   $("#paginationedit"+page).html(+a+1+"/"+end_page);
	}
/*  console.log(l);*/
  }
}

  $('#number_of_activity1').val($('#number_of_activity1').html() + l);
}

</script>
<script type="text/javascript">
	//hours and am/pm converter
	  function hourConvert(time) {
   // Check correct time format and split into components
   time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ?  ' am' : ' pm'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  }

   function ampmConvert(time) {
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "pm" && hours < 12) hours = hours + 12;
    if (AMPM == "am" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    return sHours + ":" + sMinutes;
    }
</script>
<script type="text/javascript">
var res1='';
var div_count=0;
	function viewTemplate(id)
			{
				$('#modal-611').modal('show', {backdrop: 'static'});
				$.ajax({
					url: "mot_admin_template_edit/"+id,
					type:'post',
					success: function(response)
					{   
					setTimeout(function(){	
						var number_of_activity=response['0']['activity'];
						var res='<button class="btn btn-info" id="viewtemplate" onclick="editTemplate((\''+response['0']['_id']+'\'))">Edit</button> <button class="btn btn-info" id="viewtemplate1" onclick="deleteTemplate((\''+response['0']['_id']+'\'))">Delete</button>';
						if (response[0]['deploy_status']!="pending") 
						{
							res='<button class="btn btn-info" id="viewtemplate1" onclick="deleteTemplate((\''+response['0']['_id']+'\'))">Delete</button>';
						}
						$('#btned').html(res);
					 $('#template_name11').val($('#template_name11').html() + response[0]['template_name']);
					 $('#template_name_german11').val($('#template_name_german11').html() + response[0]['template_name_german']);


					 $('#template_offset_time11').val($('#template_offset_time1').html() + replaceTo(time_of_mot,value_format,separator,response[0]['template_offset_time']));
				/*	 console.log(replaceTo(time_of_mot,value_format,response[0]['template_offset_time']));*/
				     if(res1 != '')
						{
							$('#optionBox1').append();
						
						}
						else
						{
				     for(i=0;i<number_of_activity.length;i++)
				     {
				     	div_count++;
				     	var pages=i+1;
				     	var start=response['0']['activity'][i]['activity_start'];
                        var end=response['0']['activity'][i]['activity_end']; 
				     		if(time_of_mot=='am/pm')
				     		{
				     		/*	var tel =response['0']['activity'][i]['activity_start'];
				     			var tel1 =response['0']['activity'][i]['activity_end'];
				     			var valid = (tel.search( /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/) != -1);
				     			if(valid==true)
				     			{
								 var start =hourConvert(tel);
								 var end=hourConvert(tel1);
				     			}
				     			else{
				     				var start =response['0']['activity'][i]['activity_start'];
				     			var end =response['0']['activity'][i]['activity_end'];
				     			}*/
				     			start=moment(response['0']['activity'][i]['activity_start'], ["HH:mm"]).format("h:mm A");
                                end=moment(response['0']['activity'][i]['activity_end'], ["HH:mm"]).format("h:mm A");  
				     		}
				     		else
				     		{
				     			 start=moment(response['0']['activity'][i]['activity_start'], ["h:mm A"]).format("HH:mm");
                                end=moment(response['0']['activity'][i]['activity_end'], ["h:mm A"]).format("HH:mm");
				     			/*var tel =response['0']['activity'][i]['activity_start'];
				     			var tel1 =response['0']['activity'][i]['activity_end'];
				     			console.log(tel);
				     			var valid = (tel.search( /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/) != -1);
				     			console.log(valid);
				     			if(valid==false)
				     			{
								 var start =ampmConvert(tel);
								 var end =ampmConvert(tel1);
								 console.log(res);
				     			}
				     			else{
				     				var start =response['0']['activity'][i]['activity_start'];
				     			var end =response['0']['activity'][i]['activity_end'];
				     			}*/
				     		}
				     		/*console.log(replaceTo(time_of_mot,value_format,response['0']['activity'][i]['activity_flat_hours'])+'flat');*/
				     	res1 += '<div class="block3 wid"> <div class="row">  <div class="col-md-12"> <div class="form-group sort" style=" float: left;"> <label for="field-3" class="control-label"><?php echo trans('manage_template.Activity')?></label> <div class="arrow_background"> <input style="background:#fff;"class="form-control" id="template_activity11" name="template_activity11" value="'+response['0']['activity'][i]['activity_details']['activity_name']+'" readonly>  </div> </div> </div> </div> <div class="row div-flat-time-mode'+i+'" id="div-flat-time-mode1" > <div class="col-md-12"> <div class="form-group"> <label for="field-4" class="control-label"><?php echo trans('manage_template.flat')?></label> <input type="text" id="" name="" class="form-control inputdiv" id="field-4" value="'+replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_flat_hours'])+'" readonly> </div> </div> </div> <div class="row div-amount'+i+'" id="div-amount1"> <div class="col-md-12"> <div class="form-group"> <label for="field-5" class="control-label"><?php echo trans('manage_template.amount')?>&nbsp&nbsp&nbsp</label> <input  name="" class="form-control no_border" id="field-5" value="'+replaceToComma(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_amount'])+'" style=" background-color: #fff; " readonly> </div> </div> </div> <div class="row div-start-end-break'+i+'" id="div-start-end-break1"> <div class="col-md-4"> <div class="form-group"> <label for="field-6" class="control-label"><?php echo trans('manage_template.start')?></label> <input type="text" id="" name="" class="form-control pointer notallowed" style=" cursor: pointer; background: #fff;" placeholder="00:00" value="'+start+'" readonly> </div> </div> <div class="col-md-4"> <div class="form-group"> <label for="field-7" class="control-label"><?php echo trans('manage_template.end')?></label> <input type="text" id="" name="" class="form-control pointer notallowed" placeholder="00:00" style=" cursor: pointer;background: #fff; " value="'+end+'" readonly> </div> </div> <div class="col-md-4 div-break-deduction'+i+'" id="div-break-deduction1"> <div class="form-group"> <label for="field-8" class="control-label"><?php echo trans('manage_template.break')?></label> <input type="text" id="" name="" class="form-control inputdiv notallowed" id="field-8" placeholder="1:00" value="'+replaceTo(time_of_mot,value_format,separator,response['0']['activity'][i]['activity_break'])+'" readonly> </div> </div> </div><p>'+pages+'/'+number_of_activity.length+'</p></div>';	
				     }
				     $('#optionBox1').html(res1);
				     res1='';
				     div_count=0;
				     $('.optionBox1').slick({
  						slidesToShow: 1,
   						dots: true,
  						slidesToScroll: 1
						});

				      for(i=0;i<number_of_activity.length;i++)
				     {
				     	$("#template_activity"+i).val(response['0']['activity'][i]['activity_details']['_id']);
				     	     	activityDetails = response[0]['activity'][i]['activity_details'];
					//hide or show amount field
					if(activityDetails['amount'] == false){
					$(".div-amount"+i).hide();
					}else{
					$(".div-amount"+i).show();
					}
					//hide or show flat_break_deduction
					if(activityDetails['flat_break_deduction'] == false){
					$(".div-break-deduction"+i).hide();
					}
					else{
						$(".div-break-deduction"+i).show();
					}

					//hide or show flat_time_mode
					if(activityDetails['flat_time_mode'] == false){
					$(".div-start-end-break"+i).show();
					$(".div-flat-time-mode"+i).hide();
					}
					else{
					$(".div-start-end-break"+i).hide();
					$(".div-flat-time-mode"+i).show();
				/*	$('input[name="template_start"]').val('00:00');  
					$('input[name="template_end"]').val('00:00');*/
					}
					}
				}
					},100);
				}

				});
$('.optionBox1').slick('unslick');
			}


</script>
<script type="text/javascript">
function deleteTemplate(id)
{
swal({   
	title: "Are you sure?",   
	text: "You will not be able to recover this template!",   
	type: "warning",   
	showCancelButton: true,   
	confirmButtonColor: "#DD6B55",   
	confirmButtonText: "Yes, delete it!",   
	closeOnConfirm: false 
	}, 
	function()
	{   
			$.ajax({
					url: "defaultTemplateDelete/"+id,
					type:'post',
					success: function(response)
					{   
						console.log(response);
						if(response == 1)
						{
							 swal({  
                         title: "Success", 
                         text: "Deleted successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            location.reload();
  
                        });
						}
						else{
							 swal({  
                         title: "Error", 
                         text: "Error Occured",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            location.reload();
  
                        });
						}
					}

				}); 
	});
}

//deploy

function changeStatusDeploy(id)
{
var deployed_time = moment().format('YYYY-MM-DD HH:mm:ss');
swal({   
	title: "Are you sure?",   /*
	text: "You will not be able to recover this template!",*/   
	type: "warning",   
	showCancelButton: true,   
	confirmButtonColor: "#2acd84",   
	confirmButtonText: "Yes, deploy it!",   
	closeOnConfirm: false 
	}, 
	function()
	{   
			$.ajax({
					method: 'POST',
					url: '<?= url('').'/changeStatusDeployForSuperAdmin' ?>',
					data:{"id":id,"deployed_time":deployed_time},
					success: function(response) 
					{   
						console.log(response);
						if(response['status'] == 'success')
						{
							 swal({  
                         title: "Success", 
                         text: "Deployed Successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            location.reload();
  
                        });
						}
						else{
							 swal({  
                         title: "Error", 
                         text: "Error Occured",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            location.reload();
  
                        });
						}
					}

				}); 
	});
}
</script>
<script src="<?php echo url('js/jquery.form.js')?>"></script>
<script>
$('#addbtn').click(function(){
 	
(function() {
$(".slick-cloned :input").attr("disabled", true);//diable the input felid of cloned div formed in addition of slider
$('form').ajaxForm({
    beforeSend: function () {
    var templateName = $('#template_name').val();
    var templateOffsetTime = $('#template_offset_time').val();
    var templateActivity = $("#template_activity").val();
    
    
    var templateFlatHours = $("#template_flat_hours").val();
    var templateAmount = $("#template_amount").val();
    var templateStart = $("#template_start").val();
    var templateEnd = $("#template_end").val();
    var templateBreak = $("#template_break").val();
    var data = [templateName,templateOffsetTime,templateActivity,templateFlatHours,templateAmount,templateStart,templateEnd,templateBreak];
    console.log(data);
    var isValid = true;
    // if(templateName == null || templateOffsetTime == null || templateActivity == null || templateFlatHours == null || templateAmount == null || templateStart == null || templateEnd == null || templateBreak == null ||
    //     templateName == '' || templateOffsetTime == '' || templateActivity == '' || templateFlatHours == '' || templateAmount == '' || templateStart == '' || templateEnd == '' || templateBreak == ''){

	if(templateName == null || templateName == ''){
        isValid = false;
        $('#template_name').css('border-color', '#de7676');
        swal("Error!", "Please fill all the fields", "error");
         return false;
    }
$('#template_name').css('border-color', '#e4e4e4');
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
    	 console.log(data);
/* return;*/
    	$('#addbtn').hide();
 	$('#addloader').show();
    	/*return;*/

 if(data.status=='success')
 {
     swal({  
                         title: "Success", 
                         text: "Added successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             location.reload();
  
                        });
  /*  $("#form1")[0].reset();*/
 }
 else if(data.status=='failure'){
    swal("Error!", "Template name already exists!", "error");
    $('#addbtn').show();
 	$('#addloader').hide();
}

    },
    complete: function() {
    
    }
}); 
$('#addbtn').show();
 	$('#addloader').hide();
})();
});
</script>
<script>
$('#addbtn1').click(function(){
 (function() {
 $(".slick-cloned :input").attr("disabled", true);//diable the input felid of cloned div formed in addition of slider
$('#form2').ajaxForm({
    beforeSend: function () {
    var id = $('#template_id').val();
    var templateName = $('#template_name1').val();
    var templateOffsetTime = $('#template_offset_time1'+id).val();
    var templateActivity = $("#template_activity1").val();
    var templateFlatHours = $("#template_flat_hours1"+id).val();
    var templateAmount = $("#template_amount1").val();
    var templateStart = $("#template_start1"+id).val();
    var templateEnd = $("#template_end1"+id).val();
    var templateBreak = $("#template_break1"+id).val();
  /*  var data = [templateName,templateOffsetTime,templateActivity,templateFlatHours,templateAmount,templateStart,templateEnd,templateBreak];
    console.log(id);
    return false;*/
    var isValid = true;
    if(templateName == null ||
        templateName == '' ){
        isValid = false;
    $('#template_name1').css('border-color', '#de7676');
        swal("Error!", "Please fill all the fields", "error");
         return false;
    }
   $('#template_name').css('border-color', '#e4e4e4'); 
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
    	$('#addbtn1').hide();
 	$('#addloader1').show();
 console.log(data);
 if(data.status=='success')
 {
     swal({  
                         title: "Success", 
                         text: "Updated successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                           location.reload();
  
                        });
  /*  $("#form1")[0].reset();*/
 }
 else if(data.status=='failure'){
    swal("Error!", "Template name already exists!", "error");
    $('#addloader1').hide();
    $('#addbtn1').show();
}

    },
    complete: function() {
    
    }
}); 
$('#addbtn1').show();
 	$('#addloader1').hide();

})();
});
</script>
<script type="text/javascript">

if(time_of_mot=='')
{
	if(userLanguage=='en')
	{
	$("#template_start"+i).timeDropper();
	$("#template_end"+i).timeDropper();
	}
	else
	{
	$("#template_start"+i).timeDropper5();
	$("#template_end"+i).timeDropper5();
	}
}
else if(time_of_mot=='am/pm')
{
	$("#template_start"+i).timeDropper();
	$("#template_end"+i).timeDropper();
}
else if(time_of_mot=='24 hours')
{
	$("#template_start"+i).timeDropper5();
	$("#template_end"+i).timeDropper5();
}
else if (time_of_mot=='Industrial')
{
	$("#template_start"+i).timeDropper5();
	$("#template_end"+i).timeDropper5();
}
else
{
	$("#template_start"+i).timeDropper();
	$("#template_end"+i).timeDropper();
}
//separator
if(time_of_mot=='am/pm')
	{
		if(value_format=='8.15')
		{
			/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
			$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
			$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});		
		}
		else if(value_format=='8:15')
		{
			/*$("#template_offset_time").timeDropper1();*/
			$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: ''});
			$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
				$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00,00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i).timeDropper111({format:'HH,mm',});;
				$( function() {
			Globalize.culture('de');
			$('#template_amount').html($('#template_amount').val("0,00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});		
			}
			else
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
				$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
				$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});		
			}
		}
	}
else if(time_of_mot=='24 hours')
	{
		if(value_format=='8.15')
		{
			/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
			$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
			$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});				
		}
		else if(value_format=='8:15')
		{
			/*$("#template_offset_time").timeDropper1();*/
			$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: ''});
			$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
				$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00,00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i).timeDropper111({format:'HH,mm',});;
				$( function() {
			Globalize.culture('de');
			$('#template_amount').html($('#template_amount').val("0,00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});		
			}
			else
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
				$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
				$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
		}
	}
else if (time_of_mot=='Industrial')
	{
		if(value_format=='8.15')
		{
			/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
			$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
			$("#template_break"+i).timeDropper11({format:'HH.mm',});;		
			$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});			
		}
		else if(value_format=='8:15')
		{
			/*$("#template_offset_time").timeDropper1();*/
			$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: ''});
			$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
		else if(value_format=="Device region")
		{
			if(separator=="comma")
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
				$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00,00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: ','});
				$("#template_break"+i).timeDropper111({format:'HH,mm',});;	
				$( function() {
			Globalize.culture('de');
			$('#template_amount').html($('#template_amount').val("0,00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
			else
			{
				/*$("#template_offset_time").timeDropper1({format:'HH,mm',});*/
				$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
				$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00.00'));
				$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: '',separator: '.'});
				$("#template_break"+i).timeDropper11({format:'HH.mm',});;	
				$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
			}
		}
	}
	else
		{
			/*$("#template_offset_time").timeDropper1();*/
			$("#template_offset_time").timeEntry({unlimitedHours: true,spinnerImage: ''});
			$('#template_offset_time').val($('#template_offset_time').html()+replaceTo(time_of_mot,value_format,separator,offsetValue));
			$("#template_flat_hours"+i).html($("#template_flat_hours"+i).val('00:00'));
			$("#template_flat_hours"+i).timeEntry({unlimitedHours: true,spinnerImage: ''});
			$("#template_break"+i).timeDropper1();
			$( function() {
			Globalize.culture('en');
			$('#template_amount').html($('#template_amount').val("0.00"));
			$("#template_amount").spinner({
			step: 0.01,
			numberFormat: "n",
			});
			});	
		}
</script>
<script type="text/javascript">
	$('#company_template').click(function(){
   $("#float_button").show();	
});
	$('#mot_template').click(function(){
		 $("#float_button").hide();	
	});
</script>
<script type="text/javascript">
/*$('.select_box').change(function(){
	alert($(".select_box").val());
});*/

var activityChange = function(e,id){
	/*console.log(i);*/
	var index = e.selectedIndex;
	console.log(index);
	console.log(id);
		$(".activity_table"+id).hide();
	$("."+id+index).show();
	}
var activityChangeOnLoad = function(index){
/*alert(index);*/
		$(".activity_table").hide();
	$(".activity_table"+index).show();
	}
	activityChangeOnLoad(0);
</script>
<?php include 'admin_footer.php';?>
<script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.culture.de-DE.js')?>"></script>
<script src="<?php echo url('js/jquery.mousewheel.js')?>"></script>
<script>
/*$( function() {
 Globalize.culture('de');
	$('#spinner').html($('#spinner').val("0,00"));
  $("#spinner").spinner({
        step: 0.01,
        numberFormat: "n",
    });
});*/
</script>
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">

	
	<script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>


	<!-- Imported scripts on this page -->
	<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>