<?php  include 'admin_header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/employer.css')?>">
<style type="text/css">
	.employee_whole_left{
		padding: 0px !important;
	}
	.btn {
  -webkit-border-radius: 4;
  -moz-border-radius: 4;
      border-radius: 4px;
    color: #ffffff;
    font-size: 11px;
    background: #3498db;
    padding: 5px 15px 5px 15px;
    text-decoration: none;
    margin: 0px;
}

.btn:hover {
	color: #fff !important;
  text-decoration: none;
}
</style>
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row employer">
				<div class="col-md-12 link">
					<p style=" float: left; "> 
					<a href="#">Home</a>&nbsp
					<span class="template_link">>
					<a href="#">&nbspEmployer Profile</a>
					</span> 
					</p>
					<div style=" float: right;" id="block_btn">
					<button " class="btn" onclick="deleteCompany()" style="background:red">Delete</button>
						<?php 
						if($data[0]['status']=='active')
						{
							echo'<button " class="btn" onclick="block()" id="block">Block</button>';
						}
						else
						{
							echo'<button " class="btn" onclick="unblock()" id="block">Un block</button>';
						}
						?>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
				<div class="fill">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 profile_picture" id="profile_picture">
					<div class="company_logo_heading"><?php echo trans('employer.company_logo')?></div>
					<div class="image " id="profile-image" style="background: url('<?php if (!empty($data[0]['profile_image'])){echo $data[0]['profile_image'];}else{echo url("image/nologo.png");} ?>') no-repeat center #fff;">
					</div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 description">
						<div class="xe-widget xe-conversations subscription_information">		
							<div class="xe-label subscription_info grey">
								<p class="para bold_heading">Subscription Details</p>
								
							</div>
							<?php 
							/*if (!empty($data[0]['subscription_detail'])) {
								$div= '<div class="col-md-6 col-sm-12 xe-body subscription_info_body">
							<div class="col-md-12 col-xs-12 subscription_info_list">
								<div class="subscription_info_title">
									<p class="para light">Subscribed Plan</p>
								</div>
								<div class="subscription_info_value">
									<a href="#">
									<p class="para name">Plan B</p>
									</a>
								</div>	
							</div>
							<div class="col-md-12 col-xs-12 subscription_info_list">
								<div class="subscription_info_title">
									<p class="para light">No of Employees Subscribed</p>
								</div>
								<div class="subscription_info_value">
									<a href="#">
									<p class="para name">500</p>
									</a>
								</div>	
							</div>
						</div>
						<div class="col-md-6 col-sm-12 xe-body subscription_info_body">
							<div class="col-md-12 col-xs-12 subscription_info_list">
								<div class="subscription_info_title">
									<p class="para light">Subscription Period</p>
								</div>
								<div class="subscription_info_value">
									<a href="#">
									<p class="para name">1/6/2015 - 1/6/2016</p>
									</a>
								</div>	
							</div>
							<div class="col-md-12 col-xs-12 subscription_info_list">
								<div class="subscription_info_title">
									<p class="para light">Subscription Expire</p>
								</div>
								<div class="subscription_info_value">
									<a href="#">
									<p class="para name">10/1/2016</p>
									</a>
								</div>	
							</div>
						</div>';
						echo $div;
					}
					else{*/
						$div1='<div style=" width: 100%; float: left; padding: 16px; "><div class="col-md-12 col-sm-12 xe-body subscription_info_body">
							<p style="margin: 10px;font-size: 20px;text-align: center;padding: 23px;">Under free trail</p>
						</div>';
						echo $div1;
				/*	}*/

          ?>



						</div> 
					</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 show_animate employee_whole_left">
					<div class="border">
						<div class="xe-widget xe-conversations basic_information">
							<div class="basic_edit grey">		
							<div class="xe-label basic_info">
								<p class="para bold_heading">Basic Information</p>
							</div>
							<div class="edit_profile">
								<a href="<?php echo url('companyEmployeeList/'.$data[0]['_id'])?>" style=" color: #2196F3; ">
								<i class="fa fa-male" aria-hidden="true" style=" font-size: 25px; "></i>
								&nbsp View employee list
								</a>
							</div>
							</div>
						</div>
						<div class="col-md-12 xe-body basic_info_body">
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light">Company Name</p>

								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name"><?php echo $data[0]['company_name'];?></p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light">Company ID</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name"><?php echo $data[0]['company_unique_id'];?></p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light">Email Address</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name"><?php echo $data[0]['email'];?></p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light">Phone</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$phone=$data[0]['phone'];
										if (!empty($phone))
											{
												echo $phone;
											}
											else{
												echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light">Country</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$country_name=$data[0]['country_name'];
										if (!empty($country_name))
											{
												echo $country_name;
											}
											else{
												echo "--";
												}
										?></p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light">Language</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$language_name=$data[0]['language_name'];
										if (!empty($language_name))
											{
												echo $language_name;
											}
											else{
												echo "--";
												}
										?></p>
									</a>
								</div>	
							</div>
						</div>
						</div>
					</div>

					</div>
<script type="text/javascript">
	function block()
	{
		swal({   
			title: "Are you sure?",   
			text: "If you block company wont be able to login",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Yes, block it!",   
			closeOnConfirm: false 
		}, 
			function(){
			$.ajax({
					url:'blockCompany/<?php echo $data[0]['_id']?>',
					type:'post',
					success:function(response){  
					console.log(response); 
				swal("Blocked!", "This company has been blocked.", "success"); 
				window.location.reload();
			}
			});
		});
	}
function unblock()
	{
		swal({   
			title: "Are you sure?",   
			text: "",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Yes, unblock it!",   
			closeOnConfirm: false 
		}, 
			function(){
			$.ajax({
					url:'unblockCompany/<?php echo $data[0]['_id']?>',
					type:'post',
					success:function(response){  
					console.log(response); 
				swal("Unblocked!", "This company has been Un blocked.", "success"); 
				 window.location.reload();
			}
			});
		});
	}
function deleteCompany()
	{
		swal({   
			title: "Are you sure?",   
			text: "",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Yes, delete it!",   
			closeOnConfirm: false 
		}, 
			function(){
			$.ajax({
					url:'deleteCompany',
					type:'post',
					data:{"id":'<?php echo $data[0]['_id']?>'},
					success:function(response){  
					console.log(response); 
					if (response=="success") 
					{
						swal("Success!", "This company has been deleted.", "success"); 
				 		window.location.href = '<?php echo url('admin_dashboard'); ?>';
					}
					else
					{
						swal("Error!", "Try again", "error"); 
				 		window.location.href = '<?php echo url('admin_dashboard'); ?>';
				 	}
				
			}
			});
		});		
	}
</script>
<?php  include 'admin_footer.php';?>