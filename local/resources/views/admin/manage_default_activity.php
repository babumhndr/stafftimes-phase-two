<?php  include 'admin_header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/manage_activity.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link href="<?php echo url('css/jquery.timeentry.css')?>" rel="stylesheet">
<style type="text/css">
	.float_button
	{
		position: absolute;
		left: 89%;
		top: -4px;
	}
</style>
 <?php 
 $general_setting=$details['general_setting'];
 $separator=$details['separator'];
  $valueFormat='';
 $timeOfMot='';
 $separatorValue='';
 if (!empty($general_setting['response'])) {$valueFormat=$general_setting['response']['0']['value_format'];} 
 if (!empty($general_setting['response'])) {$timeOfMot=$general_setting['response']['0']['time_style'];} 
 if (!empty($separator['data'])) {$separatorValue=$separator['data'];}
function replaceToDot($valueFormat,$timeOfMot, $separatorValue,$str)
{
 if($timeOfMot=='am/pm')
	{
		if($valueFormat=='8.15')
		{
			 return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
else if($timeOfMot=='24 hours')
	{
		if($valueFormat=='8.15')
		{
			return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
else if ($timeOfMot=='Industrial')
	{
		if($valueFormat=='8.15')
		{
			return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
	else
	{
		return $str;
	}
}
 ?>   
<script type="text/javascript">
	var time1='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['time_style'];} ?>';
  var value_format='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['value_format'];} ?>';
  var separator='<?php if (!empty($separator['data'])) {echo $separator['data'];} ?>';
  var offset='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['set_working_hours'];} ?>';
  console.log(time1);
  console.log(value_format);
  console.log(separator);
  console.log(offset);
  var baseUrl="<?= url('');?>";
</script>
<div class="row mot_template">
				<div class="col-md-12 link">
				<div style="float:left;">
					<p> 
					<a href="dashboard"><?php echo trans('header.dashboard')?></a>&nbsp
					<span class="template_link">>
					<a href="manage_activity">&nbsp<?php echo trans('manage_activity.manage_activity')?></a></span> 
					</p>
				</div>
				
				
		</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
				<ul class="nav nav-tabs">
						<li class="top_list active" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_tab'); ?></p> ">
							<a href="#home" class="top_anchor" data-toggle="tab" id="company_activity">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"><?php echo trans('manage_activity.company_activity')?></span>
							</a>
						</li>
						<!-- <li class="top_list">
							<a href="#profile" class="top_anchor" data-toggle="tab" id="mot_activity">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs"><?php echo trans('manage_activity.my_overtime_activity')?></span>
							</a>
						</li> -->
						<!-- <li class="top_list">
							<a href="#messages" class="top_anchor" data-toggle="tab">
								<span class="visible-xs"><i class="fa-envelope-o"></i></span>
								<span class="hidden-xs">Most Used Template</span>
							</a>
						</li> -->
					</ul>
					
					<div class="tab-content custom">
						<div class="tab-pane active" id="home">
							
							<div class="col-md-12 col-sm-12 col-xs-12 center">
								<div class="xe-widget xe-conversations temp_title">		
						<div class="xe-body basic_info_body">
							
		
	
			
			<!-- Basic Setup -->
			<div class="">
				
				<div class="panel-body">
					
					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-1").dataTable({
							aLengthMenu: [
								[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
							]
						});
					});
					</script>
					<?php
						if($details['activity_list']['status'] == 'success'){
							$tableDisplay = "display:block";
							$noItem = "display:none";
						}
						else{
							$tableDisplay = "display:none";
							$noItem = "display:block";
						}
					?>
					<div style="<?=$tableDisplay?>">
					<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%" >
						<thead>
							<tr>
								<th><?php echo trans('manage_activity.activity');?></th>
								<th>Activity In German</th>
								<th><?php echo trans('manage_activity.offset')?></th>
								<th><?php echo trans('manage_activity.flat')?></th>
								<th><?php echo trans('manage_activity.own')?></th>
								<th><?php echo trans('manage_activity.amount')?></th>
								<th><?php echo trans('manage_activity.amount_type')?></th>
								<th><?php echo trans('manage_activity.rate')?></th>
								<th>Deploy Status</th>
							</tr>
						</thead>
					<tbody>

						<?php
							for($i=0;$i<count($details['activity_list']['response']);$i++){
								($details['activity_list']['response'][$i]['flat_break_deduction'] == true) ? $flat_break_deduction = 'On' : $flat_break_deduction = 'Off';
								($details['activity_list']['response'][$i]['own_account'] == true) ? $own_account = 'On' : $own_account = 'Off';
								($details['activity_list']['response'][$i]['amount'] == true) ? $amount = 'On' : $amount = 'Off';
								$activityId = '"'.$details['activity_list']['response'][$i]['_id'].'"';

								if ($details['activity_list']['response'][$i]['deploy_status']=="pending") 
								{
									$deployDiv="<button class='deploy_btn' onclick ='changeStatusDeploy(".$activityId.");' >Deploy</button>
										<button class='delete_btn' onclick ='deleteActivity(".$activityId.");' >Delete</button>";
								}
								else
								{
									$deployDiv='<p style=" color: #2acd84; ">Deployed</p>';
								}

								echo "<tr>
								<td onclick = 'viewActivity(".$activityId.");' style='color:#2392ec;cursor:pointer;'>".$details['activity_list']['response'][$i]['activity_name']."</td>
								<td>".$details['mot_activity_list']['response'][$i]['activity_german_name']."</td>
								<td>".$details['activity_list']['response'][$i]['against_offset']."</td>
								<td>".$flat_break_deduction."</td>
								<td>".$own_account."</td>
								<td>".$amount."</td>
								<td>".$details['activity_list']['response'][$i]['amount_type']."</td>
								<td>".replaceToDot($valueFormat,$timeOfMot, $separatorValue,$details['activity_list']['response'][$i]['hourly_rate_multiplier'])."</td>
								<td>".$deployDiv."</td>
								
								
								
							</tr>";
							}
						?>
					
						
							
							
							
						</tbody>
					</table>
				</div>
					<div class="col-md-12 error_msg" style="<?=$noItem?>"><p><?php echo trans('manage_activity.data_not_available')?></p></div>
					
				</div>
			</div>
			
			
	
				</div>
			</div>								
	
								
							</div>
							
						</div>
	

						<div class="float_button" id="float_button">
							<button onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"  type="submit" class="circular_button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							<p class="add_temp_text"><?php echo trans('manage_activity.add')?></p>
							<p class="add_temp_text"><?php echo trans('manage_activity.activity')?></p></button>
						</div>
					</div>
				</div>
			</div>
				</div>
			</div>
			<div class="modal fade" id="modal-6">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_activity.modal_title')?></h5>
				</div>
			<div class="modal-content custom-content">
				
				<div class="modal-body custom-body">
				<form method="post" action="addDefaultActivity">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">Name of the activity in English</label>
								
								<input type="text" class="form-control" id="activity_name" name="activity_name" placeholder="Activity name" maxlength="20">
								<label for="field-1" class="control-label" style=" margin-top: 6px; ">Name of the activity in German</label>
								<input type="text" class="form-control" id="activity_german_name" name="activity_german_name" placeholder="Activity Name German" maxlength="20">
							</div>	
							
						</div>
					</div>
						<div class="row">
						<div class="col-md-12 extra_margin">
							
							<div class="form-group">
									<label class="col-sm-4 control-label no_padding" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_activity.offset')?></label>
									
									<div class="col-sm-8 no_padding">
									<p>
											<label class="radio-inline" style=" display: inline; ">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_count"  value="Count" checked>
												<?php echo trans('manage_activity.offset_count')?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_not_count" value="Not Count">
												<?php echo trans('manage_activity.offset_nocount')?>
											</label>
									</p>
									</div>
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-7 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_flat'); ?></p> "><?php echo trans('manage_activity.flat')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_break_deduction" name="flat_break_deduction" id="flat_break_deduction" ></input>
							</div>	
							</div>
							<div class="col-sm-5 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_own'); ?></p> "><?php echo trans('manage_activity.own')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="own_account" id="own_account" ></input>
							</div>	
							</div>
							<div class="col-sm-4 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_amount'); ?></p> "><?php echo trans('manage_activity.amount')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info lump_hours" name="amount" id="lump_hours" >
							</div>	
							</div>
							<div class="col-sm-4 no_padding t_align">
								<button type="button" class="btn lab disabled" id="toggle_lab" style=" background: #2392ec; padding: 2px 10px 3px 10px; border-radius: 5px; "><?php echo trans('manage_activity.lump_sum')?></button>
								<input  type="hidden" class="input_class" id="amount_type" name="amount_type" value="Lump"></input>		
							</div>
							<div class="col-sm-4 no_padding t_align input-1">
								<input class="input_class" placeholder="99.99" value="0" id="amount_value" name="amount_value" style=" background: white; border: 1px solid #e6e6e6; padding: 0px; "></input>	
							</div>
						<!-- <div class="col-sm-4 no_padding t_align input-2 input-3">
								<input  type="text" class="input_class" placeholder="99.99" disabled></input>
									
							</div> -->
						</div>
						<div class="col-md-12 extra_margin lab_data">
							<p class="center_align">
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="hourly">
										<?php echo trans('manage_activity.hourl')?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="lump_sum">
										<?php echo trans('manage_activity.lump_sum')?>
								</label>
							</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
							<div class="col-md-12">
								<label for="field-4" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_allownce'); ?></p> "><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;
								
								<input type="text" value="0:00" class="form-control custom_box" id="activity_hour" name="activity_hour">&nbsp;&nbsp;
								<label for="field-5" class="control-label"><?php echo trans('manage_activity.hours')?></label>
								</div>
								<div class="col-md-12" style=" margin-bottom: 10px; ">
								<label for="field-4" class="control-label"><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;
								<input type="text" class="form-control custom_box" id="activity_day" name="activity_day" value="0"  readonly >&nbsp;&nbsp;
								<label for="field-5" class="control-label" ><?php echo trans('manage_activity.days')?></label>
								</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="form-group">
								<label for="field-6" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_hourly'); ?></p> "><?php echo trans('manage_activity.Hourly')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input  class="form-control custom_box hourly_rate_multiplier no_border" id="hourly_rate_multiplier" name="hourly_rate_multiplier" style="width: 65px;">

								</div>	
							</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group float_left">
								<label for="field-7" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_input'); ?></p> "><?php echo trans('manage_activity.flat_time_mode')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_time_mode" id="flat_time_mode" name="flat_time_mode">
							</div>	
							
						</div>
					</div>
					<div class="row ftm_data">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-8" class="col-sm-6 control-label no_padding"><?php echo trans('manage_activity.Pre_populated')?></label>
									
								<div class="col-sm-4 no_padding">
										
									<div class="form-block">
										<label>
											<input type="radio" name="pre_populated_with" class="cbr cbr-blue form-control" value="Offset" id="offset">
												<?php echo trans('manage_activity.off')?>
										</label>
										<br />
										<label>
											<input type="radio" name="pre_populated_with" class="cbr cbr-blue form-control" value="Hours"  id="hours" checked> 
												<?php echo trans('manage_activity.hours')?>
										</label>
									</div>
							</div>
							<div class="col-sm-2 no_padding">
								<input type="text" class="form-control custom_box" style="margin-top: 18px;" id="pre_populated" name="pre_populated_value">
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group float_left">
								<label for="field-9" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_reduce'); ?></p> "><?php echo trans('manage_activity.Overtime_Reducerd')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="overtime_reducer">
					</div>
					</div>	
				</div>
				<input type="hidden" id="time" name="time"></input>
				<div class="modal-footer custom-footer">
				<div id="addbtn"> 
					<button type="submit" class="btn btn-info" id="save_btn"><?php echo trans('manage_activity.save')?></button>
					</div>
					<div id="addloader" style="display:none">
						<img src="<?=url('image/mot_loader.gif')?>">
					</div>
				</div>
			</div>
			</form>    
		</div>
	</div>
	</div>


<div class="modal fade" id="modal-61">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_activity.edit')?></h5>
				</div>
			<div class="modal-content custom-content">
				<form method="post" action="updateDefaultActivity" id="form2">
				<div class="modal-body custom-body" id="modal-body6">
										<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">Name of the activity in English</label>
								
								<input type="text" class="form-control" id="activity_name1" name="activity_name" placeholder="Activity name" maxlength="20">
								<label for="field-1" class="control-label" style=" margin-top: 6px; ">Name of the activity in German</label>
								<input type="text" class="form-control" id="activity_german_name1" name="activity_german_name" placeholder="Activity Name German" maxlength="20">
							</div>	
							
						</div>
					</div>
						<div class="row">
						<div class="col-md-12 extra_margin">
							
							<div class="form-group">
									<label class="col-sm-4 control-label no_padding" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_activity.offset')?></label>
									
									<div class="col-sm-8 no_padding">
									<p>
											<label class="radio-inline" style=" display: inline; ">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control " id="against_offset_count1"  value="Count" >
												<?php echo trans('manage_activity.offset_count')?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_not_count1" value="Not Count">
											<?php echo trans('manage_activity.offset_nocount')?>
											</label>
									</p>
									</div>
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-7 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_flat'); ?></p> "><?php echo trans('manage_activity.flat')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_break_deduction" name="flat_break_deduction" id="flat_break_deduction1" ></input>
							</div>	
							</div>
							<div class="col-sm-5 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_own'); ?></p> "><?php echo trans('manage_activity.own')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="own_account" id="own_account1" ></input>
							</div>	
							</div>
							<div class="col-sm-4 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_amount'); ?></p> "><?php echo trans('manage_activity.amount')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" id="lump_hours1" name="amount">
							</div>	
							</div>
							<div class="col-sm-4 no_padding t_align">
								<button type="button" class="btn lab disabled" id="toggle_lab1" style=" background: #2392ec; padding: 2px 10px 3px 10px; border-radius: 5px; "></button>
								<input  type="hidden" class="input_class" id="amount_type1" name="amount_type" value="Lump"></input>		
							</div>
							<div class="col-sm-4 no_padding t_align input-1">
								<input class="input_class" placeholder="99.99" id="amount_value1" name="amount_value" style=" background: white; border: 1px solid #e6e6e6; padding: 0px; "></input>	
							</div>
<!-- 							<div class="col-sm-4 no_padding t_align input-2 input-3">
								<input  type="text" class="input_class" placeholder="99.99" disabled></input>
									
							</div> -->
						</div>
						<div class="col-md-12 extra_margin lab_data">
							<p class="center_align">
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="hourly1">
										<?php echo trans('manage_activity.hourl')?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="lump_sum1">
										<?php echo trans('manage_activity.lump_sum')?>
								</label>
							</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
							<div class="col-md-12">
								<label for="field-4" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_allownce'); ?></p> "><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input type="text" class="form-control custom_box" id="activity_hour1" name="activity_hour">&nbsp;
								<label for="field-5" class="control-label"><?php echo trans('manage_activity.hours')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="col-md-12" style="margin-bottom: 10px">
								<label for="field-4" class="control-label"><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="form-control custom_box" id="activity_day1" name="activity_day" readonly >&nbsp;
								<label for="field-5" class="control-label" ><?php echo trans('manage_activity.days')?></label>
								</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="form-group">
								<label for="field-6" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_hourly'); ?></p> "><?php echo trans('manage_activity.Hourly')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input class="form-control custom_box no_border" id="hourly_rate_multiplier1" name="hourly_rate_multiplier">

								</div>	
							</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group float_left">
								<label for="field-7" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_input'); ?></p> "><?php echo trans('manage_activity.flat_time_mode')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_time_mode" id="flat_time_mode1" name="flat_time_mode">
							</div>	
							
						</div>
					</div>
					<div class="row ftm_data" id="ftm_data1">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-8" class="col-sm-6 control-label no_padding"><?php echo trans('manage_activity.Pre_populated')?></label>
									
								<div class="col-sm-4 no_padding">
										
									<div class="form-block">
										<label>
											<input type="radio" id="offset1" name="pre_populated_with" class="cbr cbr-blue form-control" value="Offset">
												<?php echo trans('manage_activity.off')?>
										</label>
										<br />
										<label>
											<input type="radio" id="hours1" name="pre_populated_with" class="cbr cbr-blue form-control" value="Hours"> 
												<?php echo trans('manage_activity.hours')?>
										</label>
									</div>
							</div>
							<div class="col-sm-2 no_padding">
								<input type="text" class="form-control custom_box"  style="margin-top: 18px;" id="pre_populated1" name="pre_populated_value">
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group float_left">
								<label for="field-9" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_reduce'); ?></p> "><?php echo trans('manage_activity.Overtime_Reducerd')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="overtime_reducer" id="overtime_reducer1">
					</div>
					</div>	
				</div>
	<!-- 			<input type="hidden" id="time" name="time"></input> -->


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->


	<input type="hidden" id="activity_id" name="activity_id" value=""></input>
				<div class="modal-footer custom-footer">
					<div id="addbtn1"> 
					<button type="submit" class="btn btn-info" id="save_btn"><?php echo trans('manage_activity.save')?></button>
					</div>
					<div id="addloader1" style="display:none">
						<img src="<?=url('image/mot_loader.gif')?>">
					</div>
				</div>
			</div>
		     </div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-611">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" id="modal-611-close" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_activity.view_activity')?></h5>
				</div>
			<div class="modal-content custom-content">
				<div class="modal-body custom-body" id="modal-body61">
										<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label">Name of the activity in English</label>
								
								<input type="text" class="form-control" id="activity_name11" name="activity_name" placeholder="Activity name" maxlength="20" readonly>
								<label for="field-1" class="control-label" style=" margin-top: 6px; ">Name of the activity in German</label>
								<input type="text" class="form-control" id="activity_german_name11" name="activity_german_name" placeholder="Activity Name German" maxlength="20" readonly>
							</div>
							
						</div>
					</div>
						<div class="row">
						<div class="col-md-12 extra_margin">
							
							<div class="form-group">
									<label class="col-sm-4 control-label no_padding" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_activity.offset')?></label>
									
									<div class="col-sm-8 no_padding">
									<p>
											<label class="radio-inline" id="against_offset_count111" style=" display: inline; ">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control " id="against_offset_count11"  value="Count" >
												<?php echo trans('manage_activity.offset_count')?>
											</label>
											<label class="radio-inline" id="against_offset_not_count111">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_not_count11" value="Not Count" >
												<?php echo trans('manage_activity.offset_nocount')?>
											</label>
									</p>
									</div>
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-7 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_flat'); ?></p> "><?php echo trans('manage_activity.flat')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="flat_break_deduction" id="flat_break_deduction11" disabled></input>
							</div>	
							</div>
							<div class="col-sm-5 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_own'); ?></p> "><?php echo trans('manage_activity.own')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="own_account" id="own_account11" disabled></input>
							</div>	
							</div>
							<div class="col-sm-4 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_amount'); ?></p> "><?php echo trans('manage_activity.amount')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" id="lump_hour" disabled>
							</div>	
							</div>
							<div class="col-sm-4 no_padding t_align">
								<button type="button" class="btn lab disabled" id="toggle_lab11" style=" background: #2392ec; padding: 2px 10px 3px 10px; border-radius: 5px; "></button>
								<input  type="hidden" class="input_class" id="amount_type11" name="amount_type1" value="Lump" readonly></input>		
							</div>
							<div class="col-sm-4 no_padding t_align input-1">
								<input  type="text" class="input_class" step="0.01" placeholder="99.99" id="amount_value11" name="amount_value" style=" background: white; border: 1px solid #e6e6e6; padding: 0px; " readonly></input>	
							</div>
<!-- 							<div class="col-sm-4 no_padding t_align input-2 input-3">
								<input  type="text" class="input_class" placeholder="99.99" disabled></input>
									
							</div> -->
						</div>
						<div class="col-md-12 extra_margin lab_data">
							<p class="center_align">
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="hourly11" readonly>
										<?php echo trans('manage_activity.hourl')?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="lump_sum11" readonly>
										<?php echo trans('manage_activity.lump_sum')?>
								</label>
							</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
							<div class="col-md-12">
								<label for="field-4" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_allownce'); ?></p> "><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input type="text" class="form-control custom_box" id="activity_hour11" name="activity_hour" style=" background: #fff; " readonly>&nbsp;
								<label for="field-5" class="control-label"><?php echo trans('manage_activity.hours')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="col-md-12" style="margin-bottom: 10px;">
								<label for="field-4" class="control-label"><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="form-control custom_box" id="activity_day11" name="activity_day1"  readonly >&nbsp;
								<label for="field-5" class="control-label" ><?php echo trans('manage_activity.days')?></label>
								</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="form-group">
								<label for="field-6" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_hourly'); ?></p> "><?php echo trans('manage_activity.Hourly')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input type="text" class="form-control custom_box" id="hourly_rate_multiplier11" name="hourly_rate_multiplier" style=" background: #fff; " readonly>

								</div>	
							</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group float_left">
								<label for="field-7" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_input'); ?></p> "><?php echo trans('manage_activity.flat_time_mode')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info " id="flat_time_mode11" name="flat_time_mode" disabled>
							</div>	
							
						</div>
					</div>
					<div class="row ftm_data" id="ftm_data2">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-8" class="col-sm-6 control-label no_padding"><?php echo trans('manage_activity.Pre_populated')?></label>
									
								<div class="col-sm-4 no_padding">
										
									<div class="form-block">
										<label class="offset11">
											<input type="radio" id="offset11" name="pre_populated_with" class="cbr cbr-blue form-control" value="Offset" >
												<?php echo trans('manage_activity.off')?>
										</label>
										<br />
										<label class="hours11">
											<input type="radio" id="hours11" name="pre_populated_with" class="cbr cbr-blue form-control" value="Hours"  > 
												<?php echo trans('manage_activity.hours')?>
										</label>
									</div>
							</div>
							<div class="col-sm-2 no_padding hours11">
								<input type="text" class="form-control custom_box" id="pre_populated111" name="pre_populated_value" style=" background: #fff;margin-top: 8px;" readonly>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group float_left">
								<label for="field-9" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_reduce'); ?></p> "><?php echo trans('manage_activity.Overtime_Reducerd')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="overtime_reducer" id="overtime_reducer11" disabled>
					</div>
					</div>	
				</div>
				<div class="modal-footer custom-footer" id="btned">
					<!-- <button class="btn btn-info" id="viewactivity" >Edit</button>
					<button class="btn btn-info" id="viewactivity1">Delete</button> -->
				</div>
			</div>
		     </div>
			</div>
		</div>
	</div>

<script src="<?php echo url('js/jquery.form.js')?>"></script>
<script src="<?php echo url('js/jquery.plugin.min.js')?>"></script>
<script src="<?php echo url('js/jquery.timeentry.js')?>"></script>
<script type="text/javascript" src="<?= url('js/admin_manage_activity.js')?>"></script>
<?php  include 'admin_footer.php';?>
<script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.culture.de-DE.js')?>"></script>
<script src="<?php echo url('js/jquery.mousewheel.js')?>"></script>
<!-- <script type="text/javascript">
	$( function() {
	  Globalize.culture('de');
	  $('#spinner').html($('#spinner').val("1,000"));
  $("#spinner").spinner({
        min: 0.000,
        max: 2.000,
        step: 0.001,
        numberFormat: "N3",
    });
});
</script> -->
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="<?= url('assets/js/datatables/dataTables.bootstrap.css')?>">

	
	<script src="<?= url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>


	<!-- Imported scripts on this page -->
	<script src="<?= url('assets/js/datatables/dataTables.bootstrap.js')?>"></script>
	<script src="<?= url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js')?>"></script>
	<script src="<?= url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>	

