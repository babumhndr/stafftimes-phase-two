<?php  include 'admin_header.php';?>
<div class="col-md-12">
   <form >
    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top:20px;" >
   	<!-- <h4 style="text-align:center;">Germany</h4> -->
    <label>Select a country you want to edit the price for :</label>
    <select id="country_id" onchange="country()"><?php for($i=0;$i<count($details);$i++) { echo "<option value=".$details[$i]['_id']['country_code']."> ".$details[$i]['_id']['country_name']."</option>";}?>
    </select>
    <div id="form_append" style="padding-top:10px;"></div>
    </div>
</form>

<script>
function country() {
    var code = $('#country_id').val();
    /*console.log(x);*/
    
    var form_data= "<form method='post' id='form'><div class='col-md-4'><label >Country currency</label><br /><textarea style='width:100%;' rows=1; id='currency1' readonly></textarea><br /><label >Plan Name</label><br /><textarea style='width:100%;' rows=1; id='plan_name1' readonly></textarea><br /><label >Plan Price</label><br /><textarea style='width:100%;' rows=1; id='plan_price1' name='plan_price1' ></textarea><br /><input type='hidden' id='plan_id1' ><br /></div><div class='col-md-4'><label >Country currency</label><br /><textarea style='width:100%;' rows=1; id='currency2' readonly></textarea><br /><label >Plan Name</label><br /><textarea style='width:100%;' rows=1; id='plan_name2' readonly></textarea><br /><label >Plan Price</label><br /><textarea style='width:100%;' rows=1; id='plan_price2' name='plan_price2'></textarea><br /><input type='hidden' id='plan_id2' ><br /></div><div class='col-md-4'><label >Country currency</label><br /><textarea style='width:100%;' rows=1; id='currency3' readonly></textarea><br /><label >Plan Name</label><br /><textarea style='width:100%;' rows=1; id='plan_name3' readonly></textarea><br /><label >Plan Price</label><br /><textarea style='width:100%;' rows=1; id='plan_price3' name='plan_price3'></textarea><br /><input type='hidden' id='plan_id3' ><br /></div><div class='addbtn'><input type='button' id='save' onclick='updatePrice()' class='btn btn-info' value='Save'></div></form>"
    $('#form_append').html(form_data);
     $.ajax({
          type:"GET",
          url: "<?php echo url('editPrice');?>/"+code,
          success: function(response) {
            console.log(response);
           /* var x = response['faq_details'][0]['topic_en'];
            console.log(x);*/
            $("#currency1").val($("#currency1").html()+response[0]['currency_code']);
            $("#plan_name1").val($("#plan_name1").html()+response[0]['plan']);
            $("#plan_price1").val($("#plan_price1").html()+response[0]['plan_price']);
            $("#plan_id1").val($("#plan_id1").html()+response[0]['_id']);
            $("#currency2").val($("#currency2").html()+response[1]['currency_code']);
             $("#plan_name2").val($("#plan_name2").html()+response[1]['plan']);
            $("#plan_price2").val($("#plan_price2").html()+response[1]['plan_price']);
            $("#plan_id2").val($("#plan_id2").html()+response[1]['_id']);
             $("#currency3").val($("#currency3").html()+response[2]['currency_code']);
            $("#plan_name3").val($("#plan_name3").html()+response[2]['plan']);
            $("#plan_price3").val($("#plan_price3").html()+response[2]['plan_price']);
            $("#plan_id3").val($("#plan_id3").html()+response[2]['_id']);
          } 
        });
}
</script>
<script >
function updatePrice() { 
  var plan_price1=$('#plan_price1').val();
  var plan_price2= $('#plan_price2').val();
  var plan_price3= $('#plan_price3').val();
  var plan_id1=$('#plan_id1').val();
  var plan_id2= $('#plan_id2').val();
  var plan_id3= $('#plan_id3').val();
  /*console.log(id);
  console.log(englishtopic);
  console.log(germantopic);*/
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    /*console.log(plan_price1)*/
    if (isConfirm) {   
        $.ajax({
          type:"POST",
          url: "<?php echo url('updatePrice');?>",
          data: {'plan_id1':plan_id1, '0':plan_price1, 'plan_id2':plan_id2, '1':plan_price2, 'plan_id3':plan_id3, '2':plan_price3},
          success: function(response) {
              console.log(response)
             if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminCountryPriceCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
}
</script>