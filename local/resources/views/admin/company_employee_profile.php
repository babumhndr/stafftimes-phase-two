<?php  include 'admin_header.php';?>

    <link rel="stylesheet" href="<?php echo url('assets/css/mot_profile.css')?>">
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row mot_profile_content">
				<div class="col-md-4 col-xs-12">
					<div class="fill">
						<div class="col-md-7 col-sm-6 col-xs-12 employee_picture">
							<div class="profile_picture" style="background: url('<?php echo $data[0]['profile_image'];?>') no-repeat center #fff;">
							</div>
							<div class="profile_name">
							<p style=" margin-bottom: 0px; margin-top: 9px; color: #2392ec; "><?php echo $data[0]['name'];?></p>
							<p style="font-size: 12px;">
							<?php 
							$department_data=$data[0]['department'];
							if (!empty($department_data))
							{
								echo $department_data;
							}
							else{
								echo "--";
							}
							?>
							</p></div>
						</div>
						<div class="col-md-5 col-sm-6 col-xs-12 employee_data">
							<div class="xe-widget xe-conversations profile_data">		
								<div class="xe-label profile_heading">
									<p>Weekly Overtime</p>
									<p>Overview</p>
								</div>
							<div class="xe-body profile_body">
								<ul class="list-unstyled">
									<li>
										<div class="mot_day_stats">
											<div class="xe-comment">
												<p class="day">Monday</p>
												<p class="hours">10 hours</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats grey">
											<div class="xe-comment">
												<p class="day">Tuesday</p>
												<p class="hours">10 hours</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats">
											<div class="xe-comment">
												<p class="day">Wednesday</p>
												<p class="hours">10 hours</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats grey">
											<div class="xe-comment">
												<p class="day">Thursday</p>
												<p class="hours">10 hours</p>
											</div>
										</div>
									</li>
									<li>
										<div class="mot_day_stats end">
											<div class="xe-comment">
												<p class="day">Friday</p>
												<p class="hours">10 hours</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 employee_whole">
						<div class="xe-widget xe-conversations basic_information">		
							<div class="xe-label basic_info grey">
								<p>Basic Information</p>
							</div>
						<div class="xe-body basic_info_body">
							<ul class="list-unstyled">
						<li>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p>Email</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$email_data=$data[0]['email'];
											if (!empty($email_data))
												{
													echo $email_data;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list grey">
								<div class="basic_info_title">
									<p>Phone</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$phone_data=$data[0]['phone_number'];
											if (!empty($phone_data))
												{
													echo $phone_data;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p>Skype</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$skype_data=$data[0]['skype'];
											if (!empty($skype_data))
												{
													echo $skype_data;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list grey">
								<div class="basic_info_title">
									<p>Url</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$url_data=$data[0]['url'];
											if (!empty($url_data))
												{
													echo $url_data;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
						<li>
							<div class="basic_info_list end">
								<div class="basic_info_title">
									<p>Fax</p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p>
										<?php 
											$fax_data=$data[0]['fax'];
											if (!empty($fax_data))
												{
													echo $fax_data;
												}
											else{
													echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
						</li>
					</ul>
				</div>
			</div>
						</div>
					</div>
				</div>
				<div class="col-md-8 tabs">
					<div class="fill">
						<div class="col-md-6 employee_left">
							<div class="xe-widget xe-counter">
								<div class="xe-icon">
									<img src="<?php echo url('assets/images/clock2 75x75png.png')?>">
								</div>
							<div class="xe-label">
								<strong class="num">470</strong>
								<span>Total number of Overtime hours</span>
							</div>
							</div>
						</div>
						<div class="col-md-6 employee_right">
							<div class="xe-widget xe-counter">
								<div class="xe-icon">
									<img src="<?php echo url('assets/images/clock2 75x75png.png')?>">
								</div>
							<div class="xe-label">
								<strong class="num">470</strong>
								<span>Total number of Overtime hours</span>
							</div>
							</div>
						</div>
						<div class="col-md-9 employee_left">
							<div class="xe-widget xe-status-update">
						<div class="xe-header">
							<p class="about_user">About <?php echo $data[0]['name'];?></p>
						</div>
						<div class="xe-body">
							<p class="user_info">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</div>
						</div>
						</div>
						<div class="col-md-3 col-xs-12 employee_right">
							<div class="xe-widget xe-vertical-counter xe-vertical-counter-white">
						<div class="xe-icon">
							<img src="<?php echo url('assets/images/clock2 75x75png.png')?>">
						</div>
						
						<div class="xe-label">
							<strong class="num">00</strong>
							<span>Day balance</span>
						</div>
					</div>
						</div>
						<div class="col-md-5 employee_left">
							<div class="xe-widget xe-conversations activity_body">
							<div class="activity grey">		
							<div class="xe-label activity_title">
								<p>Day Activities</p>
							</div>
							<p class="date">19 June 2015</p>
							</div>
						<div class="xe-body">
							<div class="time_left">
								<p class="medium">Offset Hours</p>
								<p class="grey large">8</p>
								<p class="break">Break     1hr</p>
							</div>
							<div class="time_right">
								<p class="medium center">At Office</p>
								<p class="para grey"><span class="activity_left">Checked In</span><span class="activity_right">10 AM</span></p>
								<p class="para"><span class="activity_left">Checked Out</span><span class="activity_right">8 PM</span></p>
								<p class="para break grey"><span class="activity_left">Break</span><span class="activity_right">1 hour</span></p>
							</div>
						</div>
							</div>
						</div>
						<div class="col-md-5 employee_center">
							<div class="xe-widget xe-conversations address_information">		
								<div class="xe-label address_info grey">
									<p>Address</p>
								</div>
								<div class="xe-body address_info_body">
									<ul class="list-unstyled">
										<li>
										<div class="address_info_list">
											<div class="basic_info_title">
												<p>Flat no</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>#007</p>
												</a>
											</div>	
										</div>
										</li>
										<li>
										<div class="address_info_list grey">
											<div class="basic_info_title">
												<p>Street</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>Domlur</p>
												</a>
											</div>	
										</div>
										</li>
										<li>
										<div class="address_info_list">
											<div class="basic_info_title">
												<p>City</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>Bangalore</p>
												</a>
											</div>	
										</div>
										</li>
										<li>
										<div class="address_info_list end grey">
											<div class="basic_info_title">
												<p>State</p>
											</div>
											<div class="address_info_value">
												<a href="#">
												<p>Karnataka</p>
												</a>
											</div>	
										</div>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-2 col-xs-12 employee_right">
							<div class="xe-widget xe-vertical-counter xe-vertical-counter-white monthly_balance">
						<div class="xe-icon">
							<img src="<?php echo url('assets/images/clock2 75x75png.png')?>">
						</div>
						
						<div class="xe-label">
							<strong class="num">00</strong>
							<span class="center">Monthly balance</span>
						</div>
					</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

		<script>
		$(".left-links li a").click(function(){
    	$(this).find('i').toggleClass('fa-indent fa-outdent')
});
	</script>
	 <?php  include 'admin_footer.php';?>
