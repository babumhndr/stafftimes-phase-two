<?php  include 'admin_header.php';?>
<?php include 'cms_links.php'; ?>
<div class="row" style="margin:0px">
	<div class="col-md-12">
		<select id="language" style=" float: right; ">
   			<option value="en">ENGLISH</option>
   			<option value="ge">DEUTSCH</option>
		</select>
	</div>
	<div class="col-md-12">
		<div class="col-sm-6">
			<form >
				<label >Employee Listing Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='employee_listing'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Placeholder for Type to search field</label><br />
					<textarea rows="2" cols="65" type="text" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_search'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Sort by Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_by'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Sort by First Option Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Sort by Second Option Text</label><br />
					<textarea rows="2" cols="65" type="text" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Active Tab text</label><br />
					<textarea rows="2" cols="65" type="text" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Active'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Pending Tab text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pending'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Archive Tab text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Archive'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >View Report Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_report'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Department Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='deparment'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				
				<label >Working Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Overtime Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Data not available text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

			</form>
		</div>
		<div class="col-sm-6" id="englishdiv">
			<form id="cms_form" enctype="multipart/form-data">
				<label >Employee Listing Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='employee_listing'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='employee_listing'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Placeholder for Type to search field</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_search'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_search'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sort by Title</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_by'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_by'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sort by First Option Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sort by Second Option Text</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Active Tab text</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Active'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Active'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Pending Tab text</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pending'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pending'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Archive Tab text</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Archive'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Archive'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >View Report Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_report'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_report'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Department Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='deparment'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='deparment'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Working Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Overtime Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Data not available text</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['_id'];}}?>">
				
				<input type="hidden" id="lang" name="languag">
				<input id="submit_button" type="button" value="Update" />
			</form>
		</div>



		<div class="col-sm-6" id="germandiv" style="display:none;">
			<form id="cms_form1" enctype="multipart/form-data">
				<label >Employee Listing Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='employee_listing'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='employee_listing'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Placeholder for Type to search field</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_search'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_search'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sort by Title</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_by'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_by'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sort by First Option Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sort by Second Option Text</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sort_option_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Active Tab text</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Active'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Active'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Pending Tab text</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pending'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pending'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Archive Tab text</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Archive'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Archive'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >View Report Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_report'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_report'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Department Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='deparment'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='deparment'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Working Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Overtime Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_hours'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Data not available text</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['_id'];}}?>">
				
				<input type="hidden" id="lang1" name="languag">
				<input id="submit_button1" type="button" value="Update"/>
			</form>
		</div>
	</div>
</div>
<script src="js/jquery.form.js"></script>
<script>
 $('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendListing',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminListingCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendListing',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminListingCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>
var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$(function () {
        $("#language").change(function () {
            if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
    });
</script>
<?php  include 'admin_footer.php';?>
<style>
hr {
border-top: 1px solid #a9a9a9;
}
</style>