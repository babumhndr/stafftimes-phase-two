	<!-- Bottom Scripts -->
	 <!-- for fetching time from frontend-->
<script type="text/javascript">
    //get the current unix time 
   $(function(){
    function getCurrentTime(){
   // Get the current time as Unix time
            var currentUnixTime = Math.round((new Date()).getTime() / 1000);
            return timeConverter(currentUnixTime);

   function timeConverter(UNIX_timestamp){
              var a = new Date(UNIX_timestamp * 1000);
              var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
              var year = a.getFullYear();
              var month = months[a.getMonth()];
              var date = ("0" + a.getDate()).slice(-2);
              var hour = a.getHours();
              var min = a.getMinutes();
              var sec = a.getSeconds();
              var time = date + '-' + month + '-' + year + ' ' + hour + ':' + min + ':' + sec ;
              return time;
            }

  }
  var res=getCurrentTime();
  $('#time').val(res);
   $('.time').val(res);
});
</script>
	<script src="<?php echo url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo url('assets/js/TweenMax.min.js')?>"></script>
	<script src="<?php echo url('assets/js/resizeable.js')?>"></script>
	<script src="<?php echo url('assets/js/joinable.js')?>"></script>
	<script src="<?php echo url('assets/js/xenon-api.js')?>"></script>
	<script src="<?php echo url('assets/js/xenon-toggles.js')?>"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>

	<script src="<?php echo url('assets/js/xenon-widgets.js')?>"></script>
	<script src="<?php echo url('assets/js/devexpress-web-14.1/js/globalize.min.js')?>"></script>
	<script src="<?php echo url('assets/js/devexpress-web-14.1/js/dx.chartjs.js')?>"></script>
	<script src="<?php echo url('assets/js/toastr/toastr.min.js')?>"></script>
  <script src="<?php echo url('assets/js/sweet-alert.js')?>"></script>
	<!-- JavaScripts initializations and stuff -->
	<script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
