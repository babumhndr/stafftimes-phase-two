<?php  include 'admin_header.php';?>
<?php include 'cms_links.php' ?>
<div class="row" style="margin:0px">
 <div class="col-md-12">
  <select id="language" style=" float: right; ">
        <option value="en">ENGLISH</option>
        <option value="ge">DEUTSCH</option>
    </select>
 </div>
 <div class="col-md-12">
  <div class="col-sm-6">
   <form >
    <label >Breadcrum Title</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employer'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company logo title</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_logo'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Upload button</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='upload'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Cancel button</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cancel'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Supscription details</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Subscription'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Basic information title</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Basic'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Edit Basic information title</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='editBasic'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company name</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyname'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company id</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyid'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company email id</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company phone number</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='phone'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company country</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='country'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company language</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='language'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company fax number</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fax'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company skype id</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='skype'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    

    <label >Company placeholder name</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_name'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company placeholder id</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_id'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company placeholder email</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_email'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company placeholder phone</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_phone'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company placeholder fax</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_fax'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company placeholder skype</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_skype'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

	<label >Change password</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='change_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company old password</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='old_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company new password</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='new_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Company retype password</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='retype_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />


    <label >Save button</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Submit button</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='submit_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    </form>
  </div>
  <div class="col-sm-6" id="englishdiv">
   <form id="cms_form" enctype="multipart/form-data">
   	<label >Breadcrum Title</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employer'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employer'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company logo title</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_logo'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_logo'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Upload button</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='upload'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='upload'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Cancel button</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cancel'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cancel'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Supscription details</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Subscription'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Subscription'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Basic information title</label><br />
    <textarea rows="2" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Basic'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Basic'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Edit Basic information title</label><br />
    <textarea rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='editBasic'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='editBasic'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company name</label><br />
    <textarea rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyname'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyname'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company id</label><br />
    <textarea rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyid'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyid'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company email id</label><br />
    <textarea rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company phone number</label><br />
    <textarea rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='phone'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='phone'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company country</label><br />
    <textarea rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='country'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='country'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company language</label><br />
    <textarea rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='language'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='language'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company fax number</label><br />
    <textarea rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fax'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fax'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company skype id</label><br />
    <textarea rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='skype'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='skype'){ echo $details['Translation'][$i]['_id'];}}?>">

    <label >Company placeholder name</label><br />
    <textarea rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_name'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_name'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder id</label><br />
    <textarea rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_id'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_id'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder email</label><br />
    <textarea rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_email'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_email'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder phone</label><br />
    <textarea rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_phone'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_phone'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder fax</label><br />
    <textarea rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_fax'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_fax'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder skype</label><br />
    <textarea rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_skype'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_skype'){ echo $details['Translation'][$i]['_id'];}}?>">
	<label >Change password</label><br />
    <textarea rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='change_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='change_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company old password</label><br />
    <textarea rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='old_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='old_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company new password</label><br />
    <textarea rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='new_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='new_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company retype password</label><br />
    <textarea rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='retype_password'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='retype_password'){ echo $details['Translation'][$i]['_id'];}}?>">

    <label >Save button</label><br />
    <textarea rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Submit button</label><br />
    <textarea rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='submit_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
	<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='submit_button'){ echo $details['Translation'][$i]['_id'];}}?>">
	<input type="hidden" id="lang" name="languag">
    <input id="submit_button" type="button" value="Update" />
   </form>
  </div>
  <div class="col-sm-6" id="germandiv" style="display:none;">
   <form id="cms_form1" enctype="multipart/form-data">
   	<label >Breadcrum Title</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employer'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employer'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company logo title</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_logo'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_logo'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Upload button</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='upload'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='upload'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Cancel button</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cancel'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='cancel'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Supscription details</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Subscription'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Subscription'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Basic information title</label><br />
    <textarea rows="2" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Basic'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Basic'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Edit Basic information title</label><br />
    <textarea rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='editBasic'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='editBasic'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company name</label><br />
    <textarea rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyname'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyname'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company id</label><br />
    <textarea rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyid'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='companyid'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company email id</label><br />
    <textarea rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='email'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company phone number</label><br />
    <textarea rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='phone'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='phone'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company country</label><br />
    <textarea rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='country'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='country'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company language</label><br />
    <textarea rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='language'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='language'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company fax number</label><br />
    <textarea rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fax'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fax'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company skype id</label><br />
    <textarea rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='skype'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='skype'){ echo $details['Translation'][$i]['_id'];}}?>">

    <label >Company placeholder name</label><br />
    <textarea rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_name'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_name'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder id</label><br />
    <textarea rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_id'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_id'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder email</label><br />
    <textarea rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_email'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_email'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder phone</label><br />
    <textarea rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_phone'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_phone'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder fax</label><br />
    <textarea rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_fax'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_fax'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company placeholder skype</label><br />
    <textarea rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_skype'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='placeholder_skype'){ echo $details['Translation'][$i]['_id'];}}?>">
	<label >Change password</label><br />
    <textarea rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='change_password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='change_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company old password</label><br />
    <textarea rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='old_password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='old_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company new password</label><br />
    <textarea rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='new_password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='new_password'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Company retype password</label><br />
    <textarea rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='retype_password'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='retype_password'){ echo $details['Translation'][$i]['_id'];}}?>">

    <label >Save button</label><br />
    <textarea rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Submit button</label><br />
    <textarea rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='submit_button'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
	<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='submit_button'){ echo $details['Translation'][$i]['_id'];}}?>">
	<input type="hidden" id="lang1" name="languag">
    <input id="submit_button1" type="button" value="Update" />
   </form>
  </div>
 </div>
</div>
<script src="js/jquery.form.js"></script>
<script>
                              
$('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendEmployer',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminEmployerCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendEmployer',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminEmployerCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>

var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$("#language").change(function () {
   if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
</script>
<?php  include 'admin_footer.php';?>