<?php  include 'admin_header.php';?>
<style>
 .panel .panel-body {
    color: #575757;
}
input[type="search"] {
    border: 1px solid #eee;
    padding: 5px;
}
.table.table-bordered > tbody > tr > td{
	font-size: 14px;
}
#myTable th {
    font-size: 11px;
    width: 74px !important;
}
#myTable td {
    font-size: 11px;
    width: 74px !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
	background: #fff;
	border-color:#3597D3; 
	color: #3597D3 !important; 
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{
	background: #3597D3;
	color: #fff!important; 
	border-color:#3597D3; 
}
.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
	background: #3597D3;
	color: #fff!important; 
	border-color:#3597D3; 
}
.dt-buttons
{
    width: 36px;
    text-align: center;
    padding: 5px;
    position: absolute;
    background: #f2f2f2;
    border: 1px solid;
}
</style>
 <script>
$(document).ready(function(){
    $('#myTable').DataTable( {
        dom: 'Bfrtip',
        'scrollX':true,
        buttons: [
        {
            extend: 'csv',
            title: 'Employee List',
         }
        ]
    } );
});
</script>
<div class="col-sm-12" style=" padding-top: 15px; padding-bottom: 15px; ">
		<!-- Basic Setup -->
			<div class="panel panel-default" style=" font-family: sans-serif;width: 100%;">
				<div class="panel-heading">
					<h3 class="panel-title">Employee List</h3>
<!-- 					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a href="#" data-toggle="remove">
							&times;
						</a>
					</div> -->
				</div>
				<div class="panel-body" style="padding-top: 25px;">
					
					<table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%" style="border-bottom-color: #eee;">
						<thead>
							<tr>
								<th>Account ID</th>
								<th>Company Name</th>
								<th>Email address</th>
								<th>Email verified status</th>
								<th>Payment Status</th>
								<th>Employee name</th>
								<th>Employee email addresss</th>
								<th>Status</th>
								<th>Subscription Start Date</th>
								<th>Subscription End Date</th>
								<th>Trial Start</th>
								<th>Trial End</th>
							</tr>
						</thead>
					
						<tbody>
						<?php
							for($i=0;$i<sizeof($data);$i++) 
								{	
									/*
									if($data[$i]['status'] == 'pending')
										{
											$status ='Pending';
										}
									else if($data[$i]['status'] == 'active')
										{
											$status ='Active';
										}
									else{
											$status ='Archive';
										}

									*/
									$subscriptionStart='--';
									$subscriptionEnd='--';
									if($data[$i]['company']['emailverified'] == true)
										{
											$emailverified ='Yes';
										}
									else 
										{
											$emailverified ='no';
										}
									if($data[$i]['employee']['payment_status'] == 'pending')
										{
											$paymentStatus ='Pending';
										}
									else if($data[$i]['employee']['payment_status'] == 'active')
										{
											$paymentStatus ='Active';
											$subscriptionStart=date_create($data[$i]['employee']['subscription_start_date']);
											$subscriptionStart=date_format($subscriptionStart,"d-m-Y");
											$subscriptionEnd=date_create($data[$i]['employee']['subscription_end_date']);
											$subscriptionEnd=date_format($subscriptionEnd,"d-m-Y");
										}
									$trailStart=date_create($data[$i]['company']['trail_start_date']);
									$trailStart=date_format($trailStart,"d-m-Y");
									$trailEnd=date_create($data[$i]['company']['trail_end_date']);
									$trailEnd=date_format($trailEnd,"d-m-Y");
									echo '<tr>
								          <td>'.$data[$i]['company']['company_unique_id'].'</td>
								          <td>'.$data[$i]['company']['company_name'].'</td>
								          <td>'.$data[$i]['company']['email'].'</td>
										  <td>'.$emailverified.'</td>
										  <td>'.$data[$i]['employee']['payment_status'].'</td>
										  <td>'.$data[$i]['employee']['name'].'</td>
										  <td>'.$data[$i]['employee']['email'].'</td>
										  <td>'.$paymentStatus.'</td>
										  <td>'.$subscriptionStart.'</td>
										  <td>'.$subscriptionEnd.'</td>
										  <td>'.$trailStart.'</td>
										  <td>'.$trailEnd.'</td>
							         	  </tr>';
								}
						?>
							<!-- <tr>
								<td>Tiger Nixon</td>
								<td>System Architect</td>
								<td>Edinburgh</td>
							</tr> -->
						</tbody>
					</table>
					
				</div>
			</div>	
</div>
<?php  include 'admin_footer.php';?>