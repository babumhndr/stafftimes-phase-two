<?php  include 'admin_header.php';?>
<?php include 'cms_links.php'; ?>
<script type="text/javascript">
var time_of_mot= '<?php if (!empty($details['general_setting']['response']['0']['time_style'])) {echo $details['general_setting']['response']['0']['time_style'];}?>';
	</script>
<div class="row" style="margin:0px">
	<div class="col-md-12">
		<select id="language" style=" float: right; ">
   			<option value="en">ENGLISH</option>
   			<option value="ge">DEUTSCH</option>
		</select>
	</div>
	<div class="col-md-12">
		<div class="col-sm-6">
			<form >
				<label >Breadcrum Settings</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >General Settings</label><br />
					<textarea rows="2" cols="65" type="text" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='general_settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Set working days</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Overtime handling</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='handling'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Set annual allowance</label><br />
					<textarea rows="2" cols="65" type="text" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='annual_allowance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				<label >Time Picker increments</label><br />
					<textarea rows="2" cols="65" type="text" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_picker'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Time Styles</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_style'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Fast Check-in</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fast_checkin'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Total Values format</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_value'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Set working hours</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Industrial Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='industrial'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Device region</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='device_region'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				
				<label >Show days</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='show_days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Work days offset</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='work_days_offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Break Default</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break_default'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Template</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Conditions 1 Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Days</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Conditions</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >From</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='from'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Factor</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='factor'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Condition 2 Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='condition_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				<label >OverTime for period</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_for_period'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Clear overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='clear_overtime_balance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Activity</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Click here to add Activity</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='click_here_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Activity with flat time on and reducer off not found</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_not_found'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Total in</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_in'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Go button</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='go'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >SAVE Button</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				<!-- tooltip cms section -->
				<label >Tooltip text for General Settings tab</label><br />
					<textarea type="text" rows="4" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_general_settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip text for Set Working Days tab</label><br />
					<textarea type="text" rows="4" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_working'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip text for Overtime Handling tab</label><br />
					<textarea type="text" rows="4" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_overtime_handling'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip text for Set Annual Allowance tab</label><br />
					<textarea type="text" rows="4" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_annual'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip first list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip second list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip third list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				<label >Monday</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='mon'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tuesday</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tues'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Wednesday</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='wed'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Thursday</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thur'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Friday</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fri'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Saturday</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Sunday</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sun'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				<label >Time Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Hours Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >No Condition Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='no_conditions'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Day for Formula</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='day'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Fomula text</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='formula'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Update Annual Allowances checkbox</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='update'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >AM/PM</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='am'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				<label >Threshold Hours</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='threshold'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Hourly Rate</label><br />
					<textarea type="text" rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourly_rate'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />






			</form>
		</div>
		<div class="col-sm-6" id="englishdiv">
			<form id="cms_form" enctype="multipart/form-data">
				<label >Breadcrum Settings</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='settings'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >General Settings</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='general_settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='general_settings'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Set working days</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Overtime handling</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='handling'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='handling'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Set annual allowance</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='annual_allowance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='annual_allowance'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Time Picker increments</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_picker'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_picker'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Time Styles</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_style'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_style'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Fast Check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fast_checkin'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fast_checkin'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Total Values format</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_value'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_value'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Set working hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Industrial Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='industrial'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='industrial'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Device region</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='device_region'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='device_region'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='show_days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='show_days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Work days offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='work_days_offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='work_days_offset'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break_default'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break_default'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Conditions 1 Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Days</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Conditions</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >From</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='from'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='from'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Factor</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='factor'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='factor'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Condition 2 Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='condition_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='condition_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>


				<label >OverTime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_for_period'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_for_period'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Clear overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='clear_overtime_balance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='clear_overtime_balance'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Click here to add Activity</label><br />
					<textarea type="text" rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='click_here_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='click_here_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity with flat time on and reducer off not found</label><br />
					<textarea type="text" rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_not_found'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_not_found'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Total in</label><br />
					<textarea type="text" rows="2" cols="65" name="key28"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_in'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id28" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_in'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Go button</label><br />
					<textarea type="text" rows="2" cols="65" name="key29"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='go'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id29" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='go'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >SAVE Button</label><br />
					<textarea type="text" rows="2" cols="65" name="key30"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id30" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<!-- tooltip cms section -->
				<label >Tooltip text for General Settings tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key31"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_general_settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id31" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_general_settings'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip text for Set Working Days tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key32"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_working'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id32" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_working'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip text for Overtime Handling tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key33"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_overtime_handling'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id33" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_overtime_handling'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip text for Set Annual Allowance tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key34"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_annual'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id34" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_annual'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Tooltip first list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" name="key35"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id35" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" name="key36"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id36" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" name="key37"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id37" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" name="key38"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id38" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" name="key39"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id39" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" name="key40"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id40" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key41"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id41" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key42"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id42" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key43"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id43" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" name="key44"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id44" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" name="key45"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id45" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" name="key46"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id46" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key47"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id47" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key48"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id48" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key49"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id49" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_three'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Tooltip first list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key50"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id50" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key51"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id51" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key52"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id52" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key53"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id53" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key54"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id54" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key55"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id55" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key56"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id56" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key57"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id57" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key58"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id58" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key59"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id59" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key60"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id60" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key61"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id61" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" name="key62"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id62" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" name="key63"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id63" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" name="key64"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id64" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" name="key65"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id65" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" name="key66"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id66" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" name="key67"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id67" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key68"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id68" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key69"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id69" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key70"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id70" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key71"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id71" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key72"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id72" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key73"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id73" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" name="key74"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id74" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" name="key75"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id75" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" name="key76"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id76" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" name="key77"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id77" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" name="key78"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id78" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" name="key79"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id79" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_three'){ echo $details['Translation'][$i]['_id'];}}?>">

					<label >Tooltip first list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" name="key96"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id96" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" name="key97"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id97" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" name="key98"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id98" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Monday</label><br />
					<textarea type="text" rows="1" cols="65" name="key80"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='mon'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id80" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='mon'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tuesday</label><br />
					<textarea type="text" rows="1" cols="65" name="key81"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tues'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id81" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tues'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Wednesday</label><br />
					<textarea type="text" rows="1" cols="65" name="key82"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='wed'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id82" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='wed'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Thursday</label><br />
					<textarea type="text" rows="1" cols="65" name="key83"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thur'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id83" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thur'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Friday</label><br />
					<textarea type="text" rows="1" cols="65" name="key84"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fri'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id84" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fri'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Saturday</label><br />
					<textarea type="text" rows="1" cols="65" name="key85"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id85" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sunday</label><br />
					<textarea type="text" rows="1" cols="65" name="key86"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sun'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id86" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sun'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Time Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" name="key87"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id87" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hours Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" name="key88"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id88" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >No Condition Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" name="key89"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='no_conditions'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id89" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='no_conditions'){ echo $details['Translation'][$i]['_id'];}}?>">


				<label >Day for Formula</label><br />
					<textarea type="text" rows="1" cols="65" name="key90"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='day'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id90" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='day'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Fomula text</label><br />
					<textarea type="text" rows="1" cols="65" name="key91"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='formula'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id91" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='formula'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Update Annual Allowances checkbox</label><br />
					<textarea type="text" rows="1" cols="65" name="key92"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='update'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id92" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='update'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >AM/PM</label><br />
					<textarea type="text" rows="1" cols="65" name="key93"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='am'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id93" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='am'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>
				<label >Threshold Hours</label><br />
					<textarea type="text" rows="1" cols="65" name="key94"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='threshold'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id94" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='threshold'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hourly Rate</label><br />
					<textarea type="text" rows="1" cols="65" name="key95"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourly_rate'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id95" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourly_rate'){ echo $details['Translation'][$i]['_id'];}}?>">





				<input type="hidden" id="lang" name="languag">
				<input id="submit_button" type="button" value="Update" />
			</form>
		</div>



		<div class="col-sm-6" id="germandiv" style="display:none;">
			<form id="cms_form1" enctype="multipart/form-data">
				<label >Breadcrum Settings</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='settings'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='settings'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >General Settings</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='general_settings'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='general_settings'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Set working days</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_days'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Overtime handling</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='handling'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='handling'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Set annual allowance</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='annual_allowance'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='annual_allowance'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Time Picker increments</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_picker'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_picker'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Time Styles</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_style'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time_style'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Fast Check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fast_checkin'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fast_checkin'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Total Values format</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_value'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_value'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Set working hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='working_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Industrial Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='industrial'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='industrial'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Device region</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='device_region'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='device_region'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='show_days'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='show_days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Work days offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='work_days_offset'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='work_days_offset'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break_default'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break_default'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Conditions 1 Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Days</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Conditions</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='conditions'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >From</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='from'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='from'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Factor</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='factor'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='factor'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Condition 2 Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='condition_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='condition_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>


				<label >OverTime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_for_period'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_for_period'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Clear overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='clear_overtime_balance'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='clear_overtime_balance'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Click here to add Activity</label><br />
					<textarea type="text" rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='click_here_activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='click_here_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity with flat time on and reducer off not found</label><br />
					<textarea type="text" rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_not_found'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_not_found'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Total in</label><br />
					<textarea type="text" rows="2" cols="65" name="key28"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_in'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id28" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_in'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Go button</label><br />
					<textarea type="text" rows="2" cols="65" name="key29"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='go'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id29" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='go'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >SAVE Button</label><br />
					<textarea type="text" rows="2" cols="65" name="key30"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id30" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save_button'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<!-- tooltip cms section -->
				<label >Tooltip text for General Settings tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key31"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_general_settings'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id31" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_general_settings'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip text for Set Working Days tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key32"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_working'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id32" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_working'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip text for Overtime Handling tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key33"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_overtime_handling'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id33" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_overtime_handling'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip text for Set Annual Allowance tab</label><br />
					<textarea type="text" rows="4" cols="65" name="key34"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_annual'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id34" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_set_annual'){ echo $details['Translation'][$i]['_id'];}}?>">
					
				<label >Tooltip first list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" name="key35"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id35" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" name="key36"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id36" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Time picker</label><br />
					<textarea type="text" rows="2" cols="65" name="key37"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id37" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timepicker_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" name="key38"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id38" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" name="key39"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id39" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Time style</label><br />
					<textarea type="text" rows="2" cols="65" name="key40"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id40" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_timestyle_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key41"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id41" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key42"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id42" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Fast check-in</label><br />
					<textarea type="text" rows="2" cols="65" name="key43"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id43" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_fastcheckin_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" name="key44"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id44" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" name="key45"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id45" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Total Value Format</label><br />
					<textarea type="text" rows="2" cols="65" name="key46"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id46" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalvalue_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key47"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id47" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key48"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id48" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Working Hours</label><br />
					<textarea type="text" rows="2" cols="65" name="key49"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id49" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_working_three'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Tooltip first list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key50"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id50" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key51"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id51" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Show days</label><br />
					<textarea type="text" rows="2" cols="65" name="key52"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id52" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_showdays_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key53"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id53" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key54"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id54" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Work Days Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key55"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id55" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_workdays_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key56"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id56" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key57"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id57" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Break Default</label><br />
					<textarea type="text" rows="2" cols="65" name="key58"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id58" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_break_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key59"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id59" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key60"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id60" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key61"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id61" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" name="key62"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id62" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" name="key63"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id63" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Condititon 1</label><br />
					<textarea type="text" rows="2" cols="65" name="key64"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id64" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition1_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" name="key65"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id65" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" name="key66"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id66" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Condition 2</label><br />
					<textarea type="text" rows="2" cols="65" name="key67"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id67" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_condition2_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key68"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id68" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key69"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id69" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Activity in Overtime for period</label><br />
					<textarea type="text" rows="2" cols="65" name="key70"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id70" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityovertime_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key71"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id71" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key72"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id72" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Activity in Clear Overtime balance</label><br />
					<textarea type="text" rows="2" cols="65" name="key73"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id73" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityclear_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" name="key74"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id74" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" name="key75"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id75" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Activity in Set Annual Allowance</label><br />
					<textarea type="text" rows="2" cols="65" name="key76"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id76" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activityannual_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip first list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" name="key77"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id77" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" name="key78"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id78" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for Total in tag</label><br />
					<textarea type="text" rows="2" cols="65" name="key79"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id79" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_totalin_three'){ echo $details['Translation'][$i]['_id'];}}?>">

					<label >Tooltip first list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" name="key96"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id96" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip second list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" name="key97"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id97" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip third list item for CTB</label><br />
					<textarea type="text" rows="2" cols="65" name="key98"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id98" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_ctb_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Monday</label><br />
					<textarea type="text" rows="1" cols="65" name="key80"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='mon'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id80" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='mon'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tuesday</label><br />
					<textarea type="text" rows="1" cols="65" name="key81"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tues'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id81" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tues'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Wednesday</label><br />
					<textarea type="text" rows="1" cols="65" name="key82"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='wed'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id82" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='wed'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Thursday</label><br />
					<textarea type="text" rows="1" cols="65" name="key83"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thur'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id83" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thur'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Friday</label><br />
					<textarea type="text" rows="1" cols="65" name="key84"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fri'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id84" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fri'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Saturday</label><br />
					<textarea type="text" rows="1" cols="65" name="key85"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sat'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id85" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sunday</label><br />
					<textarea type="text" rows="1" cols="65" name="key86"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sun'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id86" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sun'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>

				<label >Time Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" name="key87"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id87" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='time'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hours Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" name="key88"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id88" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >No Condition Dropdown Item</label><br />
					<textarea type="text" rows="1" cols="65" name="key89"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='no_conditions'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id89" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='no_conditions'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Day for Formula</label><br />
					<textarea type="text" rows="1" cols="65" name="key90"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='day'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id90" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='day'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Fomula text</label><br />
					<textarea type="text" rows="1" cols="65" name="key91"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='formula'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id91" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='formula'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Update Annual Allowances checkbox</label><br />
					<textarea type="text" rows="1" cols="65" name="key92"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='update'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id92" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='update'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >AM/PM</label><br />
					<textarea type="text" rows="1" cols="65" name="key93"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='am'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id93" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='am'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>
				<label >Threshold Hours</label><br />
					<textarea type="text" rows="1" cols="65" name="key94"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='threshold'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id94" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='threshold'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hourly Rate</label><br />
					<textarea type="text" rows="1" cols="65" name="key95"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourly_rate'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id95" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourly_rate'){ echo $details['Translation'][$i]['_id'];}}?>">



				<input type="hidden" id="lang1" name="languag">
				<input id="submit_button1" type="button" value="Update"/>
			</form>
		</div>
	</div>
</div>
<script src="js/jquery.form.js"></script>
<script>
 $('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendSettings',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminSettingsCms'); ?>';
  
                        });
            }	
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendSettings',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminSettingsCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>
var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$(function () {
        $("#language").change(function () {
            if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
    });
</script>
<?php  include 'admin_footer.php';?>
<style>
hr {
border-top: 1px solid #a9a9a9;
}
</style>