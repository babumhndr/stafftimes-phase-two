<?php  include 'admin_header.php';?>
<?php include 'cms_links.php'; ?>
<div class="row" style="margin:0px">
 <div class="col-md-12">
  <select id="language" style=" float: right; ">
   <option value="en">ENGLISH</option>
   <option value="ge">DEUTSCH</option>
  </select>
 </div>
 <div class="col-md-12">
    <h2 style="text-align:center;">You are currently editing the Header of the website.</h2>
 </div>
 <div class="col-md-12" style="padding-bottom:15px;">
  <div class="col-sm-6" >
   <form >
    <label >Home</label><br />
     <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='home'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Tour</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tour'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Faq</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='faq'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Blog</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='blog'){ echo $details['Translation'][$i]['translation_en'];}}?>  </textarea><br />
    <label >Login</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='login'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Register</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='register'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Contact us</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='contactus'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Pricing</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='price'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Terms and conditions</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='terms_conditions'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Privacy Policy</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='privacy'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
   </form>
  </div>
  <div class="col-sm-6" id="englishdiv">
   <form id="cms_form" enctype="multipart/form-data">
    <label >HOME</label><br />
    <textarea rows="1" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='home'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='home'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >TOUR</label><br />
    <textarea rows="1" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tour'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tour'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >FAQ</label><br />
    <textarea rows="1" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='faq'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='faq'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >BLOG</label><br />
    <textarea rows="1" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='blog'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='blog'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >LOGIN</label><br />
    <textarea rows="1" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='login'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='login'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >REGISTER</label><br />
    <textarea rows="1" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='register'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='register'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Contact us</label><br />
    <textarea rows="1" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='contactus'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='contactus'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Pricing</label><br />
    <textarea rows="1" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='price'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='price'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Terms and conditions</label><br />
    <textarea rows="1" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='terms_conditions'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='terms_conditions'){ echo $details['Translation'][$i]['_id'];}}?>"> 
    <label >Privacy Policy</label><br />
    <textarea rows="1" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='privacy'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
     <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='privacy'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang" name="languag">
   <input id="submit_button" type="button" value="Update" >
   </form>
  </div>
  <div class="col-sm-6" id="germandiv" style="display:none;">
   <form id="cms_form1" action="cmsSend" method="POST" enctype="multipart/form-data">
    <label >HOME</label><br />
    <textarea rows="1" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='home'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='home'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >TOUR</label><br />
    <textarea rows="1" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tour'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tour'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >FAQ</label><br />
    <textarea rows="1" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='faq'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='faq'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >BLOG</label><br />
    <textarea rows="1" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='blog'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='blog'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >LOGIN</label><br />
    <textarea rows="1" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='login'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='login'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >REGISTER</label><br />
    <textarea rows="1" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='register'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='register'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Contact us</label><br />
    <textarea rows="1" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='contactus'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='contactus'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Pricing</label><br />
    <textarea rows="1" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='price'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='price'){ echo $details['Translation'][$i]['_id'];}}?>">   
    <label >Terms and conditions</label><br />
    <textarea rows="1" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='terms_conditions'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='terms_conditions'){ echo $details['Translation'][$i]['_id'];}}?>"> 
    <label >Privacy Policy</label><br />
    <textarea rows="1" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='privacy'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
     <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='privacy'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang1" name="languag">
    <input id="submit_button1" type="button" value="Update" />
   </form>
  </div>
 </div>
</div>
<script src="js/jquery.form.js"></script>
<script>
                              
$('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSend',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminHeaderCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSend',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminHeaderCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>

var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$("#language").change(function () {
   if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
</script>
<?php  include 'admin_footer.php';?>