<?php  include 'admin_header.php';?>
<?php include 'cms_links.php'; ?>
<div class="row" style="margin:0px">
	<div class="col-md-12">
		<select id="language" style=" float: right; ">
   			<option value="en">ENGLISH</option>
   			<option value="ge">DEUTSCH</option>
		</select>
	</div>
	<div class="col-md-12">
		<div class="col-sm-6">
			<form >
				<label >Dashboard</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Manage Template</label><br />
					<textarea rows="2" cols="65" type="text" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Manage_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Company Templates Heading</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >My Overtime Templates Heading</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Add Text (button)</label><br />
					<textarea rows="2" cols="65" type="text" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Template Text (button)</label><br />
					<textarea rows="2" cols="65" type="text" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Add Template</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Template Name Placeholder</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template_name'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Offset Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				
				<label >Flat Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Amount Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Start Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >End Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='end'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Break Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />


				<label >Save Button Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip for Name of the Template One</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Name of the Template Two</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Name of the Template Three</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for My Template Tab</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Edit Template Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Edit_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >View Template Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='View_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Data Not Avialable Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for offset time</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Add template</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />



			</form>
		</div>
		<div class="col-sm-6" id="englishdiv">
			<form id="cms_form" enctype="multipart/form-data">
				<label >Dashboard</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Manage Template</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Manage_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Manage_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Company Templates Heading</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >My Overtime Templates Heading</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Text (button)</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Template Text (button)</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Template Name Placeholder</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template_name'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template_name'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Flat Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Amount Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Start Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >End Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='end'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='end'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Break Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Save Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Tooltip for Name of the Template One</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Template Two</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Template Three</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for My Template Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Edit Template Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Edit_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Edit_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >View Template Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='View_Template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='View_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Data Not Avialable Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for offset time</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_offset'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Tooltip for Add template</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_template'){ echo $details['Translation'][$i]['_id'];}}?>">	

				<input type="hidden" id="lang" name="languag">
				<input id="submit_button" type="button" value="Update" />
			</form>
		</div>



		<div class="col-sm-6" id="germandiv" style="display:none;">
			<form id="cms_form1" enctype="multipart/form-data">
				<label >Dashboard</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Manage Template</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Manage_Template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Manage_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Company Templates Heading</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >My Overtime Templates Heading</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='overtime_template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Text (button)</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Template Text (button)</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Template</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Template Name Placeholder</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template_name'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template_name'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Flat Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Amount Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Start Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >End Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='end'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='end'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Break Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='break'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Save Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['_id'];}}?>">
				


				<label >Tooltip for Name of the Template One</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Template Two</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Template Three</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_template_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for My Template Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Edit Template Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Edit_Template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Edit_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >View Template Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='View_Template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='View_Template'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Data Not Avialable Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for offset time</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_offset'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_offset'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Add template</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_template'){ echo $details['Translation'][$i]['_id'];}}?>">
					
				<input type="hidden" id="lang1" name="languag">
				<input id="submit_button1" type="button" value="Update"/>
			</form>
		</div>
	</div>
</div>
<script src="js/jquery.form.js"></script>
<script>
 $('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendTemplate',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminManageTemplateCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendTemplate',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminManageTemplateCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>
var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$(function () {
        $("#language").change(function () {
            if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
    });
</script>
<?php  include 'admin_footer.php';?>
<style>
hr {
border-top: 1px solid #a9a9a9;
}
</style>