<?php  include 'admin_header.php';?>
<link rel="stylesheet" href="<?php echo url('/assets/css/xenon-components.css')?>">
<?php include 'cms_links.php' ?>
 <script>
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>

<div class="row" style="margin:0px">
 <div class="col-md-12">
    <select id="language" style=" float: right; ">
        <option value="en">ENGLISH</option>
        <option value="ge">DEUTSCH</option>
    </select>
  </div>
 <div class="col-md-12">
  <div class="col-sm-6">
   <form >
    <label >Read More text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='read_more'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Show Less text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='show_less'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Comments text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='comments'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />

    <!-- <label >Blog One Title text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_one'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Blog One Para text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_one'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Blog Two Title text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_two'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Blog Two Para text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_two'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Blog Three Title text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_three'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Blog Three Para text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_three'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Blog Four Title text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_four'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Blog Four Para text</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_four'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br /> -->
   </form>
  </div>
  <div class="col-sm-6" id="englishdiv">
   <form id="cms_form" enctype="multipart/form-data">
    <label >Read More text</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='read_more'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='read_more'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Show Less text</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='show_less'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='show_less'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Comments text</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='comments'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='comments'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <!-- <label >Blog One Title text</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_one'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_one'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog One Para text</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_one'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_one'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Two Title text</label><br />
    <textarea rows="2" cols="65" name="key6"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_two'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_two'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Two Para text</label><br />
    <textarea rows="2" cols="65" name="key7"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_two'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_two'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Three Title text</label><br />
    <textarea rows="2" cols="65" name="key8"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_three'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_three'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Three Para text</label><br />
    <textarea rows="2" cols="65" name="key9"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_three'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_three'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Four Title text</label><br />
    <textarea rows="2" cols="65" name="key10"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_four'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_four'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Four Para text</label><br />
    <textarea rows="2" cols="65" name="key11"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_four'){ echo $result['details']['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id11" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_four'){ echo $result['details']['Translation'][$i]['_id'];}}?>"> -->
    <input type="hidden" id="lang" name="languag">
    <input id="submit_button" type="button" value="Update" />
   </form>
  </div>
  <div class="col-sm-6" id="germandiv" style="display:none;">
   <form id="cms_form1" enctype="multipart/form-data">
    <label >Read More text</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='read_more'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='read_more'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Show Less text</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='show_less'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='show_less'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Comments text</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='comments'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='comments'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <!-- <label >Blog One Title text</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_one'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_one'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog One Para text</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_one'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_one'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Two Title text</label><br />
    <textarea rows="2" cols="65" name="key6"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_two'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_two'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Two Para text</label><br />
    <textarea rows="2" cols="65" name="key7"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_two'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_two'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Three Title text</label><br />
    <textarea rows="2" cols="65" name="key8"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_three'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_three'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Three Para text</label><br />
    <textarea rows="2" cols="65" name="key9"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_three'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_three'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Four Title text</label><br />
    <textarea rows="2" cols="65" name="key10"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_four'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='title_four'){ echo $result['details']['Translation'][$i]['_id'];}}?>">
    <label >Blog Four Para text</label><br />
    <textarea rows="2" cols="65" name="key11"><?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_four'){ echo $result['details']['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id11" value="<?php for($i=0;$i<count($result['details']['Translation']);$i++){if($result['details']['Translation'][$i]['translation_key']=='para_four'){ echo $result['details']['Translation'][$i]['_id'];}}?>"> -->
    <input type="hidden" id="lang1" name="languag">
    <input id="submit_button1" type="button" value="Update" />
   </form>
  </div>
  

 
</div>
<div class="col-md-12 blog_body" >
    <div class="col-sm-12" style=" padding-top: 15px; " id="table">
      <div class="panel panel-default" style=" font-family: sans-serif; width: 100% !important; ">
        <div class="panel-heading">
          <h3 class="panel-title">Blog List</h3>
        </div>
        <div class="panel-body" style="padding-top: 25px;">
          
          <table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%" style="border-bottom-color: #eee;">
            <thead>
              <tr>
                <th>Blog title</th>
                <th>Link</th>
                <th>Action</th>
              </tr>
            </thead>
          <tbody>
            <?php 
                for($i=0;$i<sizeof($result['data']);$i++) 
                { 
                  echo '<tr>
                          <td>'.$result['data'][$i]['title_en'].'</td>
                      <td>'.$result['data'][$i]['link'].'</td>
                      <td><button type="button" id="edit'.$i.'" data-id="'.$result["data"][$i]["_id"].'" onclick="editBlog('.$i.');">Edit</button>
                      <button type="button" id="delete'.$i.'" data-id="'.$result["data"][$i]["_id"].'" onclick="deleteBlog('.$i.');">Delete</button>
                          </tr>';
                }
            ?>
            </tbody>
          </table>
          
        </div>
      </div>  
</div>
<div class="col-sm-12 " id="add_form" style=" padding-top: 15px; display:none;">
  <form type="multipart/form-data" id="uploadBlog">
    <label >Blog Title English</label><br />
        <textarea rows="3" name="title_en" style="width:100%;" required ></textarea><br /> 
      <label >Blog Body English</label><br />
      <textarea rows="4" name="body_en" style="width:100%;" required></textarea><br />
      <label >Blog Title German</label><br />
        <textarea rows="3" name="title_de" style="width:100%;" required ></textarea><br /> 
      <label >Blog Body German</label><br />
      <textarea rows="4" name="body_de" style="width:100%;" required></textarea><br />
      <label>Enter the link for this blog</label>
      <input type="url" name="link" style="width:100%;"><br/>
      <label>Select image to upload:</label>
      <input type="file" name="image" accept="image/*">
      <input type="hidden" name="created_on" id="created_on">
      <input type="hidden" name="created_on_format" id="created_on_format">
      <button type="button" style="margin-top:10px; margin-bottom:10px;" id="load_blog">Upload to blog</button>
  </form>

</div>
      
  </div>
  <div style="width:100%;float:left; text-align:center; padding-bottom: 15px; " id="new_blog">
    <button type="button"id="add">Add a new blog</button>
  </div>

 
</div>
</div>
</div>
<div class="modal fade" id="modal">
  <div class="modal-dialog custom-dialog">
    <div class="modal-header custom-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
      <h5 class="modal-title custom-title">Edit Topic</h5>
    </div>
    <div class="modal-content custom-content" id="addModal">
                       
    </div>
  </div>
</div>

<style>
textarea {
  width: 100%;
}
.custom-title {
    text-align: center;
    font-weight: 300;
    color: #fff;
    font-size: 15px;
}
.custom-header {
    background: #2392ec;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.custom-content {
    padding: 0px 30px 30px 30px !important;
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
}
.custom-body {
    padding: 20px 0px 5px 0px !important;
}
.custom-footer {
    padding: 0px !important;
    text-align: center;
    border-top: 0px;
}
.custom-dialog {
    width: 800px;
    margin: 105px auto;
}
.textarea_width
{
    height: 100px;
}
.fade {
        font-family: 'Open Sans';
    }
</style>
<script src="js/jquery.form.js"></script>
<script>
  $('#add').click(function(){
    $('#table').hide();
    $('#add_form').show();
    $('#new_blog').hide();
  });
</script>
<script>
function editBlog(id) {
  /*alert(id);*/
  var blog_id= $('#edit'+id).attr("data-id");
  /*alert(faq_id);*/
  $('#modal').modal('show', {backdrop: 'static'});
  var form_data= '<form type="multipart/form-data" id="form"><div class="modal-body custom-body"><div class="row"> <div class="col-md-12"><div class="form-group"><label >Blog Title English</label><br /><textarea rows="3" name="title_en" id="title_en" style="width:100%;" required ></textarea><br /> <label >Blog Body English</label><br /><textarea rows="4" class="textarea_width" name="body_en" id="body_en" style="width:100%;" required></textarea><br /><label >Blog Title German</label><br /><textarea rows="3" name="title_de" id="title_de" style="width:100%;" required ></textarea><br /> <label >Blog Body German</label><br /><textarea rows="4" class="textarea_width" class="" name="body_de" id="body_de" style="width:100%;" required></textarea><br /><label>Enter the link for this blog</label><input type="url" name="link" id="link" style="width:100%;"><br/><input type="hidden" id="blogId" name="blogId"><div id="image_div"></div></div></div></div><div class="modal-footer custom-footer"><div class="addbtn"><input type="button" id="save" onclick="updateBlog()" class="btn btn-info" value="Save"></div></div></div></form>';
 $('#addModal').html(form_data);
 $.ajax({
          type:"GET",
          url: "<?php echo url('editBlog');?>/"+blog_id,
          success: function(response) {
            console.log(response);
            var x = response['_id'];
            console.log(x);
            $("#blogId").val($("#blogId").html()+response['_id']);
            $("#title_en").val($("#title_en").html()+response['title_en']);
            $("#body_en").val($("#body_en").html()+response['body_en']);
            $("#title_de").val($("#title_de").html()+response['title_de']);
            $("#body_de").val($("#body_de").html()+response['body_de']);
            $("#link").val($("#link").html()+response['link']);
            /*console.log((response['blog_image']))*/
            if(response['blog_image'] == 'null') {
                var blog_without_image= '<input type="file" name="image" accept="image/*">';
                $('#image_div').append(blog_without_image); 
                
            }
            else {
                var blog_image= '<img src='+response['blog_image']+' width="50" height="50"><input type="file" name="image" accept="image/*"><input type="hidden" name="image1" value="'+response['blog_image']+'">';
                $('#image_div').append(blog_image);
            }
            
          } 
        });
}
</script>
<script >
$('#created_on').val(moment().format("YYYY-MM-DD 00:00:00"));
$('#created_on_format').val(moment(created_on).format('MMMM Do YYYY'));
function updateBlog() { 
  var id=$('#blogId').val();
  var title_en= $('#title_en').val();
  var body_en= $('#body_en').val();
  var title_de= $('#title_de').val();
  var body_de= $('#body_de').val();
  var link= $('#link').val();
  var created_on= moment().format("YYYY-MM-DD 00:00:00");
  var created_on_format=moment(created_on).format('MMMM Do YYYY');
  
  /*console.log(id);
  console.log(englishtopic);
  console.log(germantopic);*/
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        $('#form').ajaxSubmit({
          type:"POST",
          url: "<?php echo url('updateBlog');?>",
          data: $('#form').serialize(),
          cache: false,
          success: function(response) {
             console.log(response);
             if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminBlogCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
}
</script>
<script>
function deleteBlog(id) {
  var id= $('#delete'+id).attr("data-id");
  swal({   
      title: "Are you sure?", 
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
    function(isConfirm)
  {   
    if (isConfirm) {
      $.ajax({
          type:"GET",
          url: "<?php echo url('deleteBlog');?>/"+id,
          success: function(response) {
             console.log(response) 
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Deleted successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminBlogCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to delete! Try again.", "error")
            }
          } 
        });
    }
    else 
      {     
        swal("Cancelled", "The content is not deleted", "error");   
      } 
});
}
</script>

<script>
$('#load_blog').click(function() {
       swal({   
      title: "Are you sure?", 
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    },
    function(isConfirm)
  {   
    if (isConfirm) {   
        $('#uploadBlog').ajaxSubmit({ 
        type: "POST",
        url: "<?php echo url('uploadBlog');?>",
        data: $('#uploadBlog').serialize(),
        cache: false,
        success:function(response){
             console.log(response) 
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminBlogCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  }); 

}); 

</script>

<script>
 $('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendBlog',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminBlogCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendBlog',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminBlogCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>
var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$(function () {
        $("#language").change(function () {
            if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
    });
</script>
<?php  include 'admin_footer.php';?>