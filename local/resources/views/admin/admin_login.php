<!DOCTYPE html>
<html>
    <head>
        <title>StaffTimes Super Admin</title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo url('/assets/css/css.css')?>">
        <link rel="stylesheet" href="<?php echo url('css/font-awesome.min.css')?>">
        <link rel="stylesheet" href="<?php echo url('css/bootstrap.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo url('css/login.css')?>">
        <script src="<?php echo url('assets/js/jquery-1.11.1.min.js')?>"></script>
        <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-core.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-forms.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-components.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-skins.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/sweet-alert.css')?>">
    </head>
    <style type="text/css">
      .mot_login_form {
      background: rgba(139, 195, 74, 0.48)!important;
     }
     #sign_in {
    background-color: #4CAF50 !important;
    }
    </style>
    <body>
    <div>
    	<div class="row mot_login">
    		<div id="container" class="container">
    			<div class="mot_login_form">
    				<form method='POST' action="adminLogin">
    				<div class="col-md-12">
    				<p class="mot_login_heading">Staff Times</p>
    				<div class="right-inner-addon ">
    					<i class="fa fa-user"></i>
    					<input type="text" id="name" name="email" placeholder="Email address" autofocus="autofocus" required>
    				</div>
    				<div class="right-inner-addon ">
    					<i class="fa fa-lock"></i>
    				<input type="password" id="password" name="password" placeholder="Password" required>
    				</div>
  					<button type="submit" class="form-control" id="sign_in">Sign in</button>
  					<div class="login_cannot_access">
  						<label for="cant_access"><a href="#" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="cant_access_label">Cannot access your account?</a></label>
					</div>
                      <?php
                        $msg = '';
                        $msgDisplay = 'display:block';
                        if(Session::has('message')){
                            $msg = Session::get('message');
                        }
                        if($msg == ''){
                            $msgDisplay = 'display:none';
                        }
                    ?>
                    <p style="color:red;text-align: center;font-size: 12px;margin-top: -5px;"><?=$msg?></p>
					</form>
				</div>
    		</div>
    	</div>
    </div>

        <div class="modal fade" id="modal-6">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Reset Password</h4>
                </div>
                
                <div class="modal-body">
                
                    <form method="POST" action="forgotPassword" id="enter_email">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-group">
                                <label for="field-1" class="control-label">Enter your email id</label>
                                
                                <input type="text" class="form-control" id="field-1" name="email" placeholder="Email id" autofocus="autofocus" required>
                                <button type="submit" class="form-control" id="mail_send">SEND</button>
                            </div>  
                            
                        </div>
                    </div>
                    </form>
                 </div>   
                </div>
                </div>
                </div>
                <script src="js/jquery.form.js"></script>
                <script>
 (function() {
$('#enter_email').ajaxForm({
    beforeSend: function () {
        var email = $('#field-1').val();
        var message;
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    console.log(regex.test(email));
          var isValid = true;
             if(email == null ||email == '')
              {
                     swal({  
                         title: "Error", 
                         text: "Please fill email id",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });
                     isValid= false;
              }
              else if(regex.test(email) == false){
                isValid = false;
                swal("Error!", "Please check the Email address", "error");
            }
            if(isValid == false) {
                return false;
            }

    
 },
    success: function(data) {
        /*console.log(data);*/
 if(data.status=='success')
 {
  swal({  
                         title: "Success", 
                         text: data.response,   
                         type: "success",   
                         confirmButtonText : "Ok"
                        });

 }
 else if(data.status=='failure'){
swal({  
                         title: "Failure", 
                         text: data.response,   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });

 }
}
}); 

})();
</script>
        <script src="<?php echo url('js/bootstrap.js')?>"></script>
        <script src="<?php echo url('assets/js/TweenMax.min.js')?>"></script>
    <script src="<?php echo url('assets/js/resizeable.js')?>"></script>
    <script src="<?php echo url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo url('assets/js/joinable.js')?>"></script>
    <script src="<?php echo url('assets/js/xenon-api.js')?>"></script>
    <script src="<?php echo url('assets/js/xenon-toggles.js')?>"></script>
    <script src="<?php echo url('assets/js/sweet-alert.js')?>"></script>


    <script src="<?php echo url('assets/js/xenon-widgets.js')?>"></script>
    </body>
</html>


