<?php  include 'admin_header.php';?>
<?php include 'cms_links.php'; ?>
<div class="row" style="margin:0px">
	<div class="col-md-12">
		<select id="language" style=" float: right; ">
   			<option value="en">ENGLISH</option>
   			<option value="ge">DEUTSCH</option>
		</select>
	</div>
	<div class="col-md-12">
		<div class="col-sm-6">
			<form >
				<label >Main Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='title'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Sub title one</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Sub title two</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Steps title</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='takes'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Step One</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='first'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Steps Two</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='second'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Steps Three</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='third'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Title next to image</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='image_title'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Per Year Per User</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='per_year'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Free trail button</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='free_trail'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features one</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features one Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features two</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features two Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features three</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features three Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features four</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features four Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features five</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features five Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features six</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features six Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features seven</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features seven Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features eight</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features eight Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features nine</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features nine Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features ten</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features ten Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features eleven</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features eleven Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features twelve</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features twelve Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features thirteen</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features thirteen Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features fourteen</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features fourteen Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features fifteen</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features fifteen Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features sixteen</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features sixteen Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features seventeen</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Features seventeen Details</label><br />
					<textarea type="text" rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
			</form>
  		</div>
  		<div class="col-sm-6" id="englishdiv">
   		<form id="cms_form" enctype="multipart/form-data">
   			<label >Main Title</label><br />
				<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='title'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='title'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sub title one</label><br />
					<textarea type="text" rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sub title two</label><br />
					<textarea type="text" rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Steps title</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='takes'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='takes'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Step One</label><br />
					<textarea type="text" rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='first'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='first'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Steps Two</label><br />
					<textarea type="text" rows="2" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='second'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='second'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Steps Three</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='third'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='third'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Title next to image</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='image_title'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='image_title'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Per Year Per User</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='per_year'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='per_year'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Free trail button</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='free_trail'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='free_trail'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features one</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features one Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features two</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features two Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features three</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features three Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features four</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features four Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features five</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features five Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features six</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features six Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seven</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seven Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eight</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eight Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features nine</label><br />
					<textarea type="text" rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features nine Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key28"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id28" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features ten</label><br />
					<textarea type="text" rows="2" cols="65" name="key29"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id29" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features ten Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key30"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id30" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eleven</label><br />
					<textarea type="text" rows="2" cols="65" name="key31"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id31" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eleven Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key32"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id32" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features twelve</label><br />
					<textarea type="text" rows="2" cols="65" name="key33"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id33" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features twelve Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key34"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id34" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features thirteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key35"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id35" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features thirteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key36"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id36" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fourteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key37"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id37" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fourteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key38"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id38" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fifteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key39"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id39" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fifteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key40"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id40" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features sixteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key41"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id41" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features sixteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key42"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id42" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seventeen</label><br />
					<textarea type="text" rows="2" cols="65" name="key43"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id43" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seventeen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key44"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen_details'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id44" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<input type="hidden" id="lang" name="languag">
    		<input id="submit_button" type="button" value="Update" />
   		</form>
  		</div>
  		<div class="col-sm-6" id="germandiv" style="display:none;">
   			<form id="cms_form1" enctype="multipart/form-data">
   				<label >Main Title</label><br />
				<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='title'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
				<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='title'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sub title one</label><br />
					<textarea type="text" rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Sub title two</label><br />
					<textarea type="text" rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sub_title_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Steps title</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='takes'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='takes'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Step One</label><br />
					<textarea type="text" rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='first'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='first'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Steps Two</label><br />
					<textarea type="text" rows="2" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='second'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='second'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Steps Three</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='third'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='third'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Title next to image</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='image_title'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='image_title'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Per Year Per User</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='per_year'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='per_year'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Free trail button</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='free_trail'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='free_trail'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features one</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features one Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='one_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features two</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features two Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='two_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features three</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features three Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='three_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features four</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features four Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='four_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features five</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features five Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='five_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features six</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features six Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='six_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seven</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seven Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seven_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eight</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eight Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eight_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features nine</label><br />
					<textarea type="text" rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features nine Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key28"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id28" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='nine_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features ten</label><br />
					<textarea type="text" rows="2" cols="65" name="key29"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id29" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features ten Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key30"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id30" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='ten_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eleven</label><br />
					<textarea type="text" rows="2" cols="65" name="key31"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id31" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features eleven Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key32"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id32" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='eleven_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features twelve</label><br />
					<textarea type="text" rows="2" cols="65" name="key33"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id33" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features twelve Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key34"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id34" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='twelve_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features thirteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key35"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id35" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features thirteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key36"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id36" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='thirteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fourteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key37"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id37" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fourteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key38"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id38" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fourteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fifteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key39"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id39" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features fifteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key40"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id40" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='fifteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features sixteen</label><br />
					<textarea type="text" rows="2" cols="65" name="key41"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id41" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features sixteen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key42"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id42" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='sixteen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seventeen</label><br />
					<textarea type="text" rows="2" cols="65" name="key43"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id43" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Features seventeen Details</label><br />
					<textarea type="text" rows="2" cols="65" name="key44"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen_details'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id44" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='seventeen_details'){ echo $details['Translation'][$i]['_id'];}}?>">
				<input type="hidden" id="lang1" name="languag">
   <input id="submit_button1" type="button" value="Update" />
  </form>
 </div>
</div>
</div>
<script src="js/jquery.form.js"></script>
<script>
                              
$('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendPrice',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminPriceCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendPrice',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminPriceCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>

var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$("#language").change(function () {
   if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
</script>
<?php  include 'admin_footer.php';?>




				