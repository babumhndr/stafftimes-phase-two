<?php  include 'admin_header.php';?>
<?php include 'cms_links.php'; ?>
<div class="row" style="margin:0px">
	<div class="col-md-12">
		<select id="language" style=" float: right; ">
   			<option value="en">ENGLISH</option>
   			<option value="ge">DEUTSCH</option>
		</select>
	</div>
	<div class="col-md-12">
		<div class="col-sm-6">
			<form >
				<label >Dashboard</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Manage Activity</label><br />
					<textarea rows="2" cols="65" type="text" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='manage_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Add Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='modal_title'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Name of the Activity Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_name'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Against Offset</label><br />
					<textarea rows="2" cols="65" type="text" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Offset Count Text</label><br />
					<textarea rows="2" cols="65" type="text" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_count'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Offset No Count Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_nocount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Flat Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Own Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='own'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Amount Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				
				<label >Allowance Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Allowance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Hourly Rate Multiplier Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Hourly'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Flat Time Mode Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat_time_mode'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Pre Populated Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pre_populated'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<hr>

				<label >Company Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<!-- <label >MyOvertime Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='my_overtime_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br /> -->
				<label >Data Not Available Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Add Button Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Activity Button Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />


				<label >Amount Type</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount_type'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Lump Sum Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='lump_sum'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Hourly Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourl'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Offset Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='off'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Days Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Edit Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='edit'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >View Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Overtime Reducer Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_Reducerd'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Save Button Text</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

				<label >Tooltip for Name of the Activity One</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Name of the Activity Two</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Name of the Activity Three</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Against the Offset</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_count'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Flat Break Deduction</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_flat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Own Account</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_own'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Amount</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_amount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Allowance Bank</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_allownce'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Hourly Rate Multiplier</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_hourly'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Flat Time Mode</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_input'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Overtime Reducer</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_reduce'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
				<label >Tooltip for Add Activity</label><br />
					<textarea type="text" rows="2" cols="65" readonly> <?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />

			</form>
		</div>
		<div class="col-sm-6" id="englishdiv">
			<form id="cms_form" enctype="multipart/form-data">
				<label >Dashboard</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Manage Activity</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='manage_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='manage_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Activity Title</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='modal_title'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='modal_title'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Name of the Activity Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_name'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_name'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Against Offset</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset Count Text</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_count'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_count'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset No Count Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_nocount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_nocount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Flat Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Own Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='own'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='own'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Amount Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Allowance Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Allowance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Allowance'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hourly Rate Multiplier Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Hourly'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Hourly'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Flat Time Mode Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat_time_mode'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat_time_mode'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Pre Populated Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pre_populated'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pre_populated'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>


				<label >Company Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<!-- <label >MyOvertime Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='my_overtime_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='my_overtime_activity'){ echo $details['Translation'][$i]['_id'];}}?>"> -->
				<label >Data Not Available Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Amount Type</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount_type'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount_type'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Lump Sum Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='lump_sum'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='lump_sum'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hourly Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourl'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourl'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='off'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='off'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Days Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Edit Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='edit'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='edit'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >View Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Overtime Reducer Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_Reducerd'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_Reducerd'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Save Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key28"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id28" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Tooltip for Name of the Activity One</label><br />
					<textarea type="text" rows="2" cols="65" name="key29"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_one'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id29" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Activity Two</label><br />
					<textarea type="text" rows="2" cols="65" name="key30"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_two'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id30" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Activity Three</label><br />
					<textarea type="text" rows="2" cols="65" name="key31"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_three'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id31" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Against the Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key32"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_count'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id32" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_count'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Flat Break Deduction</label><br />
					<textarea type="text" rows="2" cols="65" name="key33"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_flat'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id33" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_flat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Own Account</label><br />
					<textarea type="text" rows="2" cols="65" name="key34"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_own'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id34" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_own'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Amount</label><br />
					<textarea type="text" rows="2" cols="65" name="key35"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_amount'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id35" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_amount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Allowance Bank</label><br />
					<textarea type="text" rows="2" cols="65" name="key36"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_allownce'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id36" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_allownce'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Hourly Rate Multiplier</label><br />
					<textarea type="text" rows="2" cols="65" name="key37"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_hourly'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id37" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_hourly'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Flat Time Mode</label><br />
					<textarea type="text" rows="2" cols="65" name="key38"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_input'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id38" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_input'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Overtime Reducer</label><br />
					<textarea type="text" rows="2" cols="65" name="key39"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_reduce'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id39" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_reduce'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key40"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id40" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Add Activity</label><br />
					<textarea type="text" rows="2" cols="65" name="key41"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
					<input type="hidden" name="id41" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_activity'){ echo $details['Translation'][$i]['_id'];}}?>">

				<input type="hidden" id="lang" name="languag">
				<input id="submit_button" type="button" value="Update" />
			</form>
		</div>



		<div class="col-sm-6" id="germandiv" style="display:none;">
			<form id="cms_form1" enctype="multipart/form-data">
				<label >Dashboard</label><br />
					<textarea type="text" rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Dashboard'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Manage Activity</label><br />
					<textarea rows="2" cols="65" type="text" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='manage_activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='manage_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Activity Title</label><br />
					<textarea rows="2" cols="65" type="text" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='modal_title'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='modal_title'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Name of the Activity Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_name'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity_name'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Against Offset</label><br />
					<textarea rows="2" cols="65" type="text" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset Count Text</label><br />
					<textarea rows="2" cols="65" type="text" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_count'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_count'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset No Count Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_nocount'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset_nocount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Flat Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Own Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='own'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='own'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Amount Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Allowance Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Allowance'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Allowance'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hourly Rate Multiplier Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Hourly'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Hourly'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Flat Time Mode Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat_time_mode'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='flat_time_mode'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Pre Populated Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pre_populated'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Pre_populated'){ echo $details['Translation'][$i]['_id'];}}?>">
				<hr>


				<label >Company Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='company_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<!-- <label >MyOvertime Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='my_overtime_activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='my_overtime_activity'){ echo $details['Translation'][$i]['_id'];}}?>"> -->
				<label >Data Not Available Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='data_not_available'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Add Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='add'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Activity Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Amount Type</label><br />
					<textarea type="text" rows="2" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount_type'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='amount_type'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Lump Sum Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='lump_sum'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='lump_sum'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hourly Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourl'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hourl'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Offset Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='off'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='off'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Hours Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='hours'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Days Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='days'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Edit Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key25"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='edit'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id25" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='edit'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >View Activity Title</label><br />
					<textarea type="text" rows="2" cols="65" name="key26"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id26" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='view_activity'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Overtime Reducer Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key27"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_Reducerd'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id27" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Overtime_Reducerd'){ echo $details['Translation'][$i]['_id'];}}?>">





				<label >Save Button Text</label><br />
					<textarea type="text" rows="2" cols="65" name="key28"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id28" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='save'){ echo $details['Translation'][$i]['_id'];}}?>">

				<label >Tooltip for Name of the Activity One</label><br />
					<textarea type="text" rows="2" cols="65" name="key29"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_one'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id29" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_one'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Activity Two</label><br />
					<textarea type="text" rows="2" cols="65" name="key30"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_two'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id30" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_two'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Name of the Activity Three</label><br />
					<textarea type="text" rows="2" cols="65" name="key31"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_three'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id31" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_activity_three'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Against the Offset</label><br />
					<textarea type="text" rows="2" cols="65" name="key32"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_count'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id32" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_count'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Flat Break Deduction</label><br />
					<textarea type="text" rows="2" cols="65" name="key33"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_flat'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id33" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_flat'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Own Account</label><br />
					<textarea type="text" rows="2" cols="65" name="key34"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_own'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id34" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_own'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Amount</label><br />
					<textarea type="text" rows="2" cols="65" name="key35"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_amount'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id35" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_amount'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Allowance Bank</label><br />
					<textarea type="text" rows="2" cols="65" name="key36"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_allownce'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id36" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_allownce'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Hourly Rate Multiplier</label><br />
					<textarea type="text" rows="2" cols="65" name="key37"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_hourly'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id37" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_hourly'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Flat Time Mode</label><br />
					<textarea type="text" rows="2" cols="65" name="key38"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_input'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id38" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_input'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Overtime Reducer</label><br />
					<textarea type="text" rows="2" cols="65" name="key39"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_reduce'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id39" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_reduce'){ echo $details['Translation'][$i]['_id'];}}?>">
				<label >Tooltip for Activity Tab</label><br />
					<textarea type="text" rows="2" cols="65" name="key40"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id40" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_tab'){ echo $details['Translation'][$i]['_id'];}}?>">
					<label >Tooltip for Add Activity</label><br />
					<textarea type="text" rows="2" cols="65" name="key41"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
					<input type="hidden" name="id41" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='tooltip_add_activity'){ echo $details['Translation'][$i]['_id'];}}?>">


				<input type="hidden" id="lang1" name="languag">
				<input id="submit_button1" type="button" value="Update"/>
			</form>
		</div>
	</div>
</div>
<script src="js/jquery.form.js"></script>
<script>
 $('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendActivity',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminManageActivityCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendActivity',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminManageActivityCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>
var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$(function () {
        $("#language").change(function () {
            if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
    });
</script>
<?php  include 'admin_footer.php';?>
<style>
hr {
border-top: 1px solid #a9a9a9;
}
</style>