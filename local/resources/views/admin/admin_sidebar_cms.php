<?php  include 'admin_header.php';?>
<?php include 'cms_links.php' ?>
<div class="row" style="margin:0px">
 <div class="col-md-12">
  <select id="language" style=" float: right; ">
        <option value="en">ENGLISH</option>
        <option value="ge">DEUTSCH</option>
    </select>
 </div>
 <div class="col-md-12">
  <div class="col-sm-6">
   <form >
    <label >Dashboard</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='dashboard'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Employee</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employee'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Add Employee</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Employee'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Employee Listing</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Listing'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Book Your Time</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Book_your_time'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Manage Template</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Manage Activity</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Backup & Restore</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Backup'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Settings</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Subscription</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans_Main'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Plans & Pricing</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Subscribed User</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='subscribed_user'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Setup guide Title</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_guide'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Welcome to Stafftimes</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='welcome_stafftimes'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Ready guide</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='prepared_guide'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Lear how to setup organization</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_organization'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Start tutorial</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tutorial'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Start tracking</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tracking'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Documentation</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='documentation'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Guide Timesheets</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_timesheets'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Faq Guide</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_faq'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Blog Guide</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_blog'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >User Role</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='features_user'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <label >Close Button</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_close'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />   
    <label >Planner Report</label><br />
    <textarea rows="1" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='planner_report'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />   
   </form>
  </div>
  <div class="col-sm-6" id="englishdiv">
   <form id="cms_form" enctype="multipart/form-data">
    <label >Dashboard</label><br />
    <textarea rows="1" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='dashboard'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='dashboard'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Employee</label><br />
    <textarea rows="1" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employee'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employee'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Add Employee</label><br />
    <textarea rows="1" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Employee'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Employee'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Employee Listing</label><br />
    <textarea rows="1" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Listing'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Listing'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Book Your Time</label><br />
    <textarea rows="1" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Book_your_time'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Book_your_time'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Manage Template</label><br />
    <textarea rows="1" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Manage Activity</label><br />
    <textarea rows="1" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Backup & Restore</label><br />
    <textarea rows="1" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Backup'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Backup'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Settings</label><br />
    <textarea rows="1" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Settings'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Settings'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Subscription</label><br />
    <textarea rows="1" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans_Main'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans_Main'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Plans & Pricing</label><br />
    <textarea rows="1" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans'){ echo $details['Translation'][$i]['_id'];}}?>">
     <label >Subscribed User</label><br />
    <textarea rows="1" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='subscribed_user'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='subscribed_user'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Setup guide Title</label><br />
    <textarea rows="1" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_guide'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_guide'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Welcome to Stafftimes</label><br />
    <textarea rows="1" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='welcome_stafftimes'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='welcome_stafftimes'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Ready guide</label><br />
    <textarea rows="1" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='prepared_guide'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='prepared_guide'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Lear how to setup organization</label><br />
    <textarea rows="1" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_organization'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_organization'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Start tutorial</label><br />
    <textarea rows="1" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tutorial'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tutorial'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Start tracking</label><br />
    <textarea rows="1" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tracking'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tracking'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Documentation</label><br />
    <textarea rows="1" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='documentation'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='documentation'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Guide Timesheets</label><br />
    <textarea rows="1" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_timesheets'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_timesheets'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Faq Guide</label><br />
    <textarea rows="1" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_faq'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_faq'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Blog Guide</label><br />
    <textarea rows="1" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_blog'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_blog'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >User Role</label><br />
    <textarea rows="1" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='features_user'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='features_user'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Close Button</label><br />
    <textarea rows="1" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_close'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_close'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Planner Report</label><br />
    <textarea rows="1" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='planner_report'){ echo $details['Translation'][$i]['translation_en'];}}?> </textarea><br />
    <input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='planner_report'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang" name="languag">
    <input id="submit_button" type="button" value="Update" />
   </form>
  </div>
  <div class="col-sm-6" id="germandiv" style="display:none;">
   <form id="cms_form1" enctype="multipart/form-data">
    <label >Dashboard</label><br />
    <textarea rows="1" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='dashboard'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='dashboard'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Employee</label><br />
    <textarea rows="1" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employee'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Employee'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Add Employee</label><br />
    <textarea rows="1" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Employee'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Add_Employee'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Employee Listing</label><br />
    <textarea rows="1" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Listing'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Listing'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Book Your Time</label><br />
    <textarea rows="1" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Book_your_time'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Book_your_time'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Manage Template</label><br />
    <textarea rows="1" cols="65" name="key6"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id6" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='template'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Manage Activity</label><br />
    <textarea rows="1" cols="65" name="key7"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id7" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='activity'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Backup & Restore</label><br />
    <textarea rows="1" cols="65" name="key8"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Backup'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id8" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Backup'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Settings</label><br />
    <textarea rows="1" cols="65" name="key9"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Settings'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id9" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Settings'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Subscription</label><br />
    <textarea rows="1" cols="65" name="key24"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans_Main'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id24" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans_Main'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Plans & Pricing</label><br />
    <textarea rows="1" cols="65" name="key10"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id10" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='Plans'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Subscribed User</label><br />
    <textarea rows="1" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='subscribed_user'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='subscribed_user'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Setup guide Title</label><br />
    <textarea rows="1" cols="65" name="key11"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_guide'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id11" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_guide'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Welcome to Stafftimes</label><br />
    <textarea rows="1" cols="65" name="key12"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='welcome_stafftimes'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id12" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='welcome_stafftimes'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Ready guide</label><br />
    <textarea rows="1" cols="65" name="key13"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='prepared_guide'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id13" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='prepared_guide'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Lear how to setup organization</label><br />
    <textarea rows="1" cols="65" name="key14"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_organization'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id14" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='setup_organization'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Start tutorial</label><br />
    <textarea rows="1" cols="65" name="key15"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tutorial'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id15" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tutorial'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Start tracking</label><br />
    <textarea rows="1" cols="65" name="key16"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tracking'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id16" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='start_tracking'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Documentation</label><br />
    <textarea rows="1" cols="65" name="key17"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='documentation'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id17" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='documentation'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Guide Timesheets</label><br />
    <textarea rows="1" cols="65" name="key18"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_timesheets'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id18" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_timesheets'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Faq Guide</label><br />
    <textarea rows="1" cols="65" name="key19"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_faq'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id19" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_faq'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Blog Guide</label><br />
    <textarea rows="1" cols="65" name="key20"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_blog'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id20" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_blog'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >User Role</label><br />
    <textarea rows="1" cols="65" name="key21"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='features_user'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id21" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='features_user'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Close Button</label><br />
    <textarea rows="1" cols="65" name="key22"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_close'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id22" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='guide_close'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Planner Report</label><br />
    <textarea rows="1" cols="65" name="key23"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='planner_report'){ echo $details['Translation'][$i]['translation_de'];}}?> </textarea><br />
    <input type="hidden" name="id23" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='planner_report'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang1" name="languag">
    <input id="submit_button1" type="button" value="Update" />
   </form>
  </div>
 </div>
</div>
<script src="js/jquery.form.js"></script>
<script>
                              
$('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendSidebar',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminSidebarCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendSidebar',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminSidebarCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>

var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$("#language").change(function () {
   if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
</script>
<?php  include 'admin_footer.php';?>