<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="author" content="" />
	
	<title>StaffTimes Super Admin</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.css"/>
	<link rel="stylesheet" href="<?php echo url('/assets/css/css.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/hover.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/fonts/linecons/css/linecons.css')?>">
	<link rel="stylesheet" href="<?php echo url('css/fonts/fontawesome/css/font-awesome.css')?>">
	<link rel="stylesheet" href="<?php echo url('css/font-awesome.min.css')?>">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo url('/assets/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/bootstrap-social.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/xenon-core.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/xenon-forms.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/xenon-components.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/xenon-skins.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/header.css')?>">
	<link rel="stylesheet" href="<?php echo url('/assets/css/sweet-alert.css')?>">

	<script src="<?php echo url('assets/js/jquery-1.11.1.min.js')?>"></script>
	<script src="<?php echo url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo url('assets/js/date.format.js')?>"></script>
	<script src="<?php echo url('assets/js/geolocator.js')?>"></script>
    <script src="<?php echo url('assets/js/sweet-alert.js')?>"></script>
    <script src="<?php echo url('js/moment-with-locales.js')?>"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<style type="text/css">
	  ::-webkit-scrollbar {
width: 6px;
}
::-webkit-scrollbar-thumb {
background-color: #4CAF50;
}
::-webkit-scrollbar-track {
background-color: #D4D4D4;
}
	.notifications {
		left: auto;
		right: 0;
	}
	.user-info-navbar .user-info-menu > li .dropdown-menu.user-profile-menu {
		right: 0px;
	}
	.open > .notifications:after {
		content: '';
position: absolute;
border-style: solid;
border-width: 0 15px 15px;
border-color: #FFFFFF transparent;
display: block;
width: 0;
z-index: 1;
top: -15px;
left: auto;
right: 15px;
	}
	.open > .user-profile-menu:after {
		content: '';
position: absolute;
border-style: solid;
border-width: 0 15px 15px;
border-color: #FFFFFF transparent;
display: block;
width: 0;
z-index: 1;
top: -15px;
left: auto;
right: 15px;
	}
/*	.tooltip {
		font-family: 'Open Sans';
	}*/
.user-info-navbar {
    background-color: #14984c !important
}
.profile_time_list {
    border: 1px solid #4CAF50 !important;
    background-color: #4CAF50 !important;
}
.table.table-bordered > tbody > tr > td{
	font-size: 14px;
}
 .sidebar-menu .main-menu {
    padding-right: 16px;
  }
  .mot_dashboard_profile_inner {
    width: 250px;
    text-align: center;
    margin-top: auto;
    }
	</style>
	<script type="text/javascript">
	var userLang = navigator.language || navigator.userLanguage;
        	var userLanguages = userLang.substring(0, 2);
		</script>
		<?php
function time_in_24_hour_format($time){
$time_in_24_hour_format  = date("H:i", strtotime($time));
return $time_in_24_hour_format;
} 
function time_in_12_hour_format($time){
$time_in_12_hour_format  = date("g:i a", strtotime($time));
return $time_in_12_hour_format;
} 
?>
<body class="page-body">

	<!-- <div class="settings-pane">
			
		<a href="#" data-toggle="settings-pane" data-animate="true">
			&times;
		</a>
		
		<div class="settings-pane-inner">
			 
			<div class="row">
				
				<div class="col-md-4">
					
					<div class="user-info">
						
						<div class="user-image">
							<a href="extra-profile.html">
								<img src="assets/images/user-2.png" class="img-responsive img-circle" />
							</a>
						</div>
						
						<div class="user-details">
							
							<h3>
								<a href="extra-profile.html">John Smith</a>
								
								
								<span class="user-status is-online"></span>
							</h3>
							
							<p class="user-title">Web Developer</p>
							
							<div class="user-links">
								<a href="extra-profile.html" class="btn btn-primary">Edit Profile</a>
								<a href="extra-profile.html" class="btn btn-success">Upgrade</a>
							</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div class="col-md-8 link-blocks-env">
					
					<div class="links-block left-sep">
						<h4>
							<span>Notifications</span>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk1" />
								<label for="sp-chk1">Messages</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk2" />
								<label for="sp-chk2">Events</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk3" />
								<label for="sp-chk3">Updates</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk4" />
								<label for="sp-chk4">Server Uptime</label>
							</li>
						</ul>
					</div>
					
					<div class="links-block left-sep">
						<h4>
							<a href="#">
								<span>Help Desk</span>
							</a>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Support Center
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Submit a Ticket
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Domains Protocol
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Terms of Service
								</a>
							</li>
						</ul>
					</div>
					
				</div>
				
			</div>
		
		</div>
		
	</div> -->
	
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			
		<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
		<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
		<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
		<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="#" class="logo-expanded">
							<img src="<?php echo url('image/webapp.png')?>" width="75" alt="" />
							
						</a>
						
						<a href="#" class="logo-collapsed">
							<img src="<?php echo url('image/stafftimes2.png')?>" alt="" style=" width: 53px; height: 32px; margin-left: -16px; margin-top: -4px; "/>
							<!-- <p class="logo">MOT</p> -->
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="dropdown">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success notif_mobile">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa fa-bars"></i>
						</a>
					</div>

					
					<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
					<!-- <div class="settings-icon">
						<a href="#" data-toggle="settings-pane" data-animate="true">
							<i class="linecons-cog"></i>
						</a>
					</div> -->
					
								
				</header>
						
				
						
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<?php echo url('admin_dashboard')?>">
							<img src="<?php echo url('image/user.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">Users</span>
						</a>
					</li>
					<!-- <li>
						<a href="<?php echo url('#')?>">
							<img src="<?php echo url('assets/images/two.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">Users</span>
						</a>
						<ul>
							<li>
								<a href="<?php echo url('add_employee')?>">
									<span class="title">Add new employee</span>
								</a>
							</li>
							<li>
								<a href="<?php echo url('listing')?>">
									<span class="title">Listing</span>
								</a>
							</li>
						</ul>
					</li> -->
					<!-- <li>
						<a href="#">
							<img src="<?php echo url('image/activity.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">Users</span>
						</a>
					</li> -->
					<li>
						<a href="#">
							<img src="<?php echo url('image/activity.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">Template/Activity</span>
						</a>
						<ul>
							<li>
								<a href="<?php echo url('adminManageTemplate')?>">
									<span class="title">Manage default template</span>
								</a>
							</li>
							<li>
								<a href="<?php echo url('adminManageActivity')?>">
									<span class="title">Manage default activity</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="<?php echo url('adminHeaderCms')?>">
							<img src="<?php echo url('image/add-language.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">CMS</span>
						</a>
					</li>
					<li>
						<a href="<?php echo url('adminCountryPriceCms')?>">
							<img src="<?php echo url('image/edit-pricing.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">Edit Pricing</span>
							<!-- <span class="label label-success pull-right">5</span> -->
						</a>
					</li>
					<li>
						<a href="<?php echo url('adminSettings')?>">
							<img src="<?php echo url('assets/images/icon (1).png')?>" class="a">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">Settings</span>
						</a>
					</li>
					<li>
						<a href="<?php echo url('allEmployeeListing')?>">
							<img src="<?php echo url('assets/images/icon (1).png')?>" class="a">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">All Employee Listing</span>
						</a>
					</li>
					<li>
						<a href="<?php echo url('adminBlogCms')?>">
							<img src="<?php echo url('image/add-language.png')?>">&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="title">Blog</span>
						</a>
					</li>
				</ul>
						
			</div>
			
		</div>
		
		<div class="main-content">
					
			<!-- User Info, Notifications and Menu Bar -->
			<nav class="navbar user-info-navbar" role="navigation">
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa fa-outdent" aria-hidden="true"></i>
						</a>
					</li>
					
					<!-- <li class="dropdown hover-line">
						<a href="#" data-toggle="dropdown">
							<i class="fa-envelope-o"></i>
							<span class="badge badge-green">15</span>
						</a>
							
						<ul class="dropdown-menu messages">
							<li>
									
								<ul class="dropdown-menu-list list-unstyled ps-scrollbar">
								
									<li class="active"> --><!-- "active" class means message is unread -->
										<!-- <a href="#">
											<span class="line">
												<strong>Luc Chartier</strong>
												<span class="light small">- yesterday</span>
											</span>
											
											<span class="line desc small">
												This ain’t our first item, it is the best of the rest.
											</span>
										</a>
									</li>
									
									<li class="active">
										<a href="#">
											<span class="line">
												<strong>Salma Nyberg</strong>
												<span class="light small">- 2 days ago</span>
											</span>
											
											<span class="line desc small">
												Oh he decisively impression attachment friendship so if everything. 
											</span>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span class="line">
												Hayden Cartwright
												<span class="light small">- a week ago</span>
											</span>
											
											<span class="line desc small">
												Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
											</span>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span class="line">
												Sandra Eberhardt
												<span class="light small">- 16 days ago</span>
											</span>
											
											<span class="line desc small">
												On so attention necessary at by provision otherwise existence direction.
											</span>
										</a>
									</li> -->
									
									<!-- Repeated -->
									
									<!-- <li class="active"> --><!-- "active" class means message is unread -->
										<!-- <a href="#">
											<span class="line">
												<strong>Luc Chartier</strong>
												<span class="light small">- yesterday</span>
											</span>
											
											<span class="line desc small">
												This ain’t our first item, it is the best of the rest.
											</span>
										</a>
									</li>
									
									<li class="active">
										<a href="#">
											<span class="line">
												<strong>Salma Nyberg</strong>
												<span class="light small">- 2 days ago</span>
											</span>
											
											<span class="line desc small">
												Oh he decisively impression attachment friendship so if everything. 
											</span>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span class="line">
												Hayden Cartwright
												<span class="light small">- a week ago</span>
											</span>
											
											<span class="line desc small">
												Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
											</span>
										</a>
									</li>
									
									<li>
										<a href="#">
											<span class="line">
												Sandra Eberhardt
												<span class="light small">- 16 days ago</span>
											</span>
											
											<span class="line desc small">
												On so attention necessary at by provision otherwise existence direction.
											</span>
										</a>
									</li>
									
								</ul>
								
							</li>
							
							<li class="external">
								<a href="blank-sidebar.html">
									<span>All Messages</span>
									<i class="fa-link-ext"></i>
								</a>
							</li>
						</ul>
					</li> -->
					
					
					
				</ul>
				<ul class="hidden-sm hidden-xs mot_dashboard_profile_time">
					<div class="mot_dashboard_profile_inner">
    				<ul class="profile_time_list">
    					<li class="current_stats"><i class="fa fa-calendar-o" aria-hidden="true">&nbsp&nbsp<span class="current_stats_font" id="date_now"></span></i></li>
    					<li class="current_stats" style="border: none !important;"><i class="fa fa-clock-o" aria-hidden="true">&nbsp&nbsp<span class="current_stats_font" id="time_now"></span></i></li>
    				</ul>
    				</div>
    			</ul>
				
				<!-- Right links for user info navbar -->
				<ul class="user-info-menu right-links list-inline list-unstyled">
					
					<!-- <li class="search-form"> --><!-- You can add "always-visible" to show make the search input visible -->
						
						<!-- <form method="get" action="extra-search.html">
							<input type="text" name="s" class="form-control search-field" placeholder="Type to search..." />
							
							<button type="submit" class="btn btn-link">
								<i class="linecons-search"></i>
							</button>
						</form>
						
					</li> -->
<!-- 					<li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="7 days remaining">
						<a href="#" >
							<img src="<?php echo url('assets/images/warning.png')?>" width="20">
						</a>
					</li> -->
<!-- 
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">
							<i class="fa-bell-o"></i>
							<span class="badge badge-purple notif">7</span>
						</a>
							
						<ul class="dropdown-menu notifications">
							<li class="top">
								<p class="small">
									<a href="#" class="pull-right">Mark all Read</a>
									You have <strong>7</strong> new notifications.
								</p>
							</li>
							
							<li>
								<ul class="dropdown-menu-list list-unstyled ps-scrollbar">
									<li class="active notification-success">
										<a href="#">
											<i class="fa-user"></i>
											
											<span class="line">
												<strong>John Doe registered</strong>
											</span>
											
											<span class="line small time">
												30 seconds ago
											</span>
										</a>
									</li>
									
									<li class="active notification-secondary">
										<a href="#">
											<i class="fa-lock"></i>
											
											<span class="line">
												<strong>John Doe added a new template</strong>
											</span>
											
											<span class="line small time">
												3 hours ago
											</span>
										</a>
									</li>
								</ul>
							</li>
							
							<li class="external">
								<a href="#">
									<span>View all notifications</span>
									<i class="fa-link-ext"></i>
								</a>
							</li>
						</ul>
					</li> -->
					
					<li class="dropdown user-profile">
						<a href="#" data-toggle="dropdown">
							<img src="<?php 
							$res=Auth::user();
							if (!empty($res))
							{
								echo $res['profile_image'];
							}
							else{
								echo "assets/images/dummy.png";
							}
							?>" alt="user-image" class="img-circle img-inline userpic-32" width="32"style=" height: 34px; width: 34px; " />
							<span class="user_name">
							<?php 
							$res=Auth::user();
							if (!empty($res))
							{
								echo $res['company_name']; 
							}
							else{
								echo 'User&nbsp';
							}
							?>
								<i class="fa fa-caret-down"></i>
							</span>
						</a>
						
						<ul class="dropdown-menu user-profile-menu list-unstyled">
							<!-- <li>
							
								<a href="<?php  $res=Auth::user(); echo url('profile/'.$res['id']); ?>">
									<i class="fa-user"></i>
									My Profile
								</a>
							</li> -->
							<li class="last">
								<a href="<?php echo url('admin_logout')?>">
									<i class="fa-lock"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
					
					<!-- <li>
						<a href="#" data-toggle="chat">
							<i class="fa-comments-o"></i>
						</a>
					</li>
					 -->
				</ul>
				
			</nav>
			<script>
					var now= new Date();
				document.getElementById("date_now").innerHTML = now.format('ddd dd mmm, yyyy');
			</script>
			<script>
var myVar = setInterval(myTimer ,1000);
function myTimer() {
    var d = new Date();
    document.getElementById("time_now").innerHTML = d.toLocaleTimeString();
}
</script>


</body>
</html>