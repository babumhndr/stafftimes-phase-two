<?php  include 'admin_header.php';?>
 <link rel="stylesheet" href="<?php echo url('/assets/css/admin_dashboard.css')?>">
 <style type="text/css">
 .panel .panel-body {
    color: #575757;
}
input[type="search"] {
    border: 1px solid #eee;
    padding: 5px;
}
.table.table-bordered > tbody > tr > td{
	font-size: 14px;
}
#myTable th{
	font-size: 14px;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current{
	background: #fff;
	border-color:#3597D3; 
	color: #3597D3 !important; 
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{
	background: #3597D3;
	color: #fff!important; 
	border-color:#3597D3; 
}
.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
	background: #3597D3;
	color: #fff!important; 
	border-color:#3597D3; 
}
 </style>
 <script>
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
<div class="col-sm-12" style=" padding-top: 15px; padding-bottom: 15px; ">
		<!-- Basic Setup -->
			<div class="panel panel-default" style=" font-family: sans-serif; ">
				<div class="panel-heading">
					<h3 class="panel-title">Company List</h3>
<!-- 					
					<div class="panel-options">
						<a href="#" data-toggle="panel">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a href="#" data-toggle="remove">
							&times;
						</a>
					</div> -->
				</div>
				<div class="panel-body" style="padding-top: 25px;">
					
					<table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%" style="border-bottom-color: #eee;">
						<thead>
							<tr>
								<th>Company name</th>
								<th>Email address</th>
								<th style="width: 140px;">Phone no.</th>
								<th>Email verification status</th>
								<th>Payment status</th>
								<th>Extend Trial</th>
								<!-- <th>Age</th>
								<th>Start date</th>
								<th>Salary</th> -->
							</tr>
						</thead>
					
						<!-- <tfoot>
							<tr>
								<th>Company name</th>
								<th>Email address</th>
								<th>Email verified</th>
								 <th>Office</th>
								<th>Age</th>
								<th>Start date</th>
								<th>Salary</th> 
							</tr>
						</tfoot> -->
					
						<tbody>
						<?php
						$extendbutton='';
						$phone = '';
							for($i=0;$i<sizeof($data);$i++) 
								{
									if ($data[$i]['phone'] == null) {
										$phone = "--";
									}else{
										$phone = $data[$i]['phone'];
									}
									if($data[$i]['emailverified'] == true)
										{
											$emailverified ='Verified';
										}
										else
										{
											$emailverified ='Not verified';
										}
									if($data[$i]['trail_status'] == 'pending')
									{
										$payment='Not verified';
									}
									else if ($data[$i]['trail_status'] == 'active' && $data[$i]['payment_status'] == 'pending') {
										$payment='Free trial';
									}
									else if ($data[$i]['payment_status'] == 'active') {
										$payment='Subscribed';
									}
									else
									{
										$payment='';
									}
									if ($data[$i]['trail_status'] == 'expired') {
										$extendbutton="<button type='button' id='trailButton' onclick=\"updateFreeTrail('".$data[$i]['_id']."');\">Extend Trial</button>";
									}
									else 
									{
										$extendbutton='';
									}
									echo '<tr>
								          <td><a href="companyProfile/'.$data[$i]['_id'].'"style=" color: #03A9F4; ">'.$data[$i]['company_name'].'</a></td>
										  <td>'.$data[$i]['email'].'</td>
										  <td>'.$phone.'</td>
										  <td>'.$emailverified.'</td>
										  <td>'.$payment.'</td>
										  <td>'.$extendbutton.'</td>
							         	  </tr>';
								}
						?>
							<!-- <tr>
								<td>Tiger Nixon</td>
								<td>System Architect</td>
								<td>Edinburgh</td>
							</tr> -->
						</tbody>
					</table>
					
				</div>
			</div>	
</div>
   <script>
		$( ".whole" ).hover(function() {
			$( ".whole" ).removeClass("on_load");
		});
	</script>
	<script>
		
		function updateFreeTrail(id) {

    		swal({   
    			title: "Are you sure?",   
    			text: "You want to extend the trial period?",   
    			type: "warning",   
    			showCancelButton: true,   
    			confirmButtonColor: "#DD6B55",   
    			confirmButtonText: "Yes",   
    			cancelButtonText: "Cancel",   
    			closeOnConfirm: false,   
    			closeOnCancel: false }, 
    			function(isConfirm){   
    				if (isConfirm) 
    					{
			$.ajax({
				url:"extendTrial/"+id,
				type:'post',
				success: function(response)
					{   
						
						if(response.status=='success')
 						{
  							swal({  
                        		title: "<?php echo trans('popup.success');?>", 
                         		text: "Trial period extended successfully",   
                         		type: "success",   
                         		confirmButtonText : "Ok"
                        	},
                        	function(){
								/* window.location.href = '<?php  $res=Auth::user(); echo url('profile/'.$res['id']); ?>';*/
                            	location.reload();
                        	});	
						}
 						else if(response.status=='failure'){
  							swal({  
                         		title: "<?php echo trans('popup.error_');?>", 
		                        text: "Something went wrong, please try again!",   
		                        type: "error",   
		                        confirmButtonText : "Ok"
                        	});
 						}
					}
			});
		}
		else 
    					{     
    						swal("<?php echo trans('popup.cancelled');?>", "trail period not extended", "error");   
    					} 
	});
}

	</script>
	 <?php  include 'admin_footer.php';?>
	
