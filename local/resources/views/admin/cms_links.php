<style>
   .report_variants {
    padding-left: 5px;
    padding-right: 5px;
   } 
   .text_fields {
    padding: 12px !important;
   }
   .first {
    padding-top: 20px;
   }
   .fields {
    margin-bottom: 15px !important;
   }
   .accordion_head {
        background: #FAFAFA;
    color: #7D7D7D !important;
    cursor: pointer;
    font-family: arial;
    font-size: 15px;
    margin: 0 0 1px 0;
    padding: 11px 9px;
    font-weight: lighter;
    border: 1px solid #F1F1F1;
}
.accordion_body {
    overflow: auto;
    border: 1px solid #F1F1F1;
    border-top: none;
    margin-bottom: 3px;
    color: #A39A9A;
}


</style>
<script>
$(document).ready(function(){
 //toggle the componenet with class accordion_body
 $(".accordion_head").click(function(){
  if ($('.accordion_body').is(':visible')) {
   $(".accordion_body").slideUp(600);
   
  }
  else
  {
  $(".accordion_body").slideDown(600); 
  
  }
 });
});
</script>

<div class="accordion_head" style="border-top-left-radius: 5px;border-top-right-radius: 5px;">
    <h2 style="text-align:center;">You can edit the content of the website. Choose the page which you want to edit.</h2>
</div>
<div class="accordion_body" >
<div class="col-md-12 col-sm-12 col-xs-12 first">
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants start hvr-pop" id="reports_1">
                        <a href="<?php echo url('adminHeaderCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>HEADER</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                        </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminFooterCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>FOOTER</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="reports">
                        <a href="<?php echo url('adminHomeCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>HOME</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot1.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                        </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminTourCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>TOUR</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="graph_report">
                            <a href="<?php echo url('adminFaqCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>FAQ</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot3.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop end" id="activity_graph_report">
                        <a href="<?php echo url('adminBlogCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>BLOG</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot5.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                        </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminLoginCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>LOGIN</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminPriceCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>PRICE</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminContactCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>CONTACT</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminSidebarCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>SIDEBAR</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminDashboardCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>DASHBOARD</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminEmployerCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>EMPLOYER PROFILE</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminAddEmployeeCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>ADD EMPLOYEE</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminListingCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>LISTING</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminManageTemplateCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>MANAGE TEMPLATE</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminManageActivityCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>MANAGE ACTIVITY</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminBackupCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>BACKUP AND RESTORE</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminSettingsCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>DEFAULT SETTINGS</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminPricingCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>PLANS AND PRICING</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminTileCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>REPORT TILES</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminReportsFirstCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>MY OVERTIME REPORT</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminStatCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>STATISTICS REPORT</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminReportsCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>DETAILED REPORT</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 report_variants hvr-pop" id="stats_reports">
                            <a href="<?php echo url('adminAllowanceCms')?>">
                            <div class="xe-widget xe-counter fields">
                                <div class="xe-label text_fields">
                                    <p>ALLOWANCE REPORT</p>
                                </div>
                                <!-- <div class="xe-icon icon_fields">
                                    <div class="split_icon" style="background: url('<?php echo url('assets/images/icons-mot2.png')?>') no-repeat;">
                                    </div>
                                </div> -->
                            </div>
                            </a>
                        </div>
                        
</div>
</div>
                    <script>
            $('document').ready(function(){
            var url=window.location.href;
           var last_part=url.substr(url.lastIndexOf('/') + 1);
           $('#'+last_part).addClass('hover_color');

            });
           
            </script>
