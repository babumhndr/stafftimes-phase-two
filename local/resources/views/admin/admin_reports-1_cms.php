<?php  include 'admin_header.php';?>
<?php include 'cms_links.php' ?>
<div class="row" style="margin:0px">
 <div class="col-md-12">
  <select id="language" style=" float: right; ">
        <option value="en">ENGLISH</option>
        <option value="ge">DEUTSCH</option>
    </select>
 </div>
 <div class="col-md-12">
  <div class="col-sm-6">
   <form >
    <label >Table Header Date</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='date'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Table Header Total hours worked</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Table Header Offset</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Table Header Balance</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='balance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <label >Table Header Total Overtime</label><br />
    <textarea rows="2" cols="65" readonly><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_overtime'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
   </form>
  </div>
  <div class="col-sm-6" id="englishdiv">
   <form id="cms_form" enctype="multipart/form-data">
    <label >Table Header Date</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='date'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='date'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Total hours worked</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_hours'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Offset</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Balance</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='balance'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='balance'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Total Overtime</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_overtime'){ echo $details['Translation'][$i]['translation_en'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_overtime'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang" name="languag">
    <input id="submit_button" type="button" value="Update" />
   </form>
  </div>
  <div class="col-sm-6" id="germandiv" style="display:none;">
   <form id="cms_form1" enctype="multipart/form-data">
    <label >Table Header Date</label><br />
    <textarea rows="2" cols="65" name="key1"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='date'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id1" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='date'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Total hours worked</label><br />
    <textarea rows="2" cols="65" name="key2"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_hours'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id2" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_hours'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Offset</label><br />
    <textarea rows="2" cols="65" name="key3"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id3" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='offset'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Balance</label><br />
    <textarea rows="2" cols="65" name="key4"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='balance'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id4" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='balance'){ echo $details['Translation'][$i]['_id'];}}?>">
    <label >Table Header Total Overtime</label><br />
    <textarea rows="2" cols="65" name="key5"><?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_overtime'){ echo $details['Translation'][$i]['translation_de'];}}?></textarea><br />
    <input type="hidden" name="id5" value="<?php for($i=0;$i<count($details['Translation']);$i++){if($details['Translation'][$i]['translation_key']=='total_overtime'){ echo $details['Translation'][$i]['_id'];}}?>">
    <input type="hidden" id="lang1" name="languag">
    <input id="submit_button1" type="button" value="Update" />
   </form>
  </div>
 </div>
</div>
<script src="js/jquery.form.js"></script>
<script>
                              
$('#submit_button').click(function() { 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendReportsFirst',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminReportsFirstCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
$('#submit_button1').click(function()
{ 
swal({   
      title: "Are you sure?",   
      text: "You will not be able to recover the content",   
      type: "warning",   
      showCancelButton: true,   
      confirmButtonColor: "#14984C",   
      confirmButtonText: "Yes, update it!",   
      cancelButtonText: "Cancel!",   
      closeOnConfirm: false,   
      closeOnCancel: false 
    }, 
function(isConfirm)
  {   
    if (isConfirm) {   
        var str = $('#cms_form1').serialize();
        $.ajax({
          type:"POST",
          url: 'cmsSendReportsFirst',
          data: str,
          success: function(response) {
              
            if(response.status == 'success')
            {
              swal({  
                         title: "Success!", 
                         text: "Content changed successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             window.location.href = '<?php echo url('adminReportsFirstCms'); ?>';
  
                        });
            }
            else
            {
              swal("Error!", "Not able to update! Try again.", "error")
            }
          } 
        });
      }      
    else 
      {     
        swal("Cancelled", "The content is unchanged", "error");   
      } 
  });
});
</script>
<script>

var lan = $( '#language' ).val();
$( '#lang' ).val(lan);
$("#language").change(function(){
  var lan = $(this).val();
  $( '#lang1' ).val(lan);
  $( '#lang' ).val(lan);
});
$("#language").change(function () {
   if ($(this).val() == "ge") {
                $("#germandiv").show();
                $("#englishdiv").hide();
            } else {
                $("#germandiv").hide();
                $("#englishdiv").show();
            }
        });
</script>
<?php  include 'admin_footer.php';?>