<?php 
$metaTitle=$data['title'];
$metaUrl=$data['url'];
$metaDescription=$data['description'];
$metaKeyword=$data['keyword'];
?>
<?php  include 'layout/main_page_header.php';?>
  <link rel="stylesheet" type="text/css" href="css/landing_page.css">
  <link rel="stylesheet" href="<?php echo url('css/selectize.css')?>">
<!-- smooth scrolling script -->
<!-- <script type="text/javascript">
  $(function () {

$('a[href^="#"]').click(function(event) {
var id = $(this).attr("href");
var offset = 20;
var target = $(id).offset().top - offset;

$('html, body').animate({scrollTop:target}, 1000);
event.preventDefault();
});

});

</script> -->
<style >
.tier-alert.dark-bg {
    background-color: transparent;
}
.dark-bg {
    background-color: #fff;
    float: left;
    width: 100%;
    height: auto;
}
.tier-alert.dark-bg .main {
    background: #fff;
}
@media screen and (min-width: 668px){
.tier-alert .text-container {
    padding: 0 6px;
}
}
.tier-alert .text-container {
    max-width: 90%;
    width: 100%;
    margin: 15px auto 15px auto;
    border: 0;
    font-size: 1em;
    padding: 0 20px;
}
@media (min-device-width: 1280px) {
.tier-alert .main {
    right: 0;
}
.tier-alert .main {
    margin: 0;
    margin-left: 0;
    max-width: 100%;
    width: 100%;
    float: left;
    transition: min-height .2s ease-out;
}
}
@media (min-width: 1024px) {
.main {
    position: relative;
    left: auto;
    float: none;
    width: 100%;
    margin-left: auto;
    margin: 0 auto;
}
.main {
    float: left;
    width: 100%;
}

.tier-alert * {
    box-sizing: border-box;
}
}
@media (min-device-width: 1280px) {
.tier-alert {
    max-width: 99%;
}
.tier-alert {
    background: transparent;
    box-sizing: border-box;
    position: fixed;
    bottom: 0;
    width: 100%;
    padding: 0 10px;
    text-align: center;
    z-index: 10999;
    font-size: 16px;
    display: none;
}
}
.tier-alert.dark-bg h2, .tier-alert.dark-bg p {
    color: #474747;
}
@media (min-width: 481px) {
.tier-alert .text-container h2 {
    margin-bottom: 10px;
}
.tier-alert .text-container h2 {
    font-family: 'Gotham SSm A','Gotham SSm B',"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: 200;
    font-style: normal;
    width: auto;
    margin-bottom: 5px;
    font-size: 1.4em;
    text-transform: none;
    max-width: 100%;
}
}
@media screen and (min-width: 0) and (min-width: 769px) {
h1.headline, h1.headline-with-sub, h2.headline, h2.headline-with-sub, h3.headline, h3.headline-with-sub {
    font-size: 16px;
}
}
@media (min-width: 0) {
h1.headline, h1.headline-with-sub, h2.headline, h2.headline-with-sub, h3.headline, h3.headline-with-sub {
    width: 320px;
    max-width: 100%;
    float: none;
    font-family: 'Gotham SSm A','Gotham SSm B',"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: 500;
    font-style: normal;
    font-size: 13px;
    color: #e61d2b;
}
}
@media screen and (min-width: 768px) {
.headline {
    margin: 0 auto 36px;
}
}
.headline {
    margin: 0 auto 30px;
    width: 200px;
    line-height: 1.1em;
    font-weight: bold;
}
.tier-alert a.alert-btn {
    font-family: 'Gotham SSm A','Gotham SSm B',"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: 500;
    font-style: normal;
    text-transform: uppercase;
    color: #fff;
    background-color: #b9b9b9;
    display: inline-block;
    text-align: center;
    border-bottom: 10px solid #999;
    margin: 0 3px;
    border-top-width: 0;
    padding: 10px;
    font-size: 14px;
    vertical-align: middle;
    transform: translateZ(0);
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -moz-osx-font-smoothing: grayscale;
    position: relative;
    overflow: hidden;
}
@media (min-width: 768px) {
.tier-alert a.alert-btn {
    margin: 0 auto;
}
.tier-alert a.alert-btn {
    background-color: #e61d2b;
    border: 0;
    border-bottom: 10px solid #bf1420;
    color: #fff;
    display: inline-block;
    font-family: 'Gotham SSm A','Gotham SSm B',"Helvetica Neue",Helvetica,Arial,sans-serif;
    font-weight: 500;
    font-style: normal;
    font-size: 14px;
    margin: 3px 5px;
    min-width: 200px;
    padding: 10px 3px;
}
}
.tier-alert.dark-bg a {
    color: #e61d2b;
}
.tier-alert .text-container a {
    color: #e61d2b;
}
.tier-alert a {
    transition: color .2s ease-out,background .2s ease-out;
}
.tier-alert a.alert-close {
    position: absolute;
    top: 9px;
    right: .9em;
    display: block;
    color: #434243;
    font-size: 25px;
    transition: color .2s ease-out,right .2s ease-out,top .2s ease-out;
}
[placeholder]:focus::-webkit-input-placeholder {
  color: transparent;
}
[placeholder]:focus::-webkit-input-placeholder {
  transition: opacity 0.2s 0.2s ease; 
  opacity: 0;
}


</style>
<script>jQuery(window).scroll(function(){if(jQuery(this).scrollTop()>0)
{jQuery('#scrollTeaser').fadeOut();}
else
{jQuery('#scrollTeaser').fadeIn();}});</script>
<script>
var $overlay;
var player;
var lastActivity=(new Date()).getTime();
var pulseReminders=3;var pulseScrollTeaserRunning=false;
function runPulseScrollTeaser(){if(!pulseScrollTeaserRunning){jQuery('#scrollTeaser').hide();return;}
var pulseSpeed=500;
jQuery('#scrollTeaser-down1').fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed);jQuery('#scrollTeaser-down2').delay(300).fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed);jQuery('#scrollTeaser-down3').delay(500).fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed,runPulseScrollTeaser);}
function startPulseScrollTeaser(){if(pulseScrollTeaserRunning==true)
return false;pulseScrollTeaserRunning=true;
jQuery('#scrollTeaser').show();runPulseScrollTeaser();return true;}
function stopPulseScrollTeaser(){pulseScrollTeaserRunning=false;}
function buildGallery(){if(carouselLive){$("#slideshow").jCarouselLite({btnNext:".next",btnPrev:".prev",circular:false,visible:5});$('.gallery-thumb').hover(function(){if($(this).hasClass('inactive')){$(this).stop().fadeTo(200,0);}
$(this).parent().addClass('active');},function(){if($(this).hasClass('inactive')){$(this).stop().fadeTo(200,1);}
$(this).parent().removeClass('active');});$('#content-switch').css('opacity','0');$('#content-switch, #content-bg').addClass('image1');$('.gallery-thumb').click(function(e){e.preventDefault();if($('.animating').length>0){return;}
$this=$(this);
var current=$(this).attr('href');$(this).addClass('animating');$('#content-switch').attr('class','').addClass(current).fadeTo(2000,1,function(){$('#content-bg').attr('class','').addClass(current)}).fadeTo(1,0,function(){$this.removeClass('animating');});});}else{$('#slideshowcontainer').css('display','none');}}
var KyronSite=(function(){var settings={},defaults={startAt:0,sectionCheckInterval:1000,clampWidth:1600,tracking:false},scrollAnimate,currentSection=-1,checkSectionLock=0,updateCount=0,loadProgress;var wHeight,wWidth,wCenter,outroComp,ratio;var $scrollBar,$scrollThumb,isScrolling,scrollBarHeight,scrollThumbHeight,thumbDelta,scrollThumbPosition,scrollPercent;var tooltipJustOpened=true;var $overlay;var player;var lastActivity=(new Date()).getTime();var pulseReminders=3;var pulseScrollTeaserRunning=false;var animationFunctions={absPosition:function(opts){var defaults={startLeft:0,startTop:0,endLeft:0,endTop:0},settings=$.extend(defaults,opts);this.startProperties['left']=settings.startLeft;this.startProperties['top']=settings.startTop;this.endProperties['left']=settings.endLeft;this.endProperties['top']=settings.endTop;this.startProperties['display']='block';this.endProperties['display']='none';},bottomLeftOutside:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);var portrait=false,elemHalfWidth=anim._elem.width()/2,
elemHalfHeight=anim._elem.height()/2,
adj=portrait?wWidth/2+ elemHalfWidth:adj=wHeight/2+ elemHalfHeight,tan=Math.sqrt(Math.pow(adj,2)+ Math.pow(adj,2));this.properties['top']=wCenter.top+ adj- elemHalfHeight+(portrait?settings.offset:0);this.properties['left']=wCenter.left- adj- elemHalfWidth+(portrait?0:settings.offset);},topRightOutside:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);var portrait=false,elemHalfWidth=anim._elem.width()/2,
elemHalfHeight=anim._elem.height()/2,
adj=portrait?wWidth/2+ elemHalfWidth:adj=wHeight/2+ elemHalfHeight,tan=Math.sqrt(Math.pow(adj,2)+ Math.pow(adj,2));this.properties['top']=wCenter.top- adj- elemHalfHeight+(portrait?settings.offset:0);this.properties['left']=wCenter.left+ adj- elemHalfWidth+(portrait?0:settings.offset);},leftOutside:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);this.properties['left']=-anim._elem.width()+ settings.offset;},leftOutsideClampWidth:function(anim,opts){this.properties['left']=-anim._elem.width()-(settings.clampWidth- wWidth)/2;
},leftOutsideClampWidth1:function(anim,opts){this.properties['left']=-anim._elem.width()-(settings.clampWidth- wWidth)/2;
},rightOutside:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);this.properties['left']=wWidth+ settings.offset;},rightOutsideClampWidth:function(anim,opts){this.properties['left']=wWidth+(settings.clampWidth- wWidth)/2;
},centerV:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);var elemHalfHeight=anim._elem.height()/2;
this.properties['top']=wCenter.top- elemHalfHeight+ settings.offset;},centerH:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);var elemHalfWidth=anim._elem.width()/2;
this.properties['left']=wCenter.left- elemHalfWidth+ settings.offset;},bottomOutside:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);this.properties['top']=wHeight+ settings.offset;},topOutside:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);this.properties['top']=-anim._elem.height()+ settings.offset;},rightInside:function(anim,opts){var defaults={offset:0},settings=$.extend(defaults,opts);this.properties['left']=-anim._elem.width()+(wWidth)+ settings.offset;},backgroundA:function(anim,opts){var defaults={offset:0,bgsize:0},settings=$.extend(defaults,opts);this.properties['background-position']=anim._elem.width()+ settings.offset-(1600-wWidth)/2;
},backgroundB:function(anim,opts){var defaults={offset:0,bgsize:0},settings=$.extend(defaults,opts);this.properties['background-position']=-opts.bgsize+ settings.offset+(1600-wWidth)/2;
}}
var t=0;var totalHeightPx=8000;var outroHeightPx=3000;var detailStart=470;var outroStart=2680;var horizontal1_detailStart=2500;var outroLength=0;var eventsDelta=0;var maxScroll=0;var sectionIndex=[{id:"#intro1",name:"intro1",tag:"#intro1",position:0,correct:false},{id:"#horizontalSection",name:"horizontal",tag:"#horizontal",position:detailStart,correct:false},{id:"#horizontalSection2",name:"horizontal2",tag:"#horizontal2",position:horizontal1_detailStart-410,correct:false},{id:"#horizontalSection1",name:"horizontal1",tag:"#horizontal1",position:horizontal1_detailStart+800,correct:false},{id:"#intro2",name:"intro2",tag:"#intro2",position:horizontal1_detailStart+2400,correct:false}];maxScroll=horizontal1_detailStart+2450;var i2delta=50;function checkSection(){if(checkSectionLock>0){checkSectionLock--;return;}
if((pulseReminders>0)&&((new Date()).getTime()-lastActivity>2000)){if(startPulseScrollTeaser()){pulseReminders--;}}
var scrollTop=scrollAnimate.getScrollTop();var extraFudgeFactor=Math.max(0,wHeight-1000);for(var i=0;i<sectionIndex.length;i++){var section=sectionIndex[i];var actualScrollTop=section.correct?scrollTop+ratio+extraFudgeFactor:scrollTop;if(actualScrollTop>section.position){var sectionEnd=(i==sectionIndex.length-1)?scrollAnimate.getMaxScroll()+ratio+extraFudgeFactor:sectionIndex[i+1].position;if(actualScrollTop<sectionEnd){if(i>3)
pulseReminders=0;enterSection(i);return;}}};}
function gotoSectionTag(sectionTag){for(var i=0;i<sectionIndex.length;i++){if(sectionIndex[i].tag===sectionTag){var newpos=sectionIndex[i].position+ 1;if(sectionIndex[i].correct==true)
newpos-=ratio;scrollAnimate.scrollTo(newpos);enterSection(i);return;}}}
function enterSection(index){if(currentSection==index){return;}
currentSection=index;$('#navigation > a').each(function(i,elm){if(i==index){$(elm).addClass('active');}else{$(elm).removeClass('active');}});_gaq.push(['_trackEvent','Section Entered',sectionIndex[index].name]);}
function initalizeNavigation(){var navContent="";for(var i=0;i<sectionIndex.length;i++){sectionIndex[i].name=$(sectionIndex[i].id).data("navigationTag");navContent+="<a class='nav-link' data-name='"+sectionIndex[i].name+"' title ='Scroll Down or Use Up/Down Arrow' href=\'"+sectionIndex[i].tag+"\'><div class='navigation_div'>"+sectionIndex[i].name+"</div></a>";}
$('#navigation').html(navContent);$('#navigation').after("<div id='navtip'></div><div id='navtipArrow'></div>");$('.nav-link').click(function(e){e.preventDefault();var hash=this.href.substring(this.href.indexOf('#'));checkSectionLock=3;pulseReminders=0;stopPulseScrollTeaser();gotoSectionTag(hash);return false;});if(!isTouch()){$('#navigation').hover(function(){$('#navtip').show();$('#navtipArrow').show();tooltipJustOpened=true;},function(){$('#navtip').hide();$('#navtipArrow').hide();tooltipJustOpened=true;});}
enterSection(0);}
function activateScrollBar(thumbHeight){scrollThumbHeight=thumbHeight;scrollThumbPosition=0;scrollPercent=0;isScrolling=false;$scrollBar=$('#scrollBar');$scrollBar.show();$scrollThumb=$('#scrollBar .thumb');$scrollThumb.css('height',scrollThumbHeight+"px");$scrollThumb.bind('mousedown',startScroll);}
function resizeScrollBar(){scrollBarHeight=wHeight-60;$scrollBar.css('height',scrollBarHeight+"px");setScrollBarPosition(scrollPercent);}
function startScroll(event){isScrolling=true;thumbDelta=scrollThumbPosition- event.pageY;$(document).bind('mousemove',scrollUpdate);$(document).bind('mouseup',endScroll);return false;}
function scrollUpdate(event){scrollThumbPosition=event.pageY+thumbDelta;scrollThumbPosition=Math.max(0,Math.min(scrollBarHeight-scrollThumbHeight,scrollThumbPosition));scrollPercent=scrollThumbPosition/(scrollBarHeight-scrollThumbHeight);scrollPercent=Math.max(0,Math.min(1,scrollPercent));scrollAnimate.scrollTo(maxScroll*scrollPercent);return false;}
function setScrollBarPosition(percent){scrollThumbPosition=(scrollBarHeight-scrollThumbHeight)*percent;$scrollThumb.css('top',scrollThumbPosition);}
function endScroll(event){isScrolling=false;$(document).unbind('mousemove',scrollUpdate);$(document).unbind('mouseup',endScroll);return false;}
function showOverlay(opacity,inSpeed,outSpeed,contentData){scrollAnimate.pause();$overlay.data('outSpeed',outSpeed);var contentClass;if(contentData!==undefined){$overlay.data('contentData',contentData);if(contentData.type=="bgimage"){contentClass=".imageOverlay";$overlay.after('<div class="overlayContent imageOverlay offscreen '+contentData.bgclass+'"><div id="overlayClose"><a href="#">Close</a></div></div>');$(contentClass).css({'width':contentData.width,'height':contentData.height});}else if(contentData.type=="video"){contentClass=".videoOverlay";if(!player){$overlay.after('<div class="overlayContent videoOverlay offscreen"><div id="overlayClose"><a href="#">Close</a></div><video id="player" class="video-js vjs-default-skin" controls poster="'+contentData.poster+'" width="'+contentData.width+'" height="'+contentData.height+'" preload="auto"><source type="video/mp4" src="'+contentData.url+'" /></video></div>');_V_("player",{},function(){player=this;player.load();setTimeout(function(){player.play();},100);});}else{player.src(contentData.url);player.size(contentData.width,contentData.height);player.load();setTimeout(function(){player.play();},100);}}else{$overlay.after('<div class="overlayContent"></div>');}
$('.overlayContent').css({'margin-left':-contentData.width/2,'margin-top':-contentData.height/2});}
$('#overlayClose').fadeTo(inSpeed,1).bind('click',removeOverlay);$overlay.fadeTo(inSpeed,opacity,function(){$overlay.bind('click',removeOverlay);$(contentClass).removeClass('offscreen');$('#overlayClose').show();});}
function removeOverlay(e){if(e)
e.preventDefault();$overlay.unbind('click',removeOverlay);$overlay.fadeOut($overlay.data('outSpeed')||100);$('#overlayClose').fadeOut($overlay.data('outSpeed')||100).unbind('click',removeOverlay);var contentData=$overlay.data('contentData');if(player){player.pause();if($.browser.mozilla){player.src("");}}
if(contentData){if(contentData.closeAction=="destroy"){$('.overlayContent:not(.offscreen)').remove();}}
$('.overlayContent').addClass('offscreen');$overlay.data('contentData',null);scrollAnimate.resume();}
function runPulseScrollTeaser(){if(!pulseScrollTeaserRunning){$('#scrollTeaser').hide();return;}
var pulseSpeed=500;$('#scrollTeaser-down1').fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed);$('#scrollTeaser-down2').delay(300).fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed);$('#scrollTeaser-down3').delay(500).fadeIn(pulseSpeed).delay(0).fadeOut(pulseSpeed,runPulseScrollTeaser);}
function startPulseScrollTeaser(){if(pulseScrollTeaserRunning==true)
return false;pulseScrollTeaserRunning=true;$('#scrollTeaser').show();runPulseScrollTeaser();return true;}
function stopPulseScrollTeaser(){pulseScrollTeaserRunning=false;}
function buildGallery(){if(carouselLive){$("#slideshow").jCarouselLite({btnNext:".next",btnPrev:".prev",circular:false,visible:5});$('.gallery-thumb').hover(function(){if($(this).hasClass('inactive')){$(this).stop().fadeTo(200,0);}
$(this).parent().addClass('active');},function(){if($(this).hasClass('inactive')){$(this).stop().fadeTo(200,1);}
$(this).parent().removeClass('active');});$('#content-switch').css('opacity','0');$('#content-switch, #content-bg').addClass('image1');$('.gallery-thumb').click(function(e){e.preventDefault();if($('.animating').length>0){return;}
$this=$(this);var current=$(this).attr('href');$(this).addClass('animating');$('#content-switch').attr('class','').addClass(current).fadeTo(2000,1,function(){$('#content-bg').attr('class','').addClass(current)}).fadeTo(1,0,function(){$this.removeClass('animating');});});}else{$('#slideshowcontainer').css('display','none');}}
function isTouch(){return'ontouchstart'in window;}
function track(a,b,c){if(settings.tracking==true){}}
function runIntro(){}
function initalizePage(){_V_.options.flash.swf="videojs/video-js.swf";$overlay=$('#overlay');$('.autoPlay').click(function(e){showOverlay(0.8,1000,100,{closeAction:"destroy",width:800,height:400});return false;});$('.findOutMore').click(function(e){scrollAnimate.resume();return false;});runIntro();buildGallery();}})();jQuery(document).ready(function(){window.siteAnimator=startPulseScrollTeaser();});
</script>
<!-- end smooth scrolling script -->
    </head>
    <body>
<!-- <div class="erropean-cookie-bar-wp" style="display: block;">
    <div class="alert-close cookie-listener">
        <section class="tier-alert gray-bg">
            <div class="main">
                <h2 class="headline">Cookie Notification</h2>
                <p>We use cookies to deliver the best possible web experience. By continuing and using the site, including by remaining on the landing page, you consent to the use of cookies. If you wish to disable them, please take a look at our <a href="/cookie-policy.html" class="alert-close" target="_blank">Cookies Policy</a>. Please note that parts of the site may not function correctly if you disable all cookies.</p>
                <a href="#" class="button regular alert-close btn-continue">Continue</a>
            </div>
            <a href="javascript:void(0);" class="alert-close btn-close">X</a>
        </section>
    </div>
</div> -->
<!-- <div id="cookie-alert-wrap"></div>
<section class="tier-alert dark-bg" id="cookie-alert">
        <div class="main main__author-fix">
            <div class="text-container text-container-accept">
                <h2 class="headline">COMMENT NOUS UTILISONS LES COOKIES ?</h2>
                <p></p><p style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;">En poursuivant votre navigation sans modifier le paramétrage des cookies, vous consentez à l’utilisation de ces cookies, notamment pour réaliser des statistiques de visite. Notez que certaines parties du site ne pourront pas fonctionner si certains cookies sont désactivés. Pour plus d’informations sur les cookies, et&nbsp; savoir comment les paramétrer,&nbsp;<a href="/legal/cookies.html">cliquez ici</a>.&nbsp;</p>
<p></p>
                <a href="javascript:void(0);" class="alert-btn alert-accept">CONTINUER</a>
                </div>
            </div>
        <a href="javascript:void(0);" class="alert-close"><span class="icon-cc-close"></span></a>
    </section> -->
    <div class="row mot_header">
    <div class="videobackground">
      <video playsinline autoplay muted loop poster="image/In-And-Out.jpg">
    <source src="image/In-And-Out.mp4" type="video/mp4">
    </video>
    </div>
       <?php  include 'layout/main_page_header_nav.php';?>
        <div class="col-md-12 content-part">
            <div class="col-md-3 hidden-sm hidden-xs"></div>
            <div class="col-md-6 content">
                <p class="content-heading lovelo_font" id="para1"><?php echo trans('welcome.para1'); ?></p>
                <p class="content-sub-heading" id="para2"><?php echo trans('welcome.para2'); ?></p>
                <div class="btn1">
                    <a href="#get_started"><button type="button" id="btn" ><?php echo trans('welcome.get_started_now'); ?></button></a>
                    <a class="take_a_tour" href="<?php echo url('tour');?>"><?php echo trans('translate.take_a_tour'); ?></a>
                </div>
              <!--   <div class="take-tour take-tutorial inv-taketutorial-video home-widget-video">
                    <span style="font-size:20px;color: #7f7d7b;" onclick="jQuery('#myModal').modal('show', {backdrop: 'static'});">
                        <img src="image/take_tutorial_video.gif" class="play-icon">
                            <?php echo trans('welcome.take_tour'); ?>
                        </span> 
                </div> -->
            <div class="col-md-3 hidden-sm hidden-xs"></div>   
        </div>
           <!--  <div class="col-md-5 laptop">
                <div class="top_image fadeInRight"></div>
            </div> -->
        </div>
      <a href="#down"> <div id="scrollTeaser" style="display: none;" class="scale">

        <div id="scrollTeaser-down1" style="display: none; opacity: 0.407208;"></div>
        <div id="scrollTeaser-down2" style="display: block; opacity: 0.549819;"></div>
        <div id="scrollTeaser-down3" style="display: block; opacity: 0.750988;"></div>
        </div></a>
    </div>
      <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo trans('welcome.take_tour'); ?></h4>
        </div>
        <div class="modal-body">
         <iframe width="100%" height="480" src="<?php echo trans('welcome.url'); ?>" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="myModal_signup" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo trans('welcome.quick_preview'); ?></h4>
        </div>
        <div class="modal-body">
         <iframe width="100%" height="480" src="<?php echo trans('welcome.signup_url'); ?>" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default close" data-dismiss="modal"><?php echo trans('translate.close'); ?></button>
        </div>
      </div>
      
    </div>
  </div>


    <div class="mobile_click">
    <div class="row mot_our_story" id="down">
        <div class="container mot_our_story_body">
            <div class="col-md-12 col-sm-12 col-xs-12 second-part">
                <div class="col-md-6 col-sm-12 col-xs-12 our-story-image-div" >
                    <!-- <img class="our-story-image" src="image/Untitled-1.jpg"> -->
                     <iframe width="100%" height="480" src="<?php echo trans('welcome.url'); ?>" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 our-story-content">
                    <p class="our-story-heading lovelo_font"><?php echo trans('welcome.our_story_head') ?></p>
                    <!-- <p style=" font-size: 21px; "><p class="our-story-para1"><?php echo trans('welcome.our_story_para1') ?> <a href="<?php echo url('tour'); ?>" style="text-decoration:underline;"><?php echo trans('translate.know_more') ?> >>></a></p> -->
                   <!--  <p class="our-story-para1"><?php echo trans('welcome.our_story_para1') ?></p>
                    <p class="our-story-para2"><?php echo trans('welcome.our_story_para2') ?></p>
                    <p>Best wishes,</p>
                    <p>Daniel Gubler</p>
                    <p>Founder &amp; CEO</p> -->
                     <!-- <p class="our-story-para1"><?php echo trans('welcome.our_story_para1') ?></p>
                      <p class="our-story-para1"><?php echo trans('welcome.our_story_para2') ?></p>
                       <p class="our-story-para1"><?php echo trans('welcome.our_story_para3') ?></p>
                    <p class="our-story-para1"><?php echo trans('welcome.our_story_para4') ?></p>
                     <p class="our-story-para1"><?php echo trans('welcome.our_story_para3') ?></p>
                    <p class="our-story-para1"><?php echo trans('welcome.our_story_para4') ?></p> -->
                    <div class="col-md-1 hidden-sm hidden-xs"></div> 
                    <div class="col-md-10 col-sm-12 col-xs-12">
                        <table class="features_listing">
                        <tr>
                        <td class="mobi"><i class="fa fa-check check1"></td>
                        <td><p class="listing"><?php echo trans('welcome.our_story_para1') ?></p></td>     
                        </tr>
                        <tr>
                        <td><i class="fa fa-check check1"></td>
                        <td><p class="listing"><?php echo trans('welcome.our_story_para2') ?></p></td>   
                        </tr>
                        <tr>
                        <td><i class="fa fa-check check1"></td>
                        <td><p class="listing"><?php echo trans('welcome.our_story_para3') ?></p></td>   
                        </tr>
                        <tr>
                        <td><i class="fa fa-check check1"></td>
                        <td><p class="listing"><?php echo trans('welcome.our_story_para4') ?> </p></td>   
                        </tr>
                        <tr>
                        <td><i class="fa fa-check check1"></td>
                        <td><p class="listing"><?php echo trans('welcome.our_story_para5') ?></p></td>   
                        </tr>
                        <tr>
                        <td><i class="fa fa-check check1"></td>
                        <td><p class="listing"><?php echo trans('welcome.our_story_para6') ?></p></td>   
                        </tr>
                        </table>
                        <a href="<?php echo url('tour'); ?>"><button type="button" id="btn_learn_more" onclick="#get_started"><?php echo trans('translate.know_more') ?></button></a>
                    <!--  <p class="our-story-para1"><a href="<?php echo url('tour'); ?>" style="text-decoration:underline;"><?php echo trans('translate.know_more') ?> >>></a></p> -->
                      </div>
                     <div class="col-md-1 hidden-sm hidden-xs"></div> 
                </div>   
            </div>
        </div>
    </div>
    <div class="row mot_our_services">
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12 third-part">
            <h1 class="services-heading lovelo_font"><?php echo trans('welcome.services_heading') ?></h1>
            <h3 class="services-sub-heading"><?php echo trans('welcome.services_sub_heading') ?></h3>
            <hr class="line-styling">
        </div>
        
            <div class="col-md-12 col-sm-12 col-xs-12 services">
                <div class="col-md-4 col-sm-12 col-xs-12 tile">
                   <div class="services-image">
                      <div class="icon" style="background: url('image/time-and-at.png') no-repeat;"></div>
                   </div> 
                   <div class="services-text">
                       <p class="service-text-heading lovelo_font"><?php echo trans('welcome.services1') ?></p>
                       <p class="service-text-sub-heading"><?php echo trans('welcome.services1_body') ?></p>
                   </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 tile">
                   <div class="services-image">
                       <div class="icon" style="background: url('image/Task-tracking.png') no-repeat;"></div>
                   </div> 
                   <div class="services-text">
                       <p class="service-text-heading lovelo_font"><?php echo trans('welcome.services2') ?></p>
                       <p class="service-text-sub-heading"><?php echo trans('welcome.services2_body') ?></p>
                   </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 tile">
                   <div class="services-image">
                       <div class="icon" style="background: url('image/hourely-rate.png') no-repeat;"></div>
                   </div> 
                   <div class="services-text">
                       <p class="service-text-heading lovelo_font"><?php echo trans('welcome.services3') ?></p>
                       <p class="service-text-sub-heading"><?php echo trans('welcome.services3_body') ?></p>
                   </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 tile">
                   <div class="services-image">
                       <div class="icon" style="background: url('image/overtime-rates.png') no-repeat;"></div>
                   </div> 
                   <div class="services-text">
                       <p class="service-text-heading lovelo_font"><?php echo trans('welcome.services4') ?></p>
                       <p class="service-text-sub-heading"><?php echo trans('welcome.services4_body') ?></p>
                   </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 tile">
                   <div class="services-image">
                       <div class="icon" style="background: url('image/staff.png') no-repeat;"></div>
                   </div> 
                   <div class="services-text">
                       <p class="service-text-heading lovelo_font"><?php echo trans('welcome.services5') ?></p>
                       <p class="service-text-sub-heading"><?php echo trans('welcome.services5_body') ?></p>
                   </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 tile">
                   <div class="services-image">
                       <div class="icon" style="background: url('image/report.png') no-repeat;"></div>
                   </div> 
                   <div class="services-text">
                       <p class="service-text-heading lovelo_font"><?php echo trans('welcome.services6') ?></p>
                       <p class="service-text-sub-heading"><?php echo trans('welcome.services6_body') ?></p>
                   </div>
                </div>
            </div>
        </div>
    </div>
<!--     <div class="row mot_statistics">
        <div class="container">
            <div class="col-md-12 fourth-part">
                <div class="col-md-3 tile2">
                    <div class="downloads-image">
                        <div class="next_icon" style="background: url('image/google-play.png') no-repeat;"></div>
                    </div>
                    <div class="downloads-image-description">
                        <p class="download-number">1000</p>
                        <p class="download-name"><?php echo trans('welcome.download_andriod') ?></p>
                    </div>
                </div>
                <div class="col-md-3 tile2">
                    <div class="downloads-image">
                        <div class="next_icon" style="background: url('image/app-store(2).png') no-repeat;"></div>
                    </div>
                    <div class="downloads-image-description">
                        <p class="download-number">1000</p>
                        <p class="download-name"><?php echo trans('welcome.download_ios') ?></p>
                    </div>
                </div>
                <div class="col-md-3 tile2">
                    <div class="downloads-image">
                        <div class="next_icon" style="background: url('image/plp.png') no-repeat;"></div>
                    </div>
                    <div class="downloads-image-description">
                        <p class="download-number">2000</p>
                        <p class="download-name"><?php echo trans('welcome.followers') ?></p>
                    </div>
                </div>
                <div class="col-md-3 tile2">
                    <div class="downloads-image">
                        <div class="next_icon" style="background: url('image/like.png') no-repeat;"></div>
                    </div>
                    <div class="downloads-image-description">
                        <p class="download-number">1000</p>
                        <p class="download-name"><?php echo trans('welcome.satisfied_clients') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <section class="testimonial-section testimonial-section-3">
        <div class="container-fluid">
            <h2 class="title lovelo_font"><?php echo trans('welcome.testimonial_heading') ?></h2>
            <div class="carousel-widget ctrl-1 owl1" data-margin="20" data-itemrange="0,1|420,1|600,2|768,2|992,2|1200,2" data-autoplay="true" data-nav="false" data-pager="true" data-center="true" id="owl1" style="opacity: 1;">
                <div class="owl-carousel owl-theme owl-center owl-loaded fs-equalize-element" style="opacity: 1;"> -->
                    
<!-- <div class="owl-stage-outer owl-height" style="height: 362px;">
    <div class="owl-stage" style="width: 4719px;"> -->
                    <!-- <div class="owl-item active center" style="width: 100%; margin-right: 20px;"><div class="item">
                        <div class="feedback-box feedback-box3">
                            <i class="quote fa fa-quote-right"></i>
                            <p class="feedback" style="height: 132px;">Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="person">
                                <img src="image/person2.jpg" alt="Person">
                                <div class="info">
                                    <strong>Oupsum dolor</strong>
                                    <em>Creative Director</em>
                                </div>
                            </div>
                            <i class="down-arrow fa fa-caret-down"></i>
                        </div> --><!-- /.feedback-box -->
                    <!-- </div></div>
                    <div class="owl-item active" style="width: 100%; margin-right: 20px;"><div class="item">
                        <div class="feedback-box feedback-box3">
                            <i class="quote fa fa-quote-right"></i>
                            <p class="feedback" style="height: 132px;">Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="person">
                                <img src="image/person2.jpg" alt="Person">
                                <div class="info">
                                    <strong>Oupsum dolor</strong>
                                    <em>Creative Director</em>
                                </div>
                            </div>
                            <i class="down-arrow fa fa-caret-down"></i>
                        </div> --><!-- /.feedback-box -->
                    <!-- </div></div>
                    <div class="owl-item active" style="width: 100%; margin-right: 20px;"><div class="item">
                        <div class="feedback-box feedback-box3">
                            <i class="quote fa fa-quote-right"></i>
                            <p class="feedback" style="height: 132px;">Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="person">
                                <img src="image/person2.jpg" alt="Person">
                                <div class="info">
                                    <strong>Oupsum dolor</strong>
                                    <em>Creative Director</em>
                                </div>
                            </div>
                            <i class="down-arrow fa fa-caret-down"></i>
                        </div> --><!-- /.feedback-box -->
                    <!-- </div></div>    
                    <div class="owl-item active" style="width: 100%; margin-right: 20px;"><div class="item">
                        <div class="feedback-box feedback-box3">
                            <i class="quote fa fa-quote-right"></i>
                            <p class="feedback" style="height: 132px;">Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="person">
                                <img src="image/person2.jpg" alt="Person">
                                <div class="info">
                                    <strong>Oupsum dolor</strong>
                                    <em>Creative Director</em>
                                </div>
                            </div>
                            <i class="down-arrow fa fa-caret-down"></i>
                        </div> --><!-- /.feedback-box -->
                    <!-- </div></div>    
                    <div class="owl-item active" style="width: 100%; margin-right: 20px;"><div class="item">
                        <div class="feedback-box feedback-box3">
                            <i class="quote fa fa-quote-right"></i>
                            <p class="feedback" style="height: 132px;">Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <div class="person">
                                <img src="image/person2.jpg" alt="Person">
                                <div class="info">
                                    <strong>Oupsum dolor</strong>
                                    <em>Creative Director</em>
                                </div>
                            </div>
                            <i class="down-arrow fa fa-caret-down"></i>
                        </div> --><!-- /.feedback-box -->
                    <!-- </div></div>
                    </div> --><!-- /.owl-carousel -->
            <!-- </div> --><!-- /.carousel-widget -->

        </div>
    </section>
    <div class="row mot_free_trail" id="get_started">
        <div class="container">
            <div class="col-md-12 fifth-part">
                <div class="col-md-5 free_trail_form_left">
                <img src="image/laptop.png">
<!--                     <p class="features_heading lovelo_font"><?php echo trans('welcome.features')?></p>
                    <hr class="line_styling_features">
                 <table class="features_listing">
                  <tr>
                   <td><i class="fa fa-check check1"></td>
                    <td><p class="listing">Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor</p></td>     
                  </tr>
                  <tr>
                  <td><i class="fa fa-check check1"></td>
                   <td><p class="listing">Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor</p></td>   
                  </tr>
                  <tr>
                  <td><i class="fa fa-check check1"></td>
                  <td><p class="listing">Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor</p></td>   
                  </tr>
                  <tr>
                  <td><i class="fa fa-check check1"></td>
                  <td><p class="listing">Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor</p></td>   
                  </tr>
                  <tr>
                  <td><i class="fa fa-check check1"></td>
                  <td><p class="listing">Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor</p></td>   
                  </tr>
                  </table> -->
                  <div class="trail-watch-a-video take-tutorial inv-taketutorial-video home-widget-video">
                    <span style="font-size:20px;color: #fff;text-decoration: underline;" onclick="jQuery('#myModal_signup').modal('show', {backdrop: 'static'});">

                            <?php echo trans('welcome.quick_preview'); ?>
                        </span> 
                </div>
                </div>
                <div class="col-md-7 free_trail_form_right" id="register">
                    <p class="features_heading lovelo_font"><?php echo trans('welcome.free_trail')?></p>
                    <hr class="line_styling_features">
                    
                    <form method="POST" action="<?php echo route('companyRegister')?>" id="form" >
                    <div class="contact-form_div">
                    <span>
                    <input type="text" class="form-control contact-form price" name="name" id="name" placeholder="<?php echo trans('welcome.palceholder_company_name')?>" autocomplete="off">
                    </span>
                    <span>
                    <input type="email" class="form-control contact-form price" name="email" id="email" placeholder="<?php echo trans('welcome.palceholder_email')?>" autocomplete="off">
                    </span>
                    <span>
                    <input type="password" class="form-control contact-form price" name="password" id="password" placeholder="<?php echo trans('welcome.palceholder_password')?>" autocomplete="off">
                    <i class="fa fa-question-circle" id="help" aria-hidden="true" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<?php echo trans('popup.password_minimum');?>"></i>
                    </span>
                    <span>
                    <input type="password" class="form-control contact-form price" name="confirm_password" id="confirm_password" placeholder="<?php echo trans('welcome.palceholder_cpassword')?>" autocomplete="off">
                    </span>
                    <input type="hidden" id="time" name="time"></input>
                    <span>
                    <select class="comp_name contact-form price" id="select-country" name="country" placeholder="<?php echo trans('translate.select_a_country')?>" style="font-size: 15px !important">
                    <option value="" selected></option> 
                    <?php
                        if (trans('translate.lang')=="en") 
                        {
                  echo "
                        <option value='US'>".trans('country_translation.US')."</option>
                        <option value='GB'>".trans('country_translation.GB')."</option>
                        <option value='AU'>".trans('country_translation.AU')."</option>
                        <optgroup label='----------------------------------'>
                        <option value='AF'>".trans('country_translation.AF')."</option>
                        <option value='AL'>".trans('country_translation.AL')."</option>
                        <option value='DZ'>".trans('country_translation.DZ')."</option>
                        <option value='AS'>".trans('country_translation.AS')."</option>
                        <option value='AD'>".trans('country_translation.AD')."</option>
                        <option value='AO'>".trans('country_translation.AO')."</option>
                        <option value='AI'>".trans('country_translation.AI')."</option>
                        <option value='AG'>".trans('country_translation.AG')."</option>
                        <option value='AR'>".trans('country_translation.AR')."</option>
                        <option value='AM'>".trans('country_translation.AM')."</option>
                        <option value='AW'>".trans('country_translation.AW')."</option>
                        <option value='AT'>".trans('country_translation.AT')."</option>
                        <option value='AZ'>".trans('country_translation.AZ')."</option>
                        <option value='BS'>".trans('country_translation.BS')."</option>
                        <option value='BH'>".trans('country_translation.BH')."</option>
                        <option value='BD'>".trans('country_translation.BD')."</option>
                        <option value='BB'>".trans('country_translation.BB')."</option>
                        <option value='BY'>".trans('country_translation.BY')."</option>
                        <option value='BE'>".trans('country_translation.BE')."</option>
                        <option value='BZ'>".trans('country_translation.BZ')."</option>
                        <option value='BJ'>".trans('country_translation.BJ')."</option>
                        <option value='BM'>".trans('country_translation.BM')."</option>
                        <option value='BT'>".trans('country_translation.BT')."</option>
                        <option value='BO'>".trans('country_translation.BO')."</option>
                        <option value='BA'>".trans('country_translation.BA')."</option>
                        <option value='BW'>".trans('country_translation.BW')."</option>
                        <option value='BR'>".trans('country_translation.BR')."</option>
                        <option value='VG'>".trans('country_translation.VG')."</option>
                        <option value='BN'>".trans('country_translation.BN')."</option>
                        <option value='BG'>".trans('country_translation.BG')."</option>
                        <option value='BF'>".trans('country_translation.BF')."</option>
                        <option value='MM'>".trans('country_translation.MM')."</option>
                        <option value='BI'>".trans('country_translation.BI')."</option>
                        <option value='KH'>".trans('country_translation.KH')."</option>
                        <option value='CM'>".trans('country_translation.CM')."</option>
                        <option value='CA'>".trans('country_translation.CA')."</option>
                        <option value='CV'>".trans('country_translation.CV')."</option>
                        <option value='KY'>".trans('country_translation.KY')."</option>
                        <option value='CF'>".trans('country_translation.CF')."</option>
                        <option value='TD'>".trans('country_translation.TD')."</option>
                        <option value='CL'>".trans('country_translation.CL')."</option>
                        <option value='CN'>".trans('country_translation.CN')."</option>
                        <option value='CX'>".trans('country_translation.CX')."</option>
                        <option value='CC'>".trans('country_translation.CC')."</option>
                        <option value='CO'>".trans('country_translation.CO')."</option>
                        <option value='KM'>".trans('country_translation.KM')."</option>
                        <option value='CK'>".trans('country_translation.CK')."</option>
                        <option value='CR'>".trans('country_translation.CR')."</option>
                        <option value='HR'>".trans('country_translation.HR')."</option>
                        <option value='CU'>".trans('country_translation.CU')."</option>
                        <option value='CW'>".trans('country_translation.CW')."</option>
                        <option value='CY'>".trans('country_translation.CY')."</option>
                        <option value='CZ'>".trans('country_translation.CZ')."</option>
                        <option value='CD'>".trans('country_translation.CD')."</option>
                        <option value='DK'>".trans('country_translation.DK')."</option>
                        <option value='DJ'>".trans('country_translation.DJ')."</option>
                        <option value='DM'>".trans('country_translation.DM')."</option>
                        <option value='DO'>".trans('country_translation.DO')."</option>
                        <option value='EC'>".trans('country_translation.EC')."</option>
                        <option value='EG'>".trans('country_translation.EG')."</option>
                        <option value='SV'>".trans('country_translation.SV')."</option>
                        <option value='GQ'>".trans('country_translation.GQ')."</option>
                        <option value='ER'>".trans('country_translation.ER')."</option>
                        <option value='EE'>".trans('country_translation.EE')."</option>
                        <option value='ET'>".trans('country_translation.ET')."</option>
                        <option value='FO'>".trans('country_translation.FO')."</option>
                        <option value='FJ'>".trans('country_translation.FJ')."</option>
                        <option value='FI'>".trans('country_translation.FI')."</option>
                        <option value='FR'>".trans('country_translation.FR')."</option>
                        <option value='GF'>".trans('country_translation.GF')."</option>
                        <option value='PF'>".trans('country_translation.PF')."</option>
                        <option value='GA'>".trans('country_translation.GA')."</option>
                        <option value='GM'>".trans('country_translation.GM')."</option>
                        <option value='GE'>".trans('country_translation.GE')."</option>
                        <option value='DE'>".trans('country_translation.DE')."</option>
                        <option value='GH'>".trans('country_translation.GH')."</option>
                        <option value='GI'>".trans('country_translation.GI')."</option>
                        <option value='GR'>".trans('country_translation.GR')."</option>
                        <option value='GL'>".trans('country_translation.GL')."</option>
                        <option value='GD'>".trans('country_translation.GD')."</option>
                        <option value='GP'>".trans('country_translation.GP')."</option>
                        <option value='GU'>".trans('country_translation.GU')."</option>
                        <option value='GT'>".trans('country_translation.GT')."</option>
                        <option value='GG'>".trans('country_translation.GG')."</option>
                        <option value='GN'>".trans('country_translation.GN')."</option>
                        <option value='GW'>".trans('country_translation.GW')."</option>
                        <option value='GY'>".trans('country_translation.GY')."</option>
                        <option value='HT'>".trans('country_translation.HT')."</option>
                        <option value='VA'>".trans('country_translation.VA')."</option>
                        <option value='HN'>".trans('country_translation.HN')."</option>
                        <option value='HK'>".trans('country_translation.HK')."</option>
                        <option value='HU'>".trans('country_translation.HU')."</option>
                        <option value='IS'>".trans('country_translation.IS')."</option>
                        <option value='IN'>".trans('country_translation.IN')."</option>
                        <option value='ID'>".trans('country_translation.ID')."</option>
                        <option value='IR'>".trans('country_translation.IR')."</option>
                        <option value='IQ'>".trans('country_translation.IQ')."</option>
                        <option value='IE'>".trans('country_translation.IE')."</option>
                        <option value='IM'>".trans('country_translation.IM')."</option>
                        <option value='IL'>".trans('country_translation.IL')."</option>
                        <option value='IT'>".trans('country_translation.IT')."</option>
                        <option value='CI'>".trans('country_translation.CI')."</option>
                        <option value='JM'>".trans('country_translation.JM')."</option>
                        <option value='JP'>".trans('country_translation.JP')."</option>
                        <option value='JE'>".trans('country_translation.JE')."</option>
                        <option value='JO'>".trans('country_translation.JO')."</option>
                        <option value='KZ'>".trans('country_translation.KZ')."</option>
                        <option value='KE'>".trans('country_translation.KE')."</option>
                        <option value='KI'>".trans('country_translation.KI')."</option>
                        <option value='XK'>".trans('country_translation.XK')."</option>
                        <option value='KW'>".trans('country_translation.KW')."</option>
                        <option value='KG'>".trans('country_translation.KG')."</option>
                        <option value='LA'>".trans('country_translation.LA')."</option>
                        <option value='LV'>".trans('country_translation.LV')."</option>
                        <option value='LB'>".trans('country_translation.LB')."</option>
                        <option value='LS'>".trans('country_translation.LS')."</option>
                        <option value='LR'>".trans('country_translation.LR')."</option>
                        <option value='LY'>".trans('country_translation.LY')."</option>
                        <option value='LI'>".trans('country_translation.LI')."</option>
                        <option value='LT'>".trans('country_translation.LT')."</option>
                        <option value='LU'>".trans('country_translation.LU')."</option>
                        <option value='MO'>".trans('country_translation.MO')."</option>
                        <option value='MK'>".trans('country_translation.MK')."</option>
                        <option value='MG'>".trans('country_translation.MG')."</option>
                        <option value='MW'>".trans('country_translation.MW')."</option>
                        <option value='MY'>".trans('country_translation.MY')."</option>
                        <option value='MV'>".trans('country_translation.MV')."</option>
                        <option value='ML'>".trans('country_translation.ML')."</option>
                        <option value='MT'>".trans('country_translation.MT')."</option>
                        <option value='MH'>".trans('country_translation.MH')."</option>
                        <option value='MQ'>".trans('country_translation.MQ')."</option>
                        <option value='MR'>".trans('country_translation.MR')."</option>
                        <option value='MU'>".trans('country_translation.MU')."</option>
                        <option value='YT'>".trans('country_translation.YT')."</option>
                        <option value='MX'>".trans('country_translation.MX')."</option>
                        <option value='FM'>".trans('country_translation.FM')."</option>
                        <option value='MD'>".trans('country_translation.MD')."</option>
                        <option value='MC'>".trans('country_translation.MC')."</option>
                        <option value='MN'>".trans('country_translation.MN')."</option>
                        <option value='ME'>".trans('country_translation.ME')."</option>
                        <option value='MS'>".trans('country_translation.MS')."</option>
                        <option value='MA'>".trans('country_translation.MA')."</option>
                        <option value='MZ'>".trans('country_translation.MZ')."</option>
                        <option value='NA'>".trans('country_translation.NA')."</option>
                        <option value='NR'>".trans('country_translation.NR')."</option>
                        <option value='NP'>".trans('country_translation.NP')."</option>
                        <option value='NL'>".trans('country_translation.NL')."</option>
                        <option value='AN'>".trans('country_translation.AN')."</option>
                        <option value='NC'>".trans('country_translation.NC')."</option>
                        <option value='NZ'>".trans('country_translation.NZ')."</option>
                        <option value='NI'>".trans('country_translation.NI')."</option>
                        <option value='NE'>".trans('country_translation.NE')."</option>
                        <option value='NG'>".trans('country_translation.NG')."</option>
                        <option value='NU'>".trans('country_translation.NU')."</option>
                        <option value='NF'>".trans('country_translation.NF')."</option>
                        <option value='KP'>".trans('country_translation.KP')."</option>
                        <option value='MP'>".trans('country_translation.MP')."</option>
                        <option value='NO'>".trans('country_translation.NO')."</option>
                        <option value='OM'>".trans('country_translation.OM')."</option>
                        <option value='PK'>".trans('country_translation.PK')."</option>
                        <option value='PW'>".trans('country_translation.PW')."</option>
                        <option value='PS'>".trans('country_translation.PS')."</option>
                        <option value='PA'>".trans('country_translation.PA')."</option>
                        <option value='PG'>".trans('country_translation.PG')."</option>
                        <option value='PY'>".trans('country_translation.PY')."</option>
                        <option value='PE'>".trans('country_translation.PE')."</option>
                        <option value='PH'>".trans('country_translation.PH')."</option>
                        <option value='PN'>".trans('country_translation.PN')."</option>
                        <option value='PL'>".trans('country_translation.PL')."</option>
                        <option value='PT'>".trans('country_translation.PT')."</option>
                        <option value='PR'>".trans('country_translation.PR')."</option>
                        <option value='QA'>".trans('country_translation.QA')."</option>
                        <option value='CG'>".trans('country_translation.CG')."</option>
                        <option value='RE'>".trans('country_translation.RE')."</option>
                        <option value='RO'>".trans('country_translation.RO')."</option>
                        <option value='RU'>".trans('country_translation.RU')."</option>
                        <option value='RW'>".trans('country_translation.RW')."</option>
                        <option value='BL'>".trans('country_translation.BL')."</option>
                        <option value='SH'>".trans('country_translation.SH')."</option>
                        <option value='KN'>".trans('country_translation.KN')."</option>
                        <option value='LC'>".trans('country_translation.LC')."</option>
                        <option value='SX'>".trans('country_translation.SX')."</option>
                        <option value='PM'>".trans('country_translation.PM')."</option>
                        <option value='VC'>".trans('country_translation.VC')."</option>
                        <option value='WS'>".trans('country_translation.WS')."</option>
                        <option value='SM'>".trans('country_translation.SM')."</option>
                        <option value='ST'>".trans('country_translation.ST')."</option>
                        <option value='SA'>".trans('country_translation.SA')."</option>
                        <option value='SN'>".trans('country_translation.SN')."</option>
                        <option value='RS'>".trans('country_translation.RS')."</option>
                        <option value='SC'>".trans('country_translation.SC')."</option>
                        <option value='SL'>".trans('country_translation.SL')."</option>
                        <option value='SG'>".trans('country_translation.SG')."</option>
                        <option value='SK'>".trans('country_translation.SK')."</option>
                        <option value='SI'>".trans('country_translation.SI')."</option>
                        <option value='SB'>".trans('country_translation.SB')."</option>
                        <option value='SO'>".trans('country_translation.SO')."</option>
                        <option value='ZA'>".trans('country_translation.ZA')."</option>
                        <option value='KR'>".trans('country_translation.KR')."</option>
                        <option value='SS'>".trans('country_translation.SS')."</option>
                        <option value='ES'>".trans('country_translation.ES')."</option>
                        <option value='LK'>".trans('country_translation.LK')."</option>
                        <option value='SD'>".trans('country_translation.SD')."</option>
                        <option value='SR'>".trans('country_translation.SR')."</option>
                        <option value='SJ'>".trans('country_translation.SJ')."</option>
                        <option value='SZ'>".trans('country_translation.SZ')."</option>
                        <option value='SE'>".trans('country_translation.SE')."</option>
                        <option value='CH'>".trans('country_translation.CH')."</option>
                        <option value='SY'>".trans('country_translation.SY')."</option>
                        <option value='TW'>".trans('country_translation.TW')."</option>
                        <option value='TJ'>".trans('country_translation.TJ')."</option>
                        <option value='TZ'>".trans('country_translation.TZ')."</option>
                        <option value='TH'>".trans('country_translation.TH')."</option>
                        <option value='TL'>".trans('country_translation.TL')."</option>
                        <option value='TG'>".trans('country_translation.TG')."</option>
                        <option value='TK'>".trans('country_translation.TK')."</option>
                        <option value='TO'>".trans('country_translation.TO')."</option>
                        <option value='TT'>".trans('country_translation.TT')."</option>
                        <option value='TN'>".trans('country_translation.TN')."</option>
                        <option value='TR'>".trans('country_translation.TR')."</option>
                        <option value='TM'>".trans('country_translation.TM')."</option>
                        <option value='TC'>".trans('country_translation.TC')."</option>
                        <option value='TV'>".trans('country_translation.TV')."</option>
                        <option value='UG'>".trans('country_translation.UG')."</option>
                        <option value='UA'>".trans('country_translation.UA')."</option>
                        <option value='AE'>".trans('country_translation.AE')."</option>
                        <option value='UY'>".trans('country_translation.UY')."</option>
                        <option value='UZ'>".trans('country_translation.UZ')."</option>
                        <option value='VU'>".trans('country_translation.VU')."</option>
                        <option value='VE'>".trans('country_translation.VE')."</option>
                        <option value='VN'>".trans('country_translation.VN')."</option>
                        <option value='VI'>".trans('country_translation.VI')."</option>
                        <option value='WF'>".trans('country_translation.WF')."</option>
                        <option value='YE'>".trans('country_translation.YE')."</option>
                        <option value='ZM'>".trans('country_translation.ZM')."</option>
                        <option value='ZW'>".trans('country_translation.ZW')."</option>
                        <option value='AX'>".trans('country_translation.AX')."</option>
                        </optgroup>
                        ";
                        }
                        else
                        {
                   echo "<option value='DE'>".trans('country_translation.DE')."</option>
                        <option value='CH'>".trans('country_translation.CH')."</option>
                        <option value='AT'>".trans('country_translation.AT')."</option>
                        <optgroup label='----------------------------------'>
                        <option value='AF'>".trans('country_translation.AF')."</option>
                        <option value='AL'>".trans('country_translation.AL')."</option>
                        <option value='DZ'>".trans('country_translation.DZ')."</option>
                        <option value='AS'>".trans('country_translation.AS')."</option>
                        <option value='AD'>".trans('country_translation.AD')."</option>
                        <option value='AO'>".trans('country_translation.AO')."</option>
                        <option value='AI'>".trans('country_translation.AI')."</option>
                        <option value='AG'>".trans('country_translation.AG')."</option>
                        <option value='AR'>".trans('country_translation.AR')."</option>
                        <option value='AM'>".trans('country_translation.AM')."</option>
                        <option value='AW'>".trans('country_translation.AW')."</option>
                        <option value='AU'>".trans('country_translation.AU')."</option>
                        <option value='AZ'>".trans('country_translation.AZ')."</option>
                        <option value='BS'>".trans('country_translation.BS')."</option>
                        <option value='BH'>".trans('country_translation.BH')."</option>
                        <option value='BD'>".trans('country_translation.BD')."</option>
                        <option value='BB'>".trans('country_translation.BB')."</option>
                        <option value='BY'>".trans('country_translation.BY')."</option>
                        <option value='BE'>".trans('country_translation.BE')."</option>
                        <option value='BZ'>".trans('country_translation.BZ')."</option>
                        <option value='BJ'>".trans('country_translation.BJ')."</option>
                        <option value='BM'>".trans('country_translation.BM')."</option>
                        <option value='BT'>".trans('country_translation.BT')."</option>
                        <option value='BO'>".trans('country_translation.BO')."</option>
                        <option value='BA'>".trans('country_translation.BA')."</option>
                        <option value='BW'>".trans('country_translation.BW')."</option>
                        <option value='BR'>".trans('country_translation.BR')."</option>
                        <option value='VG'>".trans('country_translation.VG')."</option>
                        <option value='BN'>".trans('country_translation.BN')."</option>
                        <option value='BG'>".trans('country_translation.BG')."</option>
                        <option value='BF'>".trans('country_translation.BF')."</option>
                        <option value='MM'>".trans('country_translation.MM')."</option>
                        <option value='BI'>".trans('country_translation.BI')."</option>
                        <option value='KH'>".trans('country_translation.KH')."</option>
                        <option value='CM'>".trans('country_translation.CM')."</option>
                        <option value='CA'>".trans('country_translation.CA')."</option>
                        <option value='CV'>".trans('country_translation.CV')."</option>
                        <option value='KY'>".trans('country_translation.KY')."</option>
                        <option value='CF'>".trans('country_translation.CF')."</option>
                        <option value='TD'>".trans('country_translation.TD')."</option>
                        <option value='CL'>".trans('country_translation.CL')."</option>
                        <option value='CN'>".trans('country_translation.CN')."</option>
                        <option value='CX'>".trans('country_translation.CX')."</option>
                        <option value='CC'>".trans('country_translation.CC')."</option>
                        <option value='CO'>".trans('country_translation.CO')."</option>
                        <option value='KM'>".trans('country_translation.KM')."</option>
                        <option value='CK'>".trans('country_translation.CK')."</option>
                        <option value='CR'>".trans('country_translation.CR')."</option>
                        <option value='HR'>".trans('country_translation.HR')."</option>
                        <option value='CU'>".trans('country_translation.CU')."</option>
                        <option value='CW'>".trans('country_translation.CW')."</option>
                        <option value='CY'>".trans('country_translation.CY')."</option>
                        <option value='CZ'>".trans('country_translation.CZ')."</option>
                        <option value='CD'>".trans('country_translation.CD')."</option>
                        <option value='DK'>".trans('country_translation.DK')."</option>
                        <option value='DJ'>".trans('country_translation.DJ')."</option>
                        <option value='DM'>".trans('country_translation.DM')."</option>
                        <option value='DO'>".trans('country_translation.DO')."</option>
                        <option value='EC'>".trans('country_translation.EC')."</option>
                        <option value='EG'>".trans('country_translation.EG')."</option>
                        <option value='SV'>".trans('country_translation.SV')."</option>
                        <option value='GQ'>".trans('country_translation.GQ')."</option>
                        <option value='ER'>".trans('country_translation.ER')."</option>
                        <option value='EE'>".trans('country_translation.EE')."</option>
                        <option value='ET'>".trans('country_translation.ET')."</option>
                        <option value='FO'>".trans('country_translation.FO')."</option>
                        <option value='FJ'>".trans('country_translation.FJ')."</option>
                        <option value='FI'>".trans('country_translation.FI')."</option>
                        <option value='FR'>".trans('country_translation.FR')."</option>
                        <option value='GF'>".trans('country_translation.GF')."</option>
                        <option value='PF'>".trans('country_translation.PF')."</option>
                        <option value='GA'>".trans('country_translation.GA')."</option>
                        <option value='GM'>".trans('country_translation.GM')."</option>
                        <option value='GE'>".trans('country_translation.GE')."</option>
                        <option value='GH'>".trans('country_translation.GH')."</option>
                        <option value='GI'>".trans('country_translation.GI')."</option>
                        <option value='GR'>".trans('country_translation.GR')."</option>
                        <option value='GL'>".trans('country_translation.GL')."</option>
                        <option value='GD'>".trans('country_translation.GD')."</option>
                        <option value='GP'>".trans('country_translation.GP')."</option>
                        <option value='GU'>".trans('country_translation.GU')."</option>
                        <option value='GT'>".trans('country_translation.GT')."</option>
                        <option value='GG'>".trans('country_translation.GG')."</option>
                        <option value='GN'>".trans('country_translation.GN')."</option>
                        <option value='GW'>".trans('country_translation.GW')."</option>
                        <option value='GY'>".trans('country_translation.GY')."</option>
                        <option value='HT'>".trans('country_translation.HT')."</option>
                        <option value='VA'>".trans('country_translation.VA')."</option>
                        <option value='HN'>".trans('country_translation.HN')."</option>
                        <option value='HK'>".trans('country_translation.HK')."</option>
                        <option value='HU'>".trans('country_translation.HU')."</option>
                        <option value='IS'>".trans('country_translation.IS')."</option>
                        <option value='IN'>".trans('country_translation.IN')."</option>
                        <option value='ID'>".trans('country_translation.ID')."</option>
                        <option value='IR'>".trans('country_translation.IR')."</option>
                        <option value='IQ'>".trans('country_translation.IQ')."</option>
                        <option value='IE'>".trans('country_translation.IE')."</option>
                        <option value='IM'>".trans('country_translation.IM')."</option>
                        <option value='IL'>".trans('country_translation.IL')."</option>
                        <option value='IT'>".trans('country_translation.IT')."</option>
                        <option value='CI'>".trans('country_translation.CI')."</option>
                        <option value='JM'>".trans('country_translation.JM')."</option>
                        <option value='JP'>".trans('country_translation.JP')."</option>
                        <option value='JE'>".trans('country_translation.JE')."</option>
                        <option value='JO'>".trans('country_translation.JO')."</option>
                        <option value='KZ'>".trans('country_translation.KZ')."</option>
                        <option value='KE'>".trans('country_translation.KE')."</option>
                        <option value='KI'>".trans('country_translation.KI')."</option>
                        <option value='XK'>".trans('country_translation.XK')."</option>
                        <option value='KW'>".trans('country_translation.KW')."</option>
                        <option value='KG'>".trans('country_translation.KG')."</option>
                        <option value='LA'>".trans('country_translation.LA')."</option>
                        <option value='LV'>".trans('country_translation.LV')."</option>
                        <option value='LB'>".trans('country_translation.LB')."</option>
                        <option value='LS'>".trans('country_translation.LS')."</option>
                        <option value='LR'>".trans('country_translation.LR')."</option>
                        <option value='LY'>".trans('country_translation.LY')."</option>
                        <option value='LI'>".trans('country_translation.LI')."</option>
                        <option value='LT'>".trans('country_translation.LT')."</option>
                        <option value='LU'>".trans('country_translation.LU')."</option>
                        <option value='MO'>".trans('country_translation.MO')."</option>
                        <option value='MK'>".trans('country_translation.MK')."</option>
                        <option value='MG'>".trans('country_translation.MG')."</option>
                        <option value='MW'>".trans('country_translation.MW')."</option>
                        <option value='MY'>".trans('country_translation.MY')."</option>
                        <option value='MV'>".trans('country_translation.MV')."</option>
                        <option value='ML'>".trans('country_translation.ML')."</option>
                        <option value='MT'>".trans('country_translation.MT')."</option>
                        <option value='MH'>".trans('country_translation.MH')."</option>
                        <option value='MQ'>".trans('country_translation.MQ')."</option>
                        <option value='MR'>".trans('country_translation.MR')."</option>
                        <option value='MU'>".trans('country_translation.MU')."</option>
                        <option value='YT'>".trans('country_translation.YT')."</option>
                        <option value='MX'>".trans('country_translation.MX')."</option>
                        <option value='FM'>".trans('country_translation.FM')."</option>
                        <option value='MD'>".trans('country_translation.MD')."</option>
                        <option value='MC'>".trans('country_translation.MC')."</option>
                        <option value='MN'>".trans('country_translation.MN')."</option>
                        <option value='ME'>".trans('country_translation.ME')."</option>
                        <option value='MS'>".trans('country_translation.MS')."</option>
                        <option value='MA'>".trans('country_translation.MA')."</option>
                        <option value='MZ'>".trans('country_translation.MZ')."</option>
                        <option value='NA'>".trans('country_translation.NA')."</option>
                        <option value='NR'>".trans('country_translation.NR')."</option>
                        <option value='NP'>".trans('country_translation.NP')."</option>
                        <option value='NL'>".trans('country_translation.NL')."</option>
                        <option value='AN'>".trans('country_translation.AN')."</option>
                        <option value='NC'>".trans('country_translation.NC')."</option>
                        <option value='NZ'>".trans('country_translation.NZ')."</option>
                        <option value='NI'>".trans('country_translation.NI')."</option>
                        <option value='NE'>".trans('country_translation.NE')."</option>
                        <option value='NG'>".trans('country_translation.NG')."</option>
                        <option value='NU'>".trans('country_translation.NU')."</option>
                        <option value='NF'>".trans('country_translation.NF')."</option>
                        <option value='KP'>".trans('country_translation.KP')."</option>
                        <option value='MP'>".trans('country_translation.MP')."</option>
                        <option value='NO'>".trans('country_translation.NO')."</option>
                        <option value='OM'>".trans('country_translation.OM')."</option>
                        <option value='PK'>".trans('country_translation.PK')."</option>
                        <option value='PW'>".trans('country_translation.PW')."</option>
                        <option value='PS'>".trans('country_translation.PS')."</option>
                        <option value='PA'>".trans('country_translation.PA')."</option>
                        <option value='PG'>".trans('country_translation.PG')."</option>
                        <option value='PY'>".trans('country_translation.PY')."</option>
                        <option value='PE'>".trans('country_translation.PE')."</option>
                        <option value='PH'>".trans('country_translation.PH')."</option>
                        <option value='PN'>".trans('country_translation.PN')."</option>
                        <option value='PL'>".trans('country_translation.PL')."</option>
                        <option value='PT'>".trans('country_translation.PT')."</option>
                        <option value='PR'>".trans('country_translation.PR')."</option>
                        <option value='QA'>".trans('country_translation.QA')."</option>
                        <option value='CG'>".trans('country_translation.CG')."</option>
                        <option value='RE'>".trans('country_translation.RE')."</option>
                        <option value='RO'>".trans('country_translation.RO')."</option>
                        <option value='RU'>".trans('country_translation.RU')."</option>
                        <option value='RW'>".trans('country_translation.RW')."</option>
                        <option value='BL'>".trans('country_translation.BL')."</option>
                        <option value='SH'>".trans('country_translation.SH')."</option>
                        <option value='KN'>".trans('country_translation.KN')."</option>
                        <option value='LC'>".trans('country_translation.LC')."</option>
                        <option value='SX'>".trans('country_translation.SX')."</option>
                        <option value='PM'>".trans('country_translation.PM')."</option>
                        <option value='VC'>".trans('country_translation.VC')."</option>
                        <option value='WS'>".trans('country_translation.WS')."</option>
                        <option value='SM'>".trans('country_translation.SM')."</option>
                        <option value='ST'>".trans('country_translation.ST')."</option>
                        <option value='SA'>".trans('country_translation.SA')."</option>
                        <option value='SN'>".trans('country_translation.SN')."</option>
                        <option value='RS'>".trans('country_translation.RS')."</option>
                        <option value='SC'>".trans('country_translation.SC')."</option>
                        <option value='SL'>".trans('country_translation.SL')."</option>
                        <option value='SG'>".trans('country_translation.SG')."</option>
                        <option value='SK'>".trans('country_translation.SK')."</option>
                        <option value='SI'>".trans('country_translation.SI')."</option>
                        <option value='SB'>".trans('country_translation.SB')."</option>
                        <option value='SO'>".trans('country_translation.SO')."</option>
                        <option value='ZA'>".trans('country_translation.ZA')."</option>
                        <option value='KR'>".trans('country_translation.KR')."</option>
                        <option value='SS'>".trans('country_translation.SS')."</option>
                        <option value='ES'>".trans('country_translation.ES')."</option>
                        <option value='LK'>".trans('country_translation.LK')."</option>
                        <option value='SD'>".trans('country_translation.SD')."</option>
                        <option value='SR'>".trans('country_translation.SR')."</option>
                        <option value='SJ'>".trans('country_translation.SJ')."</option>
                        <option value='SZ'>".trans('country_translation.SZ')."</option>
                        <option value='SE'>".trans('country_translation.SE')."</option>
                        <option value='SY'>".trans('country_translation.SY')."</option>
                        <option value='TW'>".trans('country_translation.TW')."</option>
                        <option value='TJ'>".trans('country_translation.TJ')."</option>
                        <option value='TZ'>".trans('country_translation.TZ')."</option>
                        <option value='TH'>".trans('country_translation.TH')."</option>
                        <option value='TL'>".trans('country_translation.TL')."</option>
                        <option value='TG'>".trans('country_translation.TG')."</option>
                        <option value='TK'>".trans('country_translation.TK')."</option>
                        <option value='TO'>".trans('country_translation.TO')."</option>
                        <option value='TT'>".trans('country_translation.TT')."</option>
                        <option value='TN'>".trans('country_translation.TN')."</option>
                        <option value='TR'>".trans('country_translation.TR')."</option>
                        <option value='TM'>".trans('country_translation.TM')."</option>
                        <option value='TC'>".trans('country_translation.TC')."</option>
                        <option value='TV'>".trans('country_translation.TV')."</option>
                        <option value='UG'>".trans('country_translation.UG')."</option>
                        <option value='UA'>".trans('country_translation.UA')."</option>
                        <option value='AE'>".trans('country_translation.AE')."</option>
                        <option value='GB'>".trans('country_translation.GB')."</option>
                        <option value='US'>".trans('country_translation.US')."</option>
                        <option value='UY'>".trans('country_translation.UY')."</option>
                        <option value='UZ'>".trans('country_translation.UZ')."</option>
                        <option value='VU'>".trans('country_translation.VU')."</option>
                        <option value='VE'>".trans('country_translation.VE')."</option>
                        <option value='VN'>".trans('country_translation.VN')."</option>
                        <option value='VI'>".trans('country_translation.VI')."</option>
                        <option value='WF'>".trans('country_translation.WF')."</option>
                        <option value='YE'>".trans('country_translation.YE')."</option>
                        <option value='ZM'>".trans('country_translation.ZM')."</option>
                        <option value='ZW'>".trans('country_translation.ZW')."</option>
                        <option value='AX'>".trans('country_translation.AX')."</option>
                        </optgroup>
                        ";
                        }
                    ?>
                    </select>
                    </span>
                    <span>
                    <select class="comp_name contact-form price" id="select-language" name="language" placeholder="<?php echo trans('translate.select_a_language')?>">
                    <option value="" selected></option> 
                    <option value="de">Deutsch</option>
                    <option value="en">English</option>
                    </select>
                    </span>
                    <label><input type="checkbox" class="price" name="check_box" id="check_box" value="privacy"><span class="accept">
                    &nbsp&nbsp <?php echo trans('translate.terms_and_condition_para')?> <a href="<?php echo url('termsandcondition')?>" class="underline_on_hover" target="_blank" ><!-- <?php echo trans('translate.terms_and_condition'); ?> --><?php echo trans('translate.terms_and_condition_para1')?></a> <?php echo trans('translate.terms_and_condition_para2')?> <a href="<?php echo url('privacy')?>" class="underline_on_hover" target="_blank" > <?php echo trans('translate.terms_and_condition_para3')?></a></span></label>
                    <div class="trail_button" style="text-align:center;">
                        <button type="submit" class="form-control" id="btn2"><?php echo trans('welcome.create_account'); ?></button>

                        <img id="loader" src="image/hourglass.gif" style="display:none; width:35px; height: 35px;"/>

                  <!--  <p id="demo" style="color:red;margin-top: 10px;font-weight:bold;"></p> -->
                    </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>


<?php  include 'layout/main_page_footer.php';?>
<script src="<?php echo url('js/selectize.js')?>"></script>
 <script src="js/jquery.form.js"></script>
 <script >
$(document).ready(function() {
    $('#cookie-alert').show();

});
 </script>
<script>
$('#select-country').selectize();
$('#select-language').selectize();
 (function() {
$('form').ajaxForm({
    beforeSend: function () {
    var name = $('#name').val();
    var email = $('#email').val();
    var password = $("#password").val();
    var confirmPassword = $("#confirm_password").val();
    var termsAndCond = $("#check_box").is(":checked");
    var isValid = true;
    var message;
    var country=$('#select-country').val();
    var language=$('#select-language').val();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    console.log(regex.test(email));
    var pass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    console.log(pass.test(password));
    if(name == null || email == null || password == null || confirmPassword == null || termsAndCond == false ||
        name == '' || email == '' || password == '' || confirmPassword == ''){
        isValid = false;
        swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.all_fields');?>", "error");
    }
    else if(regex.test(email) == false){
        isValid = false;
        swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.check_email');?>", "error");
    }
    else if(pass.test(password)  == false){
        isValid=false;
        swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.password_minimum');?>", "error");
    }
    else if(password != confirmPassword){
        isValid = false;
         swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.not_matching');?>", "error");
    }
    else if(country == "null"){
        isValid = false;
         swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.select_country');?>", "error");
    }
    else if(language == "null"){
        isValid = false;
         swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.select_language');?>", "error");
    }
    if(isValid == false){
      /*  $('#demo').css('color','red');
        $("#demo").html(message);*/
        return false;
    }
    else{
        $('#btn2').hide();
        $('#loader').show();
    }
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
        console.log(data);
 if(data.status=='success')
 {
    swal("<?php echo trans('popup.success');?>!", "<?php echo trans('popup.thank_you');?>", "success");
    $("#form")[0].reset();
     $('#loader').hide();
     $('#btn2').show();
 }
 else if(data.status=='failure'){
    $('#btn2').show();
    $('#loader').hide();
    swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.email_exists');?>", "error");
}

    },
    complete: function() {
    
    }
}); 

})();
//warning before u select the german language
$('#select-language').change(function()
{
var typeOfLang=$('#select-language').val();
if (typeOfLang=="de") 
    {
     swal("Hinweis","Die Sprache bestimmt die Grundeinstellung der Zeitkonten. Wenn Sie Mitarbeiter im Ausland einschliessen möchten, empfehlen wir hier Englisch zu wählen. Sie können aber auch nachträglich die Zeitkonten in der gewünschten Sprache selber erfassen.");
    }
});
/*$('#help').click(function()
{
swal("","<?php echo trans('popup.password_minimum');?>");
});*/
$('.close').click(function () {
  $('#myModal_signup').hide();
  $('#myModal_signup iframe').attr("src", jQuery("#myModal_signup iframe").attr("src"));
});

</script>
    <script src="js/rgen_min.js"></script>
<script src="js/rgen.js"></script>

