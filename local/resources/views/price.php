<?php 
$metaTitle=$data['metaTag']['title'];
$metaUrl=$data['metaTag']['url'];
$metaDescription=$data['metaTag']['description'];
$metaKeyword=$data['metaTag']['keyword'];
?>
<?php  include 'layout/main_page_header.php';?>
<?php
function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                $output = array(
                    "city"           => @$ipdat->geoplugin_city,
                    "state"          => @$ipdat->geoplugin_regionName,
                    "country"        => @$ipdat->geoplugin_countryName,
                    "country_code"   => @$ipdat->geoplugin_countryCode,
                    "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                    "continent_code" => @$ipdat->geoplugin_continentCode
                );
                break;
                case "address":
                $address = array($ipdat->geoplugin_countryName);
                if (@strlen($ipdat->geoplugin_regionName) >= 1)
                    $address[] = $ipdat->geoplugin_regionName;
                if (@strlen($ipdat->geoplugin_city) >= 1)
                    $address[] = $ipdat->geoplugin_city;
                $output = implode(", ", array_reverse($address));
                break;
                case "city":
                $output = @$ipdat->geoplugin_city;
                break;
                case "state":
                $output = @$ipdat->geoplugin_regionName;
                break;
                case "region":
                $output = @$ipdat->geoplugin_regionName;
                break;
                case "country":
                $output = @$ipdat->geoplugin_countryName;
                break;
                case "countrycode":
                $output = @$ipdat->geoplugin_countryCode;
                break;
            }
        }
    }
    return $output;
}
/*echo ip_info("Visitor", "Country"); // India
echo ip_info("Visitor", "Country Code"); // IN
echo ip_info("Visitor", "State"); // Andhra Pradesh
echo ip_info("Visitor", "City"); // Proddatur
echo ip_info("Visitor", "Address"); // Proddatur, Andhra Pradesh, India*/
$countryCode = ip_info("Visitor", "Country Code");

$plans=$data['plans'];
      $currency=$data['defaultPlans'][0]['currency_code'];
      $price=$data['defaultPlans'][0]['plan_price'];
 for ($i=0; $i < sizeof($plans); $i++) 
  {
   if ($plans[$i]['country_code']==$countryCode && $plans[$i]['plan']=="A") 
   {
      $currency=$plans[$i]['currency_code'];
      $price=$plans[$i]['plan_price'];
   }
  }
?> 
<script type="text/javascript">
  console.log('<?=ip_info("Visitor", "Country Code");?>');
</script>
<link rel="stylesheet" type="text/css" href="<?php echo url('css/price.css')?>">
<div class="row mot_header">
 <?php  include 'layout/main_page_header_nav.php';?>
 </div>
<div class="col-md-12" style="padding: 0px;">
<div class="container main_view">
     <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;margin-top: 25px;margin-bottom: 25px;">
       <h2 style="margin-bottom: 25px;"><?php echo trans('price.title')?></h2>
      	<p><?php echo trans('price.sub_title_one')?></p>
        <p><?php echo trans('price.sub_title_two')?></p>
     </div> 
     <div class="col-md-6 col-sm-12 col-xs-12" style="padding-bottom:40px;" >
          <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
               <img class="dummy_image" width="360" src="<?php echo url('image/price.png')?>">
          </div>
          
     </div>
     <div class="col-md-6 col-sm-12 col-xs-12 price_label" >
        <div class="col-md-12 col-sm-12 col-xs-12 small">
          <h5 style="font-weight:700; font-size:20px;"><?php echo trans('price.takes')?> :</h5>
          <p style="font-weight:500; font-size:16px;">1.<?php echo trans('price.first')?></p>
          <p style="font-weight:500; font-size:16px;">2.<?php echo trans('price.second')?></p>
          <p style="font-weight:500; font-size:16px;">3.<?php echo trans('price.third')?></p>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 small">
          <h6 style="padding-bottom:10px; margin-bottom:0px; margin-top:15px;"><?php echo trans('price.image_title')?></h6>
          <span class="tiny"><?php echo trans('price.per_year')?> : <?=$price?> <?=$currency?></span>
          <a href="<?php echo url('');?>">
          <button type="submit" class="form-control" id="btn2"><?php echo trans('price.free_trail')?></button></a>
        </div>
     </div>
</div>
</div>
     <div class="col-md-12 col-sm-12 col-xs-12 employee_data" style="padding-left:0px;padding-right:0px;">
                                   <div class="xe-body profile_body">
                                        <ul class="list-unstyled">
                                             <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.one')?></b>: <?php echo trans('price.one_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.two')?></b>: <?php echo trans('price.two_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.three')?></b>: <?php echo trans('price.three_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.four')?></b>: <?php echo trans('price.four_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.five')?></b>: <?php echo trans('price.five_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.six')?></b>: <?php echo trans('price.six_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                           <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.seven')?></b>: <?php echo trans('price.seven_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.eight')?></b>: <?php echo trans('price.eight_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.nine')?></b>: <?php echo trans('price.nine_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.ten')?></b>: <?php echo trans('price.ten_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.eleven')?></b>: <?php echo trans('price.eleven_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.twelve')?></b>: <?php echo trans('price.twelve_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.thirteen')?></b>: <?php echo trans('price.thirteen_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                              <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.fourteen')?></b>: <?php echo trans('price.fourteen_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.fifteen')?></b>: <?php echo trans('price.fifteen_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.sixteen')?></b>: <?php echo trans('price.sixteen_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div class="mot_day_stats grey">
                                                       <div class="container">
                                                            <div class="col-md-1 col-sm-1 col-xs-1 check"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11"><span class="text"><b><?php echo trans('price.seventeen')?></b>: <?php echo trans('price.seventeen_details')?></span></div>
                                                       </div>
                                                  </div>
                                             </li>
                                        </ul>
                                   </div>
                              </div>


<!-- <div class="col-md-12 col-sm-12 col-xs-12 strip">
    <div class="col-md-9 col-sm-8 col-xs-12"><h3 class="strip-text">Lorem Ipsum is simply dummy text </h3></div>
    <div class="col-md-3 col-sm-4 col-xs-12"><button class="btn1">READ MORE</button></div>
</div> -->
   
<?php  include 'layout/main_page_footer.php';?>