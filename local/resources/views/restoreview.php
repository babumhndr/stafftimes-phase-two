<?php  include 'layout/header.php';?>
 <?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
    }
</script> 
<style type="text/css">
    .link {
        padding-top: 10px !important;
            padding-bottom: 10px !important;
    }
    .template_link {
        color: #2392ec;
    }
    .template_link a {
        color: #2392ec;
    }
    .mot_template {
        margin-left: 0px;
        margin-right: 0px;
    }
    .warning-msg{
        color: #ac1414;
        margin-top: 9px;
    }
        .error_msg{
        text-align: center; 
        font-size: 20px; 
        height: 500px; 
        margin-top: 10px;
        }
        .row {
     margin-left: 0px !important; 
     margin-right: 0px !important; 
}
</style>
                
                    <script type="text/javascript">
                    jQuery(document).ready(function($)
                    {
                        $("#example-1").dataTable({
                            aLengthMenu: [
                                [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                            ],
                            language: {
                        "url":languageUrl,
                        },
                                'bSort':false
                                
                        });
                    });
                    </script>
                    <?php
                        if($data['status'] == 'success'){
                            $tableDisplay = "display:block";
                            $noItem = "display:none";
                        }
                        else{
                            $tableDisplay = "display:none";
                            $noItem = "display:block";
                        }
                    ?>

<div class="row mot_template">
    <div class="col-md-12 link">
        <p> 
        <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('backup')?>"><?php echo trans ('header.Backup')?></a></span> / 
        <a>Restore</a>
        </p>
    </div>
<div class="col-md-12 col-sm-12 col-xs-12 content" style=" padding: 16px; ">
<form id="upload_zip_form">
<input  name="zip_file" type="file" accept=".zip">
<input type="hidden" id="time_log" name="time_log">
</form>
<p class="warning-msg"><?php echo trans('translate.restore_text'); ?></p>
<button  id="upload-file" type="button"><?php echo trans('employer.upload')?></button>
</div>
<div style="<?=$tableDisplay?>">
                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                        <thead>
                            <tr>
                                <th>Log no.</th>
                                <th><?php echo trans ('reports-1.date')?></th>
                            </tr>
                        </thead>
                    <tbody>

                        <?php
                        if ($data['status']=="success") 
                        {
                            for($i=0;$i<count($data['response']);$i++){
                                $link='"'.$data['response'][$i]['link'].'"';
                                echo "<tr>
                                <td>".($i+1)."</td> 
                                 <td>".$data['response'][$i]['log']."</td> 
                            </tr>";
                            }
                        }
                        ?>              
                        </tbody>
                    </table>
                </div>
                    <div class="col-md-12 error_msg" style="<?=$noItem?>"><p><?php echo trans('manage_activity.data_not_available')?></p>
                    </div>
</div>

<script src="<?php echo url('js/jquery.form.js')?>"></script>
<script type="text/javascript">
	$('#upload-file').click(function(){
        var time_log =moment().format("YYYY-MM-DD HH:mm");
        $("#time_log").val($("#time_log").html()+time_log);
	$('#upload_zip_form').ajaxSubmit({ 
        type: "POST",
        url: "importDate",
        data: $('#upload_zip_form').serialize(),
        cache: false,
        success:function(response){
        	console.log(response);

            if(response['status']=='success')
            {
            swal({  
            title:"<?php echo trans('popup.success');?>", 
            text: "<?php echo trans('translate.restore_success');?>",   
            type: "success",   
            confirmButtonText : "Ok"
            },
            function(){

            window.location.href = '<?php echo url('restoreView'); ?>';

            });
            /*  $("#form1")[0].reset();*/
            }
            else if(response['status']=='failure'){
            swal("<?php echo trans('popup.error_');?>!","<?php echo trans('translate.restore_error');?>", "error");
            }
	   }
    });
	});
</script>
<?php include 'layout/footer.php';?>
<link rel="stylesheet" href="<?= url('assets/js/datatables/dataTables.bootstrap.css')?>">
<script src="<?= url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
<!-- Imported scripts on this page -->
<script src="<?= url('assets/js/datatables/dataTables.bootstrap.js')?>"></script>
<script src="<?= url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js')?>"></script>
<script src="<?= url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>