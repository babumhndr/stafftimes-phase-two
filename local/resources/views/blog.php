<?php 
$metaTitle=$blogData['metaTag']['title'];
$metaUrl=$blogData['metaTag']['url'];
$metaDescription=$blogData['metaTag']['description'];
$metaKeyword=$blogData['metaTag']['keyword'];
?>
<?php  include 'layout/main_page_header.php';?>
<?php $data=$blogData['blogResult'];?>
 <link rel="stylesheet" type="text/css" href="<?php echo url('css/tour.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo url('css/timeline.css')?>">
  <script type="text/javascript" src="<?php echo url('js/jquery-latest.js')?>"></script>
  <style type="text/css">
  .timeline_symbol{
  margin: 0px; 
  font-size: 18px; 
  color: #fff; 
  text-transform: uppercase; 
  }

  .linked-in-btn{
    padding-top: 5px;
  }

  .comment_div{
   color: #B8B8B8 !important;
   text-decoration: none;
   font-size: 14px;
   position: relative;top: 15px;
  }
  .blog_para_head{
    font-size: 18px !important;
  }
  .comment_icon{
    font-size: 18px !important;
    margin-right: 20px !important;
    color: #8D8D8D !important;
  }
  .comment_details{
    border: 1px solid #F4F4F4; 
    border-left: 0px; 
    border-right: 0px; 
    padding: 20px; 
    padding-left: 0px; 
    padding-right: 0px; 
  }
  .comment_input{
   padding: 20px;
   padding-left: 0px; 
   padding-right: 0px; 
   border-color: #B9B7B7; 
  }
  .comment_details_pic{
     width: 40px; 
     height: 40px; 
     background: #C2C2C2; 
     border-radius: 50%; 
     float: left; 
  }
  textarea{
    width: 100%;
    height: 80px;
    padding-left: 8px;
    padding-top: 4px; 
    resize: none;
    outline: none;
    border: 1px solid #F4F4F4;;
  }
  .submit_button{
   float: right; 
   padding: 2px 8px;
    border: 0px; 
    color: #f1f1f1; 
    background: #B9B8B8; "
  }
   @media only screen and (max-width: 800px) {
    .timeline-centered:before{
      top: 75px !important
    }
    .timeline-centered{
      margin-top: 40px !important;
    }
  .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time{
    position: initial !important;
  }
}
.more {
	height:60px;
	text-overflow: ellipsis;
	overflow:hidden;
}
  </style>
<script type="text/javascript">
$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 200; // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "<?php echo trans('blog.read_more'); ?>";
    var lesstext = "<?php echo trans('blog.show_less'); ?>";
    var pagelink = '<?php echo url('bloglanding/'.$data['0']['_id'])?>';
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            
            var html = c + '<span class="moreellipses">' +ellipsestext+ '&nbsp;</span>';
            /* var res =html+'<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="'+ pagelink +'" class="morelink"style="margin-top: 7px; ">' + moretext + '</a></span>' */
 
            
            /* $(this).html(res); */
        }
 
    });
 
    /*$(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle(1);
        $(this).prev().toggle(1);
        return false;
    });*/
});
</script>
<!-- <SCRIPT>
$(document).ready(function() {
  var showChar = 300;
  var ellipsestext = "...";
  var moretext = "<?php echo trans('blog.read_more'); ?>";
    var lesstext = "<?php echo trans('blog.show_less'); ?>";
  $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      $(this).html(html);
    }

  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</SCRIPT> -->
<!-- comment_toggle -->
  <script type="text/javascript">
    $(function() {
        $('.comment_toggle').click(function() {
          var drop=$('a',this).attr('id');
          $(".toggle4_"+drop).slideToggle('fast');
          return false;
});

    });
  </script>
<style type="text/css">
    .show-read-more .more-text{
        display: none;
    } 
</style>
   <div class="row mot_header">
 <?php  include 'layout/main_page_header_nav.php';?>
 </div>
 <div class="row"style=" margin: 0px !important; background: #EEEEEE !important; ">
<div class="row"style="margin-top: 30px !important;padding-right: 30px;margin: 0px;">
  <div class="row">
    <div class="timeline-centered">
    
  <!-- <article class="timeline-entry">
    
    <div class="timeline-entry-inner">
      <div class="timeline-time"><span style=" color: #000; ">April 3, 2016</span> <span>3 <?php echo trans('blog.comments'); ?></span></div>
      
      <div class="timeline-icon"style="background-color: #DDDDDD;
    color: #fff;">
        <p class="timeline_symbol">M</p>
      </div>
      
      <div class="timeline-label">
        <h2><p class="blog_para_head"><?php echo trans('blog.title_one'); ?></p></h2>
        <p class="comment more"><?php echo trans('blog.para_one'); ?></p>
        <div class="comment_toggle"><a href="#" id="toggle" class="comment_div"><i class="fa fa-commenting-o comment_icon" aria-hidden="true"></i><?php echo trans('blog.comments'); ?>(3)</a></div><br /><br />
                <div class="toggle4_toggle" style="display:none;">
                  <div class="comment_details">
                    <div class="comment_details_pic"></div>
                    <div style=" padding-left: 50px; ">
                      <p style=" margin: 0px !important; font-size: 14px;color: #686868;">Babin Kochana <span style="color: #DBDBDB;font-size: 10px;margin-left: 10px;">01 April 2016 - 12:45</span> </p>
                      <p style=" margin: 0px !important; color: #A8A8A8 !important; ">Let’s go through a simple example of branching and merging with a workflow that you might use in the real world.</p>
                    </div>
                  </div>
                  <div class="comment_input">
                  <form action="#" id="my_form" method="post" name="my_form">
                    <textarea id="text" placeholder="Comments"></textarea>
                    <input class="submit_button"type="submit" value="Comment">
                  </form>
                  </div>
                </div>
    
      </div>
    </div>
    
  </article>
  
  
  <article class="timeline-entry ">
    
    <div class="timeline-entry-inner">
      <div class="timeline-time"><span style=" color: #000; ">April 3, 2016</span> <span>3 <?php echo trans('blog.comments'); ?></span></div>
      <div class="timeline-icon"style="background-color: #92C611;
    color: #fff;">
        <p class="timeline_symbol">M</p>
      </div>
      
      <div class="timeline-label">
        <h2><p class="blog_para_head"><?php echo trans('blog.title_two'); ?></p></h2>
        <p class="comment more" ><?php echo trans('blog.para_two'); ?></p>
        <div class="comment_toggle"><a href="#" id="toggle2" class="comment_div"><i class="fa fa-commenting-o comment_icon" aria-hidden="true"></i><?php echo trans('blog.comments'); ?>(4)</a></div><br /><br />
                <div class="toggle4_toggle2" style="display:none;">
                  <p> Added forth chief trees but rooms think may.</p>
                  <p>Greatly who affixed suppose but enquire compact prepare all put.</p>
                  <p>Added forth chief trees but rooms think may.Tolerably earnestly middleton extremely distrusts she boy now not</p>
                </div>
      </div>
    </div>
    
  </article>
  
  
  <article class="timeline-entry">
    
    <div class="timeline-entry-inner">
        <div class="timeline-time"><span style=" color: #000; ">April 3, 2016</span> <span>3 <?php echo trans('blog.comments'); ?></span></div>
      
      <div class="timeline-icon"style="background-color: #2D2E30;
    color: #fff;">
        <p class="timeline_symbol">M</p>
      </div>
        <div class="timeline-label">
        <h2><p class="blog_para_head"><?php echo trans('blog.title_three'); ?></p></h2>
        <p class="comment more"><?php echo trans('blog.para_three'); ?></p>
        <div class="comment_toggle"><a href="#" id="toggle3" class="comment_div"><i class="fa fa-commenting-o comment_icon" aria-hidden="true"></i><?php echo trans('blog.comments'); ?>(5)</a></div><br /><br />
                <div class="toggle4_toggle3" style="display:none;">
                  <p> Added forth chief trees but rooms think may.</p>
                  <p>Greatly who affixed suppose but enquire compact prepare all put.</p>
                  <p>Added forth chief trees but rooms think may.Tolerably earnestly middleton extremely distrusts she boy now not</p>
                </div>
      </div>
    
  </article>

  
  <article class="timeline-entry">
  
    <div class="timeline-entry-inner">
      <div class="timeline-time"><span style=" color: #000; ">April 3, 2016</span> <span>3 <?php echo trans('blog.comments'); ?></span></div>
      <div class="timeline-icon" style="background-color: #45BAEE;
    color: #fff;">
        <p class="timeline_symbol">M</p>
      </div>
      <div class="timeline-label">
        <h2><p class="blog_para_head"><?php echo trans('blog.title_four'); ?></p></h2>
        <p class="comment more"><?php echo trans('blog.para_four'); ?></p>
        <div class="comment_toggle"><a href="#" id="toggle4" class="comment_div"><i class="fa fa-commenting-o comment_icon" aria-hidden="true"></i><?php echo trans('blog.comments'); ?>(3)</a></div><br /><br />
                <div class="toggle4_toggle4" style="display:none;">
                  <p> Added forth chief trees but rooms think may.</p>
                  <p>Greatly who affixed suppose but enquire compact prepare all put.</p>
                  <p>Added forth chief trees but rooms think may.Tolerably earnestly middleton extremely distrusts she boy now not</p>
                </div>
      </div>
    </div>
    
  </article>
 -->
<?php
$langType=trans('translate.lang');

function colorRand() {
            $background_colors = array('#282E33', '#DDDDDD', '#92C611', '#495E67', '#2D2E30');
            $rand_background = $background_colors[array_rand($background_colors)];
            return $rand_background;
            //return $color = dechex(rand(0x000000, 0xFFFFFF));
         }
              for($i=0;$i<sizeof($data);$i++) {
                $id= $data[$i]['_id'];
                $color = colorRand();
                if($color == 'fff' || $color == 'ffffff') {
                $color = colorRand();
                }
                $landing=url('bloglanding/'.$data[$i]['_id']);
				$readmore=trans('blog.read_more'); 
                echo "<article class='timeline-entry'>
  
    <div class='col-lg-9 timeline-entry-inner'>
    <div class='timeline-time'><span style='color: #000;font-weight: 600;'>" .date("Y-m-d", strtotime($data[$i]['created_on']))."</span></div>
      <div class='timeline-icon' style='background-color:".$color.";
    color: #fff;''>";
      if ($langType =="en") {
        echo "<p class='timeline_symbol'>" .$data[$i]['title_en'][0]."</p>";
      }
      else{
        echo "<p class='timeline_symbol'>" .$data[$i]['title_de'][0]."</p>";
      }
      echo"</div>
      <div class='timeline-label'>
        <div class='col-md-12 col-sm-12 col-xs-12' style='float:left;padding-bottom:10px;'>";
        if ($langType =="en") {
        echo "<h2><p class='blog_para_head'><a href=".$landing.">" .$data[$i]['title_en']."</a></p></h2>
        <p class='comment more'>" .$data[$i]['body_en']."</p>
		<a href=".$landing." class='morelink'>".$readmore."</a>
        <a href=".$data[$i]['link']." target='_blank'>".$data[$i]['link']."</a>
        </div>";
      }
      else {
        echo "<h2><p class='blog_para_head'>" .$data[$i]['title_de']."</p></h2>
        <p class='comment more'>" .$data[$i]['body_de']."</p>
		<a href=".$landing." class='morelink'>".$readmore."</a>
        <a href=".$data[$i]['link']." target='_blank'>".$data[$i]['link']."</a>
        </div>";
      }
        if($data[$i]['blog_image'] != 'null') {
          echo "<div class='col-md-12 col-lg-12 col-sm-12 col-xs-12' style='background: url(".$data[$i]['blog_image'].") no-repeat center #fff;float:left; background-size:contain; height:300px;margin:15px;'>
          </div>";
        }
		echo "<div class='fb-share-button' data-href='https://stafftimes.com/bloglanding/".$id."' data-layout='button' data-size='small' data-mobile-iframe='true'><a class='fb-xfbml-parse-ignore' target='_blank' href='http://test.stafftimes.com/bloglanding/".$id."'>Share</a></div>";

   echo " <div class='linked-in-btn'><script src='//platform.linkedin.com/in.js' type='text/javascript'> lang: en_US</script>
<script type='IN/Share' data-url='https://stafftimes.com/bloglanding/".$id."'></script></div>";

       echo "<div class='fb-comments' data-href='https://stafftimes.com/blog#".$id."' data-numposts='5' data-width='850' data-order-by='reverse_time' data-mobile='true' data-colorscheme='light'></div>
    </div>
    
  </article>";
}
?>
</div>
  </div>
</div>
</div>
<script>
function comment(blog_id,commentarea) 
{
  var comment = commentarea.value;
  alert(comment);
}
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=775297599308579";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php  include 'layout/main_page_footer.php';?>
