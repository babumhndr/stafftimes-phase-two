
<?php  include 'layout/header.php';?>
<style type="text/css">
    #ui-datepicker-div
    {
        z-index:55 !important;
    }
    #fakeLoader{
    display: none;
    top: inherit !important;
    left: inherit !important;
    background-color: rgb(238, 238, 238)!important;
    }
    .spinner2
    {
        left: 40% !important;
        top: 40% !important;
        color: #000;
    } 
    .container1 > div, .container2 > div, .container3 > div {
    background-color: #000 !important;
    }
     .dataTables_wrapper .dataTables_filter input {
    border: 1px;
    padding: 3px;
    }
    .dataTables_wrapper .table thead > tr .sorting_asc
    {
    background: #eeeeee !important;
    }
    .icon{
    font-size: 17px;
    }
    .total {
    background: rgba(158, 158, 158, 0.61) !important;
    border: 1px solid rgb(195, 195, 195) !important;
    }
    .activeBtn{
        display:block!important;
    }
    .inactiveBtn{
        display:none!important;
    }
    .color{
        color:#fff!important;
    }
    .btn + .btn {
        margin-left: auto!important;
        margin-right: auto!important;
    }
    .checkmark {
        display:inline-block;
        width: 22px;
        height:22px;
        margin: 0px 10px;
        -ms-transform: rotate(45deg); /* IE 9 */
        -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
        transform: rotate(45deg);
    }

    .checkmark_circle {
        position: absolute;
        width:22px;
        height:22px;
        background-color: #fff;
        border-radius:11px;
        left:0;
        top:0;
    }

    .checkmark_stem {
        position: absolute;
        width:3px;
        height:9px;
        background-color:#68b828;
        left:11px;
        top:6px;
    }

    .checkmark_kick {
        position: absolute;
        width:3px;
        height:3px;
        background-color:#68b828;
        left:8px;
        top:12px;
    }
    #part_one{
        float:none!important;
        margin: 0px auto!important;
        text-align: center!important;
    }
    .mot_dashboard_content {
        margin-right: 0px !important;
        margin-left: 0px !important;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .btn_style{
        width: 90px;
        margin: 0px auto;
        text-align: center;
        margin-left:none!important;
    }
    .no-sorting{
        width:20px!important;
    }
	.status_style1{
		color:#68b828;
	}
	.status_style2{
		color:#cc3f44;
	}
	.status_style3{
		color:#40bbea;
	}
  .subscribed_user_btn{
    float: right;
    font-size: 12px;
    background: #2392ec;
    border: none;
    padding: 6px;
  }
  .left_align_name
  {
    text-align: left !important;
    padding: 10px !important;
  }
  .error_msg {
    text-align: center;
    font-size: 20px;
    height: 500px;
}
.panel{
  padding: 20px 40px 50px 40px!important;
}
</style>
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<link rel="stylesheet" href="<?php echo url('assets/css/stats-reports.css')?>">
<link rel="stylesheet" href="<?php echo url('/assets/css/dashboard.css')?>">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link href="<?php echo url('css/fakeLoader.css')?>" rel="stylesheet"> 


 <div class="col-md-12"> 
 <div class="col-md-12 link">
<p> 
<span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a  href="<?php echo url('plansandprice')?>" ><?php echo trans ('header.Plans')?></a></span> /
<a ><?php echo trans ('header.subscribed_user')?></a>
</p>
</div>  
        <div class="row mot_dashboard_content">
            <div class="col-md-5 col-sm-6 parts" id="part_one">
                <div class="panel panel-default">
                    <div class="invite_people_icon">
                        <img src="<?php echo url('assets/images/invite_dashboard.png')?>" class="mail_box">
                    </div>
                    <div class="invite_people_heading">
                        <p><?php echo trans('dashboard.invite')?></p>
                    </div>
                    <div class="invite_people_sub_heading">
                        <p><?php echo trans('dashboard.invite_msg')?></p>
                    </div>
                    <div class="submit_invite" id="inviteForm">
                    <form id="invite" method="post" action="invitePeople" style="text-align: center;"><input type="text" class="invite_mail_id" id="name" name="name" placeholder="<?php echo trans('dashboard.invite_name')?>"></input><input type="text" class="invite_mail_id" id="email" name="email" placeholder="<?php echo trans('dashboard.invite_mail')?>"></input><input type="hidden" id="time" name="time"></input><button class="send_invite" type="submit" id="send_invite_btn"><?php echo trans('dashboard.invite_btn')?></button>
                        <div id="loader" style="display:none">
                            <img src="<?php echo url('image/hourglass.gif') ?>" style="width:35px; height: 35px;"/>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo trans('subscrpition.employee_subscribed');?></h3>
                    <a href="<?php echo url('plansandprice')?>" class="subscribed_user_btn"><?php echo trans ('header.Plans')?> </a>
                    <div class="panel-options" id="subBtn">
                      
                    </div> 
                </div>
                <div class="col-md-12" id="active" style="padding: 10px 40px;"><div class="col-md-12 error_msg"><p>Data not available</p></div></div>
                <div id="yesData" class="panel-body" style="display:none;">
                    <?php $languageTranslation=trans("translate.lang"); ?>
                    <script type="text/javascript">
                      var languageType="<?= $languageTranslation?>";
                      var languageUrl="";
                      if (languageType=="de") 
                      {
                          languageUrl="<?= url('js/German.json') ?>";
                      }
                      else
                      {
                          languageUrl="<?= url('js/English.json') ?>";
                      }
                    </script>
                    <script>
                  var companyId = localStorage.getItem("companyId");
                  var companyName = localStorage.getItem("companyName");
                  var companyEmail = localStorage.getItem("companyEmail");
                  var companyAmount = localStorage.getItem("companyAmount");
				  var companyCurrency = localStorage.getItem("companyCurrency");
                  var companyPlanMain = localStorage.getItem("companyPlanMain");

                  console.log(companyId+'---'+companyName+'---'+companyEmail+'---'+companyAmount+'---'+companyPlanMain);
                  jQuery(document).ready(function($)
                    {
                        $("#example-2").dataTable({
                            dom: "t" + "<'row'<'col-xs-6'i><'col-xs-6'p>>",
                            aoColumns: [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null
                            ],
                        });
                        
                        // Replace checkboxes when they appear
                        var $state = $("#example-2 thead input[type='checkbox']");
                        
                        $("#example-2").on('draw.dt', function()
                        {
                            cbr_replace();
                            
                            $state.trigger('change');
                        });
                        
                        // Script to select all checkboxes
                        function selectAll()
                        {
                            var $state = $("#example-2 thead input[type='checkbox']");
                            var $chcks = $("#example-2 tbody input[type='checkbox']");
                            alert($state);
                            if($state.is(':checked'))
                            {
                                $chcks.prop('checked', true).trigger('change');
                            }
                            else
                            {
                                $chcks.prop('checked', false).trigger('change');
                            }
                        };
                    });
                    </script>
                
                    
                    <table class="table table-bordered table-striped" id="example-2">
                        <thead>
                            <tr>
                                <th class='left_align_name'><?php echo trans('subscrpition.employee_name');?></th>
                                <th class='left_align_name'><?php echo trans('subscrpition.employee_email');?></th>
                                <th style="width:130px!important;"><?php echo trans('subscrpition.employee_date');?></th>
                                <th style="width:130px!important;"><?php echo trans('subscrpition.employee_renewal');?></th>
                                <th style="width:110px!important;"><?php echo trans('subscrpition.employee_verified');?></th>
                                <th style="width:130px!important;"><?php echo trans('subscrpition.employee_subscribed_status');?></th>
                            </tr>
                        </thead>
                        
                        <tbody class="middle-align" id="data_table">
                        
                            
                        </tbody>
                    </table>
                    
                </div>
            </div>
            </div>

 <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
  <script src="js/jquery.form.js"></script>
<script>
var subemployees = [];
                    function selectAll()
                        {
                            var $state = $("#example-2 thead input[type='checkbox']");
                            var check = $state.is(':checked');
                            var $chcks = $("#example-2 tbody input[type='checkbox']");
                            if(check){
                            $.ajax({
                                    url:"<?php echo url('subscribedemployeeslist')?>",
                                    type:'post',
                                    success: function(response)
                                    {  
                                    var pendingUsers = [];
                                    var empId = [];
                                    for (var j = 0; j < response['employee_list'].length; j++) { 
                                    pendingUsers[j] = response['employee_list'][j]['payment_status'];
                                    if(pendingUsers[j] == "pending" || pendingUsers[j] == "expired"){
                                        empId[j] = response['employee_list'][j]['_id'];
                                        subemployees.push(empId[j]);
                                    }      
                                  }
                                }
                            });

                            if($state.is(':checked'))
                            {
                                $chcks.prop('checked', true).trigger('change');
                            }
                            else
                            {
                                $chcks.prop('checked', false).trigger('change');
                            }
                          }
                          else{
                            subemployees = [];
                            if($state.is(':checked'))
                            {
                                $chcks.prop('checked', true).trigger('change');
                            }
                            else
                            {
                                $chcks.prop('checked', false).trigger('change');
                            } 
                          }
                        };

$( document ).ready(function() {
function employeeList(){
            var res=0;           
            var table = $("#example-2").DataTable();
            table.destroy();
            $.ajax({
                    url:"<?php echo url('subscribedemployeeslist')?>",
                    type:'post',
                    success: function(response)
                    {  
                    if(response['employee_list'].length == 0){
                      $('#active').show();
                      $('#yesData').hide();
                    }
                    else {
                      $('#active').hide();
                      $('#yesData').show(); 
                    }
                    var subscribedDate = [];
                    var renewalDate = [];
                    var paymentStatus = [];
                    var pendingTrue = [];
                    var activeTrue = [];
                    var expiredTrue = [];
                    var selectUser = [];
                    var selectStyle = [];
                    var subscribedBtn = [];
                    var subscribedBtnExp = [];
                    var status = [];
                    var alreadyDone = false;
                    for (var j = 0; j < response['employee_list'].length; j++) { 
                       subscribedDate[j] = moment(response['employee_list'][j]['subscription_start_date']).format("MMM Do YYYY");
                       renewalDate[j] = moment(response['employee_list'][j]['subscription_end_date']).format("MMM Do YYYY");
                       status[j] = response['employee_list'][j]['status'];
                       if(status[j] == 'pending'){
                       status[j] = "<?php echo trans('subscrpition.inactive');?>";
                       }
                       if(status[j] == 'active'){
                       status[j] = "<?php echo trans('subscrpition.active');?>";
                       }
                       if(status[j] == 'archive'){
                       status[j] = "<?php echo trans('subscrpition.archived');?>";
                       }
                       paymentStatus[j] = response['employee_list'][j]['payment_status'];
                       if(paymentStatus[j] == "pending" || paymentStatus[j] == "expired"){
                       if(alreadyDone == false){
                       //$('#subBtn').html("<a href='javascript:void(0)' onclick='subscribeFn()' class='btn btn-secondary color'><?php echo trans('subscrpition.subscribe');?></a>");
                       //$('#checkBtn').html("<input type='checkbox' onclick='selectAll()'>");
                       alreadyDone = true;
                       }
                       }
                       else{
                       if(alreadyDone == false){
                       //$('#subBtn').html("<a href='javascript:void(0)' class='btn btn-info color'><?php echo trans('subscrpition.all_subscribed');?></a>");
                       //$('#checkBtn').html("<span class='checkmark'><div class='checkmark_circle'></div><div class='checkmark_stem'></div><div class='checkmark_kick'></div></span>");
                       alreadyDone = false;
                       }
                       }

                       if(paymentStatus[j] == "pending"){
                       subscribedBtn[j] = 'display:none';
                       subscribedDate[j] = '-';
                       renewalDate[j] = '-';
                       }
                       else{
                       subscribedBtn[j] = 'display:block';
                       }

                       if(paymentStatus[j] == "pending"){
                       pendingTrue[j] = 'activeBtn';
                       }
                       else{
                       pendingTrue[j] = 'inactiveBtn';
                       }
                       if(paymentStatus[j] == "active"){
                       activeTrue[j] = 'activeBtn';
                       selectUser[j] = 'disabled = "true"'; 
                       selectStyle[j] = 'display:none';
                       }
                       else{
                       activeTrue[j] = 'inactiveBtn';
                       }
                       if(paymentStatus[j] == "expired"){
                       expiredTrue[j] = 'activeBtn';
                       }
                       else{
                       expiredTrue[j] = 'inactiveBtn';
                       }
                       res+="'<tr><input type='hidden' id='emp"+j+"' value="+response['employee_list'][j]['_id']+"><td class='left_align_name'>"+response['employee_list'][j]['name']+"</td> <td class='left_align_name'>"+response['employee_list'][j]['email']+"</td> <td>"+subscribedDate[j]+"</td> <td>"+renewalDate[j]+"</td> <td>"+status[j]+"</td> <td><span class='status_style1 "+activeTrue[j]+"'><?php echo trans('subscrpition.employee_subscribed_status');?></span><span class='status_style2 "+expiredTrue[j]+"'><?php echo trans('subscrpition.expired');?></span><span class='status_style3 "+pendingTrue[j]+"'><?php echo trans('subscrpition.not_yet');?></span></td></tr>'";
                  }
                  $('#data_table').html(res);
                      $("#example-2").dataTable({
                            dom: "t" + "<'row'<'col-xs-6'i><'col-xs-6'p>>",
                            aoColumns: [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null
                            ],
                                language: {
                        "url":languageUrl,
                        },
                        });

                }
                    
            });
         }
         employeeList();

       (function() {
        $('#send_invite_btn').hide();
        $('#loader').show();
        $('form').ajaxForm({
            beforeSend: function () {
                     var name = $('#name').val();
                    var email = $('#email').val();
                    var isValid = true;
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(name == null || email == null || name == '' || email == '')
                    {
                        isValid = false;
                     swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.all_fields');?>", "error");
                   }
                  /*  else if(regex.test(email) == false)
                    {
                        isValid = false;
                        swal("Error!", "Please check the Email address", "error");
                    }*/
                if(isValid == false)
                {
                    return false;
                }
                },
            uploadProgress: function() {
      
                },
            success: function(data) {

                if(data.status=='success')
                   {
                    $('#example-2').dataTable().fnDestroy();
                    employeeList();
                    swal("<?php echo trans('popup.great');?>", "<?php echo trans('popup.invite_employee');?>", "success");
                    $("#invite")[0].reset();
                   }
                  else if(data.status=='failure'){  
                    swal("<?php echo trans('popup.oops');?>", "<?php echo trans('popup.already_registered');?>", "error");
                  }     
                },      
            complete: function() {
    
                 }
            }); 

        })();
        $('#send_invite_btn').show();
        $('#loader').hide();  
        
    });


 
var subemployees = [];
function selectEmployee(item){
        var ischecked = $('#check'+item).is(':checked');
        if(!ischecked){
            var empId = $('#emp'+item).val();
            var index=subemployees.indexOf(empId)
            subemployees.splice(index,1); 
        }
        else{
            var empId = $('#emp'+item).val();
            subemployees.push(empId);
        } 

}

function subscribeFn(){
    var empLen = subemployees.length; 
    localStorage.setItem("memberCount", empLen);
	if(companyCurrency == 'EUR'){
	   var merchantAccountId = 'stafftimesEUR';
	   localStorage.setItem("merchantAccountId", merchantAccountId);
	}
	if(companyCurrency == 'CHF'){
	   var merchantAccountId = 'danstafftimescom';
	   localStorage.setItem("merchantAccountId", merchantAccountId);
	}
	if(companyCurrency == 'GBP'){
	   var merchantAccountId = 'stafftimesGBP';
	   localStorage.setItem("merchantAccountId", merchantAccountId);
	}
	if(companyCurrency == 'USD'){
	   var merchantAccountId = 'stafftimesUSD';
	   localStorage.setItem("merchantAccountId", merchantAccountId);
	}
    var totalAmount = empLen * companyAmount;
    localStorage.setItem("totalAmount", totalAmount);
    localStorage.setItem("memberIds", subemployees);
    //console.log(empLen);
    //console.log(totalAmount);
    //console.log(subemployees);
	if(empLen > 0){
		location.href = "payment/public_html/index.php";
	}
	else{
    swal("<?php echo trans('popup.oops');?>", "<?php echo trans('popup.atleast_one');?>", "error");
	}
    
}
</script>
<!-- Data Table Scripts -->
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
    
 <?php  include 'layout/footer.php';?>

