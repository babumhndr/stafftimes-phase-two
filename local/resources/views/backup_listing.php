<?php  include 'layout/header.php';?>
 <?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
    }
</script> 
<style type="text/css">
	.error_msg{
text-align: center; 
font-size: 20px; 
height: 500px; 
margin-top: 10px;
}
.link {
padding-top: 10px !important;
    padding-bottom: 10px !important;
    padding: 0px !important;
}
.template_link {
color: #2392ec;
}
.template_link a {
color: #2392ec;
}
</style>
			<!-- Basic Setup -->
			<div class="">
				
				<div class="panel-body">
					
					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$("#example-1").dataTable({
							aLengthMenu: [
								[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
							],
							 "order": [[ 0, "desc" ]],
							language: {
                        "url":languageUrl,
                        },
              					
						});
					});
					</script>
					<?php
						if($data['status'] == 'success'){
							$tableDisplay = "display:block";
							$noItem = "display:none";
						}
						else{
							$tableDisplay = "display:none";
							$noItem = "display:block";
						}
					?>
					<style>
      th, td { white-space: nowrap; }
        div.container {
        width: 100%;
        }
         .dataTables_scrollBody::-webkit-scrollbar-track
{
    //-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    //border-radius: 10px;
    background-color: #D4D4D4;
}

.dataTables_scrollBody::-webkit-scrollbar
{
    height: 6px;
    background-color: #67B0E1;
}

.dataTables_scrollBody::-webkit-scrollbar-thumb
{
   
    background-color: #67B0E1;
}
    </style>
    <div class="col-md-12 link">
        <p> 
        <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('backup')?>"><?php echo trans ('header.Backup')?></a></span> / 
        <a><?php echo trans ('translate.backup')?></a>
        </p>
    </div>
					<div style="<?=$tableDisplay?>">
					<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%" >
						<thead>
							<tr>
								<th><?php echo trans ('reports-1.date')?></th>
								<th><?php echo trans ('translate.backup')?></th>
							</tr>
						</thead>
					<tbody>

						<?php
						if ($data['status']=="success") 
						{
							for($i=0;$i<count($data['response']);$i++){
								$filrname='';
								$res=explode('/', $data['response'][$i]['link']);
								$filrname=str_replace(".zip","",$res[5]);
								$link='"'.$data['response'][$i]['link'].'"';
								$download= trans('translate.download');
								echo "<tr>
								<td>".$filrname." UTC</td>	
								<td><button onclick='backup(".$link.");'>".$download."</button></td> 
							</tr>";
							}
						}
						?>				
						</tbody>
					</table>
				</div>
					<div class="col-md-12 error_msg" style="<?=$noItem?>"><p><?php echo trans('manage_activity.data_not_available')?></p>
					</div>
					
				</div>
			</div>
<?php include 'layout/footer.php';?>
<script type="text/javascript">
	function backup(link)
	{
		window.location.href=link;
	}
</script>
<link rel="stylesheet" href="<?= url('assets/js/datatables/dataTables.bootstrap.css')?>">
<script src="<?= url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>
<!-- Imported scripts on this page -->
<script src="<?= url('assets/js/datatables/dataTables.bootstrap.js')?>"></script>
<script src="<?= url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js')?>"></script>
<script src="<?= url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>