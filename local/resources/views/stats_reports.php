
<?php  include 'layout/header.php';?>
<style type="text/css">
    #ui-datepicker-div
    {
        z-index:55 !important;
    }
    #fakeLoader{
    display: none;
    top: inherit !important;
    left: inherit !important;
    background-color: rgb(238, 238, 238)!important;
    }
    .spinner2
    {
        left: 40% !important;
        top: 40% !important;
        color: #000;
    } 
    .container1 > div, .container2 > div, .container3 > div {
    background-color: #000 !important;
    }
     .dataTables_wrapper .dataTables_filter input {
    border: 1px;
    padding: 3px;
    }
    .dataTables_wrapper .table thead > tr .sorting_asc
    {
    background: #eeeeee !important;
    }
    .icon{
    font-size: 17px;
    }
    @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
    .main-content {
        width: 100%;
    }
}
</style>
<?php 
$employeeName='';
if (!empty($data['employee'][0]['name']))
    {
        $employeeName=$data['employee'][0]['name'];
    }
 $separator=$data['separator'];
 $employeeBaseUrl=url('employee_profile/'.$data['employee_details']['employee_id']);
 $employeeProfileLink='<a href="'.$employeeBaseUrl.'" style="color:#2392ec">'.$employeeName.'</a>';
?>
<?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    var pageTrans="";
    var ofTrans="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
        pageTrans="Seite ";
        ofTrans=" von ";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
        pageTrans="Page ";
        ofTrans=" of ";
    }
</script>
<script type="text/javascript">
    var employeeName="<?= $employeeName?>";
    var separator='<?php if (!empty($separator['data'])) {echo $separator['data'];} ?>';
</script>
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<link rel="stylesheet" href="<?php echo url('assets/css/stats-reports.css')?>">
 <link href="<?php echo url('css/fakeLoader.css')?>" rel="stylesheet">
            <!-- <h3>Here starts everything&hellip;</h3> -->
            
            <div class="row reports">
                <div class="col-md-12 link">
                    <p> 
       <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('listing')?>" ><?php echo trans ('header.Listing')?></a></span> /
                   <a><?php echo trans ('report_links.title')." - ".$employeeProfileLink?></a>
                    </p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="fill">
                    <?php include 'layout/report_links.php' ?>   
                    <div class="col-md-12 col-sm-12 col-xs-12 second">
                    <div class="fakeloader" id="fakeLoader" ></div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-md-9 col-sm-12 col-xs-12 statsrespad">
                        <div class="fill" style=" text-align: center; ">
                        <form role="form" action="#">                       
                        <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <div class="fill">
                                <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.from')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="start_date" class="form-control ">         
                            </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.to')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="end_date" class="form-control">         
                            </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <div class="form-group">
                            <!-- <label class="radio-inline">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="radio-1" checked>
                                    Daily
                            </label> -->
                            <label class="radio-inline radio_range">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="weekly">
                                    <?php echo trans ('report_links.weekly')?>
                            </label>
                            <!-- <label class="radio-inline radio_range">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="monthly">
                                    <?php echo trans ('report_links.monthly')?>
                            </label>
                            <label class="radio-inline radio_range">
                                <input type="radio" name="radio-2" class="cbr cbr-blue " id="yearly">
                                    <?php echo trans ('report_links.yearly')?>
                            </label> -->
                        </div>
                        </div>
                        </form>
                        </div>
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 third">
                       <!--  <div class="col-md-3 col-sm-3 col-xs-12 center">
                            <p class="small_strong">My Date Range: 01/02/2016 - 23/03/2016</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 center">
                            <p class="small_strong">Balance For Range: 37.30</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 center">
                            <p class="small_strong">Balance at Range Start: 0.00</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 center">
                            <p class="small_strong">Balance at Range End: 0.00</p>
                        </div> -->
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 fourth">
                    <div class="panel-body over">
                    
                    <script type="text/javascript">
                    $(document).ready(function($)
                    {
                        $('#example-1').DataTable();
                    });
                    </script>
                    
                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="custom_width" style="width: 350px"><?php echo trans ('stats_reports.week')?></th>
                                <th><?php echo trans ('stats_reports.at_start')?></th>
                                <th><?php echo trans ('stats_reports.range')?></th>
                                <th><?php echo trans ('stats_reports.total')?></th>
                            </tr>
                        </thead>
                    
                        <tbody id="data_table">
                          <!--   <tr>
                                <td class="tiny">Week : 11 (1/1/2014 to 7/1/2014)</td>
                                <td class="tiny">8:00</td>
                                <td class="tiny">8:00</td>
                                <td class="tiny">8:00</td>

                            </tr> -->
  
                        </tbody>
                    </table>
                    
                </div>
                </div>
                    </div>
                </div>
            </div>
  </div>
 <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
   <script type="text/javascript" src="<?php echo url('js/datepicker-de.js')?>"></script>
 <script>
 function fillArray(value, len) {
  if (len == 0) return [];
  var a = [value-0.4+'%'];
  while (a.length * 2 <= len) a = a.concat(a);
  if (a.length < len) a = a.concat(a.slice(0, len - a.length));
  a[0]=value+4+'%';
  return a;
}
        function docWidth(docLength){
        if(docLength>0){
        var res =Math.floor(100/docLength);
        return fillArray(res,docLength);
        }else{
        return ['100%'];
        }
        }
 var titleOf=toTitleCase("<?php echo trans ('report_links.three_line_one')?> <?php echo trans ('report_links.three_line_two')?>");
 var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
  var time_style='<?php if (!empty($data['general_setting']['response']['0']['time_style'])) { echo $data['general_setting']['response']['0']['time_style']; } else { echo ""; } ?>';//time format from general settings
    var value_format='<?php if (!empty($data['general_setting']['response']['0']['value_format'])) { echo $data['general_setting']['response']['0']['value_format']; } else { echo ""; } ?>';//time format from general settings
        $(".left-links li a").click(function(){
        $(this).find('i').toggleClass('fa-indent fa-outdent');
        });

//check weekly onload
$('#weekly').click();

//date range ajax
var dateToday = '';
var employee_id="<?=$data['employee_details']['employee_id']?>";
var company_id="<?=$data['employee_details']['company_id']?>";
/*console.log(employee_id);
console.log(company_id);*/
var dates = $("#start_date, #end_date").datepicker({
    defaultDate: "+1w",
    firstDay: 1,
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    dateFormat:"dd-mm-yy",
    onSelect: function(selectedDate) {
        var option = this.id == "start_date" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
        $.datepicker.regional[languageType];
        var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
        localStorage.start_date=start_date;
        localStorage.end_date=end_date;
       /* console.log(start_date+'//'+end_date+'//');*/
        if(start_date!=''&&end_date!='')
        {
            $('#fakeLoader').show();
        if($('#weekly').prop("checked") == true){
            var res=0;
            var table = $('#example-1').DataTable();
            table.destroy();
            /*var table = $('#example-1').DataTable();
            table.destroy();*/
            $.ajax({
                    url:"<?php echo url('listingTimesheetWithRangeForStatistics/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    success: function(response)
                    {  
                    console.log(response);
                        var range=0;
                        var At_start=0;
                        var total=0;
                    for (var i = 0; i < response.length; i++) {
                        At_start=total+Number(response[i][0]['start_balance']);
                        At_start1=Number(response[i][0]['start_balance']);
                        range=Number(response[i][0]['range_balance']);
                        total=Number(total)+range+At_start1;

                    var atStartDisplay=minToHrs(At_start);
                    var rangeDisplay=minToHrs(range);
                    var totalDisplay=minToHrs(total);

                    if (time_style=='am/pm') 
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                            }
                    }
                    else if(time_style=='24 hours')
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                        {
                             if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                        }
                    }
                    else if(time_style=='Industrial')
                    {
                        atStartDisplay=timeToDecimal(atStartDisplay);
                        rangeDisplay=timeToDecimal(rangeDisplay);
                        totalDisplay=timeToDecimal(totalDisplay);
                        if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                    {
                                          atStartDisplay=atStartDisplay.replace('.',',');
                                        rangeDisplay=rangeDisplay.replace('.',',');
                                        totalDisplay=totalDisplay.replace('.',',');
                                    }
                            }
                    }

                     res+="'<tr> <td class='tiny'>"+response[i]['1']+"</td> <td class='tiny'>"+atStartDisplay+"</td> <td class='tiny'>"+rangeDisplay+"</td> <td class='tiny'>"+totalDisplay+"</td> </tr>'";
                        range='';
                    }
                    $('#data_table').html(res);
                    $('#example-1').DataTable( {
                         language: {
                        "url":languageUrl,
                        },
                    "columnDefs": [ {
                     "targets"  : 'no-sort',
                     "orderable": false,
                        "order": []
                     }],
                     dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                           doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                             doc.content[2].layout = 'noBorders';
                             doc.content[2].margin =[0, 0, 0, 10];
                             doc.styles.tableHeader.fontSize = 7;
                             doc['footer']=(function(page, pages) {
                                                return {
                                                columns: ['',
                                                {
                                                alignment: 'right',
                                                text: [pageTrans,
                                                { text: page.toString(), italics: true },
                                                ofTrans,
                                                { text: pages.toString(), italics: true }
                                                ]
                                                }
                                                ],
                                                margin: [10, 0]
                                                }
                                            });
                             doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        }
                        ],
                        "bSort": false,
                    });
                    $('#fakeLoader').hide();
                    }
                });
                }

                //for month radio button 
            if($('#monthly').prop("checked") == true){
                var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetForStatisticsMonths/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                   success: function(response)
                    {  
                    console.log(response);
                        var range=0;
                        var At_start=0;
                        var total=0;
                    for (var i = 0; i < response.length; i++) {
                        At_start=total+Number(response[i][0]['start_balance']);
                        At_start1=Number(response[i][0]['start_balance']);
                        range=Number(response[i][0]['range_balance']);
                        total=Number(total)+range+At_start1;

                    var atStartDisplay=minToHrs(At_start);
                    var rangeDisplay=minToHrs(range);
                    var totalDisplay=minToHrs(total);

                    if (time_style=='am/pm') 
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                            }
                    }
                    else if(time_style=='24 hours')
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                        {
                             if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                        }
                    }
                    else if(time_style=='Industrial')
                    {
                        atStartDisplay=timeToDecimal(atStartDisplay);
                        rangeDisplay=timeToDecimal(rangeDisplay);
                        totalDisplay=timeToDecimal(totalDisplay);
                        if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                    {
                                          atStartDisplay=atStartDisplay.replace('.',',');
                                        rangeDisplay=rangeDisplay.replace('.',',');
                                        totalDisplay=totalDisplay.replace('.',',');
                                    }
                            }
                    }

                     res+="'<tr> <td class='tiny'>"+response[i]['1']+"</td> <td class='tiny'>"+atStartDisplay+"</td> <td class='tiny'>"+rangeDisplay+"</td> <td class='tiny'>"+totalDisplay+"</td> </tr>'";
                        range='';
                    }
                    $('#data_table').html(res);
                    $('#example-1').DataTable( {
                         language: {
                        "url":languageUrl,
                        },
                    "columnDefs": [ {
                     "targets"  : 'no-sort',
                     "orderable": false,
                        "order": []
                     }],
                     dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                            doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                             doc.content[2].layout = 'noBorders';
                             doc.content[2].margin =[0, 0, 0, 10];
                             doc.styles.tableHeader.fontSize = 7;
                             doc['footer']=(function(page, pages) {
                                                return {
                                                columns: ['',
                                                {
                                                alignment: 'right',
                                                text: [pageTrans,
                                                { text: page.toString(), italics: true },
                                                ofTrans,
                                                { text: pages.toString(), italics: true }
                                                ]
                                                }
                                                ],
                                                margin: [10, 0]
                                                }
                                            });
                             doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        }
                        ],
                        "bSort": false,
                    });
                    $('#fakeLoader').hide();
                    }
                }); 
            }

              //for month radio button 
            if($('#yearly').prop("checked") == true){
                var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetForStatisticsYearly/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    success: function(response)
                    {  
                    console.log(response);
                        var range=0;
                        var At_start=0;
                        var total=0;
                    for (var i = 0; i < response.length; i++) {
                        At_start=total+Number(response[i][0]['start_balance']);
                        At_start1=Number(response[i][0]['start_balance']);
                        range=Number(response[i][0]['range_balance']);
                        total=Number(total)+range+At_start1;

                    var atStartDisplay=minToHrs(At_start);
                    var rangeDisplay=minToHrs(range);
                    var totalDisplay=minToHrs(total);

                    if (time_style=='am/pm') 
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                            }
                    }
                    else if(time_style=='24 hours')
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                        {
                             if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                        }
                    }
                    else if(time_style=='Industrial')
                    {
                        atStartDisplay=timeToDecimal(atStartDisplay);
                        rangeDisplay=timeToDecimal(rangeDisplay);
                        totalDisplay=timeToDecimal(totalDisplay);
                        if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                    {
                                          atStartDisplay=atStartDisplay.replace('.',',');
                                        rangeDisplay=rangeDisplay.replace('.',',');
                                        totalDisplay=totalDisplay.replace('.',',');
                                    }
                            }
                    }

                     res+="'<tr> <td class='tiny'>"+response[i]['1']+"</td> <td class='tiny'>"+atStartDisplay+"</td> <td class='tiny'>"+rangeDisplay+"</td> <td class='tiny'>"+totalDisplay+"</td> </tr>'";
                        range='';
                    }
                    $('#data_table').html(res);
                    $('#example-1').DataTable( {
                         language: {
                        "url":languageUrl,
                        },
                    "columnDefs": [ {
                     "targets"  : 'no-sort',
                     "orderable": false,
                        "order": []
                     }],
                     dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                          doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc.styles.tableHeader.fontSize = 7;
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                 doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        }
                        ],
                        "bSort": false,
                    });
                    $('#fakeLoader').hide();
                    }
                }); 
            }
        }
    }
});
//on click radio button
$('#weekly').click(function(){
var start_date1=$('#start_date').val();
var end_date1=$('#end_date').val();
if(start_date1!=''&&end_date1!='')
        {
            $('#fakeLoader').show();
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
        if($('#weekly').prop("checked") == true){
             var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetWithRangeForStatistics/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                    success: function(response)
                    {  
                    console.log(response);
                        var range=0;
                        var At_start=0;
                        var total=0;
                    for (var i = 0; i < response.length; i++) {
                        At_start=total+Number(response[i][0]['start_balance']);
                        At_start1=Number(response[i][0]['start_balance']);
                        range=Number(response[i][0]['range_balance']);
                        total=Number(total)+range+At_start1;

                    var atStartDisplay=minToHrs(At_start);
                    var rangeDisplay=minToHrs(range);
                    var totalDisplay=minToHrs(total);

                    if (time_style=='am/pm') 
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                            }
                    }
                    else if(time_style=='24 hours')
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                        {
                             if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                        }
                    }
                    else if(time_style=='Industrial')
                    {
                        atStartDisplay=timeToDecimal(atStartDisplay);
                        rangeDisplay=timeToDecimal(rangeDisplay);
                        totalDisplay=timeToDecimal(totalDisplay);
                        if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                    {
                                          atStartDisplay=atStartDisplay.replace('.',',');
                                        rangeDisplay=rangeDisplay.replace('.',',');
                                        totalDisplay=totalDisplay.replace('.',',');
                                    }
                            }
                    }

                     res+="'<tr> <td class='tiny'>"+response[i]['1']+"</td> <td class='tiny'>"+atStartDisplay+"</td> <td class='tiny'>"+rangeDisplay+"</td> <td class='tiny'>"+totalDisplay+"</td> </tr>'";
                        range='';
                    }
                    $('#data_table').html(res);
                    $('#example-1').DataTable( {
                         language: {
                        "url":languageUrl,
                        },
                    "columnDefs": [ {
                     "targets"  : 'no-sort',
                     "orderable": false,
                        "order": []
                     }],
                     dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                           doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc.styles.tableHeader.fontSize = 7;
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                 doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        }
                        ],
                        "bSort": false,
                    });
                    $('#fakeLoader').hide();
                    }
                });
                }

        }

});

$('#monthly').click(function(){
var start_date1=$('#start_date').val();
var end_date1=$('#end_date').val();
if(start_date1!=''&&end_date1!='')
        {
            $('#fakeLoader').show();
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
         //for month radio button 
            if($('#monthly').prop("checked") == true){
                var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetForStatisticsMonths/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                   success: function(response)
                    {  
                    console.log(response);
                        var range=0;
                        var At_start=0;
                        var total=0;
                    for (var i = 0; i < response.length; i++) {
                        At_start=total+Number(response[i][0]['start_balance']);
                        At_start1=Number(response[i][0]['start_balance']);
                        range=Number(response[i][0]['range_balance']);
                        total=Number(total)+range+At_start1;
                        
                    var atStartDisplay=minToHrs(At_start);
                    var rangeDisplay=minToHrs(range);
                    var totalDisplay=minToHrs(total);

                    if (time_style=='am/pm') 
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                            }
                    }
                    else if(time_style=='24 hours')
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                        {
                             if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                        }
                    }
                    else if(time_style=='Industrial')
                    {
                        atStartDisplay=timeToDecimal(atStartDisplay);
                        rangeDisplay=timeToDecimal(rangeDisplay);
                        totalDisplay=timeToDecimal(totalDisplay);
                        if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                    {
                                          atStartDisplay=atStartDisplay.replace('.',',');
                                        rangeDisplay=rangeDisplay.replace('.',',');
                                        totalDisplay=totalDisplay.replace('.',',');
                                    }
                            }
                    }

                     res+="'<tr> <td class='tiny'>"+response[i]['1']+"</td> <td class='tiny'>"+atStartDisplay+"</td> <td class='tiny'>"+rangeDisplay+"</td> <td class='tiny'>"+totalDisplay+"</td> </tr>'";
                        range='';
                    }
                    $('#data_table').html(res);
                    $('#example-1').DataTable( {
                         language: {
                        "url":languageUrl,
                        },
                    "columnDefs": [ {
                     "targets"  : 'no-sort',
                     "orderable": false,
                        "order": []
                     }],
                     dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                           doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc.styles.tableHeader.fontSize = 7;
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                 doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        }
                        ],
                        "bSort": false,
                    });
                    $('#fakeLoader').hide();
                    }
                }); 
            }

        }

});
$('#yearly').click(function(){
var start_date1=$('#start_date').val();
var end_date1=$('#end_date').val();
if(start_date1!=''&&end_date1!='')
        {
            $('#fakeLoader').show();
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
        //for month radio button 
            if($('#yearly').prop("checked") == true){
                var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetForStatisticsYearly/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                    success: function(response)
                    {  
                    console.log(response);
                        var range=0;
                        var At_start=0;
                        var total=0;
                    for (var i = 0; i < response.length; i++) {
                       At_start=total+Number(response[i][0]['start_balance']);
                        At_start1=Number(response[i][0]['start_balance']);
                        range=Number(response[i][0]['range_balance']);
                        total=Number(total)+range+At_start1;

                    var atStartDisplay=minToHrs(At_start);
                    var rangeDisplay=minToHrs(range);
                    var totalDisplay=minToHrs(total);

                    if (time_style=='am/pm') 
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                            }
                    }
                    else if(time_style=='24 hours')
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                        {
                             if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                        }
                    }
                    else if(time_style=='Industrial')
                    {
                        atStartDisplay=timeToDecimal(atStartDisplay);
                        rangeDisplay=timeToDecimal(rangeDisplay);
                        totalDisplay=timeToDecimal(totalDisplay);
                        if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                    {
                                          atStartDisplay=atStartDisplay.replace('.',',');
                                        rangeDisplay=rangeDisplay.replace('.',',');
                                        totalDisplay=totalDisplay.replace('.',',');
                                    }
                            }
                    }

                     res+="'<tr> <td class='tiny'>"+response[i]['1']+"</td> <td class='tiny'>"+atStartDisplay+"</td> <td class='tiny'>"+rangeDisplay+"</td> <td class='tiny'>"+totalDisplay+"</td> </tr>'";
                        range='';
                    }
                    $('#data_table').html(res);
                    $('#example-1').DataTable( {
                         language: {
                        "url":languageUrl,
                        },
                    "columnDefs": [ {
                     "targets"  : 'no-sort',
                     "orderable": false,
                        "order": []
                     }],
                     dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                          doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc.styles.tableHeader.fontSize = 7;
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                 doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        }
                        ],
                        "bSort": false,
                    });
                    $('#fakeLoader').hide();
                    }
                }); 
            }
        }

});

//on load report

$(window).load(function(){

var today = new Date();
var dayOfWeekStartingSundayZeroIndexBased = today.getDay(); // 0 : Sunday ,1 : Monday,2,3,4,5,6 : Saturday
var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()-6);
var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+7);
var start_date1 =moment(mondayOfWeek).format('D-M-YYYY');
var end_date1 =moment(sundayOfWeek).format('D-M-YYYY');
if (typeof(localStorage.start_date)=='undefined') { localStorage.start_date=start_date1}
if (typeof(localStorage.end_date)=='undefined') { localStorage.end_date=end_date1}
$('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
$('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));
start_date1 =localStorage.getItem("start_date");
end_date1 =localStorage.getItem("end_date");

if(start_date1!=''&&end_date1!='')
        {
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
        if($('#weekly').prop("checked") == true){
             var res=0;
                var table = $('#example-1').DataTable();
                table.destroy();
            $.ajax({
                    url:"<?php echo url('listingTimesheetWithRangeForStatistics/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                    success: function(response)
                    {  
                    console.log(response);
                        var range=0;
                        var At_start=0;
                        var total=0;
                    for (var i = 0; i < response.length; i++) {
                        At_start=total+Number(response[i][0]['start_balance']);
                        At_start1=Number(response[i][0]['start_balance']);
                        range=Number(response[i][0]['range_balance']);
                        total=Number(total)+range+At_start1;

                    var atStartDisplay=minToHrs(At_start);
                    var rangeDisplay=minToHrs(range);
                    var totalDisplay=minToHrs(total);

                    if (time_style=='am/pm') 
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                            }
                    }
                    else if(time_style=='24 hours')
                    {
                        if(value_format=='8.15')
                            {
                                atStartDisplay=atStartDisplay.replace(':','.');
                                rangeDisplay=rangeDisplay.replace(':','.');
                                totalDisplay=totalDisplay.replace(':','.');
                            }
                        else if(value_format=="Device region")
                        {
                             if(separator=="comma")
                                {
                                    atStartDisplay=atStartDisplay.replace(':',',');
                                    rangeDisplay=rangeDisplay.replace(':',',');
                                    totalDisplay=totalDisplay.replace(':',',');
                                }
                                else if(separator=="dot")
                                {
                                    atStartDisplay=atStartDisplay.replace(':','.');
                                    rangeDisplay=rangeDisplay.replace(':','.');
                                    totalDisplay=totalDisplay.replace(':','.');
                                }
                        }
                    }
                    else if(time_style=='Industrial')
                    {
                        atStartDisplay=timeToDecimal(atStartDisplay);
                        rangeDisplay=timeToDecimal(rangeDisplay);
                        totalDisplay=timeToDecimal(totalDisplay);
                        if(value_format=="Device region")
                            {
                                if(separator=="comma")
                                    {
                                          atStartDisplay=atStartDisplay.replace('.',',');
                                        rangeDisplay=rangeDisplay.replace('.',',');
                                        totalDisplay=totalDisplay.replace('.',',');
                                    }
                            }
                    }

                     res+="'<tr> <td class='tiny'>"+response[i]['1']+"</td> <td class='tiny'>"+atStartDisplay+"</td> <td class='tiny'>"+rangeDisplay+"</td> <td class='tiny'>"+totalDisplay+"</td> </tr>'";
                        range='';
                    }
                    $('#data_table').html(res);
                    $('#example-1').DataTable( {
                         language: {
                        "url":languageUrl,
                        },
                    "columnDefs": [ {
                     "targets"  : 'no-sort',
                     "orderable": false,
                        "order": []
                     }],
                     dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                          doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc.styles.tableHeader.fontSize = 7;
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                 doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf,
                        message:employeeName+'\n'+companyOf
                        }
                        ],
                        "bSort": false,
                    });
                    $('#fakeLoader').hide();
                    }
                });
                }

        }

});

 //60th to 100th unit for time format industrial
   function timeToDecimal(time)
        {
              substring = "-";
          checkNegative=(time.indexOf(substring) !== -1);
          if(checkNegative==true)
          {
          time=time.replace('-','');
             Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return '-'+parseFloat(Hours + Minutes/60,10).toFixed(2);
           }
           else
           {
            Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return parseFloat(Hours + Minutes/60).toFixed(2);
           }
        }
        //change caps to title case
        function toTitleCase(str)
        {
             return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }

        function minToHrs(minutes) {
        var sign ='';
        if(minutes < 0){
        sign = '-';
        }

        var hours = Math.floor(Math.abs(minutes) / 60);
        var minutes = leftPad(Math.abs(minutes) % 60);

        return sign + hours +':'+minutes;

        }

        function leftPad(number) {  
        return ((number < 10 && number >= 0) ? '0' : '') + number;
        }
    </script>
    <script src="<?php echo url('js/fakeLoader.min.js')?>"></script>
    <script type="text/javascript">
    $(".fakeloader").fakeLoader({
    timeToHide:15000, //Time in milliseconds for fakeLoader disappear
    zIndex:"999",//Default zIndex
    spinner:"spinner2",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
    bgColor:"#2392ec", //Hex, RGB or RGBA colors
    });
    </script>
<!-- Data Table Scripts -->
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
 <script type="text/javascript" src="<?php echo url('js/buttons.html5.min.js')?>"></script>
 <script type="text/javascript" src="<?php echo url('js/pdfmake.min.js')?>"></script>
 <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo url('assets/js/timepicker/bootstrap-timepicker.min.js')?>"></script>

    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
    
 <?php  include 'layout/footer.php';?>

