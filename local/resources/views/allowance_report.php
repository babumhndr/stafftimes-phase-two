
<?php  include 'layout/header.php';?>
<style type="text/css">
    #ui-datepicker-div
    {
        z-index:55 !important;
    }
    #fakeLoader{
    display: none;
    top: inherit !important;
    left: inherit !important;
    background-color: rgb(238, 238, 238)!important;
    }
    .spinner2
    {
        left: 40% !important;
        top: 40% !important;
        color: #000;
    } 
    .container1 > div, .container2 > div, .container3 > div {
    background-color: #000 !important;
    }
     .dataTables_wrapper .dataTables_filter input {
    border: 1px;
    padding: 3px;
    }
    .dataTables_wrapper .table thead > tr .sorting_asc
    {
    background: #eeeeee !important;
    }
    .icon{
    font-size: 17px;
    }
    .total {
        background: rgba(158, 158, 158, 0.61) !important;
        border: 1px solid #d2d2d2 !important;
    }
    .align_left{
      text-align: left !important;
    }
    .align_left_tr{
        text-align: left !important;
        padding-left: 10px !important;
    }
</style>
<?php 
$employeeName='';
if (!empty($data['employee'][0]['name']))
    {
        $employeeName=$data['employee'][0]['name'];
    }
 $separator=$data['separator'];
 $employeeBaseUrl=url('employee_profile/'.$data['employee_details']['employee_id']);
 $employeeProfileLink='<a href="'.$employeeBaseUrl.'" style="color:#2392ec">'.$employeeName.'</a>';
 $offset=0;
$general_setting=$data['general_setting']; 
if (!empty($general_setting['response'])) { $offset=$general_setting['response']['0']['set_working_hours'];} 
$offset=hrsToMin($offset);
?>
<?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    var pageTrans="";
    var ofTrans="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
        pageTrans="Seite ";
        ofTrans=" von ";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
        pageTrans="Page ";
        ofTrans=" of ";
    }
</script>
<script type="text/javascript">
    var employeeName="<?= $employeeName?>";
    var separator='<?php if (!empty($separator['data'])) {echo $separator['data'];} ?>';
</script>
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<link rel="stylesheet" href="<?php echo url('assets/css/stats-reports.css')?>">
 <link href="<?php echo url('css/fakeLoader.css')?>" rel="stylesheet">
            <!-- <h3>Here starts everything&hellip;</h3> -->
            
            <div class="row reports">
                <div class="col-md-12 link">
                    <p> 
                          <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('listing')?>" ><?php echo trans ('header.Listing')?></a></span> /
                   <a><?php echo trans ('report_links.title')." - ".$employeeProfileLink?></a>
                    </p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="fill">
                    <?php include 'layout/report_links.php' ?>  
                    <div class="fakeloader" id="fakeLoader" ></div> 
                    <div class="col-md-12 col-sm-12 col-xs-12 fourth">
                    <div class="panel-body over">
                    
                    <script type="text/javascript">
                    var titleOf=toTitleCase("<?php echo trans ('report_links.six_line_one')?> <?php echo trans ('report_links.six_line_two')?>");
                    var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
                    $(document).ready(function($)
                    {
                        $('#example-1').DataTable({
                             language: {
                        "url":languageUrl,
                        },
                            dom: 'Bfrtip',
                             buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        message:employeeName+'\n'+companyOf,
                        title: titleOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                 doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }
                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf
                        }
                        ],
                        "bSort": false,
                        });
                    });

                    function toTitleCase(str)
        {
             return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }
                    </script>
                    
                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="align_left_tr"><?php echo trans ('allowance_report.activity')?></th>
                                <th><?php echo trans ('allowance_report.allowance')?></th>
                                <th><?php echo trans ('allowance_report.date')?></th>
                                <th><?php echo trans ('allowance_report.applied')?></th>
                                <th><?php echo trans ('allowance_report.balance')?></th>
                            </tr>
                        </thead>
                    
                        <tbody id="data_table">
                            <?php
                            function minToHrs($hrs)
                            {
                                $hours  = floor($hrs/60); //round down to nearest minute. 
                                       $minutes = $hrs % 60;
                                       $hrs=$hours.".".$minutes;
                                        return $hrs;
                            }
                            function hrsToMin($hours) 
                            { 
                            $minutes = 0;
                            if (strpos($hours, ':') !== false) 
                            { 
                            // Split hours and minutes. 
                            list($hours, $minutes) = explode(':', $hours); 
                            } 
                            return $hours * 60 + $minutes; 
                            }
                            $allownace='';
                            $res='';
                            $res1='';
                            if (!empty($data['allownace'])) {
                            $allownace=$data['allownace'];
                            }
                            /*print_r($allownace);*/
                            $div='';
                            $totalHours='';
                            if($allownace!='')
                            {
                                if(count($allownace[0])>0)
                                {
                                for ($i=0; $i <sizeof($allownace) ; $i++) 
                                { 
                                if(count($allownace[$i])>0)
                                {
                                    $res='';
                                    $res1='';
                                    $totalHours='';
                                    for ($j=0; $j < sizeof($allownace[$i]); $j++) 
                                    { 
                                        if ($j==0) 
                                        {
                                            $div.="<tr><td class='tiny align_left'>".$allownace[$i][$j]['timesheetActivity']['activity_name']."</td>
                                            <td class='tiny'></td>
                                            <td class='tiny'>".$allownace[$i][$j]['date']."</td>
                                            <td class='tiny'>".$allownace[$i][$j]['total_hours']."</td>
                                            <td class='tiny'></td></tr>";
                                        }
                                        else
                                        {
                                           $div.="<tr><td class='tiny'>,,</td>
                                            <td class='tiny'></td>
                                            <td class='tiny'>".$allownace[$i][$j]['date']."</td>
                                            <td class='tiny'>".$allownace[$i][$j]['total_hours']."</td>
                                            <td class='tiny'></td></tr>";      
                                        }
                                        $res=$allownace[$i][$j]['timesheetActivity']['allowance_bank_hours'];
                                        $res1=$allownace[$i][$j]['timesheetActivity']['allowance_bank_days'];
                                       $totalHours=$totalHours+$allownace[$i][$j]['total_hours_min'];
                                    }
                                        $totalHoursFinal=$totalHours;
                                        $totalHoursFinal=number_format(minToHrs($totalHoursFinal), 2, '.', '');
                                        $totalHoursDay=$totalHours/$offset;
                                        $totalHoursDay=number_format($totalHoursDay, 2, '.', '');
                                        $res=str_replace(':','.',$res);
                                        $res1=number_format($res1, 2, '.', '');
                                        $balance=$res-$totalHoursFinal;
                                        $balanceFinal=$res1-$totalHoursDay;
                                        /*echo $balance."s".$balanceFinal;*/
                                        $div.="<tr><td class='tiny total'>".trans ('allowance_report.total_hrs')."</td>
                                            <td class='tiny total'>".$res."</td>
                                            <td class='tiny total'></td>
                                            <td class='tiny total'>". $totalHoursFinal."</td>
                                            <td class='tiny total'>".$balance."</td></tr>"; 
                                            $div.="<tr><td class='tiny total'>". trans ('allowance_report.total_days')."</td>
                                            <td class='tiny total'>".$res1."</td>
                                            <td class='tiny total'></td>
                                            <td class='tiny total'>". $totalHoursDay."</td>
                                            <td class='tiny total'>".$balanceFinal."</td></tr>"; 
                                }
                            }
                            echo $div;
                            }
                            }
                            else
                            {
                                echo "";
                            }
                            ?>
                        </tbody>
                    </table>
                    
                </div>
                </div>
                    </div>
                </div>
            </div>
            <div>
 <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
 <script>
  var time_style='<?php if (!empty($data['general_setting']['response']['0']['time_style'])) { echo $data['general_setting']['response']['0']['time_style']; } else { echo ""; } ?>';//time format from general settings
    var value_format='<?php if (!empty($data['general_setting']['response']['0']['value_format'])) { echo $data['general_setting']['response']['0']['value_format']; } else { echo ""; } ?>';//time format from general settings
        $(".left-links li a").click(function(){
        $(this).find('i').toggleClass('fa-indent fa-outdent');
        });

var employee_id="<?=$data['employee_details']['employee_id']?>";
var company_id="<?=$data['employee_details']['company_id']?>";


 //60th to 100th unit for time format industrial
   function timeToDecimal(time)
        {
              substring = "-";
          checkNegative=(time.indexOf(substring) !== -1);
          if(checkNegative==true)
          {
          time=time.replace('-','');
             Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return '-'+parseFloat(Hours + Minutes/60,10).toFixed(2);
           }
           else
           {
            Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return parseFloat(Hours + Minutes/60).toFixed(2);
           }
        }
    </script>
    <script src="<?php echo url('js/fakeLoader.min.js')?>"></script>
    <script type="text/javascript">
    $(".fakeloader").fakeLoader({
    timeToHide:15000, //Time in milliseconds for fakeLoader disappear
    zIndex:"999",//Default zIndex
    spinner:"spinner2",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
    bgColor:"#2392ec", //Hex, RGB or RGBA colors
    });
    </script>
<!-- Data Table Scripts -->
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
 <script type="text/javascript" src="<?php echo url('js/buttons.html5.min.js')?>"></script>
 <script type="text/javascript" src="<?php echo url('js/pdfmake.min.js')?>"></script>
 <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo url('assets/js/timepicker/bootstrap-timepicker.min.js')?>"></script>

    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
    
 <?php  include 'layout/footer.php';?>

