<?php  include 'layout/header.php';?>
<style type="text/css">
@media (min-width: 1600px){
.centered {
  width:1100px;
  position:relative;
  margin-left:auto;
  margin-right:auto;
}
}
</style>
<?php 
$currency='';
$price1='';
$price2='';
$price3='';
$planData=$data['plans'];
$defaultPlanData=$data['defaultPlans'];
/*echo $planData;
echo $defaultPlanData;*/
if (sizeof($planData)>0) 
{
  /*echo "ssdf";*/
  $currency=$planData[0]['currency_code'];
  for ($i=0; $i < sizeof($planData); $i++) 
  { 
    if ($planData[$i]['plan']=="A") {
      $price1=$planData[$i]['plan_price'];
    }
    else if ($planData[$i]['plan']=="B") {
      $price2=$planData[$i]['plan_price'];
    }
    else if ($planData[$i]['plan']=="C") {
      $price3=$planData[$i]['plan_price'];
    }
  }
}
else 
{
  /*echo "ssdf";*/
  $currency=$defaultPlanData[0]['currency_code'];
  for ($i=0; $i < sizeof($defaultPlanData); $i++) 
  { 
    if ($defaultPlanData[$i]['plan']=="A") {
      $price1=$defaultPlanData[$i]['plan_price'];
    }
    else if ($defaultPlanData[$i]['plan']=="B") {
      $price2=$defaultPlanData[$i]['plan_price'];
    }
    else if ($defaultPlanData[$i]['plan']=="C") {
      $price3=$defaultPlanData[$i]['plan_price'];
    }
  }
}
?>
  <link rel="stylesheet" href="<?php echo url('assets/css/pricesandplanning.css')?>">

      <!-- <h3>Here starts everything&hellip;</h3> -->
      <div class="row mot_price_content">
      <div class="col-md-12">
        <p> 
        <span class="template_link">
        <a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> /
        <a><?php echo trans ('pricing.title')?></a>
        </p>
      </div>
      <div class="col-md-12 content">
        <div class="centered">
        <div class="col-lg-4 col-md-4 col-sm-6 pricing_table column1">
       <!--  <div class="divBlur1" style="display:block;"></div> -->
              <div class="whole">
                <div class="pricing_panel" style="background-image: url('assets/images/green.png');">
                  <div class="pricing_panel-heading">
                   <div class="ribbon-wrapper-yellow"><div class="ribbon-yellow"><?php echo trans ('pricing.tag_one')?></div></div>
                  <p class="heading"><?php echo trans ('pricing.plan_one')?></p>         
                  </div>
                  <div class="pricing_panel_sub_heading">
                    <p class="text"><?=$price1?>.<span class="tiny">00 <?=$currency?></span></p>
                  </div>
                </div>
                <div class="pricing_panel_listing">
                  <div class="subscribe_button">
                    <button id="btn1" class="sub_btn"><?php echo trans ('pricing.subscribe_button')?></button>
                  </div>
                  <div class="panel_list">
                    <ul class="list-unstyled">
                      <li>
                        <p><?php echo trans('popup.feature_one_a');?></p>
                      </li>
                      <li class="grey">
                        <p class="text_list"><?=$price1?>.00 <?=$currency?>&nbsp;&nbsp;<?php echo trans('popup.feature_two_a');?></p>
                      </li>
                      <li>
                        <p><?php echo trans('popup.feature_three_a');?></p>
                      </li>
                      <li class="grey">
                        <p><?php echo trans('popup.feature_four_a');?></p>
                      </li>
                      <li>
                        <p><?php echo trans('popup.feature_five_a');?></p>
                      </li>
                      <li class="grey end">
                        <p><?php echo trans('popup.feature_six_a');?></p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 pricing_table column2">
            <!-- <div class="divBlur2" style="display:block;"></div> -->
              <div class="whole">
                <div class="pricing_panel" style="background-image: url('assets/images/Blue.png');">
                  <div class="pricing_panel-heading">
                  <div class="ribbon-wrapper-green"><div class="ribbon-green"><?php echo trans ('pricing.tag_two')?></div></div>
                  <p class="heading"><?php echo trans ('pricing.plan_two')?></p>          
                  </div>
                  <div class="pricing_panel_sub_heading">
                    <p class="text"><?=$price2?>.<span class="tiny">00 <?=$currency?></span></p>
                  </div>
                </div>
                <div class="pricing_panel_listing">
                  <div class="subscribe_button">
                    <button id="btn2" class="sub_btn"><?php echo trans ('pricing.subscribe_button')?></button>
                  </div>
                  <div class="panel_list">
                    <ul class="list-unstyled">
                      <li>
                        <p><?php echo trans('popup.feature_one_b');?></p>
                      </li>
                      <li class="grey">
                        <p class="text_list"><?=$price2?>.00 <?=$currency?>&nbsp;&nbsp;<?php echo trans('popup.feature_two_b');?></p>
                      </li>
                      <li>
                        <p><?php echo trans('popup.feature_three_b');?></p>
                      </li>
                      <li class="grey">
                        <p><?php echo trans('popup.feature_four_b');?></p>
                      </li>
                      <li>
                        <p><?php echo trans('popup.feature_five_b');?></p>
                      </li>
                      <li class="grey end">
                        <p><?php echo trans('popup.feature_six_b');?></p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 pricing_table column3">
            <!-- <div class="divBlur3" style="display:block;"></div> -->
              <div class="whole">
                <div class="pricing_panel" style="background-image: url('assets/images/Untitled-3.png');">
                  <div class="pricing_panel-heading">
                  <div class="ribbon-wrapper-red"><div class="ribbon-red"><?php echo trans ('pricing.tag_three')?></div></div>
                  <p class="heading"><?php echo trans ('pricing.plan_three')?></p>          
                  </div>
                  <div class="pricing_panel_sub_heading">
                    <p class="text"><?=$price3?>.<span class="tiny">00 <?=$currency?></span></p>
                  </div>
                </div>
                <div class="pricing_panel_listing">
                  <div class="subscribe_button">
                    <button id="btn3" class="sub_btn"><?php echo trans ('pricing.subscribe_button')?></button>
                  </div>
                  <div class="panel_list">
                    <ul class="list-unstyled">
                      <li>
                        <p><?php echo trans('popup.feature_one_c');?></p>
                      </li>
                      <li class="grey">
                        <p class="text_list"><?=$price3?>.00 <?=$currency?>&nbsp;&nbsp;<?php echo trans('popup.feature_two_c');?></p>
                      </li>
                      <li>
                        <p><?php echo trans('popup.feature_three_c');?></p>
                      </li>
                      <li class="grey">
                        <p><?php echo trans('popup.feature_four_c');?></p>
                      </li>
                      <li>
                        <p><?php echo trans('popup.feature_five_c');?></p>
                      </li>
                      <li class="grey end">
                        <p><?php echo trans('popup.feature_six_c');?></p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-lg-3 col-md-4 col-sm-6 pricing_table column4">
              <div class="whole on_load">
                <div class="pricing_panel" style="background-image: url('assets/images/Gray.png');">
                  <div class="pricing_panel-heading">
                  <div class="ribbon-wrapper-blue"><div class="ribbon-blue"><?php echo trans ('pricing.tag_four')?></div></div>
                  <p class="heading"><?php echo trans ('pricing.plan_four')?></p>          
                  </div>
                  <div class="pricing_panel_sub_heading">
                    <p class="text">$39.<span class="tiny">00</span></p>
                  </div>
                </div>
                <div class="pricing_panel_listing">
                  <div class="subscribe_button">
                    <button id="btn4" class="sub_btn"><?php echo trans ('pricing.subscribe_button')?></button>
                  </div>
                  <div class="panel_list">
                    <ul class="list-unstyled">
                      <li>
                        <p>Lorem Ipsum dolor</p>
                      </li>
                      <li class="grey">
                        <p>Lorem Ipsum dolor</p>
                      </li>
                      <li>
                        <p>Lorem Ipsum dolor</p>
                      </li>
                      <li class="grey">
                        <p>Lorem Ipsum dolor</p>
                      </li>
                      <li>
                        <p>Lorem Ipsum dolor</p>
                      </li>
                      <li class="grey end">
                        <p>Lorem Ipsum dolor</p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
      </div>
      </div>
    </div>
    
      
    <!-- start: Chat Section -->
    <!-- <div id="chat" class="fixed">
      
      <div class="chat-inner">
      
        
        <h2 class="chat-header">
          <a href="#" class="chat-close" data-toggle="chat">
            <i class="fa-plus-circle rotate-45deg"></i>
          </a>
          
          Chat
          <span class="badge badge-success is-hidden">0</span>
        </h2>
        
        <script type="text/javascript">
        // Here is just a sample how to open chat conversation box
        jQuery(document).ready(function($)
        {
          var $chat_conversation = $(".chat-conversation");
          
          $(".chat-group a").on('click', function(ev)
          {
            ev.preventDefault();
            
            $chat_conversation.toggleClass('is-open');
            
            $(".chat-conversation textarea").trigger('autosize.resize').focus();
          });
          
          $(".conversation-close").on('click', function(ev)
          {
            ev.preventDefault();
            $chat_conversation.removeClass('is-open');
          });
        });
        </script>
        
        
        <div class="chat-group">
          <strong>Favorites</strong>
          
          <a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
          <a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
          <a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
          <a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
        </div>
        
        
        <div class="chat-group">
          <strong>Work</strong>
          
          <a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
        </div>
        
        
        <div class="chat-group">
          <strong>Other</strong>
          
          <a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
          <a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
          <a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
          <a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
          <a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
          <a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
          <a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
          <a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
          <a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
          <a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
        </div>
      
      </div>
      
 -->      <!-- conversation template -->
      <!-- <div class="chat-conversation">
        
        <div class="conversation-header">
          <a href="#" class="conversation-close">
            &times;
          </a>
          
          <span class="user-status is-online"></span>
          <span class="display-name">Arlind Nushi</span> 
          <small>Online</small>
        </div>
        
        <ul class="conversation-body">  
          <li>
            <span class="user">Arlind Nushi</span>
            <span class="time">09:00</span>
            <p>Are you here?</p>
          </li>
          <li class="odd">
            <span class="user">Brandon S. Young</span>
            <span class="time">09:25</span>
            <p>This message is pre-queued.</p>
          </li>
          <li>
            <span class="user">Brandon S. Young</span>
            <span class="time">09:26</span>
            <p>Whohoo!</p>
          </li>
          <li class="odd">
            <span class="user">Arlind Nushi</span>
            <span class="time">09:27</span>
            <p>Do you like it?</p>
          </li>
        </ul>
        
        <div class="chat-textarea">
          <textarea class="form-control autogrow" placeholder="Type your message"></textarea>
        </div>
        
      </div>
      
    </div> -->
    <!-- end: Chat Section -->
    
    
  </div>
<style>
.divBlur1 {
    position: absolute;
    border: 1px solid #e5e5e5;
    box-shadow: 1px 1px 5px #999;
    width: 100%;
    float: left;
    height: 588px;
    opacity: 0.1;
    text-align: center;
    background-color: #f1f1f1;
    border-radius: 8px;
}
.divBlur2 {
    position: absolute;
    border: 1px solid #e5e5e5;
    box-shadow: 1px 1px 5px #999;
    width: 100%;
    float: left;
    height: 588px;
    opacity: 0.1;
    text-align: center;
    background-color: #f1f1f1;
    border-radius: 8px;
}
.divBlur3 {
    position: absolute;
    border: 1px solid #e5e5e5;
    box-shadow: 1px 1px 5px #999;
    width: 100%;
    float: left;
    height: 588px;
    opacity: 0.1;
    text-align: center;
    background-color: #f1f1f1;
    border-radius: 8px; 
}
</style>
<script>
  $( document ).ready(function() {
    var userDetails = <?php echo Auth::user();?>;
	var currency = '<?=$currency?>';
	localStorage.setItem("companyCurrency", currency);
    
    localStorage.setItem("companyId", userDetails['_id']);
    localStorage.setItem("companyLang", userDetails['language_name']);
    localStorage.setItem("companyName", userDetails['company_name']);
    localStorage.setItem("companyEmail", userDetails['email']);

    var employeeCount = <?php echo count($data['employee_list']);?>;
    if(employeeCount >= 0 && employeeCount <= 20){
     $('.column1 .whole').addClass('on_load');
     $('.divBlur1').hide();
     $('.divBlur2').show();
     $('.divBlur3').show();  
     var price1 = '<?=$price1?>';	 
     localStorage.setItem("companyPlanMain", "Plan1");
     localStorage.setItem("companyAmount", price1);    
    }
    if(employeeCount >= 21 && employeeCount <= 40){
     $('.column2 .whole').addClass('on_load');
     $('.divBlur1').show();
     $('.divBlur2').hide();
     $('.divBlur3').show();
	 var price2 = '<?=$price2?>';
     localStorage.setItem("companyPlanMain", "Plan2");
     localStorage.setItem("companyAmount", price2);
    }
    if(employeeCount >= 41){
     $('.column3 .whole').addClass('on_load');
     $('.divBlur1').show();
     $('.divBlur2').show();
     $('.divBlur3').hide();
	 var price3 = '<?=$price3?>';
     localStorage.setItem("companyPlanMain", "Plan3");
     localStorage.setItem("companyAmount", price3);
    }
 });
</script>
  
  <script>
    $(".left-links li a").click(function(){
      $(this).find('i').toggleClass('fa-indent fa-outdent')
    });
  
  $("#btn1").click(function(){
    var employeeCount = <?php echo count($data['employee_list']);?>;
    if(employeeCount >= 0 && employeeCount <= 20){
      window.location.href = 'subscribedemployees'; 
    }
    else{
      swal({
        title: "Oops!",
        text: "You cannot opt for this plan!",
        type: "warning",
        html: true
      });
    }
  });

  $("#btn2").click(function(){
    var employeeCount = <?php echo count($data['employee_list']);?>;
    if(employeeCount >= 21 && employeeCount <= 40){
      window.location.href = 'subscribedemployees'; 
    }
    else{
      swal({
        title: "Oops!",
        text: "You cannot opt for this plan!",
        type: "warning",
        html: true
      }); 
    }
  });

  $("#btn3").click(function(){
    var employeeCount = <?php echo count($data['employee_list']);?>;
    if(employeeCount >= 41){
      window.location.href = 'subscribedemployees'; 
    }
    else{
      swal({
        title: "Oops!",
        text: "You cannot opt for this plan!",
        type: "warning",
        html: true
      });
    }
  });

  </script>
  <script>
    $( ".whole" ).hover(function() {
      $( ".whole" ).removeClass("on_load");
    
  });
  </script>

 <?php  include 'layout/footer.php';?>