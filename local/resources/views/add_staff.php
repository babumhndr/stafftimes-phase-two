
<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/add_employee.css')?>">
		
<style type="text/css">
	body{background-color: #fff !important;}
	/* -- quantity box -- */

.quantity {
 display: inline-block; }

.quantity .input-text.qty {
 width: 45px;
 height: 39px;
 padding: 0px;
 text-align: center;
 background-color: #fff;
 border: 1px solid #efefef;
 color: #3084dc;
}

.quantity.buttons_added {
 text-align: left;
 position: relative;
 white-space: nowrap;
 vertical-align: top; }

.quantity.buttons_added input {
 display: inline-block;
 margin: 0;
 vertical-align: top;
 box-shadow: none;
}

.quantity.buttons_added .minus,
.quantity.buttons_added .plus {

 background-color: #ffffff;
 border: 1px solid #efefef;
 cursor:pointer;
 width: 20px;
 height: 20.5px;
 
}

.quantity.buttons_added .minus{
 position:relative;
     top: 18px;
    left: -26px;
 border-left: none;
 border-top :1px solid #efefef;
 margin: 0px;
}
.quantity.buttons_added .plus{
 position:relative;
 border-bottom :1px solid #efefef;
 border-left: none;
 margin: 0px;
 position: relative;
    left: -2px;
}

.quantity.buttons_added .minus:hover,
.quantity.buttons_added .plus:hover {
 background: #eeeeee; }

.quantity input::-webkit-outer-spin-button,
.quantity input::-webkit-inner-spin-button {
 -webkit-appearance: none;
 -moz-appearance: none;
 margin: 0; }
 
 .quantity.buttons_added .minus:focus,
.quantity.buttons_added .plus:focus {
 outline: none; }


 .custom_table{
 	border:1px solid #ddd;
 	text-align: center;
 	width: 100%;
 }

 .custom_table > tbody > tr > td{
 	border-right:1px solid #ddd;
 	padding: 10px 0px;
 }
 .first-row{
 	border-bottom:1px solid #ddd;
 }
 .zero-row{
 	background-color: #fbfcfe;
 	height: 80px;
 }

 tags{
 	font-size: 16px;
 	display: inline-block;
 	vertical-align: middle;
 	padding: 8px;
 }
 .date{
 	color: #3084dc;
 	font-weight: bold;
 	padding-left: 10px;
 	padding-right: 10px;
 }
.vac-sett{
	padding: 25px 0px;
	font-size: 14px;
}
.vaction_sett{
	-webkit-box-shadow: 0 0 2px #ccc;
	    background-color: #fff;
    margin: 20px 0;
    border-radius: 3px;
}



/* Hide the browser's default radio button */
.container_radio input {
    position: absolute;
    opacity: 0;
    cursor: pointer;

}

/* Create a custom radio button */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 18px;
    width: 18px;
    background-color: #eee;
    border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container_radio:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container_radio input:checked ~ .checkmark {
    background-color: #fff;
    border: 2px solid #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.container_radio input:checked ~ .checkmark:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.container_radio .checkmark:after {
 	top: 3px;
	left: 3px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: #2196F3;
}

.container_radio {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    margin-top: 8px;
}

.teamlist{
	border:1px dashed #aaa;
	border-radius: 4px;
	background-color: #fafbff;
	min-height: 123px;
	padding: 10px;
}

.projectlist{
	border:1px solid #aaa;
	border-radius: 4px;
	background-color: #fff;
	min-height: 123px;
	padding: 10px;
}
chips{
	border:1px solid #eee;
	padding: 4px 5px;
	border-radius: 4px;
	background-color: #fff;
	margin:5px 5px;
	color: #aaa;
	display: inline-block;
}
.det{
	border: 1px solid #eee;
	padding: 5px;
}
.empDet{
	margin: 5px 0px;
}
.detBottom{
	border: 1px solid #eee;
	padding: 5px;
	background-color: #f6fafd;
	min-height: 60px;
	margin-bottom: 50px;
}
.navline{
	width: 100%;
	padding: 50px;
}
</style>
<div id="one">
	<div class="navline">

		<div class="navTitle" >
			<div class="col-md-2" style="margin-left: 40px; color: #2392ec" >
			<strong>Add an employee</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set ToE(Terms)</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Timesheet Locks</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Team & Projects</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Preview & Publish</strong>
			</div>
		</div>
		<img src="image/prog/1 Progress bar.png" class="img-responsive" width="100%">
	</div>
	<div class="col-sm-12 addEmloyeeMain">
		<div class="col-sm-12 addEmloyee">
			<h3 class="addEmpHead">Add an employee</h3>
			<div class="col-sm-6">
				<div class="form-group">
				  <label for="usr">Staff Name</label>
				  <input type="text" class="form-control addempinput" placeholder="Your Text">
				</div>
				<div class="form-group">
				  <label for="pwd">Email Id</label>
				  <input type="password" class="form-control addempinput" placeholder="Your Text">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
				  <label for="usr">Staff Id</label>
				  <input type="text" class="form-control addempinput" placeholder="Your Text">
				</div>
				<div class="form-group">
				  <label for="pwd">Phone Number</label>
				  <input type="password" class="form-control addempinput" placeholder="Your Text">
				</div>
			</div>
			<div class="col-sm-12 addempBtndiv">
				<p class="addempInfo">** An invite will be sent to the employee to download & install the stafftimes employee app</p>
				<button class="btn-primary btn addempBtn" id="btnOne">Proceed to the next step</button>
			</div>
		</div>
	</div>
</div>

<div id="two">
	<div class="navline">

		<div class="navTitle" >
			<div class="col-md-2" style="margin-left: 40px;" >
			<strong>Add an employee</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;color: #2392ec;">
				<strong>Set ToE(Terms)</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Timesheet Locks</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Team & Projects</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Preview & Publish</strong>
			</div>
		</div>
		<img src="image/prog/2 Progress bar.png" class="img-responsive" width="100%">
	</div>
	<div class="col-sm-12">
		<h3 class="addEmpHead">Set Terms of Employment(ToE) for <a href="">Babin Kochana</a></h3>
		<div class="col-sm-5 empworkformMain">
			<div class="form-group">
				<label for="usr">Select job title</label>
				 <input type="text" class="form-control addempinput" placeholder="Enter your text">
			</div>
			<div class="form-group">
				<label for="pwd">select job type</label>
				<select class="selectpicker form-control" style="height: 43px;">
				  <option>Full time job</option>
				  <option>Part time job</option>
				  <option>Freelancer</option>
				</select>

			</div>
			<div class="form-group">
				<label for="usr">Select start date</label>
				<div class="input-group">
		           <input type="date" class="form-control addempinput" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status">
		           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar" style="color: blue"></i></span>
		         </div>
			</div>
			<div class="form-group">
				<label for="usr">Select End date</label>
				<div class="input-group">
		           <input type="date" class="form-control addempinput" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status">
		           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar" style="color: blue"></i></span>
		         </div>
			</div>
		</div>

		<div class="col-sm-7">
			<div class="col-md-10 col-md-offset-1 empworkCalMain bg_opacity" >
			<h4 class="termsofemphead">Working hours (offset)</h4>
			<div class="col-sm-12 pad0">
				<div class="col-sm-9 pad0">
				<p class="termsempsub">standard working hours</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsub alignrightval">40.00</p>
				</div>
			</div>
			<div class="col-sm-12 pad0 btm-hr" >
				<div class="col-sm-9 pad0">
				<p class="termsempsub">Employment ratio</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsub alignrightval">* 100%</p>
				</div>
			</div>
			<div class="col-sm-12 pad0">
				<div class="col-sm-9 pad0">
				<p class="termsempsubmain">Working hours per week</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsubmain alignrightval">40.00</p>
				</div>
			</div>
			<div class="col-sm-12 pad0 btm-hr">
				<div class="col-sm-9 pad0">
				<p class="termsempsub">Number of working days per week</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsub alignrightval">5 d</p>
				</div>
			</div>
			<div class="col-sm-12 pad0">
				<div class="col-sm-9 pad0">
				<p  class="termsempsubmain">Working hours per day</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsubmain alignrightval">5 d</p>
				</div>
			</div>
			<h4 class="termsofemphead">Leave of absence</h4>
			<div class="col-sm-12 pad0">
				<div class="col-sm-9 pad0">
				<p class="termsempsub">standard leave of absence</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsub alignrightval">20 d</p>
				</div>
			</div>
			<div class="col-sm-12 pad0 btm-hr" >
				<div class="col-sm-9 pad0">
				<p class="termsempsub">5 day work week</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsub alignrightval">100%</p>
				</div>
			</div>
			<div class="col-sm-12 pad0">
				<div class="col-sm-9 pad0">
				<p class="termsempsubmain">annual leave</p>
				</div>
				<div class="col-sm-3 pad0">
				<p class="termsempsubmain alignrightval">20 d</p>
				</div>
			</div>
		</div>
		</div>
		<div class="col-sm-12 addEmloyeeMain">
		<h3 class="addEmpHead">Workhours setting for <a href="">Babin Kochana</a></h3>
		<table class="custom_table">
			<tr class="zero-row">
				<td colspan="2" style="border: none;">
					<tags>Employment ratio</tags> 
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td colspan="2" style="border: none;">
					<tags>Employment ratio</tags> 
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td colspan="2" style="border: none;">
					<tags>Employment ratio</tags> 
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td colspan="2" style="border: none;">
					<tags>Employment ratio</tags> 
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
			</tr>
			<tr class="first-row">
				<td></td>
				<td>MON</td>
				<td>TUE</td>
				<td>WED</td>
				<td>THU</td>
				<td>FRI</td>
				<td>SAT</td>
				<td>SUN</td>
			</tr>
			<tr>
				<td>AM</td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
			</tr>
			<tr>
				<td>PM</td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
				<td><input type="checkbox" class="check" name=""></td>
			</tr>
			<tr>
				<td>%</td>
				<td>100%</td>
				<td>100%</td>
				<td>100%</td>
				<td>100%</td>
				<td>100%</td>
				<td>100%</td>
				<td>100%</td>
			</tr>
			<tr>
				<td>Decimal style</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
			</tr>
			<tr>
				<td>Time format</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
				<td>8:00</td>
			</tr>
			<tr>
				<td>Apply offset</td>
				<td style="padding: 7px 0px;">
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td style="padding: 7px 0px;">
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td style="padding: 7px 0px;">
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td style="padding: 7px 0px;">
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td style="padding: 7px 0px;">
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td style="padding: 7px 0px;">
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				<td style="padding: 7px 0px;">
					<div class="quantity buttons_added">
						<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
						<input type="button" value="+" class="plus">
						<input type="button" value="-" class="minus">
					</div>
				</td>
				
			</tr>
		</table>
	</div>
	</div>
	<div class="col-sm-12">
		<h3 class="addEmpHead">Vacation setting for <a href="">Babin Kochana</a></h3>
			<div class="col-sm-12 ">
				<div class="col-sm-12 vaction_sett">
					<h4>Annual leave allowance for <span class="date">01-01-2017</span> To <span class="date">31-02-2017</span></h4>
					<div class="col-sm-3 vac-sett">
						Full year equivalent <strong>364</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Entitlement pro rated <strong>20.00</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Carry over <strong>2.00</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Adjustment 
						<span style="position: relative;top: -10px;margin-left: 15px;">
							<div class="quantity buttons_added">
								<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
								<input type="button" value="+" class="plus">	
								<input type="button" value="-" class="minus">
							</div>
						</span>
					</div>
					<div class="col-sm-3 vac-sett">
						Total working days <strong>364</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Total leaves in hours <strong>160</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Taken this year <strong>0.00</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Remaining balance <strong style="color: #36c45e;">22.00</strong>
					</div>
				</div>
			</div>
			<div style="text-align: center;margin: 50px 0px;">
				<button class="btn-primary btn addempBtn" id="btnTwo">Save Terms of Employment</button>	
			</div>
			
	</div>
	<div class="col-sm-12" style="background-color: #eee;">

          <h3>Babin's ToE History</h3>
			<br>
         <table class="table table-striped">
              <thead style="background: #edf7ff">
                   <th>Sl No</th>
                   <th>ToE (DESIGNATION NAME)</th>
                   <th>FROM DATE</th>
                   <th>TO DATE</th>
                   <th>WORKING DAYS</th>
                   <th>TOTAL LEAVES</th>
                   <th>LEAVES TAKEN</th>
                   <th>LEAVE BALANCE</th>

              </thead>
              <tr>
                   <td>2</td>
                   <td> Senior sales leader(current)</td>
                   <td>01-01-2018</td>
                   <td>31-12-2018</td>
                   <td>364</td>
                   <td>20</td>
                   <td>00</td>
                   <td>22</td>
              </tr>
              <tr>
                   <td>2</td>
                   <td> Senior</td>
                   <td>01-01-2018</td>
                   <td>31-12-2018</td>
                   <td>364</td>
                   <td>20</td>
                   <td>00</td>
                   <td>22</td>
              </tr>
              <tr>
                   <td>2</td>
                   <td> Senior sales leader(current)</td>
                   <td>01-01-2018</td>
                   <td>31-12-2018</td>
                   <td>364</td>
                   <td>20</td>
                   <td>00</td>
                   <td>22</td>
              </tr>
              <tr>
                   <td>2</td>
                   <td> Senior sales leader(current)</td>
                   <td>01-01-2018</td>
                   <td>31-12-2018</td>
                   <td>364</td>
                   <td>20</td>
                   <td>00</td>
                   <td>22</td>
              </tr>
              <tr>
                   <td>2</td>
                   <td> Senior sales leader(current)</td>
                   <td>01-01-2018</td>
                   <td>31-12-2018</td>
                   <td>364</td>
                   <td>20</td>
                   <td>00</td>
                   <td>22</td>
              </tr>
              <tr>
                   <td>2</td>
                   <td> Senior sales leader(current)</td>
                   <td>01-01-2018</td>
                   <td>31-12-2018</td>
                   <td>364</td>
                   <td>20</td>
                   <td>00</td>
                   <td>22</td>
              </tr>
         </table>
	</div>
</div>

<div id="three">
	<div class="navline">

		<div class="navTitle" >
			<div class="col-md-2" style="margin-left: 40px;" >
			<strong>Add an employee</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set ToE(Terms)</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;color: #2392ec;">
				<strong>Set Timesheet Locks</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Team & Projects</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Preview & Publish</strong>
			</div>
		</div>
		<img src="image/prog/3 Progress bar.png" class="img-responsive" width="100%">
	</div>
	<div class="col-sm-12" style="margin:50px 0px">
		<div class="col-sm-12 ">
			<div class="col-md-9">
				<div class="col-md-8">
					<h3 class="addEmpHead">Change Timesheet Lock Setting for</h3>
				</div>
				<div class="col-md-4">
					<select class="selectpicker form-control dropdwn">
					  <option>THIS EMPLOYEE ONLY</option>
					  <option>ALL EMPLOYEES</option>
					</select>
				</div>


				<div class="row">
					<div class="col-md-12">
					  <p>Lock setting</p>

					  <div class="col-md-1">
					  	<label class="container_radio">
						  <input type="radio" checked="checked" name="lock_setting">
						  <span class="checkmark"></span>
						</label>
					  </div>
					  <div class="col-md-8">
					  	<input type="text" class="form-control addempinput" placeholder="Lock all timesheets until X number of days ago">
					  </div>
					  <div class="col-md-3">
					  	<div class="quantity buttons_added">
							<input type="number" step="1" min="1" max="" name="quantity" value="2.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
							<input type="button" value="+" class="plus">
							<input type="button" value="-" class="minus">
						</div>

						<span>Days</span>
					  </div>
					  
					</div>


					<div class="col-md-12">
					  <p style="margin: 20px 80px;">(OR)</p>

					  <div class="col-md-1">
					  	<label class="container_radio">
						  <input type="radio" checked="checked" name="lock_setting">
						  <span class="checkmark"></span>
						</label>
					  </div>
					  <div class="col-md-8">
					  	<input type="text" class="form-control addempinput" placeholder="Lock all timesheets until previous month end">
					  </div>
					  
					  
					</div>
					
				</div>

				<div class="row" style="margin-top: 50px;">
					<div class="col-md-12">
					  <p>Remainders</p>

					  <div class="col-md-1">
					  	<label class="container_radio">
						  <input type="radio" checked="checked" name="remainder">
						  <span class="checkmark"></span>
						</label>
					  </div>
					  <div class="col-md-8">
					  	<input type="text" class="form-control addempinput" placeholder="Remainders to be sent X number of days before locking">
					  </div>
					  <div class="col-md-3">
					  	<div class="quantity buttons_added">
							<input type="number" step="1" min="1" max="" name="quantity" value="10.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
							<input type="button" value="+" class="plus">
							<input type="button" value="-" class="minus">
						</div>

						<span>Days</span>
					  </div>
					  
					</div>


					<div class="col-md-12">
					   <p style="margin: 20px 80px;">(OR)</p>

					  <div class="col-md-1">
					  	<label class="container_radio">
						  <input type="radio" checked="checked" name="remainder">
						  <span class="checkmark"></span>
						</label>
					  </div>
					  <div class="col-md-8">
					  	<input type="text" class="form-control addempinput" placeholder="Remainder to sent on Nth day of the month">
					  </div>
					  <div class="col-md-3">
					  	<div class="quantity buttons_added">
							<input type="number" step="1" min="1" max="" name="quantity" value="15" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
							<input type="button" value="+" class="plus">
							<input type="button" value="-" class="minus">
						</div>

						<span>Days</span>
					  </div>
					  
					</div>
					
				</div>

			</div>
			<div class="col-md-3 locking_range" >
				<p class="lock_title">Locking Range</p>

				<div class="form-group">
					<label for="usr">From</label>
					<div class="input-group">
			           <input type="date" class="form-control addempinput" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status">
			           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar" style="color: blue"></i></span>
			         </div>
				</div>
				<div class="form-group">
					<label for="usr">To</label>
					<div class="input-group">
			           <input type="date" class="form-control addempinput" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status">
			           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar" style="color: blue"></i></span>
			         </div>
				</div>
			</div>
			
			<div class="col-sm-12 addempBtndiv">
				
				<button class="btn-primary btn addempBtn" id="btnThree">Save & move to the next step..</button>
			</div>
		</div>
	</div>
</div>

<div id="four">
	<div class="navline">

		<div class="navTitle" >
			<div class="col-md-2" style="margin-left: 40px;" >
			<strong>Add an employee</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set ToE(Terms)</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Timesheet Locks</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;color: #2392ec;">
				<strong>Set Team & Projects</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Preview & Publish</strong>
			</div>
		</div>
		<img src="image/prog/4 Progress bar.png" class="img-responsive" width="100%">
	</div>
	<div class="col-sm-12" style="margin:50px 0px">
		<div class="col-sm-12 ">
			<h3 class="addEmpHead">Assign <span style="color: #2392ec;">Babin</span> to a team</h3>
					<p>You can change assignment of an employee to a team, anytime from "Manage Teams" section</p>


			<div class="row vaction_sett" style="padding-bottom: 30px;">
				
				<div class="col-md-6">
						<p style="margin: 20px 10px;">Team</p>
					<div class="col-md-12">
					  	<input type="text" class="form-control addempinput" placeholder="Select a team / department">
					</div>

					<div class="col-md-12">
					  <p style="margin: 10px 10px;">(OR) Add a new team</p>
					  <div class="input-group">
						  <input type="text" class="form-control addempinput" placeholder="Enter team / department name" aria-describedby="basic-addon2">
						  <span class="input-group-addon text-info" id="basic-addon2">Add Team</span>
						</div>
					</div>
				</div>
				<div class="col-md-6">
						<p style="margin: 20px 10px;">Employees currently in this team (5)</p>
					<div class="col-md-12 teamlist">
					  	<chips><span class="glyphicon glyphicon-user"></span> John Doe</chips>
					  	<chips><span class="glyphicon glyphicon-user"></span> Patrika Lake</chips>
					  	<chips><span class="glyphicon glyphicon-user"></span> Nitin Naik</chips>
					  	<chips><span class="glyphicon glyphicon-user"></span> Prithviraj</chips>
					  	<chips><span class="glyphicon glyphicon-user"></span> Abhijith</chips>
					  	<chips><span class="glyphicon glyphicon-user"></span> Lokesh</chips>
					</div>
				</div>

			</div>
		</div>
			
		<div class="col-sm-12 ">
			<h3 class="addEmpHead">Assign <span style="color: #2392ec;">Babin</span> to projects</h3>
					<p>You can change assignment of an employee to one or more projects, anytime from "Manage Projects" section</p>


			<div class="row" style="padding-bottom: 30px;">
			
				<div class="col-md-12">
					<div class="col-md-12 projectlist">
					  	<chips>Stafftimes <span class="glyphicon glyphicon-remove"></span> </chips>
					  	<chips>My overtime  <span class="glyphicon glyphicon-remove"></span> </chips>
					</div>
				</div>

			</div>
		</div>
		
			
			<div class="col-sm-12 addempBtndiv">
				
				<button class="btn-primary btn addempBtn" id="btnFour">Great! Preview & Publish User Profile</button>
			</div>
	</div>
</div>

<div id="five">
	<div class="navline">

		<div class="navTitle" >
			<div class="col-md-2" style="margin-left: 40px; " >
			<strong>Add an employee</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set ToE(Terms)</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Timesheet Locks</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px;">
				<strong>Set Team & Projects</strong>
			</div>
			<div class="col-md-2" style="margin-left: 30px; color: #2392ec;">
				<strong>Preview & Publish</strong>
			</div>
		</div>
		<img src="image/prog/5 Progress bar.png" class="img-responsive" width="100%">
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-md-6">
				<h3 class="addEmpHead">Employee Details</a></h3>
			</div>
			<div class="col-md-6">
				<button class="btn-primary btn addempBtn pull-right" >Save & Publish</button>
				<button class="btn-primary btn transparentBtn pull-right" >Go back to edit</button>
			</div>
			<div class="col-md-12">
				<div class="col-md-12 det">
					<div class="col-md-3 empDet">
						<p>Name <b>Babin Kochana</b></p>
					</div>
					<div class="col-md-3 empDet">
						<p>Staff Id <b>ST54123</b></p>
					</div>
					<div class="col-md-3 empDet">
						<p>Email <b>Babin@gmail.com</b></p>
					</div>
					<div class="col-md-3 empDet">
						<p>Mobile <b>+91 9874563210</b></p>
					</div>
					<div class="col-md-3 empDet">
						<p>Job Title <b>Designer</b></p>
					</div>
					<div class="col-md-3 empDet">
						<p>Job type <b>Full Time</b></p>
					</div>
					<div class="col-md-3 empDet">
						<p>Role <b>Designer</b></p>
					</div>

				</div>
				<div class="col-md-12 detBottom">
					<span style="margin: 10px;">Groups</span>
					<chips>Stafftimes</chips>
					  	<chips>My overtime </chips>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="col-md-10 empworkCalMain bg_opacity" >
					<h4 class="termsofemphead">Working hours (offset)</h4>
					<div class="col-sm-12 pad0">
						<div class="col-sm-9 pad0">
						<p class="termsempsub">standard working hours</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsub alignrightval">40.00</p>
						</div>
					</div>
					<div class="col-sm-12 pad0 btm-hr" >
						<div class="col-sm-9 pad0">
						<p class="termsempsub">Employment ratio</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsub alignrightval">* 100%</p>
						</div>
					</div>
					<div class="col-sm-12 pad0">
						<div class="col-sm-9 pad0">
						<p class="termsempsubmain">Working hours per week</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsubmain alignrightval">40.00</p>
						</div>
					</div>
					<div class="col-sm-12 pad0 btm-hr">
						<div class="col-sm-9 pad0">
						<p class="termsempsub">Number of working days per week</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsub alignrightval">5 d</p>
						</div>
					</div>
					<div class="col-sm-12 pad0">
						<div class="col-sm-9 pad0">
						<p  class="termsempsubmain">Working hours per day</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsubmain alignrightval">5 d</p>
						</div>
					</div>
					<h4 class="termsofemphead">Leave of absence</h4>
					<div class="col-sm-12 pad0">
						<div class="col-sm-9 pad0">
						<p class="termsempsub">standard leave of absence</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsub alignrightval">20 d</p>
						</div>
					</div>
					<div class="col-sm-12 pad0 btm-hr" >
						<div class="col-sm-9 pad0">
						<p class="termsempsub">5 day work week</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsub alignrightval">100%</p>
						</div>
					</div>
					<div class="col-sm-12 pad0">
						<div class="col-sm-9 pad0">
						<p class="termsempsubmain">annual leave</p>
						</div>
						<div class="col-sm-3 pad0">
						<p class="termsempsubmain alignrightval">20 d</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-5" style="border:1px solid #aaa;">

				<div class="col-md-12" style="background-color: #fff;padding: 5px 0px;">
					<div class="col-md-6">
						<br>
						Employement Ratio <strong>100%</strong>
						<br><br>
						Weekly offset <strong>40.00</strong>
					</div>
					<div class="col-md-6">
						<br>
						Working Days <strong>5</strong>
						<br><br>
						Daily Offset <strong>8:00</strong>
					</div>

				</div>
		         <table class="table table-striped">
		              <thead >
		                   <th>DAY</th>
		                   <th>OFFSET (HRS)</th>
		                   <th>RATIO (%)</th>

		              </thead>
		              <tr>
		                   <td>Monday</td>
		                   <td>8:00</td>
		                   <td>100%</td>
		              </tr>
		              <tr>
		                   <td>Tuesday</td>
		                   <td>8:00</td>
		                   <td>100%</td>
		              </tr>
		              <tr>
		                   <td>wednesday</td>
		                   <td>8:00</td>
		                   <td>100%</td>
		              </tr>
		              <tr>
		                   <td>Thursday</td>
		                   <td>8:00</td>
		                   <td>100%</td>
		              </tr>
		              <tr>
		                   <td>Friday</td>
		                   <td>8:00</td>
		                   <td>100%</td>
		              </tr>
		              <tr>
		                   <td>Saturday</td>
		                   <td>0:00</td>
		                   <td>0%</td>
		              </tr>
		              <tr>
		                   <td>Sunday</td>
		                   <td>0:00</td>
		                   <td>0%</td>
		              </tr>
		         </table>
			</div>
		
		</div>
	
	<div class="col-sm-12">
		<h3 class="addEmpHead">Vacation Details</h3>
			<div class="col-sm-12 ">
				<div class="col-sm-12 vaction_sett">
					<h4>Terms of Employment <span class="date">01-01-2017</span> To <span class="date">31-02-2017</span></h4>
					<div class="col-sm-3 vac-sett">
						Full year equivalent <strong>364</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Entitlement pro rated <strong>20.00</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Carry over <strong>2.00</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Adjustment 
						<span style="position: relative;top: -10px;margin-left: 15px;">
							<div class="quantity buttons_added">
								<input type="number" step="1" min="1" max="" name="quantity" value="8.00" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
								<input type="button" value="+" class="plus">	
								<input type="button" value="-" class="minus">
							</div>
						</span>
					</div>
					<div class="col-sm-3 vac-sett">
						Total working days <strong>364</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Total leaves in hours <strong>160</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Taken this year <strong>0.00</strong>
					</div>
					<div class="col-sm-3 vac-sett">
						Remaining balance <strong style="color: #36c45e;">22.00</strong>
					</div>
				</div>
			</div>
		<h3 class="addEmpHead">Timesheet Lock setting</h3>
			<div class="col-sm-12 ">
				<div class="col-sm-12 vaction_sett" style="padding: 20px 0px;">
					<div class="col-md-4">
						<p>Timesheet locked until <strong>2 days ago</strong></p>
					
					</div>
					<div class="col-md-5">
						<p>Timesheet remainders every <strong>15th day of the month</strong></p>
					
					</div>
					<div class="col-md-3">
						<p style="color: #2392ec"><i>for this employee only</i></p>
					
					</div>
				</div>
			</div>
			<div style="text-align: center;margin: 50px 0px;">
				<button class="btn-primary btn transparentBtn " id="goBack">Go back to edit</button>
				<button class="btn-primary btn addempBtn " id="btnFive">Save & Publish</button>	
			</div>
			
	</div>

	
</div>
<br>

 <?php  include 'layout/footer.php';?>

<script type="text/javascript">
	$('document').ready(function(){
		window.scrollTo(0,0);
		$('#one').show();
		$('#two').hide();
		$('#three').hide();
		$('#four').hide();
		$('#five').hide();
	});

	$('#btnOne').click(function(){
		window.scrollTo(0,0);
		$('#one').hide();
		$('#two').show();
		$('#three').hide();
		$('#four').hide();
		$('#five').hide();
	});
	$('#btnTwo').click(function(){
		window.scrollTo(0,0);
		$('#one').hide();
		$('#two').hide();
		$('#three').show();
		$('#four').hide();
		$('#five').hide();
	});
	$('#btnThree').click(function(){
		window.scrollTo(0,0);
		$('#one').hide();
		$('#two').hide();
		$('#three').hide();
		$('#four').show();
		$('#five').hide();
	});
	$('#btnFour').click(function(){
		window.scrollTo(0,0);
		$('#one').hide();
		$('#two').hide();
		$('#three').hide();
		$('#four').hide();
		$('#five').show();
	});

	$('#goBack').click(function(){
		window.scrollTo(0,0);
		$('#one').show();
		$('#two').hide();
		$('#three').hide();
		$('#four').hide();
		$('#five').hide();
	});

$('#btnFive').click(function(){
	window.location.href = 'manage_users';
});
	

</script>

<script type="text/javascript">
	function wcqib_refresh_quantity_increments() {
    jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
        var c = jQuery(b);
        c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
    })
}
String.prototype.getDecimals || (String.prototype.getDecimals = function() {
    var a = this,
        b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
}), jQuery(document).ready(function() {
    wcqib_refresh_quantity_increments()
}), jQuery(document).on("updated_wc_div", function() {
    wcqib_refresh_quantity_increments()
}), jQuery(document).on("click", ".plus, .minus", function() {
    var a = jQuery(this).closest(".quantity").find(".qty"),
        b = parseFloat(a.val()),
        c = parseFloat(a.attr("max")),
        d = parseFloat(a.attr("min")),
        e = a.attr("step");
    b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
});
</script>