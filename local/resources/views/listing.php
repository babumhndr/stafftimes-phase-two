<?php  include 'layout/header.php';?>
<style>
	#ui-datepicker-div
    {
        z-index:1200 !important;
    }
</style>
 <link rel="stylesheet" href="<?php echo url('assets/css/listing.css')?>">
 <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
	<div class="row mot_template">
		<div class="col-md-12 link">
		<div style="float: left;">
			<p> 
			<span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span>  /
			<a><?php echo trans ('listing.employee_listing')?></a>
			</p>
		</div>
		<div style=" float: right; ">
		    <div class="right-inner-addon" style="float: left;">
				<i class="icon-search" style="color: #BDBDBD;"></i>
				<input type="search" id="search" class="form-control" autocomplete="off" placeholder="<?php echo trans ('listing.placeholder_search')?>..." style=" border-color: #e4e4e4; "/>
			</div>
			<div class="sort" style=" float: left; padding-left: 10px;margin-top: 1px; ">
				<label style="padding-right: 7px;font-weight: 600;"><?php echo trans('listing.sort_by')?>:</label>
					<div class="arrow_background">
						<select style=" border: none;outline: none;" id="sort_select_box">
							<option value="date"><?php echo trans('listing.sort_option_two')?></option>
							<option value="name"><?php echo trans('listing.sort_option_one')?></option>
						</select>
						</div>
			</div>
			<!-- <div class="float_button add_button_class" id="float_button">
				<button onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"  type="submit" class="circular_button"><i class="fa fa-pencil-square-o add_icon" aria-hidden="true" ></i>
				<p class="add_temp_text"><?php echo trans('manage_activity.add')?></p>
				<p class="add_temp_text"><?php echo trans('manage_activity.activity')?></p></button>
			</div> -->
		</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 content">
			<ul class="nav nav-tabs">
				<li class="top_list active">
					<a href="#home" class="top_anchor" data-toggle="tab">
						<span class="visible-xs"><i class="fa fa-check-circle-o" aria-hidden="true"></i></span>
						<span class="hidden-xs"style=" font-size: 14px; "><?php echo trans('listing.Active')?></span>
					</a>
				</li>
				<li class="top_list">
					<a href="#profile" class="top_anchor" data-toggle="tab" id="pending1">
						<span class="visible-xs"><i class="fa fa-minus-circle" aria-hidden="true"></i></span>
						<span class="hidden-xs"style=" font-size: 14px; "><?php echo trans('listing.Pending')?></span>
					</a>
				</li>
				<li class="top_list">
					<a href="#messages" class="top_anchor" data-toggle="tab" id="archive1">
						<span class="visible-xs"><i class="fa fa-archive" aria-hidden="true"></i></span>
						<span class="hidden-xs"style=" font-size: 14px; "><?php echo trans('listing.Archive')?></span>
					</a>
				</li>
			</ul>
			<div class="tab-content custom">
			<div class="tab-pane active" id="home">
				<div class="col-md-12" id="active" style="padding: 0px;">
                <div style="text-align:center;" id="active_loader">	
			        <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/>
			    </div>
				</div>
				</div>
				<div class="tab-pane" id="profile">
				<div class="col-md-12" id="pending" style="padding: 0px;">
                <div style="text-align:center;" id="pending_loader">	
			        <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/>
			    </div>
				</div>
				</div>
				<div class="tab-pane" id="messages">
				 <div class="col-md-12" id="archive" style="padding: 0px;">
                <div style="text-align:center;" id="archive_loader">	
			        <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/>
			    </div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>


	<div class="modal fade" id="modal-6">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_activity.modal_title')?></h5>
				</div>
			<div class="modal-content custom-content">
				
				<div class="modal-body custom-body">
					<div class="row">
						<div class="col-md-12">
							<div class="input-group box">
								<label for="field-1" class="control-label" ><!--<?php echo trans('manage_activity.activity_name')?>-->Start Date</label>
                                <input type="text" id="start_date" class="form-control ">
							</div>
						</div>
						<div class="col-md-12">
							<div class="input-group box">
								<label for="field-1" class="control-label" ><!--<?php echo trans('manage_activity.activity_name')?>-->End Date</label>
                                <input type="text" id="end_date" class="form-control">
							</div>
						</div>
					</div>
				<div class="modal-footer custom-footer">
				<div id="addbtn"> 
					<button class="btn btn-info" id="save_btn"><!--<?php echo trans('manage_activity.save')?>-->Send</button>
					</div>
					<div id="addloader" style="display:none">
						<img src="<?=url('image/hourglass.gif')?>" style="width:35px; height: 35px;"/>
					</div>
				</div>   
		</div>
	</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
<script type="text/javascript" language="javascript">

$( window ).load(function(){
sortByDate();
});
function minsToHrs(mins){
  var hours = Math.trunc(mins/60);
  var minutes = mins % 60;
  if (minutes <0 ) {
  	minutes = minutes*(-1); 
  }
  console.log(hours +":"+ minutes);
  return hours +"."+ minutes;
}

function sortByDate()
{
	var para=0;
	var para1=0;
	var para2=0;
	var res='';
	var res1='';
	var res2='';
		$.ajax({
	url:'employee_listing_active/0',
	type:'post',
	success:function(response){
	if(response !='')
	{
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res += '<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"><a href="reports_1/'+response[i]['_id']+'"><?php echo trans('listing.view_report')?></a></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div><a href="employee_profile/'+response[i]['_id']+'"> <img src="'+response[i]['profile_image']+'"></a><table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr>  <tr> <th><?php echo trans('listing.Balance_hours')?>:</th> <td>'+minsToHrs(response[i]['total_overtime'])+' <?php echo trans('dashboard.hours')?>  </td> </tr> </table> </div> </div>';
	}
	$('#active_loader').hide();
	$('#active').append(res);
	}
	else{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#active').html(message);
	}
	}
	});


	function scrollinfi(para)
	{
	$.ajax({
	url:'employee_listing_active/'+para,
	type:'post',
	success:function(response){
	var res='';
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res += '<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"><a href="reports_1/'+response[i]['_id']+'"><?php echo trans('listing.view_report')?></a></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div><a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"></a><table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div>';
	}
	$('#active').append(res);
	}
	});
	}



	$(window).scroll(function() {
	var scrollAmount = $(document).height() - $(window).height() - 1000;


	if ($(window).scrollTop() >= ($(document).height() - $(window).height() )) {

		 para = para + 16;
		
	   scrollinfi(para);
	}
	});


	$.ajax({
	url:'employee_listing_pending/0',
	type:'post',
	success:function(response){
		console.log(response);
	if(response !='')
	{
	if(res1 != '')
	{
	$('#pending').append();
	}
	else{
	for(var i=0;i<response.length;i++)
	{
	res1 += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Pending')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div><a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"> </a><table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table>  <a style="float: right; margin: 3px; color: #ff0000;" href="#" onclick="deleteEmployee((\''+response[i]['_id']+'\'))"><?php echo trans('translate.delete')?></a></div> </div>';
	} 
	$('#pending_loader').hide();
	$('#pending').append(res1);
	}
	}
	else
	{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#pending').html(message);	
	}
	}
	});


	function scrollinfi1(para1)
	{
	$.ajax({
	url:'employee_listing_pending/'+para1,
	type:'post',
	success:function(response){
	var res='';
	for(var i=0;i<response.length;i++)
	{
	res += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Pending')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></div><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5> <a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"> </a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table>  <a style="float: right; margin: 3px; color: #ff0000;" href="#" onclick="deleteEmployee((\''+response[i]['_id']+'\'))"><?php echo trans('translate.delete')?></a></div> </div>';
	}
	$('#pending').append(res);
	}
	});
	}



	$(window).scroll(function() {
	var scrollAmount = $(document).height() - $(window).height() - 1000;


	if ($(window).scrollTop() >= ($(document).height() - $(window).height() )) {

		 para1 = para1 + 16;
		
	   scrollinfi1(para1);
	}
	});


	$.ajax({
		url:'employee_listing_archive/0',
		type:'post',
		success:function(response){
	if(response !='')
	{
	 if(res2 != '')
	{
	('#archive').append();

	}
	else{
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res2 += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Archive')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.department')?>:</th> <td class="text_listing_dep">'+response[i]['department']+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></a></div><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5><a href="employee_profile/'+response[i]['_id']+'"> <img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table></div> </div>';

	} 
	$('#archive_loader').hide();
	$('#archive').append(res2);

	}
	}	
	else
	{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#archive').html(message);
	}	


	}

	});


	function scrollinfi2(para2)
	{
	$.ajax({
		url:'employee_listing_archive/'+para2,
		type:'post',
		success:function(response){
		var res='';

		for(var i=0;i<response.length;i++)
		{
			$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
		 res += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Archive')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.department')?>:</th> <td>'+response[i]['department']+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
		 '<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></a></div><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5><a href="employee_profile/'+response[i]['_id']+'"> <img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table></div> </div>';
	}
	$('#archive').append(res);
	}

	});
	}



	$(window).scroll(function() {
	var scrollAmount = $(document).height() - $(window).height() - 1000;


	if ($(window).scrollTop() >= ($(document).height() - $(window).height() )) {

		 para2 = para2 + 16;
		
	   scrollinfi2(para1);
	}
	});
}

function sortByName()
{
	var para=0;
	var para1=0;
	var para2=0;
	var res='';
	var res1='';
	var res2='';
	$.ajax({
	url:'employee_listing_active_sort/0',
	type:'post',
	success:function(response){
	if(response !='')
	{
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res += '<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"><a class="listing_link" href="reports_1/'+response[i]['_id']+'"><?php echo trans('listing.view_report')?></a></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div> <a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"> </a><table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div>';
	}
	$('#active_loader').hide();
	$('#active').append(res);
	}
	else{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#active').html(message);
	}
	}
	});


	function scrollinfi(para)
	{
	$.ajax({
	url:'employee_listing_active_sort/'+para,
	type:'post',
	success:function(response){
	var res='';
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res += '<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"><a class="listing_link" href="reports_1/'+response[i]['_id']+'"><?php echo trans('listing.view_report')?></a></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div> <a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table>  </div> </div>';
	}
	$('#active').append(res);
	}
	});
	}



	$(window).scroll(function() {
	var scrollAmount = $(document).height() - $(window).height() - 1000;


	if ($(window).scrollTop() >= ($(document).height() - $(window).height() )) {

		 para = para + 16;
		
	   scrollinfi(para);
	}
	});


	$.ajax({
	url:'employee_listing_pending_sort/0',
	type:'post',
	success:function(response){
		console.log(response);
	if(response !='')
	{
	if(res1 != '')
	{
	$('#pending').append();
	}
	else{
	for(var i=0;i<response.length;i++)
	{
	res1 += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Pending')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div><a href="employee_profile/'+response[i]['_id']+'"> <img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table>  <a style="float: right; margin: 3px; color: #ff0000;" href="#" onclick="deleteEmployee((\''+response[i]['_id']+'\'))"><?php echo trans('translate.delete')?></a></div> </div>';
	} 
	$('#pending_loader').hide();
	$('#pending').append(res1);
	}
	}
	else
	{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#pending').html(message);	
	}
	}
	});


	function scrollinfi1(para1)
	{
	$.ajax({
	url:'employee_listing_pending_sort/'+para1,
	type:'post',
	success:function(response){
	var res='';
	for(var i=0;i<response.length;i++)
	{
	res += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Pending')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></div><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5><a href="employee_profile/'+response[i]['_id']+'"> <img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table>  <a style="float: right; margin: 3px; color: #ff0000;" href="#" onclick="deleteEmployee((\''+response[i]['_id']+'\'))"><?php echo trans('translate.delete')?></a></div> </div>';
	}
	$('#pending').append(res);
	}
	});
	}



	$(window).scroll(function() {
	var scrollAmount = $(document).height() - $(window).height() - 1000;


	if ($(window).scrollTop() >= ($(document).height() - $(window).height() )) {

		 para1 = para1 + 16;
		
	   scrollinfi1(para1);
	}
	});


	$.ajax({
		url:'employee_listing_archive_sort/0',
		type:'post',
		success:function(response){
	if(response !='')
	{
	 if(res2 != '')
	{
	('#archive').append();

	}
	else{
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res2 += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Archive')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.department')?>:</th> <td>'+response[i]['department']+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></a></div><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5><a href="employee_profile/'+response[i]['_id']+'"> <img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table></div> </div>';

	} 
	$('#archive_loader').hide();
	$('#archive').append(res2);

	}
	}	
	else
	{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#archive').html(message);
	}	


	}

	});


	function scrollinfi2(para2)
	{
	$.ajax({
		url:'employee_listing_archive_sort/'+para2,
		type:'post',
		success:function(response){
		var res='';

		for(var i=0;i<response.length;i++)
		{
			$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
		 res += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Archive')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.department')?>:</th> <td class="text_listing_dep">'+response[i]['department']+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
		 '<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></a></div><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5><a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table></div> </div>';
	}
	$('#archive').append(res);
	}

	});
	}



	$(window).scroll(function() {
	var scrollAmount = $(document).height() - $(window).height() - 1000;


	if ($(window).scrollTop() >= ($(document).height() - $(window).height() )) {

		 para2 = para2 + 16;
		
	   scrollinfi2(para1);
	}
	});
}
function deleteEmployee(id)
{
/*	alert(id);*/
swal({   
	title: "<?php echo trans('popup.you_sure');?>",   
	text: "<?php echo trans('popup.retrieve_employee');?>",   
	type: "warning",   
	showCancelButton: true,   
	confirmButtonColor: "#DD6B55",   
	confirmButtonText: "<?php echo trans('popup.delete_it');?>",   
	cancelButtonText: "<?php echo trans('popup.cancel_it');?>",   
	closeOnConfirm: false,   
	closeOnCancel: false }, 
	function(isConfirm){   
		if (isConfirm) 
			{     
				$.ajax({
					method: 'POST',
					url: '<?= url('').'/deleteEmployee' ?>',
					data:{"id":id},
					success:function(response){ 
						console.log(response);
						if (response.status='success') 
							{
							swal("<?php echo trans('popup.deleted');?>!", "<?php echo trans('popup.employee_deleted');?>", "success"); 
							window.location.reload();  
							}  
						else    
							{
							swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.error');?>", "error"); 
							window.location.reload(); 
							}    	
					}
				});  
			} 
		else 
			{     
				swal("<?php echo trans('popup.cancelled');?>", " <?php echo trans('popup.not_deleted');?>", "error");   
			} 
	});
}

$('#sort_select_box').change(function()
{
	$('#active').html('<div style="text-align:center;" id="active_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	$('#pending').html('<div style="text-align:center;" id="pending_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	$('#archive').html('<div style="text-align:center;" id="archive_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	var type = $('#sort_select_box').val();
	if (type=="name") 
	{
		sortByName();
	}
	else
	{
		sortByDate();
	}
});

function searchByName(keyword)
{
	var para=0;
	var para1=0;
	var para2=0;
	var res='';
	var res1='';
	var res2='';
	$.ajax({
	url:'employee_listing_active_search/'+keyword,
	type:'post',
	success:function(response){
		$('#active').html('<div style="text-align:center;" id="active_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	if(response !='')
	{
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res += '<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"><a class="listing_link" href="reports_1/'+response[i]['_id']+'"><?php echo trans('listing.view_report')?></a></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div><a href="employee_profile/'+response[i]['_id']+'"> <img src="'+response[i]['profile_image']+'"></a> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div>';
	}
	$('#active_loader').hide();
	$('#active').append(res);
	}
	else{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#active').html(message);
	}
	}
	});


	$.ajax({
	url:'employee_listing_pending_search/'+keyword,
	type:'post',
	success:function(response){
		console.log(response);
	$('#pending').html('<div style="text-align:center;" id="pending_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	if(response !='')
	{
	if(res1 != '')
	{
	$('#pending').append();
	}
	else{
	for(var i=0;i<response.length;i++)
	{
	res1 += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Pending')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></div><div style="width:53%; white-space: nowrap;"><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5></div><a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"> </a><table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table>  <a style="float: right; margin: 3px; color: #ff0000;" href="#" onclick="deleteEmployee((\''+response[i]['_id']+'\'))"><?php echo trans('translate.delete')?></a></div> </div>';
	} 
	$('#pending_loader').hide();
	$('#pending').append(res1);
	}
	}
	else
	{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#pending').html(message);	
	}
	}
	});

	$.ajax({
		url:'employee_listing_archive_search/'+keyword,
		type:'post',
		success:function(response){		
	$('#archive').html('<div style="text-align:center;" id="archive_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	if(response !='')
	{
	 if(res2 != '')
	{
	('#archive').append();

	}
	else{
	for(var i=0;i<response.length;i++)
	{
		$department=response[i]['department'];
		if ($department==""||$department==undefined) 
		{
			$department="--";
		}
	res2 += /*'<a href="employee_profile/'+response[i]['_id']+'"><div class="col-md-3 list"> <div class="col-md-12 listing_div"> <p style=" float: right; margin: 3px; color: #33B251; "><?php echo trans('listing.Archive')?></p> <h5>'+response[i]['name']+'</h5> <img src="'+response[i]['profile_image']+'"> <table> <tr> <th><?php echo trans('listing.department')?>:</th> <td>'+response[i]['department']+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table> </div> </div></a>';*/
	'<div class="col-md-3 list"> <div class="col-md-12 listing_div"><div class="listing_link_div"></a></div><h5 style="overflow:hidden; text-overflow:ellipsis;">'+response[i]['name']+'</h5><a href="employee_profile/'+response[i]['_id']+'"><img src="'+response[i]['profile_image']+'"> </a><table> <tr> <th><?php echo trans('listing.deparment')?>:</th> <td class="text_listing_dep">'+$department+'</td> </tr> <tr> <th><?php echo trans('listing.working_hours')?>:</th> <td>--</td> </tr> <tr> <th><?php echo trans('listing.Overtime_hours')?>:</th> <td>--</td> </tr> </table></div> </div>';

	} 
	$('#archive_loader').hide();
	$('#archive').append(res2);

	}
	}	
	else
	{
	var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>'
	$('#archive').html(message);
	}	
	}
	});

}
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

$('#search').on("keyup", function() 
{
	delay(function(){
		var message='<div class="col-md-12 error_msg"><p><?php echo trans('listing.data_not_available')?></p></div>';
	$('#active').html('<div style="text-align:center;" id="active_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	$('#pending').html('<div style="text-align:center;" id="pending_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	$('#archive').html('<div style="text-align:center;" id="archive_loader"> <img id="loader" class="loader" src="image/hourglass_s.gif" style="width:35px; height: 35px;"/> </div>');
	 var keyword=$('#search').val();
	 if (keyword.length>=1) 
	 {
	 	searchByName(keyword);
	 }
	 else if (keyword.length<=0) 
	 {
	 	var typeOfSort = $('#sort_select_box').val();
			if (typeOfSort=="name") 
			{
				sortByName();
			}
			else
			{
				sortByDate();
			}
	 }
	 else
	 {
		$('#archive').html(message);
		$('#pending').html(message);
		$('#active').html(message);
		$('#archive_loader').hide();
	 	$('#pending_loader').hide();
	 	$('#active_loader').hide();
	 }
    }, 500 );
	
});
</script>
<script>
	var dates = $("#start_date, #end_date").datepicker({
	    defaultDate: "+1w",
	    firstDay: 1,
	    changeMonth: true,
	    numberOfMonths: 1,
	    dateFormat:"dd-mm-yy" 
	});
	// $("#start_date, #end_date").change(function(){
	// 	var start_date = $('#start_date').val();
	// 	var end_date = $('#end_date').val();
	// 	console.log({start_date:start_date, end_date:end_date});

 //  });
	$('#save_btn').click(function(){
		var start_date = $('#start_date').val();
		var end_date = $('#end_date').val();
		//console.log({start_date:start_date, end_date:end_date});
		if(start_date!=''&&end_date!='') 
		{
			var url="<?php echo url('selectSyncDates')?>";
            url=url+"/"+start_date+"/"+end_date;
            console.log(url);
            $.ajax({
                   	url:url,
                    type:'post',
                    success: function(response)
                    {  
                     console.log(response);
                 	}
                });
		}		

	});
	
// 	$(document).ready(function(){
// 		$('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
// 		$('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));

// 		getDates(start_date,end_date);
// 	});
// function getDates(start_date,end_date) {
// 	var dates = $("#start_date, #end_date").datepicker({
//     defaultDate: "+1w",
//     firstDay: 1,
//     changeMonth: true,
//     numberOfMonths: 1,
//     dateFormat:"dd-mm-yy",
//      onSelect: function(selectedDate) {
//         var option = this.id == "start_date" ? "minDate" : "maxDate",
//         instance = $(this).data("datepicker"),
//             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
//         dates.not(this).datepicker("option", option, date);
//         var start_date=$('#start_date').val();
//         var end_date=$('#end_date').val();
//         localStorage.start_date=start_date;
//         localStorage.end_date=end_date;
//        	//console.log(start_date+'//'+end_date+'//');
//        	if(start_date!=''&&end_date!='')
//         {
//             $('#fakeLoader').show();
//              var url="<?php echo url('selectSyncDates')?>";
//              url=url+"/"+start_date+"/"+end_date;
//              console.log(url);
//              $.ajax({
//                     url:url,
//                     type:'post',
//                     success: function(response)
//                     {  
//                      console.log(response);
//                  	}
//                 });
//         }
//       }
// 	});
// }

	
	//console.log(start_date);
	//console.log(end_date);
</script>
<?php include 'layout/footer.php';?>