<?php  include 'layout/header.php';?>
<style type="text/css">
    #ui-datepicker-div
    {
        z-index:55 !important;
    }
   #fakeLoader{
    display: none;
    top: inherit !important;
    left: inherit !important;
    background-color: rgb(238, 238, 238)!important;
    }
    .spinner2
    {
        left: 40% !important;
        top: 40% !important;
        color: #000;
    } 
    .container1 > div, .container2 > div, .container3 > div {
    background-color: #000 !important;
    }
</style>
<?php 
$employeeName='';
if (!empty($data['employee'][0]['name']))
    {
        $employeeName=$data['employee'][0]['name'];
    }
?>
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<link rel="stylesheet" href="<?php echo url('assets/css/activity_graph_report.css')?>">
<link href="<?php echo url('css/fakeLoader.css')?>" rel="stylesheet">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="<?php echo url('js/exporting.js')?>"></script><!-- 
<script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script> -->
            <!-- <h3>Here starts everything&hellip;</h3> -->
           
            <div class="row reports">
                <div class="col-md-12 link">
                    <p> 
                    <a href="#"><?php echo trans ('header.dashboard')?></a>&nbsp
                    <span class="template_link">>
                    <a href="#">&nbsp<?php echo trans ('report_links.title')?></a></span> 
                    </p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="fill">
                        <?php include 'layout/report_links.php' ?>  
                    <div class="col-md-12 col-sm-12 col-xs-12 second">
                     <div class="fakeloader" id="fakeLoader" ></div>
                        <div class="col-md-4 hidden-sm hidden-xs"></div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="fill" style=" text-align: center; ">
                        <form >                       
                        <div class="col-md-12 col-sm-12 col-xs-12 half">
                            <div class="fill">
                                <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.from')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="start_date" class="form-control ">         
                            </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.to')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="end_date" class="form-control">         
                            </div>
                            </div>
                            </div>
                        </div>
                        </form>
                        </div>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs"></div>
                    </div>
                    <button type="submit" onclick="PrintElem('#bar-5')" style="margin-bottom:15px;">
                        <i class="fa fa-print icon" aria-hidden="true"></i>
                    </button>
                    <div class="col-md-12 col-sm-12 col-xs-12 fourth" style="background: #fff; margin-bottom: 15px;">
                    <!-- <button onclick="createPdf()">bbbb</button> -->
                   <div class="panel-body"> 
                           <!--  <script type="text/javascript">
                                jQuery(document).ready(function($)
                                {
                                    var dataSource = [
                                        { work: "At work", blue: 30 },
                                        { work: "Training", red: 18 },
                                        { work: "Break", green: 36 },
                                        { work: "Balance", yellow: 50 },
                                        { work: "Working", orange: 41 },
                                        { work: "Start", lightblue: 64 }
                                    ];
                                    
                                    $("#bar-5").dxChart({
                                        rotated: true,
                                        pointSelectionMode: "multiple",
                                        dataSource: dataSource,
                                        commonSeriesSettings: {
                                            argumentField: "work",
                                            type: "stackedbar",
                                            selectionStyle: {
                                                hatching: {
                                                    direction: "left"
                                                }
                                            }
                                        },
                                        series: [
                                            { valueField: "blue", name: "emp", color: "#2392ec" },
                                            { valueField: "green", name: "emp", color: "#00b33c" },
                                            { valueField: "yellow", name: "emp", color: "#ffcc00" },
                                            { valueField: "orange", name: "emp", color: "#ff6600" },
                                            { valueField: "lightblue", name: "emp", color: "#669999" },
                                            { valueField: "red", name: "emp", color: " #ff471a" }
                                        ],
                                        legend: {
                                            visible : false,
                                        },
                                        pointClick: function(point) {
                                            point.isSelected() ? point.clearSelection() : point.select();
                                        }
                                    });
                                });
                            </script> -->
                            <div id="bar-5" style="height: 450px; width: 100%;"></div>
                        </div>
                        
                </div>
                    </div>
                </div>
            </div>
    
    </div>
    
    <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script> 
    <script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'Activity Graph Report', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Activity Graph Report</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }

</script>
    <script>
        $(".left-links li a").click(function(){
        $(this).find('i').toggleClass('fa-indent fa-outdent');
            });
        var dateToday = '';
var employee_id="<?=$data['employee_details']['employee_id']?>";
var company_id="<?=$data['employee_details']['company_id']?>";
/*console.log(employee_id);
console.log(company_id);*/
var dates = $("#start_date, #end_date").datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    dateFormat:"dd-mm-yy",
    onSelect: function(selectedDate) {
        var option = this.id == "start_date" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
        var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
         localStorage.start_date=start_date;
        localStorage.end_date=end_date;
       /* console.log(start_date+'//'+end_date+'//');*/
        if(start_date!=''&&end_date!='')
        {
             $('#fakeLoader').show();
            /*console.log(start_date+'//'+end_date+'//');*/
            $.ajax({
                    url:"<?php echo url('listingTimesheetForActivityGraph/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    success: function(response)
                    {  
                     console.log(response);
                     var graph=[];
                     for (var i = 0; i < response.length; i++) {
                        var work_value=response[i]['1'];
                        graph.push({"range":work_value,"blue":response[i]['0']});
                     }
                     dataGraph(graph);
                    }
                   });
        }
        }
    });

//on load 
$( window ).load(function(){
var today = new Date();
var dayOfWeekStartingSundayZeroIndexBased = today.getDay(); // 0 : Sunday ,1 : Monday,2,3,4,5,6 : Saturday
var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+1);
var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+7);
var start_date1 =moment(mondayOfWeek).format('D-M-YYYY');
var end_date1 =moment(sundayOfWeek).format('D-M-YYYY');
if (typeof(localStorage.start_date)=='undefined') { localStorage.start_date=start_date1}
if (typeof(localStorage.end_date)=='undefined') { localStorage.end_date=end_date1}
$('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
$('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));
start_date1 =localStorage.getItem("start_date");
end_date1 =localStorage.getItem("end_date");
if(start_date1!=''&&end_date1!='')
        {
            var employee_id="<?=$data['employee_details']['employee_id']?>";
            var company_id="<?=$data['employee_details']['company_id']?>";
            $.ajax({
                    url:"<?php echo url('listingTimesheetForActivityGraph/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                     success: function(response)
                    {  
                     console.log(response);
                     var graph=[];
                     for (var i = 0; i < response.length; i++) {
                        var work_value=response[i]['1'];
                        graph.push({"range":work_value,"blue":response[i]['0']});
                     }
                     dataGraph(graph);
                    }
                });

        }
});

function dataGraph(dataSource){
    var start_date=$('#start_date').val();
    var end_date=$('#end_date').val();
    var message="<?php echo trans ('reports.date_range')?> : "+start_date+" <?php echo trans ('report_links.to')?> "+end_date;
    var companyOf="<?php echo trans('employer.companyname')?> : "+companyName;
    var employeeName="<?= $employeeName?>";
    var textFinal=employeeName+'<br>'+companyOf+'<br>'+message;
    var titleOf=toTitleCase("<?php echo trans ('report_links.five_line_one')?> <?php echo trans ('report_links.five_line_two')?>");
    console.log('sd');
    var categories=[];
    var data=[];
    for (var i = 0; i < dataSource.length; i++) {
    categories.push(dataSource[i]['range']);
    data.push(dataSource[i]['blue']);
    }
    $(function () {
    Highcharts.chart('bar-5', {
        chart: {
            type: 'bar'
        },
        title: {
            align:"left",
            text:textFinal,
            style:{color:"#333333",fontSize:"12px"},
        },
        xAxis: {
            categories:categories
        },
        yAxis: {
            title: {
                text: 'Total Hours'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        exporting: {
        filename: titleOf,
        },
        credits: {
        enabled: false
        },
        series: [{
            name: 'Range',
            data: data
        }]
    });
    });
    $('#fakeLoader').hide();
}

/*function createPdf()
{
console.log('hiii');

html2canvas($("#bar-5"), {
    onrendered: function(canvas) 
    {
        console.log('in');
        var imgData = canvas.toDataURL('image/png');     
        var doc = new jsPDF('l', 'mm', 'a4');
        //doc.lines([[2,2],[-2,2],[1,1,2,2,3,3],[2,1]], 212,110, 10);
        doc.setFontStyle('bold');
        doc.addImage(imgData, 'PNG', 10, 10);
        doc.save('formoutput.pdf');
    }
});


}*/
    //change caps to title case
    function toTitleCase(str)
    {
     return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
    </script>
    <script src="<?php echo url('js/fakeLoader.min.js')?>"></script>
    <script type="text/javascript">
    $(".fakeloader").fakeLoader({
    timeToHide:15000, //Time in milliseconds for fakeLoader disappear
    zIndex:"999",//Default zIndex
    spinner:"spinner2",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
    bgColor:"#2392ec", //Hex, RGB or RGBA colors
    });
    </script>
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="<?php echo url('assets/js/datatables/dataTables.bootstrap.css')?>">
    <!-- Bottom Scripts -->
    <script src="<?php echo url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>


    <!-- Imported scripts on this page -->
    <script src="<?php echo url('assets/js/datatables/dataTables.bootstrap.js')?>"></script>
    <script src="<?php echo url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js')?>"></script>
    <script src="<?php echo url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>
    <script src="<?php echo url('assets/js/timepicker/bootstrap-timepicker.min.js')?>"></script>

    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
    
 <?php  include 'layout/footer.php';?>


