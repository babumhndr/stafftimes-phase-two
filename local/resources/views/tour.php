<?php 
$metaTitle='';
$metaUrl='';
$metaDescription='';
$metaKeyword='';
?>
<?php  include 'layout/main_page_header.php';?>
    <link rel="stylesheet" type="text/css" href="<?php echo url('css/tour.css')?>">
    <link rel="stylesheet" href="<?php echo url('js/bx/jquery.bxslider.css')?>">
    <style type="text/css">
    .horizontal_line{
        background: #f3f4f5;
    width: 100%;
    height: 1px;
    float: left;
    margin-top: -5px;
              }
    .spacemaker_img{
     text-align: center;
     padding: 50px;
     margin-top: 111px;
    }
    .spacemaker_img1{
        text-align: center;
     padding: 22px;
     margin-top: 25px;
    }
    .spacemaker_para{
    text-align: center;
    padding: 50px;
    margin-top: 50px;
    }
    @media screen and (max-width: 400px) {
        .spacemaker_img{
            padding: 0px !important;
        }
        .spacemaker_img1{
           padding: 0px !important;
        }   
    .spacemaker_para{
       padding: 0px !important;
     }
    .h4{
       padding-top: 20px !important; 
    }
}
   @media screen and (max-width: 770px) {
   .main_view{
    margin-top: 100px;
   }
    }
    @media screen and (max-width: 600px) {
        .spacemaker_img{
            margin-top: 15px !important;
        }
        .spacemaker_img1{
           margin-top: 0px !important;
        }   
    .spacemaker_para{
       margin-top: 0px !important;
     }
    }
    .concept_div
    {
      padding: 36px;
    padding-top: 0px;
    padding-bottom: 0px;
    text-align: justify;
    padding-right: 12px;
    }
    .bx-controls-direction
    {
      display: none !important;
    }
    .bx-wrapper img {
    margin-left: auto;
    margin-right: auto;
}
</style>
<?php 
$langFolder=trans('translate.lang');
?>
   <div class="row mot_header">
 <?php  include 'layout/main_page_header_nav.php';?>
 </div>
<div class="col-md-12" style="padding: 0px;">
<div class="container main_view">
   <div class="col-md-12" style="text-align: center;margin-top: 25px;margin-bottom: 25px;">
       <h2 style="margin-bottom: 3px;"><?php echo trans('tour.main_head1')?></h2>
      <!--  <h2><?php echo trans('tour.main_head2')?></h2> -->
   </div> 
   <div class="col-md-12" style="text-align: center;margin-top: 25px;margin-bottom: 25px;">
   <div class="col-md-4"></div>
    <div class="col-md-4">
       <img src="<?php echo url('image/Shine-copy-3.png')?>" alt="myovertime" style="width: 100%;">
    </div>
     <div class="col-md-4"></div>
   </div>
</div> 
<div class="col-md-12" style="background: #fafafa">
  <div class="col-md-6 spacemaker_para" style="margin-top: 20px;">
    <div class="h4">
      <h4><?php echo trans('tour.the_concept')?></h4>
    </div>
    <div><p style="margin-top: 50px;line-height: 1.9 !important;"><?php echo trans('tour.the_concept_body')?></p>
    <div class="concept_div">
    <ul style="list-style-type: amharic;">
      <li><p style="line-height: 1.9 !important;"><?php echo trans('tour.the_concept_body1')?></p></li>
      <li><p style="line-height: 1.9 !important;"><?php echo trans('tour.the_concept_body2')?></p></li>
    </ul>
    </div>
    </div>
  </div>
  <div class="col-md-6 spacemaker_img">
  <ul class="bxslider">
    <li><img src="<?php echo url('image/tour/'.$langFolder.'/concept1.jpg')?>" alt="myovertime"style="width: 100%;"></li>
    <li><img src="<?php echo url('image/tour/'.$langFolder.'/concept2.jpg')?>" alt="myovertime"style="width: 100%;"></li>
  </ul>
  </div>
</div>
<div class="col-md-12">
<div class="col-md-6 spacemaker_img">
<ul class="bxslider">
    <li><img src="<?php echo url('image/tour/'.$langFolder.'/admin1.jpg')?>" alt="myovertime"style="width: 100%;"></li>
    <li><img src="<?php echo url('image/tour/'.$langFolder.'/admin2.jpg')?>" alt="myovertime"style="width: 100%;"></li>
    <li><img src="<?php echo url('image/tour/'.$langFolder.'/admin3.jpg')?>" alt="myovertime"style="width: 100%;"></li>
  </ul>
</div>
<div class="col-md-6 spacemaker_para">
<div class="h4"><h4><?php echo trans('tour.desktop_administrator')?></h4></div>
<div><p style="margin-top: 50px;line-height: 1.9 !important;"><?php echo trans('tour.desktop_administrator_body')?></p></div>
</div>
</div>
<div class="col-md-12"style="background: #fafafa;padding: 0px;">
<div class="col-md-6 spacemaker_para">
<div class="h4"><h4><?php echo trans('tour.staff_members')?></h4></div>
<div><p style="margin-top: 50px;line-height: 1.9 !important;"><?php echo trans('tour.staff_members_body')?></p></div>
</div>
<div class="col-md-6 spacemaker_img1">
<ul class="bxslider">
    <li style="background: #fafafa;"><img src="<?php echo url('image/tour/'.$langFolder.'/1.png')?>" alt="myovertime"></li>
    <li style="background: #fafafa;"><img src="<?php echo url('image/tour/'.$langFolder.'/2.png')?>" alt="myovertime"></li>
    <li style="background: #fafafa;"><img src="<?php echo url('image/tour/'.$langFolder.'/3.png')?>" alt="myovertime"></li>
    <li style="background: #fafafa;"><img src="<?php echo url('image/tour/'.$langFolder.'/4.png')?>" alt="myovertime"></li>
    <li style="background: #fafafa;"><img src="<?php echo url('image/tour/'.$langFolder.'/5.png')?>" alt="myovertime"></li>
    <li style="background: #fafafa;"><img src="<?php echo url('image/tour/'.$langFolder.'/6.png')?>" alt="myovertime"></li>
  </ul>
</div>
</div>
<div class="col-md-12">
<div class="col-md-6 spacemaker_img">

<ul class="bxslider">
    <li><img src="<?php echo url('image/tour/'.$langFolder.'/report1.jpg')?>" alt="myovertime"style="width: 100%;"></li>
    <li><img src="<?php echo url('image/tour/'.$langFolder.'/report2.jpg')?>" alt="myovertime"style="width: 100%;"></li>
  </ul>
</div>
<div class="col-md-6 spacemaker_para">
<div class="h4"><h4><?php echo trans('tour.reports')?></h4></div>
<div><p style="margin-top: 50px;line-height: 1.9 !important;"><?php echo trans('tour.reports_body')?></p></div>
</div>
</div>
</div>
<script src="<?php echo url('js/bx/jquery.bxslider.min.js')?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.bxslider').bxSlider({
    auto:true,
    speed:2000,
});
});
</script>
<?php  include 'layout/main_page_footer.php';?>
