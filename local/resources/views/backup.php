<?php  include 'layout/header.php';?>
<style>
  body {
        font-family: 'Open Sans';
    }
    .logo-env {
        text-align: center;
        background-color: #fff;
    }
    /*.mot_dashboard_profile_time {
      float: left;
        padding-top: 25px;
        width: 60%;
    
    }
    .sidebar-menu {
        width: 275px;
    }
    .mot_dashboard_profile_inner {
    width: 402px;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    }
    .user-info-navbar {
      background-color: #2392ec;
      
    }
    .user-info-navbar .user-info-menu > li {
      border: 0px;
    }
    .user-info-navbar .user-info-menu > li > a {
      color: #fff !important;
      border-bottom: 0px;
    }
    .user-info-navbar .user-info-menu > li > a i {
      font-size: 22px;
    }
    .mot_header_lists {
      border: 0px;
    }
    .profile_time_list {
      list-style: none;
      border: 1px solid #4e91be;
      background-color: #4e91be;
      width: 100%;
      margin-right: auto;
      margin-left: auto; 
      padding-left: 0px;
      border-radius: 20px;
      padding-top: 8px;
      padding-bottom: 8px;
      font-size: 12px;
    }
    .current_stats {
      display: inline; 
      border-right: 1px solid #fff;
      padding-right: 10px;
      padding-left: 10px;
        color: #fff;
    }
    .current_stats_font{
        font-family: 'Open Sans' !important;
    }
    .left-links {
        width: 10%;
    }
    .right-links {
    }

    .last {
      border-right: 0;
    }
    .notif_mobile {
        background-color: #f8f8f8 !important;
        color: #53A2CA;
    }
    .notif {
      background-color: #f8f8f8 !important; 
      top: 24px !important;
      right: 15px !important;
      color: #53A2CA;
      min-width: 16px;
    }
    .profile_options {
      list-style: none;
    }
    .profile_options_listing {
      display: inline; 
    }
    .mot_dashboard_profile_item {
      padding-top: 20px;
      text-align: right;
      font-size: 16px;
      float: right;
    }
    .user_name {
      color: #fff;
      font-size: 14px !important;
    }
    .user_name i {
      font-size: 14px !important;
    }
    .user-info-navbar {
        margin-bottom: 15px;
        margin: 0px;

    }
    .page-container .main-content {
        padding: 0px;
    }*/
    .link {
        padding-top: 10px;
            padding-bottom: 10px;
    }
    .template_link {
        color: #2392ec;
    }
    .template_link a {
        color: #2392ec;
    }
    .content {
        margin-top: 0px;    
        float: left;
    }
    .mot_template {
        margin-left: 0px;
        margin-right: 0px;
    }
    .custom {
        padding: 55px 45px 45px 45px !important;
        margin-bottom: 10px !important;
        float: left;
        width: 100%;
        background-color: #ffffff; 
    }
    .right_border {
  border-right: 1px solid #e5e5e5;
}
  .no_padding {
    padding-right: 0px;
    padding-left: 0px;
  }
  .t_align {
    text-align: center;
  }
  .add_text {
    font-size: 18px;
    margin-bottom: 0px;
  }
  .input_radius {
    border-radius: 2px !important;
  }
  .custom_div {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
  }
  .custom_div1 {
    float: left;
    width: 100%;
  }
  #cancel_btn {
    color: #fff;
    background-color: #7D7D7D;
    border: 1px solid #7D7D7D;
  }
  #cancel_btn:hover, #cancel_btn:active, #cancel_btn:focus  {
    color: #fff;
    background-color: #7D7D7D;
    border: 1px solid #7D7D7D; 
  }
  .buttons {
    margin-bottom: 15px;
    float: left;
  }
  .button_cancel {
    margin-bottom: 0px;
    float: left;
    margin-top: 25px;
  }
  .btn_class {
  margin-bottom: 0px !important;
  padding: 8px 25px 8px 25px;
  font-size: 14px;
  width: 180px;
  border-radius: 3px;
  }
  .btn_class:hover,.btn_class:active, .btn_class:focus {
    background-color: #2392ec;
    border:1px solid #2392ec;
    outline: none;
  }
  .btn:hover {
  text-decoration: none;
  color: #fff;
}
.btn-info {
  background-color: #2392ec;
  margin-bottom: 0px;

  }
  .space {
    padding-bottom: 40px;
  }
  .arrow_background3 {
    float: right;
    background: url("image/arrow.png") no-repeat right #fff;
    background-size: 10% 37%;
    background-position-x: 98%;
    background-position-y: 91%;
    border-radius: 4px;
  }
  .arrow_background3 input {
    color: #333;
    padding-right: 16px;
    padding-top: 2px;
    padding-bottom: 2px;
    padding-left: 13px;
    border-radius: 2px; 
  }
</style>
<div class="row mot_template">
  <div class="col-md-12 link">
    <p> 
    <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / 
    <a><?php echo trans ('header.Backup')?></a>
    </p>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 content">
    <div class="tab-content custom">
      <!-- <div style="text-align: center;height:100vh;">
      <h1><?php echo trans('translate.comin_soon')?></h1>
      </div> -->
    <div class="col-md-12 col-sm-12">
      <div class="col-md-6 col-sm-12 no_padding right_border">
        <div class="custom_div">
        <!-- <div class="col-md-12 col-sm-12 no_padding t_align space">
          <p class="add_text"><?php echo trans ('backup.select_backup_date')?></p>
        </div> -->
      <!--  <div class="custom_div1 space">
        <div class="col-md-4 col-sm-4 no_padding" style="margin-left: 25px;">
                            <div class="input-group">
                            <div class="arrow_background3">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                      </div>
                    </div>
                    </div>
                    <label class="col-md-2 col-sm-2 control-label t_align no_padding" style="    padding-top: 5px;"><?php echo trans ('backup.to')?></label>

                    <div class="col-md-4 col-sm-4 no_padding">
                            <div class="input-group">
                      <div class="arrow_background3">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                      </div>
                    </div>
                    </div>
                    </div> -->
                    <div class="col-md-12 t_align buttons">
                  <a href="<?php echo url('backupListing'); ?>"><button class="btn btn-info btn_class"><?php echo trans ('backup.Backup_button')?></button></a>              
              </div>
                          
        </div>
      </div>
      <div class="col-md-6 col-sm-12 no_padding">
        <div class="custom_div">
        <!-- <div class="col-md-12 col-sm-12 no_padding t_align space">
          <p class="add_text"><?php echo trans ('backup.select_restore_date')?></p>
        </div> -->
  <!--      <div class="custom_div1 space">
        <div class="col-md-4 col-sm-4 no_padding" style="margin-left: 25px;">
                            <div class="input-group">
                      <div class="arrow_background3">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                      </div>
                    </div>
                    </div>
                    <label class="col-md-2 col-sm-2 control-label t_align no_padding" style="    padding-top: 5px;"><?php echo trans ('backup.to')?></label>

                    <div class="col-md-4 col-sm-4 no_padding">
                            <div class="input-group">
                      <div class="arrow_background3">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                      </div>
                    </div>
                    </div>
                    </div> -->
                    <div class="col-md-12 t_align buttons">
                  <a href="<?php echo url('restoreView'); ?>"><button class="btn btn-info btn_class"><?php echo trans ('backup.Restore_button')?></button> </a>               
              </div>
                          
        </div>        
      </div>
      
    </div>
<!--    <div class="col-md-12 col-sm-12 t_align button_cancel">
            <button class="btn btn-white btn_class" id="cancel_btn"><?php echo trans ('backup.cancel_button')?></button>                
        </div> -->
    </div>
  </div>
</div>
<link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">
        <script src="assets/js/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="assets/js/daterangepicker/daterangepicker.js"></script>
  <script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
<?php include 'layout/footer.php';?>