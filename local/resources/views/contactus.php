<?php 
$metaTitle=$data['title'];
$metaUrl=$data['url'];
$metaDescription=$data['description'];
$metaKeyword=$data['keyword'];
?>
<?php  include 'layout/main_page_header.php';?>
<link rel="stylesheet" type="text/css" href="<?php echo url('css/contactus.css')?>">
<div class="row mot_header">
 <?php  include 'layout/main_page_header_nav.php';?>
 </div>
<div class="col-md-12" style="padding: 0px;">
<div class="container main_view">
   <div class="col-md-12" style="text-align: center;margin-top: 25px;margin-bottom: 25px;">
       <h2 style="margin-bottom: 15px;"><?php echo trans('contact.main_heading')?></h2>
      	<p><?php echo trans('contact.sub_heading')?></p>
   </div> 
   <div class="col-md-1 hidden-md"></div>
   <div class="col-md-10" style="text-align: center;margin-top: 25px;margin-bottom: 25px;">
   <form method="POST" action="<?php echo route('sendContactUs')?>" id="form" >
       	<div class="col-md-6 inner-addon right-addon" style="margin-bottom:20px;">
    	<select class="form-control purpose purpose-select" style="height: 42px !important;" name="purpose">
        <option><?php echo trans('contact.purpose')?></option>  
         <option><?php echo trans('contact.purpose_one')?></option>  
         <option><?php echo trans('contact.purpose_two')?></option>  
         <option><?php echo trans('contact.purpose_three')?></option> 
        </select>
		</div>
       	<div class="col-md-6 col-sm-12 col-xs-12 inner-addon right-addon" style="margin-bottom:20px;">
        <div class="col-md-12 col-sm-12 col-xs-12" style=" padding: 0px;    border: 1px solid #ccc; border-radius: 2px;">
   			<div class="col-md-4 col-sm-3 col-xs-4" style="padding: 0px">
        <select class="form-control purpose box-shadow code-select-box" id="phone-code">
            <?php
            if (trans('translate.lang')=="en") 
            {
            echo "
            <option data-countryCode='US' value='1'>".trans('country_translation.US')."(+1)"."</option>
            <option data-countryCode='GB' value='44'>".trans('country_translation.GB')."(+44)"."</option>
            <option data-countryCode='AU' value='61'>".trans('country_translation.AU')."(+61)"."</option>
            <optgroup label='----------------------------------'>
            <option data-countryCode='AF' value='93'>".trans('country_translation.AF')."(+93)"."</option>
            <option data-countryCode='AL' value='355'>".trans('country_translation.AL')."(+355)"."</option>
            <option data-countryCode='DZ' value='213'>".trans('country_translation.DZ')."(+213)"."</option>
            <option data-countryCode='AS' value='1-684'>".trans('country_translation.AS')."(+1-684)"."</option>
            <option data-countryCode='AD' value='376'>".trans('country_translation.AD')."(+376)"."</option>
            <option data-countryCode='AO' value='244'>".trans('country_translation.AO')."(+244)"."</option>
            <option data-countryCode='AI' value='1809'>".trans('country_translation.AI')."(+1809)"."</option>
            <option data-countryCode='AG' value='1809'>".trans('country_translation.AG')."(+1809)"."</option>
            <option data-countryCode='AR' value='54'>".trans('country_translation.AR')."(+54)"."</option>
            <option data-countryCode='AM' value='374'>".trans('country_translation.AM')."(+374)"."</option>
            <option data-countryCode='AW' value='297'>".trans('country_translation.AW')."(+297)"."</option>
            <option data-countryCode='AT' value='43'>".trans('country_translation.AT')."(+43)"."</option>
            <option data-countryCode='AZ' value='994'>".trans('country_translation.AZ')."(+994)"."</option>
            <option data-countryCode='BS' value='1809'>".trans('country_translation.BS')."(+1809)"."</option>
            <option data-countryCode='BH' value='973'>".trans('country_translation.BH')."(+973)"."</option>
            <option data-countryCode='BD' value='880'>".trans('country_translation.BD')."(+880)"."</option>
            <option data-countryCode='BB' value='1809'>".trans('country_translation.BB')."(+1809)"."</option>
            <option data-countryCode='BY' value='375'>".trans('country_translation.BY')."(+375)"."</option>
            <option data-countryCode='BE' value='32'>".trans('country_translation.BE')."(+32)"."</option>
            <option data-countryCode='BZ' value='501'>".trans('country_translation.BZ')."(+501)"."</option>
            <option data-countryCode='BJ' value='229'>".trans('country_translation.BJ')."(+229)"."</option>
            <option data-countryCode='BM' value='1809'>".trans('country_translation.BM')."(+1809)"."</option>
            <option data-countryCode='BT' value='975'>".trans('country_translation.BT')."(+975)"."</option>
            <option data-countryCode='BO' value='591'>".trans('country_translation.BO')."(+591)"."</option>
            <option data-countryCode='BA' value='387'>".trans('country_translation.BA')."(+387)"."</option>
            <option data-countryCode='BW' value='267'>".trans('country_translation.BW')."(+267)"."</option>
            <option data-countryCode='BR' value='55'>".trans('country_translation.BR')."(+55)"."</option>
            <option data-countryCode='VG' value='1-284'>".trans('country_translation.VG')."(+1-284)"."</option>
            <option data-countryCode='BN' value='673'>".trans('country_translation.BN')."(+673)"."</option>
            <option data-countryCode='BG' value='359'>".trans('country_translation.BG')."(+359)"."</option>
            <option data-countryCode='BF' value='226'>".trans('country_translation.BF')."(+226)"."</option>
            <option data-countryCode='MM' value='95'>".trans('country_translation.MM')."(+95)"."</option>
            <option data-countryCode='BI' value='257'>".trans('country_translation.BI')."(+257)"."</option>
            <option data-countryCode='KH' value='855'>".trans('country_translation.KH')."(+855)"."</option>
            <option data-countryCode='CM' value='237'>".trans('country_translation.CM')."(+237)"."</option>
            <option data-countryCode='CA' value='1'>".trans('country_translation.CA')."(+1)"."</option>
            <option data-countryCode='CV' value='238'>".trans('country_translation.CV')."(+238)"."</option>
            <option data-countryCode='KY' value='1809'>".trans('country_translation.KY')."(+1809)"."</option>
            <option data-countryCode='CF' value='236'>".trans('country_translation.CF')."(+236)"."</option>
            <option data-countryCode='TD' value='235'>".trans('country_translation.TD')."(+235)"."</option>
            <option data-countryCode='CL' value='56'>".trans('country_translation.CL')."(+56)"."</option>
            <option data-countryCode='CN' value='86'>".trans('country_translation.CN')."(+86)"."</option>
            <option data-countryCode='CX' value='6724'>".trans('country_translation.CX')."(+6724)"."</option>
            <option data-countryCode='CC' value='891'>".trans('country_translation.CC')."(+891)"."</option>
            <option data-countryCode='CO' value='57'>".trans('country_translation.CO')."(+57)"."</option>
            <option data-countryCode='KM' value='269'>".trans('country_translation.KM')."(+269)"."</option>
            <option data-countryCode='CK' value='682'>".trans('country_translation.CK')."(+682)"."</option>
            <option data-countryCode='CR' value='506'>".trans('country_translation.CR')."(+506)"."</option>
            <option data-countryCode='HR' value='385'>".trans('country_translation.HR')."(+385)"."</option>
            <option data-countryCode='CU' value='53'>".trans('country_translation.CU')."(+53)"."</option>
            <option data-countryCode='CW' value='599-9'>".trans('country_translation.CW')."(+599-9)"."</option>
            <option data-countryCode='CY' value='357'>".trans('country_translation.CY')."(+357)"."</option>
            <option data-countryCode='CZ' value='42'>".trans('country_translation.CZ')."(+42)"."</option>
            <option data-countryCode='CD' value='242'>".trans('country_translation.CD')."(+242)"."</option>
            <option data-countryCode='DK' value='45'>".trans('country_translation.DK')."(+45)"."</option>
            <option data-countryCode='DJ' value='253'>".trans('country_translation.DJ')."(+253)"."</option>
            <option data-countryCode='DM' value='1809'>".trans('country_translation.DM')."(+1809)"."</option>
            <option data-countryCode='DO' value='1809'>".trans('country_translation.DO')."(+1809)"."</option>
            <option data-countryCode='EC' value='593'>".trans('country_translation.EC')."(+593)"."</option>
            <option data-countryCode='EG' value='20'>".trans('country_translation.EG')."(+20)"."</option>
            <option data-countryCode='SV' value='503'>".trans('country_translation.SV')."(+503)"."</option>
            <option data-countryCode='GQ' value='240'>".trans('country_translation.GQ')."(+240)"."</option>
            <option data-countryCode='ER' value='291'>".trans('country_translation.ER')."(+291)"."</option>
            <option data-countryCode='EE' value='372'>".trans('country_translation.EE')."(+372)"."</option>
            <option data-countryCode='ET' value='251'>".trans('country_translation.ET')."(+251)"."</option>
            <option data-countryCode='FO' value='298'>".trans('country_translation.FO')."(+298)"."</option>
            <option data-countryCode='FJ' value='679'>".trans('country_translation.FJ')."(+679)"."</option>
            <option data-countryCode='FI' value='358'>".trans('country_translation.FI')."(+358)"."</option>
            <option data-countryCode='FR' value='33'>".trans('country_translation.FR')."(+33)"."</option>
            <option data-countryCode='GF' value='594'>".trans('country_translation.GF')."(+594)"."</option>
            <option data-countryCode='PF' value='689'>".trans('country_translation.PF')."(+689)"."</option>
            <option data-countryCode='GA' value='241'>".trans('country_translation.GA')."(+241)"."</option>
            <option data-countryCode='GM' value='220'>".trans('country_translation.GM')."(+220)"."</option>
            <option data-countryCode='GE' value='995'>".trans('country_translation.GE')."(+995)"."</option>
            <option data-countryCode='DE' value='49'>".trans('country_translation.DE')."(+49)"."</option>
            <option data-countryCode='GH' value='233'>".trans('country_translation.GH')."(+233)"."</option>
            <option data-countryCode='GI' value='350'>".trans('country_translation.GI')."(+350)"."</option>
            <option data-countryCode='GR' value='30'>".trans('country_translation.GR')."(+30)"."</option>
            <option data-countryCode='GL' value='299'>".trans('country_translation.GL')."(+299)"."</option>
            <option data-countryCode='GD' value='1809'>".trans('country_translation.GD')."(+1809)"."</option>
            <option data-countryCode='GP' value='590'>".trans('country_translation.GP')."(+590)"."</option>
            <option data-countryCode='GU' value='671'>".trans('country_translation.GU')."(+671)"."</option>
            <option data-countryCode='GT' value='502'>".trans('country_translation.GT')."(+502)"."</option>
            <option data-countryCode='GG' value='44'>".trans('country_translation.GG')."(+44)"."</option>
            <option data-countryCode='GN' value='224'>".trans('country_translation.GN')."(+224)"."</option>
            <option data-countryCode='GW' value='245'>".trans('country_translation.GW')."(+245)"."</option>
            <option data-countryCode='GY' value='592'>".trans('country_translation.GY')."(+592)"."</option>
            <option data-countryCode='HT' value='509'>".trans('country_translation.HT')."(+509)"."</option>
            <option data-countryCode='VA' value='396'>".trans('country_translation.VA')."(+396)"."</option>
            <option data-countryCode='HN' value='504'>".trans('country_translation.HN')."(+504)"."</option>
            <option data-countryCode='HK' value='852'>".trans('country_translation.HK')."(+852)"."</option>
            <option data-countryCode='HU' value='36'>".trans('country_translation.HU')."(+36)"."</option>
            <option data-countryCode='IS' value='354'>".trans('country_translation.IS')."(+354)"."</option>
            <option data-countryCode='IN' value='91'>".trans('country_translation.IN')."(+91)"."</option>
            <option data-countryCode='ID' value='62'>".trans('country_translation.ID')."(+62)"."</option>
            <option data-countryCode='IR' value='98'>".trans('country_translation.IR')."(+98)"."</option>
            <option data-countryCode='IQ' value='964'>".trans('country_translation.IQ')."(+964)"."</option>
            <option data-countryCode='IE' value='353'>".trans('country_translation.IE')."(+353)"."</option>
            <option data-countryCode='IM' value='44-1624'>".trans('country_translation.IM')."(+44-1624)"."</option>
            <option data-countryCode='IL' value='972'>".trans('country_translation.IL')."(+972)"."</option>
            <option data-countryCode='IT' value='39'>".trans('country_translation.IT')."(+39)"."</option>
            <option data-countryCode='CI' value='225'>".trans('country_translation.CI')."(+225)"."</option>
            <option data-countryCode='JM' value='1876'>".trans('country_translation.JM')."(+1876)"."</option>
            <option data-countryCode='JP' value='81'>".trans('country_translation.JP')."(+81)"."</option>
            <option data-countryCode='JE' value='44'>".trans('country_translation.JE')."(+44)"."</option>
            <option data-countryCode='JO' value='962'>".trans('country_translation.JO')."(+962)"."</option>
            <option data-countryCode='KZ' value='7'>".trans('country_translation.KZ')."(+7)"."</option>
            <option data-countryCode='KE' value='254'>".trans('country_translation.KE')."(+254)"."</option>
            <option data-countryCode='KI' value='686'>".trans('country_translation.KI')."(+686)"."</option>
            <option data-countryCode='XK' value='383'>".trans('country_translation.XK')."(+383)"."</option>
            <option data-countryCode='KW' value='965'>".trans('country_translation.KW')."(+965)"."</option>
            <option data-countryCode='KG' value='996'>".trans('country_translation.KG')."(+996)"."</option>
            <option data-countryCode='LA' value='856'>".trans('country_translation.LA')."(+856)"."</option>
            <option data-countryCode='LV' value='371'>".trans('country_translation.LV')."(+371)"."</option>
            <option data-countryCode='LB' value='961'>".trans('country_translation.LB')."(+961)"."</option>
            <option data-countryCode='LS' value='266'>".trans('country_translation.LS')."(+266)"."</option>
            <option data-countryCode='LR' value='231'>".trans('country_translation.LR')."(+231)"."</option>
            <option data-countryCode='LY' value='218'>".trans('country_translation.LY')."(+218)"."</option>
            <option data-countryCode='LI' value='423'>".trans('country_translation.LI')."(+423)"."</option>
            <option data-countryCode='LT' value='370'>".trans('country_translation.LT')."(+370)"."</option>
            <option data-countryCode='LU' value='352'>".trans('country_translation.LU')."(+352)"."</option>
            <option data-countryCode='MO' value='853'>".trans('country_translation.MO')."(+853)"."</option>
            <option data-countryCode='MK' value='389'>".trans('country_translation.MK')."(+389)"."</option>
            <option data-countryCode='MG' value='261'>".trans('country_translation.MG')."(+261)"."</option>
            <option data-countryCode='MW' value='265'>".trans('country_translation.MW')."(+265)"."</option>
            <option data-countryCode='MY' value='60'>".trans('country_translation.MY')."(+60)"."</option>
            <option data-countryCode='MV' value='960'>".trans('country_translation.MV')."(+960)"."</option>
            <option data-countryCode='ML' value='223'>".trans('country_translation.ML')."(+223)"."</option>
            <option data-countryCode='MT' value='356'>".trans('country_translation.MT')."(+356)"."</option>
            <option data-countryCode='MH' value='692'>".trans('country_translation.MH')."(+692)"."</option>
            <option data-countryCode='MQ' value='596'>".trans('country_translation.MQ')."(+596)"."</option>
            <option data-countryCode='MR' value='222'>".trans('country_translation.MR')."(+222)"."</option>
            <option data-countryCode='MU' value='230'>".trans('country_translation.MU')."(+230)"."</option>
            <option data-countryCode='YT' value='269'>".trans('country_translation.YT')."(+269)"."</option>
            <option data-countryCode='MX' value='52'>".trans('country_translation.MX')."(+52)"."</option>
            <option data-countryCode='FM' value='691'>".trans('country_translation.FM')."(+691)"."</option>
            <option data-countryCode='MD' value='373'>".trans('country_translation.MD')."(+373)"."</option>
            <option data-countryCode='MC' value='3393'>".trans('country_translation.MC')."(+3393)"."</option>
            <option data-countryCode='MN' value='976'>".trans('country_translation.MN')."(+976)"."</option>
            <option data-countryCode='ME' value='382'>".trans('country_translation.ME')."(+382)"."</option>
            <option data-countryCode='MS' value='1809'>".trans('country_translation.MS')."(+1809)"."</option>
            <option data-countryCode='MA' value='212'>".trans('country_translation.MA')."(+212)"."</option>
            <option data-countryCode='MZ' value='258'>".trans('country_translation.MZ')."(+258)"."</option>
            <option data-countryCode='NA' value='264'>".trans('country_translation.NA')."(+264)"."</option>
            <option data-countryCode='NR' value='674'>".trans('country_translation.NR')."(+674)"."</option>
            <option data-countryCode='NP' value='977'>".trans('country_translation.NP')."(+977)"."</option>
            <option data-countryCode='NL' value='31'>".trans('country_translation.NL')."(+31)"."</option>
            <option data-countryCode='AN' value='599'>".trans('country_translation.AN')."(+599)"."</option>
            <option data-countryCode='NC' value='687'>".trans('country_translation.NC')."(+687)"."</option>
            <option data-countryCode='NZ' value='64'>".trans('country_translation.NZ')."(+64)"."</option>
            <option data-countryCode='NI' value='505'>".trans('country_translation.NI')."(+505)"."</option>
            <option data-countryCode='NE' value='227'>".trans('country_translation.NE')."(+227)"."</option>
            <option data-countryCode='NG' value='234'>".trans('country_translation.NG')."(+234)"."</option>
            <option data-countryCode='NU' value='683'>".trans('country_translation.NU')."(+683)"."</option>
            <option data-countryCode='NF' value='6723'>".trans('country_translation.NF')."(+6723)"."</option>
            <option data-countryCode='KP' value='850'>".trans('country_translation.KP')."(+850)"."</option>
            <option data-countryCode='MP' value='1-670'>".trans('country_translation.MP')."(+1-670)"."</option>
            <option data-countryCode='NO' value='47'>".trans('country_translation.NO')."(+47)"."</option>
            <option data-countryCode='OM' value='968'>".trans('country_translation.OM')."(+968)"."</option>
            <option data-countryCode='PK' value='92'>".trans('country_translation.PK')."(+92)"."</option>
            <option data-countryCode='PW' value='680'>".trans('country_translation.PW')."(+680)"."</option>
            <option data-countryCode='PS' value='970'>".trans('country_translation.PS')."(+970)"."</option>
            <option data-countryCode='PA' value='507'>".trans('country_translation.PA')."(+507)"."</option>
            <option data-countryCode='PG' value='675'>".trans('country_translation.PG')."(+675)"."</option>
            <option data-countryCode='PY' value='595'>".trans('country_translation.PY')."(+595)"."</option>
            <option data-countryCode='PE' value='51'>".trans('country_translation.PE')."(+51)"."</option>
            <option data-countryCode='PH' value='63'>".trans('country_translation.PH')."(+63)"."</option>
            <option data-countryCode='PN' value='649'>".trans('country_translation.PN')."(+649)"."</option>
            <option data-countryCode='PL' value='48'>".trans('country_translation.PL')."(+48)"."</option>
            <option data-countryCode='PT' value='351'>".trans('country_translation.PT')."(+351)"."</option>
            <option data-countryCode='PR' value='1809'>".trans('country_translation.PR')."(+1809)"."</option>
            <option data-countryCode='QA' value='974'>".trans('country_translation.QA')."(+974)"."</option>
            <option data-countryCode='CG' value='242'>".trans('country_translation.CG')."(+242)"."</option>
            <option data-countryCode='RE' value='262'>".trans('country_translation.RE')."(+262)"."</option>
            <option data-countryCode='RO' value='40'>".trans('country_translation.RO')."(+40)"."</option>
            <option data-countryCode='RU' value='7'>".trans('country_translation.RU')."(+7)"."</option>
            <option data-countryCode='RW' value='250'>".trans('country_translation.RW')."(+250)"."</option>
            <option data-countryCode='BL' value='590'>".trans('country_translation.BL')."(+590)"."</option>
            <option data-countryCode='SH' value='290'>".trans('country_translation.SH')."(+290)"."</option>
            <option data-countryCode='KN' value='1809'>".trans('country_translation.KN')."(+1809)"."</option>
            <option data-countryCode='LC' value='1809'>".trans('country_translation.LC')."(+1809)"."</option>
            <option data-countryCode='SX' value='1-721'>".trans('country_translation.SX')."(+1-721)"."</option>
            <option data-countryCode='PM' value='508'>".trans('country_translation.PM')."(+508)"."</option>
            <option data-countryCode='VC' value='1809'>".trans('country_translation.VC')."(+1809)"."</option>
            <option data-countryCode='WS' value='685'>".trans('country_translation.WS')."(+685)"."</option>
            <option data-countryCode='SM' value='378'>".trans('country_translation.SM')."(+378)"."</option>
            <option data-countryCode='ST' value='239'>".trans('country_translation.ST')."(+239)"."</option>
            <option data-countryCode='SA' value='966'>".trans('country_translation.SA')."(+966)"."</option>
            <option data-countryCode='SN' value='221'>".trans('country_translation.SN')."(+221)"."</option>
            <option data-countryCode='RS' value='381'>".trans('country_translation.RS')."(+381)"."</option>
            <option data-countryCode='SC' value='248'>".trans('country_translation.SC')."(+248)"."</option>
            <option data-countryCode='SL' value='232'>".trans('country_translation.SL')."(+232)"."</option>
            <option data-countryCode='SG' value='65'>".trans('country_translation.SG')."(+65)"."</option>
            <option data-countryCode='SK' value='42'>".trans('country_translation.SK')."(+42)"."</option>
            <option data-countryCode='SI' value='386'>".trans('country_translation.SI')."(+386)"."</option>
            <option data-countryCode='SB' value='677'>".trans('country_translation.SB')."(+677)"."</option>
            <option data-countryCode='SO' value='252'>".trans('country_translation.SO')."(+252)"."</option>
            <option data-countryCode='ZA' value='27'>".trans('country_translation.ZA')."(+27)"."</option>
            <option data-countryCode='KR' value='82'>".trans('country_translation.KR')."(+82)"."</option>
            <option data-countryCode='SS' value='211'>".trans('country_translation.SS')."(+211)"."</option>
            <option data-countryCode='ES' value='34'>".trans('country_translation.ES')."(+34)"."</option>
            <option data-countryCode='LK' value='94'>".trans('country_translation.LK')."(+94)"."</option>
            <option data-countryCode='SD' value='249'>".trans('country_translation.SD')."(+249)"."</option>
            <option data-countryCode='SR' value='597'>".trans('country_translation.SR')."(+597)"."</option>
            <option data-countryCode='SJ' value='47'>".trans('country_translation.SJ')."(+47)"."</option>
            <option data-countryCode='SZ' value='268'>".trans('country_translation.SZ')."(+268)"."</option>
            <option data-countryCode='SE' value='46'>".trans('country_translation.SE')."(+46)"."</option>
            <option data-countryCode='CH' value='41'>".trans('country_translation.CH')."(+41)"."</option>
            <option data-countryCode='SY' value='963'>".trans('country_translation.SY')."(+963)"."</option>
            <option data-countryCode='TW' value='886'>".trans('country_translation.TW')."(+886)"."</option>
            <option data-countryCode='TJ' value='992'>".trans('country_translation.TJ')."(+992)"."</option>
            <option data-countryCode='TZ' value='255'>".trans('country_translation.TZ')."(+255)"."</option>
            <option data-countryCode='TH' value='66'>".trans('country_translation.TH')."(+66)"."</option>
            <option data-countryCode='TL' value='670'>".trans('country_translation.TL')."(+670)"."</option>
            <option data-countryCode='TG' value='228'>".trans('country_translation.TG')."(+228)"."</option>
            <option data-countryCode='TK' value='690'>".trans('country_translation.TK')."(+690)"."</option>
            <option data-countryCode='TO' value='676'>".trans('country_translation.TO')."(+676)"."</option>
            <option data-countryCode='TT' value='296'>".trans('country_translation.TT')."(+296)"."</option>
            <option data-countryCode='TN' value='216'>".trans('country_translation.TN')."(+216)"."</option>
            <option data-countryCode='TR' value='90'>".trans('country_translation.TR')."(+90)"."</option>
            <option data-countryCode='TM' value='993'>".trans('country_translation.TM')."(+993)"."</option>
            <option data-countryCode='TC' value='1-649'>".trans('country_translation.TC')."(+1-649)"."</option>
            <option data-countryCode='TV' value='688'>".trans('country_translation.TV')."(+688)"."</option>
            <option data-countryCode='UG' value='256'>".trans('country_translation.UG')."(+256)"."</option>
            <option data-countryCode='UA' value='380'>".trans('country_translation.UA')."(+380)"."</option>
            <option data-countryCode='AE' value='971'>".trans('country_translation.AE')."(+971)"."</option>
            <option data-countryCode='UY' value='598'>".trans('country_translation.UY')."(+598)"."</option>
            <option data-countryCode='UZ' value='998'>".trans('country_translation.UZ')."(+998)"."</option>
            <option data-countryCode='VU' value='678'>".trans('country_translation.VU')."(+678)"."</option>
            <option data-countryCode='VE' value='58'>".trans('country_translation.VE')."(+58)"."</option>
            <option data-countryCode='VN' value='84'>".trans('country_translation.VN')."(+84)"."</option>
            <option data-countryCode='VI' value='1809'>".trans('country_translation.VI')."(+1809)"."</option>
            <option data-countryCode='WF' value='681'>".trans('country_translation.WF')."(+681)"."</option>
            <option data-countryCode='YE' value='967'>".trans('country_translation.YE')."(+967)"."</option>
            <option data-countryCode='ZM' value='260'>".trans('country_translation.ZM')."(+260)"."</option>
            <option data-countryCode='ZW' value='263'>".trans('country_translation.ZW')."(+263)"."</option>
            <option data-countryCode='AX' value='358'>".trans('country_translation.AX')."(+358)"."</option>

            </optgroup>
            ";
            }
            else
            {
            echo "
            <option data-countryCode='DE' value='49'>".trans('country_translation.DE')."(+49)"."</option>
            <option data-countryCode='CH' value='41'>".trans('country_translation.CH')."(+41)"."</option>
            <option data-countryCode='AT' value='43'>".trans('country_translation.AT')."(+43)"."</option>
            <optgroup label='----------------------------------'>
            <option data-countryCode='AF' value='93'>".trans('country_translation.AF')."(+93)"."</option>
            <option data-countryCode='AL' value='355'>".trans('country_translation.AL')."(+355)"."</option>
            <option data-countryCode='DZ' value='213'>".trans('country_translation.DZ')."(+213)"."</option>
            <option data-countryCode='AS' value='1-684'>".trans('country_translation.AS')."(+1-684)"."</option>
            <option data-countryCode='AD' value='376'>".trans('country_translation.AD')."(+376)"."</option>
            <option data-countryCode='AO' value='244'>".trans('country_translation.AO')."(+244)"."</option>
            <option data-countryCode='AI' value='1809'>".trans('country_translation.AI')."(+1809)"."</option>
            <option data-countryCode='AG' value='1809'>".trans('country_translation.AG')."(+1809)"."</option>
            <option data-countryCode='AR' value='54'>".trans('country_translation.AR')."(+54)"."</option>
            <option data-countryCode='AM' value='374'>".trans('country_translation.AM')."(+374)"."</option>
            <option data-countryCode='AW' value='297'>".trans('country_translation.AW')."(+297)"."</option>
            <option data-countryCode='AU' value='61'>".trans('country_translation.AU')."(+61)"."</option>
            <option data-countryCode='AZ' value='994'>".trans('country_translation.AZ')."(+994)"."</option>
            <option data-countryCode='BS' value='1809'>".trans('country_translation.BS')."(+1809)"."</option>
            <option data-countryCode='BH' value='973'>".trans('country_translation.BH')."(+973)"."</option>
            <option data-countryCode='BD' value='880'>".trans('country_translation.BD')."(+880)"."</option>
            <option data-countryCode='BB' value='1809'>".trans('country_translation.BB')."(+1809)"."</option>
            <option data-countryCode='BY' value='375'>".trans('country_translation.BY')."(+375)"."</option>
            <option data-countryCode='BE' value='32'>".trans('country_translation.BE')."(+32)"."</option>
            <option data-countryCode='BZ' value='501'>".trans('country_translation.BZ')."(+501)"."</option>
            <option data-countryCode='BJ' value='229'>".trans('country_translation.BJ')."(+229)"."</option>
            <option data-countryCode='BM' value='1809'>".trans('country_translation.BM')."(+1809)"."</option>
            <option data-countryCode='BT' value='975'>".trans('country_translation.BT')."(+975)"."</option>
            <option data-countryCode='BO' value='591'>".trans('country_translation.BO')."(+591)"."</option>
            <option data-countryCode='BA' value='387'>".trans('country_translation.BA')."(+387)"."</option>
            <option data-countryCode='BW' value='267'>".trans('country_translation.BW')."(+267)"."</option>
            <option data-countryCode='BR' value='55'>".trans('country_translation.BR')."(+55)"."</option>
            <option data-countryCode='VG' value='1-284'>".trans('country_translation.VG')."(+1-284)"."</option>
            <option data-countryCode='BN' value='673'>".trans('country_translation.BN')."(+673)"."</option>
            <option data-countryCode='BG' value='359'>".trans('country_translation.BG')."(+359)"."</option>
            <option data-countryCode='BF' value='226'>".trans('country_translation.BF')."(+226)"."</option>
            <option data-countryCode='MM' value='95'>".trans('country_translation.MM')."(+95)"."</option>
            <option data-countryCode='BI' value='257'>".trans('country_translation.BI')."(+257)"."</option>
            <option data-countryCode='KH' value='855'>".trans('country_translation.KH')."(+855)"."</option>
            <option data-countryCode='CM' value='237'>".trans('country_translation.CM')."(+237)"."</option>
            <option data-countryCode='CA' value='1'>".trans('country_translation.CA')."(+1)"."</option>
            <option data-countryCode='CV' value='238'>".trans('country_translation.CV')."(+238)"."</option>
            <option data-countryCode='KY' value='1809'>".trans('country_translation.KY')."(+1809)"."</option>
            <option data-countryCode='CF' value='236'>".trans('country_translation.CF')."(+236)"."</option>
            <option data-countryCode='TD' value='235'>".trans('country_translation.TD')."(+235)"."</option>
            <option data-countryCode='CL' value='56'>".trans('country_translation.CL')."(+56)"."</option>
            <option data-countryCode='CN' value='86'>".trans('country_translation.CN')."(+86)"."</option>
            <option data-countryCode='CX' value='6724'>".trans('country_translation.CX')."(+6724)"."</option>
            <option data-countryCode='CC' value='891'>".trans('country_translation.CC')."(+891)"."</option>
            <option data-countryCode='CO' value='57'>".trans('country_translation.CO')."(+57)"."</option>
            <option data-countryCode='KM' value='269'>".trans('country_translation.KM')."(+269)"."</option>
            <option data-countryCode='CK' value='682'>".trans('country_translation.CK')."(+682)"."</option>
            <option data-countryCode='CR' value='506'>".trans('country_translation.CR')."(+506)"."</option>
            <option data-countryCode='HR' value='385'>".trans('country_translation.HR')."(+385)"."</option>
            <option data-countryCode='CU' value='53'>".trans('country_translation.CU')."(+53)"."</option>
            <option data-countryCode='CW' value='599-9'>".trans('country_translation.CW')."(+599-9)"."</option>
            <option data-countryCode='CY' value='357'>".trans('country_translation.CY')."(+357)"."</option>
            <option data-countryCode='CZ' value='42'>".trans('country_translation.CZ')."(+42)"."</option>
            <option data-countryCode='CD' value='242'>".trans('country_translation.CD')."(+242)"."</option>
            <option data-countryCode='DK' value='45'>".trans('country_translation.DK')."(+45)"."</option>
            <option data-countryCode='DJ' value='253'>".trans('country_translation.DJ')."(+253)"."</option>
            <option data-countryCode='DM' value='1809'>".trans('country_translation.DM')."(+1809)"."</option>
            <option data-countryCode='DO' value='1809'>".trans('country_translation.DO')."(+1809)"."</option>
            <option data-countryCode='EC' value='593'>".trans('country_translation.EC')."(+593)"."</option>
            <option data-countryCode='EG' value='20'>".trans('country_translation.EG')."(+20)"."</option>
            <option data-countryCode='SV' value='503'>".trans('country_translation.SV')."(+503)"."</option>
            <option data-countryCode='GQ' value='240'>".trans('country_translation.GQ')."(+240)"."</option>
            <option data-countryCode='ER' value='291'>".trans('country_translation.ER')."(+291)"."</option>
            <option data-countryCode='EE' value='372'>".trans('country_translation.EE')."(+372)"."</option>
            <option data-countryCode='ET' value='251'>".trans('country_translation.ET')."(+251)"."</option>
            <option data-countryCode='FO' value='298'>".trans('country_translation.FO')."(+298)"."</option>
            <option data-countryCode='FJ' value='679'>".trans('country_translation.FJ')."(+679)"."</option>
            <option data-countryCode='FI' value='358'>".trans('country_translation.FI')."(+358)"."</option>
            <option data-countryCode='FR' value='33'>".trans('country_translation.FR')."(+33)"."</option>
            <option data-countryCode='GF' value='594'>".trans('country_translation.GF')."(+594)"."</option>
            <option data-countryCode='PF' value='689'>".trans('country_translation.PF')."(+689)"."</option>
            <option data-countryCode='GA' value='241'>".trans('country_translation.GA')."(+241)"."</option>
            <option data-countryCode='GM' value='220'>".trans('country_translation.GM')."(+220)"."</option>
            <option data-countryCode='GE' value='995'>".trans('country_translation.GE')."(+995)"."</option>
            <option data-countryCode='GH' value='233'>".trans('country_translation.GH')."(+233)"."</option>
            <option data-countryCode='GI' value='350'>".trans('country_translation.GI')."(+350)"."</option>
            <option data-countryCode='GR' value='30'>".trans('country_translation.GR')."(+30)"."</option>
            <option data-countryCode='GL' value='299'>".trans('country_translation.GL')."(+299)"."</option>
            <option data-countryCode='GD' value='1809'>".trans('country_translation.GD')."(+1809)"."</option>
            <option data-countryCode='GP' value='590'>".trans('country_translation.GP')."(+590)"."</option>
            <option data-countryCode='GU' value='671'>".trans('country_translation.GU')."(+671)"."</option>
            <option data-countryCode='GT' value='502'>".trans('country_translation.GT')."(+502)"."</option>
            <option data-countryCode='GG' value='44'>".trans('country_translation.GG')."(+44)"."</option>
            <option data-countryCode='GN' value='224'>".trans('country_translation.GN')."(+224)"."</option>
            <option data-countryCode='GW' value='245'>".trans('country_translation.GW')."(+245)"."</option>
            <option data-countryCode='GY' value='592'>".trans('country_translation.GY')."(+592)"."</option>
            <option data-countryCode='HT' value='509'>".trans('country_translation.HT')."(+509)"."</option>
            <option data-countryCode='VA' value='396'>".trans('country_translation.VA')."(+396)"."</option>
            <option data-countryCode='HN' value='504'>".trans('country_translation.HN')."(+504)"."</option>
            <option data-countryCode='HK' value='852'>".trans('country_translation.HK')."(+852)"."</option>
            <option data-countryCode='HU' value='36'>".trans('country_translation.HU')."(+36)"."</option>
            <option data-countryCode='IS' value='354'>".trans('country_translation.IS')."(+354)"."</option>
            <option data-countryCode='IN' value='91'>".trans('country_translation.IN')."(+91)"."</option>
            <option data-countryCode='ID' value='62'>".trans('country_translation.ID')."(+62)"."</option>
            <option data-countryCode='IR' value='98'>".trans('country_translation.IR')."(+98)"."</option>
            <option data-countryCode='IQ' value='964'>".trans('country_translation.IQ')."(+964)"."</option>
            <option data-countryCode='IE' value='353'>".trans('country_translation.IE')."(+353)"."</option>
            <option data-countryCode='IM' value='44-1624'>".trans('country_translation.IM')."(+44-1624)"."</option>
            <option data-countryCode='IL' value='972'>".trans('country_translation.IL')."(+972)"."</option>
            <option data-countryCode='IT' value='39'>".trans('country_translation.IT')."(+39)"."</option>
            <option data-countryCode='CI' value='225'>".trans('country_translation.CI')."(+225)"."</option>
            <option data-countryCode='JM' value='1876'>".trans('country_translation.JM')."(+1876)"."</option>
            <option data-countryCode='JP' value='81'>".trans('country_translation.JP')."(+81)"."</option>
            <option data-countryCode='JE' value='44'>".trans('country_translation.JE')."(+44)"."</option>
            <option data-countryCode='JO' value='962'>".trans('country_translation.JO')."(+962)"."</option>
            <option data-countryCode='KZ' value='7'>".trans('country_translation.KZ')."(+7)"."</option>
            <option data-countryCode='KE' value='254'>".trans('country_translation.KE')."(+254)"."</option>
            <option data-countryCode='KI' value='686'>".trans('country_translation.KI')."(+686)"."</option>
            <option data-countryCode='XK' value='383'>".trans('country_translation.XK')."(+383)"."</option>
            <option data-countryCode='KW' value='965'>".trans('country_translation.KW')."(+965)"."</option>
            <option data-countryCode='KG' value='996'>".trans('country_translation.KG')."(+996)"."</option>
            <option data-countryCode='LA' value='856'>".trans('country_translation.LA')."(+856)"."</option>
            <option data-countryCode='LV' value='371'>".trans('country_translation.LV')."(+371)"."</option>
            <option data-countryCode='LB' value='961'>".trans('country_translation.LB')."(+961)"."</option>
            <option data-countryCode='LS' value='266'>".trans('country_translation.LS')."(+266)"."</option>
            <option data-countryCode='LR' value='231'>".trans('country_translation.LR')."(+231)"."</option>
            <option data-countryCode='LY' value='218'>".trans('country_translation.LY')."(+218)"."</option>
            <option data-countryCode='LI' value='423'>".trans('country_translation.LI')."(+423)"."</option>
            <option data-countryCode='LT' value='370'>".trans('country_translation.LT')."(+370)"."</option>
            <option data-countryCode='LU' value='352'>".trans('country_translation.LU')."(+352)"."</option>
            <option data-countryCode='MO' value='853'>".trans('country_translation.MO')."(+853)"."</option>
            <option data-countryCode='MK' value='389'>".trans('country_translation.MK')."(+389)"."</option>
            <option data-countryCode='MG' value='261'>".trans('country_translation.MG')."(+261)"."</option>
            <option data-countryCode='MW' value='265'>".trans('country_translation.MW')."(+265)"."</option>
            <option data-countryCode='MY' value='60'>".trans('country_translation.MY')."(+60)"."</option>
            <option data-countryCode='MV' value='960'>".trans('country_translation.MV')."(+960)"."</option>
            <option data-countryCode='ML' value='223'>".trans('country_translation.ML')."(+223)"."</option>
            <option data-countryCode='MT' value='356'>".trans('country_translation.MT')."(+356)"."</option>
            <option data-countryCode='MH' value='692'>".trans('country_translation.MH')."(+692)"."</option>
            <option data-countryCode='MQ' value='596'>".trans('country_translation.MQ')."(+596)"."</option>
            <option data-countryCode='MR' value='222'>".trans('country_translation.MR')."(+222)"."</option>
            <option data-countryCode='MU' value='230'>".trans('country_translation.MU')."(+230)"."</option>
            <option data-countryCode='YT' value='269'>".trans('country_translation.YT')."(+269)"."</option>
            <option data-countryCode='MX' value='52'>".trans('country_translation.MX')."(+52)"."</option>
            <option data-countryCode='FM' value='691'>".trans('country_translation.FM')."(+691)"."</option>
            <option data-countryCode='MD' value='373'>".trans('country_translation.MD')."(+373)"."</option>
            <option data-countryCode='MC' value='3393'>".trans('country_translation.MC')."(+3393)"."</option>
            <option data-countryCode='MN' value='976'>".trans('country_translation.MN')."(+976)"."</option>
            <option data-countryCode='ME' value='382'>".trans('country_translation.ME')."(+382)"."</option>
            <option data-countryCode='MS' value='1809'>".trans('country_translation.MS')."(+1809)"."</option>
            <option data-countryCode='MA' value='212'>".trans('country_translation.MA')."(+212)"."</option>
            <option data-countryCode='MZ' value='258'>".trans('country_translation.MZ')."(+258)"."</option>
            <option data-countryCode='NA' value='264'>".trans('country_translation.NA')."(+264)"."</option>
            <option data-countryCode='NR' value='674'>".trans('country_translation.NR')."(+674)"."</option>
            <option data-countryCode='NP' value='977'>".trans('country_translation.NP')."(+977)"."</option>
            <option data-countryCode='NL' value='31'>".trans('country_translation.NL')."(+31)"."</option>
            <option data-countryCode='AN' value='599'>".trans('country_translation.AN')."(+599)"."</option>
            <option data-countryCode='NC' value='687'>".trans('country_translation.NC')."(+687)"."</option>
            <option data-countryCode='NZ' value='64'>".trans('country_translation.NZ')."(+64)"."</option>
            <option data-countryCode='NI' value='505'>".trans('country_translation.NI')."(+505)"."</option>
            <option data-countryCode='NE' value='227'>".trans('country_translation.NE')."(+227)"."</option>
            <option data-countryCode='NG' value='234'>".trans('country_translation.NG')."(+234)"."</option>
            <option data-countryCode='NU' value='683'>".trans('country_translation.NU')."(+683)"."</option>
            <option data-countryCode='NF' value='6723'>".trans('country_translation.NF')."(+6723)"."</option>
            <option data-countryCode='KP' value='850'>".trans('country_translation.KP')."(+850)"."</option>
            <option data-countryCode='MP' value='1-670'>".trans('country_translation.MP')."(+1-670)"."</option>
            <option data-countryCode='NO' value='47'>".trans('country_translation.NO')."(+47)"."</option>
            <option data-countryCode='OM' value='968'>".trans('country_translation.OM')."(+968)"."</option>
            <option data-countryCode='PK' value='92'>".trans('country_translation.PK')."(+92)"."</option>
            <option data-countryCode='PW' value='680'>".trans('country_translation.PW')."(+680)"."</option>
            <option data-countryCode='PS' value='970'>".trans('country_translation.PS')."(+970)"."</option>
            <option data-countryCode='PA' value='507'>".trans('country_translation.PA')."(+507)"."</option>
            <option data-countryCode='PG' value='675'>".trans('country_translation.PG')."(+675)"."</option>
            <option data-countryCode='PY' value='595'>".trans('country_translation.PY')."(+595)"."</option>
            <option data-countryCode='PE' value='51'>".trans('country_translation.PE')."(+51)"."</option>
            <option data-countryCode='PH' value='63'>".trans('country_translation.PH')."(+63)"."</option>
            <option data-countryCode='PN' value='649'>".trans('country_translation.PN')."(+649)"."</option>
            <option data-countryCode='PL' value='48'>".trans('country_translation.PL')."(+48)"."</option>
            <option data-countryCode='PT' value='351'>".trans('country_translation.PT')."(+351)"."</option>
            <option data-countryCode='PR' value='1809'>".trans('country_translation.PR')."(+1809)"."</option>
            <option data-countryCode='QA' value='974'>".trans('country_translation.QA')."(+974)"."</option>
            <option data-countryCode='CG' value='242'>".trans('country_translation.CG')."(+242)"."</option>
            <option data-countryCode='RE' value='262'>".trans('country_translation.RE')."(+262)"."</option>
            <option data-countryCode='RO' value='40'>".trans('country_translation.RO')."(+40)"."</option>
            <option data-countryCode='RU' value='7'>".trans('country_translation.RU')."(+7)"."</option>
            <option data-countryCode='RW' value='250'>".trans('country_translation.RW')."(+250)"."</option>
            <option data-countryCode='BL' value='590'>".trans('country_translation.BL')."(+590)"."</option>
            <option data-countryCode='SH' value='290'>".trans('country_translation.SH')."(+290)"."</option>
            <option data-countryCode='KN' value='1809'>".trans('country_translation.KN')."(+1809)"."</option>
            <option data-countryCode='LC' value='1809'>".trans('country_translation.LC')."(+1809)"."</option>
            <option data-countryCode='SX' value='1-721'>".trans('country_translation.SX')."(+1-721)"."</option>
            <option data-countryCode='PM' value='508'>".trans('country_translation.PM')."(+508)"."</option>
            <option data-countryCode='VC' value='1809'>".trans('country_translation.VC')."(+1809)"."</option>
            <option data-countryCode='WS' value='685'>".trans('country_translation.WS')."(+685)"."</option>
            <option data-countryCode='SM' value='378'>".trans('country_translation.SM')."(+378)"."</option>
            <option data-countryCode='ST' value='239'>".trans('country_translation.ST')."(+239)"."</option>
            <option data-countryCode='SA' value='966'>".trans('country_translation.SA')."(+966)"."</option>
            <option data-countryCode='SN' value='221'>".trans('country_translation.SN')."(+221)"."</option>
            <option data-countryCode='RS' value='381'>".trans('country_translation.RS')."(+381)"."</option>
            <option data-countryCode='SC' value='248'>".trans('country_translation.SC')."(+248)"."</option>
            <option data-countryCode='SL' value='232'>".trans('country_translation.SL')."(+232)"."</option>
            <option data-countryCode='SG' value='65'>".trans('country_translation.SG')."(+65)"."</option>
            <option data-countryCode='SK' value='42'>".trans('country_translation.SK')."(+42)"."</option>
            <option data-countryCode='SI' value='386'>".trans('country_translation.SI')."(+386)"."</option>
            <option data-countryCode='SB' value='677'>".trans('country_translation.SB')."(+677)"."</option>
            <option data-countryCode='SO' value='252'>".trans('country_translation.SO')."(+252)"."</option>
            <option data-countryCode='ZA' value='27'>".trans('country_translation.ZA')."(+27)"."</option>
            <option data-countryCode='KR' value='82'>".trans('country_translation.KR')."(+82)"."</option>
            <option data-countryCode='SS' value='211'>".trans('country_translation.SS')."(+211)"."</option>
            <option data-countryCode='ES' value='34'>".trans('country_translation.ES')."(+34)"."</option>
            <option data-countryCode='LK' value='94'>".trans('country_translation.LK')."(+94)"."</option>
            <option data-countryCode='SD' value='249'>".trans('country_translation.SD')."(+249)"."</option>
            <option data-countryCode='SR' value='597'>".trans('country_translation.SR')."(+597)"."</option>
            <option data-countryCode='SJ' value='47'>".trans('country_translation.SJ')."(+47)"."</option>
            <option data-countryCode='SZ' value='268'>".trans('country_translation.SZ')."(+268)"."</option>
            <option data-countryCode='SE' value='46'>".trans('country_translation.SE')."(+46)"."</option>
            <option data-countryCode='SY' value='963'>".trans('country_translation.SY')."(+963)"."</option>
            <option data-countryCode='TW' value='886'>".trans('country_translation.TW')."(+886)"."</option>
            <option data-countryCode='TJ' value='992'>".trans('country_translation.TJ')."(+992)"."</option>
            <option data-countryCode='TZ' value='255'>".trans('country_translation.TZ')."(+255)"."</option>
            <option data-countryCode='TH' value='66'>".trans('country_translation.TH')."(+66)"."</option>
            <option data-countryCode='TL' value='670'>".trans('country_translation.TL')."(+670)"."</option>
            <option data-countryCode='TG' value='228'>".trans('country_translation.TG')."(+228)"."</option>
            <option data-countryCode='TK' value='690'>".trans('country_translation.TK')."(+690)"."</option>
            <option data-countryCode='TO' value='676'>".trans('country_translation.TO')."(+676)"."</option>
            <option data-countryCode='TT' value='296'>".trans('country_translation.TT')."(+296)"."</option>
            <option data-countryCode='TN' value='216'>".trans('country_translation.TN')."(+216)"."</option>
            <option data-countryCode='TR' value='90'>".trans('country_translation.TR')."(+90)"."</option>
            <option data-countryCode='TM' value='993'>".trans('country_translation.TM')."(+993)"."</option>
            <option data-countryCode='TC' value='1-649'>".trans('country_translation.TC')."(+1-649)"."</option>
            <option data-countryCode='TV' value='688'>".trans('country_translation.TV')."(+688)"."</option>
            <option data-countryCode='UG' value='256'>".trans('country_translation.UG')."(+256)"."</option>
            <option data-countryCode='UA' value='380'>".trans('country_translation.UA')."(+380)"."</option>
            <option data-countryCode='AE' value='971'>".trans('country_translation.AE')."(+971)"."</option>
            <option data-countryCode='GB' value='44'>".trans('country_translation.GB')."(+44)"."</option>
            <option data-countryCode='US' value='1'>".trans('country_translation.US')."(+1)"."</option>
            <option data-countryCode='UY' value='598'>".trans('country_translation.UY')."(+598)"."</option>
            <option data-countryCode='UZ' value='998'>".trans('country_translation.UZ')."(+998)"."</option>
            <option data-countryCode='VU' value='678'>".trans('country_translation.VU')."(+678)"."</option>
            <option data-countryCode='VE' value='58'>".trans('country_translation.VE')."(+58)"."</option>
            <option data-countryCode='VN' value='84'>".trans('country_translation.VN')."(+84)"."</option>
            <option data-countryCode='VI' value='1809'>".trans('country_translation.VI')."(+1809)"."</option>
            <option data-countryCode='WF' value='681'>".trans('country_translation.WF')."(+681)"."</option>
            <option data-countryCode='YE' value='967'>".trans('country_translation.YE')."(+967)"."</option>
            <option data-countryCode='ZM' value='260'>".trans('country_translation.ZM')."(+260)"."</option>
            <option data-countryCode='ZW' value='263'>".trans('country_translation.ZW')."(+263)"."</option>
            <option data-countryCode='AX' value='358'>".trans('country_translation.AX')."(+358)"."</option>
            </optgroup>
            ";
            }
            ?>
        </select></div>
        <div class="col-md-1 col-sm-1 col-xs-2" style=" padding: 0px;"><input type="text" class="form-control box-shadow " id="phone-code-input" style="border: 0px;padding: 2px;background: #fff; color: #aaa" name="phone-code" readonly/></div>
        <div class="col-md-7 col-sm-8 col-xs-6" style="padding: 0px"><input type="number" class="form-control box-shadow" style="border: 0px" placeholder="<?php echo trans('contact.placeholder_number')?>" name="phone-number" id="phone" /></div>
        </div>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 inner-addon right-addon" style="margin-bottom:20px;">
    		<input type="text" class="form-control purpose box-shadow" placeholder="<?php echo trans('contact.placeholder_name')?>"  name="name" id="name" />
		</div>
       	<div class="col-md-6 col-sm-12 col-xs-12 inner-addon right-addon" style="margin-bottom:20px;">
   			<input type="text" class="form-control purpose box-shadow" placeholder="<?php echo trans('contact.placeholder_email')?>" name="email" id="email" />
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px;">
   			<textarea type="text"  rows="6" cols="124" placeholder="<?php echo trans('contact.placeholder_message')?>" name="message"></textarea>
		</div>
    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;text-align: center">
       <button class="btn" id="btn2"><?php echo trans('contact.submit_button')?></button>
       <img id="loader" src="image/hourglass.gif" style="display:none; width:35px; height: 35px;"/>
    </div>
   	</div>
   	<div class="col-md-1 hidden-md"></div>
    </form>
</div>
</div>
   <div class="col-md-12 col-sm-12 col-xs-12" style="background: #f9f9f9  ">
          <div class="contact-strip" style="padding-bottom:15px;">
          <div class="col-md-4 col-sm-4 col-xs-12 center-align">
          <div class="col-md-12 col-sm-12 col-xs-12 make-div-center">
          <div class="col-md-4 col-sm-6 col-xs-12 contact-type"><?php echo trans('contact.country_two')?></div>
          <div class="col-md-8 col-sm-6 col-xs-12" style="padding: 0px;text-align: left;">
          <ul><li>+44 20 32877307</li><li>(<?php echo trans('contact.country_two_time')?>)</li></ul>
          </div>
          </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 center-align middle-div">
          <div class="col-md-12 col-sm-12 col-xs-12 make-div-center">
          <div class="col-md-4 col-sm-6 col-xs-12 contact-type"><?php echo trans('contact.country_three')?></div>
          <div class="col-md-8 col-sm-6 col-xs-12" style="padding: 0px;text-align: left;">
          <ul><li>+49 7531 8919 990</li><li>(<?php echo trans('contact.country_three_time')?>)</li></ul>
          </div>
          </div>
          </div>
          <div class="col-md-4 center-align">
          <div class="col-md-12 col-sm-12 col-xs-12 make-div-center">
          <div class="col-md-4 col-sm-6 col-xs-12 contact-type"><?php echo trans('contact.country_four')?></div>
          <div class="col-md-8 col-sm-6 col-xs-12" style="padding: 0px;text-align: left;">
          <ul><li>+41 44 586 70 07</li><li>(<?php echo trans('contact.country_four_time')?>)</li></ul>
          </div>
          </div>
          </div>
     </div>
     <div class="contact-strip" style="padding-top:15px;">
          <!-- <div class="col-md-4 col-sm-4 col-xs-12 center-align">
          <div class="col-md-12 col-sm-12 col-xs-12 make-div-center">
          <div class="col-md-4 col-sm-6 col-xs-12 contact-type"><?php echo trans('contact.email')?></div>
          <div class="col-md-8 col-sm-6 col-xs-12" style="padding: 0px;text-align: left;">
          <ul><li>support@stafftimes.com</li><li>info@stafftimes.com</li></ul>
          </div>
          </div>
          </div> -->
          <div class="col-md-6 col-sm-6 col-xs-12 center-align" style="border-right: 1px solid #959595;">
          <div class="col-md-12 col-sm-12 col-xs-12 make-div-center">
          <div class="col-md-4 col-sm-6 col-xs-12 contact-type"><?php echo trans('contact.country_one')?></div>
          <div class="col-md-8 col-sm-6 col-xs-12" style="padding: 0px;text-align: left;">
          <ul><li>(516) 704 9554</li><li>(<?php echo trans('contact.country_one_time')?>)</li></ul>
          </div>
          </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 center-align">
          <div class="col-md-12 col-sm-12 col-xs-12 make-div-center">
          <div class="col-md-4 col-sm-6 col-xs-12 contact-type"><?php echo trans('contact.skype')?></div>
          <div class="col-md-8 col-sm-6 col-xs-12" style="padding: 0px;text-align: left;">
          <ul><li style="padding-top: 10px;">Staff Times Support</li></ul>
          </div>
          </div>
          </div>
        </div> 
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 strip">
    <div class="col-md-9 col-sm-8 col-xs-12"><h3 class="strip-text"><?php echo trans('contact.call_to_action')?></h3></div>
    <div class="col-md-3 col-sm-4 col-xs-12"><a href="<?php echo route('faq')?>"><button class="btn1"><?php echo trans('contact.read_more')?></button></a></div>
    </div>
<script src="js/jquery.form.js"></script>
<script>
var fields = "<?php echo trans('popup.all_fields');?>";
var check = "<?php echo trans('popup.check_email');?>";
var back = "<?php echo trans('popup.back_soon');?>";
var try_again = "<?php echo trans('popup.try_again');?>";
var success = "<?php echo trans('popup.success');?>";
var error_="<?php echo trans('popup.error_');?>!";
</script>
<script src="<?php echo url('js/contactus.js')?>"></script>
<?php  include 'layout/main_page_footer.php';?>
