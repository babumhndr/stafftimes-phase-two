
  <?php  include 'layout/header.php';?>
  <link rel="stylesheet" href="<?php echo url('assets/css/add_employee.css')?>">
  		<!-- <h3>Here starts everything&hellip;</h3> -->
  		<div class="row invite">
  			<div class="col-md-12 link">
  				<p> 
  				<span class="template_link">
  				<a href="<?php echo url('dashboard')?>"><?php echo trans('add_employee.home')?></a></span>  /
                    <a ><?php echo trans('add_employee.Add_Employee')?></a>
  				</p>
  			</div>
        <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="panel">
                      <h3><b>User Details</b></h3>
                      <br>
                      <div class="row top_space">
                        <div class="col-md-4">
                          <label>Email Id</label>
                          <input type="text" class="invite_mail_id" id="name" name="email" placeholder="Your Text"></input>
                        </div>
                        <div class="col-md-2">
                          <button class="send_invite" type="submit" id="send_invite_btn_def">Send Invite</button>
                        </div>
                        <div class="col-md-6">
                          <label>Phone Number</label>
                          <input type="text" class="invite_mail_id" id="name" name="phone" placeholder="Your Text"></input>
                        </div>
                      </div>
                      <div class="row top_space">
                        <div class="col-md-6">
                          <label>Staff Id</label>
                          <input type="text" class="invite_mail_id" id="name" name="staff_id" placeholder="Your Text"></input>
                        </div>
                        <div class="col-md-4">
                          <label>Team</label>
                          <input type="text" class="invite_mail_id" id="name" name="team" placeholder="Your Text"></input>
                        </div>
                        <div class="col-md-2">
                          <button class="send_invite" type="submit" id="send_invite_btn_def">Create Team</button>
                        </div>
                        
                      </div>
                      <div class="row top_space">
                          <center>
                            <button class="send_invite" type="submit" id="create_terms_of_employment_btn">Create terms of employment</button>
                          </center>
                        </div>
                    </div>
                </div>
        </div>
  			<!-- <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="panel">
                    <div class="invite_people_icon">
                        <img src="<?php echo url('assets/images/invite_main.png')?>"class="mail_box">
                    </div>
                    <div class="invite_people_heading">
                        <p><?php echo trans('dashboard.invite')?></p>
                    </div>
                    <div class="invite_people_sub_heading">
                        <p><?php echo trans('dashboard.invite_msg')?></p>
                    </div>
                    <div class="submit_invite">
                    <form id="invite" method="post" action="invitePeople">
                        <input type="text" class="invite_mail_id" id="name"name="name" placeholder="<?php echo trans('dashboard.invite_name')?>"></input>
  					<input type="text" class="invite_mail_id" id="email"name="email" placeholder="<?php echo trans('dashboard.invite_mail')?>"></input>
                         <input type="hidden" id="time" name="time"></input>
                        <button class="send_invite" type="submit" id="send_invite_btn"><?php echo trans('dashboard.invite_btn')?></button>
                    </form>
                    </div>
                </div>
  			</div> -->
  	  </div>
  			
  			

  </div>

  	
  	

  <script src="js/jquery.form.js"></script>
  <script>
  	(function() {
  	$('form').ajaxForm({
    		beforeSend: function () {
                     var name = $('#name').val();
                    var email = $('#email').val();
                    var trailStatus='<?php echo $data['trail_status'];?>';
                    var paymentStatus='<?php echo $data['payment_status'];?>';
                    var isValid = true;
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(name == null || email == null || name == '' || email == '')
                    {
                    	isValid = false;
                     swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.all_fields');?>", "error");
                   }
                    else if(regex.test(email) == false)
                    {
                    	isValid = false;
        				swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.check_email');?>", "error");
    				}
    				else if (paymentStatus=="pending") 
   					{
   						if (trailStatus=="expired")
   						 	{
   						 		isValid = false;
  							swal({  
                     				title: "<?php echo trans('popup.error_');?>", 
                     				text: "<?php echo trans('popup.expired_trail');?>",   
                     				type: "error",   
                     				confirmButtonText : "<?php echo trans('popup.go_to_plans');?>"
                    				},
                    				function(){
                         				window.location.href = '<?php echo url('plansandprice'); ?>';

                   					 });
   						 	}
   					}
   				else if (paymentStatus=="expired")  
   					{
   						isValid = false;
   								swal({  
                     				title: "<?php echo trans('popup.error_');?>", 
                     				text: "<?php echo trans('popup.expired_subscription');?>",   
                     				type: "error",   
                     				confirmButtonText : "<?php echo trans('popup.go_to_plans');?>"
                    				},
                    				function(){
                         				window.location.href = '<?php echo url('plansandprice'); ?>';

                   					 });
   					}
    	        if(isValid == false)
    	        {
        			return false;
      			}
  				},
    		uploadProgress: function() {
      
    			},
    		success: function(data) {

    			 if(data.status=='success')
                   {
                   	swal("<?php echo trans('popup.great');?>", "<?php echo trans('popup.invite_employee');?>", "success");
                   	$("#invite")[0].reset();
                   }
                  else if(data.status=='failure'){	
                  	swal("<?php echo trans('popup.oops');?>", "<?php echo trans('popup.already_registered');?>", "error");
                  }	
    			},		
    		complete: function() {
    
   				 }
  		}); 

  	})();	
  </script>
  <script>
  	$(".left-links li a").click(function(){
    	$(this).find('i').toggleClass('fa-indent fa-outdent');
  });
  </script>
  <!-- Imported styles on this page -->

  <?php  include 'layout/footer.php';?>


