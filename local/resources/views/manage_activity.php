

<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/manage_activity.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link href="<?php echo url('css/jquery.timeentry.css')?>" rel="stylesheet">
 <?php 
 $general_setting=$details['general_setting'];
 $separator=$details['separator'];
  $valueFormat='';
 $timeOfMot='';
 $separatorValue='';
 if (!empty($general_setting['response'])) {$valueFormat=$general_setting['response']['0']['value_format'];} 
 if (!empty($general_setting['response'])) {$timeOfMot=$general_setting['response']['0']['time_style'];} 
 if (!empty($separator['data'])) {$separatorValue=$separator['data'];}
function replaceToDot($valueFormat,$timeOfMot, $separatorValue,$str)
{
 if($timeOfMot=='am/pm')
	{
		if($valueFormat=='8.15')
		{
			 return $str;
		}
		else if($valueFormat=='8:15')
		{ 
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
else if($timeOfMot=='24 hours')
	{
		if($valueFormat=='8.15')
		{
			return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
else if ($timeOfMot=='Industrial')
	{
		if($valueFormat=='8.15')
		{
			return $str;
		}
		else if($valueFormat=='8:15')
		{
			return $str;
		}
		else if($valueFormat=="Device region")
		{
			if($separatorValue=="comma")
			{
				return str_replace('.',',',$str);
			}
			else
			{
				return $str;
			}
		}
	}
	else
	{
		return $str;
	}
}
 ?> 
  <?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
    }
</script>   
<style type="text/css">
.copy_btn {
	cursor: pointer !important;
    color: #2392ec !important;
}
#example-1{
 margin: 0px !important;
}
.tool_tip_p
{
    text-align: left;
    color: #fff;
}
.tooltip-inner {

}
.priority_btn{
	background: #2392ec;
    padding: 2px;
    border-radius: 3px;
    cursor: pointer;
}
.priority_btn i{
	padding: 2px !important;
}
</style>
<script type="text/javascript">
	var time1='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['time_style'];} ?>';
  var value_format='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['value_format'];} ?>';
  var separator='<?php if (!empty($separator['data'])) {echo $separator['data'];} ?>';
  var offset='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['set_working_hours'];} ?>';
  console.log(time1);
  console.log(value_format);
  console.log(separator);
  console.log(offset);
  var baseUrl="<?= url('');?>";
</script>
<div class="row mot_template">
				<div class="col-md-12 link">
				<div style="float:left;">
					<p> 
					<span class="template_link">
					<a href="<?php echo url('dashboard')?>"><?php echo trans('header.dashboard')?></a> /</span>
					<a ><?php echo trans('manage_activity.manage_activity')?></a>
					</p>
				</div>
		</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
				<ul class="nav nav-tabs">
						<li class="top_list active" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_tab'); ?></p> ">
							<a href="#home" class="top_anchor" data-toggle="tab" id="company_activity">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"><?php echo trans('manage_activity.company_activity')?></span>
							</a>
						</li>
						<i class="fa fa-star" aria-hidden="true" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('translate.tooltip_default_activity'); ?></p> "></i>
						<!-- <button class="add_button_class">gbgffggfgfg</button> -->
							<div class="float_button add_button_class" id="float_button">
							<i class="fa fa-info-circle info_icon" aria-hidden="true" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_add_activity'); ?></p> "></i>
							<button onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"  type="submit" class="circular_button"><i class="fa fa-pencil-square-o add_icon" aria-hidden="true" ></i>
							<p class="add_temp_text"><?php echo trans('manage_activity.add')?></p>
							<p class="add_temp_text"><?php echo trans('manage_activity.activity')?></p></button>
						</div>
						<!-- <li class="top_list">
							<a href="#profile" class="top_anchor" data-toggle="tab" id="mot_activity">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs"><?php echo trans('manage_activity.my_overtime_activity')?></span>
							</a>
						</li> -->
						<!-- <li class="top_list">
							<a href="#messages" class="top_anchor" data-toggle="tab">
								<span class="visible-xs"><i class="fa-envelope-o"></i></span>
								<span class="hidden-xs">Most Used Template</span>
							</a>
						</li> -->
					</ul>
					
					<div class="tab-content custom">
						<div class="tab-pane active" id="home">
							
							<div class="col-md-12 col-sm-12 col-xs-12 center">
								<div class="xe-widget xe-conversations temp_title">		
						<div class="xe-body basic_info_body">
							
		
	
			
			<!-- Basic Setup -->
			<div class="">
				
				<div class="panel-body">
					
					<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						var a = [10, 25, 50, 100, -1];
						var b = [10, 25, 50, 100, "All"];
						var table = $("#example-1").dataTable({
							 language: {
							 	
                        "url":languageUrl,
                        },aLengthMenu: [a, b], 
                        "iDisplayLength": 10,
                        'scrollX':true,
								columnDefs: [
								{ 
								orderable: false, targets:  "no-sort"}
								]	
						});
						table.on( 'page.dt', function ( e, settings, json ) {
						    console.log( 'Ajax event occurred. Returned data: ', json );
						} );

					});

					$('select[name="example-1_length"]').change(function() {
					  alert( "Handler for .change() called." );
					});
					</script>
					<?php
						if($details['activity_list']['status'] == 'success'){
							$tableDisplay = "display:block";
							$noItem = "display:none";
						}
						else{
							$tableDisplay = "display:none";
							$noItem = "display:block";
						}
					?>
					<style>
      th, td { white-space: nowrap; }
        div.container {
        width: 100%;
        }
         .dataTables_scrollBody::-webkit-scrollbar-track
{
    //-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    //border-radius: 10px;
    background-color: #D4D4D4;
}

.dataTables_scrollBody::-webkit-scrollbar
{
    height: 6px;
    background-color: #67B0E1;
}

.dataTables_scrollBody::-webkit-scrollbar-thumb
{
   
    background-color: #67B0E1;
}
.sort:after{
	display: none !important;
}
.no-sort:after{
	display: none !important;
}
.sort:before{
	position: relative !important;
	top: -15px !important;
	left: -15px !important;
}
.fa-star {
    color: #000;
    margin-left: 2px;
}
    </style>
					<div style="<?=$tableDisplay?>">
					<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%" >
						<thead>
							<tr>
								<th class="sort"><?php echo trans('popup.seq')?></th>
								<th class="no-sort"><?php echo trans('translate.activity')?></th>
								<th class="no-sort" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_tab'); ?></p> "><?php echo trans('translate.color')?>&nbsp;<i class="fa fa-pencil" aria-hidden="true"></i></th>
								<th class="no-sort"><?php echo trans('manage_activity.offset')?></th>
								<th class="no-sort"><?php echo trans('manage_activity.flat')?></th>
								<th class="no-sort"><?php echo trans('manage_activity.own')?></th>
								<th class="no-sort"><?php echo trans('manage_activity.amount')?></th>
								<th class="no-sort"><?php echo trans('manage_activity.amount_type')?></th>
								<th class="no-sort"><?php echo trans('manage_activity.rate')?></th>
								<th class="fixed_width no-sort" style="width:180px;"><?php echo trans('translate.deploy'); ?> Status</th>
								<th class="no-sort"><?php echo trans('translate.copy')?></th>
								<th class="no-sort"><button class="btn btn-info" style=" margin-bottom: 0px;" onclick="deleteSelectedActivity();"><?php echo trans('translate.delete')?></button></th>
							</tr>
						</thead>
					<tbody>

						<?php
							$activityListLength=count($details['activity_list']['response']);
							for($i=0;$i<count($details['activity_list']['response']);$i++){
								$deployDiv='';
								$asterisk='';
								($details['activity_list']['response'][$i]['flat_break_deduction'] == true) ? $flat_break_deduction = trans('translate.on') : $flat_break_deduction = trans('translate.off');
								($details['activity_list']['response'][$i]['own_account'] == true) ? $own_account = trans('translate.on') : $own_account = trans('translate.off');
								($details['activity_list']['response'][$i]['amount'] == true) ? $amount = trans('translate.on') : $amount = trans('translate.off');
								$activityId = '"'.$details['activity_list']['response'][$i]['_id'].'"';
								/*echo $details['activity_list']['response'][$i]['activity_color'];*/
								if ( $details['general_setting']['response'] != null )
								{
									if ($details['activity_list']['response'][$i]['_id'] == $details['general_setting']['response'][0]['general_setting_activity']) 
									{
										//echo $details['general_setting']['response'][0]['general_setting_activity'];
										$asterisk="<span><i class='fa fa-star' aria-hidden='true'></i></span>";
										 //|| || $details['activity_list']['response'][$i]['_id'] == $details['overtime_handling']['response'][0]['clear_overtime_balance'] || $details['activity_list']['response'][$i]['_id'] == $details['annual_allowance']['response'][0]['set_annual_allowance_activity']
									//data-toggle='tooltip'  data-original-title='<p class='tool_tip_p'>".$activityId ."</p> '
									}
									
								}
								
								if ( $details['overtime_handling']['response'] != null ) 
								{
									if ($details['activity_list']['response'][$i]['_id'] == $details['overtime_handling']['response'][0]['overtime_for_period'])
									{
										$asterisk="<span><i class='fa fa-star' aria-hidden='true'></i></span>";
									}
									

								}
								
								if ( $details['overtime_handling']['response'] != null ) 
								{
									if ($details['activity_list']['response'][$i]['_id'] == $details['overtime_handling']['response'][0]['clear_overtime_balance'])
									{
										$asterisk="<span><i class='fa fa-star' aria-hidden='true'></i></span>";
									}
									

								} 
								
								if ( $details['annual_allowance']['response'] != null ) 
								{
									if ($details['activity_list']['response'][$i]['_id'] == $details['annual_allowance']['response'][0]['set_annual_allowance_activity'])
									{
										$asterisk="<span><i class='fa fa-star' aria-hidden='true'></i></span>";
									}
									
								}	
						

								if ($details['activity_list']['response'][$i]['activity_color'] == null || $details['activity_list']['response'][$i]['activity_color'] == '') {
									$activity_color="#2392ec";
								}
								else
								{
									$activity_color=$details['activity_list']['response'][$i]['activity_color'];
								}

								if ($details['activity_list']['response'][$i]['deploy_status']=="pending") 
								{
									$deployDiv="<button class='deploy_btn' onclick ='changeStatusDeploy(".$activityId.");' >".trans('translate.deploy')."</button>
										<button class='delete_btn' onclick ='deleteActivity(".$activityId.");' >".trans('translate.delete')."</button>";
								}
								else
								{
									$deployDiv='<p style=" color: #2acd84; ">'.trans('translate.deployed').'</p>';
								}
								$count=$details['activity_list']['response'][$i]['against_offset'];
								$countTranslation="";
								if ($count=="Count") {
									$countTranslation=trans('translate.count');
								}
								elseif ($count=="Not Count") {
									$countTranslation=trans('translate.not_count');
								}
								else
								{
									$countTranslation='';
								}
								$amount_type=$details['activity_list']['response'][$i]['amount_type'];
								if ($details['activity_list']['response'][$i]['amount_type']=="Hourly") {
									$amount_type=trans('manage_activity.hourl');
								}
								elseif ($details['activity_list']['response'][$i]['amount_type']=="Lump") {
									$amount_type=trans('manage_activity.lump_sum');
								}
								else
								{
									$amount_type='--';
								}
								$activity_color_without = ltrim($activity_color, '#');
								$activity_colorDis = "#".$activity_color_without;

								echo "<tr>
								<td id='asterisk'><buttton class ='priority_btn' type='button' onclick =\"addPriority('".$details['activity_list']['response'][$i]['_id']."');\"><i class='fa fa-caret-up' aria-hidden='true'></i>".($i+1)."</button>".$asterisk."</td>
								<td onclick = 'viewActivity(".$activityId.")' style='color:#2392ec;cursor:pointer;width:123px;'>".$details['activity_list']['response'][$i]['activity_name']."</td>
								<td style='cursor:pointer;'><buttton type='button' onclick =\"editDeployedActivityColor('".$details['activity_list']['response'][$i]['_id']."','".$activity_color_without."');\"><div style='border:1px solid transparent; border-radius:50%; width:15px; height:15px;margin-left:auto; margin-right:auto;background:".$activity_colorDis."'><input type='hidden' id='activity_color".$details['activity_list']['response'][$i]['_id']."' name='activity_color' value=".$activity_color."></button></td>
								<td>".$countTranslation."</td>
								<td>".$flat_break_deduction."</td>
								<td>".$own_account."</td>
								<td>".$amount."</td>
								<td>".$amount_type."</td>
								<td>".replaceToDot($valueFormat,$timeOfMot, $separatorValue,$details['activity_list']['response'][$i]['hourly_rate_multiplier'])."</td>
								<td style='padding: 3px;    width: 170px;'>".$deployDiv."</td>
								<td style='text-align:center'><i class='fa fa-plus copy_btn' aria-hidden='true' onclick=\"copyActivity('".$details['activity_list']['response'][$i]['_id']."');\"></i></td>	
								<td style='text-align:center'><input type='checkbox' class='delete_activity cbr cbr-blue' value='delete_activity' id=".$details['activity_list']['response'][$i]['_id']."></td>
								</tr>";
							}
						?>							
						</tbody>
					</table>
				</div>
					<div class="col-md-12 error_msg" style="<?=$noItem?>"><p><?php echo trans('manage_activity.data_not_available')?></p></div>
					
				</div>
			</div>
			
			
	
				</div>
			</div>								
	
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
				</div>
			</div>
			<div class="modal fade" id="modal-6">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_activity.modal_title')?></h5>
				</div>
			<div class="modal-content custom-content">
				
				<div class="modal-body custom-body">
				<form method="post" action="addCompanyActivity">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'><li><?php echo trans('manage_activity.tooltip_activity_one');?></li> <li><?php echo trans('manage_activity.tooltip_activity_two');?></li> <li><?php echo trans('manage_activity.tooltip_activity_three');?></li></ul>"><?php echo trans('manage_activity.activity_name')?></label>
								
								<input type="text" class="form-control" id="activity_name" name="activity_name" placeholder="Activity name" maxlength="20">
							</div>	
							<div class="form-group float_left"> 
								<label for="field-10" class="control-label float_left" style="padding-right:15px;"><?php echo trans('translate.activity_color')?></label>
								<input  class="jscolor table-color" id="selected_activity_color112"  value="#2392ec" disabled style="background-color:#2392ec">
								<input type="hidden" name="activity_color" id="activity_color112" value="">
							</div>
							
						</div>
					</div>
						<div class="row">
						<div class="col-md-12 extra_margin">
							
							<div class="form-group">
									<label class="col-sm-4 control-label no_padding" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_activity.offset')?></label>
									
									<div class="col-sm-8 no_padding">
									<p>
											<label class="radio-inline" style=" display: inline; ">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_count"  value="Count" checked>
												<?php echo trans('manage_activity.offset_count')?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_not_count" value="Not Count">
												<?php echo trans('manage_activity.offset_nocount')?>
											</label>
									</p>
									</div>
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-7 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_flat'); ?></p> "><?php echo trans('manage_activity.flat')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_break_deduction" name="flat_break_deduction" id="flat_break_deduction" ></input>
							</div>	
							</div>
							<div class="col-sm-5 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_own'); ?></p> "><?php echo trans('manage_activity.own')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="own_account" id="own_account" ></input>
							</div>	
							</div>
							<div class="col-sm-4 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_amount'); ?></p> "><?php echo trans('manage_activity.amount')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info lump_hours" name="amount" id="lump_hours" >
							</div>	
							</div>
							<div class="col-sm-4 no_padding t_align">
								<button type="button" class="btn lab disabled" id="toggle_lab" style=" background: #2392ec; padding: 2px 10px 3px 10px; border-radius: 5px; "><?php echo trans('manage_activity.lump_sum')?></button>
								<input  type="hidden" class="input_class" id="amount_type" name="amount_type" value="Lump"></input>		
							</div>
							<div class="col-sm-4 no_padding t_align input-1">
								<input class="input_class" placeholder="99.99" value="0" id="amount_value" name="amount_value" style=" background: white; border: 1px solid #e6e6e6; padding: 0px; "></input>	
							</div>
						<!-- <div class="col-sm-4 no_padding t_align input-2 input-3">
								<input  type="text" class="input_class" placeholder="99.99" disabled></input>
									
							</div> -->
						</div>
						<div class="col-md-12 extra_margin lab_data">
							<p class="center_align">
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="hourly">
										<?php echo trans('manage_activity.hourl')?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="lump_sum">
										<?php echo trans('manage_activity.lump_sum')?>
								</label>
							</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
							<div class="col-md-12">
								<label for="field-4" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_allownce'); ?></p> "><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;
								
								<input type="text" value="0:00" class="form-control custom_box" id="activity_hour" name="activity_hour">&nbsp;&nbsp;
								<label for="field-5" class="control-label"><?php echo trans('manage_activity.hours')?></label>
								</div>
								<div class="col-md-12" style=" margin-bottom: 10px; ">
								<label for="field-4" class="control-label"><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;
								<input type="text" class="form-control custom_box" id="activity_day" name="activity_day" value="0"  readonly >&nbsp;&nbsp;
								<label for="field-5" class="control-label" ><?php echo trans('manage_activity.days')?></label>
								</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="form-group">
								<label for="field-6" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_hourly'); ?></p> "><?php echo trans('manage_activity.Hourly')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input  class="form-control custom_box hourly_rate_multiplier no_border" id="hourly_rate_multiplier" name="hourly_rate_multiplier" style="width: 65px;">

								</div>	
							</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group float_left">
								<label for="field-7" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_input'); ?></p> "><?php echo trans('manage_activity.flat_time_mode')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_time_mode" id="flat_time_mode" name="flat_time_mode">
							</div>	
							
						</div>
					</div>
					<div class="row ftm_data">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-8" class="col-sm-6 control-label no_padding"><?php echo trans('manage_activity.Pre_populated')?></label>
									
								<div class="col-sm-4 no_padding">
										
									<div class="form-block">
										<label>
											<input type="radio" name="pre_populated_with" class="cbr cbr-blue form-control" value="Offset" id="offset">
												<?php echo trans('manage_activity.off')?>
										</label>
										<br />
										<label>
											<input type="radio" name="pre_populated_with" class="cbr cbr-blue form-control" value="Hours"  id="hours" checked> 
												<?php echo trans('manage_activity.hours')?>
										</label>
									</div>
							</div>
							<div class="col-sm-2 no_padding" style=" top: 20px;">
								<input type="text" class="form-control custom_box" id="pre_populated" name="pre_populated_value">
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group float_left">
								<label for="field-9" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_reduce'); ?></p> "><?php echo trans('manage_activity.Overtime_Reducerd')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="overtime_reducer">
					</div>
					</div>	
				</div>
				<input type="hidden" id="time" name="time"></input>
				<div class="modal-footer custom-footer">
				<div id="addbtn"> 
					<button type="submit" class="btn btn-info" id="save_btn"><?php echo trans('manage_activity.save')?></button>
					</div>
					<div id="addloader" style="display:none">
						<img src="<?=url('image/hourglass.gif')?>" style="width:35px; height: 35px;"/>
					</div>
				</div>
			</div>
			</form>    
		</div>
	</div>
	</div>


<div class="modal fade" id="modal-61">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_activity.edit')?></h5>
				</div>
			<div class="modal-content custom-content">
				<form method="post" action="updateCompanyActivity" id="form2">
				<div class="modal-body custom-body" id="modal-body6">
										<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('manage_activity.tooltip_activity_one');?></li> <li><?php echo trans('manage_activity.tooltip_activity_two');?></li> <li><?php echo trans('manage_activity.tooltip_activity_three');?></li> </ul>"><?php echo trans('manage_activity.activity_name')?></label>
								
								<input type="text" class="form-control" id="activity_name1" name="activity_name" placeholder="Peter" maxlength="20">
							</div>
							<div class="form-group float_left">
								<label for="field-10" class="control-label float_left" style="padding-right:15px;"><?php echo trans('translate.activity_color')?></label>
								
								<input  class="jscolor activity_color_class" onchange="activitycolor1(this.jscolor)" name="activity_color" value="" readonly>
								<input type="hidden" id="activity_color1">
							</div>	
							
						</div>
					</div>
						<div class="row">
						<div class="col-md-12 extra_margin">
							
							<div class="form-group">
									<label class="col-sm-4 control-label no_padding" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_activity.offset')?></label>
									
									<div class="col-sm-8 no_padding">
									<p>
											<label class="radio-inline" style=" display: inline; ">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control " id="against_offset_count1"  value="Count" >
												<?php echo trans('manage_activity.offset_count')?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_not_count1" value="Not Count">
											<?php echo trans('manage_activity.offset_nocount')?>
											</label>
									</p>
									</div>
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-7 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_flat'); ?></p> "><?php echo trans('manage_activity.flat')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_break_deduction" name="flat_break_deduction" id="flat_break_deduction1" ></input>
							</div>	
							</div>
							<div class="col-sm-5 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_own'); ?></p> "><?php echo trans('manage_activity.own')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="own_account" id="own_account1" ></input>
							</div>	
							</div>
							<div class="col-sm-4 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_amount'); ?></p> "><?php echo trans('manage_activity.amount')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" id="lump_hours1" name="amount">
							</div>	
							</div>
							<div class="col-sm-4 no_padding t_align">
								<button type="button" class="btn lab disabled" id="toggle_lab1" style=" background: #2392ec; padding: 2px 10px 3px 10px; border-radius: 5px; "></button>
								<input  type="hidden" class="input_class" id="amount_type1" name="amount_type" value="Lump"></input>		
							</div>
							<div class="col-sm-4 no_padding t_align input-1">
								<input class="input_class" placeholder="99.99" id="amount_value1" name="amount_value" style=" background: white; border: 1px solid #e6e6e6; padding: 0px; "></input>	
							</div>
<!-- 							<div class="col-sm-4 no_padding t_align input-2 input-3">
								<input  type="text" class="input_class" placeholder="99.99" disabled></input>
									
							</div> -->
						</div>
						<div class="col-md-12 extra_margin lab_data">
							<p class="center_align">
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="hourly1">
										<?php echo trans('manage_activity.hourl')?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="lump_sum1">
										<?php echo trans('manage_activity.lump_sum')?>
								</label>
							</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
							<div class="col-md-12">
								<label for="field-4" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_allownce'); ?></p> "><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input type="text" class="form-control custom_box" id="activity_hour1" name="activity_hour">&nbsp;
								<label for="field-5" class="control-label"><?php echo trans('manage_activity.hours')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="col-md-12" style="margin-bottom: 10px">
								<label for="field-4" class="control-label"><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="form-control custom_box" id="activity_day1" name="activity_day" readonly >&nbsp;
								<label for="field-5" class="control-label" ><?php echo trans('manage_activity.days')?></label>
								</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="form-group">
								<label for="field-6" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_hourly'); ?></p> "><?php echo trans('manage_activity.Hourly')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input class="form-control custom_box no_border" id="hourly_rate_multiplier1" name="hourly_rate_multiplier">

								</div>	
							</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group float_left">
								<label for="field-7" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_input'); ?></p> "><?php echo trans('manage_activity.flat_time_mode')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_time_mode" id="flat_time_mode1" name="flat_time_mode">
							</div>	
							
						</div>
					</div>
					<div class="row ftm_data" id="ftm_data1">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-8" class="col-sm-6 control-label no_padding"><?php echo trans('manage_activity.Pre_populated')?></label>
									
								<div class="col-sm-4 no_padding">
										
									<div class="form-block">
										<label>
											<input type="radio" id="offset1" name="pre_populated_with" class="cbr cbr-blue form-control" value="Offset">
												<?php echo trans('manage_activity.off')?>
										</label>
										<br />
										<label>
											<input type="radio" id="hours1" name="pre_populated_with" class="cbr cbr-blue form-control" value="Hours" checked> 
												<?php echo trans('manage_activity.hours')?>
										</label>
									</div>
							</div>
							<div class="col-sm-2 no_padding" style=" top: 20px;">
								<input type="test" class="form-control custom_box" id="pre_populated1" name="pre_populated_value">
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group float_left">
								<label for="field-9" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_reduce'); ?></p> "><?php echo trans('manage_activity.Overtime_Reducerd')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="overtime_reducer" id="overtime_reducer1">
					</div>
					</div>	
				</div>
	<!-- 			<input type="hidden" id="time" name="time"></input> -->


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->


	<input type="hidden" id="activity_id" name="activity_id" value=""></input>
				<div class="modal-footer custom-footer">
					<div id="addbtn1"> 
					<button type="submit" class="btn btn-info" id="save_btn"><?php echo trans('manage_activity.save')?></button>
					</div>
					<div id="addloader1" style="display:none">
						<img src="<?=url('image/hourglass.gif')?>" style="width:35px; height: 35px;"/>
					</div>
				</div>
			</div>
		     </div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-6111">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title">Copy</h5>
				</div>
			<div class="modal-content custom-content">
				<form method="post" action="addCompanyActivity" id="form3">
				<div class="modal-body custom-body" id="modal-body611">
										<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('manage_activity.tooltip_activity_one');?></li> <li><?php echo trans('manage_activity.tooltip_activity_two');?></li> <li><?php echo trans('manage_activity.tooltip_activity_three');?></li> </ul>"><?php echo trans('manage_activity.activity_name')?></label>
								
								<input type="text" class="form-control" id="activity_name111" name="activity_name" placeholder="Peter" maxlength="20">
							</div>	
							<div class="form-group float_left">
								<label for="field-10" class="control-label float_left" style="padding-right:15px;"><?php echo trans('translate.activity_color')?></label>
								
								<!-- <input class="jscolor" id="activity_color111" name="activity_color"> -->
								<input  class="jscolor" onchange="activity_color111(this.jscolor)" name="activity_color" disabled>
								<input type="hidden" id="activity_color111">
							</div>
							
						</div>
					</div>
						<div class="row">
						<div class="col-md-12 extra_margin">
							
							<div class="form-group">
									<label class="col-sm-4 control-label no_padding" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_activity.offset')?></label>
									
									<div class="col-sm-8 no_padding">
									<p>
											<label class="radio-inline" style=" display: inline; ">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control " id="against_offset_count111"  value="Count" >
												<?php echo trans('manage_activity.offset_count')?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_not_count111" value="Not Count">
											<?php echo trans('manage_activity.offset_nocount')?>
											</label>
									</p>
									</div>
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-7 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_flat'); ?></p> "><?php echo trans('manage_activity.flat')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_break_deduction" name="flat_break_deduction" id="flat_break_deduction111" ></input>
							</div>	
							</div>
							<div class="col-sm-5 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_own'); ?></p> "><?php echo trans('manage_activity.own')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="own_account" id="own_account111" ></input>
							</div>	
							</div>
							<div class="col-sm-4 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_amount'); ?></p> "><?php echo trans('manage_activity.amount')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" id="lump_hours111" name="amount">
							</div>	
							</div>
							<div class="col-sm-4 no_padding t_align">
								<button type="button" class="btn lab disabled" id="toggle_lab111" style=" background: #2392ec; padding: 2px 10px 3px 10px; border-radius: 5px; "></button>
								<input  type="hidden" class="input_class" id="amount_type111" name="amount_type" value="Lump"></input>		
							</div>
							<div class="col-sm-4 no_padding t_align input-1">
								<input class="input_class" placeholder="99.99" id="amount_value111" name="amount_value" style=" background: white; border: 1px solid #e6e6e6; padding: 0px; "></input>	
							</div>
<!-- 							<div class="col-sm-4 no_padding t_align input-2 input-3">
								<input  type="text" class="input_class" placeholder="99.99" disabled></input>
									
							</div> -->
						</div>
						<div class="col-md-12 extra_margin lab_data">
							<p class="center_align">
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="hourly111">
										<?php echo trans('manage_activity.hourl')?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="lump_sum111">
										<?php echo trans('manage_activity.lump_sum')?>
								</label>
							</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
							<div class="col-md-12">
								<label for="field-4" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_allownce'); ?></p> "><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input type="text" class="form-control custom_box" id="activity_hour111" name="activity_hour">&nbsp;
								<label for="field-5" class="control-label"><?php echo trans('manage_activity.hours')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="col-md-12" style="margin-bottom: 10px">
								<label for="field-4" class="control-label"><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="form-control custom_box" id="activity_day111" name="activity_day" readonly >&nbsp;
								<label for="field-5" class="control-label" ><?php echo trans('manage_activity.days')?></label>
								</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="form-group">
								<label for="field-6" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_hourly'); ?></p> "><?php echo trans('manage_activity.Hourly')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input class="form-control custom_box no_border" id="hourly_rate_multiplier111" name="hourly_rate_multiplier">

								</div>	
							</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group float_left">
								<label for="field-7" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_input'); ?></p> "><?php echo trans('manage_activity.flat_time_mode')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info flat_time_mode" id="flat_time_mode111" name="flat_time_mode">
							</div>	
							
						</div>
					</div>
					<div class="row ftm_data" id="ftm_data111">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-8" class="col-sm-6 control-label no_padding"><?php echo trans('manage_activity.Pre_populated')?></label>
									
								<div class="col-sm-4 no_padding">
										
									<div class="form-block">
										<label>
											<input type="radio" id="offset111" name="pre_populated_with" class="cbr cbr-blue form-control" value="Offset">
												<?php echo trans('manage_activity.off')?>
										</label>
										<br />
										<label>
											<input type="radio" id="hours111" name="pre_populated_with" class="cbr cbr-blue form-control" value="Hours" checked> 
												<?php echo trans('manage_activity.hours')?>
										</label>
									</div>
							</div>
							<div class="col-sm-2 no_padding" style=" top: 20px;">
								<input type="test" class="form-control custom_box" id="pre_populated111" name="pre_populated_value">
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group float_left">
								<label for="field-9" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_reduce'); ?></p> "><?php echo trans('manage_activity.Overtime_Reducerd')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="overtime_reducer" id="overtime_reducer111">
					</div>
					</div>	
				</div>
	<!-- 			<input type="hidden" id="time" name="time"></input> -->


<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->


	<input type="hidden" id="activity_id" name="activity_id" value=""></input>
				<div class="modal-footer custom-footer">
					<div id="addbtn11"> 
					<button type="submit" class="btn btn-info" id="save_btn"><?php echo trans('manage_activity.save')?></button>
					</div>
					<div id="addloader11" style="display:none">
						<img src="<?=url('image/hourglass.gif')?>" style="width:35px; height: 35px;"/>
					</div>
				</div>
			</div>
		     </div>
				</form>
			</div>
		</div>
	<div class="modal fade" id="modal-611">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" id="modal-611-close" class="close" data-dismiss="modal" aria-hidden="true" style="text-shadow: none;opacity: 0.5;outline: none;">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('manage_activity.view_activity')?></h5>
				</div>
			<div class="modal-content custom-content">
				<div class="modal-body custom-body" id="modal-body61">
										<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-1" class="control-label"data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('manage_activity.tooltip_activity_one');?></li> <li><?php echo trans('manage_activity.tooltip_activity_two');?></li> <li><?php echo trans('manage_activity.tooltip_activity_three');?></li> </ul>"><?php echo trans('manage_activity.activity_name')?></label>
								
								<input type="text" class="form-control" id="activity_name11" name="activity_name" placeholder="Peter" style=" background: #fff; " readonly>
							</div>	
							<div class="form-group float_left">
								<label for="field-10" class="control-label float_left" style="padding-right:15px;"><?php echo trans('translate.activity_color')?></label>
								
								<input  id="activity_color11" name="activity_color" disabled>
							</div>
							
						</div>
					</div>
						<div class="row">
						<div class="col-md-12 extra_margin">
							
							<div class="form-group">
									<label class="col-sm-4 control-label no_padding" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_count'); ?></p> "><?php echo trans('manage_activity.offset')?></label>
									
									<div class="col-sm-8 no_padding">
									<p>
											<label class="radio-inline" id="against_offset_count1111" style=" display: inline; ">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control " id="against_offset_count11"  value="Count" >
												<?php echo trans('manage_activity.offset_count')?>
											</label>
											<label class="radio-inline" id="against_offset_not_count1111">
												<input type="radio" name="against_offset" class="cbr cbr-blue form-control" id="against_offset_not_count11" value="Not Count" >
												<?php echo trans('manage_activity.offset_nocount')?>
											</label>
									</p>
									</div>
							</div>	
						
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="col-sm-7 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_flat'); ?></p> "><?php echo trans('manage_activity.flat')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="flat_break_deduction" id="flat_break_deduction11" disabled></input>
							</div>	
							</div>
							<div class="col-sm-5 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_own'); ?></p> "><?php echo trans('manage_activity.own')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="own_account" id="own_account11" disabled></input>
							</div>	
							</div>
							<div class="col-sm-4 no_padding">
							<div class="form-group float_left">
								<label for="field-3" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_amount'); ?></p> "><?php echo trans('manage_activity.amount')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" id="lump_hour" disabled>
							</div>	
							</div>
							<div class="col-sm-4 no_padding t_align">
								<button type="button" class="btn lab disabled" id="toggle_lab11" style=" background: #2392ec; padding: 2px 10px 3px 10px; border-radius: 5px; "></button>
								<input  type="hidden" class="input_class" id="amount_type11" name="amount_type1" value="Lump" readonly></input>		
							</div>
							<div class="col-sm-4 no_padding t_align input-1">
								<input  type="text" class="input_class" step="0.01" placeholder="99.99" id="amount_value11" name="amount_value" style=" background: white; border: 1px solid #e6e6e6; padding: 0px; " readonly></input>	
							</div>
<!-- 							<div class="col-sm-4 no_padding t_align input-2 input-3">
								<input  type="text" class="input_class" placeholder="99.99" disabled></input>
									
							</div> -->
						</div>
						<div class="col-md-12 extra_margin lab_data">
							<p class="center_align">
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="hourly11" readonly>
										<?php echo trans('manage_activity.hourl')?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="amount_value1" class="cbr cbr-blue form-control" id="lump_sum11" readonly>
										<?php echo trans('manage_activity.lump_sum')?>
								</label>
							</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
							<div class="col-md-12">
								<label for="field-4" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_allownce'); ?></p> "><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input type="text" class="form-control custom_box" id="activity_hour11" name="activity_hour" style=" background: #fff; " readonly>&nbsp;
								<label for="field-5" class="control-label"><?php echo trans('manage_activity.hours')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="col-md-12" style="margin-bottom: 10px;">
								<label for="field-4" class="control-label"><?php echo trans('manage_activity.Allowance')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="form-control custom_box" id="activity_day11" name="activity_day1"  readonly >&nbsp;
								<label for="field-5" class="control-label" ><?php echo trans('manage_activity.days')?></label>
								</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="form-group">
								<label for="field-6" class="control-label" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_hourly'); ?></p> "><?php echo trans('manage_activity.Hourly')?></label>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<input type="text" class="form-control custom_box" id="hourly_rate_multiplier11" name="hourly_rate_multiplier" style=" background: #fff; " readonly>

								</div>	
							</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group float_left">
								<label for="field-7" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_input'); ?></p> "><?php echo trans('manage_activity.flat_time_mode')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info " id="flat_time_mode11" name="flat_time_mode" disabled>
							</div>	
							
						</div>
					</div>
					<div class="row ftm_data" id="ftm_data2">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-8" class="col-sm-6 control-label no_padding"><?php echo trans('manage_activity.Pre_populated')?></label>
									
								<div class="col-sm-4 no_padding">
										
									<div class="form-block">
										<label class="offset11">
											<input type="radio" id="offset11" name="pre_populated_with" class="cbr cbr-blue form-control" value="Offset">
												<?php echo trans('manage_activity.off')?>
										</label>
										<br />
										<label class="hours11">
											<input type="radio" id="hours11" name="pre_populated_with" class="cbr cbr-blue form-control" value="Hours" > 
												<?php echo trans('manage_activity.hours')?>
										</label>
									</div>
							</div>
							<div class="col-sm-2 no_padding hours11" >
								<input type="text" class="form-control custom_box" id="pre_populated1111" name="pre_populated_value" style=" background: #fff; " readonly>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group float_left">
								<label for="field-9" class="control-label float_left" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('manage_activity.tooltip_reduce'); ?></p> "><?php echo trans('manage_activity.Overtime_Reducerd')?></label>
								
								<input type="checkbox" class="iswitch iswitch-info" name="overtime_reducer" id="overtime_reducer11" disabled>
					</div>
					</div>	
				</div>
				<div class="modal-footer custom-footer" id="btned">
					<!-- <button class="btn btn-info" id="viewactivity" >Edit</button>
					<button class="btn btn-info" id="viewactivity1">Delete</button> -->
				</div>
			</div>
		     </div>
			</div>
		</div>
	</div>




	<div class="modal fade" id="modal-color">
		<div class="modal-dialog custom-dialog">
			<div class="modal-header custom-header">
					<button type="button" id="modal-2-close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title custom-title"><?php echo trans('translate.edit_color')?></h5>
				</div>
			<div class="modal-content custom-content">
				
				<div class="modal-body custom-body">
					<div class="row">
						<div class="col-md-12">	
							<div class="form-group float_left">
								<label for="field-10" class="control-label float_left" style="padding-right:15px;"><?php echo trans('translate.activity_color')?></label>
								
								<!-- <input   class="jscolor" id="activity_color21" name="activity_color"> -->
								<input  class="jscolor table-color" value="" id="activity_color_updated" name="activity_color" >
								<!-- <input  class="jscolor table-color" value="" onchange="activity_color21(this.jscolor)" name="activity_color" disabled> -->
								<input type="hidden" id="activity_color21">
							</div>
							
						</div>
					</div>
						
				<input type="hidden" id="activity_id" name="activity_id" value=""></input>
				<div class="modal-footer custom-footer">
					<button type="button" id="addbtn2" class="btn btn-info" id="save_btn"><?php echo trans('manage_activity.save')?></button>
					</div>
			</div>   
		</div>
	</div>
	</div>

<script src="<?php echo url('js/jquery.form.js')?>"></script>
<script src="<?php echo url('js/jquery.plugin.min.js')?>"></script>
<script src="<?php echo url('js/jquery.timeentry.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<script src="<?php echo url('js/jscolor.js')?>"></script>
<script>
	var companyId = "<?php echo Auth::user()['_id']?>";
	var activityListLength = "<?php echo $activityListLength;?>";
		console.log({activity:activityListLength})
		activityListLength = Number(activityListLength);
	var you_sure = "<?php echo trans('popup.you_sure');?>";
	var recover_activity = "<?php echo trans('popup.recover_activity');?>";
	var delete_it = "<?php echo trans('popup.delete_it');?>";
	var deleted_successfully = "<?php echo trans('popup.deleted_successfully');?>";
	var error = "<?php echo trans('popup.error');?>";
	var all_fields = "<?php echo trans('popup.all_fields');?>";
	var added_successfully = "<?php echo trans('popup.added_successfully');?>";
	var activity_exists = "<?php echo trans('popup.activity_exists');?>";
	var updated_successfully = "<?php echo trans('popup.updated_successfully');?>";
	var reached_limit = "<?php echo trans('popup.reached_limit');?>";
	var limited_space = "<?php echo trans('popup.limited_space');?>";
	var deploy_it = "<?php echo trans('popup.deploy_it');?>";
	var deployed_successfully = "<?php echo trans('popup.deployed_successfully');?>";
	var success = "<?php echo trans('popup.success');?>";
	var error_="<?php echo trans('popup.error_');?>!";
	var deleteTranslation="<?php echo trans('translate.delete')?>";
	var editTranslation="<?php echo trans('translate.edit')?>";
	var cancel="<?php echo trans('popup.cancel');?>";
	var lump="<?php echo trans('translate.lump')?>";
	var hourly="<?php echo trans('translate.hourly')?>";
	var cannot_delete="<?php echo trans('popup.cannot_delete');?>!";
	var priority_of_activity="<?php echo trans('popup.priority_of_activity');?>";
	var priority_updated="<?php echo trans('popup.priority_updated');?>";
	var number_not_zero="<?php echo trans('popup.number_not_zero');?>";
	var number_more_than_activity="<?php echo trans('popup.number_more_than_activity');?>";
	var submit_button="<?php echo trans('popup.submit_button');?>";
	var select_delete="<?php echo trans('popup.select_delete');?>";
	var cancel="<?php echo trans('popup.cancel');?>";
</script>
<!-- <script type="text/javascript" src="<?= url('js/manage_activity.js')?>"></script> -->
<script src="<?php echo url('js/manage_activity.js')?>"></script>

<script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
<?php include 'layout/footer.php';?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.1/sweetalert2.all.min.js"></script>
<script src="<?php echo url('js/globalize/globalize.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.culture.de-DE.js')?>"></script>
<script src="<?php echo url('js/jquery.mousewheel.js')?>"></script>
<!-- <script type="text/javascript">
	$( function() {
	  Globalize.culture('de');
	  $('#spinner').html($('#spinner').val("1,000"));
  $("#spinner").spinner({
        min: 0.000,
        max: 2.000,
        step: 0.001,
        numberFormat: "N3",
    });
});
</script> -->
<!-- Imported styles on this page -->
	 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>

	
	<script src="<?= url('assets/js/datatables/js/jquery.dataTables.min.js')?>"></script>


	<!-- Imported scripts on this page -->
	<script src="<?= url('assets/js/datatables/dataTables.bootstrap.js')?>"></script>
	<script src="<?= url('assets/js/datatables/yadcf/jquery.dataTables.yadcf.js')?>"></script>
	<script src="<?= url('assets/js/datatables/tabletools/dataTables.tableTools.min.js')?>"></script>	

	<!-- <script type="text/javascript">
		function editDeployedActivityColor(id,color)
			{
				// console.log(id,color);
				$('#modal-2-close').click();
				$('#modal-color').modal('show', {backdrop: 'static'});
				console.log(id);
				console.log(color);

				var colour= $("#activity_color"+id).val();
				console.log({colour:colour});
				$('#activity_color21').val($('#activity_color21').html() + "#"+color);
				$('.table-color').val($('.table-color').html() + "#"+color);
				$('.table-color').css('background-color', "#"+color);
				$('#activity_id').val($('#activity_id').html() + id);

				
			}
	</script> -->