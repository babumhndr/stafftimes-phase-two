<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/employer.css')?>">
<link rel="stylesheet" href="<?php echo url('css/selectize.css')?>">
<style type="text/css">
/*#profile-image {
    cursor: pointer;
}*/
/*.profile_picture1:hover{
	background: url('http://localhost/MyOvertime/image/upload.png')no-repeat;
    background-size: 27% 30%;
    background-position: 50%;
    width: 100%;
    height: 100%;
    float: left;
}*/
a:focus {
	text-decoration: none;
}
button:focus{
	outline: none;
}
.form-control:focus{
	border-color:#e4e4e4; 
}
.upload_cancel{
	position: absolute;
	top: 43%;
	width: 100%;
    /*padding: 0px 13px;*/
}
.loader_div
{
	display: none;
	position: absolute;
	top: 43%;
	width: 100%;
	text-align: center;
}
.loader_div img
{
	width: 50px;
	height: 50px;
	border-radius: 14px;
	background: #ffffff;
}
.cancelphoto{
	display:none;
}
.uploadphoto{
	display:none;
}
.btn {
  -webkit-border-radius: 60;
  -moz-border-radius: 60;
  border-radius: 60px;
  color: #ffffff;
  text-decoration: none;
  border:none;

}
.btn:hover {
  text-decoration: none;
}
.camera_icon{
	    position: absolute;
    font-size: 30px;
    color: #565656;
    top: -13px;
    right: 3px;
    cursor: pointer;
    display: none;	
}
#profile_picture:hover .camera_icon{
	display: block;
	}
.profile_camera{
	position: relative;
}
.selectize-control {
    padding: 0px !important;
    width: 100% !important;
    border: none !important;
}
.selectize-control.single .selectize-input
{
	    padding: 10px !important;
    box-shadow: none !important;
    border-color: #e5e5e5 !important;
    border-radius: 1px !important;
}
.selectize-dropdown-content
{
	background: #fff;
	border-top:none;
	border: 1px solid #e5e5e5 ;
}
.selectize-input > input
    {
        font-size: 16px !important;
        text-align: center !important;
    }
  .cursor_not_allowed
  {
  	 cursor: not-allowed;
    color: #b6b6b6;
  }
</style>

			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row employer">
				<div class="col-md-12 link">
					<p> 
					<a href="#"><?php echo trans('header.dashboard')?></a>&nbsp
					<span class="template_link">>
					<a href="#">&nbsp<?php echo trans('employer.Employer')?></a>
					</span> 
					</p>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
				<div class="fill">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 profile_picture" id="profile_picture">
					<div class="company_logo_heading"><?php echo trans('employer.company_logo')?></div>
					<form id="image_form">
					
					<input id="profile-image-upload" name="image" class="hidden"  type="file" >
					</form>
					<label class="profile_camera" for="profile-image-upload">								  
								    <span class="camera_icon hvr-pop">
									 <i class="fa fa-camera"></i> </span> 
									</label>
					<div class="image " id="profile-image" style="background: url('<?php if (!empty($data[0]['profile_image'])){echo $data[0]['profile_image'];}else{echo url("image/".trans('translate.lang').".jpg");} ?>') no-repeat center #fff;">

					<img src="" class="mot_before_upload" id="profile_img" style=" width: 100%; height: 100%; ">
					
					</div>
					<div>
                     <div class="upload_cancel">             
                     <button class="uploadphoto show_btn btn-primary btn" id="upload-profile-img" style="background: #2392ec;margin: 0px; margin-left: 2px;" type="button"><?php echo trans('employer.upload')?></button>
                     <button  class="cancelphoto show_btn btn-danger btn" type="button" style="float: right;margin-right: 2px;"><?php echo trans('employer.cancel')?></button>
                     </div>
                     <div class="loader_div">
                     	<img src="<?php echo url('image/hourglass.gif')?>" />
                     </div>
                     </div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 description">
						<div class="xe-widget xe-conversations subscription_information">		
							<div class="xe-label subscription_info grey">
								<p class="para bold_heading"><?php echo trans('employer.Subscription')?></p>	
							</div>
							<?php 
							if (!empty($data[0])) {
							if ($data[0]['payment_status']=="active") {
							$noOfEmployeeSubscribed=0;
							if ($data[3]!='') {
							$noOfEmployeeSubscribed=$data[3];
							}
							$subscription_start_date='';
							$subscription_end_date='';
							if (!empty($data[2])) {
							$subscription_start_date=$data[2][0][0]['subscription_start_date'];
							$subscription_start_date = date("d-m-Y", strtotime($subscription_start_date));
							$subscription_end_date=$data[2][0][0]['subscription_end_date'];
							$subscription_end_date = date("d-m-Y", strtotime($subscription_end_date));
							}
							$div= '<div class="col-md-6 col-sm-12 xe-body subscription_info_body">
							<div class="col-md-12 col-xs-12 subscription_info_list">
							<div class="subscription_info_title">
							<p class="para light">'.trans('translate.subscription').'</p>
							</div>
							<div class="subscription_info_value">
							<a href="#">
							<p class="para name" style="text-transform: capitalize;">'.$data[0]['payment_status'].'</p>
							</a>
							</div>	
							</div>
							<div class="col-md-12 col-xs-12 subscription_info_list">
							<div class="subscription_info_title">
							<p class="para light">'.trans('translate.no_of_employees_subscribed').'</p>
							</div>
							<div class="subscription_info_value">
							<a href="#">
							<p class="para name">'.$noOfEmployeeSubscribed.'</p>
							</a>
							</div>	
							</div>
							</div>
							<div class="col-md-6 col-sm-12 xe-body subscription_info_body">
							<div class="col-md-12 col-xs-12 subscription_info_list">
							<div class="subscription_info_title">
							<p class="para light">'.trans('translate.subscription_period').'</p>
							</div>
							<div class="subscription_info_value">
							<a href="#">
							<p class="para name">'.$subscription_start_date.' '.trans('translate.to').' '.$subscription_end_date.'</p>
							</a>
							</div>	
							</div>
							<div class="col-md-12 col-xs-12 subscription_info_list">
							<div class="subscription_info_title">
							<p class="para light">'.trans('translate.subscription_expiring').'</p>
							</div>
							<div class="subscription_info_value">
							<a href="#">
							<p class="para name">'.$subscription_end_date.'</p>
							</a>
							</div>	
							</div>
							</div>';
							echo $div;
							}
							else if ($data[0]['trail_status']=="expired") {
								$div1='<div style=" width: 100%; float: left; padding: 16px; "><div class="col-md-12 col-sm-12 xe-body subscription_info_body">
							<p style="margin: 10px;font-size: 20px;text-align: center;">'.trans('popup.expired_trail').'</p>
							</div>
							<div class="col-md-12 col-sm-12 xe-body subscription_info_body">
							<p style="margin: 10px;font-size: 20px;text-align: center;">'.trans('translate.For_Subscription').' <a href='.url('plansandprice').'>'.trans('translate.click_here').' </a></p>
							</div></div>';
							echo $div1;
							}
							else if ($data[0]['payment_status']=="expired") {
								$div1='<div style=" width: 100%; float: left; padding: 16px; "><div class="col-md-12 col-sm-12 xe-body subscription_info_body">
							<p style="margin: 10px;font-size: 20px;text-align: center;">'.trans('popup.expired_subscription').'</p>
							</div>
							<div class="col-md-12 col-sm-12 xe-body subscription_info_body">
							<p style="margin: 10px;font-size: 20px;text-align: center;">'.trans('translate.For_Subscription').' <a href='.url('plansandprice').'>'.trans('translate.click_here').' </a></p>
							</div></div>';
							echo $div1;
							}
							else
							{
							$div1='<div style=" width: 100%; float: left; padding: 16px; "><div class="col-md-12 col-sm-12 xe-body subscription_info_body">
							<p style="margin: 10px;font-size: 20px;text-align: center;">'.trans('translate.You_are_under_Free_trial').'</p>
							</div>
							<div class="col-md-12 col-sm-12 xe-body subscription_info_body">
							<p style="margin: 10px;font-size: 20px;text-align: center;">'.trans('translate.For_Subscription').' <a href='.url('plansandprice').'>'.trans('translate.click_here').' </a></p>
							</div></div>';
							echo $div1;
							}
						}
							?>
						</div> 
					</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 show_animate employee_whole_left">
					<div class="border">
						<div class="xe-widget xe-conversations basic_information">
							<div class="basic_edit grey">		
							<div class="xe-label basic_info">
								<p class="para bold_heading"><?php echo trans('employer.Basic')?></p>
							</div>
							<div class="edit_profile">
								<a href="#">
								<i class="fa fa-pencil fo" aria-hidden="true"></i>
								</a>
							</div>
							</div>
						</div>
						<div class="col-md-12 xe-body basic_info_body">
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.companyname')?></p>

								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name"><?php echo $data[0]['company_name'];?></p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.companyid')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name"><?php echo $data[0]['company_unique_id'];?></p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.email')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name"><?php echo $data[0]['email'];?></p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.phone')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$phone=$data[0]['phone'];
										if (!empty($phone))
											{
												echo $phone;
											}
											else{
												echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.country')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$country=$data[0]['country'];
										if (!empty($country))
											{
												echo trans('country_translation.'.$country);
											}
											else{
												echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
							<div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.language')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$language=$data[0]['language_name'];
										if (!empty($language))
											{
												echo $language;
											}
											else{
												echo "--";
												}
										?>
									</p>
									</a>
								</div>	
							</div>
							<!-- <div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.fax')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$fax=$data[0]['fax'];
										if (!empty($fax))
											{
												echo $fax;
											}
											else{
												echo "--";
												}
										?></p>
									</a>
								</div>	
							</div> -->
							<!-- <div class="basic_info_list">
								<div class="basic_info_title">
									<p class="para light"><?php echo trans('employer.skype')?></p>
								</div>
								<div class="basic_info_value">
									<a href="#">
									<p class="para name">
									<?php 
										$skype=$data[0]['skype'];
										if (!empty($skype))
											{
												echo $skype;
											}
											else{
												echo "--";
												}
										?></p>
									</a>
								</div>	
							</div> -->
						</div>
						</div>
					</div>
						<div class="col-md-12 col-sm-12 col-xs-12 show_animate employee_whole_edit">
						<div class="border">
							<div class="xe-widget xe-conversations basic_information_new">
								<div class="basic_edit grey">		
									<div class="xe-label basic_info">
										<p class="para bold_heading"><?php echo trans('employer.editBasic')?></p>
									</div>
									<div class="close_edit_profile">
										<a href="#">
										<i class="fa fa-times close" aria-hidden="true"></i>
										</a>
									</div>
								<a href="#" onclick="jQuery('#password_reset').modal('show', {backdrop: 'static'});" style=" float: right; margin-top: 11px; margin-right: 20px;color: #2392ec; text-decoration: underline;"><?php echo trans('employer.change_password')?></a>
								</div>
							</div>
								<form method="post" action="<?php echo url('companyUpdate')?>">
									<div class="col-md-12 col-sm-12 col-xs-12 xe-body basic_info_body new">
										<div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"><?php echo trans('employer.companyname')?></p>
											<p class="edit_input">
												<input type="text" id="company_name"class="comp_name"name="companyname" value="<?php echo $data[0]['company_name'];?>" placeholder="<?php echo trans('employer.placeholder_name')?>"></input>
											</p>
										</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"> <?php echo trans('employer.companyid')?></p>
											<p class="edit_input">
												<input type="text" class="comp_name cursor_not_allowed" name="companyid" readonly value="<?php echo $data[0]['company_unique_id'];?>" placeholder="<?php echo trans('employer.placeholder_id')?>"></input>
										</p>
										</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"><?php echo trans('employer.email')?></p>
											<p class="edit_input">
												<input type="text" class="comp_name cursor_not_allowed" name="companyemail" readonly value="<?php echo $data[0]['email'];?>" placeholder="<?php echo trans('employer.placeholder_email')?>"></input>
										</p>
										</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"><?php echo trans('employer.country')?></p>
											<p class="edit_input">
												<!-- <input type="text" class="comp_name" name="companyemail" readonly value="<?php echo $data[0]['email'];?>" placeholder="Enter your email address"></input> -->
						<select class="comp_name" id="select-country" name="companycountry" placeholder="<?php echo trans('translate.select_a_country')?>" style="font-size: 15px !important">
						<option value="" selected></option> 
						<?php
                        if (trans('translate.lang')=="en") 
                        {
                  echo "
                        <option value='US'>".trans('country_translation.US')."</option>
                        <option value='GB'>".trans('country_translation.GB')."</option>
                        <option value='AU'>".trans('country_translation.AU')."</option>
                        <optgroup label='----------------------------------'>
                        <option value='AF'>".trans('country_translation.AF')."</option>
                        <option value='AL'>".trans('country_translation.AL')."</option>
                        <option value='DZ'>".trans('country_translation.DZ')."</option>
                        <option value='AS'>".trans('country_translation.AS')."</option>
                        <option value='AD'>".trans('country_translation.AD')."</option>
                        <option value='AO'>".trans('country_translation.AO')."</option>
                        <option value='AI'>".trans('country_translation.AI')."</option>
                        <option value='AG'>".trans('country_translation.AG')."</option>
                        <option value='AR'>".trans('country_translation.AR')."</option>
                        <option value='AM'>".trans('country_translation.AM')."</option>
                        <option value='AW'>".trans('country_translation.AW')."</option>
                        <option value='AT'>".trans('country_translation.AT')."</option>
                        <option value='AZ'>".trans('country_translation.AZ')."</option>
                        <option value='BS'>".trans('country_translation.BS')."</option>
                        <option value='BH'>".trans('country_translation.BH')."</option>
                        <option value='BD'>".trans('country_translation.BD')."</option>
                        <option value='BB'>".trans('country_translation.BB')."</option>
                        <option value='BY'>".trans('country_translation.BY')."</option>
                        <option value='BE'>".trans('country_translation.BE')."</option>
                        <option value='BZ'>".trans('country_translation.BZ')."</option>
                        <option value='BJ'>".trans('country_translation.BJ')."</option>
                        <option value='BM'>".trans('country_translation.BM')."</option>
                        <option value='BT'>".trans('country_translation.BT')."</option>
                        <option value='BO'>".trans('country_translation.BO')."</option>
                        <option value='BA'>".trans('country_translation.BA')."</option>
                        <option value='BW'>".trans('country_translation.BW')."</option>
                        <option value='BR'>".trans('country_translation.BR')."</option>
                        <option value='VG'>".trans('country_translation.VG')."</option>
                        <option value='BN'>".trans('country_translation.BN')."</option>
                        <option value='BG'>".trans('country_translation.BG')."</option>
                        <option value='BF'>".trans('country_translation.BF')."</option>
                        <option value='MM'>".trans('country_translation.MM')."</option>
                        <option value='BI'>".trans('country_translation.BI')."</option>
                        <option value='KH'>".trans('country_translation.KH')."</option>
                        <option value='CM'>".trans('country_translation.CM')."</option>
                        <option value='CA'>".trans('country_translation.CA')."</option>
                        <option value='CV'>".trans('country_translation.CV')."</option>
                        <option value='KY'>".trans('country_translation.KY')."</option>
                        <option value='CF'>".trans('country_translation.CF')."</option>
                        <option value='TD'>".trans('country_translation.TD')."</option>
                        <option value='CL'>".trans('country_translation.CL')."</option>
                        <option value='CN'>".trans('country_translation.CN')."</option>
                        <option value='CX'>".trans('country_translation.CX')."</option>
                        <option value='CC'>".trans('country_translation.CC')."</option>
                        <option value='CO'>".trans('country_translation.CO')."</option>
                        <option value='KM'>".trans('country_translation.KM')."</option>
                        <option value='CK'>".trans('country_translation.CK')."</option>
                        <option value='CR'>".trans('country_translation.CR')."</option>
                        <option value='HR'>".trans('country_translation.HR')."</option>
                        <option value='CU'>".trans('country_translation.CU')."</option>
                        <option value='CW'>".trans('country_translation.CW')."</option>
                        <option value='CY'>".trans('country_translation.CY')."</option>
                        <option value='CZ'>".trans('country_translation.CZ')."</option>
                        <option value='CD'>".trans('country_translation.CD')."</option>
                        <option value='DK'>".trans('country_translation.DK')."</option>
                        <option value='DJ'>".trans('country_translation.DJ')."</option>
                        <option value='DM'>".trans('country_translation.DM')."</option>
                        <option value='DO'>".trans('country_translation.DO')."</option>
                        <option value='EC'>".trans('country_translation.EC')."</option>
                        <option value='EG'>".trans('country_translation.EG')."</option>
                        <option value='SV'>".trans('country_translation.SV')."</option>
                        <option value='GQ'>".trans('country_translation.GQ')."</option>
                        <option value='ER'>".trans('country_translation.ER')."</option>
                        <option value='EE'>".trans('country_translation.EE')."</option>
                        <option value='ET'>".trans('country_translation.ET')."</option>
                        <option value='FO'>".trans('country_translation.FO')."</option>
                        <option value='FJ'>".trans('country_translation.FJ')."</option>
                        <option value='FI'>".trans('country_translation.FI')."</option>
                        <option value='FR'>".trans('country_translation.FR')."</option>
                        <option value='GF'>".trans('country_translation.GF')."</option>
                        <option value='PF'>".trans('country_translation.PF')."</option>
                        <option value='GA'>".trans('country_translation.GA')."</option>
                        <option value='GM'>".trans('country_translation.GM')."</option>
                        <option value='GE'>".trans('country_translation.GE')."</option>
                        <option value='DE'>".trans('country_translation.DE')."</option>
                        <option value='GH'>".trans('country_translation.GH')."</option>
                        <option value='GI'>".trans('country_translation.GI')."</option>
                        <option value='GR'>".trans('country_translation.GR')."</option>
                        <option value='GL'>".trans('country_translation.GL')."</option>
                        <option value='GD'>".trans('country_translation.GD')."</option>
                        <option value='GP'>".trans('country_translation.GP')."</option>
                        <option value='GU'>".trans('country_translation.GU')."</option>
                        <option value='GT'>".trans('country_translation.GT')."</option>
                        <option value='GG'>".trans('country_translation.GG')."</option>
                        <option value='GN'>".trans('country_translation.GN')."</option>
                        <option value='GW'>".trans('country_translation.GW')."</option>
                        <option value='GY'>".trans('country_translation.GY')."</option>
                        <option value='HT'>".trans('country_translation.HT')."</option>
                        <option value='VA'>".trans('country_translation.VA')."</option>
                        <option value='HN'>".trans('country_translation.HN')."</option>
                        <option value='HK'>".trans('country_translation.HK')."</option>
                        <option value='HU'>".trans('country_translation.HU')."</option>
                        <option value='IS'>".trans('country_translation.IS')."</option>
                        <option value='IN'>".trans('country_translation.IN')."</option>
                        <option value='ID'>".trans('country_translation.ID')."</option>
                        <option value='IR'>".trans('country_translation.IR')."</option>
                        <option value='IQ'>".trans('country_translation.IQ')."</option>
                        <option value='IE'>".trans('country_translation.IE')."</option>
                        <option value='IM'>".trans('country_translation.IM')."</option>
                        <option value='IL'>".trans('country_translation.IL')."</option>
                        <option value='IT'>".trans('country_translation.IT')."</option>
                        <option value='CI'>".trans('country_translation.CI')."</option>
                        <option value='JM'>".trans('country_translation.JM')."</option>
                        <option value='JP'>".trans('country_translation.JP')."</option>
                        <option value='JE'>".trans('country_translation.JE')."</option>
                        <option value='JO'>".trans('country_translation.JO')."</option>
                        <option value='KZ'>".trans('country_translation.KZ')."</option>
                        <option value='KE'>".trans('country_translation.KE')."</option>
                        <option value='KI'>".trans('country_translation.KI')."</option>
                        <option value='XK'>".trans('country_translation.XK')."</option>
                        <option value='KW'>".trans('country_translation.KW')."</option>
                        <option value='KG'>".trans('country_translation.KG')."</option>
                        <option value='LA'>".trans('country_translation.LA')."</option>
                        <option value='LV'>".trans('country_translation.LV')."</option>
                        <option value='LB'>".trans('country_translation.LB')."</option>
                        <option value='LS'>".trans('country_translation.LS')."</option>
                        <option value='LR'>".trans('country_translation.LR')."</option>
                        <option value='LY'>".trans('country_translation.LY')."</option>
                        <option value='LI'>".trans('country_translation.LI')."</option>
                        <option value='LT'>".trans('country_translation.LT')."</option>
                        <option value='LU'>".trans('country_translation.LU')."</option>
                        <option value='MO'>".trans('country_translation.MO')."</option>
                        <option value='MK'>".trans('country_translation.MK')."</option>
                        <option value='MG'>".trans('country_translation.MG')."</option>
                        <option value='MW'>".trans('country_translation.MW')."</option>
                        <option value='MY'>".trans('country_translation.MY')."</option>
                        <option value='MV'>".trans('country_translation.MV')."</option>
                        <option value='ML'>".trans('country_translation.ML')."</option>
                        <option value='MT'>".trans('country_translation.MT')."</option>
                        <option value='MH'>".trans('country_translation.MH')."</option>
                        <option value='MQ'>".trans('country_translation.MQ')."</option>
                        <option value='MR'>".trans('country_translation.MR')."</option>
                        <option value='MU'>".trans('country_translation.MU')."</option>
                        <option value='YT'>".trans('country_translation.YT')."</option>
                        <option value='MX'>".trans('country_translation.MX')."</option>
                        <option value='FM'>".trans('country_translation.FM')."</option>
                        <option value='MD'>".trans('country_translation.MD')."</option>
                        <option value='MC'>".trans('country_translation.MC')."</option>
                        <option value='MN'>".trans('country_translation.MN')."</option>
                        <option value='ME'>".trans('country_translation.ME')."</option>
                        <option value='MS'>".trans('country_translation.MS')."</option>
                        <option value='MA'>".trans('country_translation.MA')."</option>
                        <option value='MZ'>".trans('country_translation.MZ')."</option>
                        <option value='NA'>".trans('country_translation.NA')."</option>
                        <option value='NR'>".trans('country_translation.NR')."</option>
                        <option value='NP'>".trans('country_translation.NP')."</option>
                        <option value='NL'>".trans('country_translation.NL')."</option>
                        <option value='AN'>".trans('country_translation.AN')."</option>
                        <option value='NC'>".trans('country_translation.NC')."</option>
                        <option value='NZ'>".trans('country_translation.NZ')."</option>
                        <option value='NI'>".trans('country_translation.NI')."</option>
                        <option value='NE'>".trans('country_translation.NE')."</option>
                        <option value='NG'>".trans('country_translation.NG')."</option>
                        <option value='NU'>".trans('country_translation.NU')."</option>
                        <option value='NF'>".trans('country_translation.NF')."</option>
                        <option value='KP'>".trans('country_translation.KP')."</option>
                        <option value='MP'>".trans('country_translation.MP')."</option>
                        <option value='NO'>".trans('country_translation.NO')."</option>
                        <option value='OM'>".trans('country_translation.OM')."</option>
                        <option value='PK'>".trans('country_translation.PK')."</option>
                        <option value='PW'>".trans('country_translation.PW')."</option>
                        <option value='PS'>".trans('country_translation.PS')."</option>
                        <option value='PA'>".trans('country_translation.PA')."</option>
                        <option value='PG'>".trans('country_translation.PG')."</option>
                        <option value='PY'>".trans('country_translation.PY')."</option>
                        <option value='PE'>".trans('country_translation.PE')."</option>
                        <option value='PH'>".trans('country_translation.PH')."</option>
                        <option value='PN'>".trans('country_translation.PN')."</option>
                        <option value='PL'>".trans('country_translation.PL')."</option>
                        <option value='PT'>".trans('country_translation.PT')."</option>
                        <option value='PR'>".trans('country_translation.PR')."</option>
                        <option value='QA'>".trans('country_translation.QA')."</option>
                        <option value='CG'>".trans('country_translation.CG')."</option>
                        <option value='RE'>".trans('country_translation.RE')."</option>
                        <option value='RO'>".trans('country_translation.RO')."</option>
                        <option value='RU'>".trans('country_translation.RU')."</option>
                        <option value='RW'>".trans('country_translation.RW')."</option>
                        <option value='BL'>".trans('country_translation.BL')."</option>
                        <option value='SH'>".trans('country_translation.SH')."</option>
                        <option value='KN'>".trans('country_translation.KN')."</option>
                        <option value='LC'>".trans('country_translation.LC')."</option>
                        <option value='SX'>".trans('country_translation.SX')."</option>
                        <option value='PM'>".trans('country_translation.PM')."</option>
                        <option value='VC'>".trans('country_translation.VC')."</option>
                        <option value='WS'>".trans('country_translation.WS')."</option>
                        <option value='SM'>".trans('country_translation.SM')."</option>
                        <option value='ST'>".trans('country_translation.ST')."</option>
                        <option value='SA'>".trans('country_translation.SA')."</option>
                        <option value='SN'>".trans('country_translation.SN')."</option>
                        <option value='RS'>".trans('country_translation.RS')."</option>
                        <option value='SC'>".trans('country_translation.SC')."</option>
                        <option value='SL'>".trans('country_translation.SL')."</option>
                        <option value='SG'>".trans('country_translation.SG')."</option>
                        <option value='SK'>".trans('country_translation.SK')."</option>
                        <option value='SI'>".trans('country_translation.SI')."</option>
                        <option value='SB'>".trans('country_translation.SB')."</option>
                        <option value='SO'>".trans('country_translation.SO')."</option>
                        <option value='ZA'>".trans('country_translation.ZA')."</option>
                        <option value='KR'>".trans('country_translation.KR')."</option>
                        <option value='SS'>".trans('country_translation.SS')."</option>
                        <option value='ES'>".trans('country_translation.ES')."</option>
                        <option value='LK'>".trans('country_translation.LK')."</option>
                        <option value='SD'>".trans('country_translation.SD')."</option>
                        <option value='SR'>".trans('country_translation.SR')."</option>
                        <option value='SJ'>".trans('country_translation.SJ')."</option>
                        <option value='SZ'>".trans('country_translation.SZ')."</option>
                        <option value='SE'>".trans('country_translation.SE')."</option>
                        <option value='CH'>".trans('country_translation.CH')."</option>
                        <option value='SY'>".trans('country_translation.SY')."</option>
                        <option value='TW'>".trans('country_translation.TW')."</option>
                        <option value='TJ'>".trans('country_translation.TJ')."</option>
                        <option value='TZ'>".trans('country_translation.TZ')."</option>
                        <option value='TH'>".trans('country_translation.TH')."</option>
                        <option value='TL'>".trans('country_translation.TL')."</option>
                        <option value='TG'>".trans('country_translation.TG')."</option>
                        <option value='TK'>".trans('country_translation.TK')."</option>
                        <option value='TO'>".trans('country_translation.TO')."</option>
                        <option value='TT'>".trans('country_translation.TT')."</option>
                        <option value='TN'>".trans('country_translation.TN')."</option>
                        <option value='TR'>".trans('country_translation.TR')."</option>
                        <option value='TM'>".trans('country_translation.TM')."</option>
                        <option value='TC'>".trans('country_translation.TC')."</option>
                        <option value='TV'>".trans('country_translation.TV')."</option>
                        <option value='UG'>".trans('country_translation.UG')."</option>
                        <option value='UA'>".trans('country_translation.UA')."</option>
                        <option value='AE'>".trans('country_translation.AE')."</option>
                        <option value='UY'>".trans('country_translation.UY')."</option>
                        <option value='UZ'>".trans('country_translation.UZ')."</option>
                        <option value='VU'>".trans('country_translation.VU')."</option>
                        <option value='VE'>".trans('country_translation.VE')."</option>
                        <option value='VN'>".trans('country_translation.VN')."</option>
                        <option value='VI'>".trans('country_translation.VI')."</option>
                        <option value='WF'>".trans('country_translation.WF')."</option>
                        <option value='YE'>".trans('country_translation.YE')."</option>
                        <option value='ZM'>".trans('country_translation.ZM')."</option>
                        <option value='ZW'>".trans('country_translation.ZW')."</option>
                        <option value='AX'>".trans('country_translation.AX')."</option>
                        </optgroup>
                        ";
                        }
                        else
                        {
                   echo "<option value='DE'>".trans('country_translation.DE')."</option>
                        <option value='CH'>".trans('country_translation.CH')."</option>
                        <option value='AT'>".trans('country_translation.AT')."</option>
                        <optgroup label='----------------------------------'>
                        <option value='AF'>".trans('country_translation.AF')."</option>
                        <option value='AL'>".trans('country_translation.AL')."</option>
                        <option value='DZ'>".trans('country_translation.DZ')."</option>
                        <option value='AS'>".trans('country_translation.AS')."</option>
                        <option value='AD'>".trans('country_translation.AD')."</option>
                        <option value='AO'>".trans('country_translation.AO')."</option>
                        <option value='AI'>".trans('country_translation.AI')."</option>
                        <option value='AG'>".trans('country_translation.AG')."</option>
                        <option value='AR'>".trans('country_translation.AR')."</option>
                        <option value='AM'>".trans('country_translation.AM')."</option>
                        <option value='AW'>".trans('country_translation.AW')."</option>
                        <option value='AU'>".trans('country_translation.AU')."</option>
                        <option value='AZ'>".trans('country_translation.AZ')."</option>
                        <option value='BS'>".trans('country_translation.BS')."</option>
                        <option value='BH'>".trans('country_translation.BH')."</option>
                        <option value='BD'>".trans('country_translation.BD')."</option>
                        <option value='BB'>".trans('country_translation.BB')."</option>
                        <option value='BY'>".trans('country_translation.BY')."</option>
                        <option value='BE'>".trans('country_translation.BE')."</option>
                        <option value='BZ'>".trans('country_translation.BZ')."</option>
                        <option value='BJ'>".trans('country_translation.BJ')."</option>
                        <option value='BM'>".trans('country_translation.BM')."</option>
                        <option value='BT'>".trans('country_translation.BT')."</option>
                        <option value='BO'>".trans('country_translation.BO')."</option>
                        <option value='BA'>".trans('country_translation.BA')."</option>
                        <option value='BW'>".trans('country_translation.BW')."</option>
                        <option value='BR'>".trans('country_translation.BR')."</option>
                        <option value='VG'>".trans('country_translation.VG')."</option>
                        <option value='BN'>".trans('country_translation.BN')."</option>
                        <option value='BG'>".trans('country_translation.BG')."</option>
                        <option value='BF'>".trans('country_translation.BF')."</option>
                        <option value='MM'>".trans('country_translation.MM')."</option>
                        <option value='BI'>".trans('country_translation.BI')."</option>
                        <option value='KH'>".trans('country_translation.KH')."</option>
                        <option value='CM'>".trans('country_translation.CM')."</option>
                        <option value='CA'>".trans('country_translation.CA')."</option>
                        <option value='CV'>".trans('country_translation.CV')."</option>
                        <option value='KY'>".trans('country_translation.KY')."</option>
                        <option value='CF'>".trans('country_translation.CF')."</option>
                        <option value='TD'>".trans('country_translation.TD')."</option>
                        <option value='CL'>".trans('country_translation.CL')."</option>
                        <option value='CN'>".trans('country_translation.CN')."</option>
                        <option value='CX'>".trans('country_translation.CX')."</option>
                        <option value='CC'>".trans('country_translation.CC')."</option>
                        <option value='CO'>".trans('country_translation.CO')."</option>
                        <option value='KM'>".trans('country_translation.KM')."</option>
                        <option value='CK'>".trans('country_translation.CK')."</option>
                        <option value='CR'>".trans('country_translation.CR')."</option>
                        <option value='HR'>".trans('country_translation.HR')."</option>
                        <option value='CU'>".trans('country_translation.CU')."</option>
                        <option value='CW'>".trans('country_translation.CW')."</option>
                        <option value='CY'>".trans('country_translation.CY')."</option>
                        <option value='CZ'>".trans('country_translation.CZ')."</option>
                        <option value='CD'>".trans('country_translation.CD')."</option>
                        <option value='DK'>".trans('country_translation.DK')."</option>
                        <option value='DJ'>".trans('country_translation.DJ')."</option>
                        <option value='DM'>".trans('country_translation.DM')."</option>
                        <option value='DO'>".trans('country_translation.DO')."</option>
                        <option value='EC'>".trans('country_translation.EC')."</option>
                        <option value='EG'>".trans('country_translation.EG')."</option>
                        <option value='SV'>".trans('country_translation.SV')."</option>
                        <option value='GQ'>".trans('country_translation.GQ')."</option>
                        <option value='ER'>".trans('country_translation.ER')."</option>
                        <option value='EE'>".trans('country_translation.EE')."</option>
                        <option value='ET'>".trans('country_translation.ET')."</option>
                        <option value='FO'>".trans('country_translation.FO')."</option>
                        <option value='FJ'>".trans('country_translation.FJ')."</option>
                        <option value='FI'>".trans('country_translation.FI')."</option>
                        <option value='FR'>".trans('country_translation.FR')."</option>
                        <option value='GF'>".trans('country_translation.GF')."</option>
                        <option value='PF'>".trans('country_translation.PF')."</option>
                        <option value='GA'>".trans('country_translation.GA')."</option>
                        <option value='GM'>".trans('country_translation.GM')."</option>
                        <option value='GE'>".trans('country_translation.GE')."</option>
                        <option value='GH'>".trans('country_translation.GH')."</option>
                        <option value='GI'>".trans('country_translation.GI')."</option>
                        <option value='GR'>".trans('country_translation.GR')."</option>
                        <option value='GL'>".trans('country_translation.GL')."</option>
                        <option value='GD'>".trans('country_translation.GD')."</option>
                        <option value='GP'>".trans('country_translation.GP')."</option>
                        <option value='GU'>".trans('country_translation.GU')."</option>
                        <option value='GT'>".trans('country_translation.GT')."</option>
                        <option value='GG'>".trans('country_translation.GG')."</option>
                        <option value='GN'>".trans('country_translation.GN')."</option>
                        <option value='GW'>".trans('country_translation.GW')."</option>
                        <option value='GY'>".trans('country_translation.GY')."</option>
                        <option value='HT'>".trans('country_translation.HT')."</option>
                        <option value='VA'>".trans('country_translation.VA')."</option>
                        <option value='HN'>".trans('country_translation.HN')."</option>
                        <option value='HK'>".trans('country_translation.HK')."</option>
                        <option value='HU'>".trans('country_translation.HU')."</option>
                        <option value='IS'>".trans('country_translation.IS')."</option>
                        <option value='IN'>".trans('country_translation.IN')."</option>
                        <option value='ID'>".trans('country_translation.ID')."</option>
                        <option value='IR'>".trans('country_translation.IR')."</option>
                        <option value='IQ'>".trans('country_translation.IQ')."</option>
                        <option value='IE'>".trans('country_translation.IE')."</option>
                        <option value='IM'>".trans('country_translation.IM')."</option>
                        <option value='IL'>".trans('country_translation.IL')."</option>
                        <option value='IT'>".trans('country_translation.IT')."</option>
                        <option value='CI'>".trans('country_translation.CI')."</option>
                        <option value='JM'>".trans('country_translation.JM')."</option>
                        <option value='JP'>".trans('country_translation.JP')."</option>
                        <option value='JE'>".trans('country_translation.JE')."</option>
                        <option value='JO'>".trans('country_translation.JO')."</option>
                        <option value='KZ'>".trans('country_translation.KZ')."</option>
                        <option value='KE'>".trans('country_translation.KE')."</option>
                        <option value='KI'>".trans('country_translation.KI')."</option>
                        <option value='XK'>".trans('country_translation.XK')."</option>
                        <option value='KW'>".trans('country_translation.KW')."</option>
                        <option value='KG'>".trans('country_translation.KG')."</option>
                        <option value='LA'>".trans('country_translation.LA')."</option>
                        <option value='LV'>".trans('country_translation.LV')."</option>
                        <option value='LB'>".trans('country_translation.LB')."</option>
                        <option value='LS'>".trans('country_translation.LS')."</option>
                        <option value='LR'>".trans('country_translation.LR')."</option>
                        <option value='LY'>".trans('country_translation.LY')."</option>
                        <option value='LI'>".trans('country_translation.LI')."</option>
                        <option value='LT'>".trans('country_translation.LT')."</option>
                        <option value='LU'>".trans('country_translation.LU')."</option>
                        <option value='MO'>".trans('country_translation.MO')."</option>
                        <option value='MK'>".trans('country_translation.MK')."</option>
                        <option value='MG'>".trans('country_translation.MG')."</option>
                        <option value='MW'>".trans('country_translation.MW')."</option>
                        <option value='MY'>".trans('country_translation.MY')."</option>
                        <option value='MV'>".trans('country_translation.MV')."</option>
                        <option value='ML'>".trans('country_translation.ML')."</option>
                        <option value='MT'>".trans('country_translation.MT')."</option>
                        <option value='MH'>".trans('country_translation.MH')."</option>
                        <option value='MQ'>".trans('country_translation.MQ')."</option>
                        <option value='MR'>".trans('country_translation.MR')."</option>
                        <option value='MU'>".trans('country_translation.MU')."</option>
                        <option value='YT'>".trans('country_translation.YT')."</option>
                        <option value='MX'>".trans('country_translation.MX')."</option>
                        <option value='FM'>".trans('country_translation.FM')."</option>
                        <option value='MD'>".trans('country_translation.MD')."</option>
                        <option value='MC'>".trans('country_translation.MC')."</option>
                        <option value='MN'>".trans('country_translation.MN')."</option>
                        <option value='ME'>".trans('country_translation.ME')."</option>
                        <option value='MS'>".trans('country_translation.MS')."</option>
                        <option value='MA'>".trans('country_translation.MA')."</option>
                        <option value='MZ'>".trans('country_translation.MZ')."</option>
                        <option value='NA'>".trans('country_translation.NA')."</option>
                        <option value='NR'>".trans('country_translation.NR')."</option>
                        <option value='NP'>".trans('country_translation.NP')."</option>
                        <option value='NL'>".trans('country_translation.NL')."</option>
                        <option value='AN'>".trans('country_translation.AN')."</option>
                        <option value='NC'>".trans('country_translation.NC')."</option>
                        <option value='NZ'>".trans('country_translation.NZ')."</option>
                        <option value='NI'>".trans('country_translation.NI')."</option>
                        <option value='NE'>".trans('country_translation.NE')."</option>
                        <option value='NG'>".trans('country_translation.NG')."</option>
                        <option value='NU'>".trans('country_translation.NU')."</option>
                        <option value='NF'>".trans('country_translation.NF')."</option>
                        <option value='KP'>".trans('country_translation.KP')."</option>
                        <option value='MP'>".trans('country_translation.MP')."</option>
                        <option value='NO'>".trans('country_translation.NO')."</option>
                        <option value='OM'>".trans('country_translation.OM')."</option>
                        <option value='PK'>".trans('country_translation.PK')."</option>
                        <option value='PW'>".trans('country_translation.PW')."</option>
                        <option value='PS'>".trans('country_translation.PS')."</option>
                        <option value='PA'>".trans('country_translation.PA')."</option>
                        <option value='PG'>".trans('country_translation.PG')."</option>
                        <option value='PY'>".trans('country_translation.PY')."</option>
                        <option value='PE'>".trans('country_translation.PE')."</option>
                        <option value='PH'>".trans('country_translation.PH')."</option>
                        <option value='PN'>".trans('country_translation.PN')."</option>
                        <option value='PL'>".trans('country_translation.PL')."</option>
                        <option value='PT'>".trans('country_translation.PT')."</option>
                        <option value='PR'>".trans('country_translation.PR')."</option>
                        <option value='QA'>".trans('country_translation.QA')."</option>
                        <option value='CG'>".trans('country_translation.CG')."</option>
                        <option value='RE'>".trans('country_translation.RE')."</option>
                        <option value='RO'>".trans('country_translation.RO')."</option>
                        <option value='RU'>".trans('country_translation.RU')."</option>
                        <option value='RW'>".trans('country_translation.RW')."</option>
                        <option value='BL'>".trans('country_translation.BL')."</option>
                        <option value='SH'>".trans('country_translation.SH')."</option>
                        <option value='KN'>".trans('country_translation.KN')."</option>
                        <option value='LC'>".trans('country_translation.LC')."</option>
                        <option value='SX'>".trans('country_translation.SX')."</option>
                        <option value='PM'>".trans('country_translation.PM')."</option>
                        <option value='VC'>".trans('country_translation.VC')."</option>
                        <option value='WS'>".trans('country_translation.WS')."</option>
                        <option value='SM'>".trans('country_translation.SM')."</option>
                        <option value='ST'>".trans('country_translation.ST')."</option>
                        <option value='SA'>".trans('country_translation.SA')."</option>
                        <option value='SN'>".trans('country_translation.SN')."</option>
                        <option value='RS'>".trans('country_translation.RS')."</option>
                        <option value='SC'>".trans('country_translation.SC')."</option>
                        <option value='SL'>".trans('country_translation.SL')."</option>
                        <option value='SG'>".trans('country_translation.SG')."</option>
                        <option value='SK'>".trans('country_translation.SK')."</option>
                        <option value='SI'>".trans('country_translation.SI')."</option>
                        <option value='SB'>".trans('country_translation.SB')."</option>
                        <option value='SO'>".trans('country_translation.SO')."</option>
                        <option value='ZA'>".trans('country_translation.ZA')."</option>
                        <option value='KR'>".trans('country_translation.KR')."</option>
                        <option value='SS'>".trans('country_translation.SS')."</option>
                        <option value='ES'>".trans('country_translation.ES')."</option>
                        <option value='LK'>".trans('country_translation.LK')."</option>
                        <option value='SD'>".trans('country_translation.SD')."</option>
                        <option value='SR'>".trans('country_translation.SR')."</option>
                        <option value='SJ'>".trans('country_translation.SJ')."</option>
                        <option value='SZ'>".trans('country_translation.SZ')."</option>
                        <option value='SE'>".trans('country_translation.SE')."</option>
                        <option value='SY'>".trans('country_translation.SY')."</option>
                        <option value='TW'>".trans('country_translation.TW')."</option>
                        <option value='TJ'>".trans('country_translation.TJ')."</option>
                        <option value='TZ'>".trans('country_translation.TZ')."</option>
                        <option value='TH'>".trans('country_translation.TH')."</option>
                        <option value='TL'>".trans('country_translation.TL')."</option>
                        <option value='TG'>".trans('country_translation.TG')."</option>
                        <option value='TK'>".trans('country_translation.TK')."</option>
                        <option value='TO'>".trans('country_translation.TO')."</option>
                        <option value='TT'>".trans('country_translation.TT')."</option>
                        <option value='TN'>".trans('country_translation.TN')."</option>
                        <option value='TR'>".trans('country_translation.TR')."</option>
                        <option value='TM'>".trans('country_translation.TM')."</option>
                        <option value='TC'>".trans('country_translation.TC')."</option>
                        <option value='TV'>".trans('country_translation.TV')."</option>
                        <option value='UG'>".trans('country_translation.UG')."</option>
                        <option value='UA'>".trans('country_translation.UA')."</option>
                        <option value='AE'>".trans('country_translation.AE')."</option>
                        <option value='GB'>".trans('country_translation.GB')."</option>
                        <option value='US'>".trans('country_translation.US')."</option>
                        <option value='UY'>".trans('country_translation.UY')."</option>
                        <option value='UZ'>".trans('country_translation.UZ')."</option>
                        <option value='VU'>".trans('country_translation.VU')."</option>
                        <option value='VE'>".trans('country_translation.VE')."</option>
                        <option value='VN'>".trans('country_translation.VN')."</option>
                        <option value='VI'>".trans('country_translation.VI')."</option>
                        <option value='WF'>".trans('country_translation.WF')."</option>
                        <option value='YE'>".trans('country_translation.YE')."</option>
                        <option value='ZM'>".trans('country_translation.ZM')."</option>
                        <option value='ZW'>".trans('country_translation.ZW')."</option>
                        <option value='AX'>".trans('country_translation.AX')."</option>
                        </optgroup>
                        ";
                        }
                    ?>
												</select>
										</p>
										</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"><?php echo trans('employer.language')?></p>
											<p class="edit_input">
												<select class="comp_name " id="select-language" name="companylangugage" placeholder="<?php echo trans('translate.select_a_language')?>" value="ak">
                                                <option value="" selected></option> 
												<option value="de">Deutsch</option>
												<option value="en">English</option>
												</select>
										</p>
										</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"><?php echo trans('employer.phone')?></p>
											<p class="edit_input">
												<input type="text" id="company_phone"class="comp_name" name="companyphone"value="<?php echo $data[0]['phone'];?>" placeholder="<?php echo trans('employer.placeholder_phone')?>"></input>
										</p>
										</div>
										</div>	
										<!-- <div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"><?php echo trans('employer.fax')?></p>
											<p class="edit_input">
												<input type="text" id="company_fax"class="comp_name" name="companyfax"value="<?php echo $data[0]['fax'];?>" placeholder="<?php echo trans('employer.placeholder_fax')?>"></input>
										</p>
										</div>
										</div> -->
										<!-- <div class="col-md-6 col-sm-12 col-xs-12 no_padding">
											<div class="basic_info_listing">
											<p class="edit_label"><?php echo trans('employer.skype')?></p>
											<p class="edit_input">
												<input type="text" id="company_skype" class="comp_name" name="companyskype"value="<?php echo $data[0]['skype'];?>" placeholder="<?php echo trans('employer.placeholder_skype')?>"></input>
										</p>
										</div>
										</div> -->
										<input type="hidden" id="time" name="time"></input>
									</div>
									<div class="col-md-12 edit_submit">
										<button type="submit" class="save"><?php echo trans('employer.save_button')?></button>
									</div>
								</form>
							</div>
							</div>
					</div>
						
					</div>
				</div>
			 <div class="modal fade" id="password_reset">
        <div class="modal-dialog">
            <div class="modal-content" style="padding: 17px;">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style=" font-family: sans-serif; "><?php echo trans('employer.change_password')?></h4>
                </div>
                
                <div class="modal-body">
                
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-group">
                            <form  method="post" id="change_password" action="<?php echo url('passwordChange')?>">
                                <label for="field-3" class="control-label" style=" font-family: sans-serif; "><?php echo trans('employer.old_password')?></label>
                                
                                <input type="password" class="form-control" id="old_password" name="old_password" placeholder="<?php echo trans('employer.old_password')?>" style=" font-family: sans-serif; " autocomplete="off">
                                <label for="field-3" class="control-label" style=" font-family: sans-serif;margin-top: 17px; "><?php echo trans('employer.new_password')?></label>
                                
                                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="<?php echo trans('employer.new_password')?>" style=" font-family: sans-serif; " autocomplete="off">
                                <label for="field-3" class="control-label" style=" font-family: sans-serif;margin-top: 17px; "><?php echo trans('employer.retype_password')?></label>
                                
                                <input type="password" class="form-control" id="retype_password" name="retype_password" placeholder="<?php echo trans('employer.retype_password')?>" style=" font-family: sans-serif; " autocomplete="off">
                                <div style=" text-align: center; position: relative; top: 24px; ">
                                <button type="submit" class="save" style=" font-family: sans-serif; "><?php echo trans('employer.submit_button')?></button>
                                </div>
                                </form>
                            </div>  
                            
                        </div>
                    </div>
                
                 </div>   
                </div>
                </div>
                </div>
                
	</div>
	<script src="<?php echo url('js/jquery.form.js')?>"></script>
	<script src="<?php echo url('js/selectize.js')?>"></script>
<script type="text/javascript">
//select country and langauge file
$('#select-country option[value=<?php echo $data[0]['country'];?>]').attr('selected','selected');
$('#select-language option[value=<?php echo $data[0]['language'];?>]').attr('selected','selected');
//select.js
$('#select-country').selectize();
$('#select-language').selectize();

  function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profile_img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
   		 }
    }


 $("#profile-image-upload").on("change", function() {
   	readURL(this);
   	 $('.mot_before_upload').show();
    $('.show_btn').show();
    $('.camera_icon').hide();
  });

 $("#profile-image-upload").on("change", function() {
    $('.show_btn').show();
   })

$('.cancelphoto').click(function(){
    $('.mot_before_upload').hide();
    $('.show_btn').hide();
    $('.camera_icon').show();
});
$('#upload-profile-img').click(function(){
	$('.upload_cancel').hide();
	$('.loader_div').show();
	$('#image_form').ajaxSubmit({ 
        type: "POST",
        url: "upload/image1",
        data: $('#image_form').serialize(),
        cache: false,
        success:function(response){
        if(response=='success')
      			{
				  swal({  
 	                        title: "<?php echo trans('popup.success');?>", 
                         text: "<?php echo trans('popup.added_successfully');?>",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){
                        	$('.loader_div').hide();
                        	$('.upload_cancel').hide();
                            /* window.location.href = '<?php  $res=Auth::user(); echo url('profile/'.$res['id']); ?>';*/
                            location.reload();
  
                        });

				 }
				 else
				 {
				 	$('.loader_div').hide();
                    $('.upload_cancel').show();
				 }
   	   }
    });
	});
</script>  
	<script>
		$(".left-links li a").click(function(){
    	$(this).find('i').toggleClass('fa-indent fa-outdent');
       });
	</script>
	<script>
		$(".fo").click(function(){ 
			$('.show_animate').slideToggle(600);
			$('.employee_whole_edit').css('display','block');

		});
	</script>
	<script>
		$(".close").click(function(){ 
			$('.show_animate').slideToggle(600);
			$('.employee_whole_edit').css('display','none');
			$('.basic_information').show();
		});
	</script> 
<script>
 (function() {
$('form').ajaxForm({
    beforeSend: function () {
    	var company_name = $('#company_name').val();
          var isValid = true;
             if(company_name == null ||company_name == '')
              {
       				 swal({  
                         title: "<?php echo trans('popup.error_');?>", 
                         text: "<?php echo trans('popup.company_name');?>",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });
       				 return false;
   			  }
    
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
 if(data.status=='success')
 {
  swal({  
                         title: "<?php echo trans('popup.success');?>", 
                         text: "<?php echo trans('popup.added_successfully');?>",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            /* window.location.href = '<?php  $res=Auth::user(); echo url('profile/'.$res['id']); ?>';*/
                            location.reload();
  
                        });

 }
 else if(data.status=='failure'){
swal("failure!");
 }
    },
    complete: function() {
    
    }
}); 

})();
</script>
<script>
 (function() {
$('#change_password').ajaxForm({
    beforeSend: function () {
       var old_password = $('#old_password').val();
    	var new_password = $('#new_password').val();
    	var retype_password = $('#retype_password').val();
    	 var pass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    /*	console.log(old_password);
    	console.log(new_password);*/
    	/*return false; */        
    	 var isValid = true;
             if(old_password == null ||new_password == null ||retype_password ==null ||old_password == ''||new_password == '' || retype_password == '')
              {
       				 swal({  
                         title: "<?php echo trans('popup.error_');?>", 
                         text: "<?php echo trans('popup.all_fields');?>",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });
       				 return false;
   			  }
   	   else if(pass.test(old_password)  == false || pass.test(new_password)  == false || pass.test(retype_password) == false)
   	   {
        isValid=false;
        swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.password_minimum');?>", "error");
    	}
   		 else if(new_password != retype_password){
        isValid = false;
         swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.not_matching');?>", "error");
    	}
    	if(isValid == false)
    	{
        return false;
   		 }
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
    	console.log(data);
 if(data.status=='success')
 {
  swal({  
                         title: "<?php echo trans('popup.success');?>", 
                         text: "<?php echo trans('popup.password_changed');?>",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                            /* window.location.href = '<?php  $res=Auth::user(); echo url('profile/'.$res['id']); ?>';*/
                            location.reload();
  
                        });

 }
 else if(data.status=='failure'){
  swal({  
                         title: "<?php echo trans('popup.error_');?>", 
                         text: "<?php echo trans('popup.old_password');?>",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });
 }
    },
    complete: function() {
    
    }
}); 

})();
</script>
 <?php  include 'layout/footer.php';?>
