
<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/add_employee.css')?>">
<style type="text/css">
	.form-control{
		margin-bottom: 30px;
		height: 3em;
	}
</style>
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row invite">
				<div class="col-md-12 link">
					
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="panel">
                    <div class="row">
                    	<div class="col-md-6">
                    		<h3>Create Company</h3>
                    		<br>
                    	</div>
                    
                    </div>
                    <div class="row">
                    	<form>
                    		<div class="col-md-6">
                    			<label>Company Name</label>
                    			<input type="text" class="form-control" name="" placeholder="Your text here">
                    		</div>
                    		<div class="col-md-6">
                    			<label>Company Location</label>
                    			<input type="text" class="form-control" name="" placeholder="City / Town">
                    		</div>
                    		<div class="col-md-6">
                    			<label>Company type/industry</label>
                    			<select class="form-control" name="" placeholder="Please select the type / industry ">
                    				<option>Please enter the type / industry</option>
                    				<option>Food</option>
                    				<option>Finance</option>
                    				<option>Courier Services</option>
                    				<option>Software Company</option>
                    			</select> 
                    		</div>
                    		<div class="col-md-6">
                    			<label>Company Size</label>
                    			<select class="form-control" name="" >
                    				<option>0 - 20 employees</option>
                    				<option>20 - 60 employees</option>
                    				<option>60 - 100 employees</option>
                    				<option>100 - 200 employees</option>
                    				<option>> 200  employees</option>
                    			</select> 
                    		</div>
                    		<div class="col-md-6">
                    			<label>Company level weekly offset</label>
                    			<div class="row">
                    				<div class="col-md-4">
	                    				<input type="number" class="form-control" name="" value="40.00">
	                    			</div>
	                    			<div class="col-md-6">
	                    				<p style="margin-top: 10px">Work hours / week</p>
	                    			</div>
                    			</div>
                    		</div>
                    		<div class="col-md-6">
                    			<label>Company level standard annual leave</label>
                    			<div class="row">
                    				<div class="col-md-4">
	                    				<input type="number" class="form-control" name="" value="20.00">
	                    			</div>
	                    			<div class="col-md-6">
	                    				<p style="margin-top: 10px">Days / year</p>
	                    			</div>
                    			</div>
                    		</div>
                    		<div class="col-md-6">
                    			<label>Business owner / deputy email</label>
                    			<div class="row">
                    				<div class="col-md-8">
	                    				<input type="text" class="form-control" name="" value="invite Company (optional)">
	                    			</div>
	                    			<div class="col-md-4">
	                    				<button class="" id="send_invite_btn_x">Send Invite</button>
	                    			</div>
                    			</div>
                    		</div>
                    		<div class="col-md-6"></div>
                    		<br>
                    		<div class="col-md-12" style="text-align: center;">
                    			<a href="manage_users"><button class="btn btn-info" type="button" style="padding: 1.1em">Save & continue adding staff..</button></a> <span style="font-size: 20px;margin-left: 20px;">Save now & add staff later</span>
                    			
                    		</div>	
                    	</form>
                    </div>
                    
                </div>
				</div>
		  </div>
				
				

</div>

		
		
	</div>

 <script src="js/jquery.form.js"></script>


 <?php  include 'layout/footer.php';?>


