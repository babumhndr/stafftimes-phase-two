<body style="padding:0px;margin:0px;font-family: sans-serif;">
<table style="width: 75%;margin: 0px auto;">
<tr style="background-color: rgba(165, 161, 161, 0.24);"><td><div style="text-align:center;padding-top:5px;background: #f2f2f2;padding-bottom:5px;"><a href="<?php echo url('')?>" target="_blank" style="text-decoration: none;"><img src="<?php echo url('image/st.png')?>" style=" height: 51px; margin-top: 4px; "></a></div></td></tr>
<div style="padding-left:50px;padding-right:50px;padding-bottom:1px">
    <div style="border-bottom:1px solid #ededed"></div>
    <div style="margin:20px 0px;font-size:28px;line-height:30px;text-align:center;"><?php echo trans('payment.para'); ?></div>
    <div style="margin-bottom:30px;text-align: center;">
        <div><?php echo trans('payment.para1'); ?></div><br>
        <table style="text-align: center;margin: 0px auto;">
        <tbody style="text-align: left">
        <tr><td><div style="margin-bottom:20px;"><b><?php echo trans('payment.para2'); ?> : </b> <?php echo $order_no?><br></div></td></tr>
        <tr><td><div style="margin-bottom:20px;"><b><?php echo trans('payment.para3'); ?> : </b><?php echo $name?><br></div></td></tr>
        <tr><td><div style="margin-bottom:20px;"><b><?php echo trans('payment.para4'); ?> : </b> <?php echo $email?><br></div></td></tr>
        <tr><td><div style="margin-bottom:20px;"><b><?php echo trans('payment.para5'); ?> : </b> <?php echo $date?><br></div></td></tr>
        </tbody>
        </table>
    </div>
    <div>
        <table style="width:100%;margin:5px 0">
            <tbody>
                <tr>
                    <td style="text-align:left;font-weight:bold;font-size:12px"><?php echo trans('payment.para6'); ?></td>
                    <td style="text-align:right;font-weight:bold;font-size:12px" width="70"><?php echo trans('payment.para7'); ?></td>
                </tr>
            </tbody>
        </table>
        <div style="border-bottom:1px solid #ededed"></div>
        <table style="width:100%;margin:5px 0">
            <tbody>
                <tr></tr>
                <tr>
                    <td style="text-align:left;font-size:12px"><b><?php echo $plan?></b></td>
                    <td style="text-align:right;vertical-align:text-top;font-size:12px" width="70"><span><?php echo $currency?>&nbsp;<?php echo $amount?></span><span></span>
                    </td>
                </tr>
               <!--  <tr>
                    <td style="text-align:left;font-size:12px">Song Purchase</td>
                    <td style="text-align:right;font-size:12px"></td>
                </tr> -->
            </tbody>
        </table>
        <div style="border-bottom:1px solid #ededed"></div>
            <table style="width:100%;margin:5px 0">
                <tbody>
                    <tr><td style="text-align:right;font-size:12px" width="150"><?php echo trans('payment.para8'); ?> : <?php echo $currency?>&nbsp;<?php echo $amount?></td></tr>
                    <tr><td style="text-align:right;font-size:12px" width="150">(<?php echo trans('payment.para9'); ?>)</td></tr>
                </tbody>
            </table>
            <div style="border-bottom:1px solid #ededed"></div>
            <table style="width:100%;margin:5px 0">
                <tbody>
                    <tr><td style="text-align:left;font-weight:bold;font-size:12px"><?php echo trans('payment.para10'); ?>:</td><td>
                        <table style="margin-left:auto">
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td style="font-size:12px"><?php echo trans('payment.para11'); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>
    </div>
    <tr style="background-color: rgba(165, 161, 161, 0.24);"><td><div style="text-align:center;padding-top:5px;background: #f2f2f2;padding-bottom:5px;"><div class="col-md-4 col-sm-4 col-xs-12" style="text-align: center;">
      <div style=" margin-top: 5px; ">
      <a href="<?php echo url('')?>" target="_blank" style="text-decoration: none;">
      <img src="<?php echo url('image/webapp.png')?>" width="55" alt="">
      </a>
      </div>
      <p style=" font-size: 11px; ">Zur Kesselschmiede 29<br>
      8400 Winterthur, <?php echo trans('payment.para12'); ?><br>
      <a href="mailto:support@stafftimes.com">support@stafftimes.com</a></a></p>
      </div></div></td></tr>
</table>
</body>
