<body style="padding:0px;margin:0px;font-family: sans-serif;">
    <div style="padding-left:20px;padding-right:20px;background:#fff">
        <table style="width:100%;background:white">
        <tbody><tr style="background-color: rgba(165, 161, 161, 0.24);"><td><div style="text-align:center;padding-top:5px;padding-bottom:5px;"><a href="<?php echo url('')?>" target="_blank" style="text-decoration: none;"><img src="<?php echo url('image/st.png')?>" style=" height: 51px; margin-top: 4px; "></a></div></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td style="color:#242424;font-size:14px;padding-left:20px"><?php echo trans('password_e.dear')?> <?php echo $name?>,</td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <!-- <tr><td style="color:#242424;font-size:14px;padding-left:20px;padding-right:20px;"></td></tr> -->
            <tr><td></td></tr>
            <tr><td></td></tr>
            <!--<tr><td style="color:#242424;font-size:14px;padding-left:20px;padding-right:20px;">Lawmatters brings dispute resolution experts and consumers online under one platform to facilitate reliable, faster resolution.</td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td style="color:#242424;font-size:14px;padding-left:20px;padding-right:20px;"><b>What can you do on Lawmatters?</b></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td style="color:#242424;font-size:14px;padding-left:20px;padding-right:20px;">You can connect and engage with people in the legal professional network, paralegals and ADRS experts, find jobs or post jobs and create your virtual office to collaborate for your work online.</td></tr>-->
            <tr><td style="color:#242424;font-size:14px;padding-left:20px;padding-right:20px;"><?php echo trans('password_e.thank_you')?> <br> <a href="<?php echo url('/'.'resetPassword/'.$email) ?>" style="color:#175e56;"><?php echo trans('password_e.click_here')?></a></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td style="color:#242424;font-size:14px;padding-left:20px"><?php echo trans('password_e.regards')?>,<br/><?php echo trans('password_e.team')?></td></tr>
        </tbody>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        </table>
    </div>
</body>