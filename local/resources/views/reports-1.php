<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<link rel="stylesheet" href="<?php echo url('assets/css/reports-1.css')?>">
<link href="<?php echo url('css/fakeLoader.css')?>" rel="stylesheet">
<style type="text/css">
    #ui-datepicker-div
    {
        z-index:55 !important;
    }
    #fakeLoader{
    display: none;
    top: inherit !important;
    left: inherit !important;
    background-color: rgb(238, 238, 238)!important;
    }
    .spinner2
    {
        left: 40% !important;
        top: 40% !important;
        color: #000;
    } 
    .container1 > div, .container2 > div, .container3 > div {
    background-color: #000 !important;
    }
    .dataTables_wrapper .dataTables_filter input {
    border: 1px;
    padding: 3px;
    }
    .dataTables_wrapper .table thead > tr .sorting_asc
    {
    background: #eeeeee !important;
    }
    .icon{
    font-size: 17px;
    }
</style>
<?php 
$employeeName='';
if (!empty($data['employee'][0]['name']))
    {
        $employeeName=$data['employee'][0]['name'];
    }
 $separator=$data['separator'];
 $employeeBaseUrl=url('employee_profile/'.$data['employee_details']['employee_id']);
 $employeeProfileLink='<a href="'.$employeeBaseUrl.'" style="color:#2392ec">'.$employeeName.'</a>';
?>
<?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    var pageTrans="";
    var ofTrans="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
        pageTrans="Seite ";
        ofTrans=" von ";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
        pageTrans="Page ";
        ofTrans=" of ";
    }
</script>
<script type="text/javascript">
    var employeeName="<?= $employeeName?>";
    var separator='<?php if (!empty($separator['data'])) {echo $separator['data'];} ?>';
</script>

            <!-- <h3>Here starts everything&hellip;</h3> -->
            
            <div class="row reports">
                <div class="col-md-12 link">
                    <p> 
                    <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('listing')?>" ><?php echo trans ('header.Listing')?></a></span> /
                               <a><?php echo trans ('report_links.title')." - ".$employeeProfileLink?></a>
                    </p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="fill">
                        <?php include 'layout/report_links.php' ?>
                    <div class="col-md-12 col-sm-12 col-xs-12 second">
                    <div class="fakeloader" id="fakeLoader" ></div>
                        <div class="col-md-4 hidden-sm hidden-xs"></div>
                        <div class="col-md-4 col-sm-12 col-xs-12 padresponse">
                        <div class="fill">
                        <form >                       
                        <div class="col-md-12 col-sm-12 col-xs-12 half">
                            <div class="fill">
                                <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.from')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="start_date" class="form-control ">         
                            </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.to')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="end_date" class="form-control">         
                            </div>
                            </div>
                            </div>
                        </div>
                        </form>
                        </div>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs"></div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 third">
                        <div class="col-md-3 col-sm-4 col-xs-12 center">
                            <p class="small_strong"><?php echo trans ('reports.date_range')?>: <span id="header_date_range"></span></p>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-12 center for_activity_select">
                            <p class="small_strong"><?php echo trans ('reports.bal_range')?>: <span id="header_balance_range"></span></p>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-12 center for_activity_select">
                            <p class="small_strong"><?php echo trans ('reports.bal_range_start')?>: <span id="header_balance_range_at_start">0.00</span></p>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-12 center for_activity_select">
                            <p class="small_strong"><?php echo trans ('reports.bal_range_end')?>: <span id="header_balance_range_at_end"> 0.00</span></p>

                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 fourth">
                    <div class="panel-body over">
                    
                    <script type="text/javascript">
                    jQuery(document).ready(function($)
                    {
                        $("#example-1").dataTable({
                            aLengthMenu: [
                                [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
                            ]
                        });
                    });
                    </script>
                    
                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>&nbsp&nbsp&nbsp<?php echo trans ('reports-1.date')?>&nbsp&nbsp&nbsp</th>
                                <th><?php echo trans ('reports-1.total_hours')?></th>
                                <th><?php echo trans ('reports-1.offset')?></th>
                                <th><?php echo trans ('reports-1.balance')?></th>
                                <th style="width: 90px;"><?php echo trans ('reports-1.total_overtime')?></th>
                            </tr>
                        </thead>
                    
                        <tbody id="data_table">
                        </tbody>
                    </table>
                </div>
                </div>
                    </div>
                </div>
            </div>  

  <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo url('js/datepicker-de.js')?>"></script>
    <script>
        function fillArray(value, len) {
        if (len == 0) return [];
        var a = [value+'%'];
        while (a.length * 2 <= len) a = a.concat(a);
        if (a.length < len) a = a.concat(a.slice(0, len - a.length));
        return a;
        }
        function docWidth(docLength){
        if(docLength>0){
        var res =Math.floor(100/docLength);
        return fillArray(res,docLength);
        }else{
        return ['100%'];
        }
        }
     var time_style='<?php if (!empty($data['general_setting']['response']['0']['time_style'])) { echo $data['general_setting']['response']['0']['time_style']; } else { echo ""; } ?>';//time format from general settings
    var value_format='<?php if (!empty($data['general_setting']['response']['0']['value_format'])) { echo $data['general_setting']['response']['0']['value_format']; } else { echo ""; } ?>';//time format from general settings
        $(".left-links li a").click(function(){
        $(this).find('i').toggleClass('fa-indent fa-outdent');
});
var dateToday = '';
var employee_id="<?=$data['employee_details']['employee_id']?>";
var company_id="<?=$data['employee_details']['company_id']?>";
/*console.log(employee_id);
console.log(company_id);*/
var dates = $("#start_date, #end_date").datepicker({
    defaultDate: "+1w",
    firstDay: 1,
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    dateFormat:"dd-mm-yy",
    onSelect: function(selectedDate) {
        var option = this.id == "start_date" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            $.datepicker.regional[languageType];
        dates.not(this).datepicker("option", option, date);
        var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
        localStorage.start_date=start_date;
        localStorage.end_date=end_date;
        if(start_date!=''&&end_date!='')
        {
             $('#fakeLoader').show();
            var res='';
            var table = $('#example-1').DataTable();
            table.destroy();
            $.ajax({
                    url: "<?php echo url('listingTimesheetWithRangeForOvertimeReport/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    success: function(response)
                    {   
                        console.log(response);

                        for(i=0;i<response['result'].length;i++)
                        {
                            console.log(response['result'][i][2]);
                            var day =moment(response['result'][i][0]).format('DD/MM/YYYY');
                            var totalHours=minutesToStr(response['result'][i][1]);
                            var offset=minutesToStr(response['result'][i][2]);
                            var balance=minutesToStr(response['result'][i][3]);
                            var overtime=minutesToStr(response['result'][i][4]);
                            if (time_style=='am/pm') 
                            {
                                if(value_format=='8:15')
                                    {
                                       totalHours=totalHours.replace('.',':'); 
                                       offset=offset.replace('.',':'); 
                                       balance=balance.replace('.',':'); 
                                       overtime=overtime.replace('.',':'); 
                                    }
                                else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                        {
                                             totalHours=totalHours.replace('.',','); 
                                             offset=offset.replace('.',','); 
                                             balance=balance.replace('.',','); 
                                             overtime=overtime.replace('.',','); 
                                        }
                                    }
                            }
                            else if(time_style=='24 hours')
                            {
                                if(value_format=='8:15')
                                    {
                                       totalHours=totalHours.replace('.',':'); 
                                       offset=offset.replace('.',':'); 
                                       balance=balance.replace('.',':'); 
                                       overtime=overtime.replace('.',':'); 
                                    }
                                else if(value_format=="Device region")
                                {
                                    if(separator=="comma")
                                    {
                                        totalHours=totalHours.replace('.',','); 
                                         offset=offset.replace('.',','); 
                                         balance=balance.replace('.',','); 
                                         overtime=overtime.replace('.',','); 
                                    }
                                }
                            }
                            else if(time_style=='Industrial')
                            {
                                totalHours=totalHours.replace('.',':'); 
                                offset=offset.replace('.',':'); 
                                balance=balance.replace('.',':'); 
                                overtime=overtime.replace('.',':'); 
                                totalHours=timeToDecimal(totalHours); 
                                offset=timeToDecimal(offset); 
                                balance=timeToDecimal(balance); 
                                overtime=timeToDecimal(overtime);  
                                if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                            {
                                                totalHours=totalHours.replace('.',','); 
                                                offset=offset.replace('.',','); 
                                                balance=balance.replace('.',','); 
                                                overtime=overtime.replace('.',',');  
                                            }
                                    }
                            }
                            res+='<tr role="row" class="odd"> <td class="tiny sorting_1">'+day+'</td> <td class="tiny">'+totalHours+'</td> <td class="tiny">'+offset+'</td> <td class="tiny">'+balance+'</td> <td class="tiny">'+overtime+'</td> </tr>'
                        }
                            var finalTotalHours=minutesToStr(response['total']['finalTotalHours']);
                            var finalOffset=minutesToStr(response['total']['finalOffset']);
                            var finalBalance=minutesToStr(response['total']['finalBalance']);
                            var balanceAtRangeStart=minutesToStr(response['total']['balanceAtRangeStart']);
                            var balanceAtRangeEnd=minutesToStr(response['total']['balanceAtRangeEnd']);
                             if (time_style=='am/pm') 
                            {
                                if(value_format=='8:15')
                                    {
                                       finalTotalHours=finalTotalHours.replace('.',':'); 
                                       finalOffset=finalOffset.replace('.',':'); 
                                       finalBalance=finalBalance.replace('.',':'); 
                                       balanceAtRangeStart=balanceAtRangeStart.replace('.',':'); 
                                       balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                                    }
                                else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                        {
                                                finalTotalHours=finalTotalHours.replace('.',','); 
                                               finalOffset=finalOffset.replace('.',','); 
                                               finalBalance=finalBalance.replace('.',','); 
                                               balanceAtRangeStart=balanceAtRangeStart.replace('.',','); 
                                               balanceAtRangeEnd=balanceAtRangeEnd.replace('.',','); 
                                        }
                                    }
                            }
                            else if(time_style=='24 hours')
                            {
                                if(value_format=='8:15')
                                    {
                                      finalTotalHours=finalTotalHours.replace('.',':'); 
                                       finalOffset=finalOffset.replace('.',':'); 
                                       finalBalance=finalBalance.replace('.',':'); 
                                       balanceAtRangeStart=balanceAtRangeStart.replace('.',':'); 
                                       balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                                    }
                                else if(value_format=="Device region")
                                {
                                    if(separator=="comma")
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',','); 
                                               finalOffset=finalOffset.replace('.',','); 
                                               finalBalance=finalBalance.replace('.',','); 
                                               balanceAtRangeStart=balanceAtRangeStart.replace('.',','); 
                                               balanceAtRangeEnd=balanceAtRangeEnd.replace('.',','); 
                                    }
                                }
                            }
                             else if(time_style=='Industrial')
                            {
                                  finalTotalHours=finalTotalHours.replace('.',':'); 
                                       finalOffset=finalOffset.replace('.',':'); 
                                       finalBalance=finalBalance.replace('.',':'); 
                                       balanceAtRangeStart=balanceAtRangeStart.replace('.',':'); 
                                       balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                                         finalTotalHours=timeToDecimal(finalTotalHours);
                                       finalOffset=timeToDecimal(finalOffset);
                                       finalBalance=timeToDecimal(finalBalance);
                                       balanceAtRangeStart=timeToDecimal(balanceAtRangeStart);
                                       balanceAtRangeEnd=timeToDecimal(balanceAtRangeEnd);
                                if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                            {
                                               finalTotalHours=finalTotalHours.replace('.',','); 
                                               finalOffset=finalOffset.replace('.',','); 
                                               finalBalance=finalBalance.replace('.',','); 
                                               balanceAtRangeStart=balanceAtRangeStart.replace('.',','); 
                                               balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');  
                                            }
                                    }
                            }
                            res+='<tr role="row" class="odd"> <td class="tiny sorting_1 total_values"><?php echo trans ('reports.total')?></td></td> <td class="tiny total_values">'+finalTotalHours+'</td> <td class="tiny total_values">'+finalOffset+'</td> <td class="tiny total_values">'+finalBalance+'</td> <td class="tiny total_values"></td> </tr>';
                            $('#header_date_range').html( '&nbsp'+start_date+'&nbsp' +'<?php echo trans ('report_links.to')?>'+ '&nbsp'+end_date+'&nbsp');
                        var message="<?php echo trans ('reports.date_range')?>: "+start_date+" <?php echo trans ('report_links.to')?> "+end_date;
                        var message1="<?php echo trans ('reports.bal_range')?>: "+finalBalance;
                        var message2="<?php echo trans ('reports.bal_range_start')?>: "+balanceAtRangeStart;
                        var message3="<?php echo trans ('reports.bal_range_end')?>: "+balanceAtRangeEnd;
                        var titleOf=toTitleCase("<?php echo trans ('report_links.one_line_one')?> <?php echo trans ('report_links.one_line_two')?>");
                        var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
                        $('#header_balance_range').html(finalBalance);
                        $('#header_balance_range_at_start').html(balanceAtRangeStart);
                        $('#header_balance_range_at_end').html(balanceAtRangeEnd);
                        $('#data_table').html(res);
                       /* $('#data_table_total').html(res1);*/
                         $(document).ready(function() {
                        $('#example-1').DataTable( {
                            language: {
                        "url":languageUrl,
                        },
                        dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: 'Overtime report'
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                            doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                             doc.content[2].layout = 'noBorders';
                             doc.content[2].margin =[0, 0, 0, 10];
                             doc.styles.tableHeader.fontSize = 7;
                             doc['footer']=(function(page, pages) {
                                                return {
                                                columns: ['',
                                                {
                                                alignment: 'right',
                                                text: [pageTrans,
                                                { text: page.toString(), italics: true },
                                                ofTrans,
                                                { text: pages.toString(), italics: true }
                                                ]
                                                }
                                                ],
                                                margin: [10, 0]
                                                }
                                            });
                             doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }

                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf
                        }
                        ],
                        "bSort": false,
                        } );
                        } );
                        $('#fakeLoader').hide();
                    }
                }); 
        }
    }
});
$(window).load(function(){
    var today = new Date();
var dayOfWeekStartingSundayZeroIndexBased = today.getDay(); // 0 : Sunday ,1 : Monday,2,3,4,5,6 : Saturday
var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()-6);
var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+7);
var start_date1 =moment(mondayOfWeek).format('D-M-YYYY');
var end_date1 =moment(sundayOfWeek).format('D-M-YYYY');

if (typeof(localStorage.start_date)=='undefined') { localStorage.start_date=start_date1}
if (typeof(localStorage.end_date)=='undefined') { localStorage.end_date=end_date1}
$('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
$('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));
start_date1 =localStorage.getItem("start_date");
end_date1 =localStorage.getItem("end_date");

if(start_date1!=''&&end_date1!='')
        {
            var res='';
            var table = $('#example-1').DataTable();
            table.destroy();
            console.log(start_date1);
            console.log(end_date1);
              $.ajax({
                    url: "<?php echo url('listingTimesheetWithRangeForOvertimeReport/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                    success: function(response)
                    {   
                        console.log(response);
                        for(i=0;i<response['result'].length;i++)
                        {
                            var day =moment(response['result'][i][0]).format('DD/MM/YYYY');
                            var totalHours=minutesToStr(response['result'][i][1]);
                            var offset=minutesToStr(response['result'][i][2]);
                            var balance=minutesToStr(response['result'][i][3]);
                            var overtime=minutesToStr(response['result'][i][4]);
                             if (time_style=='am/pm') 
                            {
                                if(value_format=='8:15')
                                    {
                                       totalHours=totalHours.replace('.',':'); 
                                       offset=offset.replace('.',':'); 
                                       balance=balance.replace('.',':'); 
                                       overtime=overtime.replace('.',':'); 
                                    }
                                else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                        {
                                             totalHours=totalHours.replace('.',','); 
                                             offset=offset.replace('.',','); 
                                             balance=balance.replace('.',','); 
                                             overtime=overtime.replace('.',','); 
                                        }
                                    }
                            }
                            else if(time_style=='24 hours')
                            {
                                if(value_format=='8:15')
                                    {
                                       totalHours=totalHours.replace('.',':'); 
                                       offset=offset.replace('.',':'); 
                                       balance=balance.replace('.',':'); 
                                       overtime=overtime.replace('.',':'); 
                                    }
                                else if(value_format=="Device region")
                                {
                                    if(separator=="comma")
                                    {
                                        totalHours=totalHours.replace('.',','); 
                                         offset=offset.replace('.',','); 
                                         balance=balance.replace('.',','); 
                                         overtime=overtime.replace('.',','); 
                                    }
                                }
                            }
                            else if(time_style=='Industrial')
                            {
                                totalHours=totalHours.replace('.',':'); 
                                offset=offset.replace('.',':'); 
                                balance=balance.replace('.',':'); 
                                overtime=overtime.replace('.',':'); 
                                totalHours=timeToDecimal(totalHours); 
                                offset=timeToDecimal(offset); 
                                balance=timeToDecimal(balance); 
                                overtime=timeToDecimal(overtime);  
                                if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                            {
                                                totalHours=totalHours.replace('.',','); 
                                                offset=offset.replace('.',','); 
                                                balance=balance.replace('.',','); 
                                                overtime=overtime.replace('.',',');  
                                            }
                                    }
                            }
                            res+='<tr role="row" class="odd"> <td class="tiny sorting_1">'+day+'</td> <td class="tiny">'+totalHours+'</td> <td class="tiny">'+offset+'</td> <td class="tiny">'+balance+'</td> <td class="tiny">'+overtime+'</td> </tr>'
                        }
                            var finalTotalHours=minutesToStr(response['total']['finalTotalHours']);
                            var finalOffset=minutesToStr(response['total']['finalOffset']);
                            var finalBalance=minutesToStr(response['total']['finalBalance']);
                            var balanceAtRangeStart=minutesToStr(response['total']['balanceAtRangeStart']);
                            var balanceAtRangeEnd=minutesToStr(response['total']['balanceAtRangeEnd']);
                             if (time_style=='am/pm') 
                            {
                                if(value_format=='8:15')
                                    {
                                       finalTotalHours=finalTotalHours.replace('.',':'); 
                                       finalOffset=finalOffset.replace('.',':'); 
                                       finalBalance=finalBalance.replace('.',':'); 
                                       balanceAtRangeStart=balanceAtRangeStart.replace('.',':'); 
                                       balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                                    }
                                else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                        {
                                                finalTotalHours=finalTotalHours.replace('.',','); 
                                               finalOffset=finalOffset.replace('.',','); 
                                               finalBalance=finalBalance.replace('.',','); 
                                               balanceAtRangeStart=balanceAtRangeStart.replace('.',','); 
                                               balanceAtRangeEnd=balanceAtRangeEnd.replace('.',','); 
                                        }
                                    }
                            }
                            else if(time_style=='24 hours')
                            {
                                if(value_format=='8:15')
                                    {
                                      finalTotalHours=finalTotalHours.replace('.',':'); 
                                       finalOffset=finalOffset.replace('.',':'); 
                                       finalBalance=finalBalance.replace('.',':'); 
                                       balanceAtRangeStart=balanceAtRangeStart.replace('.',':'); 
                                       balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                                    }
                                else if(value_format=="Device region")
                                {
                                    if(separator=="comma")
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',','); 
                                               finalOffset=finalOffset.replace('.',','); 
                                               finalBalance=finalBalance.replace('.',','); 
                                               balanceAtRangeStart=balanceAtRangeStart.replace('.',','); 
                                               balanceAtRangeEnd=balanceAtRangeEnd.replace('.',','); 
                                    }
                                }
                            }
                            else if(time_style=='Industrial')
                            {
                                  finalTotalHours=finalTotalHours.replace('.',':'); 
                                       finalOffset=finalOffset.replace('.',':'); 
                                       finalBalance=finalBalance.replace('.',':'); 
                                       balanceAtRangeStart=balanceAtRangeStart.replace('.',':'); 
                                       balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                                         finalTotalHours=timeToDecimal(finalTotalHours);
                                       finalOffset=timeToDecimal(finalOffset);
                                       finalBalance=timeToDecimal(finalBalance);
                                       balanceAtRangeStart=timeToDecimal(balanceAtRangeStart);
                                       balanceAtRangeEnd=timeToDecimal(balanceAtRangeEnd);
                                if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                            {
                                               finalTotalHours=finalTotalHours.replace('.',','); 
                                               finalOffset=finalOffset.replace('.',','); 
                                               finalBalance=finalBalance.replace('.',','); 
                                               balanceAtRangeStart=balanceAtRangeStart.replace('.',','); 
                                               balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');  
                                            }
                                    }
                            }
                            res+='<tr role="row" class="odd"> <td class="tiny sorting_1 total_values"><?php echo trans ('reports.total')?></td></td> <td class="tiny total_values">'+finalTotalHours+'</td> <td class="tiny total_values">'+finalOffset+'</td> <td class="tiny total_values">'+finalBalance+'</td> <td class="tiny total_values"></td> </tr>';
                        $('#data_table').html(res);
                        $('#header_date_range').html( '&nbsp'+start_date1+'&nbsp' +'<?php echo trans ('report_links.to')?>'+ '&nbsp'+end_date1+'&nbsp');
                        var message="<?php echo trans ('reports.date_range')?>: "+start_date1+" <?php echo trans ('report_links.to')?> "+end_date1;
                        var message1="<?php echo trans ('reports.bal_range')?>: "+finalBalance;
                        var message2="<?php echo trans ('reports.bal_range_start')?>: "+balanceAtRangeStart;
                        var message3="<?php echo trans ('reports.bal_range_end')?>: "+balanceAtRangeEnd;
                        var titleOf=toTitleCase("<?php echo trans ('report_links.one_line_one')?> <?php echo trans ('report_links.one_line_two')?>");
                        var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
                        $('#header_balance_range').html(finalBalance);
                        $('#header_balance_range_at_start').html(balanceAtRangeStart);
                        $('#header_balance_range_at_end').html(balanceAtRangeEnd);
                       $(document).ready(function() {
                        $('#example-1').DataTable( {
                            language: {
                        "url":languageUrl,
                        },
                        dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        title: titleOf
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        orientation: 'portrait',
                        pageSize: 'A4',
                        customize: function ( doc ) {
                           doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                             doc.content[2].layout = 'noBorders';
                             doc.content[2].margin =[0, 0, 0, 10];
                             doc.styles.tableHeader.fontSize = 7;
                             doc['footer']=(function(page, pages) {
                                                return {
                                                columns: ['',
                                                {
                                                alignment: 'right',
                                                text: [pageTrans,
                                                { text: page.toString(), italics: true },
                                                ofTrans,
                                                { text: pages.toString(), italics: true }
                                                ]
                                                }
                                                ],
                                                margin: [10, 0]
                                                }
                                            });
                             doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: 40,
                                height: 30
                                } );
                        }
                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        title: titleOf
                        }
                        ],
                        "bSort": false,
                        } );
                        } );
                        $('#fakeLoader').hide();
                    }
                }); 
        }
});
    //date format dd-mm-yyyy
/*    function format(inputDate) {
        var date = new Date(inputDate);
        if (!isNaN(date.getTime())) {
            var day = date.getDate().toString();
            var month = (date.getMonth() + 1).toString();
            // Months use 0 index.

            return (day[1] ? day : '0' + day[0])+ '-' +
               (month[1] ? month : '0' + month[0])  + '-' + 
               date.getFullYear();
        }
    }
 
 */
     //minutes to hour format function
     function minutesToStr(minutes) {
            var sign ='';
            if(minutes < 0){
            sign = '-';
            }

            var hours = Math.floor(Math.abs(minutes) / 60);
            var minutes = leftPad(Math.abs(minutes) % 60);

            return sign + hours +'.'+minutes;

    }
    /*
    * add zero to numbers less than 10,Eg: 2 -> 02
    */
    function leftPad(number) {  
         return ((number < 10 && number >= 0) ? '0' : '') + number;
    }
 //60th to 100th unit for time format industrial
   function timeToDecimal(time)
        {
              substring = "-";
          checkNegative=(time.indexOf(substring) !== -1);
          if(checkNegative==true)
          {
          time=time.replace('-','');
             Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return '-'+parseFloat(Hours + Minutes/60,10).toFixed(2);
           }
           else
           {
            Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return parseFloat(Hours + Minutes/60).toFixed(2);
           }
        }

        //change caps to title case
        function toTitleCase(str)
        {
             return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }
    </script>
<script src="<?php echo url('js/fakeLoader.min.js')?>"></script>
<script type="text/javascript">
$(".fakeloader").fakeLoader({
timeToHide:15000, //Time in milliseconds for fakeLoader disappear
zIndex:"999",//Default zIndex
spinner:"spinner2",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
bgColor:"#2392ec", //Hex, RGB or RGBA colors
});
</script>
<!-- Data Table Scripts -->
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
 <script type="text/javascript" src="<?php echo url('js/buttons.html5.min.js')?>"></script>
 <script type="text/javascript" src="<?php echo url('js/pdfmake.min.js')?>"></script>
 <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo url('assets/js/timepicker/bootstrap-timepicker.min.js')?>"></script>

    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
    
 <?php  include 'layout/footer.php';?>


