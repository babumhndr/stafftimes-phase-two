<?php  include 'layout/header.php';?>
<?php
$user=Auth::user();
$id=$user['_id'];
?>
<?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
    }
</script>
	<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<style>
	.reportMain{
		width: 100%;
		float: left;
	}
    .fullWeekDisplay {
        width: 100%;
        float: left;
    }
    .headerParagraph {
        width: 190px;
        margin-left: auto;
        margin-right: auto;
        color: #000;
    }

	.reportMainSub{
		width:14.28%;
		float:left;
		text-align:center;
        padding:0 3px;
	}

    .reportMainSublast{
        width:20%;
        float:left;
        text-align:center;
       
    }

	.reportInner{
		width: 100%;
		float: left;
        background: #f3f3f3;
        border-radius: 9px;
	}

	.reportInnerblue{
		background-color: blue;
		border-radius: 12px;
        color: #fff;
	}

	.reportInneryellow{
		background-color: yellow;
		border-radius: 12px;
         color: #fff;
	}

	.reportInnergreen{
		background-color: green;
		border-radius: 12px;
         color: #fff;
	}

	.reportInnerred{
		background-color: red;
		border-radius: 12px;
         color: #fff;
	}

    .div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }

    .pannerdash{
        width: 100%;
        float: left;
        padding:10px;
    }

    body{
        background:#f1f1f1; 
    }

    .heading{
        background:#f6f8f7;
    }

    .topbar{
        margin-top: 30px;
        background: #fff;
        margin-bottom: 20px;
    }

    .topbarinner{
        padding-top: 20px;
        padding-bottom: 15px;
    }

    table.dataTable tbody td{
        padding:25px 10px;
        text-align:center;
    }

    th{
        text-align:center;
    }

    table.dataTable thead th{
        padding:15px 18px;
        border-bottom: none;
    }

    .reportInnerday{
        padding-top: 5px;
        font-size: 12px;
    }

    .reportInnerdate{
        padding-top: 5px;
        padding-bottom:5px;
        font-size: 12px;
    }

    table.dataTable.no-footer{
        border-bottom: none;
    }

    input[type=search]{
        -webkit-appearance: textfield;
    }
    .second {
        padding-left: 0px;
        padding-right: 0px;
        background: #fff;
        padding-top: 5px;
        padding-bottom: 5px;
        box-shadow: 2px 2px 3px #cdcdcd;
        margin-bottom: 15px;
     }
     .input_label {
    padding-top: 5px;
    float: left;
    padding-right: 2px;
    }
    .form-control {
    color: #000;
}
#ui-datepicker-div
    {
        z-index:55 !important;
    }
    .fourth {
        padding-left: 0px;
        padding-right: 0px;
    }
    .over {
        padding-left: 0px;
        padding-right: 0px;
    }
    ::-webkit-scrollbar {
    width: 6px;
    height: 8px;
}
    .name_link {
        color: #2392EC;
    }

    @media only screen and (min-width: 993px) and (max-width: 1200px){
        .hidden_blank {
            display: none;
        }
        .statsrespad {
            width: 100%;
        }
    } 

</style>
</head>
<body>

<div class="pannerdash">
<div class="col-md-12 col-sm-12 col-xs-12 second">
                    <div class="fakeloader" id="fakeLoader" ></div>
                        <div class="col-md-3 hidden-sm hidden-xs hidden_blank"></div>
                        <div class="col-md-5 col-sm-12 col-xs-12 statsrespad">
                        <div class="fill" style=" text-align: center; ">
                        <form role="form" action="#">                       
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="fill">
                                <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.from')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="start_date" class="form-control ">         
                            </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 half">
                            <span class="input_label"><?php echo trans ('report_links.to')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="end_date" class="form-control">         
                            </div>
                            </div>
                            </div>
                        </div>
                        </form>
                        </div>
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs hidden_blank"></div>
                    </div>


    <div class="col-md-12 col-sm-12 col-xs-12 fourth">
        <div class="panel-body table-wrapper">        
            

        <table id="example" class="cell-border order-column" cellspacing="0" width="100%">
            <thead id="table-head">
                <tr class="heading" >
                   <th>Team Member</th>
                    <th>Week1</th>
                    <th>Week2</th>
                    <th>Week3</th>
                    <th>Week4</th>
                    <th>Week5</th>
                </tr>
            </thead>
       <!--  <tfoot>
           <tr>
               <th>Name</th>
               <th>Position</th>
               <th>Office</th>
               <th>Age</th>
               <th>Start date</th>
               <th>Salary</th>
           </tr>
       </tfoot> -->
            <tbody id="employee_listing">
                
            </tbody>
        </table>
    </div>
</div>
</div>
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
  <!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
   <script type="text/javascript" src="<?php echo url('js/datepicker-de.js')?>"></script>
 <script>
 function getDateWithDay(date){
    var myDate = moment(date).format('D');
    return myDate;
 }
 function getDayForDate(date){
    var dayName = moment(date).format('dddd');
    var res = dayName.charAt(0);
    return res;
 }
/* function removeDublicatesInArray(arr) {
  var uniqueArray = new Array();
  return uniqueArray = arr.filter((elem, index, self) => self.findIndex((obj) => { return obj.date === elem.date }) === index)
 };*/
/* function removeDublicatesInArray(arr) {
    var uniqueArray = new Array();
 uniqueArray = arr.filter(function(elem, index, self) {
    return index === self.indexOf(function(obj){return obj.date===elem.date});
  });
  return uniqueArray;
};*/
function removeDublicatesInArray(originalArray, prop) {
     var newArray = [];
     var lookupObject  = {};

     for(var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
     }

     for(i in lookupObject) {
         newArray.push(lookupObject[i]);
     }
      return newArray;
 }






    var dateToday = '';
    var company_id='';
    $( window ).load(function() {
        var today = new Date();
var dayOfWeekStartingSundayZeroIndexBased = today.getDay(); // 0 : Sunday ,1 : Monday,2,3,4,5,6 : Saturday
var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()-6);
var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+7);
var start_date1 =moment(mondayOfWeek).format('D-M-YYYY');
var end_date1 =moment(sundayOfWeek).format('DD-MM-YYYY');
if (typeof(localStorage.start_date)=='undefined') { localStorage.start_date=start_date1}
localStorage.end_date=end_date1
$('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
$('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));
start_date1 =localStorage.getItem("start_date");
end_date1 =localStorage.getItem("end_date");
//console.log({mondayOfWeek:mondayOfWeek, sundayOfWeek:sundayOfWeek})
if(start_date1!=''&&end_date1!='')
        {
        var url="<?php echo url('getPlannerData')?>";
        var company_id="<?php echo $id?>"
        url=url+"/"+company_id+"/"+start_date1+"/"+end_date1;
        console.log(url);
        hitApi(url);
        }
    });

    function hitApi(url){  
    var table = $('#example').DataTable();
    table.destroy();
        $.ajax({
            url:url,
             type:'post',
                   success: function(response)
                    {
                        var res1='';
                        var res2='';
                        var res3='';
                        var range_detail=response['range_details'];
                        var employee_detail=response['planner_details'];
                        console.log({response:response})
                         console.log(range_detail);
                        if(response!=null || response!=''|| typeof(response)!==undefined)
                        {
                            for(var k = 0; k < employee_detail.length; k++) {
                                if (employee_detail[k]['employee_detail'][0]!=null) {
                                    for (var i = 0; i < range_detail.length; i++) {
                                        div='';
                                        for (var j = 0; j < range_detail[i]['datesInAWeek'].length; j++) {
                                            dayName=getDayForDate(range_detail[i]['datesInAWeek'][j]);
                                            dayDate=range_detail[i]['datesInAWeek'][j];
                                            //console.log(range_detail[i]['datesInAWeek'][j]);
                                            div+='<div class="reportMainSub" ><div class="reportInner" id="'+employee_detail[k]['employee_detail'][0]['_id']+dayDate+'"><div class="reportInnerday">'+dayName+'</div><div class="reportInnerdate">'+getDateWithDay(range_detail[i]['datesInAWeek'][j])+'</div></div></div>';
                                        }
                                        res2+='<td><div class="fullWeekDisplay">'+div+'</div></td>';
                                    }
                                    res3+="<tr><th><a class='name_link' href='reports_1/"+employee_detail[k]['employee_detail'][0]['_id']+"'>"+employee_detail[k]['employee_detail'][0]['name']+"</a></th>"+res2+"</tr>";
                                    res2='';
                                }
                            }
                            for (var i = 0; i < range_detail.length; i++) {
                                // var monthTrans = range_detail[i]['month_name'];
                                var monthTrans=range_detail[i]['month_name'];
                                if (monthTrans=="Jan") {
                                    monthTrans = "<?php echo trans('translate.Jan')?>";
                                }else if (monthTrans=="Feb") {
                                    monthTrans = "<?php echo trans('translate.Feb')?>";
                                }else if (monthTrans=="Mar") {
                                    monthTrans = "<?php echo trans('translate.Mar')?>";
                                }else if (monthTrans=="Apr") {
                                    monthTrans = "<?php echo trans('translate.Apr')?>";
                                }else if (monthTrans=="May") {
                                    monthTrans = "<?php echo trans('translate.May')?>";
                                }else if (monthTrans=="Jun") {
                                    monthTrans = "<?php echo trans('translate.Jun')?>";
                                }else if (monthTrans=="Jul") {
                                    monthTrans = "<?php echo trans('translate.Jul')?>";
                                }else if (monthTrans=="Aug") {
                                    monthTrans = "<?php echo trans('translate.Aug')?>";
                                }else if (monthTrans=="Sep") {
                                    monthTrans = "<?php echo trans('translate.Sep')?>";
                                }else if (monthTrans=="Oct") {
                                    monthTrans = "<?php echo trans('translate.Oct')?>";
                                }else if (monthTrans=="Nov") {
                                    monthTrans = "<?php echo trans('translate.Nov')?>";
                                }else if (monthTrans=="Dec") {
                                    monthTrans = "<?php echo trans('translate.Dec')?>";
                                }else
                                    monthTrans =range_detail[i]['month_name'];

                                res1+="<th><p class='headerParagraph'>"+monthTrans+" : <?php echo trans('translate.week')?> "+range_detail[i]['week_number']+"</p></th>";
                            }
                        }
                        $('#table-head').html("<tr><th style='background-color:#F1F1F1;'><p class='headerParagraph'><?php echo trans('translate.team_member')?></p></th>"+res1+"</tr>");
                        $('#employee_listing').html(res3);
                        var defaultColor ="#2392EC";
                        for(var i = 0; i < employee_detail.length; i++) {
                                if (employee_detail[i]['timesheet_detail']!=null) {
                                   var newArray=removeDublicatesInArray(employee_detail[i]['timesheet_detail'],'date');
                                   console.log({newArray:newArray,J:j})
                                    for(var j = 0; j < newArray.length; j++) {
                                        empID = newArray[j]['employee_id'];
                                        createdDate = newArray[j]['date'];
                                        if(newArray[j]['timesheet_activity']!=undefined || newArray[j]['timesheet_activity']!=null ) {
                                            // console.log({defaultColor:defaultColor,color:newArray[j]['activity_color']})
                                           if(newArray[j]['timesheet_activity']['activity_color']!=undefined || newArray[j]['timesheet_activity']['activity_color']!=null ) {
                                            // console.log({defaultColor:defaultColor,color:newArray[j]['activity_color']})
                                            $('#'+empID+createdDate).css('background-color',newArray[j]['timesheet_activity']['activity_color']);
                                            }
                                            else {
                                                 // console.log({defaultColor:defaultColor, abc:'abc'})
                                                $('#'+empID+createdDate).css('background-color',defaultColor);
                                            }
                                        }
                                        else {
                                             // console.log({defaultColor:defaultColor, abc:'abc'})
                                            $('#'+empID+createdDate).css('background-color',defaultColor);
                                        }
                                        // console.log(color,'color');
                                    }
                                }
                            }
                        $('#example').DataTable( {
                            language: {
                                
                        "url":languageUrl,
                        },
                            "paging": true,
                            "searching": true,
                            "ordering": true,
                            "scrollX": true,
                            "autoWidth" :false,
                           /* dom: 'Bfrtip',
                            buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'
                            ],*/
                            "bSort": false,
                            fixedColumns :  {
                                leftColumns: 1
                            }
                        });
                        // $('#').html(res2);
                        //console.log({res1:res1,res3:res3});
                    }//table-head <th>Team Member</th>
            });
        }

    var dates = $("#start_date, #end_date").datepicker({
    defaultDate: "+1w",
    firstDay: 1,
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    dateFormat:"dd-mm-yy",
    onSelect: function(selectedDate) {
        var option = this.id == "start_date" ? "minDate" : "maxDate",
        instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            $.datepicker.regional[languageType];
        dates.not(this).datepicker("option", option, date);
        var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
        localStorage.start_date=start_date;
        localStorage.end_date=end_date;
       // console.log(start_date+'//'+end_date+'//');
       if(start_date!=''&&end_date!='')
        {
            $('#fakeLoader').show();
             var url="<?php echo url('getPlannerData')?>";
             var company_id="<?php echo $id?>"
             url=url+"/"+company_id+"/"+start_date+"/"+end_date;
             console.log(url);
            hitApi(url);
        }
    }
});
 </script>

<script>
//     $(document).ready(function() {
//     $('#example').DataTable({
//     });
// } );
</script>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>
 <!--<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>-->
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
 <script type="text/javascript" src="<?php echo url('js/buttons.html5.min.js')?>"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js"></script>
 <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<?php include 'layout/footer.php';?>