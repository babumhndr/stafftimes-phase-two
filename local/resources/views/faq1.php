<?php 
$metaTitle=$faqData['metaTag']['title'];
$metaUrl=$faqData['metaTag']['url'];
$metaDescription=$faqData['metaTag']['description'];
$metaKeyword=$faqData['metaTag']['keyword'];
?>
<?php  include 'layout/main_page_header.php';?>
<?php $data=$faqData['faqResult'];?>
<link rel="stylesheet" type="text/css" href="<?php echo url('css/faq.css')?>">   
<style>
.accordion_container{
    width: 100%;
    float: left;
    margin-bottom: 25px;
}
.accordion_head {
        background: #FAFAFA;
    color: #666 !important;
    cursor: pointer;
    font-family: arial;
    font-size: 15px;
    margin: 0 0 1px 0;
    padding: 11px 9px;
    font-weight: 600;
    border: 1px solid #F1F1F1;
}
.accordion_body {
      padding: 20px;
    overflow: auto;
    border: 1px solid #F1F1F1;
    border-top: none;
    margin-bottom: 3px;
    color: #777;
}
.accordion_body p{
padding: 5px;
 margin: 0px;
}
 
.plusminus{
 float:right;
}
.horizontal_line{
        background: #f3f4f5;
    width: 100%;
    height: 1px;
    float: left;
    margin-top: -5px;
}
.header_statement {
    font-size: 21px;
    text-align: center;
    color: #000;
    padding: 25px;
    margin-top: 40px;
    margin-bottom: 10px;
}
.contactus_link
{
color: #0072ff;
}
.contactus_link:hover
{
    color: #00a1ff;
    text-decoration: underline;
}
</style>
<script>
$(document).ready(function(){
 $(".accordion_body").hide();
 $(".accordion_head:first-child").next(".accordion_body").slideToggle(0); 
 //toggle the componenet with class accordion_body
 $(".accordion_head").click(function(){
  if ($('.accordion_body').is(':visible')) {
   $(".accordion_body").slideUp(600);
   $(".plusminus").text('+');
  }
  $(this).next(".accordion_body").slideDown(600); 
  $(this).children(".plusminus").text('-');
 });
});
</script>
    </head>
 <div class="container">
<?php
$langType=trans('translate.lang');
if ($langType =="en") {
  for($i=0;$i<sizeof($data);$i++) {
    if ($i==6) {
   echo '<p style="font-size: 20px;color: #000; margin-top:20px;">'.$data[$i]['topic_en'].'</p>
 <div class="accordion_container">
  <div class="accordion_head" style="border-top-left-radius: 5px;border-top-right-radius: 5px;">'.$data[$i]["faqdetails"][0]['question_en'].'<span class="plusminus">-</span></div>
   <div class="accordion_body" style="display: none;">
   <p>'.$data[$i]["faqdetails"][0]['answer_en'].'</p>
   </div>';
   for($j=1;$j<sizeof($data[$i]['faqdetails']);$j++) {
      echo '<div class="accordion_head">'.$data[$i]["faqdetails"][$j]['question_en'].'<span class="plusminus">+</span></div>
   <div class="accordion_body" style="display: none;">
   <p>'.$data[$i]["faqdetails"][$j]['answer_en'].'</p>
   </div>
    ';
   }
 }
 if($i==7) {
  echo ' <div class="accordion_container">
    <p style="font-size: 20px;color: #000;margin-top: 10px;">'.$data[$i]['topic_en'].'</p>';
      for ($j=0; $j <sizeof($data[$i]['faqdetails']) ; $j++) { 
        echo '<div class="accordion_head">'.$data[$i]["faqdetails"][$j]['question_en'].'<span class="plusminus">+</span></div>
        <div class="accordion_body" style="display: none;">
        <p>'.$data[$i]["faqdetails"][$j]['answer_en'].'</p>
        </div>';
       }
       echo '</div>';
 }
}
   echo '</div>';
}
 else
 {
       for($i=0;$i<sizeof($data);$i++) {
    if ($i==6) {
   echo '<p style="font-size: 20px;color: #000; margin-top:20px;">'.$data[$i]['topic_de'].'</p>
 <div class="accordion_container">
  <div class="accordion_head" style="border-top-left-radius: 5px;border-top-right-radius: 5px;">'.$data[$i]["faqdetails"][0]['question_de'].'<span class="plusminus">-</span></div>
   <div class="accordion_body" style="display: none;">
   <p>'.$data[$i]["faqdetails"][0]['answer_de'].'</p>
   </div>';
   for($j=1;$j<sizeof($data[$i]['faqdetails']);$j++) {
      echo '<div class="accordion_head">'.$data[$i]["faqdetails"][$j]['question_de'].'<span class="plusminus">+</span></div>
   <div class="accordion_body" style="display: none;">
   <p>'.$data[$i]["faqdetails"][$j]['answer_de'].'</p>
   </div>
    ';
   }
 }
 if($i==7) {
  echo ' <div class="accordion_container">
    <p style="font-size: 20px;color: #000;margin-top: 10px;">'.$data[$i]['topic_de'].'</p>';
      for ($j=0; $j <sizeof($data[$i]['faqdetails']) ; $j++) { 
        echo '<div class="accordion_head">'.$data[$i]["faqdetails"][$j]['question_de'].'<span class="plusminus">+</span></div>
        <div class="accordion_body" style="display: none;">
        <p>'.$data[$i]["faqdetails"][$j]['answer_de'].'</p>
        </div>';
       }
       echo '</div>';
 }
}
   echo '</div>';
 }
  ?>