
<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/add_employee.css')?>">
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row invite">
			
				<div class="col-md-12 col-sm-12 col-xs-12 content">
                         <div class="panel" style="min-height: 200px;">
                              <div class="row">
                              	<div class="col-md-10">
                              		<h3>Add a new group</h3>
                              		<p>You can manage all your group (projects / operations / activity types) & assignment of people in this section</p>
                              	</div>
                                   <div class="col-md-2">
                                        <br>
                                        <a href="add_company"><button class="white_btn">View all groups</button></a>
                                   </div>
                              </div>
                         	<div class="row" style="  margin: 0px;">
                                   <div class="col-md-6">
                                        <label>Group</label>
                                        <input type="text" class="invite_mail_id" placeholder="Enter group name"></input>
                                   </div>
                                   <div class="col-md-3">
                                        <label>From Date</label>
                                        <div class="input-group">
                                           <input type="date" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status">
                                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                         </div>
                                   </div>
                                   <div class="col-md-3">
                                        <label>To Date</label>
                                        <div class="input-group">
                                           <input type="date" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status">
                                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                         </div>
                                   </div>
                                       
                              </div>
                              
                         
                         </div>
                          <br>

                          <div class="col-sm-12 pad0 manageMaindiv">
                               <h4 class="managebilltext">if this group's activities are billable, please add customer & billing info</h4>
                               <div class="col-sm-6 paddingleft">
                                    <div class="form-group ">
                                     <label for="usr">Customer name</label>
                                     <input type="text" class="form-control managebillinput" placeholder="Enter name (optional)">
                                   </div>
                               </div>
                               <div class="col-sm-6 paddingright">
                                    <div class="form-group ">
                                     <label for="usr">Bill number(s)</label>
                                     <input type="text" class="form-control managebillinput" placeholder="enter bill numbers seperated by comma (optional)">
                                   </div>
                               </div>
                          </div>

                          <div class="col-sm-12 pad0 manageAddPpl">
                               <h5>Add people who will be involved in this group</h5>
                               <div class="col-md-6"></div>
                          </div>

                    <div class="row">
                         <div class="col-md-6 ">
                              <h3>All Groups(5)</h3>
                         </div>
                         <table class="table table-striped">
                              <thead style="background: #edf7ff">
                                   <th>Sl No</th>
                                   <th>ROLE NAME</th>
                                   <th>NO. OF STAFF</th>
                                   <th colspan="2">ACTIONS</th>
                              </thead>
                              <tr>
                                   <td>2</td>
                                   <td> Supervisor</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>
                              <tr>
                                   <td>3</td>
                                   <td>Finance Head</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>
                              <tr>
                                   <td>4</td>
                                   <td>Billing Executive</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>
                              <tr>
                                   <td>5</td>
                                   <td>Cleaning Boy</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>

                             
                              
                         </table>
                    </div>
				</div>
                   
		  </div>
				
				

</div>

	
		
		
	</div>

 <script src="js/jquery.form.js"></script>


 <?php  include 'layout/footer.php';?>


