<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('css/settings.css')?>">
<link rel="stylesheet" href="<?php echo url('css/timedropper.css')?>">
<link rel="stylesheet" href="<?php echo url('js/bx/jquery.bxslider.css')?>">
<link href="<?php echo url('css/jquery.timeentry.css')?>" rel="stylesheet">
<link rel="stylesheet" type='text/css' href="<?php echo url('/css/jquery-ui.css')?>">
<style>
  #ui-datepicker-div
    {
        z-index:99999 !important;
    }
    .custom-header {
        background: #2392ec;
            border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    }
    .custom-content {
        padding: 0px 30px 30px 30px !important;
            border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    }
    .custom-body {
        padding: 20px 0px 5px 0px !important;
    }
    .custom-footer {
    padding: 10px 0px 0px 0px!important;
    text-align: center;
    border-top: 0px;
}
.box {
  width: 100%;
}
@media (min-width: 768px) {
.custom-dialog {
    width: 300px;
    margin: 105px auto;
}
}
</style>
  <?php 
 $condtion= $details['condtion'];
 $working_day_setting=$details['working_day_setting'];
 $general_setting=$details['general_setting'];
 $overtime_handling=$details['overtime_handling'];
 $set_annual_allowance=$details['set_annual_allowance'];
 $separator=$details['separator'];

if (!empty($overtime_handling['response']))
{
if ($overtime_handling['response']['0']['threshold_hours']!=''){
$threshold_hours=$overtime_handling['response']['0']['threshold_hours'];
}
else
{
$threshold_hours="00:00";
}
}
else
{
$threshold_hours="00:00";
}?>
 <script>
 var threshold_hours="<?php echo $threshold_hours; ?>"; 
 var time1='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['time_style'];} ?>';
  var value_format_setting='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['value_format'];} ?>';
  var separator='<?php if (!empty($separator['data'])) {echo $separator['data'];} ?>';
  var offset='<?php if (!empty($general_setting['response'])) {echo $general_setting['response']['0']['set_working_hours'];} ?>';
/*  alert(separator);*/
 </script>  
			<div class="row mot_template">
				<div class="col-md-12 link">
					<p>
            <span class="template_link"> 
					<a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> /
					<a><?php echo trans('settings.settings'); ?></a> 
					</p>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 content">
					<ul class="nav nav-tabs">
						<li id="general_settings_id" class="top_list">
							<a href="#general_settings" class="top_anchor general_setting_btn" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('settings.tooltip_general_settings'); ?></p>"><?php echo trans('settings.general_settings'); ?></span></span>
							</a>
						</li>
						<!-- <li class="top_list">
							<a href="#clear_overtime" class="top_anchor" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Clear Overtime Balance</span>
							</a>
						</li> -->
						<li  id="set_working_id" class="top_list">
							<a href="#set_working" class="top_anchor set_working_btn" data-toggle="tab">
								<span class="visible-xs"><i class="fa-envelope-o"></i></span>
								<span class="hidden-xs"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('settings.tooltip_set_working'); ?></p>"><?php echo trans('settings.working_days'); ?></span></span>
							</a>
						</li>
						<li id="overtime_period_id" class="top_list">
							<a href="#overtime_period" class="top_anchor overtime_handling_btn" data-toggle="tab">
								<span class="visible-xs"><i class="fa-envelope-o"></i></span>
								<span class="hidden-xs"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('settings.tooltip_overtime_handling'); ?></p>"><?php echo trans('settings.handling'); ?></span></span>
							</a>
						</li>
            <li id="set_annual_id" class="top_list">
              <a href="#set_annual" class="top_anchor set_annual_allowance_btn" data-toggle="tab">
                <span class="visible-xs"><i class="fa-envelope-o"></i></span>
                <span class="hidden-xs"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<p class='tool_tip_p'><?php echo trans('settings.tooltip_set_annual'); ?></p>"><?php echo trans('settings.annual_allowance'); ?></span></span>
              </a>
            </li>
          </ul>

<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////--> 
          <div class="tab-content custom" >
            <div class="tab-pane " id="general_settings" style="padding-left: 70px;">
            <div class="col-sm-12 no_padding column">
                <form id="general_setting" action="<?php
                  if($general_setting['status']=='success')
                  {
                    echo "updateGeneralSetting";
                  }
                  else
                  {
                     echo "setGeneralSetting";
                  }
                  ?>" method="post">
                  <div class="col-md-12 div_space">
                  <label class="col-md-4 col-sm-6 control-label label_padding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_timepicker_one');?></li> <li><?php echo trans('settings.tooltip_timepicker_two');?></li> <li><?php echo trans('settings.tooltip_timepicker_three');?></li> </ul>"><?php echo trans('settings.time_picker');?></span></label>
                            <div class="col-md-8 col-sm-6 no_padding form-group">
                            <div class="col-md-12 div_space1">
                              <div class="col-md-2 hidden-sm hidden-xs no_padding1 first_div"></div>
                              <div class="col-md-3 col-sm-12 col-xs-12 center_align interval_div"><?php echo trans('translate.interval'); ?></div> 
                              <div class="col-md-3 col-sm-12 col-xs-12 center_align rounding_div"><?php echo trans('translate.rounding_option'); ?></div>
                              <div class="col-md-5 hidden-sm hidden-xs"></div> 
                            </div>
                            <div class="col-md-12 div_space1">
                              <div class="col-md-2 col-sm-12 col-xs-12 no_padding1 first_div">Start</div> 
                              <div class="col-md-3 col-sm-12 col-xs-12 center_align interval_div">
                            <!--   <?php $start_interval="selected";
                              $start_rounding_option="selected";?> -->
                              <?php 
                              $start_interval1='';
                              $start_interval2='';
                              $start_interval3='';
                              $start_interval4='';
                              $start_interval5='';
                              $start_interval6='';
                              $start_interval10='';
                              $start_interval12='';
                              $start_interval15='';
                              $start_interval20='';
                              $start_interval30='';
                              $start_rounding_option1='';
                              $start_rounding_option2='';
                              $start_rounding_option3='';
                              if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['start_interval']=="1")
                                  {
                                    $start_interval1="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="2")
                                  {  
                                     $start_interval2="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="3")
                                  {  
                                     $start_interval3="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="4")
                                  {  
                                     $start_interval4="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="5")
                                  {  
                                     $start_interval5="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="6")
                                  {  
                                     $start_interval6="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="10")
                                  {  
                                     $start_interval10="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="12")
                                  {  
                                     $start_interval12="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="15")
                                  {  
                                     $start_interval15="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="20")
                                  {  
                                     $start_interval20="selected";
                                  }
                                  else if($general_setting['response']['0']['start_interval']=="30")
                                  {  
                                     $start_interval30="selected";
                                  }
                                  else
                                  {
                                    $start_interval1="selected";
                                  }
                                  if($general_setting['response']['0']['start_rounding_option']=="up")
                                  {
                                    $start_rounding_option1="selected";
                                  }
                                  else if($general_setting['response']['0']['start_rounding_option']=="down")
                                  {
                                    $start_rounding_option2="selected";
                                  }
                                  else if($general_setting['response']['0']['start_rounding_option']=="near")
                                  {
                                    $start_rounding_option3="selected";
                                  }
                                  else
                                  {
                                    $start_rounding_option1="selected";
                                  }
                                  }
                                  else
                                  {
                                  $start_interval1="selected";
                                  $start_rounding_option1="selected";
                                  }

                                  ?>
                                <select class="dropdown-sort1" name="start_interval">
                                  <option value="1" <?php echo $start_interval1;?>>1 min</option>
                                  <option value="2" <?php echo $start_interval2;?>>2 min</option>
                                  <option value="3" <?php echo $start_interval3;?>>3 min</option>
                                  <option value="4" <?php echo $start_interval4;?>>4 min</option>
                                  <option value="5" <?php echo $start_interval5;?>>5 min</option>
                                  <option value="6" <?php echo $start_interval6;?>>6 min</option>
                                  <option value="10" <?php echo $start_interval10;?>>10 min</option>
                                  <option value="12" <?php echo $start_interval12;?>>12 min</option>
                                  <option value="15" <?php echo $start_interval15;?>>15 min</option>
                                  <option value="20" <?php echo $start_interval20;?>>20 min</option>
                                  <option value="30" <?php echo $start_interval30;?>>30 min</option>
                                </select>
                              </div> 
                              <div class="col-md-3 col-sm-12 col-xs-12 center_align rounding_div">
                                <select class="dropdown-sort1" name="start_rounding_option">
                                  <option value="up" <?php echo $start_rounding_option1;?>><?php echo trans('translate.up'); ?></option>
                                  <option value="down"  <?php echo $start_rounding_option2;?>><?php echo trans('translate.down'); ?></option>
                                  <option value="near" <?php echo $start_rounding_option3;?>><?php echo trans('translate.near'); ?></option>
                                </select>
                              </div> 
                              <div class="col-md-5 hidden-sm hidden-xs"></div>
                            </div>
                            <div class="col-md-12 div_space1">
                              <div class="col-md-2 col-sm-12 col-xs-12 no_padding1 first_div"><?php echo trans('translate.end'); ?></div> 
                               <div class="col-md-3 col-sm-12 col-xs-12 center_align interval_div">
                            <!--   <?php $start_interval="selected";
                              $start_rounding_option="selected";?> -->
                              <?php 
                              $end_interval1='';
                              $end_interval2='';
                              $end_interval3='';
                              $end_interval4='';
                              $end_interval5='';
                              $end_interval6='';
                              $end_interval10='';
                              $end_interval12='';
                              $end_interval15='';
                              $end_interval20='';
                              $end_interval30='';
                              $end_rounding_option1='';
                              $end_rounding_option2='';
                              $end_rounding_option3='';
                              if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['end_interval']=="1")
                                  {
                                    $end_interval1="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="2")
                                  {  
                                     $end_interval2="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="3")
                                  {  
                                     $end_interval3="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="4")
                                  {  
                                     $end_interval4="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="5")
                                  {  
                                     $end_interval5="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="6")
                                  {  
                                     $end_interval6="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="10")
                                  {  
                                     $end_interval10="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="12")
                                  {  
                                     $end_interval12="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="15")
                                  {  
                                     $end_interval15="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="20")
                                  {  
                                     $end_interval20="selected";
                                  }
                                  else if($general_setting['response']['0']['end_interval']=="30")
                                  {  
                                     $end_interval30="selected";
                                  }
                                  else
                                  {
                                    $end_interval1="selected";
                                  }
                                  if($general_setting['response']['0']['end_rounding_option']=="up")
                                  {
                                    $end_rounding_option1="selected";
                                  }
                                  else if($general_setting['response']['0']['end_rounding_option']=="down")
                                  {
                                    $end_rounding_option2="selected";
                                  }
                                  else if($general_setting['response']['0']['end_rounding_option']=="near")
                                  {
                                    $end_rounding_option3="selected";
                                  }
                                  else
                                  {
                                    $end_rounding_option1="selected";
                                  }
                                  }
                                  else
                                  {
                                  $end_interval1="selected";
                                  $end_rounding_option1="selected";
                                  }

                                  ?>
                                <select class="dropdown-sort1" name="end_interval">
                                  <option value="1" <?php echo $end_interval1;?>>1 min</option>
                                  <option value="2" <?php echo $end_interval2;?>>2 min</option>
                                  <option value="3" <?php echo $end_interval3;?>>3 min</option>
                                  <option value="4" <?php echo $end_interval4;?>>4 min</option>
                                  <option value="5" <?php echo $end_interval5;?>>5 min</option>
                                  <option value="6" <?php echo $end_interval6;?>>6 min</option>
                                  <option value="10" <?php echo $end_interval10;?>>10 min</option>
                                  <option value="12" <?php echo $end_interval12;?>>12 min</option>
                                  <option value="15" <?php echo $end_interval15;?>>15 min</option>
                                  <option value="20" <?php echo $end_interval20;?>>20 min</option>
                                  <option value="30" <?php echo $end_interval30;?>>30 min</option>
                                </select>
                              </div> 
                              <div class="col-md-3 col-sm-12 col-xs-12 center_align rounding_div">
                                <select class="dropdown-sort1" name="end_rounding_option">
                                  <option value="up"  <?php echo $end_rounding_option1;?>><?php echo trans('translate.up'); ?></option>
                                  <option value="down" <?php echo $end_rounding_option2;?>><?php echo trans('translate.down'); ?></option>
                                  <option value="near" <?php echo $end_rounding_option3;?>><?php echo trans('translate.near'); ?></option>
                                </select>
                              </div> 
                              <div class="col-md-5 hidden-sm hidden-xs"></div>
                            </div>
                            </div>
                          </div>
                          <div class="col-md-12 div_space">
                  <label class="col-md-4 col-sm-6 control-label label_padding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_timestyle_one'); ?></li> <li><?php echo trans('settings.tooltip_timestyle_two'); ?></li> <li><?php echo trans('settings.tooltip_timestyle_three'); ?></li> </ul>"><?php echo trans('settings.time_style'); ?></span></label>
                            <div class="col-md-8 col-sm-6 no_padding form-group">
                              <label class="radio-inline">
                                <input type="radio" name="time_style" class="cbr cbr-blue " id="time_style1" value="am/pm"  <?php if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['time_style']=="am/pm")
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                  echo "checked";}?>>
                                    <?php echo trans('settings.am'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="time_style" class="cbr cbr-blue " id="time_style2" value="24 hours" <?php if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['time_style']=="24 hours")
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                    24 <?php echo trans('settings.hours'); ?>
                            </label>
                          <label class="radio-inline">
                                <input type="radio" name="time_style" class="cbr cbr-blue " id="time_style_industrial" value="Industrial" <?php if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['time_style']=="Industrial")
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                    <?php echo trans('settings.industrial'); ?>
                            </label>
                        </div>
                          </div>
                          <div class="col-md-12 div_space">
                            <label class="col-md-4 col-sm-6 control-label label_padding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_fastcheckin_one'); ?></li> <li><?php echo trans('settings.tooltip_fastcheckin_two'); ?></li> <li><?php echo trans('settings.tooltip_fastcheckin_three'); ?></li> </ul>"><?php echo trans('settings.fast_checkin'); ?></span></label>  
                        <div class="col-md-8 col-sm-6">
                            <div class="arrow_background">
                        <select class="dropdown-sort" name="general_setting_activity">
                            <?php
                            $details['activity_list']=$details['activity_list']['response'];
                            for($i=0;$i<count($details['activity_list']);$i++)
                            {
                              if (!empty($details['activity_list']))
                              {
                                if (!empty($general_setting['response']))
                              {
                                if($general_setting['response']['0']['general_setting_activity']==$details['activity_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['_id'].'" '.$res.'>'.$details['activity_list'][$i]['activity_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['_id'].'">'.$details['activity_list'][$i]['activity_name'].'</option>';
                              }
                            }
                              else
                              {
                              echo '';
                              }
                            }
                          ?>
                      </select>
                    </div>
                          </div>
                          </div>
                  <div class="col-md-12 div_space">
                  <label class="col-md-4 col-sm-6 control-label label_padding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_totalvalue_one'); ?></li> <li><?php echo trans('settings.tooltip_totalvalue_two'); ?></li> <li><?php echo trans('settings.tooltip_totalvalue_three'); ?></li> </ul>"><?php echo trans('settings.total_value'); ?></span></label>
                            <div class="col-md-8 col-sm-6 no_padding form-group">
                              <label class="radio-inline" style="opacity: inherit;">
                                <input type="radio" name="value_format" class="cbr cbr-blue " id="value_format1" value="8:15"  <?php if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['value_format']=="8:15")
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     if($general_setting['response']['0']['time_style']=="Industrial")
                                     {
                                      echo "disabled";
                                     }
                                     else{
                                      echo "";
                                    }
                                  }
                                  }
                                  else
                                    {
                                      echo "checked";}?>>
                                    8:15
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="value_format" class="cbr cbr-blue " id="value_format2" value="8.15" <?php if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['value_format']=="8.15")
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                    8.15
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="value_format" class="cbr cbr-blue " id="value_format3" value="Device region" <?php if (!empty($general_setting['response'])) {
                                  if($general_setting['response']['0']['value_format']=="Device region")
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                    <?php echo trans('settings.device_region'); ?>
                            </label>
                            </div>
                            </div>
                            <div class="col-md-12 div_space">
                              <label class="col-md-4 col-sm-6 control-label label_padding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_working_one'); ?></li> <li><?php echo trans('settings.tooltip_working_two'); ?></li> <li><?php echo trans('settings.tooltip_working_three'); ?></li> </ul>"><?php echo trans('settings.working_hours'); ?></span></label>
                            <div class="col-md-8 col-sm-6 no_padding form-group">
                            <label class="radio-inline">
                               <span><?php echo trans('settings.formula'); ?>: 1 <?php echo trans('settings.day'); ?> =  </span> <input type="text" name="set_working_hours" id="set_working_hours" value="<?php if (!empty($general_setting['response'])) { echo $general_setting['response']['0']['set_working_hours']; } else { echo "00:00"; } ?>"><span>  <?php echo trans('settings.hours'); ?></span><br>
                                <label style=" margin-top: 4px; ">
                                <input type="checkbox" class="cbr cbr-info" name="update_annual_allowance" id="update_annual_allowance" value="true">
                                <?php echo trans('settings.update'); ?> ?
                                </label>
                            </label>
                            </div>
                            </div>
                         <div class="col-md-12 t_align buttons">
                          <button class="btn btn-info btn_class "><!-- <?php
                  if($general_setting['status']=='success')
                  {
                    echo "EDIT";
                  }
                  else
                  {
                     echo "SAVE";
                  }
                  ?> --><?php echo trans('settings.save_button'); ?></button>
                         </div>
                         </form>
            </div>
            </div>
  <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////--> 


            <!--<div class="tab-pane" id="clear_overtime">
                 <div class="col-md-12 no_padding column">
                    <div class="col-md-6 hidden-xs">
                      
                    </div>
                    <div class="col-md-6 col-sm-12 t_align">
                      <p class="add_text">Add a timesheet</p>
                    </div>
                </div>
                <form role="form">
                <div class="col-md-12 no_padding column">
                    <div class="col-md-5 col-sm-12 form-group">
                      <div class="div_space">
                        <label class="col-md-7 col-sm-7 control-label label_padding">Balance as per</label> 
                         --><!-- <div class="col-sm-2">
                          
                        </div> --> 
                        <!-- <div class="col-md-4 col-sm-4" style="float: right;">
                            <div class="input-group">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                    </div>
                    </div>
                    </div>
                    <div class="div_space">
                    <label class="col-md-6 col-sm-7 control-label label_padding">Show balance</label>  -->
                       <!--  <div class="col-sm-2">
                          
                        </div>  -->
                        <!-- <div class="col-md-4 col-sm-4" style="float: right;">
                      <button class="btn btn-info">GO</button>
                    </div>
                    </div>
                    <div class="div_space">
                    <label class="col-md-6 col-sm-7 control-label label_padding">Balance at the end</label>  -->
                        <!-- <div class="col-sm-2">
                          
                        </div> --> 
                        <!-- <div class="col-md-4 col-sm-4" style="float: right;">
                      <p class="add_text">-3.00</p>
                    </div>
                    </div>
                  </div>
                    
                    <div class="col-md-5 form-group" style="float: right;">
                    <div class="div_space">
                      <label class="col-md-7 col-sm-7 control-label label_padding">Activity</label>  -->
                        <!-- <div class="col-sm-2">
                          
                        </div>  -->
                        <!-- <div class="col-md-5 col-sm-5" style="float: right;">
                            <div class="arrow_background2">
                        <select>
                          <option value="volvo">Payments account</option>
                          <option value="saab" selected>Balance reducer</option>
                      </select>
                    </div>
                    </div>
                    </div>
                    <div class="div_space">
                        <label class="col-md-7 col-sm-7 control-label label_padding">Choose a date</label> --> 
                        <!-- <div class="col-sm-1">
                          
                        </div>  -->
                        <!-- <div class="col-md-4 col-sm-4">
                            <div class="input-group">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                    </div>
                    </div>
                    </div>
                    <div class="div_space">
                    <label class="col-md-7 col-sm-7 control-label label_padding">Insert timesheet</label>  -->
                        <!-- <div class="col-sm-1">
                          
                        </div>  -->
                        <!-- <div class="col-md-4 col-sm-4">
                      <button class="btn btn-info">GO</button>
                    </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 t_align buttons">
                  <button class="btn btn-info btn_class">SAVE</button>
                  <button class="btn btn-white btn_class" id="cancel_btn">CANCEL</button>                
              </div>
                </form>
            </div> -->

 <!--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
            <div class="tab-pane" id="set_working">
            <form id="set_working_form" action="<?php
                  if($working_day_setting['status']=='success')
                  {
                    echo "updateWorkingData";
                  }
                  else
                  {
                     echo "setWorkingData";
                  }
                  ?>" method="post">
            <div class="col-md-12 no_padding">
      <ul class="bxslider">
      <li class="custom_bx_li">
              <div class="col-md-12 right_border first_table table_div">
                  <table class="responsive_table">
                    <thead>
                      <tr>
                          <th class="ePadding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_showdays_one'); ?></li> <li><?php echo trans('settings.tooltip_showdays_two'); ?></li> <li><?php echo trans('settings.tooltip_showdays_three'); ?></li> </ul>"><?php echo trans('settings.show_days'); ?></span></th>
                          <th class="ePadding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_workdays_one'); ?></li> <li><?php echo trans('settings.tooltip_workdays_two'); ?></li> <li><?php echo trans('settings.tooltip_workdays_three'); ?></li> </ul>"><?php echo trans('settings.work_days_offset'); ?></span></th>
                          <th class="ePadding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_break_one'); ?></li> <li><?php echo trans('settings.tooltip_break_two'); ?></li> <li><?php echo trans('settings.tooltip_break_three'); ?></li> </ul>"><?php echo trans('settings.break_default'); ?></span></th>
                          <th class="ePadding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_template_ctb_one'); ?></li> <li><?php echo trans('settings.tooltip_template_ctb_two'); ?></li> <li><?php echo trans('settings.tooltip_template_ctb_three'); ?></li> </ul>"><!-- <?php echo trans('settings.template'); ?> --><!-- Condtion towards balance --> <?php echo trans("translate.ctb")?></span></th>
                          <th class="ePadding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_template_one'); ?></li> <li><?php echo trans('settings.tooltip_template_two'); ?></li> <li><?php echo trans('settings.tooltip_template_three'); ?></li> </ul>"><?php echo trans('settings.template'); ?></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <input type="hidden" name="mondayid" value="<?php if (!empty($working_day_setting['response'])) 
                        {
                          echo $working_day_setting['response']['0']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                          <td class="responsive">
                             <label>
                                <input type="checkbox" class="cbr selected cbr-blue" name="monday_isset" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['0']['isset']==true)
                                  {
                                     echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>
                                  >
                                  <p class="weekdayname"><?php echo trans('settings.mon'); ?></p>
                              </label>
                          </td>
                          <td class="fixed_width_week">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control offset" name="work_days_offset_monday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['0']['work_days_offset']!='')
                          {
                            echo $working_day_setting['response']['0']['work_days_offset'];
                          }
                      }
                      else
                          {
                            echo "8:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="fixed_width">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control break" name="break_default_monday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['0']['break_default']!='')
                          {
                            echo $working_day_setting['response']['0']['break_default'];
                          }
                      }
                      else
                          {
                            echo "1:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="checkbox_style">
                            <input type="checkbox" class="cbr cbr-blue ctb" name="ctb_monday" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['0']['ctb']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                     echo "checked";
                                  }?>>
                          </td>
                          <td>
                      <div class="input-group input-group-minimal">
                      <select class="form-control" name="template_monday">
                      <option value="null">--</option>
                          <?php
                            for($i=0;$i<count($details['template_list']);$i++)
                            {

                              if (!empty($working_day_setting['response']))
                              {
                                if($working_day_setting['response']['0']['template_id']==$details['template_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'" '.$res.'>'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'">'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                            }
                          ?>
                      </select>
                      </div>
                          </td>
                        </tr>
                        <tr>
                        <input type="hidden" name="tuesdayid" value="<?php if (!empty($working_day_setting['response'])) 
                        {
                          echo $working_day_setting['response']['1']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                          <td class="responsive">
                             <label>
                                <input type="checkbox" class="cbr selected cbr-blue" name="tuesday_isset"<?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['1']['isset']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                  <p class="weekdayname"><?php echo trans('settings.tues'); ?></p>
                              </label>
                          </td>
                          <td class="fixed_width_week">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control offset" name="work_days_offset_tuesday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['1']['work_days_offset']!='')
                          {
                            echo $working_day_setting['response']['1']['work_days_offset'];
                          }
                      }
                      else
                          {
                            echo "8:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="fixed_width">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control break" name="break_default_tuesday"
                       value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['1']['break_default']!='')
                          {
                            echo $working_day_setting['response']['1']['break_default'];
                          }
                      }
                          else
                          {
                            echo "1:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="checkbox_style">
                            <input type="checkbox" class="cbr cbr-blue ctb" name="ctb_tuesday" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['1']['ctb']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                     echo "checked";
                                  }?>>
                          </td>
                          <td>
                            <div class="input-group input-group-minimal">
                           <!--  <button class="btn btn-info btn-block">TEMPLATE</button> -->
                           <select class="form-control" name="template_tuesday">
                           <option value="null">--</option>
                            <?php
                            for($i=0;$i<count($details['template_list']);$i++)
                            {
                              if (!empty($working_day_setting['response']))
                              {
                                if($working_day_setting['response']['1']['template_id']==$details['template_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'" '.$res.'>'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'">'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                            }
                          ?>
                      </select>
                            </div>
                          </td>
                        </tr>
                        <tr>
                        <input type="hidden" name="wednesdayid" value="<?php if (!empty($working_day_setting['response'])) 
                        {
                          echo $working_day_setting['response']['2']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                          <td class="responsive">
                             <label>
                                <input type="checkbox" class="cbr selected cbr-blue" name="wednesday_isset" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['2']['isset']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                  <p class="weekdayname"><?php echo trans('settings.wed'); ?></p>
                              </label>
                          </td>
                          <td class="fixed_width_week">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control offset" name="work_days_offset_wednesday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['2']['work_days_offset']!='')
                          {
                            echo $working_day_setting['response']['2']['work_days_offset'];
                          }
                      }
                      else
                          {
                            echo "8:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="fixed_width">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control break" name="break_default_wednesday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['2']['break_default']!='')
                          {
                            echo $working_day_setting['response']['2']['break_default'];
                          }
                      }else
                          {
                            echo "1:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="checkbox_style">
                            <input type="checkbox" class="cbr cbr-blue ctb" name="ctb_wednesday" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['2']['ctb']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                     echo "checked";
                                  }?>>
                          </td>
                          <td>
                            <div class="input-group input-group-minimal">
                           <!--  <button class="btn btn-info btn-block">TEMPLATE</button> -->
                           <select class="form-control" name="template_wednesday" >
                           <option value="null">--</option>
                            <?php
                            for($i=0;$i<count($details['template_list']);$i++)
                            {
                              if (!empty($working_day_setting['response']))
                              {
                                if($working_day_setting['response']['2']['template_id']==$details['template_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'" '.$res.'>'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'">'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                            }
                          ?>
                      </select>
                            </div>
                          </td>
                        </tr>
                        <tr>
                        <input type="hidden" name="thursdayid" value="<?php if (!empty($working_day_setting['response'])) 
                        {
                          echo $working_day_setting['response']['3']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                          <td class="responsive">
                             <label>
                                <input type="checkbox" class="cbr selected cbr-blue" name="thursday_isset" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['3']['isset']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                  <p class="weekdayname"><?php echo trans('settings.thur'); ?></p>
                              </label>
                          </td>
                          <td class="fixed_width_week">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control offset" name="work_days_offset_thursday"  value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['3']['work_days_offset']!='')
                          {
                            echo $working_day_setting['response']['3']['work_days_offset'];
                          }
                      }
                      else
                          {
                            echo "8:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="fixed_width">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control break" name="break_default_thursday"  value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['3']['break_default']!='')
                          {
                            echo $working_day_setting['response']['3']['break_default'];
                          }
                      }
                      else
                          {
                            echo "1:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="checkbox_style">
                            <input type="checkbox" class="cbr cbr-blue ctb" name="ctb_thursday" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['3']['ctb']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                     echo "checked";
                                  }?>>
                          </td>
                          <td>
                            <div class="input-group input-group-minimal">
                            <!-- <button class="btn btn-info btn-block">TEMPLATE</button> -->
                            <select class="form-control" name="template_thursday">
                            <option value="null">--</option>
                            <?php
                            for($i=0;$i<count($details['template_list']);$i++)
                            {
                              if (!empty($working_day_setting['response']))
                              {
                                if($working_day_setting['response']['3']['template_id']==$details['template_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'" '.$res.'>'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'">'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                            }
                          ?>
                      </select>
                            </div>
                          </td>
                        </tr>
                        <tr>
                        <input type="hidden" name="fridayid" value="<?php if (!empty($working_day_setting['response'])) 
                        {
                          echo $working_day_setting['response']['4']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                          <td class="responsive">
                             <label>
                                <input type="checkbox" class="cbr selected cbr-blue" name="friday_isset" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['4']['isset']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                  <p class="weekdayname"><?php echo trans('settings.fri'); ?></p>
                              </label>
                          </td>
                          <td class="fixed_width_week">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control offset" name="work_days_offset_friday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['4']['work_days_offset']!='')
                          {
                            echo $working_day_setting['response']['4']['work_days_offset'];
                          }
                          
                      }
                      else
                          {
                            echo "8:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="fixed_width">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control break" name="break_default_friday"  value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['4']['break_default']!='')
                          {
                            echo $working_day_setting['response']['4']['break_default'];
                          }
                         
                      }
                       else
                          {
                            echo "1:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="checkbox_style">
                            <input type="checkbox" class="cbr cbr-blue ctb" name="ctb_friday"  <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['4']['ctb']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                     echo "checked";
                                  }?>>
                          </td>
                          <td>
                            <div class="input-group input-group-minimal">
                           <!--  <button class="btn btn-info btn-block">TEMPLATE</button> -->
                           <select class="form-control" name="template_friday">
                           <option value="null">--</option>
                           <?php
                            for($i=0;$i<count($details['template_list']);$i++)
                            {
                              if (!empty($working_day_setting['response']))
                              {
                                if($working_day_setting['response']['4']['template_id']==$details['template_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'" '.$res.'>'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'">'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                            }
                          ?>
                      </select>
                            </div>
                          </td>
                        </tr>
                        <tr>
                        <input type="hidden" name="saturdayid" value="<?php if (!empty($working_day_setting['response'])) 
                        {
                          echo $working_day_setting['response']['5']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                          <td class="responsive">
                             <label>
                                <input type="checkbox" class="cbr cbr-blue" id="saturday_isset" name="saturday_isset" <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['5']['isset']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                  <p class="weekdayname"><?php echo trans('settings.sat'); ?></p>
                              </label>
                          </td>
                          <td class="fixed_width_week">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control offset" name="work_days_offset_saturday"  value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['5']['work_days_offset']!='')
                          {
                            echo $working_day_setting['response']['5']['work_days_offset'];
                          }
                          
                      }
                      else
                          {
                            echo "8:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="fixed_width">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control break" name="break_default_saturday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['5']['break_default']!='')
                          {
                            echo $working_day_setting['response']['5']['break_default'];
                          }
                          
                      }
                      else
                          {
                            echo "1:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="checkbox_style">
                            <input type="checkbox" class="cbr cbr-blue ctb" name="ctb_saturday"  <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['5']['ctb']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                     echo "checked";
                                  }?>>
                          </td>
                          <td>
                            <div class="input-group input-group-minimal">
                            <!-- <button class="btn btn-info btn-block">TEMPLATE</button> -->
                            <select class="form-control" name="template_saturday">
                            <option value="null">--</option>
                            <?php
                            for($i=0;$i<count($details['template_list']);$i++)
                            {
                              if (!empty($working_day_setting['response']))
                              {
                                if($working_day_setting['response']['5']['template_id']==$details['template_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'" '.$res.'>'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'">'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                            }
                          ?>
                      </select>
                            </div>
                          </td>
                        </tr>
                        <tr>
                        <input type="hidden" name="sundayid" value="<?php if (!empty($working_day_setting['response'])) 
                        {
                          echo $working_day_setting['response']['6']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                          <td class="responsive">
                             <label>
                                <input type="checkbox" class="cbr cbr-blue" id="sunday_isset" name="sunday_isset"<?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['6']['isset']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }?>>
                                  <p class="weekdayname"><?php echo trans('settings.sun'); ?></p>
                              </label>
                          </td>
                          <td class="fixed_width_week">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control offset" name="work_days_offset_sunday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['6']['work_days_offset']!='')
                          {
                            echo $working_day_setting['response']['6']['work_days_offset'];
                          }
                          
                      }
                      else
                          {
                            echo "8:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="fixed_width">
                            <div class="input-group input-group-minimal">
                      <input type="text" class="form-control break"  name="break_default_sunday" value="<?php if (!empty($working_day_setting['response'])){
                        if($working_day_setting['response']['6']['break_default']!='')
                          {
                            echo $working_day_setting['response']['6']['break_default'];
                          }
                          
                      }
                      else
                          {
                            echo "1:00";
                          }
                      ?>"/>
                        </div>
                          </td>
                          <td class="checkbox_style">
                            <input type="checkbox" class="cbr cbr-blue ctb" name="ctb_sunday"  <?php if (!empty($working_day_setting['response'])) {
                                  if($working_day_setting['response']['6']['ctb']==true)
                                  {
                                    echo "checked";
                                  }
                                  else
                                  {
                                     echo "";
                                  }
                                  }
                                  else
                                  {
                                     echo "checked";
                                  }?>>
                          </td>
                          <td>
                            <div class="input-group input-group-minimal">
                           <!--  <button class="btn btn-info btn-block">TEMPLATE</button> -->
                           <select class="form-control" name="template_sunday">
                           <option value="null">--</option>
                            <?php
                            for($i=0;$i<count($details['template_list']);$i++)
                            {
                              if (!empty($working_day_setting['response']))
                              {
                                if($working_day_setting['response']['6']['template_id']==$details['template_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'" '.$res.'>'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['template_list'][$i]['_id'].'" value="'.$details['template_list'][$i]['_id'].'">'.$details['template_list'][$i]['template_name'].'</option>';
                              }
                            }
                          ?>
                      </select>
                            </div>
                          </td>
                        </tr>
                    </tbody>
                  </table>
              </div>
        </li>
      <li class="custom_bx_li">
                    <div class="col-md-12 right_border table_div">
              <p class="table_heading"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_condition1_one'); ?></li> <li><?php echo trans('settings.tooltip_condition1_two'); ?></li> <li><?php echo trans('settings.tooltip_condition1_three'); ?></li> </ul>"><?php echo trans('settings.conditions_one'); ?></span></p>
              <table class="responsive_table">
                <thead>
                  <tr>
                    <th class="ePadding"><?php echo trans('settings.days'); ?></th>
                    <th class="ePadding"><?php echo trans('settings.conditions'); ?></th>
                    <th class="ePadding"><?php echo trans('settings.from'); ?></th>
                    <th class="ePadding"><?php echo trans('settings.factor'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  
                  <tr>
                  <input type="hidden" name="monday_condtion_id" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['0']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.mon'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                      <select class="form-control" name="conditions_monday" id="conditions_monday">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['0']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['0']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['0']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <input type="text" class="form-control from time1" id="from_monday" name="from_monday" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['0']['condtion']=="Time"){
                          echo $condtion['response']['0']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_monday11" name="from_monday11" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['0']['condtion']=="Hours"){
                          echo $condtion['response']['0']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_monday" name="factor_monday"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['0']['factor'])){
                          echo $condtion['response']['0']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_monday" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['0']['factor'])){
                          echo $condtion['response']['0']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }
                        else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="tuesday_condtion_id" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['2']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.tues'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                       <select class="form-control" name="conditions_tuesday" id="conditions_tuesday">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['2']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['2']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['2']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>      
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <input type="text" class="form-control from time1" id="from_tuesday" name="from_tuesday" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['2']['condtion']=="Time"){
                          echo $condtion['response']['2']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_tuesday11" name="from_tuesday11" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['2']['condtion']=="Hours"){
                          echo $condtion['response']['2']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_tuesday" name="factor_tuesday"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['2']['factor'])){
                          echo $condtion['response']['2']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_tuesday" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['2']['factor'])){
                          echo $condtion['response']['2']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="wednesday_condtion_id" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['4']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.wed'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_wednesday" id="conditions_wednesday">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['4']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['4']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['4']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <!-- <input type="text" class="form-control from" name="from_wednesday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_wednesday" name="from_wednesday" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['4']['condtion']=="Time"){
                          echo $condtion['response']['4']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_wednesday11" name="from_wednesday11" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['4']['condtion']=="Hours"){
                          echo $condtion['response']['4']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_wednesday" name="factor_wednesday"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['4']['factor'])){
                          echo $condtion['response']['4']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_wednesday" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['4']['factor'])){
                          echo $condtion['response']['4']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="thursday_condtion_id" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['6']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.thur'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_thursday" id="conditions_thursday">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['6']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['6']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['6']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                      <!--   <input type="text" class="form-control from" name="from_thursday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_thursday" name="from_thursday" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['6']['condtion']=="Time"){
                          echo $condtion['response']['6']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_thursday11" name="from_thursday11" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['6']['condtion']=="Hours"){
                          echo $condtion['response']['6']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_thursday" name="factor_thursday"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['6']['factor'])){
                          echo $condtion['response']['6']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_thursday" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['6']['factor'])){
                          echo $condtion['response']['6']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="friday_condtion_id" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['8']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.fri'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                       <select class="form-control" name="conditions_friday" id="conditions_friday">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['8']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['8']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['8']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <!-- <input type="text" class="form-control from" name="from_friday" value="00:00"> -->
                         <input type="text" class="form-control from time1" id="from_friday" name="from_friday" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['8']['condtion']=="Time"){
                          echo $condtion['response']['8']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_friday11" name="from_friday11" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['8']['condtion']=="Hours"){
                          echo $condtion['response']['8']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_friday" name="factor_friday"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['8']['factor'])){
                          echo $condtion['response']['8']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_friday" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['8']['factor'])){
                          echo $condtion['response']['8']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="saturday_condtion_id" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['10']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.sat'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_saturday" id="conditions_saturday">
<option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['10']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['10']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['10']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>  
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                       <!--  <input type="text" class="form-control from" name="from_saturday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_saturday" name="from_saturday" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['10']['condtion']=="Time"){
                          echo $condtion['response']['10']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_saturday11" name="from_saturday11" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['10']['condtion']=="Hours"){
                          echo $condtion['response']['10']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_saturday" name="factor_saturday"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['10']['factor'])){
                          echo $condtion['response']['10']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_saturday" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['10']['factor'])){
                          echo $condtion['response']['10']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="sunday_condtion_id" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['12']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.sun'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_sunday" id="conditions_sunday">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['12']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['12']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['12']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>   
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <!-- <input type="text" class="form-control from" name="from_sunday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_sunday" name="from_sunday" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['12']['condtion']=="Time"){
                          echo $condtion['response']['12']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_sunday11" name="from_sunday11" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['12']['condtion']=="Hours"){
                          echo $condtion['response']['12']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_sunday" name="factor_sunday"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['12']['factor'])){
                          echo $condtion['response']['12']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_sunday" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['12']['factor'])){
                          echo $condtion['response']['12']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
                
              </div>
      </li>
      <li class="custom_bx_li">
                    <div class="col-md-12 table_div">
              <p class="table_heading">
                <span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_condition2_one'); ?></li> <li><?php echo trans('settings.tooltip_condition2_two'); ?></li> <li><?php echo trans('settings.tooltip_condition2_three'); ?></li> </ul>"><?php echo trans('settings.condition_two');?>
                </span>
                </p>
              <table class="responsive_table">
                <thead>
                  <tr>
                    <th class="ePadding"><?php echo trans('settings.days'); ?></th>
                    <th class="ePadding"><?php echo trans('settings.conditions'); ?></th>
                    <th class="ePadding"><?php echo trans('settings.from'); ?></th>
                    <th class="ePadding"><?php echo trans('settings.factor'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  
                  <tr>
                  <input type="hidden" name="monday_condtion_id1" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['1']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.mon'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                      <select class="form-control" name="conditions_monday1" id="conditions_monday1">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['1']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['1']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['1']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <input type="text" class="form-control from time1" id="from_monday1" name="from_monday1" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['1']['condtion']=="Time"){
                          echo $condtion['response']['1']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_monday111" name="from_monday111" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['1']['condtion']=="Hours"){
                          echo $condtion['response']['1']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_monday1" name="factor_monday1"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['1']['factor'])){
                          echo $condtion['response']['1']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_monday1" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['1']['factor'])){
                          echo $condtion['response']['1']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="tuesday_condtion_id1" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['3']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.tues'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                       <select class="form-control" name="conditions_tuesday1" id="conditions_tuesday1">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['3']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['3']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['3']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <input type="text" class="form-control from time1" id="from_tuesday1" name="from_tuesday1" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['3']['condtion']=="Time"){
                          echo $condtion['response']['3']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_tuesday111" name="from_tuesday111" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['3']['condtion']=="Hours"){
                          echo $condtion['response']['3']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_tuesday1" name="factor_tuesday1"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['3']['factor'])){
                          echo $condtion['response']['3']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_tuesday1" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['3']['factor'])){
                          echo $condtion['response']['3']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="wednesday_condtion_id1" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['5']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.wed'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_wednesday1" id="conditions_wednesday1">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['5']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['5']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['5']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <!-- <input type="text" class="form-control from" name="from_wednesday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_wednesday1" name="from_wednesday1" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['5']['condtion']=="Time"){
                          echo $condtion['response']['5']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_wednesday111" name="from_wednesday111" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['5']['condtion']=="Hours"){
                          echo $condtion['response']['5']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_wednesday1" name="factor_wednesday1"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['5']['factor'])){
                          echo $condtion['response']['5']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_wednesday1" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['5']['factor'])){
                          echo $condtion['response']['5']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="thursday_condtion_id1" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['7']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.thur'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_thursday1" id="conditions_thursday1">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['7']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['7']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['7']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>      
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                      <!--   <input type="text" class="form-control from" name="from_thursday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_thursday1" name="from_thursday1" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['7']['condtion']=="Time"){
                          echo $condtion['response']['7']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_thursday111" name="from_thursday111" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['7']['condtion']=="Hours"){
                          echo $condtion['response']['7']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_thursday1" name="factor_thursday1"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['7']['factor'])){
                          echo $condtion['response']['7']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_thursday1" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['7']['factor'])){
                          echo $condtion['response']['7']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="friday_condtion_id1" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['9']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.fri'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                       <select class="form-control" name="conditions_friday1" id="conditions_friday1">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['9']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['9']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['9']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>   
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <!-- <input type="text" class="form-control from" name="from_friday" value="00:00"> -->
                         <input type="text" class="form-control from time1" id="from_friday1" name="from_friday1" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['9']['condtion']=="Time"){
                          echo $condtion['response']['9']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_friday111" name="from_friday111" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['9']['condtion']=="Hours"){
                          echo $condtion['response']['9']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_friday1" name="factor_friday1"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['9']['factor'])){
                          echo $condtion['response']['9']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_friday1" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['9']['factor'])){
                          echo $condtion['response']['9']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="saturday_condtion_id1" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['11']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.sat'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_saturday1" id="conditions_saturday1">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['11']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['11']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['11']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                       <!--  <input type="text" class="form-control from" name="from_saturday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_saturday1" name="from_saturday1" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['11']['condtion']=="Time"){
                          echo $condtion['response']['11']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_saturday111" name="from_saturday111" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['11']['condtion']=="Hours"){
                          echo $condtion['response']['11']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_saturday1" name="factor_saturday1"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['11']['factor'])){
                          echo $condtion['response']['11']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_saturday1" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['11']['factor'])){
                          echo $condtion['response']['11']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <input type="hidden" name="sunday_condtion_id1" value="<?php if (!empty($condtion['response'])) 
                        {
                          echo $condtion['response']['13']['_id'] ;
                        }
                        else{echo "";}
                        ?>">
                    <td>
                      <?php echo trans('settings.sun'); ?>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <select class="form-control" name="conditions_sunday1" id="conditions_sunday1">
                       <option value="Time" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['13']['condtion']=="Time"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        ?>><?php echo trans('settings.time'); ?></option>
                       <option value="Hours" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['13']['condtion']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.hours'); ?></option>
                       <option value="No Condition" <?php if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['13']['condtion']=="No Condition"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }?>><?php echo trans('settings.no_conditions'); ?></option>     
                      </select>
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group input-group-minimal">
                        <!-- <input type="text" class="form-control from" name="from_sunday" value="00:00"> -->
                        <input type="text" class="form-control from time1" id="from_sunday1" name="from_sunday1" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['13']['condtion']=="Time"){
                          echo $condtion['response']['13']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                        <input type="text" class="form-control from" id="from_sunday111" name="from_sunday111" value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if ($condtion['response']['13']['condtion']=="Hours"){
                          echo $condtion['response']['13']['from'];
                        }
                        else
                        {
                          echo "00:00";
                        }
                        }?>">
                      </div>
                    </td>
                    <td class="fixed_width">
                      <div class="input-group spinner" data-trigger="spinner">
                      <input type="text" class="factor_style" id="factor_sunday1" name="factor_sunday1"  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['13']['factor'])){
                          echo $condtion['response']['13']['factor'];
                        }
                        else
                        {
                          echo "0.000";
                        }
                        }?>" data-rule="currency">
                        <input type="hidden" class="factor_style" id="factor1_sunday1" name=""  value="<?php 
                        if (!empty($condtion['response']))
                       {
                        if (!empty($condtion['response']['13']['factor'])){
                          echo $condtion['response']['13']['factor'];
                        }
                        else
                        {
                          echo "1.000";
                        }
                        }else
                        {
                          echo "1.000";
                        }?>" data-rule="currency">
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>  
              </div>
      </li>
      </ul>
              <div class="col-md-12 col-sm-12 t_align">
                <!-- <div class="form-group reminder">
                <label for="field-9" class="control-label float_left"><b>Reminder</b>  Date missing</label>
                
                <input type="checkbox" class="iswitch iswitch-info">
          </div> -->
              </div>
              <div class="col-md-12 col-sm-12 t_align buttons">
                  <button class="btn btn-info btn_class " type="submit" id="set_working_form_btn">
                  <!-- <?php 
                  $working_day_setting=$details['working_day_setting']; 
                  if($working_day_setting['status']=='success')
                  {
                    echo 'EDIT';
                  }
                  else
                  {
                   echo 'SAVE';
                  }

                  ?> --><?php echo trans('settings.save_button'); ?></button>
                 <!--  <button class="btn btn-white btn_class" id="cancel_btn">CANCEL</button>       -->          
              </div>
              </form>
            </div>
            </div>
 <!--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --> 
            <div class="tab-pane" id="overtime_period">
            <form id="overtime_handling" action="<?php
                  if($overtime_handling['status']=='success')
                  {
                    echo "updateOvertimeHandling";
                  }
                  else
                  {
                     echo "setOvertimeHandling";
                  }
                  ?>" method="post">
                <div class="col-md-12 no_padding column">
                    <div class="col-md-7 col-sm-7 form-group">
                      <!-- <div class="div_space">
                        <label class="col-sm-5 control-label label_padding">My Date range</label> 
                        <div class="col-sm-1">
                          
                        </div> 
                        <div class="col-sm-3 col_3">
                            <div class="input-group">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                    </div>
                    </div>
                    <div class="col-sm-3 col_3">
                            <div class="input-group">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                    </div>
                    </div>
                    </div>
                    <div class="div_space">
                      <label class="col-sm-6 control-label label_padding">Exclude multiple hours</label> 
                        <div class="col-sm-2">
                          
                        </div> 
                        <div class="col-sm-4">
                      <input type="checkbox" class="iswitch iswitch-info">
                    </div>
                    </div>
                    <div class="div_space">
                    <label class="col-sm-6 control-label label_padding">1 Total Work hours</label> 
                        <div class="col-sm-2">
                          
                        </div> 
                        <div class="col-sm-4">
                      <p class="add_text">0.00 hours</p>
                    </div>
                    </div>
                  <div class="div_space">
                    <label class="col-sm-6 control-label label_padding">1 Total Offset hours</label> 
                        <div class="col-sm-2">
                          
                        </div> 
                        <div class="col-sm-4">
                      <p class="add_text">0.00 hours</p>
                    </div>
                    </div>
                  <div class="div_space">
                    <label class="col-sm-6 control-label label_padding">Balance for Range</label> 
                        <div class="col-sm-2">
                          
                        </div> 
                        <div class="col-sm-4">
                      <p class="add_text">0.00 hours</p>
                    </div>
                    </div>
                  <div class="div_space">
                    <label class="col-sm-7 control-label label_padding">3 threshold(exceeding work hours)</label> 
                        <div class="col-sm-1">
                          
                        </div> 
                        <div class="col-sm-2 col_3">
                      <div class="input-group">
                      <input type="text" class="form-control t_align input_radius" placeholder="40:00" />
                      
                    </div>
                    </div>
                    <label class="col-sm-2 control-label col_3">hours</label>
                    </div> -->
                    <div class="div_space">
                        <div class="col-sm-5">
                        </div>
                        <div class="col-sm-6" style="margin-left:12px;">
                      <p class="add_text" style="float:left; margin-bottom:0px;" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_activityovertime_one'); ?></li> <li><?php echo trans('settings.tooltip_activityovertime_two'); ?></li><li><?php echo trans('settings.tooltip_activityovertime_three'); ?></li> </ul>"><?php echo trans('settings.overtime_for_period'); ?></p>
                    </div>
                    </div>
                    <div class="div_space">
                      <label class="col-sm-5 control-label label_padding"><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_activityovertime_one'); ?></li> <li><?php echo trans('settings.tooltip_activityovertime_two'); ?></li> <li><?php echo trans('settings.tooltip_activityovertime_three'); ?></li> </ul>"><?php echo trans('settings.activity'); ?></span></label> 
            
                        <div class="col-sm-5 col-lg-6 col-md-6" style=" margin-left: 12px; ">
                            <div class="arrow_background2" style="width: 100%;">
                        <!-- <select name="overtime_for_period">
                          <option value="Payments account">Payments account</option>
                          <option value="Balance reducer" selected>Balance reducer</option>
                      </select> -->
                    <!--   <div>
                        <?php
                            for($i=0;$i<count($details['activity_list']);$i++)
                            {
                              $flat_time_mode=$details['activity_list'][$i]['flat_time_mode'];
                              $overtime_reducer=$details['activity_list'][$i]['overtime_reducer'];
                              if($flat_time_mode==true && $overtime_reducer==true )
                              {
                                echo $details['activity_list'][$i]['activity_name'];
                              }
                            }
                          ?>
                      </div> -->
                      <select name="overtime_for_period" id="overtime_for_period" class="setting_selection" style="width: 100%;">
                            <?php
                            for($i=0;$i<count($details['activity_list']);$i++)
                            {
                              $flat_time_mode=$details['activity_list'][$i]['flat_time_mode'];
                              $overtime_reducer=$details['activity_list'][$i]['overtime_reducer'];
                              if (!empty($details['activity_list']))
                              {
                                if($flat_time_mode==true && $overtime_reducer==false )
                              {

                                if (!empty($overtime_handling['response']))
                              {
                                if($overtime_handling['response']['0']['overtime_for_period']==$details['activity_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['_id'].'" '.$res.'>'.$details['activity_list'][$i]['activity_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['_id'].'">'.$details['activity_list'][$i]['activity_name'].'</option>';
                              }
                              }
                              }
                              else
                              {
                              echo '';
                              }
                            }
                          ?>
                      </select>
                    </div>
                    </div>
                    <div id="overtime_for_period_msg" ></div>
                    </div>
                    <div class="div_space">
                        <label class="col-sm-5 control-label label_padding"><?php echo trans('settings.threshold'); ?></label> 
                        <div class="col-sm-5" style=" margin-left: 12px; ">
                            <div class="input-group">
                      <input type="text" class="form-control input_radius" id="threshold_hours" name="threshold_hours" value=""/>
                    </div>
                    </div>
                    </div>
                     <div class="div_space">
                        <label class="col-sm-5 control-label label_padding"><?php echo trans('settings.hourly_rate'); ?></label> 
                        <div class="col-sm-5" style=" margin-left: 12px; ">
                            <div class="input-group">
                      <input type="text" class="form-control input_radius" id="hourly_rate" name="hourly_rate" value="<?php 
                        if (!empty($overtime_handling['response']))
                       {
                        if ($overtime_handling['response']['0']['hourly_rate']!=''){
                          echo $overtime_handling['response']['0']['hourly_rate'];
                        }
                        else
                        {
                          echo "1.500";
                        }
                        }
                        else
                        {
                          echo "1.500";
                        }?>" />
                    </div>
                    </div>
                    </div>
                  </div>  
                    <div class="col-md-5 col-sm-5 form-group">
                    <!-- <div class="div_space">
                      <label class="col-sm-4 control-label label_padding">4 Hourly rate</label> 
                        <div class="col-sm-3">
                          
                        </div> 
                        <div class="col-sm-3">
                            <div class="input-group">
                      <input type="text" class="form-control t_align input_radius" placeholder="100.200" />
                    </div>
                    </div>
                    </div>
                    <div class="div_space">
                        <label class="col-sm-6 control-label label_padding">Calculate: 1-3*4-Balance</label> 
                        <div class="col-sm-1">
                          
                        </div> 
                        <div class="col-sm-4">
                      <button class="btn btn-info">GO</button>
                    </div>
                    </div>
                    <div class="div_space">
                    <label class="col-sm-5 control-label label_padding">Resulting hours</label> 
                        <div class="col-sm-2">
                          
                        </div> 
                        <div class="col-sm-4">
                      <p class="add_text">0.00 hours</p>
                    </div>
                    </div> -->
                    <div class="div_space">
                      <div class="col-md-6 col-sm-12 col-xs-12 clearCacheset">
                      <p class="add_text" style="float:left; margin-bottom:0px;" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_activityclear_one'); ?></li> <li><?php echo trans('settings.tooltip_activityclear_two'); ?></li><li><?php echo trans('settings.tooltip_activityclear_three'); ?></li> </ul>"><?php echo trans('settings.clear_overtime_balance'); ?></p>
                    </div>
                    </div>
                    <div class="div_space">
          
                        <div class="col-lg-8 col-md-6 col-sm-5">
                            <div class="arrow_background2" style="width: 100%;">
                      <select name="clear_overtime_balance" class="setting_selection" id="clear_overtime_balance" style="width: 100%;">
                            <?php
                            for($i=0;$i<count($details['activity_list']);$i++)
                            {
                              $flat_time_mode=$details['activity_list'][$i]['flat_time_mode'];
                              $overtime_reducer=$details['activity_list'][$i]['overtime_reducer'];
                              if (!empty($details['activity_list']))
                              {
                                if($flat_time_mode==true && $overtime_reducer==true )
                              {

                                if (!empty($overtime_handling['response']))
                              {
                                if($overtime_handling['response']['0']['clear_overtime_balance']==$details['activity_list'][$i]['_id'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['_id'].'" '.$res.'>'.$details['activity_list'][$i]['activity_name'].'</option>';
                              }
                              else
                              {
                              echo '<option id="'.$details['activity_list'][$i]['_id'].'" value="'.$details['activity_list'][$i]['_id'].'">'.$details['activity_list'][$i]['activity_name'].'</option>';
                              }
                            }
                          }
                              else
                              {
                              echo '';
                              }
                            }
                          ?>
                      </select>
                    </div>
                    </div>
                    <div id="clear_overtime_balance_msg"></div>
                    </div>
                    <!-- <div class="div_space">
                        <label class="col-sm-5 control-label label_padding">Choose a date</label> 
                        <div class="col-sm-2">
                          
                        </div> 
                        <div class="col-sm-4">
                            <div class="input-group">
                      <input type="text" class="form-control datepicker t_align input_radius" />
                    </div>
                    </div>
                    </div> -->
                    </div>
                </div>
                <div class="col-md-12 t_align buttons">
                  <button class="btn btn-info btn_class "><!-- <?php
                  if($overtime_handling['status']=='success')
                  {
                    echo "EDIT";
                  }
                  else
                  {
                     echo "SAVE";
                  }
                  ?> --><?php echo trans('settings.save_button'); ?></button>
                  <!-- <button class="btn btn-white btn_class" id="cancel_btn">CANCEL</button>    -->             
              </div>
                </form>
            </div>
<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
            <div class="tab-pane" id="set_annual">
              <form id="set_annual_allowance_form" action="<?php
                  if($set_annual_allowance['status']=='success')
                  {
                    echo "updateSetAnnualAllowance";
                  }
                  else
                  {
                     echo "setSetAnnualAllowance";
                  }
                  ?>" method="post">
                <div class="col-md-12 no_padding column">
                  <div class="col-md-3 no_padding ">
                  <label class="col-md-12 col-sm-12 control-label label_padding"><?php echo trans('settings.annual_allowance'); ?></label>
                  </div>
                  <div class="col-md-9 no_padding ">
                  <div class="col-md-12 no_padding space">
                  <label class="col-lg-2 col-md-3 col-sm-3 control-label col_3 "><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_activityannual_one'); ?></li> <li><?php echo trans('settings.tooltip_activityannual_two'); ?></li> <li><?php echo trans('settings.tooltip_activityannual_three'); ?></li> </ul>"><?php echo trans('settings.activity'); ?></span></label>
                  <div class="col-lg-10 col-md-9 col-sm-9">
                    <div class="arrow_background3">
                     <?php
                     $details['activity_list1']=$details['activity_list1']['response'];
                            for($i=0;$i<count($details['activity_list1']);$i++)
                            {
                              if (!empty($details['activity_list1']))
                              {
                                echo '<input type="hidden" id="day'.$details['activity_list1'][$i]['_id'].'" value="'.$details['activity_list1'][$i]['allowance_bank_days'].'"><input type="hidden" id="hour'.$details['activity_list1'][$i]['_id'].'" value="'.$details['activity_list1'][$i]['allowance_bank_hours'].'">';
                              }
                              else
                              {
                              echo '';
                              }
                            }
                      ?>
                    <!-- <select name="set_annual_allowance_activity">
                      <option value="Public Holiday" selected>Public Holiday</option>
                      <option value="Unpaid Leave">Unpaid Leave</option>
                    </select> -->
                      <select name="set_annual_allowance_activity" class="setting_selection1" id="setting_selection">
                            <?php
                            if (!empty($set_annual_allowance['response']))
                            {
                            for($i=0;$i<count($details['activity_list1']);$i++)
                            {
                              if (!empty($details['activity_list1']))
                              {
                                if($details['activity_list1'][$i]['_id']==$set_annual_allowance['response']['0']['set_annual_allowance_activity'])
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['activity_list1'][$i]['_id'].'" value="'.$details['activity_list1'][$i]['_id'].'" '.$res.'>'.$details['activity_list1'][$i]['activity_name'].'</option>';
                              }
                              else
                              {
                              echo '';
                              }
                            }
                            }
                            else
                            {
                            for($i=0;$i<count($details['activity_list1']);$i++)
                            {
                              if (!empty($details['activity_list1']))
                              {
                                if($i==0)
                                {
                                  $res= 'selected';
                                }
                                else
                                {
                                  $res= '';
                                }
                                echo '<option id="'.$details['activity_list1'][$i]['_id'].'" value="'.$details['activity_list1'][$i]['_id'].'" '.$res.'>'.$details['activity_list1'][$i]['activity_name'].'</option>';
                              }
                              else
                              {
                              echo '';
                              }
                            }
                            }
                          ?>
                      </select>
                    </div>
                  </div>
                  </div>
                  <div class="col-md-12 no_padding space">
                  <label class="col-lg-2 col-md-3 col-sm-3 control-label col_3 "><span data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="<ul class='tool_tip'> <li><?php echo trans('settings.tooltip_totalin_one'); ?></li> <li><?php echo trans('settings.tooltip_totalin_two'); ?></li> <li><?php echo trans('settings.tooltip_totalin_three'); ?></li> </ul>"><?php echo trans('settings.total_in'); ?></span></label>
                  <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="arrow_background4">
                    <select name="total_in_type" id="total_in_type">
                      <option value="Hours"  <?php if (!empty($set_annual_allowance['response']))
                       {
                        if ($set_annual_allowance['response']['0']['total_in_type']=="Hours"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }
                        else
                        {
                          echo "selected";
                        }
                        ?>><?php echo trans('settings.hours'); ?></option>
                      <option value="Days"<?php if (!empty($set_annual_allowance['response']))
                       {
                        if ($set_annual_allowance['response']['0']['total_in_type']=="Days"){
                          echo "selected";
                        }
                        else
                        {
                          echo "";
                        }
                        }?>><?php echo trans('settings.days'); ?></option>
                    </select>
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="input-group"> 
                    <input name='total_in_value' id='total_in_value' class='form-control t_align input_radius decimal onchange_val' value='00' />
                    <input type='text' id='total_in_value1' name='total_in_value1' class='form-control inputdiv hours_val onchange_val' value='0:00'>
                  </div>
                    </div>
                  </div>
                  <div class="col-md-12 no_padding space">
                  <label class="col-lg-2 col-md-3 col-sm-3 control-label col_3 "><span style="float: left;" id="type_display"></span></label>
                  <label class="col-lg-2 col-md-3 col-sm-3 control-label equalpostion"><span>=</span></label>
                  <div class="col-md-2 col-sm-2">
                            <div class="input-group">
                      <input type="text" name="total_in_calculated_value" id="total_in_calculated_value" class="form-control t_align input_radius onchange_result" value="" readonly/>
                    </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                      <span class="btn btn-info" id="go"><?php echo trans('settings.go'); ?></span>
                    </div>
                  </div>
                  </div>
                </div>
                <div class="col-md-12 t_align buttons">
                  <button class="btn btn-info btn_class "><!-- <?php
                  if($set_annual_allowance['status']=='success')
                  {
                    echo "EDIT";
                  }
                  else
                  {
                     echo "SAVE";
                  }
                  ?> --><?php echo trans('settings.save_button'); ?></button>
                 <!--  <button class="btn btn-white btn_class" id="cancel_btn">CANCEL</button>     -->            
              </div>
              </form>
            </div>
          </div>
          </div>
          <!-- dates to lock floating btn -->
         <!--  <div class="float_button add_button_class" id="float_button">
        <button onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"  type="submit" class="circular_button"><i class="fa fa-pencil-square-o add_icon" aria-hidden="true" ></i>
        <p class="add_temp_text">Dates to</p>
        <p class="add_temp_text">lock</p></button>
      </div> -->
        </div>
      </div>



        <div class="modal fade" id="modal-6">
    <div class="modal-dialog custom-dialog">
      <div class="modal-header custom-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h5 class="modal-title custom-title"><?php echo trans('manage_activity.modal_title')?></h5>
        </div>
      <div class="modal-content custom-content">
        
        <div class="modal-body custom-body">
          <div class="row">
            <div class="col-md-12">
              <div class="input-group box">
                <label for="field-1" class="control-label" ><!--<?php echo trans('manage_activity.activity_name')?>-->Start Date</label>
                                <input type="text" id="start_date" class="form-control ">
              </div>
            </div>
            <div class="col-md-12">
              <div class="input-group box">
                <label for="field-1" class="control-label" ><!--<?php echo trans('manage_activity.activity_name')?>-->End Date</label>
                                <input type="text" id="end_date" class="form-control">
              </div>
            </div>
          </div>
        <div class="modal-footer custom-footer">
        <div id="addbtn"> 
          <button class="btn btn-info" id="save_btn"><!--<?php echo trans('manage_activity.save')?>-->Send</button>
          </div>
          <div id="addloader" style="display:none">
            <img src="<?=url('image/hourglass.gif')?>" style="width:35px; height: 35px;"/>
          </div>
        </div>   
    </div>
  </div>
  </div>
  <script src="<?php echo url('js/bx/jquery.bxslider.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo url('js/datepicker-de.js')?>"></script>
  <script type="text/javascript">
  $(document).ready(function(){
  $('.bxslider').bxSlider({
    infiniteLoop: false,
  });
  });
  var dayTrans="<?php echo trans('settings.days'); ?>";
  var hoursTrans="<?php echo trans('settings.hours'); ?>";
  /* alert($('#overtime_for_period').val());*/
   if($('#overtime_for_period').val()==null)
   {
   $("#overtime_for_period_msg").html('<?php echo trans('settings.activity_not_found'); ?> <br><a href="<?=url('manage_activity')?>"><?php echo trans('settings.click_here_activity'); ?></a>');
 }
  if($('#clear_overtime_balance').val()==null)
   {
   $("#clear_overtime_balance_msg").html('<?php echo trans('settings.activity_not_found'); ?> <br><a href="<?=url('manage_activity')?>"><?php echo trans('settings.click_here_activity'); ?></a>');
 }
 
 </script>
          <script>
  var res="<?php echo $working_day_setting['status'];?>"
  if(res=='failure')
                  {
                       $('.selected').prop('checked',true);
                  }
  </script>
  <script>
  var error_occurred = "<?php echo trans('popup.error_occurred');?>";
  var success = "<?php echo trans('popup.success');?>";
  var error_="<?php echo trans('popup.error_');?>!";
  var saved_successfully="<?php echo trans('popup.saved_successfully');?>";
  var fill_data="<?php echo trans('popup.fill_data');?>";
</script>
<script>
  var dates = $("#start_date, #end_date").datepicker({
      defaultDate: "+1w",
      firstDay: 1,
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat:"dd-mm-yy",
      onSelect: function(selectedDate) {
        var option = this.id == "start_date" ? "minDate" : "maxDate",
        instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
      }
  });
  
  // $("#start_date, #end_date").change(function(){
  //  var start_date = $('#start_date').val();
  //  var end_date = $('#end_date').val();
  //  console.log({start_date:start_date, end_date:end_date});

 //  });
  $('#save_btn').click(function(){
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    //console.log({start_date:start_date, end_date:end_date});
    if(start_date!=''&&end_date!='') 
    {
      var url="<?php echo url('selectSyncDates')?>";
            url=url+"/"+start_date+"/"+end_date;
            console.log(url);
            $.ajax({
                    url:url,
                    type:'post',
                    success: function(response)
                    {  
                      swal({  
                     title: success, 
                     text: "Dates sent to lock the default activities",   
                     type: "success",   
                     confirmButtonText : "Ok"
                    },
                    function(){
                         /*window.location.href ='<? echo url('manage_activity');?>';*/
                         location.reload();

                    });
                }
                });
    }   

  });
  
//  $(document).ready(function(){
//    $('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
//    $('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));

//    getDates(start_date,end_date);
//  });
// function getDates(start_date,end_date) {
//  var dates = $("#start_date, #end_date").datepicker({
//     defaultDate: "+1w",
//     firstDay: 1,
//     changeMonth: true,
//     numberOfMonths: 1,
//     dateFormat:"dd-mm-yy",
//      onSelect: function(selectedDate) {
//         var option = this.id == "start_date" ? "minDate" : "maxDate",
//         instance = $(this).data("datepicker"),
//             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
//         dates.not(this).datepicker("option", option, date);
//         var start_date=$('#start_date').val();
//         var end_date=$('#end_date').val();
//         localStorage.start_date=start_date;
//         localStorage.end_date=end_date;
//          //console.log(start_date+'//'+end_date+'//');
//          if(start_date!=''&&end_date!='')
//         {
//             $('#fakeLoader').show();
//              var url="<?php echo url('selectSyncDates')?>";
//              url=url+"/"+start_date+"/"+end_date;
//              console.log(url);
//              $.ajax({
//                     url:url,
//                     type:'post',
//                     success: function(response)
//                     {  
//                      console.log(response);
//                    }
//                 });
//         }
//       }
//  });
// }

  
  //console.log(start_date);
  //console.log(end_date);
</script>

<script src="<?php echo url('js/timedropper.js')?>"></script>   
<script src="<?php echo url('js/jquery.form.js')?>"></script>
<script src="<?php echo url('js/jquery.plugin.min.js')?>"></script>
<script src="<?php echo url('js/jquery.timeentry.js')?>"></script>
<!-- <script src="<?php echo url('js/settings.js')?>"></script> -->
<script src="<?php echo url('js/settings.js')?>?v=<?=md5_file(url('js/settings.js'))?>"></script>
<link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">
<script src="assets/js/timepicker/bootstrap-timepicker.min.js"></script>
<!--<script src="assets/js/daterangepicker/daterangepicker.js"></script>
<script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script> -->
<?php include 'layout/footer.php';?>
<script src="<?php echo url('js/globalize/globalize.js')?>"></script>
<script src="<?php echo url('js/globalize/globalize.culture.de-DE.js')?>"></script>
<script src="<?php echo url('js/jquery.mousewheel.js')?>"></script>
