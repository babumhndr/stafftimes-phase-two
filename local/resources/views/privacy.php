<?php 
$metaTitle= trans('privacy.privacy');;
$metaUrl='';
$metaDescription='';
$metaKeyword='';
?>
<?php  include 'layout/main_page_header.php';?>
<style type="text/css">
  .terms p
  {
    margin-left: 20px;
  }
  .terms span
  {
    color: #434546;
    font-size: 14px;
    padding-right: 5px;
  }
  .bullet
  {

  }
  .terms
  {
    margin-top: 20px;
  }
  .terms h4
  {
        margin-left: 18px;
  }
</style>
    <link rel="stylesheet" type="text/css" href="<?php echo url('css/tour.css')?>">
   <div class="row mot_header">
 <?php  include 'layout/main_page_header_nav.php';?>
 </div>
  <div class="col-md-12" style="text-align: center;"><h3><?php echo trans('privacy.privacy');?></h3></div>
  <div class="col-md-12" style="    margin-bottom: 15px;"><div class="col-md-5 hidden-sm hidden-xs"></div><div class="col-md-2" style="height: 1px; background: #000;margin-left: auto;margin-right: auto;"></div><div class="col-md-5 hidden-sm hidden-xs"></div></div>
  <div class="container terms">
  <p><?php echo trans('privacy.first_para');?></p>
  <p><?php echo trans('privacy.second_para');?></p>
  <h4><?php echo trans('privacy.Data_processing');?> :</h4>
  <p><?php echo trans('privacy.Data_body_one');?>
  <div style=" margin-left: 60px; ">
     <ul>
  <li><p><?php echo trans('privacy.Data_one');?></p></li>
  <li> <p><?php echo trans('privacy.Data_two');?></p></li>
  <li><p><?php echo trans('privacy.Data_three');?></p></li>
  <li> <p><?php echo trans('privacy.Data_four');?></p></li>
  <li><p><?php echo trans('privacy.Data_five');?></p></li>
  </ul>
  </div>
  </p>
  <p><?php echo trans('privacy.Data_body_two');?></p>
  <p><?php echo trans('privacy.Data_body_three');?></p>
  <h4><?php echo trans('privacy.Personal_info');?> :</h4>
  <p><?php echo trans('privacy.Personal_info_one');?></p>
  <p><?php echo trans('privacy.Personal_info_two');?></p>
  <h4><?php echo trans('privacy.Information_sharing');?> :</h4>
  <p><?php echo trans('privacy.Information_sharing_one');?></p>
  <h4><?php echo trans('privacy.Cookies');?> :</h4>
  <p><?php echo trans('privacy.Cookies_one');?></p>
  <p><?php echo trans('privacy.Cookies_two');?></p>
  <h4><?php echo trans('privacy.Google_analytics');?> :</h4>
  <p><?php echo trans('privacy.Google_analytics_one');?></p>
  <p><?php echo trans('privacy.Google_analytics_two');?></p>
  <h4><?php echo trans('privacy.Safety_precautions');?> :</h4>
  <p><?php echo trans('privacy.Safety_precautions_one');?></p>
  <p><?php echo trans('privacy.Safety_precautions_two');?></p>
  <h4><?php echo trans('privacy.Changes');?> :</h4>
  <p><?php echo trans('privacy.Changes_one');?></p>
  <h4><?php echo trans('privacy.More_info');?> :</h4>
  <p><?php echo trans('privacy.More_info_one');?></p>
 <a href="<?php echo url('download/STTermsConditionsprivacyEN.pdf'); ?>" download="terms_and_conditions"><button class="pdf_btn"><?php echo trans('privacy.download');?>  <i class="fa fa-file-pdf-o" style=" font-size: 18px;" aria-hidden="true"></i></button></a>
  </div>
<?php  include 'layout/main_page_footer.php';?>