
<?php  include 'layout/header.php';?>
<link rel="stylesheet" href="<?php echo url('assets/css/add_employee.css')?>">
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row invite">
			
				<div class="col-md-12 col-sm-12 col-xs-12 content">
                         <div class="panel" style="min-height: 200px;">
                              <div class="row">
                              	<div class="col-md-12">
                              		<h3>Add a new role</h3>
                              		<p>You can manage all roles in your company & assignment of people to roles in this section</p>
                              	</div>
                              	<div class="row" style="  margin: 0px;">
                                        <div class="col-md-6">
                                             <br>
                                             <input type="text" class="invite_mail_id" placeholder="Enter Role / Designation / Department / Team name"></input>
                                        </div>
                                        <div class="col-md-3">
                                             <br>
                                             <a href="add_company"><button class="btn btn-info" style="padding: 1.05em">Add this role</button></a>
                                        </div>    
                                   </div>
                              </div>
                         
                         </div>
                          <br>
                    <div class="row">
                         <div class="col-md-6 ">
                              <h3>All Roles(5)</h3>
                         </div>
                         <table class="table table-striped">
                              <thead style="background: #edf7ff">
                                   <th>Sl No</th>
                                   <th>ROLE NAME</th>
                                   <th>NO. OF STAFF</th>
                                   <th colspan="2">ACTIONS</th>
                              </thead>
                              <tr>
                                   <td>2</td>
                                   <td> Supervisor</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>
                              <tr>
                                   <td>3</td>
                                   <td>Finance Head</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>
                              <tr>
                                   <td>4</td>
                                   <td>Billing Executive</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>
                              <tr>
                                   <td>5</td>
                                   <td>Cleaning Boy</td>
                                   <td>20</td>
                                   <td><i style="color: blue" class="glyphicon glyphicon-pencil"></i></td>
                                   <td><i style="color: red" class="glyphicon glyphicon-trash"></i></td>
                                   
                              </tr>

                             
                              
                         </table>
                    </div>
				</div>
                   
		  </div>
				
				

</div>

	
		
		
	</div>

 <script src="js/jquery.form.js"></script>


 <?php  include 'layout/footer.php';?>


