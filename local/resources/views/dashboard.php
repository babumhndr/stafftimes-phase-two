 <?php  include 'layout/header.php';?>
  <?php
function MinToHours($Minutes)
	{
    	/*return  date('G.i', mktime(0,$time));*/
    $s='';
	    if ($Minutes < 0)
	    {
	        $Min = Abs($Minutes);
            $s='-';
	    }
	    else
	    {
	        $Min = $Minutes;
	    }
	    $iHours = Floor($Min / 60);
	    $Minutes = ($Min - ($iHours * 60)) / 100;
	    $tHours = $iHours + $Minutes;
	    if ($Minutes < 0)
	    {
	        $tHours = $tHours * (-1);
	    }
	    $aHours = explode(".", $tHours);
	    $iHours = $aHours[0];
	    if (empty($aHours[1]))
	    {
	        $aHours[1] = "00";
	    }
	    $Minutes = $aHours[1];
	    if (strlen($Minutes) < 2)
	    {
	        $Minutes = $Minutes ."0";
	    }
	    $tHours = $s.$iHours .".". $Minutes;
	    return $tHours;
	}
 ?>

 <link rel="stylesheet" href="<?php echo url('/assets/css/dashboard.css')?>">
			<div>
			<!-- <h3>Here starts everything&hellip;</h3> -->
			<div class="row mot_dashboard_content">
			<!-- <div class="col-md-12 col-sm-12 col-xs-12" id="cookie" style="float:left;"> -->
			</div>
			<div class="col-md-4 col-sm-6 parts" id="part_one">
				<div class="panel panel-default">
					<div class="invite_people_icon">
						<img src="<?php echo url('assets/images/invite_dashboard.png')?>" class="mail_box">
					</div>
					<div class="invite_people_heading">
						<p><?php echo trans('dashboard.invite')?></p>
					</div>
					<div class="invite_people_sub_heading">
						<p><?php echo trans('dashboard.invite_msg')?></p>
					</div>
					<div class="submit_invite" id="inviteForm">
					<form id="invite" method="post" action="invitePeople"style=" text-align: center; ">
					    <input type="text" class="invite_mail_id" id="name" name="name" placeholder="<?php echo trans('dashboard.invite_name')?>"></input>
						<input type="text" class="invite_mail_id" id="email" name="email" placeholder="<?php echo trans('dashboard.invite_mail')?>"></input>
						 <input type="hidden" id="time" name="time"></input>
						<button class="send_invite" type="submit" id="send_invite_btn"><?php echo trans('dashboard.invite_btn')?></button>
						<div id="loader" style="display:none">
							<img src="<?php echo url('image/hourglass.gif') ?>" style="width: 35px;height: 35px;">
						</div>
					</form>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 parts" id="part_two">


					<?php

						if($data['active_users'] > 0){
							$userCount = $data['active_users'];
						}
						else{
							$userCount = 0;
						}

					?>
					<div class="xe-widget xe-counter">
						<div class="xe-icon">
							<img src="<?php echo url('assets/images/image.png')?>">
						</div>
						<div class="xe-label">
							<strong class="num"><?=$userCount?></strong>
							<span><?php echo trans('dashboard.using_stafftimes')?></span>
						</div>
					</div>
					
					<div class="xe-widget xe-counter">
						<div class="xe-icon">
							<img src="<?php echo url('assets/images/image2.png')?>">
						</div>
						<div class="xe-label">
							<strong class="num"><?=$data['total_overtime_by_month']?> <?php echo trans('dashboard.hours')?></strong>
							<span><?php echo trans('dashboard.overtime_hours')?></span>
						</div>
					</div>
					
					<div class="xe-widget xe-counter end">
						<div class="xe-icon">
							<img src="<?php echo url('assets/images/image1.png')?>">
						</div>
						<div class="xe-label">
							<strong class="num"><?=$data['todays_avg_overtime']?> <?php echo trans('dashboard.hours')?></strong>
							<span><?php echo trans('dashboard.average_overtime')?></span>
						</div>
					</div>
					
				</div>

				<!-- <div class="col-md-4 col-sm-6 parts" id="part_two_temp">
					<img src="<?php echo url('image/blur_time.png') ?>"/>
				</div> -->

				<div class="col-md-4 col-sm-12 parts" id="part_three">
			<div class="xe-widget xe-conversations mot_employees">
				<div class="mot_employee_heading">		
				<div class="xe-label mot_top_employee_heading">
					<small class="top_employee_heading"><?php echo trans('dashboard.top_employee_heading')?></small>
				</div>
				<a href="<?php echo url('listing')?>" class="top_employee_view_more"><?php echo trans('dashboard.veiwall')?>&nbsp&nbsp<i class="fa fa-angle-double-right"></i></a>
				</div>
				<div class="xe-body">
					<ul class="list-unstyled">
						<?php
						$employeeCount=count($data['top_employee']);
						if ($employeeCount>4) {
						$employeeCount=4;
						}
							for($i=0;$i<$employeeCount;$i++){
								$overtime=$data["top_employee"][$i]["total_overtime"];
								/*$overtime=number_format((float)($overtime / 60), 2, '.', '');*/
								$overtime=number_format(MinToHours($overtime), 2);
								echo '<li>
							<div class="mot_top_employee_list">
								<div class="xe-comment-entry ">
									<a href="employee_profile/'.$data["top_employee"][$i]["_id"].'" class="xe-user-img">
										<img src="'.$data["top_employee"][$i]["profile_image"].'" class="img-circle" width="40" />
									</a>
									<div class="xe-comment">
										<a href="employee_profile/'.$data["top_employee"][$i]["_id"].'" class="xe-user-name" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title='.$data["top_employee"][$i]["name"].'>
										<strong>'.$data["top_employee"][$i]["name"].'</strong>
										</a>
										<small>'.$data["top_employee"][$i]["department"].'</small>
									</div>
								</div>
								<span class="no_of_hours">
									<p class="number">'.$overtime.'</p>
									<small>'.trans('dashboard.hours').'</small>
								</span>
							</div>
						</li>';
							}
						?>
					</ul>
				</div>
			</div>
		</div>
		<!-- <div class="col-md-4 col-sm-12 col-xs-12 parts" id="part_three_temp">
			<img src="<?php echo url('image/blur_emp.png') ?>"/>
		</div> -->
	</div>
	<div class="row mot_time_graph">
		<div class="col-sm-12">
			<div class="panel graph_heading">

				<!-- <div class="mot_graph_heading">
				<div class="panel-heading graph_header">
					<p><?php echo trans('dashboard.average_offset')?></p>
				</div>
				</div> -->
				<div class="panel-body" style="padding: 10px !important">	
							<div id="container" style="height: 310px; width: 100%;"></div>
							<script src="https://code.highcharts.com/highcharts.js"></script>
							<script src="https://code.highcharts.com/modules/exporting.js"></script>

							<!-- <div id="container_temp" style="height: 310px; width: 100%;">
								<img src="<?php echo url('image/blur_graph.png') ?>"/>
							</div> -->
							
				</div>
			</div>
						
		</div>
	</div>
</div>

	</div>

 <script src="js/jquery.form.js"></script>
 <script>
 	(function() {
 		$('#send_invite_btn').hide();
 		$('#loader').show();
		$('form').ajaxForm({
    		beforeSend: function () {
                     var name = $('#name').val();
                    var email = $('#email').val();
                    var trailStatus='<?php echo $data['data']['trail_status'];?>';
                    var paymentStatus='<?php echo $data['data']['payment_status'];?>';
                    var isValid = true;
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
       			if(name == null || email == null || name == '' || email == '')
        			{
        				isValid = false;
         				swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.all_fields');?>", "error");
       				}
       				else if(regex.test(email) == false)
                    {
                    	isValid = false;
        				swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.check_email');?>", "error");
    				}
       			else if (paymentStatus=="pending") 
   					{
   						if (trailStatus=="expired")
   						 	{
   						 		isValid = false;
								swal({  
                     				title: "<?php echo trans('popup.error_');?>", 
                     				text: "<?php echo trans('popup.expired_trail');?>",   
                     				type: "error",   
                     				confirmButtonText : "<?php echo trans('popup.go_to_plans');?>"
                    				},
                    				function(){
                         				window.location.href = '<?php echo url('plansandprice'); ?>';

                   					 });
   						 	}
   					}
   				else if (paymentStatus=="expired")  
   					{
   						isValid = false;
   								swal({  
                     				title: "<?php echo trans('popup.error_');?>", 
                     				text: "<?php echo trans('popup.expired_subscription');?>",   
                     				type: "error",   
                     				confirmButtonText : "<?php echo trans('popup.go_to_plans');?>"
                    				},
                    				function(){
                         				window.location.href = '<?php echo url('plansandprice'); ?>';

                   					 });
   					}
        		if(isValid == false)
        			{
							return false;
					}
 				},
    		uploadProgress: function() {
      
    			},
    		success: function(data) {

    			if(data.status=='success')
                   {
                   	swal("<?php echo trans('popup.great');?>", "<?php echo trans('popup.invite_employee');?>", "success");
                   	$("#invite")[0].reset();
                   }
                  else if(data.status=='failure'){	
                  	swal("<?php echo trans('popup.oops');?>", "<?php echo trans('popup.already_registered');?>", "error");
                  }		
    			},		
    		complete: function() {
    
   				 }
			}); 

		})();
		$('#send_invite_btn').show();
 		$('#loader').hide();	
	</script>
	<script type="text/javascript">
		$( window ).load(function()
		{
			$.ajax({
			url:'firstVisit',
			type:'post',
			success:function(response){
				console.log(response);
				        if(response=='success')
      			{
				 /* swal({   title: "<?php echo trans('popup.welcome');?>",   
				  			text: "<?php echo trans('popup.invite_yourself');?>",   
				  			
				  		},
				  		function(){
                       
                  $('#name').val($('#name').val() + '<?php $res=Auth::user(); echo $res['company_name']?>');
                   $('#email').val($('#email').val() + '<?php $res=Auth::user(); echo $res['email']?>');
                        });*/
                        $('#modal_box_display').click();
                        $('#cookie').show();
                        var text = '<?php echo trans('popup.cookie_text');?>';
                        var privacy = '<?php echo trans('popup.privacy_policy');?>';
                        var alert_div="<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button> "+text+"<a href='<?php echo route('privacy')?>'>"+privacy+"</a></div>";
                        $('#cookie').append(alert_div);
				 }
			}
		});
		});
	</script>
	</body>
				  <script>
		$( ".whole" ).hover(function() {
			$( ".whole" ).removeClass("on_load");
		
	});
	</script>
	 <?php  include 'layout/footer.php';?>
	
	<script>
	var titleTranslation="<?php echo trans('popup.dashboard_graph_title');?>";
	var timeTranslation="<?php echo trans('popup.dashboard_graph_time');?>";
	var overtimeTranslation="<?php echo trans('popup.dashboard_graph_overtime');?>";
	var avergeOffsetTranslation="<?php echo trans('popup.dashboard_graph_averge_offset');?>";
	var hrTanslation="<?php echo trans('translate.hr');?>";
	$(function () {
		var categories = <?php echo json_encode($data["chart_data"]["date"]); ?>;
		var overtime = <?php echo json_encode($data["chart_data"]["total_overtime"]); ?>;
		var offset = <?php echo json_encode($data["chart_data"]["avg_offset"]); ?>;
    Highcharts.chart('container', {
        title: {
            text: titleTranslation,
            //x: -20 //center
        },
        subtitle: {
            text: '',
            //x: -20
        },
        xAxis: {
            categories:categories
        },
        yAxis: {
            title: {
                text: timeTranslation
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: hrTanslation
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        credits: {
      enabled: false
  		},
  		exporting: {
         enabled: false
		},
        series: [ {
            name: overtimeTranslation,
            data: overtime
        }, {
            name: avergeOffsetTranslation,
            data: offset
        }]
    });
});
	/*var employeeCountForDisplay="<?php $employeeCount=count($data['top_employee']);?>";
	var part_two="<?php echo $data['active_users']?>";
	if (employeeCountForDisplay!="0") 
	{
		$('#part_two_temp').hide();
		$('#part_two').show();
	}
	else
	{
		$('#part_two_temp').show();
		$('#part_two').hide();
	}
	if (part_two!="0") 
	{
		$('#part_three_temp').hide();
		$('#part_three').show();
	}
	else
	{
		$('#part_three_temp').show();
		$('#part_three').hide();
	}*/
	</script>
