<!DOCTYPE html>
<html>
    <head>
        <title>Staff Times</title>
        <link rel="shortcut icon" href="<?php echo url('image/new-favicon-logo1.png')?>">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo url('/assets/css/css.css')?>">
        <link rel="stylesheet" href="<?php echo url('css/font-awesome.min.css')?>">
        <link rel="stylesheet" href="<?php echo url('css/bootstrap.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo url('css/login.css')?>">
        <link href="<?php echo url('/css/rgen_min.css')?>" rel="stylesheet">
        <script src="<?php echo url('assets/js/jquery-1.11.1.min.js')?>"></script>
        <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-core.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-forms.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-components.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/xenon-skins.css')?>">
    <link rel="stylesheet" href="<?php echo url('/assets/css/sweet-alert.css')?>">
      <script src="<?php echo url('js/moment-with-locales.js')?>"></script>
      <style>
    #footer {
    position: fixed;
    bottom: 0;
    width: 100%;
    background: #202020;
    border: none;
    padding-top: 15px;
    padding-bottom: 15px;
    padding-left: 15px;
    padding-right: 0px;
    text-align: center;
}
#footer {
    font-size: 12px;
    line-height: 14px;
    color: #66696a;
    font-weight: normal;
    padding-top: 17px;
    padding-bottom: 10px;
    margin-left: auto;
    margin-right: auto;
    border-top: 1px solid #ccc;
}
#footer {
    min-width: 0;
    padding-left: 0px;
    padding-right: 0px;
}
#footer .wrapper {
    padding-left: 25px;
    padding-right: 25px;
    width: auto;
    display: -moz-inline-stack;
    display: inline-block;
    vertical-align: middle;
    zoom: 1;
        clear: both;
}
#nav-legal{
    padding-left: 0px;
}
#footer #nav-legal {
    font-weight: bold;
    font-size: 12px;
    line-height: 14px;
    color: #fff;
    font-weight: normal;
    display: -moz-inline-stack;
    display: inline-block;
    vertical-align: middle;
    zoom: 1;
    list-style: none;
}
#footer #nav-legal li {
    float: none;
    display: inline-block;
    margin-left: 0;
    padding-left: 15px;
    padding-right: 15px;
}
#footer #copyright.guest span {
    height: 16px;
    width: 62px;
    text-indent: -119988px;
    overflow: hidden;
    text-align: left;
    margin-left: 0px;
    margin-right: 5px;
    padding: 0;
    background: url() no-repeat scroll top left transparent;
    margin-top: 0px;
    margin-bottom: 0px;
    float: left;
}
.guest {
    float: left;
}
#footer #nav-legal a {
    font-size: 12px;
    line-height: 14px;
    font-weight: normal;
    text-decoration: none;
    color: #FFF;
    font-weight: bold;
}
.logo {/*
                text-align: center;*/
                color: white;
                padding-left: 0px;
                float: left;
                padding-top: 23px;
                padding-right: 15px;
            }
.logo_div {
    width: 100%;
    float: left;
    padding-left: 15px;
}
.align
{
    text-align: center;
    padding: 6px;
    margin-top: 20px;
}

.mobile-only-message{
    color: #f7f7f7;
    font-size: 1.5em;
    text-align: center;
    line-height: 40px;
    margin-top: 0px;
    padding: 20px;
    overflow: auto;
    min-height: 900px;
}
</style>

    </head>
    <body>
    <div>
    <?php
        $msg = '';
        $msgDisplay = 'display:block';
        $msg1 = '';
        $msgDisplay1 = 'display:block';
        if(Session::has('message')){
            $msg = Session::get('message');
        }
        if($msg == ''){
            $msgDisplay = 'display:none';
        }
        if(Session::has('message1')){
            $msg1 = Session::get('message1');
        }
        if($msg1 == ''){
            $msgDisplay1 = 'display:none';
        }
        $email = '';
        if(Session::has('email')){
            $email = base64_decode(Session::get('email'));
        }
    ?>
    	<div class="row mot_login">
    		<div id="container" class="container">
                <div id="mobile-only">
                    <div class="logo_div">
                            <a href="./" class="logo">
                                <img src="<?php echo url('image/webapp.png')?>" width="55" alt="">
                            </a>
                            <p class="mot_login_heading">Staff Times.</p>
                        </div>

                    <div>
                       <p class="mobile-only-message"><?php echo trans('welcome.login_message')?></p>
                    </div>
                </div>
    			<div class="mot_login_form" id="desktop-only">
    				<form method='POST' action="companyLogin">
    				<div class="col-md-12">
                        <div class="logo_div">
                            <a href="./" class="logo">
                                <img src="<?php echo url('image/webapp.png')?>" width="55" alt="">
                            </a>
                            <p class="mot_login_heading">Staff Times.</p>
                        </div>
        				<div class="left-inner-addon">
                            <span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
        					<input type="text" id="name" name="email" placeholder="<?php echo trans('login.email')?>" value="<?= $email;?>"  required>
        				</div>
        				<div class="left-inner-addon">
                            <span><i class="fa fa-lock" aria-hidden="true"></i></i></span>
        				<input type="password" id="password" name="password" placeholder="<?php echo trans('login.password')?>" required>
        				</div>
      					<button type="submit" class="form-control" id="sign_in"><?php echo trans('login.signin')?></button>
      					<div class="login_cannot_access">
      						<label for="cant_access"><a href="#" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="cant_access_label"><?php echo trans('login.cant_access')?></a></label>
    					</div>
                        <!-- <p style="color:#ad0404;text-align: center;font-size: 12px;margin-top: -5px;"><?=$msg?></p> -->
                        <div class="alert alert-danger align" style="<?=$msgDisplay?>">
                        <?=$msg?>
                        </div>
                        <div class="alert alert-success align" style="<?=$msgDisplay1?>">
                         <?=$msg1?>
                        </div>
                    </div>
					</form>
				</div>
            </div>
            <script type="text/javascript">
                 var screensize = document.documentElement.clientWidth;
                    if (screensize  < 991) {
                       $('#mobile-only').show();
                       $('#desktop-only').hide();
                    }
                    else {
                        $('#mobile-only').hide();
                       $('#desktop-only').show();
                    }
            </script>
            <div id="footer" class="remote-nav" role="contentinfo">
                <div class="wrapper">
                    <p id="copyright" class="guest"><?php echo trans('main_page_footer.myovertime_copyright') ?>&nbsp&nbsp<i class="fa fa-copyright" aria-hidden="true"></i>&nbsp&nbsp <?php echo trans('main_page_footer.myovertime_right') ?></p>
                        <ul id="nav-legal">
                            <li><a href="<?php echo route('/')?>"><?php echo trans('main_page_header_nav.home');?></a></li>
                            <li><a href="<?php echo route('contactus')?>"><?php echo trans('main_page_header_nav.contactus');?></a></li>
                            <li><a href="<?php echo route('privacy')?>"><?php echo trans('main_page_header_nav.privacy');?></a></li>
                            <li><a href="<?php echo route('faq')?>" target="_blank"><?php echo trans('main_page_header_nav.faq');?></a></li>
                            <li><a href="<?php echo route('tour')?>"><?php echo trans('main_page_header_nav.tour');?></a></li>
                            <li><a href="<?php echo route('termsandcondition')?>"><?php echo trans('main_page_header_nav.terms_conditions');?></a></li>
                        </ul>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-6">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" id="close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo trans('login.reset')?></h4>
                </div>
                
                <div class="modal-body">
                
                    <form method="POST" action="forgotPassword" id="enter_email">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-group">
                                <input type="text" class="form-control" id="field-1" name="email" placeholder="<?php echo trans('login.email')?>" autofocus="autofocus" value="<?= $email;?>" required>
                                <div style="text-align:center;">
                                <button type="submit" class="form-control" id="mail_send"><?php echo trans('login.send')?></button>
                                <img id="loader" src="image/hourglass.gif" style="width: 35px;height: 35px;display: none;margin-top: 15px;"/>
                                </div>
                            </div>  
                            
                        </div>
                    </div>
                    </form>
                 </div>   
                </div>
                </div>
                </div>
                <script src="js/jquery.form.js"></script>
                <script>
 (function() {
$('#enter_email').ajaxForm({
    beforeSend: function () {
        var email = $('#field-1').val();
        var message;
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    console.log(regex.test(email));
          var isValid = true;
             if(email == null ||email == '')
              {
                     swal({  
                         title: "<?php echo trans('popup.error_');?>", 
                         text: "<?php echo trans('popup.email');?>",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });
                     isValid= false;
              }
              else if(regex.test(email) == false){
                isValid = false;
                swal("<?php echo trans('popup.error_');?>!", "<?php echo trans('popup.check_email');?>", "error");
                 $('#mail_send').show();
                $('#loader').hide();
            }
            if(isValid == false) {
                 $('#mail_send').show();
                $('#loader').hide();
                return false;
            }
            else{
        $('#mail_send').hide();
        $('#loader').show();
    }

    
 },
    success: function(data) {
        /*console.log(data);*/
 if(data.status=='success')
 {
  swal({  
                         title: "<?php echo trans('popup.success');?>", 
                         text: "<?php echo trans('popup.succes_message');?>",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){
                            $('#close_btn').click();
                            $('#mail_send').show();
                            $('#loader').hide();

                        });

 }
 else if(data.status=='failure'){
$('#mail_send').show();
$('#loader').hide();
swal({  
                         title:"<?php echo trans('popup.failure');?>", 
                         text: "<?php echo trans('popup.email_not_registerd');?>",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });

 }
}
}); 

})();
/*var today = new Date();
var dayOfWeekStartingSundayZeroIndexBased = today.getDay(); // 0 : Sunday ,1 : Monday,2,3,4,5,6 : Saturday
var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()-6);
var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+7);
var start_date1 =moment(mondayOfWeek).format('D-M-YYYY');
var end_date1 =moment(sundayOfWeek).format('D-M-YYYY');
localStorage.start_date=start_date1;
localStorage.end_date=end_date1;*/
</script>
        <script src="<?php echo url('js/bootstrap.js')?>"></script>
        <script src="<?php echo url('assets/js/TweenMax.min.js')?>"></script>
    <script src="<?php echo url('assets/js/resizeable.js')?>"></script>
    <script src="<?php echo url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo url('assets/js/joinable.js')?>"></script>
    <script src="<?php echo url('assets/js/xenon-api.js')?>"></script>
    <script src="<?php echo url('assets/js/xenon-toggles.js')?>"></script>
    <script src="<?php echo url('assets/js/sweet-alert.js')?>"></script>


    <script src="<?php echo url('assets/js/xenon-widgets.js')?>"></script>
    
    </body>
</html>


