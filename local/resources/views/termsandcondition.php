<?php 
$metaTitle=trans('terms.terms');;
$metaUrl='';
$metaDescription='';
$metaKeyword='';
?>
<?php  include 'layout/main_page_header.php';?>
<style type="text/css">
  .terms p
  {
    margin-left: 20px;
  }
  .terms span
  {
    color: #434546;
    font-size: 14px;
    padding-right: 5px;
  }
  .bullet
  {

  }
  .terms
  {
    margin-top: 20px;
  }
  .pdf_btn
  {
    margin-top: 7px;
    margin-left: 18px;
    padding: 4px;
  }
</style>
    <link rel="stylesheet" type="text/css" href="<?php echo url('css/tour.css')?>">
   <div class="row mot_header">
 <?php  include 'layout/main_page_header_nav.php';?>
 </div>
  <div class="col-md-12" style="text-align: center;"><h3><?php echo trans('terms.terms');?></h3></div>
  <div class="col-md-12" style="    margin-bottom: 15px;"><div class="col-md-5 hidden-sm hidden-xs"></div><div class="col-md-2" style="height: 1px; background: #000;margin-left: auto;margin-right: auto;"></div><div class="col-md-5 hidden-sm hidden-xs"></div></div>
  <div class="container terms">
  <h4><?php echo trans('terms.validity');?></h4>
  <p><?php echo trans('terms.validity_body');?></p>
  <h4><?php echo trans('terms.DGs_services');?></h4>
  <p><!-- <span>1)</span> --> <?php echo trans('terms.DGs_services_body_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.DGs_services_body_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.DGs_services_body_three');?></p>
  <h4><?php echo trans('terms.Free_of_charge');?></h4>
  <p><!-- <span>1)</span> -->  <?php echo trans('terms.Free_of_charge_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.Free_of_charge_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.Free_of_charge_three');?></p>
  <p><!-- <span>4)</span> -->  <?php echo trans('terms.Free_of_charge_four');?></p>
  <h4><?php echo trans('terms.Payment_Invoice');?></h4>
  <p><!-- <span>1)</span> -->  <?php echo trans('terms.Payment_Invoice_body_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.Payment_Invoice_body_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.Payment_Invoice_body_three');?></p>
  <h4><?php echo trans('terms.Termination');?></h4>
  <p><!-- <span>1)</span> -->  <?php echo trans('terms.Termination_body_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.Termination_body_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.Termination_body_three');?></p>
  <p><!-- <span>4)</span> -->  <?php echo trans('terms.Termination_body_four');?></p>
  <p><!-- <span>5)</span> -->  <?php echo trans('terms.Termination_body_five');?></p>
  <p><!-- <span>6)</span> -->  <?php echo trans('terms.Termination_body_six');?></p>
  <h4><?php echo trans('terms.User_obligations');?></h4>
  <p><!-- <span>1)</span> -->  <?php echo trans('terms.User_obligations_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.User_obligations_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.User_obligations_three');?>
  <div style=" margin-left: 60px; ">
     <ul>
  <li><p><?php echo trans('terms.User_obligations_sub_one');?></p></li>
  <li> <p><?php echo trans('terms.User_obligations_sub_two');?></p></li>
  </ul>
  </div>
  </p>
  <p><!-- <span>4)</span> -->  <?php echo trans('terms.User_obligations_four');?></p>
  <h4><?php echo trans('terms.Usage_rights');?></h4>
  <p><!-- <span>1)</span> -->  <?php echo trans('terms.Usage_rights_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.Usage_rights_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.Usage_rights_three');?></p>
  <h4><?php echo trans('terms.Availability');?></h4>
  <p><?php echo trans('terms.Availability_one');?></p>
  <h4><?php echo trans('terms.Data');?></h4>
  <p><!-- <span>1)</span> -->  <?php echo trans('terms.Data_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.Data_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.Data_three');?></p>
  <p><!-- <span>4)</span> -->  <?php echo trans('terms.Data_four');?></p>
  <p><!-- <span>5)</span> -->  <?php echo trans('terms.Data_five');?></p>
  <h4><?php echo trans('terms.Changes');?></h4>
  <p><!-- <span>1)</span> -->  <?php echo trans('terms.Changes_one');?></p>
  <p><!-- <span>2)</span> -->  <?php echo trans('terms.Changes_two');?></p>
  <p><!-- <span>3)</span> -->  <?php echo trans('terms.Changes_three');?></p>
  <h4><?php echo trans('terms.Final');?></h4>
  <p><?php echo trans('terms.Final_one');?></p>
  <a href="<?php echo url('download/STTermsConditionsprivacyEN.pdf'); ?>" download="terms_and_conditions"><button class="pdf_btn"><?php echo trans('terms.download');?> <i class="fa fa-file-pdf-o" style=" font-size: 18px;" aria-hidden="true"></i></button></a>
  </div>
<?php  include 'layout/main_page_footer.php';?>