<?php  include 'layout/header.php';?>
<style type="text/css">

      th, td { white-space: nowrap; }
        div.container {
        width: 100%;
        }
         .dataTables_scrollBody::-webkit-scrollbar-track
{
    //-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    //border-radius: 10px;
    background-color: #D4D4D4;
}

.dataTables_scrollBody::-webkit-scrollbar
{
    height: 6px;
    background-color: #67B0E1;
}

.dataTables_scrollBody::-webkit-scrollbar-thumb
{
   
    background-color: #67B0E1;
}

    #ui-datepicker-div
    {
        z-index:55 !important;
    }
    #fakeLoader{
    display: none;
    top: inherit !important;
    left: inherit !important;
    background-color: rgb(238, 238, 238)!important;
    }
    .spinner2
    {
        left: 40% !important;
        top: 40% !important;
        color: #000;
    } 
    .container1 > div, .container2 > div, .container3 > div {
    background-color: #000 !important;
    }
    .
    {
            width: 26px !important;
    }
    .dataTables_wrapper .dataTables_filter input {
    border: 1px;
    padding: 3px;
    }
.mobileSelect-container.white>div {
    width: 25%;
    margin-left: auto;
    margin-right: auto;
    height: 70%;
}
.btn-mobileSelect-gen {
    width: 158px !important;
    font-family: 'Open Sans';
    background: #cfd0d5;
    position: relative;
    top: 5px;
    padding: 5px;
}
.mobileSelect1-container.white>div {
    width: 25%;
    margin-left: auto;
    margin-right: auto;
    height: 70%;
}
.btn-mobileSelect1-gen {
    width: 155px !important;
    font-family: 'Open Sans';
    background: #cfd0d5;
    position: relative;
    top: 5px;
    padding: 5px;
}
.dataTables_wrapper .table thead > tr .sorting_asc
{
    background: #eeeeee !important;
}
.icon{
        font-size: 17px;
}
.range
{
        margin-bottom: 0px;
    position: relative;
    top: 9px;
    color: #656667;
}
.date_range
{
        position: relative;
    top: 3px;
}
.ms-options-wrap, .ms-options-wrap * {
    list-style-type: none;
    padding-left: 2px;
    display: block;
    font: status-bar;
}
.filter_icon
{
    width: 10%;
    float: left;
    font-size: 23px;
    margin-left: 10px;
}
.select_filter
{
        width: 70%;
    float: left;
}
.ms-options-wrap > .ms-options > .ms-selectall.global
{
   color: #2392ec
}
div.dt-button-collection
{
        height: 255px;
    overflow-y: scroll !important;
}
 div.dt-button-collection button.dt-button:active:not(.disabled), div.dt-button-collection button.dt-button.active:not(.disabled), div.dt-button-collection div.dt-button:active:not(.disabled), div.dt-button-collection div.dt-button.active:not(.disabled), div.dt-button-collection a.dt-button:active:not(.disabled), div.dt-button-collection a.dt-button.active:not(.disabled) {
    background:  #fff !important;
    box-shadow: none !important;
    border-radius: 1px !important;
    font-size: 13px !important;
}
div.dt-button-collection button.dt-button, div.dt-button-collection div.dt-button, div.dt-button-collection a.dt-button
{
   background: #67b0e1!important;
    box-shadow: none !important;
    border-radius: 1px !important;
    font-size: 13px !important; 
}
div.dt-button-background
{
        background: rgba(0,0,0,0.7)!important;
}
@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
    .main-content {
        width: 100%;
    }
}

</style>
<?php 
$employeeName='';
if (!empty($data['employee'][0]['name']))
    {
        $employeeName=$data['employee'][0]['name'];
    }
 $separator=$data['separator'];
 $employeeBaseUrl=url('employee_profile/'.$data['employee_details']['employee_id']);
 $employeeProfileLink='<a href="'.$employeeBaseUrl.'" style="color:#2392ec">'.$employeeName.'</a>';
?>
<script type="text/javascript">
    var employeeName="<?= $employeeName?>";
    var separator='<?php if (!empty($separator['data'])) {echo $separator['data'];} ?>';
</script>
<?php $languageTranslation=trans("translate.lang"); ?>
<script type="text/javascript">
    var languageType="<?= $languageTranslation?>";
    var languageUrl="";
    var pageTrans="";
    var ofTrans="";
    if (languageType=="de") 
    {
        languageUrl="<?= url('js/German.json') ?>";
        pageTrans="Seite ";
        ofTrans=" von ";
    }
    else
    {
        languageUrl="<?= url('js/English.json') ?>";
        pageTrans="Page ";
        ofTrans=" of ";
    }
</script>
<link rel="stylesheet" href="<?php echo url('css/bootstrap-fullscreen-select.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.structure.min.css')?>">
<link rel="stylesheet" href="<?php echo url('css/jquery-ui.theme.min.css')?>">
<link rel="stylesheet" href="<?php echo url('assets/css/reports.css')?>">
<script src="<?php echo url('js/replace.js')?>"></script>
<link href="<?php echo url('css/fakeLoader.css')?>" rel="stylesheet">
            <!-- <h3>Here starts everything&hellip;</h3> -->
            <div class="row reports">
                <div class="col-md-12 link">
                    <p> 
                <span class="template_link"><a href="<?php echo url('dashboard')?>"><?php echo trans ('header.dashboard')?></a></span> / <span class="template_link"><a href="<?php echo url('listing')?>" ><?php echo trans ('header.Listing')?></a></span> /
                <a><?php echo trans ('report_links.title')." - ".$employeeProfileLink?></a>
        
                    </p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 content">
                    <div class="fill ">
                        
                        <?php include 'layout/report_links.php' ?>  
                    <div class="col-md-12 col-sm-12 col-xs-12 second">
                    <div class="fakeloader" id="fakeLoader" ></div>
                        <div class="col-md-4 col-sm-12 col-xs-12 padfirst" style=" margin-top: 4px; ">
                        <div class="fill">
                        <form >                       
                        <div class="col-md-12 col-sm-12 col-xs-12 half">
                            <div class="fill">
                            <span class="input_label">&nbsp;<?php echo trans ('reports.date_range')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="start_date" class="form-control ">         
                            </div>
                            <span class="input_label">&nbsp;<?php echo trans ('report_links.to')?>&nbsp;</span>
                            <div class="input-group box">
                                <input type="text" id="end_date" class="form-control">         
                            </div>
                            </div>
                        </div>
                        </form>
                        </div>
                        </div>
                         <div class="col-md-4 col-sm-12 col-xs-12 center_align padcenter" id="app">
                         <!-- <button id="true" type="button">ddddd</button>
                         <button id="false" type="button">fffff</button> -->
                         <!--    <div class="col-md-12 col-sm-12 col-xs-12 for_activity_select" style="margin-top: 11px; font-size: 14px;">
                            <p class="small_strong" style="color: #676767;"><?php echo trans ('reports.date_range')?>:<span id="header_date_range"> </span></p>
                            </div> -->
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 padlast"> 
                            <div class="padlastfloat" style="float:right">
                                <form>
                                <label class="filtered"><?php echo trans ('reports.filtered')?> :</label>
                               <!--  <input type="select" id="search_by_activity" class="search_by_activity" /> -->
                               <select id="filtered_by_activity" multiple>
                                   <?php 
                                   if ($data['activity']['status']=="success") {
                                       for ($i=0; $i < sizeof($data['activity']['response']); $i++) { 
                                        echo "<option value='".$data['activity']['response'][$i]['_id']."'>".$data['activity']['response'][$i]['activity_name']."</option>";
                                   }
                                   }
                                   else
                                   {
                                    echo "<option>no activity</option>";
                                   }
                                   ?>
                               </select>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 third">
                        <div class="col-md-4 col-sm-4 col-xs-12 center for_activity_select">
                            <p class="small_strong"><?php echo trans ('reports.bal_range')?>: <span id="header_balance_range"></span></p>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 center for_activity_select">
                            <p class="small_strong"><?php echo trans ('reports.bal_range_start')?>: <span id="header_balance_range_at_start">0.00</span></p>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 center for_activity_select">
                            <p class="small_strong"><?php echo trans ('reports.bal_range_end')?>: <span id="header_balance_range_at_end"> 0.00</span></p>

                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 fourth">
                    <div class="panel-body over" style="display:none !important">       
                    <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="align_left_tr"> <?php echo trans ('reports.date')?> </th>
                                <th class="align_left_tr"> <?php echo trans ('reports.activity')?>  </th>
                                <th class=""> <?php echo trans ('reports.start')?>  </th>
                                <th class=""> <?php echo trans ('reports.end')?>  </th>
                                <th class=""><?php echo trans ('reports.break')?></th>
                                <th class=""><?php echo trans ('reports.rate')?></th>
                                <th class=" activity_slection" > <?php echo trans ('reports.condition1')?></th>
                                <th class=" activity_slection" > <?php echo trans ('reports.from')?>  </th>
                                <th class=" activity_slection" > <?php echo trans ('reports.factor')?>  </th>
                                <th class=" activity_slection"> <?php echo trans ('reports.condition2')?> </th>
                                <th class=" activity_slection" > <?php echo trans ('reports.from')?></th>
                                <th class=" activity_slection" ><?php echo trans ('reports.factor')?></th>
                                <th class=" activity_slection" ><?php echo trans ('reports.bonus_hours')?></th>
                                <th class=" activity_slection" ><?php echo trans ('reports.apply_bonus')?></th>
                                <th class=""><?php echo trans ('reports.own_account')?></th>
                                <th class=""><?php echo trans ('reports.total')?></th>
                                <th class=" activity_slection"><?php echo trans ('reports.offset')?></th>
                                <th class=" activity_slection"><?php echo trans ('reports.balance')?></th>
                                <th class=""><?php echo trans ('reports.amount')?></th>
                                <th class=""><?php echo trans ('reports.break')?><br> (min)</th>
                                <th class=""><?php echo trans ('reports.total')?><br> (min)</th>
                                <th class=" activity_slection"><?php echo trans ('reports.balance')?><br> (min)</th>
                                <th class=" activity_slection"><?php echo trans ('reports.offset')?><br> (min)</th>
                                <th class=""><?php echo trans ('reports.notes')?></th>
                            </tr>
                        </thead>
                    
                        <tbody id="data_table">

                        </tbody>
                    </table>
                    
                </div>
                </div>
                    </div>
                </div>
            </div>

        
    </div>
<script src="<?php echo url('js/bootstrap-fullscreen-select.min.js')?>"></script>
<script type="text/javascript" src="<?php echo url('js/jquery-ui.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo url('js/datepicker-de.js')?>"></script>
<script>
function fillArray(value, len) {
  if (len == 0) return [];
  var a = [value-0.4+'%'];
  while (a.length * 2 <= len) a = a.concat(a);
  if (a.length < len) a = a.concat(a.slice(0, len - a.length));
  a[1]=value+4+'%';
  a[0]=value+2+'%';
  a[len-1]=value+5+'%';
  return a;
}

function docWidth(docLength){
    if(docLength>0){
    var res =Math.floor(100/docLength);
      return fillArray(res,docLength);
  }else{
    return ['100%'];
  }
}

function fillColor(arr){
  if (arr != null) {
      for(var i=0; i<arr.length; i++){
        if(arr[i].length>0){
          if(arr[i][0].text == "Tagessumme" || arr[i][0].text == "Daily Total"){
            /*arr[i][0].fillColor = "#2392ec";*/
            arr[i].map(function(obj){
                obj.fillColor = "#c4c5c6";
            });
          }
          if (arr[i][0].text == "Total") {
            arr[i].map(function(obj){
                obj.fillColor = "#b0b1b2";
            });
          }
          if (arr[i][0].text == "Insgesamt in Tagen" || arr[i][0].text == "Total in days") {
            arr[i].map(function(obj){
                obj.fillColor = "#9a9c9e";
            });
          }
        }
      }
  }
  return arr;
}
var transOfItemSelected='<?php echo trans('translate.item_selected'); ?>';
var transOfNotingSelected='<?php echo trans('translate.no_item_selected'); ?>';
var transOfSelectAnOption='<?php echo trans('translate.select_an_option'); ?>';
var transOfClear='<?php echo trans('translate.clear'); ?>';
var transOfSave='<?php echo trans('translate.save'); ?>';
var transOfCancel='<?php echo trans('translate.cancel'); ?>';
/*alert(transOfSelectAnOption+'//'+transOfClear+'//'+transOfCancel+'//'+transOfSave);*/
/*$('#true').click(function()
{
    localStorage.coloum1='true';
    window.location.reload();
});
$('#false').click(function()
{
    localStorage.coloum1='false';
    window.location.reload();
})
console.log(localStorage.coloum1+'cs');
var bab='';
if (localStorage.coloum1=='true') 
{
    bab=true;
    console.log(bab+'cs');
}
else
{
    bab=false;
    console.log(bab+'c');
}*/
var hide0=true;
var hide1=true;
var hide2=true;
var hide3=true;
var hide4=true;
var hide5=true;
var hide6=true;
var hide7=true;
var hide8=true;
var hide9=true;
var hide10=true;
var hide11=true;
var hide12=true;
var hide13=true;
var hide14=true;
var hide15=true;
var hide16=true;
var hide17=true;
var hide18=true;
var hide19=true;
var hide20=true;
var hide21=true;
var hide22=true;
var hide23=true;

if (typeof(localStorage.coloumSeletedValue5)=='undefined') { localStorage.coloumSeletedValue5='true'}
if (typeof(localStorage.coloumSeletedValue6)=='undefined') { localStorage.coloumSeletedValue6='true'}
if (typeof(localStorage.coloumSeletedValue7)=='undefined') { localStorage.coloumSeletedValue7='true'}
if (typeof(localStorage.coloumSeletedValue8) =='undefined') {localStorage.coloumSeletedValue8='true'}
if (typeof(localStorage.coloumSeletedValue9)=='undefined') {localStorage.coloumSeletedValue9='true'}
if (typeof(localStorage.coloumSeletedValue10) =='undefined') {localStorage.coloumSeletedValue10='true'}
if (typeof(localStorage.coloumSeletedValue11) =='undefined') {localStorage.coloumSeletedValue11='true'}
if (typeof(localStorage.coloumSeletedValue12) =='undefined') {localStorage.coloumSeletedValue12='true'}
if (typeof(localStorage.coloumSeletedValue13) =='undefined') {localStorage.coloumSeletedValue13='true'}
if (typeof(localStorage.coloumSeletedValue14) =='undefined') {localStorage.coloumSeletedValue14='true'}
if (typeof(localStorage.coloumSeletedValue18) =='undefined') {localStorage.coloumSeletedValue18='true'}
if (typeof(localStorage.coloumSeletedValue19) =='undefined') {localStorage.coloumSeletedValue19='true'}
if (typeof(localStorage.coloumSeletedValue20) =='undefined') {localStorage.coloumSeletedValue20='true'}
if (typeof(localStorage.coloumSeletedValue21) =='undefined') {localStorage.coloumSeletedValue21='true'}
if (typeof(localStorage.coloumSeletedValue22) =='undefined') {localStorage.coloumSeletedValue22='true'}
console.log(localStorage.coloumSeletedValue6);
var coloumList = new Array();
coloumList[0] = ["<?php echo trans ('reports.date')?>", localStorage.coloumSeletedValue0];
coloumList[1] = ["<?php echo trans ('reports.activity')?> ", localStorage.coloumSeletedValue1];
coloumList[2] = ["<?php echo trans ('reports.start')?> ", localStorage.coloumSeletedValue2];
coloumList[3] = ["<?php echo trans ('reports.end')?>", localStorage.coloumSeletedValue3];
coloumList[4] = ["<?php echo trans ('reports.break')?>", localStorage.coloumSeletedValue4];
coloumList[5] = ["<?php echo trans ('reports.rate')?>", localStorage.coloumSeletedValue5];
coloumList[6] = ["<?php echo trans ('reports.condition1')?>", localStorage.coloumSeletedValue6];
coloumList[7] = ["<?php echo trans ('reports.from')?>", localStorage.coloumSeletedValue7];
coloumList[8] = ["<?php echo trans ('reports.factor')?>", localStorage.coloumSeletedValue8];
coloumList[9] = ["<?php echo trans ('reports.condition2')?>", localStorage.coloumSeletedValue9];
coloumList[10] = ["<?php echo trans ('reports.from')?>", localStorage.coloumSeletedValue10];
coloumList[11] = ["<?php echo trans ('reports.factor')?>", localStorage.coloumSeletedValue11];
coloumList[12] = ["<?php echo trans ('reports.bonus_hours')?>", localStorage.coloumSeletedValue12];
coloumList[13] = ["<?php echo trans ('reports.apply_bonus')?>", localStorage.coloumSeletedValue13];
coloumList[14] = ["<?php echo trans ('reports.own_account')?>", localStorage.coloumSeletedValue14];
coloumList[15] = ["<?php echo trans ('reports.total')?>", localStorage.coloumSeletedValue15];
coloumList[16] = ["<?php echo trans ('reports.offset')?>", localStorage.coloumSeletedValue16];
coloumList[17] = ["<?php echo trans ('reports.balance')?>", localStorage.coloumSeletedValue17];
coloumList[18] = ["<?php echo trans ('reports.amount')?>", localStorage.coloumSeletedValue18];
coloumList[19] = ["<?php echo trans ('reports.break')?>(min)", localStorage.coloumSeletedValue19];
coloumList[20] = ["<?php echo trans ('reports.total')?>(min)", localStorage.coloumSeletedValue20];
coloumList[21] = ["<?php echo trans ('reports.balance')?>(min)", localStorage.coloumSeletedValue21]; 
coloumList[22] = ["<?php echo trans ('reports.offset')?>(min)", localStorage.coloumSeletedValue22];
coloumList[23] = ["<?php echo trans ('reports.notes')?>", localStorage.coloumSeletedValue23];

var colArray = new Array();
colArray[0] = '0';
colArray[1] = '1';
colArray[2] = '2';
colArray[3] = '3';
colArray[4] = '4';
colArray[5] = '5';
colArray[6] = '6';
colArray[7] = '7';
colArray[8] = '8';
colArray[9] = '9';
colArray[10] = '10';
colArray[11] = '11';
colArray[12] = '12';
colArray[13] = '13';
colArray[14] = '14';
colArray[15] = '15';
colArray[16] = '16';
colArray[17] = '17';
colArray[18] = '18';
colArray[19] = '19';
colArray[20] = '20';
colArray[21] = '21';
colArray[22] = '22';
colArray[23] = '23';

console.log(coloumList);
if (localStorage.coloumSeletedValue0=="true") {hide0=false}
if (localStorage.coloumSeletedValue1=="true") {hide1=false}
if (localStorage.coloumSeletedValue2=="true") {hide2=false}
if (localStorage.coloumSeletedValue3=="true") {hide3=false}
if (localStorage.coloumSeletedValue4=="true") {hide4=false}
if (localStorage.coloumSeletedValue5=="true") {hide5=false}
if (localStorage.coloumSeletedValue6=="true") {hide6=false}
if (localStorage.coloumSeletedValue7=="true") {hide7=false}
if (localStorage.coloumSeletedValue8=="true") {hide8=false}
if (localStorage.coloumSeletedValue9=="true") {hide9=false}
if (localStorage.coloumSeletedValue10=="true") {hide10=false}
if (localStorage.coloumSeletedValue11=="true") {hide11=false}
if (localStorage.coloumSeletedValue12=="true") {hide12=false}
if (localStorage.coloumSeletedValue13=="true") {hide13=false}
if (localStorage.coloumSeletedValue14=="true") {hide14=false}
if (localStorage.coloumSeletedValue15=="true") {hide15=false}
if (localStorage.coloumSeletedValue16=="true") {hide16=false}
if (localStorage.coloumSeletedValue17=="true") {hide17=false}
if (localStorage.coloumSeletedValue18=="true") {hide18=false}
if (localStorage.coloumSeletedValue19=="true") {hide19=false}
if (localStorage.coloumSeletedValue20=="true") {hide20=false}
if (localStorage.coloumSeletedValue21=="true") {hide21=false}
if (localStorage.coloumSeletedValue22=="true") {hide22=false}
if (localStorage.coloumSeletedValue23=="true") {hide23=false}
/*var firstSelect=['6','7','8','9','10','11','12'];*/
/*localStorage["coloumList"] = JSON.stringify(coloumList);

var cars = JSON.parse(localStorage["coloumList"]);
console.log(cars);*/
 var app='';
 var selectFirst='<select id="filtered_by_coloum" multiple>';
 var selectLast="</select>";
for (var o = 0;o<coloumList.length; o++) {
    console.log(coloumList[o]);
    if (coloumList[o][1]=='true') 
    {
        app+='<option value="'+o+'"selected>'+coloumList[o][0]+'</option>';
    }
    else
    {
        app+='<option value="'+o+'">'+coloumList[o][0]+'</option>';
    }
}
var final=' <label><?php echo trans('translate.select_to_hide') ?> :&nbsp&nbsp</label>'+selectFirst+app+selectLast;
$('#app').html(final);
$('#filtered_by_coloum').mobileSelect1({title:transOfSelectAnOption,buttonSave:transOfSave,buttonClear:transOfClear,buttonCancel:transOfCancel});
$('#coloum_filter').click(function(){
    var selectedArrayOption=$('#filtered_by_coloum').val();
    console.log(selectedArrayOption);
    for (var i = 0; i < selectedArrayOption.length; i++) {
        localStorage.setItem("coloumSeletedValue"+selectedArrayOption[i], "true");
    }
    var notSelectedOption=diffInArray(colArray,selectedArrayOption);
    for (var i = 0; i < notSelectedOption.length; i++) {
        localStorage.setItem("coloumSeletedValue"+notSelectedOption[i], "false");
    }
    window.location.reload();
    });

    var time_style='<?php if (!empty($data['general_setting']['response']['0']['time_style'])) { echo $data['general_setting']['response']['0']['time_style']; } else { echo ""; } ?>';//time format from general settings
    var value_format='<?php if (!empty($data['general_setting']['response']['0']['value_format'])) { echo $data['general_setting']['response']['0']['value_format']; } else { echo ""; } ?>';//time format from general settings
    /* alert(time_style);*/
    $(".left-links li a").click(function(){
    $(this).find('i').toggleClass('fa-indent fa-outdent');
    });
$('.activity_slection').show();
var dateToday = '';
var employee_id="<?=$data['employee_details']['employee_id']?>";
var company_id="<?=$data['employee_details']['company_id']?>";
/*console.log(employee_id);
console.log(company_id);*/
var dates = $("#start_date, #end_date").datepicker({
    defaultDate: "+1w",
    firstDay: 1,
    changeMonth: true,
    numberOfMonths: 1, 
    minDate: dateToday,
    dateFormat:"dd-mm-yy",
    onSelect: function(selectedDate) {
        var option = this.id == "start_date" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            $.datepicker.regional[languageType];
        dates.not(this).datepicker("option", option, date);
        var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
       /* alert(start_date+"/"+end_date);*/
        localStorage.start_date=start_date;
        localStorage.end_date=end_date;
        if(start_date!=''&&end_date!='')
        {
             $('#fakeLoader').show();
            var res='';
            var table = $('#example-1').DataTable();
            table.destroy();
            $.ajax({
                    url: "<?php echo url('listingTimesheetWithRange/"+company_id+"/"+employee_id+"/"+start_date+"/"+end_date+"')?>",
                    type:'post',
                    success: function(response)
                    {   
                        var selectBoxContentLength=$('#filtered_by_coloum').val();
                        selectBoxContentLength=selectBoxContentLength.length;
                        selectBoxContentLength=24-selectBoxContentLength;
                         if (selectBoxContentLength>20) {
                                orientationPdf="landscape";
                                pageSize="A3";
                                 fitWidth= 60;
                                fitHeight= 50;
                            }else if (selectBoxContentLength>12){
                                 orientationPdf="landscape";
                                 pageSize="A4";
                                  fitWidth= 50;
                                fitHeight= 40;
                            } else {
                                 orientationPdf="portrait";
                                 pageSize="A4";
                                  fitWidth= 40;
                                fitHeight= 30;
                            }
                       /* console.log(selectBoxContentLength.length);*/
                        for (var i = 0; i < response['TotalTimesheetDetail'].length; i++) 
                        {
                            /*console.log((response['TotalTimesheetDetail'][i]['timesheetDetail']).length);*/
                            if ((response['TotalTimesheetDetail'][i]['timesheetDetail']).length >1) 
                            {
                                /*var ddate='';*/
                                  for (var j = 0; j < response['TotalTimesheetDetail'][i]['timesheetDetail'].length; j++) 
                                  {
                                    /*ddate =moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['date']).format('DD/MM/YYYY');*/
                                    var start_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'];
                                     var amount=decimalPoint(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['amount']);
                                    if (amount==0.0) 
                                    {
                                        amount='';
                                    }
                                    else
                                    {
                                        amount=replaceToComma(time_style,value_format,separator,amount);
                                    }
                                    var end_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'];
                                    var break_time= response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_time'];
                                    if (break_time==0 || break_time=="" || break_time==null) {break_time='';}
                                    var break_min=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_min'];
                                    if (break_min==0 || break_min=="0:00" || break_min=="" || break_min==null) {break_min='';}
                                    var total_hours=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours'];
                                    var offsets=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['offset'];
                                    var day_balances=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['day_balance'];
                                    var rate=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['rate'];
                                    var condition1 =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1'];
                                    var condition1_from =minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_from']);
                                    var condition1_factor= replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_factor']);
                                    var condition2=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2'];
                                    var condition2_from=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_from']);
                                    var condition2_factor=replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_factor']);
                                    var own_account=minutesToStrDot(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['own_account']);
                                    var bonus_hours=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['TSBonusHrs']);
                                    var apply_bonus=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['apply_bonus'];
                                    var subTotalHours=minutesToStr(response['TotalTimesheetDetail'][i]['finalTotalHours']);
                                    var subOffset=minutesToStr(response['TotalTimesheetDetail'][i]['finalOffset']);
                                    var subBalance=minutesToStr(response['TotalTimesheetDetail'][i]['finalBalance']);
                                    var subOwnAccount=response['TotalTimesheetDetail'][i]['sub_ownaccount'];
                                    var subAmount=parseFloat(Math.round(response['TotalTimesheetDetail'][i]['amount'] * 100) / 100).toFixed(2);
                                    var tick ='';
                                    if (apply_bonus==true) 
                                    {
                                        tick="<?php echo trans('translate.yes')?>";
                                    }
                                    else
                                    {
                                        tick="<?php echo trans('translate.no')?>";
                                    }
                                    if (condition1=="Time" && condition1_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                             condition1_from = moment(condition1_from, ["h:mm A"]);
                                             condition1_from = condition1_from.format("h:mm A");
                                        }
                                    }
                                     if (condition2=="Time" && condition2_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                            condition2_from = moment(condition2_from, ["h:mm A"]);
                                            condition2_from = condition2_from.format("h:mm A");
                                        }
                                    }
                                    /*console.log(time_style);*/
                                    if(start_time!='' && end_time!='' && start_time!="Flat")
                                    {
                                    if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                     start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");
                                     end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm A");
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=='8:15')
                                    {
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.replace('.',':');
                                        own_account=own_account.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                         {
                                            total_hours=total_hours.replace(':',',');
                                            offsets=offsets.replace(':',',');
                                            day_balances=day_balances.replace(':',',');
                                            subTotalHours=subTotalHours.replace('.',',');
                                            subOffset=subOffset.replace('.',',');
                                            subBalance=subBalance.replace('.',',');
                                            subOwnAccount=subOwnAccount.replace('.',',');
                                            own_account=own_account.replace('.',',');
                                            rate=rate.replace('.',',');
                                            subAmount=subAmount.replace('.',',');
                                         }
                                         else if(separator=="dot")
                                         {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');
                                         }
                                    }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                        start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                        end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                        if(value_format=='8.15')
                                        {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');

                                        }
                                        else if(value_format=='8:15')
                                        {
                                            subTotalHours=subTotalHours.replace('.',':');
                                            subOffset=subOffset.replace('.',':');
                                            subBalance=subBalance.replace('.',':');
                                            subOwnAccount=subOwnAccount.replace('.',':');
                                            own_account=own_account.replace('.',':');
                                        }
                                        else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.toString().replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                             else if(separator=="dot")
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                     else if(time_style=='Industrial')
                                    {
                                        start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                        end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.toString().replace('.',':');
                                        own_account=own_account.replace('.',':');
                                        start_time=timeToDecimal(start_time);
                                        end_time=timeToDecimal(end_time);
                                         if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                        total_hours=timeToDecimal(total_hours);
                                        offsets=timeToDecimal(offsets);
                                        day_balances=timeToDecimal(day_balances);
                                        subTotalHours=timeToDecimal(subTotalHours);
                                        subOffset=timeToDecimal(subOffset);
                                        subBalance=timeToDecimal(subBalance);
                                        subOwnAccount=timeToDecimal(subOwnAccount);
                                        own_account=timeToDecimal(own_account);
                                        if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                start_time=start_time.replace('.',',');
                                                 end_time=end_time.replace('.',',');
                                                break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                else
                                {
                                    start_time="<?php echo trans('translate.flat')?>";
                                    rate='';
                                     if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                     /*start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");*/
                                   /*  end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm");*/
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=='8:15')
                                    {
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.replace('.',':');
                                        own_account=own_account.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                         {
                                            total_hours=total_hours.replace(':',',');
                                            offsets=offsets.replace(':',',');
                                            day_balances=day_balances.replace(':',',');
                                            subTotalHours=subTotalHours.replace('.',',');
                                            subOffset=subOffset.replace('.',',');
                                            subBalance=subBalance.replace('.',',');
                                            subOwnAccount=subOwnAccount.replace('.',',');
                                            own_account=own_account.replace('.',',');
                                            rate=rate.replace('.',',');
                                            subAmount=subAmount.replace('.',',');
                                         }
                                         else if(separator=="dot")
                                         {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');
                                         }
                                    }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                       /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                                        /*end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                        if(value_format=='8.15')
                                        {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');

                                        }
                                        else if(value_format=='8:15')
                                        {
                                            subTotalHours=subTotalHours.replace('.',':');
                                            subOffset=subOffset.replace('.',':');
                                            subBalance=subBalance.replace('.',':');
                                            subOwnAccount=subOwnAccount.replace('.',':');
                                            own_account=own_account.replace('.',':');
                                        }
                                        else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.toString().replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                             else if(separator=="dot")
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                     else if(time_style=='Industrial')
                                    {
                                       /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                                      /*  end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.toString().replace('.',':');
                                        own_account=own_account.toString().replace('.',':');
                                        /*start_time=timeToDecimal(start_time);*/
                                        end_time=timeToDecimal(end_time);
                                         if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                        total_hours=timeToDecimal(total_hours);
                                        offsets=timeToDecimal(offsets);
                                        day_balances=timeToDecimal(day_balances);
                                        subTotalHours=timeToDecimal(subTotalHours);
                                        subOffset=timeToDecimal(subOffset);
                                        subBalance=timeToDecimal(subBalance);
                                        subOwnAccount=timeToDecimal(subOwnAccount);
                                        own_account=timeToDecimal(own_account);
                                        if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                               /* start_time=start_time.replace('.',',');*/
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                    var day =moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['date']).format('DD/MM/YYYY');
                                    if (j==0) 
                                    {
                                        if (response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes'] !== "(null)") {
                                            var temp_notes = response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes']
                                        }else{
                                            var temp_notes = "";
                                        }

                                         res+='<tr><td class="tiny align_left">'+day+'</td> <td class="tiny align_left">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['activity_detail']['activity_name']+'</td> <td class="tiny align_right">'+start_time+'</td> <td class="tiny align_right">'+end_time+'</td> <td class="tiny align_right">'+break_time+'</td> <td class="tiny "></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny align_right">'+own_account+'</td> <td class="tiny align_right">'+total_hours+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny align_right">'+amount+'</td> <td class="tiny align_right">'+break_min+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours_min']+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny" style="text-align: left">'+ temp_notes +'</td> </tr>'; 
                                    } 
                                    else
                                    {
                                        if (response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes'] !== "(null)") {
                                            var temp_notes = response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes']
                                        }else{
                                            var temp_notes = "";
                                        }


                                         res+='<tr><td class="tiny align_left">,,</td> <td class="tiny align_left">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['activity_detail']['activity_name']+'</td> <td class="tiny align_right">'+start_time+'</td> <td class="tiny align_right">'+end_time+'</td> <td class="tiny align_right">'+break_time+'</td> <td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny align_right">'+own_account+'</td> <td class="tiny align_right">'+total_hours+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny align_right">'+amount+'</td> <td class="tiny align_right">'+break_min+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours_min']+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny" style="text-align: left">'+ temp_notes +'</td> </tr>'; 
                                    }
                                  }
                                console.log(subTotalHours+'/'+subOffset+'/'+subBalance);
                                  res+='<tr><td class="tiny sub_total_values"><?php echo trans ('reports.daily_total')?></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values">'+rate+'</td> <td class="tiny sub_total_values">'+condition1+'</td><td class="tiny sub_total_values">'+condition1_from+'</td><td class="tiny sub_total_values">'+condition1_factor+'</td><td class="tiny sub_total_values">'+condition2+'</td><td class="tiny sub_total_values">'+condition2_from+'</td><td class="tiny sub_total_values">'+condition2_factor+'</td><td class="tiny sub_total_values align_right">'+bonus_hours+'</td><td class="tiny sub_total_values">'+tick+'</td><td class="tiny sub_total_values align_right">'+subOwnAccount+'</td><td class="tiny sub_total_values align_right">'+subTotalHours+'</td> <td class="tiny sub_total_values align_right">'+subOffset+'</td> <td class="tiny sub_total_values align_right">'+subBalance+'</td> <td class="tiny sub_total_values align_right">'+subAmount+'</td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values align_right">'+response['TotalTimesheetDetail'][i]['finalTotalHours']+'</td> <td class="tiny sub_total_values align_right">'+response['TotalTimesheetDetail'][i]['finalBalance']+'</td> <td class="tiny sub_total_values align_right">'+response['TotalTimesheetDetail'][i]['finalOffset']+'</td> <td class="tiny sub_total_values"></td> </tr>';
                            }
                            else
                            {
                                for (var j = 0; j < response['TotalTimesheetDetail'][i]['timesheetDetail'].length; j++) 
                                  {
                                    var start_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'];
                                    var end_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'];
                                     var break_time= response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_time'];
                                     if (break_time==0 || break_time=="" || break_time==null) {break_time='';}
                                     var break_min=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_min'];
                                    if (break_min==0 || break_min=="0:00" || break_min=="" || break_min==null) {break_min='';}
                                     var total_hours=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours'];
                                    var offsets=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['offset'];
                                    var day_balances=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['day_balance'];
                                    var rate=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['rate'];
                                    var amount=decimalPoint(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['amount']);
                                     var condition1 =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1'];
                                    var condition1_from =minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_from']);
                                    var condition1_factor= replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_factor']);
                                    var condition2=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2'];
                                    var condition2_from=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_from']);
                                    var condition2_factor=replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_factor']);
                                    var bonus_hours=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['TSBonusHrs']);
                                    var apply_bonus=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['apply_bonus'];
                                    var own_account=minutesToStrDot(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['own_account']);
                                   /* console.log(time_style);*/
                                   var tick ='';
                                    if (apply_bonus==true) 
                                    {
                                        tick="<?php echo trans('translate.yes')?>";
                                    }
                                    else
                                    {
                                        tick="<?php echo trans('translate.no')?>";
                                    }
                                   if (amount==0.0) {amount=""}
                                   if (condition1=="Time" && condition1_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                             condition1_from = moment(condition1_from, ["h:mm A"]);
                                             condition1_from = condition1_from.format("h:mm A");
                                        }
                                    }
                                     if (condition2=="Time" && condition2_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                            condition2_from = moment(condition2_from, ["h:mm A"]);
                                            condition2_from = condition2_from.format("h:mm A");
                                        }
                                    }
                                   if(start_time!='' && end_time!='' && start_time!="Flat")
                                    {
                                    if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                     start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");
                                     end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm A");
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                     else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                    start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='Industrial')
                                    {
                                    start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                    start_time=timeToDecimal(start_time);
                                    end_time=timeToDecimal(end_time);
                                     if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                    total_hours=timeToDecimal(total_hours);
                                    offsets=timeToDecimal(offsets);
                                    day_balances=timeToDecimal(day_balances);
                                    own_account=own_account.replace('.',':');
                                    own_account=timeToDecimal(own_account);
                                    if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                start_time=start_time.replace('.',',');
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                else
                                {
                                    start_time="<?php echo trans('translate.flat')?>";
                                    rate='';
                                    if (amount==0.0) {amount="";}
                                    if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                    /* start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");*/
                                  /*   end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm");*/
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                     else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                   /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                               /*     end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='Industrial')
                                    {
                                   /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                                  /*  end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                   /* start_time=timeToDecimal(start_time);*/
                                    end_time=timeToDecimal(end_time);
                                    if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                    total_hours=timeToDecimal(total_hours);
                                    offsets=timeToDecimal(offsets);
                                    day_balances=timeToDecimal(day_balances);
                                    own_account=own_account.replace('.',':');
                                    own_account=timeToDecimal(own_account);
                                    if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                /*start_time=start_time.replace('.',',');*/
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                    var day =moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['date']).format('DD/MM/YYYY');

                                    if (response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes'] !== "(null)") {
                                            var temp_notes = response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes']
                                        }else{
                                            var temp_notes = "";
                                        }

                                    res+='<tr><td class="tiny align_left">'+day+'</td> <td class="tiny align_left">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['activity_detail']['activity_name']+'</td> <td class="tiny align_right">'+start_time+'</td> <td class="tiny align_right">'+end_time+'</td> <td class="tiny align_right">'+break_time+'</td> <td class="tiny">'+rate+'</td><td class="tiny">'+condition1+'</td><td class="tiny">'+condition1_from+'</td><td class="tiny">'+condition1_factor+'</td><td class="tiny">'+condition2+'</td><td class="tiny">'+condition2_from+'</td><td class="tiny">'+condition2_factor+'</td><td class="tiny align_right">'+bonus_hours+'</td><td class="tiny">'+tick+'</td><td class="tiny align_right">'+own_account+'</td> <td class="tiny align_right">'+total_hours+'</td> <td class="tiny align_right">'+offsets+'</td> <td class="tiny align_right">'+day_balances+'</td> <td class="tiny align_right">'+amount+'</td> <td class="tiny align_right">'+break_min+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours_min']+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['balance_min']+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['offset_min']+'</td> <td class="tiny" style="text-align: left">'+ temp_notes +'</td> </tr>';  
                                  }
                            }

                        }
                        var finalTotalHours=minutesToStr(response['whole_total']['finalTotalHours']);
                        var ownAccountTotal=response['whole_total']['ownAccountTotal'];
                        var finalOffset=minutesToStr(response['whole_total']['finalOffset']);
                        var finalBalance=minutesToStr(response['whole_total']['finalBalance']);
                        var finalTotalHoursInDay=parseFloat(Math.round(response['whole_total']['finalTotalHoursInDay'] * 100) / 100).toFixed(2);
                        var ownAccountTotalFinal=parseFloat(Math.round(response['whole_total']['ownAccountTotalFinal'] * 100) / 100).toFixed(2);
                        var finalOffsetInDay=parseFloat(Math.round(response['whole_total']['finalOffsetInDay'] * 100) / 100).toFixed(2);
                        var finalBalanceInDay=parseFloat(Math.round(response['whole_total']['finalBalanceInDay'] * 100) / 100).toFixed(2);
                        var finalAmount=parseFloat(Math.round(response['whole_total']['finalAmount'] * 100) / 100).toFixed(2);
                        var balanceAtRangeStart=minutesToStr(response['whole_total']['balanceAtRangeStart']);
                        var balanceAtRangeEnd=minutesToStr(response['whole_total']['balanceAtRangeEnd']);
                        if (time_style=='am/pm')
                        {
                           if(value_format=='8:15')
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalOffset=finalOffset.replace('.',':');
                                        finalBalance=finalBalance.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',':');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',':');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',':');
                                         ownAccountTotal=ownAccountTotal.replace('.',':');
                                    } 
                            else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                         {
                                        finalTotalHours=finalTotalHours.replace('.',',');
                                        finalOffset=finalOffset.replace('.',',');
                                        finalBalance=finalBalance.replace('.',',');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',',');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',',');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',','); 
                                        finalAmount=finalAmount.replace('.',','); 
                                        balanceAtRangeStart=balanceAtRangeStart.replace('.',',');
                                        balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');
                                        ownAccountTotal=ownAccountTotal.toString().replace('.',',');
                                         }
                                    }
                        }
                        else if(time_style=='24 hours')
                        {
                            if(value_format=='8:15')
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalOffset=finalOffset.replace('.',':');
                                        finalBalance=finalBalance.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',':');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',':');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',':');
                                         ownAccountTotal=ownAccountTotal.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                         {
                                        finalTotalHours=finalTotalHours.replace('.',',');
                                        finalOffset=finalOffset.replace('.',',');
                                        finalBalance=finalBalance.replace('.',',');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',',');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',',');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',',');  
                                        finalAmount=finalAmount.replace('.',','); 
                                        balanceAtRangeStart=balanceAtRangeStart.replace('.',',');
                                        balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');
                                        ownAccountTotal=ownAccountTotal.toString().replace('.',',');
                                         }
                                    }
                        }
                        else if(time_style=='Industrial')
                        {
                            finalTotalHours=finalTotalHours.replace('.',':');
                            finalOffset=finalOffset.replace('.',':');
                            finalBalance=finalBalance.replace('.',':');
                            finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                            ownAccountTotalFinal=ownAccountTotalFinal.replace('.',':');
                            finalOffsetInDay=finalOffsetInDay.replace('.',':');
                            finalBalanceInDay=finalBalanceInDay.replace('.',':');
                            balanceAtRangeStart=balanceAtRangeStart.replace('.',':');
                            balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                            ownAccountTotal=ownAccountTotal.toString().replace('.',':');
                            balanceAtRangeStart=timeToDecimal(balanceAtRangeStart);
                            balanceAtRangeEnd=timeToDecimal(balanceAtRangeEnd);
                            finalTotalHours=timeToDecimal(finalTotalHours);
                            finalOffset=timeToDecimal(finalOffset);
                            finalBalance=timeToDecimal(finalBalance);
                            finalTotalHoursInDay=timeToDecimal(finalTotalHoursInDay);
                            ownAccountTotalFinal=timeToDecimal(ownAccountTotalFinal);
                            finalOffsetInDay=timeToDecimal(finalOffsetInDay);
                            finalBalanceInDay=timeToDecimal(finalBalanceInDay);
                            ownAccountTotal=ownAccountTotal;
                            if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                         {
                                        finalTotalHours=finalTotalHours.replace('.',',');
                                        finalOffset=finalOffset.replace('.',',');
                                        finalBalance=finalBalance.replace('.',',');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',',');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',',');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',',');  
                                        finalAmount=finalAmount.replace('.',','); 
                                        balanceAtRangeStart=balanceAtRangeStart.replace('.',',');
                                        balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');
                                        ownAccountTotal=ownAccountTotal.toString().replace('.',',');
                                         }
                                    }
                                   /* else
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalOffset=finalOffset.replace('.',':');
                                        finalBalance=finalBalance.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',':');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',':');
                                    }*/
                        }
                        res+='<tr><td class="tiny total_values"><?php echo trans ('reports.total')?></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td> <td class="tiny total_values align_right">'+ownAccountTotal+'</td> <td class="tiny total_values align_right">'+finalTotalHours+'</td> <td class="tiny total_values align_right">'+finalOffset+'</td> <td class="tiny total_values align_right">'+finalBalance+'</td> <td class="tiny total_values align_right">'+finalAmount+'</td> <td class="tiny total_values"></td> <td class="tiny total_values align_right">'+response['whole_total']['finalTotalHours']+'</td> <td class="tiny total_values align_right">'+response['whole_total']['finalBalance']+'</td> <td class="tiny total_values align_right">'+response['whole_total']['finalOffset']+'</td> <td class="tiny total_values"></td> </tr>';
                        res+='<tr><td class="tiny total_values1"><?php echo trans ('reports.total_in_days')?></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1 align_right">'+ownAccountTotalFinal+'</td> <td class="tiny total_values1 align_right">'+finalTotalHoursInDay+'</td> <td class="tiny total_values1 align_right">'+finalOffsetInDay+'</td> <td class="tiny total_values1 align_right">'+finalBalanceInDay+'</td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1 align_right">'+response['whole_total']['finalTotalHours']+'</td> <td class="tiny total_values1 align_right">'+response['whole_total']['finalBalance']+'</td> <td class="tiny total_values1 align_right">'+response['whole_total']['finalOffset']+'</td> <td class="tiny total_values1"></td> </tr>';
                        $('.activity_slection').show();
                        $('.for_activity_select').show();
                        $('#header_date_range').html( '&nbsp'+start_date+'&nbsp' +'<?php echo trans ('report_links.to')?>'+ '&nbsp'+end_date+'&nbsp');
                        var message="<?php echo trans ('reports.date_range')?>: "+start_date+" <?php echo trans ('report_links.to')?> "+end_date;
                        var message1="<?php echo trans ('reports.bal_range')?>: "+finalBalance;
                        var message2="<?php echo trans ('reports.bal_range_start')?>: "+balanceAtRangeStart;
                        var message3="<?php echo trans ('reports.bal_range_end')?>: "+balanceAtRangeEnd;
                        var titleOf=toTitleCase("<?php echo trans ('report_links.two_line_one')?> <?php echo trans ('report_links.two_line_two')?>");
                        var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
                        $('#header_balance_range').html(finalBalance);
                        $('#header_balance_range_at_start').html(balanceAtRangeStart);
                        $('#header_balance_range_at_end').html(balanceAtRangeEnd);
                        $('#data_table').html(res);
                       /*  $("#example-1").dataTable();*/
                        $(document).ready(function() {
                        $('#example-1').DataTable( {
                            'scrollX':true,
                            "ordering": false,
                        language: {
                        "url":languageUrl,
                        },
                        dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                         message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                         message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        orientation: orientationPdf,
                        pageSize: pageSize,
                        customize : function(doc) {
                                 doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].table.body = fillColor(doc.content[2].table.body);
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc.styles.tableHeader.fontSize = 7;
                                  doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                 width: fitWidth,
                                height: fitHeight
                                } );
                        },
                         title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                         message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                       /* {
                        extend: 'colvis',
                        text:'Select field to hide <i class="fa fa-filter icon" aria-hidden="true"></i>',
                        }*/
                        ],
                        "bSort": false,
                        "columnDefs": [
                        {
                        "targets": [ 0 ],
                        "visible": hide0,
                        "searchable": false
                        },
                        {
                        "targets": [ 1 ],
                        "visible": hide1,
                        "searchable": false
                        },
                        {
                        "targets": [ 2 ],
                        "visible": hide2,
                        "searchable": false
                        },
                        {
                        "targets": [ 3 ],
                        "visible": hide3,
                        "searchable": false
                        },
                        {
                        "targets": [ 4 ],
                        "visible": hide4,
                        "searchable": false
                        },
                        {
                        "targets": [ 5 ],
                        "visible": hide5,
                        "searchable": false
                        },
                        {
                        "targets": [ 6 ],
                        "visible": hide6,
                        "searchable": false
                        },
                        {
                        "targets": [ 7 ],
                        "visible": hide7,
                        "searchable": false
                        },
                        {
                        "targets": [ 8 ],
                        "visible": hide8,
                        "searchable": false
                        },
                        {
                        "targets": [ 9 ],
                        "visible": hide9,
                        "searchable": false
                        },
                        {
                        "targets": [ 10 ],
                        "visible": hide10,
                        "searchable": false
                        },
                        {
                        "targets": [ 11 ],
                        "visible": hide11,
                        "searchable": false
                        },
                        {
                        "targets": [ 12 ],
                        "visible": hide12,
                        "searchable": false
                        },
                        {
                        "targets": [ 13 ],
                        "visible": hide13,
                        "searchable": false
                        },
                        {
                        "targets": [ 14 ],
                        "visible": hide14,
                        "searchable": false
                        },
                        {
                        "targets": [ 15 ],
                        "visible": hide15,
                        "searchable": false
                        },
                        {
                        "targets": [ 16 ],
                        "visible": hide16,
                        "searchable": false
                        },
                        {
                        "targets": [ 17 ],
                        "visible": hide17,
                        "searchable": false
                        },
                        {
                        "targets": [ 18 ],
                        "visible": hide18,
                        "searchable": false
                        },
                        {
                        "targets": [ 19 ],
                        "visible": hide19,
                        "searchable": false
                        },
                        {
                        "targets": [ 20 ],
                        "visible": hide20,
                        "searchable": false
                        },
                        {
                        "targets": [ 21 ],
                        "visible": hide21,
                        "searchable": false
                        },
                        {
                        "targets": [ 22 ],
                        "visible": hide22,
                        "searchable": false
                        },
                        {
                        "targets": [ 23 ],
                        "visible": hide23,
                        "searchable": false
                        },
                        ],
                        } );
                        } );
                          $('.mobileSelect-clearbtn').click();//clear multiselect activity
                          $('#fakeLoader').hide();
                     /*   table.init();*/
                    }
                }); 
        }
    }
});

//on load report

$(window).load(function(){

var today = new Date();
var dayOfWeekStartingSundayZeroIndexBased = today.getDay(); // 0 : Sunday ,1 : Monday,2,3,4,5,6 : Saturday
var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()-6);
var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+7);
var start_date1 =moment(mondayOfWeek).format('D-M-YYYY');
var end_date1 =moment(sundayOfWeek).format('D-M-YYYY');
if (typeof(localStorage.start_date)=='undefined') { localStorage.start_date=start_date1}
if (typeof(localStorage.end_date)=='undefined') { localStorage.end_date=end_date1}
$('#start_date').html($('#start_date').val(localStorage.getItem("start_date")));
$('#end_date').html($('#end_date').val(localStorage.getItem("end_date")));
start_date1 =localStorage.getItem("start_date");
end_date1 =localStorage.getItem("end_date");
 if(start_date1!=''&&end_date1!='')
        {
            $('.over').show();
             $('#fakeLoader').show();
            var res='';
            var table = $('#example-1').DataTable({'scrollX':true,
              "ordering": false});
            table.destroy();
            $.ajax({
                    url: "<?php echo url('listingTimesheetWithRange/"+company_id+"/"+employee_id+"/"+start_date1+"/"+end_date1+"')?>",
                    type:'post',
                     success: function(response)
                    {   
                          var selectBoxContentLength=$('#filtered_by_coloum').val();
                        selectBoxContentLength=selectBoxContentLength.length;
                        selectBoxContentLength=24-selectBoxContentLength;
                          if (selectBoxContentLength>20) {
                                orientationPdf="landscape";
                                pageSize="A3";
                                fitWidth= 60;
                                fitHeight= 50;
                            }else if (selectBoxContentLength>12){
                                 orientationPdf="landscape";
                                 pageSize="A4";
                                  fitWidth= 50;
                                fitHeight= 40;
                            } else {
                                 orientationPdf="portrait";
                                 pageSize="A4";
                                  fitWidth= 40;
                                fitHeight= 30;
                            }
                       /* console.log(response);*/
                        for (var i = 0; i < response['TotalTimesheetDetail'].length; i++) 
                        {
                            /*console.log((response['TotalTimesheetDetail'][i]['timesheetDetail']).length);*/
                            if ((response['TotalTimesheetDetail'][i]['timesheetDetail']).length >1) 
                            {
                                /*var ddate='';*/
                                  for (var j = 0; j < response['TotalTimesheetDetail'][i]['timesheetDetail'].length; j++) 
                                  {
                                    /*ddate =moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['date']).format('DD/MM/YYYY');*/
                                    var start_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'];
                                     var amount=decimalPoint(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['amount']);
                                    if (amount==0.0) 
                                    {
                                        amount='';
                                    }
                                    else
                                    {
                                        amount=replaceToComma(time_style,value_format,separator,amount);
                                    }
                                    var end_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'];
                                    var break_time= response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_time'];
                                    if (break_time==0  || break_time=="" || break_time==null) {break_time='';}
                                    var break_min=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_min'];
                                    if (break_min==0 || break_min=="0:00" || break_min=="" || break_min==null) {break_min='';}
                                    var total_hours=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours'];
                                    var offsets=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['offset'];
                                    var day_balances=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['day_balance'];
                                    var rate=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['rate'];
                                    var condition1 =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1'];
                                    var condition1_from =minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_from']);
                                    var condition1_factor= replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_factor']);
                                    var condition2=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2'];
                                    var condition2_from=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_from']);
                                    var condition2_factor=replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_factor']);
                                    var own_account=minutesToStrDot(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['own_account']);
                                    var bonus_hours=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['TSBonusHrs']);
                                    var apply_bonus=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['apply_bonus'];
                                    var subTotalHours=minutesToStr(response['TotalTimesheetDetail'][i]['finalTotalHours']);
                                    var subOffset=minutesToStr(response['TotalTimesheetDetail'][i]['finalOffset']);
                                    var subBalance=minutesToStr(response['TotalTimesheetDetail'][i]['finalBalance']);
                                    var subOwnAccount=response['TotalTimesheetDetail'][i]['sub_ownaccount'];
                                    var subAmount=parseFloat(Math.round(response['TotalTimesheetDetail'][i]['amount'] * 100) / 100).toFixed(2);
                                    var tick ='';
                                    if (apply_bonus==true) 
                                    {
                                        tick="<?php echo trans('translate.yes')?>";
                                    }
                                    else
                                    {
                                        tick="<?php echo trans('translate.no')?>";
                                    }
                                    if (condition1=="Time" && condition1_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                             condition1_from = moment(condition1_from, ["h:mm A"]);
                                             condition1_from = condition1_from.format("h:mm A");
                                        }
                                    }
                                     if (condition2=="Time" && condition2_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                            condition2_from = moment(condition2_from, ["h:mm A"]);
                                            condition2_from = condition2_from.format("h:mm A");
                                        }
                                    }
                                    /*console.log(time_style);*/
                                    if(start_time!='' && end_time!='' && start_time!="Flat")
                                    {
                                    if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                     start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");
                                     end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm A");
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=='8:15')
                                    {
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.replace('.',':');
                                        own_account=own_account.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                         {
                                            total_hours=total_hours.replace(':',',');
                                            offsets=offsets.replace(':',',');
                                            day_balances=day_balances.replace(':',',');
                                            subTotalHours=subTotalHours.replace('.',',');
                                            subOffset=subOffset.replace('.',',');
                                            subBalance=subBalance.replace('.',',');
                                            subOwnAccount=subOwnAccount.replace('.',',');
                                            own_account=own_account.replace('.',',');
                                            rate=rate.replace('.',',');
                                            subAmount=subAmount.replace('.',',');
                                         }
                                         else if(separator=="dot")
                                         {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');
                                         }
                                    }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                        start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                        end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                        if(value_format=='8.15')
                                        {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');

                                        }
                                        else if(value_format=='8:15')
                                        {
                                            subTotalHours=subTotalHours.replace('.',':');
                                            subOffset=subOffset.replace('.',':');
                                            subBalance=subBalance.replace('.',':');
                                            subOwnAccount=subOwnAccount.replace('.',':');
                                            own_account=own_account.replace('.',':');
                                        }
                                        else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.toString().replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                             else if(separator=="dot")
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                     else if(time_style=='Industrial')
                                    {
                                        start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                        end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.toString().replace('.',':');
                                        own_account=own_account.toString().replace('.',':');
                                        start_time=timeToDecimal(start_time);
                                        end_time=timeToDecimal(end_time);
                                         if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                        total_hours=timeToDecimal(total_hours);
                                        offsets=timeToDecimal(offsets);
                                        day_balances=timeToDecimal(day_balances);
                                        subTotalHours=timeToDecimal(subTotalHours);
                                        subOffset=timeToDecimal(subOffset);
                                        subBalance=timeToDecimal(subBalance);
                                        subOwnAccount=timeToDecimal(subOwnAccount);
                                        own_account=timeToDecimal(own_account);
                                        if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                start_time=start_time.replace('.',',');
                                                 end_time=end_time.replace('.',',');
                                                break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                else
                                {
                                    start_time="<?php echo trans('translate.flat')?>";
                                    rate='';
                                     if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                     /*start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");*/
                                   /*  end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm");*/
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=='8:15')
                                    {
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.replace('.',':');
                                        own_account=own_account.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                        if(separator=="comma")
                                         {
                                            total_hours=total_hours.replace(':',',');
                                            offsets=offsets.replace(':',',');
                                            day_balances=day_balances.replace(':',',');
                                            subTotalHours=subTotalHours.replace('.',',');
                                            subOffset=subOffset.replace('.',',');
                                            subBalance=subBalance.replace('.',',');
                                            subOwnAccount=subOwnAccount.replace('.',',');
                                            own_account=own_account.replace('.',',');
                                            rate=rate.replace('.',',');
                                            subAmount=subAmount.replace('.',',');
                                         }
                                         else if(separator=="dot")
                                         {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');
                                         }
                                    }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                       /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                                        /*end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                        if(value_format=='8.15')
                                        {
                                            total_hours=total_hours.replace(':','.');
                                            offsets=offsets.replace(':','.');
                                            day_balances=day_balances.replace(':','.');

                                        }
                                        else if(value_format=='8:15')
                                        {
                                            subTotalHours=subTotalHours.replace('.',':');
                                            subOffset=subOffset.replace('.',':');
                                            subBalance=subBalance.replace('.',':');
                                            subOwnAccount=subOwnAccount.replace('.',':');
                                            own_account=own_account.replace('.',':');
                                        }
                                        else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.toString().replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                             else if(separator=="dot")
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                     else if(time_style=='Industrial')
                                    {
                                       /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                                      /*  end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                        subTotalHours=subTotalHours.replace('.',':');
                                        subOffset=subOffset.replace('.',':');
                                        subBalance=subBalance.replace('.',':');
                                        subOwnAccount=subOwnAccount.toString().replace('.',':');
                                        own_account=own_account.toString().replace('.',':');
                                        /*start_time=timeToDecimal(start_time);*/
                                        end_time=timeToDecimal(end_time);
                                         if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                        total_hours=timeToDecimal(total_hours);
                                        offsets=timeToDecimal(offsets);
                                        day_balances=timeToDecimal(day_balances);
                                        subTotalHours=timeToDecimal(subTotalHours);
                                        subOffset=timeToDecimal(subOffset);
                                        subBalance=timeToDecimal(subBalance);
                                        subOwnAccount=timeToDecimal(subOwnAccount);
                                        own_account=timeToDecimal(own_account);
                                        if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                               /* start_time=start_time.replace('.',',');*/
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                subTotalHours=subTotalHours.replace('.',',');
                                                subOffset=subOffset.replace('.',',');
                                                subBalance=subBalance.replace('.',',');
                                                subOwnAccount=subOwnAccount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                subAmount=subAmount.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                    var day =moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['date']).format('DD/MM/YYYY');
                                    if (j==0) 
                                    {   
                                        if (response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes'] !== "(null)") {
                                            var temp_notes = response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes']
                                        }else{
                                            var temp_notes = "";
                                        }

                                         res+='<tr><td class="tiny align_left">'+day+'</td> <td class="tiny align_left">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['activity_detail']['activity_name']+'</td> <td class="tiny align_right">'+start_time+'</td> <td class="tiny align_right">'+end_time+'</td> <td class="tiny align_right">'+break_time+'</td> <td class="tiny "></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny align_right">'+own_account+'</td> <td class="tiny align_right">'+total_hours+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny align_right">'+amount+'</td> <td class="tiny align_right">'+break_min+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours_min']+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny" style="text-align: left">'+ temp_notes +'</td> </tr>'; 
                                    } 
                                    else
                                    {   
                                        if (response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes'] !== "(null)") {
                                            var temp_notes = response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes']
                                        }else{
                                            var temp_notes = "";
                                        }

                                         res+='<tr><td class="tiny align_left">,,</td> <td class="tiny align_left">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['activity_detail']['activity_name']+'</td> <td class="tiny align_right">'+start_time+'</td> <td class="tiny align_right">'+end_time+'</td> <td class="tiny align_right">'+break_time+'</td> <td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny"></td><td class="tiny align_right">'+own_account+'</td> <td class="tiny align_right">'+total_hours+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny align_right">'+amount+'</td> <td class="tiny align_right">'+break_min+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours_min']+'</td> <td class="tiny align_right"></td> <td class="tiny align_right"></td> <td class="tiny" style="text-align: left">'+ temp_notes +'</td> </tr>'; 
                                    }
                                  }
                                console.log(subTotalHours+'/'+subOffset+'/'+subBalance);
                                  res+='<tr><td class="tiny sub_total_values"><?php echo trans ('reports.daily_total')?></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values">'+rate+'</td> <td class="tiny sub_total_values">'+condition1+'</td><td class="tiny sub_total_values">'+condition1_from+'</td><td class="tiny sub_total_values">'+condition1_factor+'</td><td class="tiny sub_total_values">'+condition2+'</td><td class="tiny sub_total_values">'+condition2_from+'</td><td class="tiny sub_total_values">'+condition2_factor+'</td><td class="tiny sub_total_values align_right">'+bonus_hours+'</td><td class="tiny sub_total_values">'+tick+'</td><td class="tiny sub_total_values align_right">'+subOwnAccount+'</td><td class="tiny sub_total_values align_right">'+subTotalHours+'</td> <td class="tiny sub_total_values align_right">'+subOffset+'</td> <td class="tiny sub_total_values align_right">'+subBalance+'</td> <td class="tiny sub_total_values align_right">'+subAmount+'</td> <td class="tiny sub_total_values"></td> <td class="tiny sub_total_values align_right">'+response['TotalTimesheetDetail'][i]['finalTotalHours']+'</td> <td class="tiny sub_total_values align_right">'+response['TotalTimesheetDetail'][i]['finalBalance']+'</td> <td class="tiny sub_total_values align_right">'+response['TotalTimesheetDetail'][i]['finalOffset']+'</td> <td class="tiny sub_total_values"></td> </tr>';
                            }
                            else
                            {
                                for (var j = 0; j < response['TotalTimesheetDetail'][i]['timesheetDetail'].length; j++) 
                                  {
                                    var start_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'];
                                    var end_time =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'];
                                     var break_time= response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_time'];
                                     if (break_time==0 || break_time=="" || break_time==null) {break_time='';}
                                     var break_min=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['break_min'];
                                    if (break_min==0 || break_min=="0:00" || break_min=="" || break_min==null) {break_min='';}
                                     var total_hours=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours'];
                                    var offsets=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['offset'];
                                    var day_balances=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['day_balance'];
                                    var rate=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['rate'];
                                    var amount=decimalPoint(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['amount']);
                                     var condition1 =response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1'];
                                    var condition1_from =minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_from']);
                                    var condition1_factor= replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition1_factor']);
                                    var condition2=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2'];
                                    var condition2_from=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_from']);
                                    var condition2_factor=replaceToComma(time_style,value_format,separator,response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['condition2_factor']);
                                    var bonus_hours=minToHrs(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['TSBonusHrs']);
                                    var apply_bonus=response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['apply_bonus'];
                                    var own_account=minutesToStrDot(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['own_account']);
                                   /* console.log(time_style);*/
                                    var tick ='';
                                    if (apply_bonus==true) 
                                    {
                                        tick="<?php echo trans('translate.yes')?>";
                                    }
                                    else
                                    {
                                        tick="<?php echo trans('translate.no')?>";
                                    }
                                   if (amount==0.0) {amount=""}
                                   if (condition1=="Time" && condition1_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                             condition1_from = moment(condition1_from, ["h:mm A"]);
                                             condition1_from = condition1_from.format("h:mm A");
                                        }
                                    }
                                     if (condition2=="Time" && condition2_from != 0) 
                                    {
                                        if (time_style=='am/pm') 
                                        {
                                            condition2_from = moment(condition2_from, ["h:mm A"]);
                                            condition2_from = condition2_from.format("h:mm A");
                                        }
                                    }
                                   if(start_time!='' && end_time!='' && start_time!="Flat")
                                    {
                                    if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                     start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");
                                     end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm A");
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                     else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                    start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='Industrial')
                                    {
                                    start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");
                                    start_time=timeToDecimal(start_time);
                                    end_time=timeToDecimal(end_time);
                                     if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                    total_hours=timeToDecimal(total_hours);
                                    offsets=timeToDecimal(offsets);
                                    day_balances=timeToDecimal(day_balances);
                                    own_account=own_account.replace('.',':');
                                    own_account=timeToDecimal(own_account);
                                    if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                start_time=start_time.replace('.',',');
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                else
                                {
                                    start_time="<?php echo trans('translate.flat')?>";
                                    rate='';
                                    if (amount==0.0) {amount="";}
                                    if (time_style=='am/pm') 
                                    {
                                    /*start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                    end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                    /* start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");*/
                                  /*   end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm");*/
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                     else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='24 hours')
                                    {
                                   /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                               /*     end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                    if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        offsets=offsets.replace(':','.');
                                        day_balances=day_balances.replace(':','.');
                                    }
                                    else if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                total_hours=total_hours.replace(':',',');
                                                offsets=offsets.replace(':',',');
                                                day_balances=day_balances.replace(':',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                             else if (separator=="dot") 
                                             {
                                                total_hours=total_hours.replace(':','.');
                                                offsets=offsets.replace(':','.');
                                                day_balances=day_balances.replace(':','.');
                                             }
                                        }
                                    }
                                    else if(time_style=='Industrial')
                                    {
                                   /* start_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['start_time'], ["h:mm A"]).format("HH:mm");*/
                                  /*  end_time=moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                   /* start_time=timeToDecimal(start_time);*/
                                    end_time=timeToDecimal(end_time);
                                    if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                    total_hours=timeToDecimal(total_hours);
                                    offsets=timeToDecimal(offsets);
                                    day_balances=timeToDecimal(day_balances);
                                    own_account=own_account.replace('.',':');
                                    own_account=timeToDecimal(own_account);
                                    if(value_format=="Device region")
                                        {
                                            if(separator=="comma")
                                             {
                                                /*start_time=start_time.replace('.',',');*/
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                offsets=offsets.replace('.',',');
                                                day_balances=day_balances.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                             }
                                        }
                                    }
                                }
                                    var day =moment(response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['date']).format('DD/MM/YYYY');

                                    if (response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes'] !== "(null)") {
                                            var temp_notes = response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes']
                                        }else{
                                            var temp_notes = "";
                                        }

                                    res+='<tr><td class="tiny align_left">'+day+'</td> <td class="tiny align_left">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['activity_detail']['activity_name']+'</td> <td class="tiny align_right">'+start_time+'</td> <td class="tiny align_right">'+end_time+'</td> <td class="tiny align_right">'+break_time+'</td> <td class="tiny">'+rate+'</td><td class="tiny">'+condition1+'</td><td class="tiny">'+condition1_from+'</td><td class="tiny">'+condition1_factor+'</td><td class="tiny">'+condition2+'</td><td class="tiny">'+condition2_from+'</td><td class="tiny">'+condition2_factor+'</td><td class="tiny align_right">'+bonus_hours+'</td><td class="tiny">'+tick+'</td><td class="tiny align_right">'+own_account+'</td> <td class="tiny align_right">'+total_hours+'</td> <td class="tiny align_right">'+offsets+'</td> <td class="tiny align_right">'+day_balances+'</td> <td class="tiny align_right">'+amount+'</td> <td class="tiny align_right">'+break_min+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['total_hours_min']+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['balance_min']+'</td> <td class="tiny align_right">'+response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['offset_min']+'</td> <td class="tiny" style="text-align: left">'+ temp_notes +'</td> </tr>';  
                                  }
                            }

                        }
                        var finalTotalHours=minutesToStr(response['whole_total']['finalTotalHours']);
                        var ownAccountTotal=response['whole_total']['ownAccountTotal'];
                        var finalOffset=minutesToStr(response['whole_total']['finalOffset']);
                        var finalBalance=minutesToStr(response['whole_total']['finalBalance']);
                        var finalTotalHoursInDay=parseFloat(Math.round(response['whole_total']['finalTotalHoursInDay'] * 100) / 100).toFixed(2);
                        var ownAccountTotalFinal=parseFloat(Math.round(response['whole_total']['ownAccountTotalFinal'] * 100) / 100).toFixed(2);
                        var finalOffsetInDay=parseFloat(Math.round(response['whole_total']['finalOffsetInDay'] * 100) / 100).toFixed(2);
                        var finalBalanceInDay=parseFloat(Math.round(response['whole_total']['finalBalanceInDay'] * 100) / 100).toFixed(2);
                        var finalAmount=parseFloat(Math.round(response['whole_total']['finalAmount'] * 100) / 100).toFixed(2);
                        var balanceAtRangeStart=minutesToStr(response['whole_total']['balanceAtRangeStart']);
                        var balanceAtRangeEnd=minutesToStr(response['whole_total']['balanceAtRangeEnd']);
                        if (time_style=='am/pm')
                        {
                           if(value_format=='8:15')
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalOffset=finalOffset.replace('.',':');
                                        finalBalance=finalBalance.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',':');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',':');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',':');
                                         ownAccountTotal=ownAccountTotal.replace('.',':');
                                    } 
                            else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                         {
                                        finalTotalHours=finalTotalHours.replace('.',',');
                                        finalOffset=finalOffset.replace('.',',');
                                        finalBalance=finalBalance.replace('.',',');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',',');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',',');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',','); 
                                        finalAmount=finalAmount.replace('.',','); 
                                        balanceAtRangeStart=balanceAtRangeStart.replace('.',',');
                                        balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');
                                        ownAccountTotal=ownAccountTotal.toString().replace('.',',');
                                         }
                                    }
                        }
                        else if(time_style=='24 hours')
                        {
                            if(value_format=='8:15')
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalOffset=finalOffset.replace('.',':');
                                        finalBalance=finalBalance.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',':');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',':');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',':');
                                         ownAccountTotal=ownAccountTotal.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                         {
                                        finalTotalHours=finalTotalHours.replace('.',',');
                                        finalOffset=finalOffset.replace('.',',');
                                        finalBalance=finalBalance.replace('.',',');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',',');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',',');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',',');  
                                        finalAmount=finalAmount.replace('.',','); 
                                        balanceAtRangeStart=balanceAtRangeStart.replace('.',',');
                                        balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');
                                        ownAccountTotal=ownAccountTotal.toString().replace('.',',');
                                         }
                                    }
                        }
                        else if(time_style=='Industrial')
                        {
                            finalTotalHours=finalTotalHours.replace('.',':');
                            finalOffset=finalOffset.replace('.',':');
                            finalBalance=finalBalance.replace('.',':');
                            finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                            ownAccountTotalFinal=ownAccountTotalFinal.replace('.',':');
                            finalOffsetInDay=finalOffsetInDay.replace('.',':');
                            finalBalanceInDay=finalBalanceInDay.replace('.',':');
                            balanceAtRangeStart=balanceAtRangeStart.replace('.',':');
                            balanceAtRangeEnd=balanceAtRangeEnd.replace('.',':');
                            ownAccountTotal=ownAccountTotal.toString().replace('.',':');
                            balanceAtRangeStart=timeToDecimal(balanceAtRangeStart);
                            balanceAtRangeEnd=timeToDecimal(balanceAtRangeEnd);
                            finalTotalHours=timeToDecimal(finalTotalHours);
                            finalOffset=timeToDecimal(finalOffset);
                            finalBalance=timeToDecimal(finalBalance);
                            finalTotalHoursInDay=timeToDecimal(finalTotalHoursInDay);
                            ownAccountTotalFinal=timeToDecimal(ownAccountTotalFinal);
                            finalOffsetInDay=timeToDecimal(finalOffsetInDay);
                            finalBalanceInDay=timeToDecimal(finalBalanceInDay);
                            ownAccountTotal=ownAccountTotal;
                            if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                         {
                                        finalTotalHours=finalTotalHours.replace('.',',');
                                        finalOffset=finalOffset.replace('.',',');
                                        finalBalance=finalBalance.replace('.',',');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                        ownAccountTotalFinal=ownAccountTotalFinal.replace('.',',');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',',');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',',');  
                                        finalAmount=finalAmount.replace('.',','); 
                                        balanceAtRangeStart=balanceAtRangeStart.replace('.',',');
                                        balanceAtRangeEnd=balanceAtRangeEnd.replace('.',',');
                                        ownAccountTotal=ownAccountTotal.toString().replace('.',',');
                                         }
                                    }
                                   /* else
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalOffset=finalOffset.replace('.',':');
                                        finalBalance=finalBalance.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        finalOffsetInDay=finalOffsetInDay.replace('.',':');
                                        finalBalanceInDay=finalBalanceInDay.replace('.',':');
                                    }*/
                        }
                        res+='<tr><td class="tiny total_values"><?php echo trans ('reports.total')?></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td><td class="tiny total_values"></td> <td class="tiny total_values align_right">'+ownAccountTotal+'</td> <td class="tiny total_values align_right">'+finalTotalHours+'</td> <td class="tiny total_values align_right">'+finalOffset+'</td> <td class="tiny total_values align_right">'+finalBalance+'</td> <td class="tiny total_values align_right">'+finalAmount+'</td> <td class="tiny total_values"></td> <td class="tiny total_values align_right">'+response['whole_total']['finalTotalHours']+'</td> <td class="tiny total_values align_right">'+response['whole_total']['finalBalance']+'</td> <td class="tiny total_values align_right">'+response['whole_total']['finalOffset']+'</td> <td class="tiny total_values"></td> </tr>';
                        res+='<tr><td class="tiny total_values1"><?php echo trans ('reports.total_in_days')?></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td><td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1 align_right">'+ownAccountTotalFinal+'</td> <td class="tiny total_values1 align_right">'+finalTotalHoursInDay+'</td> <td class="tiny total_values1 align_right">'+finalOffsetInDay+'</td> <td class="tiny total_values1 align_right">'+finalBalanceInDay+'</td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1 align_right">'+response['whole_total']['finalTotalHours']+'</td> <td class="tiny total_values1 align_right">'+response['whole_total']['finalBalance']+'</td> <td class="tiny total_values1 align_right">'+response['whole_total']['finalOffset']+'</td> <td class="tiny total_values1"></td> </tr>';
                        $('.activity_slection').show();
                        $('.for_activity_select').show();
                        $('#header_date_range').html( '&nbsp'+start_date1+'&nbsp' +'<?php echo trans ('report_links.to')?>'+ '&nbsp'+end_date1+'&nbsp');
                        var message="<?php echo trans ('reports.date_range')?>: "+start_date1+" <?php echo trans ('report_links.to')?> "+end_date1;
                        var message1="<?php echo trans ('reports.bal_range')?>: "+finalBalance;
                        var message2="<?php echo trans ('reports.bal_range_start')?>: "+balanceAtRangeStart;
                        var message3="<?php echo trans ('reports.bal_range_end')?>: "+balanceAtRangeEnd;
                        var titleOf=toTitleCase("<?php echo trans ('report_links.two_line_one')?> <?php echo trans ('report_links.two_line_two')?>");
                        var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
                        $('#header_balance_range').html(finalBalance);
                        $('#header_balance_range_at_start').html(balanceAtRangeStart);
                        $('#header_balance_range_at_end').html(balanceAtRangeEnd);
                        $('#data_table').html(res);
                       /*  $("#example-1").dataTable();*/
                        $(document).ready(function() {
                        $('#example-1').DataTable( {
                        'scrollX':true,
                        "ordering": false,
                        language: {
                        "url":languageUrl,
                        },
                        dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                         message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                         message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        orientation:orientationPdf,
                        pageSize: pageSize,
                        customize : function(doc) {
                            console.log(doc)
                                 doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].table.body = fillColor(doc.content[2].table.body);
                                 doc.styles.tableHeader.fontSize = 7;
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: fitWidth,
                                height: fitHeight
                                } );
                                console.log(doc)
                        },
                         title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                         message:employeeName+'\n'+companyOf+'\n'+message+'\n'+message1+'\n'+message2+'\n'+message3,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                       /* {
                        extend: 'colvis',
                        text:'Select field to hide <i class="fa fa-filter icon" aria-hidden="true"></i>',
                        }*/
                        ],
                        "bSort": false,
                        "columnDefs": [
                        {
                        "targets": [ 0 ],
                        "visible": hide0,
                        "searchable": false
                        },
                        {
                        "targets": [ 1 ],
                        "visible": hide1,
                        "searchable": false
                        },
                        {
                        "targets": [ 2 ],
                        "visible": hide2,
                        "searchable": false
                        },
                        {
                        "targets": [ 3 ],
                        "visible": hide3,
                        "searchable": false
                        },
                        {
                        "targets": [ 4 ],
                        "visible": hide4,
                        "searchable": false
                        },
                        {
                        "targets": [ 5 ],
                        "visible": hide5,
                        "searchable": false
                        },
                        {
                        "targets": [ 6 ],
                        "visible": hide6,
                        "searchable": false
                        },
                        {
                        "targets": [ 7 ],
                        "visible": hide7,
                        "searchable": false
                        },
                        {
                        "targets": [ 8 ],
                        "visible": hide8,
                        "searchable": false
                        },
                        {
                        "targets": [ 9 ],
                        "visible": hide9,
                        "searchable": false
                        },
                        {
                        "targets": [ 10 ],
                        "visible": hide10,
                        "searchable": false
                        },
                        {
                        "targets": [ 11 ],
                        "visible": hide11,
                        "searchable": false
                        },
                        {
                        "targets": [ 12 ],
                        "visible": hide12,
                        "searchable": false
                        },
                        {
                        "targets": [ 13 ],
                        "visible": hide13,
                        "searchable": false
                        },
                        {
                        "targets": [ 14 ],
                        "visible": hide14,
                        "searchable": false
                        },
                        {
                        "targets": [ 15 ],
                        "visible": hide15,
                        "searchable": false
                        },
                        {
                        "targets": [ 16 ],
                        "visible": hide16,
                        "searchable": false
                        },
                        {
                        "targets": [ 17 ],
                        "visible": hide17,
                        "searchable": false
                        },
                        {
                        "targets": [ 18 ],
                        "visible": hide18,
                        "searchable": false
                        },
                        {
                        "targets": [ 19 ],
                        "visible": hide19,
                        "searchable": false
                        },
                        {
                        "targets": [ 20 ],
                        "visible": hide20,
                        "searchable": false
                        },
                        {
                        "targets": [ 21 ],
                        "visible": hide21,
                        "searchable": false
                        },
                        {
                        "targets": [ 22 ],
                        "visible": hide22,
                        "searchable": false
                        },
                        {
                        "targets": [ 23 ],
                        "visible": hide23,
                        "searchable": false
                        },
                        ],
                        } );
                        } );
                          $('.mobileSelect-clearbtn').click();//clear multiselect activity
                          $('#fakeLoader').hide();
                     /*   table.init();*/
                    }
                }); 
        }

});

//for multi activity select
 $('#filtered_by_activity').mobileSelect({title:transOfSelectAnOption,buttonSave:transOfSave,buttonClear:transOfClear,buttonCancel:transOfCancel});
 $('#Clear_btn').click(function(){
    location.reload();
});
    $('#activity_filter').click(function(){
         var res='';
            var table = $('#example-1').DataTable();
             table.destroy();
        var start=$('#start_date').val();
        var end=$('#end_date').val();
            start1=moment(start, ["DD-MM-YYYY"]).format("YYYY-MM-DD");
            end1=moment(end, ["DD-MM-YYYY"]).format("YYYY-MM-DD");
        var date= new Array(start1,end1);
        var data=$('#filtered_by_activity').val();
        console.log(data);
        console.log(date);
        if(data!=null)
        {
            $('#fakeLoader').show();
        $.ajax({
                    url: "<?php echo url('listingTimesheetForActivity')?>",
                    method: 'POST',
                    data:{"data":data,"date":date},
                   success: function(response)
                    {   
                        var selectBoxContentLength=$('#filtered_by_coloum').val();
                        selectBoxContentLength=selectBoxContentLength.length;
                        selectBoxContentLength=24-selectBoxContentLength;
                          if (selectBoxContentLength>20) {
                                orientationPdf="landscape";
                                pageSize="A3";
                                 fitWidth= 60;
                                fitHeight= 50;
                            }else if (selectBoxContentLength>12){
                                 orientationPdf="landscape";
                                 pageSize="A4";
                                  fitWidth= 50;
                                fitHeight= 40;
                            } else {
                                 orientationPdf="portrait";
                                 pageSize="A4";
                                  fitWidth= 40;
                                fitHeight= 30;
                            }
                        for(i=0;i<response['result'].length;i++)
                        {
                            var start_time =response['result'][i]['start_time'];
                            var end_time =response['result'][i]['end_time'];
                            var break_time=response['result'][i]['break_time']; 
                            if (break_time==0  || break_time=="" || break_time==null) {break_time='';}
                            var break_min=response['result'][i]['break_min'];
                            if (break_min==0 || break_min=="0:00" || break_min=="" || break_min==null) {break_min='';}
                            var total_hours=response['result'][i]['total_hours'];
                            var amount=decimalPoint(response['result'][i]['amount']);
                            var rate=response['result'][i]['rate'];
                            var condition1 =response['result'][i]['condition1'];
                            var condition1_from =minToHrs(response['result'][i]['condition1_from']);
                            var condition1_factor= replaceToComma(time_style,value_format,separator,response['result'][i]['condition1_factor']);
                            var condition2=response['result'][i]['condition2'];
                            var condition2_from=minToHrs(response['result'][i]['condition2_from']);
                            var condition2_factor=replaceToComma(time_style,value_format,separator,response['result'][i]['condition2_factor']);
                            var bonus_hours=minToHrs(response['result'][i]['TSBonusHrs']);
                            var apply_bonus=response['result'][i]['apply_bonus'];
                            var own_account=minToHrs(response['result'][i]['own_account']);
                            console.log(time_style);
                             var tick ='';
                                    if (apply_bonus==true) 
                                    {
                                        tick="<?php echo trans('translate.yes')?>";
                                    }
                                    else
                                    {
                                        tick="<?php echo trans('translate.no')?>";
                                    }
                            if(start_time!='' && end_time!='' && start_time!="Flat")
                            {
                             if (time_style=='am/pm') 
                            {
                                /*start_time=moment(response['result'][i]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                end_time=moment(response['result'][i]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                 start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");
                                     end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm A");
                                 if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        own_account=own_account.replace(':','.');
                                    }
                                else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                                total_hours=total_hours.replace(':',',');
                                                own_account=own_account.replace(':',',');
                                                amount=amount.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                            }
                                            else if(separator=="dot")
                                            {
                                                total_hours=total_hours.replace(':','.');
                                            }
                                    }
                            }
                            else if(time_style=='24 hours')
                            {
                                 start_time=moment(response['result'][i]['start_time'], ["h:mm A"]).format("HH:mm");
                                end_time=moment(response['result'][i]['end_time'], ["h:mm A"]).format("HH:mm");
                                if(value_format=='8.15')
                                    {
                                        start_time=start_time.replace(':','.');
                                        end_time=end_time.replace(':','.');
                                         break_time=replaceToComma(time_style,value_format,separator,break_time);
                                        total_hours=total_hours.replace(':','.');
                                        own_account=own_account.replace(':','.');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                                total_hours=total_hours.replace(':',',');
                                                own_account=own_account.replace(':',',');
                                                amount=amount.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');

                                            }
                                            else if(separator=="dot")
                                            {
                                                total_hours=total_hours.replace(':','.');
                                                own_account=own_account.replace(':','.');
                                            }
                                    }
                            }
                            else if(time_style=='Industrial')
                            {    
                            start_time=moment(response['result'][i]['start_time'], ["h:mm A"]).format("HH:mm");
                                end_time=moment(response['result'][i]['end_time'], ["h:mm A"]).format("HH:mm");     
                                  console.log(start_time+',/.'+end_time);
                                    start_time=timeToDecimal(start_time);
                                    end_time=timeToDecimal(end_time);
                                    if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                    total_hours=timeToDecimal(total_hours);
                                    own_account=timeToDecimal(own_account);
                                    console.log(start_time+'/'+end_time);
                                    if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                                start_time=start_time.replace('.',',');
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                            }
                                    }
                            }
                        }
                        else
                        {
                            start_time="<?php echo trans('translate.flat')?>";
                            rate='';
                            if (amount==0.0) {amount=""}
                             if (time_style=='am/pm') 
                            {
                                /*start_time=moment(response['result'][i]['start_time'], ["HH:mm"]).locale('en').format("h:mm A");
                                end_time=moment(response['result'][i]['end_time'], ["HH:mm"]).locale('en').format("h:mm A");  */
                                 /*start_time = moment(start_time, ["h:mm A"]);
                                     start_time = start_time.format("h:mm A");*/
                                  /*   end_time = moment(end_time, ["h:mm A"]);
                                     end_time = end_time.format("h:mm ");*/
                                 if(value_format=='8.15')
                                    {
                                        total_hours=total_hours.replace(':','.');
                                        own_account=own_account.replace(':','.');
                                    }
                                else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                                total_hours=total_hours.replace(':',',');
                                                own_account=own_account.replace(':',',');
                                                amount=amount.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                            }
                                            else if(separator=="dot")
                                            {
                                                total_hours=total_hours.replace(':','.');
                                                own_account=own_account.replace(':','.');
                                            }
                                    }
                            }
                            else if(time_style=='24 hours')
                            {
                                 /*start_time=moment(response['result'][i]['start_time'], ["h:mm A"]).format("HH:mm");*/
                               /* end_time=moment(response['result'][i]['end_time'], ["h:mm A"]).format("HH:mm");*/
                                if(value_format=='8.15')
                                    {
                                        /*start_time=start_time.replace(':','.');*/
                                        end_time=end_time.replace(':','.');
                                         break_time=replaceToComma(time_style,value_format,separator,break_time);
                                        total_hours=total_hours.replace(':','.');
                                        own_account=own_account.replace(':','.');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                                total_hours=total_hours.replace(':',',');
                                                own_account=own_account.replace(':',',');
                                                amount=amount.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                            }
                                            else if(separator=="dot")
                                            {
                                                total_hours=total_hours.replace(':','.');
                                                own_account=own_account.replace(':','.');
                                            }
                                    }
                            }
                            else if(time_style=='Industrial')
                            {    
                           /* start_time=moment(response['result'][i]['start_time'], ["h:mm A"]).format("HH:mm");*/
                            /*    end_time=moment(response['result'][i]['end_time'], ["h:mm A"]).format("HH:mm");     
                                  console.log(start_time+',/.'+end_time);*/
                                  /*  start_time=timeToDecimal(start_time);*/
                                    end_time=timeToDecimal(end_time);
                                     if (break_time!='') { break_time=timeToDecimal(break_time);} else{break_time='';}
                                    total_hours=timeToDecimal(total_hours);
                                    own_account=timeToDecimal(own_account);
                                    console.log(start_time+'/'+end_time);
                                    if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                              /*  start_time=start_time.replace('.',',');*/
                                                end_time=end_time.replace('.',',');
                                                 break_time=replaceToComma(time_style,value_format,separator,break_time);
                                                total_hours=total_hours.replace('.',',');
                                                own_account=own_account.replace('.',',');
                                                rate=rate.replace('.',',');
                                                amount=amount.replace('.',',');
                                            }
                                    }
                            }
                        }
                            var day =moment(response['result'][i]['date']).format('DD/MM/YYYY');

                            if (response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes'] !== "(null)") {
                                            var temp_notes = response['TotalTimesheetDetail'][i]['timesheetDetail'][j]['notes']
                                        }else{
                                            var temp_notes = "";
                                        }

                            res+='<tr><td class="tiny align_left">'+day+'</td> <td class="tiny align_left">'+response['result'][i]['activity_detail']['activity_name']+'</td> <td class="tiny align_right">'+start_time+'</td> <td class="tiny align_right">'+end_time+'</td> <td class="tiny align_right">'+break_time+'</td> <td class="tiny">'+rate+'</td> <td class="tiny">'+condition1+'</td><td class="tiny">'+condition1_from+'</td><td class="tiny">'+condition1_factor+'</td><td class="tiny">'+condition2+'</td><td class="tiny">'+condition2_from+'</td><td class="tiny">'+condition2_factor+'</td><td class="tiny align_right">'+bonus_hours+'</td><td class="tiny">'+tick+'</td> <td class="tiny align_right">'+own_account+'</td> <td class="tiny align_right">'+total_hours+'</td> <td class="tiny activity_slection"></td> <td class="tiny activity_slection"></td> <td class="tiny align_right">'+amount+'</td> <td class="tiny align_right">'+break_min+'</td> <td class="tiny align_right">'+response['result'][i]['total_hours_min']+'</td> <td class="tiny activity_slection"></td> <td class="tiny activity_slection"></td> <td class="tiny" style="text-align: left">'+ temp_notes +'</td> </tr>';
                        }
                        var finalTotalHours=minutesToStr(response['total']['finalTotalHours']);
                        var finalTotalHoursInDay=parseFloat(Math.round(response['total']['finalTotalHoursInDay'] * 100) / 100).toFixed(2);
                        var finalOwnAccount=minutesToStrDot(response['total']['own_account']);
                        var finalOwnAccountInDay=parseFloat(Math.round(response['total']['finalOwnAccount'] * 100) / 100).toFixed(2);
                        var finalAmount=parseFloat(Math.round(response['total']['amount'] * 100) / 100).toFixed(2);
                        console.log(finalAmount);   
                        if (time_style=='am/pm')
                        {
                           if(value_format=='8:15')
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        finalOwnAccount=finalOwnAccount.replace('.',':');
                                        finalOwnAccountInDay=finalOwnAccountInDay.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                                finalTotalHours=finalTotalHours.replace('.',',');
                                                finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                                finalOwnAccount=finalOwnAccount.replace('.',',');
                                                finalOwnAccountInDay=finalOwnAccountInDay.replace('.',',');
                                                finalAmount=finalAmount.replace('.',',');
                                            }
                                    } 
                        }
                        else if(time_style=='24 hours')
                        {
                            if(value_format=='8:15')
                                    {
                                        finalTotalHours=finalTotalHours.replace('.',':');
                                        finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                                        finalOwnAccount=finalOwnAccount.replace('.',':');
                                        finalOwnAccountInDay=finalOwnAccountInDay.replace('.',':');
                                    }
                                    else if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                               finalTotalHours=finalTotalHours.replace('.',',');
                                            finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                              finalOwnAccount=finalOwnAccount.replace('.',',');
                                            finalOwnAccountInDay=finalOwnAccountInDay.replace('.',',');
                                            finalAmount=finalAmount.replace('.',',');
                                            }
                                    } 
                        }
                        else if(time_style=='Industrial')
                        {
                            finalTotalHours=finalTotalHours.replace('.',':');
                            finalTotalHoursInDay=finalTotalHoursInDay.replace('.',':');
                            finalTotalHours=timeToDecimal(finalTotalHours);
                            finalTotalHoursInDay=timeToDecimal(finalTotalHoursInDay);
                            finalOwnAccount=finalOwnAccount.replace('.',':');
                            finalOwnAccountInDay=finalOwnAccountInDay.replace('.',':');
                            finalOwnAccount=timeToDecimal(finalOwnAccount);
                            finalOwnAccountInDay=timeToDecimal(finalOwnAccountInDay);
                            if(value_format=="Device region")
                                    {
                                         if(separator=="comma")
                                            {
                                               finalTotalHours=finalTotalHours.replace('.',',');
                                            finalTotalHoursInDay=finalTotalHoursInDay.replace('.',',');
                                               finalOwnAccount=finalOwnAccount.replace('.',',');
                                            finalOwnAccountInDay=finalOwnAccountInDay.replace('.',',');
                                            finalAmount=finalAmount.replace('.',',');
                                            }
                                    } 
                        }
                        res+='<tr><td class="tiny total_values"><?php echo trans ('reports.total')?></td><td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td> <td class="tiny total_values"></td><td class="tiny total_values activity_slection"></td><td class="tiny total_values activity_slection"></td><td class="tiny total_values activity_slection"></td><td class="tiny total_values activity_slection"></td><td class="tiny total_values activity_slection"></td><td class="tiny total_values activity_slection"></td><td class="tiny total_values activity_slection"></td><td class="tiny total_values activity_slection align_right">'+finalOwnAccount+'</td> <td class="tiny total_values align_right">'+finalTotalHours+'</td> <td class="tiny total_values activity_slection"></td> <td class="tiny total_values activity_slection"></td> <td class="tiny total_values align_right">'+finalAmount+'</td> <td class="tiny total_values"></td> <td class="tiny total_values align_right">'+response['total']['finalTotalHours']+'</td> <td class="tiny total_values activity_slection"></td> <td class="tiny total_values activity_slection"></td> <td class="tiny total_values"></td> </tr>';
                        res+='<tr><td class="tiny total_values1"><?php echo trans ('reports.total_in_days')?></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td>  <td class="tiny total_values1 activity_slection"> <td class="tiny total_values1 activity_slection"> <td class="tiny total_values1 activity_slection"> <td class="tiny total_values1 activity_slection"> <td class="tiny total_values1 activity_slection"> <td class="tiny total_values1 activity_slection"> <td class="tiny total_values1 activity_slection"> <td class="tiny total_values1 activity_slection"><td class="tiny total_values1"></td> <td class="tiny total_values1 align_right">'+finalOwnAccountInDay+'</td> <td class="tiny total_values1 align_right">'+finalTotalHoursInDay+'</td> <td class="tiny total_values1 activity_slection"></td> <td class="tiny total_values1 activity_slection"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1"></td> <td class="tiny total_values1 align_right">'+response['total']['finalTotalHours']+'</td> <td class="tiny total_values1 activity_slection"></td> <td class="tiny total_values1 activity_slection"></td> <td class="tiny total_values1"></td> </tr>';
                        $('#header_date_range').html('&nbsp'+start+'&nbsp'+ '<?php echo trans ('report_links.to')?>' +'&nbsp'+end+'&nbsp');
                        var message="<?php echo trans ('reports.date_range')?>: "+start+" <?php echo trans ('report_links.to')?> "+end;
                        var titleOf=toTitleCase("<?php echo trans ('report_links.two_line_one')?> <?php echo trans ('report_links.two_line_two')?>");
                        var companyOf="<?php echo trans('employer.companyname')?>: "+companyName;
                        $('#data_table').html(res);
                        $('.for_activity_select').hide();
                      /*  $('.activity_slection').hide();//hide offset and balance col*/
                        $(document).ready(function() {
                        $('#example-1').DataTable( {
                           'scrollX':true,
                           'ordering': false, 
                           language: {
                        "url": languageUrl,
                        },
                        dom: 'Bfrtip',
                        buttons: [
                        {
                        extend: 'copyHtml5',
                        text: '<i class="fa fa-files-o icon"></i>',
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'csvHtml5',
                        text: '<i class="fa fa-file-text-o icon"></i>',
                        message:employeeName+'\n'+companyOf+'\n'+message,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'excelHtml5',
                        text:'<i class="fa fa-file-excel-o icon"></i>',
                        message:employeeName+'\n'+companyOf+'\n'+message,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'pdfHtml5',
                        text:'<i class="fa fa-file-pdf-o icon"></i>',
                        orientation:orientationPdf,
                        pageSize: pageSize,
                        message:employeeName+'\n'+companyOf+'\n'+message,
                        title: titleOf,
                        customize : function(doc) {
                            console.log(doc)
                                 doc.content[2].table.widths = docWidth(doc.content[2].table.body[0].length);
                                 doc.content[2].table.body = fillColor(doc.content[2].table.body);
                                 doc.styles.tableHeader.fontSize = 7;
                                 doc.content[2].layout = 'noBorders';
                                 doc.content[2].margin =[0, 0, 0, 10];
                                 doc['footer']=(function(page, pages) {
                                                    return {
                                                    columns: ['',
                                                    {
                                                    alignment: 'right',
                                                    text: [pageTrans,
                                                    { text: page.toString(), italics: true },
                                                    ofTrans,
                                                    { text: pages.toString(), italics: true }
                                                    ]
                                                    }
                                                    ],
                                                    margin: [10, 0]
                                                    }
                                                });
                                doc.content.splice( 1, 0, {
                                alignment: 'right',
                                image: image64,
                                width: fitWidth,
                                height: fitHeight
                                } );
                                console.log(doc)
                        },
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                        extend: 'print',
                        text:'<i class="fa fa-print icon" aria-hidden="true"></i>',
                        message:employeeName+'\n'+companyOf+'\n'+message,
                        title: titleOf,
                        exportOptions: {
                        columns: ':visible'
                        }
                        },
                        {
                            extend: 'colvis',
                        text:'Select field to hide <i class="fa fa-filter icon" aria-hidden="true"></i>',
                        }
                        ],
                        "bSort": false,
                        "columnDefs": [
                        {
                        "targets": [ 0 ],
                        "visible": hide0,
                        "searchable": false
                        },
                        {
                        "targets": [ 1 ],
                        "visible": hide1,
                        "searchable": false
                        },
                        {
                        "targets": [ 2 ],
                        "visible": hide2,
                        "searchable": false
                        },
                        {
                        "targets": [ 3 ],
                        "visible": hide3,
                        "searchable": false
                        },
                        {
                        "targets": [ 4 ],
                        "visible": hide4,
                        "searchable": false
                        },
                        {
                        "targets": [ 5 ],
                        "visible": hide5,
                        "searchable": false
                        },
                        {
                        "targets": [ 6 ],
                        "visible": hide6,
                        "searchable": false
                        },
                        {
                        "targets": [ 7 ],
                        "visible": hide7,
                        "searchable": false
                        },
                        {
                        "targets": [ 8 ],
                        "visible": hide8,
                        "searchable": false
                        },
                        {
                        "targets": [ 9 ],
                        "visible": hide9,
                        "searchable": false
                        },
                        {
                        "targets": [ 10 ],
                        "visible": hide10,
                        "searchable": false
                        },
                        {
                        "targets": [ 11 ],
                        "visible": hide11,
                        "searchable": false
                        },
                        {
                        "targets": [ 12 ],
                        "visible": hide12,
                        "searchable": false
                        },
                        {
                        "targets": [ 13 ],
                        "visible": hide13,
                        "searchable": false
                        },
                        {
                        "targets": [ 14 ],
                        "visible": hide14,
                        "searchable": false
                        },
                        {
                        "targets": [ 15 ],
                        "visible": hide15,
                        "searchable": false
                        },
                        {
                        "targets": [ 16 ],
                        "visible": false,
                        "searchable": false
                        },
                        {
                        "targets": [ 17 ],
                        "visible": false,
                        "searchable": false
                        },
                        {
                        "targets": [ 18 ],
                        "visible": hide18,
                        "searchable": false
                        },
                        {
                        "targets": [ 19 ],
                        "visible": hide19,
                        "searchable": false
                        },
                        {
                        "targets": [ 20 ],
                        "visible": hide20,
                        "searchable": false
                        },
                        {
                        "targets": [ 21 ],
                        "visible": false,
                        "searchable": false
                        },
                        {
                        "targets": [ 22 ],
                        "visible": false,
                        "searchable": false
                        },
                        {
                        "targets": [ 23 ],
                        "visible": hide23,
                        "searchable": false
                        },
                        ],
                        } );
                        } );
                          $('#fakeLoader').hide();
                    }
                });
    }
    });

/*//date format dd-mm-yyyy
function format(inputDate) {
    var date = new Date(inputDate);
    if (!isNaN(date.getTime())) {
        var day = date.getDate().toString();
        var month = (date.getMonth() + 1).toString();
        // Months use 0 index.

        return (day[1] ? day : '0' + day[0])+ '-' +
           (month[1] ? month : '0' + month[0])  + '-' + 
           date.getFullYear();
    }
}*/
     //minutes to hour format function
    function minutesToStr(minutes) {
            //if (isNaN(minutes)) return '';
            var sign ='';
            if(minutes < 0){
            sign = '-';
            }

            var hours = Math.floor(Math.abs(minutes) / 60);
            var minutes = leftPad(Math.abs(minutes) % 60);

            return sign + hours +'.'+minutes;

    }

    function minutesToStrDot(minutes) {
            if (minutes=="") return "";
            return minutes = minutes.replace(":", ".");

            /*var sign ='';
            if(minutes < 0){
            sign = '-';
            }

            var hours = Math.floor(Math.abs(minutes) / 60);
            var minutes = leftPad(Math.abs(minutes) % 60);

            return sign + hours +'.'+minutes;
*/
    }

    function minToHrs(minutes) {
            //if (isNaN(minutes)) return '';
            var sign ='';
            if(minutes < 0){
            sign = '-';
            }

            var hours = Math.floor(Math.abs(minutes) / 60);
            var minutes = leftPad(Math.abs(minutes) % 60);

            return sign + hours +':'+minutes;

    }
    /*
    * add zero to numbers less than 10,Eg: 2 -> 02
    */
    function leftPad(number) {  
         return ((number < 10 && number >= 0) ? '0' : '') + number;
    }

    //60th to 100th unit for time format industrial
      function timeToDecimal(time)
        {
            if (time == '' || time ==0) 
            {
                return time;
            }
              substring = "-";
          checkNegative=(time.indexOf(substring) !== -1);
          if(checkNegative==true)
          {
          time=time.replace('-','');
             Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return '-'+parseFloat(Hours + Minutes/60,10).toFixed(2);
           }
           else
           {
            Hours = parseInt(time.split(':')[0],10);
            Minutes = parseInt(time.split(':')[1],10);
            while (Minutes >= 60)
            {
                Minutes = Minutes % 60;
                Hours ++;
            }
          test = parseFloat((Minutes/60)/10).toFixed(2);
            return parseFloat(Hours + Minutes/60).toFixed(2);
           }
        }
        //change caps to title case
        function toTitleCase(str)
        {
             return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }

        //decimal poistion to 2 decimal
        function decimalPoint(value)
        {
            if(value!='' && value!=0)
            {
                return parseFloat(Math.round(value * 100) / 100).toFixed(2); 
            }
            else
            {
                return value;
            }
        }
    </script>
<script src="<?php echo url('js/fakeLoader.min.js')?>"></script>
<script type="text/javascript">
$(".fakeloader").fakeLoader({
timeToHide:15000, //Time in milliseconds for fakeLoader disappear
zIndex:"999",//Default zIndex
spinner:"spinner2",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
color:"#000", //Hex, RGB or RGBA colors
});
/*var sel='';*/

function diffInArray(array2,array1)
{
    var foo = [];
var i = 0;
jQuery.grep(array2, function(el) {
    if (jQuery.inArray(el, array1) == -1) foo.push(el);
    i++;
});
return foo;
}
$('#Clear_btn').html(transOfClear);
</script>
 <!-- Data Table Scripts -->
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
 <script type="text/javascript" src="<?php echo url('js/buttons.html5.min.js')?>"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js"></script>
 <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo url('assets/js/timepicker/bootstrap-timepicker.min.js')?>"></script>

    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo url('assets/js/xenon-custom.js')?>"></script>
    
 <?php  include 'layout/footer.php';?>




