<?php

use App\Translation;

return Translation::where('translation_page','graph_report')->lists('translation_en', 'translation_key')->toArray();