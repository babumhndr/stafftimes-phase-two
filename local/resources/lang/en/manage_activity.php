<?php

use App\Translation;

return Translation::where('translation_page','manage_activity')->lists('translation_en', 'translation_key')->toArray();