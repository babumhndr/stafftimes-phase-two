<?php

use App\Translation;

return Translation::where('translation_page','report_links')->lists('translation_en', 'translation_key')->toArray();