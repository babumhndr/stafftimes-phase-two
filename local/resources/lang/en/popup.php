<?php

return array(

	/*timout page*/

	"login_again" => "Please login again",

	"logged_out" => "Welcome back! Your session expired due to inactivity.<br> Please login again.",

	/*welcome page*/

	"all_fields" => "Please fill out all the fields", // +footer + add_employee + contactus +dashboard +employer +activity.js +subscription

	"check_email" => "Please enter a valid email address", // +login +footer +add_employee +contactus +dashboard +forgot_password

	"password_minimum" => "Password required minimum 8 characters at least 1 Alphabet and 1 Number", // +forgot_password +employer

	"not_matching" => "Passwords are not matching", // +forgot_password

	"select_country" => "Please select Country",

	"select_language" => "Please select Language",

	"thank_you" => "We've sent you an activation link to your registered email id",

	"email_exists" => "Email already exists",

	/*Login*/

	"email" => "Please fill email id",

	"invalid" => "Invalid credentials",

	/*footer*/

	"mail_sent" => "We've received your request, our team will get back to you soon",

	"error" => "Error Occured", // +activity.js

	/*add_employee*/

	"expired_trail" => "Your trial period is expired. Please subscribe to use our service", // +dashboard

	"go_to_plans" => "Go to Plans and Pricing", // +dashboard

	"expired_subscription" => "Your subscription period is expired. Please subscribe to use our service", // +dashboard

	"invite_employee" => "We’ve sent an email invite to the employee to join.", // +dashboard +subscription

	"already_registered" => "That email is already registered on Staff Times.",  // +dashboard

	/*contactus.js*/

	"back_soon" => "Thanks for getting in touch, We will get back to you soon",

	"try_again" => "Please try again!",

	/*dashboard*/

	"welcome" => "Welcome",

	"invite_yourself" => "Please invite yourself to check Staff Times app",

	"cookie_text" => "This site uses cookies to deliver the best possible web experience. If you continue using the site, you provide consent to the use of cookies. For more information please consult our ",

	"privacy_policy" => "Privacy Policy.",

	/*email verification*/

	"verified" => "Your email id has been verified.",

	"login" => "LOGIN",

	/*forgotpasswod*/

	/*employer*/

	"added_successfully" => "Added successfully", // +activity.js +manage_template

	"company_name" => "Please enter company name",

	"password_changed" => "Password changed successfully",

	"old_password" => "Old password does not match",

	/*activity.js*/

	"you_sure" => "Are you sure ?", // +manage_template +listing

	"recover_activity" => "You will not be able to recover this Activity",

	"delete_it" => "Yes, delete it", // +manage_template +listing

	"deleted_successfully" => "Deleted successfully", // +manage_template

	"activity_exists" => "Activity name already exists", 

	"updated_successfully" => "Updated successfully", // +manage_template

	"reached_limit" => "You have reached a limit of 20!",

	"limited_space" => "This is due to limited space on mobile app screens", // +manage_template

	"deploy_it" => "Yes, deploy it!", // +manage_template

	"deployed_successfully" => "Deployed Successfully", // +manage_template

	"priority_of_activity" => "Sort order of activity",

	"priority_updated" => "Priority updated",

	"number_not_zero" => "Number cannot be zero",

	"number_more_than_activity" => "Number should not be more than total number of activity .i.e",

	"submit_button" => "Submit",

	"select_delete" => "Please select to delete",

	"seq" =>"Seq.",

	/*manage_template*/

	"reached_temp_limit" => "You have reached a limit of 25!",

	"recover_template" => "You will not be able to recover this template",

	"template_exists" => "Template name already exists!",

	/*listing*/

	"retrieve_employee" => "This member can be restored until the current term expires.",

	"cancel_it" => "No, cancel it",

	"employee_deleted" => "Employee Deleted",

	"cancelled" => "Cancelled",

	"not_deleted" => "Employee not deleted",

	/*subscribed*/

	"atleast_one" => "Please select atleast one user to subscribe.",

	"success"=>"Success",

	"deleted"=>"Deleted",

	"error_occurred"=>"Error Occurred",

	"employee_archived"=>"Employee archived",

	"employee_not_archived"=>"Employee not archived",
	
	"error_"=>"Error",

	"archive_it"=>"Yes, archive it",

	"archived"=>"Archived",

	"cancelled"=>"Cancelled",

	"great"=>"Great!",

	"saved_successfully"=>"Saved successfully",

	"fill_data"=>"Fill total in values",

	"dashboard_graph_title"=>"Offset vs Overtime - Averages per Month",

	"dashboard_graph_time"=>"Time (hours)",

	"dashboard_graph_overtime"=>"Overtime",

	"dashboard_graph_averge_offset"=>"Average Offset",

	"mark_read_successfully"=>"Mark  as read Successfully",

	"yes_make_it_read"=>"Yes, Make it read",

	"no_new_notification_translation"=>"No new notifications",

	"oops"=>"Oops!",

	"login_not_active_message"=>"Your account is not activated. Please check your e-mail to activate your account!.",

	"cancel"=>"Cancel",

	"email_not_registerd"=>"Email is not registered",

	"failure"=>"Failure",

	"succes_message"=>"Please check your mail",

	"unarchive_msg"=>"You Want to  Unarchive the employee ?",

	"unarchive_msg1"=>"Unarchive it",

	"unarchive_msg2"=>"Unarchived",

	/*pricing*/

	"feature_one_a"=> "Up to 20 people",

	"feature_two_a"=> "per year per user",

	"feature_three_a"=> "Time & Attendance",

	"feature_four_a"=> "Flexitime",

	"feature_five_a"=> "Project tracking",

	"feature_six_a"=> "Overtime reporting",

	"feature_one_b"=> "From 21 to 40 people",

	"feature_two_b"=> "per year per user",

	"feature_three_b"=> "Time & Attendance",

	"feature_four_b"=> "Flexitime",

	"feature_five_b"=> "Project tracking",

	"feature_six_b"=> "Overtime reporting",

	"feature_one_c"=> "From 41 people",

	"feature_two_c"=> "per year per user",

	"feature_three_c"=> "Time & Attendance",

	"feature_four_c"=> "Flexitime",

	"feature_five_c"=> "Project tracking",

	"feature_six_c"=> "Overtime reporting",

	/*cookie */

	"message" => "This site uses cookies to deliver the best possible web experience. If you continue using the site, you provide consent to the use of cookies. For more information please consult our",

	"agree" => "I agree",

	"cannot_delete" => "Cannot delete activity because it is used within Default Settings. Please check the <Fast check-in> value and the <Overtime handling> setting and update it if needed.",
 );