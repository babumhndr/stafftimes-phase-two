<?php

use App\Translation;

return Translation::where('translation_page','login')->lists('translation_en', 'translation_key')->toArray();