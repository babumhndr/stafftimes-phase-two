<?php

use App\Translation;

return Translation::where('translation_page','main_page_footer')->lists('translation_en', 'translation_key')->toArray();