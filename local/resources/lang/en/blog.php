<?php

use App\Translation;

return Translation::where('translation_page','blog')->lists('translation_en', 'translation_key')->toArray();