<?php

return array(

 "dear" => "Dear",

 "welcome" => "Welcome to Staff Times",

 "mobileapp" => "Staff Times is a mobile app that enables you to check-in to work, easily on the go and log working hours, overtimes and breaks. Your timesheets are synced to cloud and your employer gets easy access to real-time data and reports",

 "iPhones" => "The app is available for Android phones and Apple devices. Please tap on the link below to download the app, log in with your credentials and start logging time on your smartphone",

 "credentials" => "Your Staff Times login credentials are",

 "id" => "Login id",

 "token" => "Token",

 "questions" => "If you have any questions or need assistance please send an email to",

 "regards" => "Regards",

 "team" => "The Staff Times team",

 "subject" => "You’ve been invited to start using Staff Times",
);