<?php

use App\Translation;

return Translation::where('translation_page','activity_graph_report')->lists('translation_en', 'translation_key')->toArray();