<?php

use App\Translation;

return Translation::where('translation_page','welcome')->lists('translation_en', 'translation_key')->toArray();