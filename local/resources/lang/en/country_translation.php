<?php

use App\CountryNameTranslation;

return CountryNameTranslation::lists('translation_en', 'translation_key')->toArray();