<?php

return array(

	"terms" => "Terms of Service",

	"validity" => "1 Validity",

	"validity_body" => "These Terms of Service will be valid for all business relationships between the users and Staff Times - Daniel Gubler (hereafter, »DG«) for the provision of the Staff Times services. The supplemental short descriptions of the individual provisions serve only for better comprehensibility and are not the binding object of these terms of service. Any deviating terms and conditions of the user will not be valid unless they have been expressly approved in writing or text form by DG. By no later than when the account is created with Staff Times, the user must acknowledge the validity of these Terms of Service.",

	"DGs_services" => "2 DG’s services",

	"DGs_services_body_one" => "(1) Through the Staff Times service, DG is offering the possibility to track and analyze working time data.",  

	"DGs_services_body_two" => "(2) Staff Times is a service which is made available via the Internet in combination with a mobile App. The Internet access, which is needed in order to use Staff Times, is not the object of DG’s services.",

	"DGs_services_body_three" => "(3) DG reserves the right to expand and improve its services at any time. No claim exists to the offering of additional functionalities for the services described in Paragraph 1.",

	"Free_of_charge" => "3 Free-of-charge trial and conclusion of the contractual agreement",

	"Free_of_charge_one" => "(1) After you have successfully created an account, a usage relationship is considered to have been realised. The creation of the account is considered to have been successfully completed when the user confirms the creation of the account, e.g. by clicking on an activation link in an e-mail.",

	"Free_of_charge_two" => "(2) The user may test Staff Times for 30 days upon a free-of-charge basis. If the user does not enter his payment data under the »Account« tab at the end of this trial, the user’s account will be set to a “read-only” mode. By no later than 14 days after the trial ends, the account will be deactivated and the usage relationship will be ended. By no later than 28 days after the trial ends, DG reserves the right to delete all data of the account.",

	"Free_of_charge_three" => "(3) If the user enters his payment data under the »Subscription« tab and clicks on the »Subscribe« button, a contractual relationship triggering costs will be realised. Using Staff Times is based on a Software as a Service model (SaaS) whereby an charge applies per user and year. Detail of the plans and pricing can be found in the menu “plans and pricing.",

	"Free_of_charge_four" => "(4) The contractual relationship will run for an unspecified length of time.",

	"Payment_Invoice" => "4 Payment/Invoice",

	"Payment_Invoice_body_one" => "(1) The annual fee for using Staff Times is due at the end of each billing year. The annual fee can be paid by credit card.",

	"Payment_Invoice_body_two" => "(2) The user must ensure that the credit card account from which the amount is deducted has the required funds to cover the payment. If the payment is not made owing to circumstances for which the user is responsible, DG may charge the additional costs incurred (e.g. costs of the charge back) to the user in the respective amount incurred.",

	"Payment_Invoice_body_three" => "(3) Staff Times’s users will be provided with invoices via our payment transactions partner “Braintree”.",

	"Termination" => "5 Termination/Cancellation", 

	"Termination_body_one" => "(1) The user can terminate the contractual agreement for the use of Staff Times at any time under the »Subscriptions« tab. The termination will become effective at the end of the current billing year. Insofar as the user cannot use this option, the termination may also be declared in writing or in text form to DG.", 

	"Termination_body_two" => "(2) The user may at any time download back-up copies of his data in a standard format in his Staff Times account. If the account is cancelled, the account will be deleted after 14 days.", 

	"Termination_body_three" => "(3) Staff Times has the right to terminate the contractual relationship with a user without being required to state reasons for so doing by providing three months’ notice with the termination to become effective at the end of the respective billing year.", 

	"Termination_body_four" => "(4) If the user is late with his payment of the annual amount by more than eight weeks, Staff Times reserves the right to terminate the contractual relationship with the termination to become effective at the end of the current billing year. Staff Times’s claims, which have been created through the past usage of Staff Times by the user, will remain unaffected.",  

	"Termination_body_five" => "(5) If Staff Times is misused (see Clause 8 Para. 3) which results in substantial restrictions of Staff Times for third parties, Staff Times reserves the right to make extraordinary termination of the contractual relationship.",

	"Termination_body_six" => "(6) Any extraordinary right of termination held by the user or Staff Times will remain unaffected.",

	"User_obligations" => "6 User’s obligations",

	"User_obligations_one" => "(1) The user is obliged to truthfully provide information about his payment data (including the billing address) when the account is created or modified insofar as Staff Times is supposed to be used beyond the free-of-charge trial.",

	"User_obligations_two" => "(2) The user is obliged to protect his login credentials for his Staff Times account so they are not disclosed to unauthorised third parties.",

	"User_obligations_three" => "(3) The user is forbidden from misusing DG’s services, particularly the Staff Times service. Misuse is considered to have occurred especially in the following cases:",

	"User_obligations_sub_one" => "The publication or dissemination of illegal or defamatory content",

	"User_obligations_sub_two" => "The use of technical tools or methods which restrict or may restrict the functionality of the provider’s services (software, scripts, bots, etc.)",

	"User_obligations_four" => "(4) Staff Times is entitled to promptly delete content created by the user which is illegal and/or abusive.",

	"Usage_rights" => "7 Usage rights",

	"Usage_rights_one" => "(1) DG will grant each user a simple, non-exclusive right to use Staff Times for the duration of the contractual agreement for his own purposes. This usage right is non-transferrable.",

	"Usage_rights_two" => "(2) Staff Times is a web service which is rendered by accessing DG’s server or servers. Software is not transferred to the user.",

	"Usage_rights_three" => "(3) Insofar as new versions, updates, upgrades or other changes are made to Staff Times during the contractual term of the contractual agreement, the aforementioned provisions will be valid",

	"Availability" => "8 Availability of the services",

	"Availability_one" => "DG ensures that the services specified in Clause 2 Para. 1 will be available 95% of the time on an annual average. Excluded from this guarantee are service defects for which DG is not responsible.",

	"Data" => "9 Data protection", 

	"Data_one" => "(1) In principle, no personal data of the user will be passed on to third parties.",

	"Data_two" => "(2) Staff Times will process only the personal data of users which are required for the provision of the services for Staff Times.",

	"Data_three" => "(3) In order to process the payment transactions, personal data must be made available to third parties (payment service providers, banks and credit card companies). However, in this case, only those data will be made available which are absolutely required for the processing of the payment transactions. At no time will the user’s time tracking data be passed on to third parties.", 

	"Data_four" => "(4) After the contractual relationship ends, the personal data will be deleted insofar as no legal retention obligations exist. In these cases, the data will be blocked. Data from accounts which have been used only within the trial phase will be deleted by no later than 28 days after the trial ends.", 

	"Data_five" => "(5) Additional information about data protection and the purpose, type and scope of the collection, processing and use of personal data can be found in the Privacy Declaration section.",

	"Changes" => "10 Changes to these Terms of Service",

	"Changes_one" => "(1) DG reserves the right to make changes to these Terms of Service in order to, for example, make adjustments as the result of changes in legal directives or to introduce new services.",

	"Changes_two" => "(2) The user will be notified by e-mail of changes that are made to the Terms of Service. The changes will become effective if the user does not object to the changes within six weeks after the user receives the notification. The user will be specially notified in the e-mail of the possibility of lodging an objection and the deadline for so doing.",

	"Changes_three" => "(3) If the user objects to the changes in the Terms of Service, Staff Times will have the right to terminate and end the contractual relationship with the termination to become effective at the end of the current billing month.",

	"Final" => "11 Final provisions",

	"Final_one" => "Should individual parts of these provisions be or become invalid, the validity of the remaining provisions remain unaffected. In such a case, the Contracting Parties shall replace invalid provisions with provisions which are as close as possible to the invalid provisions. The legal relations with DG are exclusively subject to Swiss law. The application of the United Nations Convention on Contracts for the International Sale of Goods is excluded. Jurisdiction is Winterthur as the seat of DG. The latter reserves the right to prosecute the contractual partner at his domicile or another competent court.",

	"download" => "Download As"
);