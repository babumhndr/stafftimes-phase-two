<?php

return array(

	"employee_subscribed" => "Subscribed Employees",

	"employee_name" => "Employee Name",

	"employee_email" => "Employee Email",

	"employee_date" => "Subscribed Date",

	"employee_renewal" => "Renewal Date",

	"employee_verified" => "Verified Status",

	"employee_subscribed" => "Subscribed Status",

	/*index payment*/

	"payment_gateway" => "Stafftimes Payment Gateway",

	"company_name" => "Company Name",

	"placeholder_name" => "Name",

	"email" => "Email",

	"placeholder_id" => "merchantId",

	"amount" => "Amount",

	"placeholder_amount" => "Amount",

	"pay_now" => "Pay Now",

	"subscribe"=>"Subscribe",

	"employee_subscribed_status"=>"Subscribed",

	"not_yet"=>"Not Yet",

	"active"=>"Active",

	"inactive"=>"InActive",

	"archived"=>"Archived",

	"all_subscribed"=>"All Subscribed",

	"expired"=>"Expired"

);