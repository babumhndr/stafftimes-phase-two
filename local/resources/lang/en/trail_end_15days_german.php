<?php

return array(

	"welcome" => "Lieber Staff Timer",

	"dear" => "Dear",

	"body" => "Es sind 15 Tagen vergangen seit Ihrem Beitritt. Wir hoffen Sie konnten die verschiedenen Funktionen kennenlernen und die dazugehörige Mobile-App mit Ihren Mitarbeiter ausprobieren. <br>
Wir würden uns freuen von Ihnen zu hören. Bitte kontaktieren Sie uns per Telefon, Email unter CONTACTUS oder benutzen Sie einfach die Chat-Box innerhalb der Web-App (unten rechts). <br>
Falls dieses Produkt nicht Ihren Bedürfnisse gerecht wird oder es bestehen Schwierigkeiten die App in Ihr Arbeitsumfeld zu integrieren wären wir um Ihre Erfahrung dankbar um unser Produkt weiter zu entwickeln.",

	"body_next" => "Bitte nicht auf diese Email antworten sondern die oben erwähnten Möglichkeiten benutzen.",

	

	"regards" => "Mit freundlichem Gruss",
    
    "team_by" => "Daniel Gubler",

	"team" => "Staff Times",
);