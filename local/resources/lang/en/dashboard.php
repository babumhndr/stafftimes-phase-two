<?php

use App\Translation;

return Translation::where('translation_page','dashboard')->lists('translation_en', 'translation_key')->toArray();