<?php

use App\Translation;

return Translation::where('translation_page','price')->lists('translation_en', 'translation_key')->toArray();