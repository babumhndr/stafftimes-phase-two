<?php

return array(

	"welcome" => "Dear fellow Staff Timer",

	"dear" => "Dear",

	"body" => "It has been 15 days since your account was created. We hope you have been able to get familiar with the features and have tried out the mobile app with your staff. We would be happy to hear from you. Please contact us by phone or email at (link contactus) or simply use the chat box on your screen (lower right side) to start a conversation. If this product does not fit your needs or you face challenges like integration with your working tools we would appreciate your thoughts so that we can make it better. ",

	"body_next" => "Please do not reply to this email but use the channels indicated above.",

	

	"regards" => "Kind regards",
    
    "team_by" => "Daniel Gubler",

	"team" => "Staff Times",
);