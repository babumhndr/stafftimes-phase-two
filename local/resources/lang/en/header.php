<?php

use App\Translation;

return Translation::where('translation_page','header')->lists('translation_en', 'translation_key')->toArray();