<?php

return array(


	"dear" => "Dear",

	"thank_you" => "Thank you for contacting Staff Times",

	"contact" => "Our team will contact you soon",

	"regards" => "Regards",

	"team" => "Staff Times",
);