<?php

use App\Translation;

return Translation::where('translation_page','contact')->lists('translation_en', 'translation_key')->toArray();