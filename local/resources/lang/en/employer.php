<?php

use App\Translation;

return Translation::where('translation_page','employer')->lists('translation_en', 'translation_key')->toArray();