<?php

use App\Translation;

return Translation::where('translation_page','tour')->lists('translation_en', 'translation_key')->toArray();