<?php

use App\Translation;

return Translation::where('translation_page','settings')->lists('translation_en', 'translation_key')->toArray();