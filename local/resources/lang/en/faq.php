<?php

use App\Translation;

return Translation::where('translation_page','faq')->lists('translation_en', 'translation_key')->toArray();