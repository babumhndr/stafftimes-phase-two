<?php

return array(

    "welcome" => "Dear fellow Staff Timer",

    "dear" => "Dear",

    "body" => "How time passes in a flash. Please contact us if you need more time to test or require further clarification. After 30 days this account will be locked until a subscription is purchased. If this product does not fit your needs or you face challenges like integration with your other systems please share your thoughts so that we can add this to the 'Wish List' and make it better in future releases. ",

	"body_next" => "Please do not reply to this email but use the channels indicated above.",

	

	"regards" => "Kind regards",
    
    "team_by" => "Daniel Gubler",

    "team" => "Staff Times",

);