<?php

return array(

 "welcome" => "Welcome to Staff Times",

 "dear" => "Dear",

 "enterprise" => "By activating your account you gain access to the Staff Times control panel where you can set up your timesheet format and templates as well as delivering the smartphone Tracker-App by invitation to your team members. Please",

 "click_here" => "click here",

 "activate" => "to activate your enterprise account",

 "admin_app" => "In your role as the enterprise administrator, you can easily manage working hours, overtimes and attendance of your staff and get detailed reports. If you have any questions or need assistance please send an email to",

 "setup_guide" => "and check our “Setup guide” with tutorials and further documents",

 "regards" => "Kind regards,",

 "team" => "Staff Times",

);