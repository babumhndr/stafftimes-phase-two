<?php

use App\Translation;

return Translation::where('translation_page','backup')->lists('translation_en', 'translation_key')->toArray();