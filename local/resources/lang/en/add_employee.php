<?php

use App\Translation;

return Translation::where('translation_page','add_employee')->lists('translation_en', 'translation_key')->toArray();