<?php

use App\Faq;

return Faq::where('translation_page','faq_main')->lists('question_en', 'answer_en', 'translation_key')->toArray();