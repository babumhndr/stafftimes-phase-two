<?php

use App\FaqTitle;

return FaqTitle::where('translation_page','faq_topic')->lists('translation_en', 'translation_key')->toArray();