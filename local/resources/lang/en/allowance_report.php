<?php

use App\Translation;

return Translation::where('translation_page','allowance_report')->lists('translation_en', 'translation_key')->toArray();