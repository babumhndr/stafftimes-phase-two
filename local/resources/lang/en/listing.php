<?php

use App\Translation;

return Translation::where('translation_page','listing')->lists('translation_en', 'translation_key')->toArray();