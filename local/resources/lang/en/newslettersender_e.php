<?php

return array(

	"thank_you" => "Thank you for contacting Staff Times",

	"click_here" => "Our team will contact you soon",

	"regards" => "Regards",

	"team" => "Staff Times",
);