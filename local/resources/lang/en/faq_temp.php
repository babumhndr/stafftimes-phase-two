<?php

return array(

	"security" => "1) Security & privacy",

	"question_one" => "1.1. Is it possible to access Staff Times over an encrypted connection ?",

	"answer_one" => "Certainly! HTTPS is always active. All data sent back and forth between you and our servers is encrypted by default, just like in online banking.",

	"question_two" => "1.2. Is my data really secure ? what about backups ?",

	"answer_two" => "No worries: we do save additional copies of all data for safety reasons every single day. Not only once but twice. Furthermore, you can download a complete copy of your data anytime: just go to “Options =>Backup”. Your data is available as an XML file.",  

	"question_three" => "1.3 Where is my data stored ?",

	"answer_three" => "The main servers of Staff Times are housed in a high-security data center in Zurich, Switzerland.\n\n",

	"question_four" => "1.4 My data is confidential. How is it managed in detail ?",

	"answer_four" => "Your data is your data. Period. Your data will not be analyzed for commercial or other reasons. We will not sell your data. We won’t hand it over to third parties except for payment transactions (Partner: Braintree). The one and only exception will be made if we are forced by law in case of a serious crime. To improve our service on an ongoing basis, we will analyze some previously anonymized data for statistical reasons. These evaluations will include figures such as the number of time entries, projects or users per account.\nWe will save information in our server log files transmitted by your browser automatically, e.g. your operating system or your type of browser. We will delete this data after we evaluated it for statistical reasons. Please have a look at our privacy policy if you want to dive into further details.\n",

	"technical" => "2) Technical Questions",

	"question_five" => "2.1 Which desktop browsers are supported ?",

	"answer_five" => "Staff Times works fine on all major browsers on Windows, Mac OS and Linux if you run a version that’s up to date:<br/><br/>\n\n•\tMozilla Firefox 17.0+<br/>\n•\tChrome 21.0+<br/>\n•\tSafari 5.0+<br/>\n•\tInternet Explorer 8.0+<br/>\n",

	"question_six" => "2.2 Can I use Staff Times on my smartphone or tablet ?",

	"answer_six" => "As the administrator you can access your account from your smartphone or tablet too. Simply point the browser of your smartphone or tablet to the regular login address of your account and Staff Times will recognize your device and optimize the interface for smaller displays. However, for best user experience we suggest using the desktop browser.<br/><br/>\n\nAs a team member, for tracking your time you need to use the mobile apps made for your iPhone & Android smartphone. If you are a team member and your are tracking your time please install the app and request an invite from the administrator’s account. The invite will form part of your login credentials.\n",

	"question_seven" => "2.3.\tCan I use Staff Times on my own server ?",

	"answer_seven" => "No, this is not possible at the moment. Staff Times is only available as a web application hosted carefully on our servers. We do believe in centralized infrastructure – from a user’s perspective as well as from a developer’s. If you see a long term commitment to using this application and have own IT resources to support integration an exclusive license can be arranged on a case by case basis. Please contact us to discuss your situation.",

	"question_eight" => "2.4.\tCan I use Staff Times without an internet connection ?",

	"answer_eight" => "The administrator account requires an active internet connection. However, the associated team member Apps (Apple & Android) do not require an internet connection for tracking. Any time tracked or updates by the team member will be synchronized with the account administrator the next time that device is connected to the internet. The team member should use the SAVE button provided on the timesheet page to copy your data to the cloud.", 

	"trail" => "3) Trial & Contract", 

	"question_nine" => "3.1.\tHow does the 30-day free trial work ?", 

	"answer_nine" => "The 30-day trial period gives you the opportunity to check out Staff Times in detail. Free, fully featured, no credit card required. If you don’t want to continue using Staff Times after the first 30 days, just let the trial expire or cancel your account. Shortly after the trial expires, we'll delete all of your data. If you want to continue tracking your time with Staff Times after the 30-day trial period, please navigate to 'Plan & Pricing'. Staff Times will list the number of team members due for payment. Select the team members and click 'Subscribe' and you will be asked to fill-in your payment details and confirm the billed amount. No further actions required. Timesheets entered within the trial period will stay available of course. You will receive a receipt sent to your email address with the annual subscription charge for all subscribed members.  You can add additional team members to your account at any time following the 30-day trial subject to the applicable payment plan based on number of total active users at the time of purchase.\n", 

	"question_ten" => "3.2.\tIs there a minimum contract term ? Or a cancelation period ?",  

	"answer_ten" => "Staff Times is a pay-as-you-go subscription service which means there is no set up or cancellation fee. The minimum subscription period is one year. You can cancel your annual subscription at anytime, however, any unused segments of the term will not be refunded. Example: If your annual subscription started on Feb 1 (following the 30-day trial) and you canceled your subscription after 3 months the remaining 9 months would not be refunded. In addition, when your subscription term expires there is no automatic renewal so we will not charge you without your confirming your renewal. \n",

	"question_eleven" => "3.3.\tHow do I cancel my account ?",

	"answer_eleven" => "As your account's annual subscriptions do not renew there is no cancellation procedure. As the admin you may navigate to 'Plan & Prices' and 'Subscribed Users' to check when each member's subscription expires. Make sure to backup all your data before your subscriptions expire as the account may be deleted approx. 30 days after expiry if not renewed. There is no undo. The team member's time tracking history can be exported to a XLS or CSV file from the Detail Report. \n\nIf you wish to remove a specific team member from the reports then select that member under 'Staff & Reports' => Click on the Staff's name and click on Archive. This will turn that user’s data to read-only on his device ie. no more time tracking possible and this user will be removed from all reports. There is an option to restore that staff (Unarchive) up until the subscription expires.\n",

	"question_twelve" => "3.4. How do I renew my account ?",

	"answer_twelve" => "You may start renewals at 3 months from the anniversary date. Staff Times will send you a reminder 3 months before the expiry date of the subscription period of each staff member. There is no automatic renewal. If the subscription period has expired the respective member’s data will be made read-only if no action is taken. To initiate the renewal process please navigate to 'Subscribed Users' and by selecting the relevant team member from the subscriptions listing and clicking 'Subscribe'.",

	"question_thirteen" => "3.5.\tHow do I transfer a subscription to a newly joined member ?",

	"answer_thirteen" => "Sorry, this is not possible because each team member has a unique token ID to which all data history is linked. You would need to cancel (or let expire) the existing team member’s subscription and invite the new staff and create a new subscription for the new member.",

	"payment" => "4) Payment",

	"question_fourteen" => "4.1. How much does Staff Times cost ?",

	"answer_fourteen" => "Staff Times prices are as per the pricing plan > please refer to the relevant menu point when you log in to your Account. Discounts apply if the total active users in your account exceed a specific threshold. The company that brings you Staff Times is registered in Switzerland. Depending on your official location, stated in your billing address, and whether you use Staff Times as a business or as an individual, we'll have to charge VAT (a.k.a. Sales Tax). The annual subscription price is billed based on the number of users you select for subscription.\n",

	"question_fifteen" => "4.2.\tWhich types of payment do you accept ?",

	"answer_fifteen" => "We will be happy to accept your credit card (Visa or MasterCard). At present, we are not able to accept American Express, checks or PayPal, sorry.",

	"question_sixteen" => "4.3.\tWhere can I check my payment status ?",

	"answer_sixteen" => "You will receive an invoice and receipt from Braintree our transactions partner once you have completed a payment transaction. In addition, you can check under Team Members => ... for the payment status and under Subscriptions =>  for a history of your payments.",

	"login" => "5) Login",

	"question_seventeen" => "5.1.\tI forgot my login credentials. Help !",

	"answer_seventeen" => "Please have a look at the very first e-mail we sent you after your account or user was created. There, you will find your personal login credentials. It should consist of your email address and a password (for mobile App: Token-ID).\nCan’t find that e-mail? No worries, check the login screen, you’ll find the option to reset your password. Simply enter your e-mail address. A new password will be generated and sent to you via e-mail. Please check your spam folder if you didn’t get this e-mail after a couple of minutes.\nWe won’t be able to tell you your old password though. It is stored encrypted in our database – not even the Staff Times team can access your password because of security reasons. If this does not help, just mail us at: support@stafftimes.com. Please include anything you can remember: your user name, the e-mail address you signed up with or your account name. We’ll help you as fast as possible.\n", 

	"question_eighteen" => "5.2.\tHow do I change my Account password ?",

	"answer_eighteen" => "Click on your company name in the upper right-hand corner and find 'My Profile'. You will find the edit option (pencil icon) and in Edit mode an option to change the password. Don’t forget to confirm your changes by clicking on the corresponding button (“Save”).",

	"question_nineteen" => "5.3.\tCan I change my login Mail-ID ?",

	"answer_nineteen" => "This is not possible because it forms part of the Account ID to which all team members and subscriptions are tied. If you would like to transfer ownership of the account to someone else please contact us at support@stafftimes.com and we'll help.", 

	"team" => "6) Team",

	"question_twenty" => "6.1.\tIs Staff Times a multi-user software ?",

	"answer_twenty" => "Yes, you can work together with multiple team members on one account whereby the account is managed thru the Web interface and time tracking is done through mobile apps.",

	"question_twentyone" => "6.2.\tHow many users can be added to one account ?",

	"answer_twentyone" => "There are no technical restrictions, just invite as many users ie. team members as you want and need to.",

	"question_twentytwo" => "6.3.\tI have set up the account. What does that mean ? Can I change that ?",

	"answer_twentytwo" => "The account creator is the account owner and in this capacity he becomes the administrator with the rights to set the company wide defaults and invite users to join his account. Furthermore, the account owner is the only user who is allowed to access and edit team member's subscriptions. The account owner can transfer those rights to another user. However, there is no automatic transfer function, therefore, we ask you to kindly contact us at support@stafftimes.com for assistance. Please note that only one user can be account owner.",

	"question_twentythree" => "6.4.\tCan I limit the rights of a user ? Say, let him track his time only ?",

	"answer_twentythree" => "There are two different roles; the „account administrator“ and the „team member“. The account administrator manages team member’s subscriptions, maintains payment information, timesheet formats (activities, tasks, projects, services), templates, working hours, overtime rates and conditions. The team member may use those defaults to track his own time. He only has access to the timesheets he creates from his mobile app as well as having access to a limited number of settings which he can override (e.g. language selection, time format). Please find a detailed overview of all roles & rights here\n\nCreate a page using similar format as: https://mite.yo.lk/en/help/roles-and-rights.html\n",	

	"question_twentyfour" => "6.5. Is it possible to delete a user ?",

	"answer_twentyfour" => "No. However, we can offer an alternative called 'Archive'. By archiving, that user is moved to a tab named 'Archived' within Staff & Reports and removed from all Reports. Archived users won't be able to login anymore or update their time entries (read-only). An archived user can be restore provided the subscription has not expired. To archive a user, go to 'Staff & Reports' and click on the Staff's name > look for the Archive option. ",

	"Time_Tracking" => "7) Time Tracking",

	"question_twentyfive" => "7.1. Any suggestions on how to enter a starting balance ?",

	"answer_twentyfive" => "On the mobile tracker app do the following:<br/><br/>\n1) Go to the date you want to start tracking (or one day before)<br/>\n2) Tap on 'My Activities' button and choose 'Starting balance'* from the list<br/>\n3) Set the Offset value to zero hours and update the flat hours value with the balance you want to start with<br/>\n4) Page to the next day to start your daily time entries<br/><br/>\n*If you do not find this record then ask your Administrator to create it and deploy it to you. Account administrator: a new activity record can be set up with the following parameters 'Against Offset = Count, Flat time = ON'. When ready click on the Deploy button.\n",

	"question_twentysix" => "7.2.\tHow do I reduce my accrued Overtime balance ?",

	"answer_twentysix" => "If you want to compensate your surplus balance by starting late or leaving early (as part of a flextime model) there is no special entry required if your working hours are less than the offset value which will produce a negative balance for that day and reduces your overall year to date balance. Further options to reduce your balance are as follows:<br/><br/>Option A: Reduce balance by taking a working day off for compensation <br/><br/>\n1) Go to the calendar day you want to compensate<br/>\n2) Tap the '+' to create a blank sheet<br/>\n3) Ensure the offset shows the standard working hours (e.g. 8.00)<br/>\n4) Select Activity 'Flextime burn' or 'Comp time' (whichever suits best or as per Activity provided by the Administrator) <br/>\n5) The total work hours will show 0 and the day’s balance will show a negative offset amount (eg. -8.00) which will ultimately reduce your total year to date balance.<br/>\n6) Run your reports to check that your total balance amount has been reduced. <br/><br/>\n\nOption B: Reduce your balance using “Clear Overtime Balance” <br/><br/>\n1) If you want to clear your overtime balance as per current or for a specific month or week, this feature will calculate your balance as at the specified “as per” date and create a negated value of that balance to reduce the balance to zero <br/>\n2) Go to Settings page and “Clear Overtime Balance” where you can indicate the “Balance as per” Date. <br/>\n3) Confirm with “Go” to reveal the balance for the “As per” date. <br/>\n4) Now you have the option to insert a timesheets at the desired date which will reduce the balance to zero. Please select the Activity name* which has been preset to reduce your balance.<br/>\n5) Please verify the inserted timesheet. You may wish to add the total monetary amount which you have been paid into the additional field provided. Please note that you may only enter a lump sum of the total payout (there is no hourly rate calculation made). <br/>\n6) Run your reports to check that your total overtime balance has been reduced. The amount field is included in the “Detail Report”. <br/><br/>\n*Alternatively, if you do not find a pre-defined activity then your Account administrator can create one. Account administrator: create a new activity record and set parameter as Against Offset = Count, Flat time = ON, Overtime reducer = ON. <br/><br/>\n**Please note that a Reducer Activity can be added to an existing timesheet. For example, “At work” activity and the Reducer Activity included in the same timesheet.\n",

	"question_twentyseven" => "7.3.\tHow do I manage my Overtime and hours at special rates ?",

	"answer_twentyseven" => "In the context of this app overtime refers to the total working hours which exceeds your daily offset hours which is shown at the bottom of the timesheet as the 'Day Balance'. <br/><br/>\nA basic example of overtime:<br/><br/>\nYou have an 8 hour working day. This is referred to as the 'Offset' and your workday's activities totaled to 9.15 so your Overtime for that day is 1.15. This is also called ‘surplus’ time. <br/><br/>\nYou might want to track this surplus time as it builds up and redeem this from your employer. In those companies and jobs where a variable and flexible work time model exists this might be called flextime. You might decide to compensate your surplus hours by coming in late or leaving early to burn your flextime balance or get extra pay for your surplus hours. Some industries have special regulations for surplus hours where extra pay may be considered when the total work hours per week exceed 40 hours as well as special rates on certain days (eg. for weekend work hours are calculated with a factor of 1.5x or 2x). The United States Labor Act classifies workers as 'exempt' or 'non-exempt' from Overtime tracking.<br/><br/>\nDays when special hourly factors apply <br/><br/>\nThe app enables you to gross up your hours by a factor in the following ways;<br/><br/>\n\na) using Daily conditions <br/>\nb) using an Activity set up with an hourly multiplier<br/>\nc) or a combination of both<br/>\n<br/>\nExample: you worked on the weekend for 7 hours and your hours count as 1.5x of the standard rate resulting in 3.30 bonus hours. You could enter this in the following ways;<br/><br/>\n\nOption A: use Daily Conditions <br/><br/>\n\n1) Go to a new timesheet page and tap on the upper right side icon<br/>\n2) Select the Daily Condition criteria for \"Time\" from 9:00am with factor at 1.50, Tick the Apply to Balance<br/>\n3) From default use the button \"Fast check-in\" or from the \"My Activities\" select \"At work\"<br/>\n4) Change the Activity to \"In Office\" (tap and select from list) which deducts the breaktime<br/>\n5) Tap on the Start field and set to: 9:00am and set End to 5:00pm with 1.00 hr break<br/>\n6) Update the Offset value to read 7.00 hrs<br/><br/>\n\nthese entries will result in:<br/><br/>\n\n    - Hours accounted: 7.00<br/>\n    - Bonus hours: 3.30<br/>\n    - Hours total: 10.30<br/>\n    - Day balance: 3.30<br/><br/>\n\nOption B: use an Activity with hourly multiplier<br/><br/>\n\n1) Go to a new timesheet page and remove any previous Daily conditions<br/>\n2) From default use the button \"My Activities\" select \"At work 1.5 rate\"<br/>\n3) Tap on the Start field and set to: 9:00am and set End to 5:00pm with 1.00 hr break<br/>\n4) Update the Offset value to read 7.00 hrs<br/><br/>\n\nPlease run the reports to verify your results. You may want to add the overtime pay you expect into the amount field. To have your pay calculated you simply activate the amount field for your Activity. To do this, please tap on the settings icon next to the activity name and switch on the Amount field as Hourly rate type.<br/><br/>\n\nIn the event where the weekend counts as pure overtime you could simply set the Offset value to 0.00 (instead of 7.00) which would result in a Daily balance of 10.30. <br/><br/>\n\nOn payday the predefined Activity 'Payments account' as provided by the administrated may be used (or any corresponding template) to enter the total hours and amount which will reduce your overtime balance accordingly.<br/><br/>\nWeekly Overtime: Date range when special hourly factors apply for hours in excess of X hours\nExample: your weekly work schedule has a 40 hours offset (daily 8 hr work day) and you are given a 1.5 hourly factor for every hour exceeding the 40 hours. You worked 45 hours (net of breaks). In this case, your daily timesheets would show your daily balance which would accumulate to 5 hours at the end of that week (check “Daily Summary” report to see total balance at end of that week). To calculate the 5 hrs by the 1.5 factor (5hrs * 1.5 = 7.30) and log the awarded 2.30 hrs to your balance you would use the “Overtime for Period” feature which will insert a timesheet with the 2.30 hours for that week.\n",

	"question_twentyeight" => "7.4. Please explain the feature “Overtime for Period\"",

	"answer_twentyeight" => "This enables you to identify those working hours which exceed a given threshold for the selected period and which should be calculated at special rates after (or exceeding) standard overtime for the period. These “bonus“ hours can be stored as a timesheet which will add to your overtime balance.<br/><br/>\r\nFor example, you have a standard 8.00 hours offset from Monday to Friday but you worked 9 hour days. This makes a total of 40 offset hours against a total of 45 working hours which result in 5.00 hours of standard overtime (or flextime). If your company grants an hourly bonus of 1.5 rate for hours which exceed a threshold of 40 working hours this feature will produce 2.30 hours (5.00 x 1.5 = 7.30 of which 2.30 is the bonus portion) which can be added to your balance from the list of Activity names (only flat type Activities considered).\r\n",

	"question_twentynine" => "7.5.\tWhat is meant by 'Offset' ?",

	"answer_twentynine" => "This refers to the scheduled daily working hours set by your employer (a.k.a. standard hours, planned or required hours). Normally, the offset is net of the lunch break as lunch is not a paid working hour. The app is designed to enable tracking your work time against your daily offset. However, some jobs may have a weekly or monthly working hours arrangement which may not be broken down to daily offset hours. In this case, the daily offset facility can be used for the weekly or monthly hours at the start of the period with all other daily offset set to zero. To assist with this, two weekly style templates are provided.",

	"question_thirty" => "7.6.\tWhat is the definition of 'Activity' in the context of this app?",

	"answer_thirty" => "The term 'Activity' is used to describe a unit to which you allocate your time. It is also known as 'Time account' or 'Time bank'. You may decide that a different term is more appropriate for the way you would like to use this app. For example, in some jobs this unit might rather be called a 'Task' or 'Project' or 'Shift'. In addition, you may consider using an Activity as an 'account' for deposits or balances (eg. overtime rate balances). <br/><br/>\n\nFor the administrator: A range of standard activities (including 'my templates') have been created for you as part of the setup process to give you an understanding of different ways to track your hours. However, you are free to change these to more detailed actions (eg. 'in Office' can be broken out to 'Office task A', 'Office task B', etc) or create accounts (eg. overtime work, overtime payouts)",

	"question_thirtyone" => "7.7.\tHow can I customize my timesheet formats ?",

	"answer_thirtyone" => "This option is available for the account administrator only. Within your control panel proceed as follows:<br/><br/> \n1)\tGo to “Timesheet formats” and click on “Activities” to find a pre-defined list of Activity records <br/>\n2)\tThe list can be updated to your preference by renaming, deleting, changing the order or creating new Activity records. <br/>\n3)\tClick on any Activity to see the detail view. The parameters set will determine how you see the input fields for time tracking and the calculations applied for that Activity. <br/><br/>\n\nThe parameters for your Activity record are as follows:<br/><br/>\n\n- Against Offset: determines whether the work hours will count or not count towards the daily offset value. A ‘not count’ situation will apply if you want to compensate a flexday<br/> \n- Flat break deduction: enable a lump sum deduction from your working hours. Disable this for real time tracking (timer style) <br/>\n- Own Account mode: means that the hours will be accounted for under a separate column within the Detail Report and will not count for the overtime balance<br/>\n- Hourly rate (multiplier): will multiply the hours (net of break deduction if applicable) by the factor indicated. Best used for activities with special rates like weekend work and call duty.<br/>\n- Amount: can be used for lump sum or hourly wages. If set to hourly rate this field will calculate the amount based on the total hours of the activity. <br/>\n- Allowance bank: the value is used to check the remaining credit against your timesheets. Commonly used for vacation allowance. <br/>\n- Flat time mode: wil replace start and end time with a single hours field. Option to auto-populate this field with the Offset value which is how the pre-defined Vacation and Sick day Activity records have been set up.<br/>\n- Overtime reducer: hours entered against this activity will reduce the daily balance. Best used in connection with overtime payouts.<br/>\n- Deploy: the deploy button will push the record to the team member's app with a notification message. Once deployed the record can still be edited or deleted. Please note that historic timesheets will not be impacted. To apply the change to historic timesheets the user must be advised to go to the relevant timesheet entries  and re-select the activity. The Detail Report may be used to filter the relevant activity's timesheets.<br/><br/>\nFor detail please refer to the screen tips provided when you hover your cursor over each element of the Activity record detail view. In addition, please refer to the Timesheets format PDF guide within the Setup Guide.\n",

	"question_thirtytwo" => "7.8.\tI updated an Activity’s setting, why is the change not showing up on the tracker ?",

	"answer_thirtytwo" => "Changes in the Activity’s settings made by the Account administrator will be applied on new timesheets in order to protect historic data from accidental updates. To apply changes to historic data the team member must go back to each existing sheet and reselect the activity for the change to apply (changes might be for example: name change, add flat break deduction field, add an amount field, switch to flat input mode, Count hours against Offset, Hours multiplier rate, etc.). You may use the Detail Report's Activity filter to identify the timesheets.",

	"question_thirtythree" => "7.9.\tWhat is meant by “Templates” ?",

	"answer_thirtythree" => "Templates enable you to store working day situations with a set of activities which facilitate time tracking when these situations are repetitive. For example, you might like to re-use a previous week's timesheet, need same activities every week or a combination of activities you need once a month for special occasions. <br/><br/>\nFor Account administrators; you can use the default templates provided to get started which you can change to suit your needs. Once ready please use the 'Deploy' button to push these to the team member's app. In addition, you can create a template for each day of the week within the 'Set working days' option. All deployed templates will appear in the mobile app. However, the team member may create his own templates and modify or delete them directly within the app.",

	"question_thirtyfour" => "7.10. How can I transfer my data to a new device ?",

	"answer_thirtyfour" => "Follow these steps: <br/> <br/> \r\n1)\tInstall the mobile app on the new device. You can re-install it direct from the Google Play Store or Apple’s App Store. <br/> \r\n2)\tSign in with your personal credentials (refer to the original invite mail) and your data will be made available from with the cloud database. During this process the app is restarted and you should now see all your old time sheets on the new device. <br/> \r\n3)\tFinally, please run your reports and check your settings to ensure all is as expected. <br/> <br/> \r\n\r\nPlease note that the tracker app is not optimized for running in parallel from more than one device. We are working on improvements. Please check our blog for the latest developments.\r\n",

	"Reports" => "8) Reports",

	"question_thirtyfive" => "8.1. How can I analyze my data beyond the standard reports ?",

	"answer_thirtyfive" => "You may wish to transfer your data to a spreadsheet where further filters, comments and formatting as well as graphical elements can be added. We suggest you run the “Detail Report” which contains all details of the timesheets and send the results to your computer in MS-Excel format (Settings “Report Send Options”). Alternatively, if you use a different spreadsheet application you may wish to save the report in the csv or html file format and copy or import the data to that spreadsheet application. The Detail Report includes the Total values in minutes to facilitate additional calculations you may wish to make (refer to the Detail Report filter where fields can be included or excluded).",

	"question_thirtysix" => "8.2. What is the Allowance Bank Report for ?",

	"answer_thirtysix" => "Allows tracking your vacation credit or sick days credit or any planned time. To use this report your administrator must fill-in your annual allowance (banked amount).<br/><br/>\r\n\r\nAdministrator:  refer to Default Settings and “Set Annual Allowance” (refer to the preset activity record for Vacation). For example, for a vacation allowance of 20 days with 8 hour working days (FTE) you would fill in 160.00 hours (8 hours * 20 days). The report total in Days will convert based on FTE value (App: “Hours to Days”)\r\n",

	"question_thirtyseven" => "8.3. How do I print my report from my mobile device ?",

	"answer_thirtyseven" => "If you are working from the mobile app then simply run the desired report and email the data using the Mail icon provided. From the email you have the following options: <br/><br/>\na)\tOpen the file attachment from a computer which has printing capability. <br/>\nb)\tOpen the file attachment within your mobile device mailbox if you have a wireless printer connected.",

);