<?php

use App\Translation;

return Translation::where('translation_page','price')->lists('translation_de', 'translation_key')->toArray();