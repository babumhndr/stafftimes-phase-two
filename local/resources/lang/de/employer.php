<?php

use App\Translation;

return Translation::where('translation_page','employer')->lists('translation_de', 'translation_key')->toArray();