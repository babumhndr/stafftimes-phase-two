<?php

use App\Translation;

return Translation::where('translation_page','reports-1')->lists('translation_de', 'translation_key')->toArray();