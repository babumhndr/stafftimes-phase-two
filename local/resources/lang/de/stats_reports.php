<?php

use App\Translation;

return Translation::where('translation_page','stats_reports')->lists('translation_de', 'translation_key')->toArray();