<?php

return array(

	/*timout page*/

	"login_again" => "Bitte Erneut anmelden",

	"logged_out" => "Willkommen zurück! Sie wurden wegen Inaktivität ausgeloggt (Sicherheitsmassnahme).",

	/*welcome page*/

	"all_fields" => "Bitte alle Felder erfassen", // +footer + add_employee + contactus +dashboard +employer +activity.js

	"check_email" => "Bitte die Email-Adresse prüfen.", // +login +footer +add_employee +contactus +dashboard +forgot_password 

	"password_minimum" => "Das Passwort benötigt im Minimum 8 Zeichen davon 1 Buchstaben and 1 Nummer (z.B. test1234)", // +forgot_password +employer

	"not_matching" => "Die Passwörter stimmen nicht überein", // +forgot_password

	"select_country" => "Land auswählen",

	"select_language" => "Sprache auswählen",

	"thank_you" => "Gratulation zur Registrierung. Bitte prüfen Sie Ihren Email-Eingang!",

	"email_exists" => "E-Mail existiert bereits!",

	/*Login*/

	"email" => "Bitte Email-Adresse erfassen",

	"invalid" => "Ungültige Zugangskriterien",

	/*footer*/

	"mail_sent" => "Mail verschickt",

	"error" => "Es ist ein Fehler aufgetreten", // +activity.js

	/*add_employee*/

	"expired_trail" =>  "Ihr Probeabo is abgelaufen. Bitte abonnieren, um unseren Service weiter zu benutzen", // +dashboard

	"go_to_plans" => "Gehen Sie auf Pläne und Preise", // +dashboard

	"expired_subscription" =>  "Ihr Probeabo is abgelaufen. Bitte abonnieren, um unseren Service weiter zu benutzen", // +dashboard

	"invite_employee" => "Wir haben eine Einladung per Email an den Mitarbeiter gesendet, um die App zu installieren und sich anzumelden.", // +dashboard

	"already_registered" => "Diese Email-Adresse ist bereits bei Staff Times besetzt.",  // +dashboard

	/*contactus.js*/

	"back_soon" => "Wir melden uns in kürze",

	"try_again" => "Bitte nochmals versuchen",

	/*dashboard*/

	"welcome" => "Herzlich willkommen",

	"invite_yourself" => "Bitte laden Sie sich selber ein, um die Staff Times Erfassungs-App kennenzulernen",

	"cookie_text" => "Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden. ",

	"privacy_policy" => "Weitere Infos ",

	/*email verification*/

	"verified" => "E-Mail wurde bestätigt.",

	"login" => "ANMELDUNG",

	/*forgotpasswod*/

	/*employer*/

	"added_successfully" => "Erfolgreich hinzugefügt", // +activity.js +manage_template

	"company_name" => "Bitte den Firmennamen erfassen",

	"password_changed" => "Das Passwort wurde erfolgreich geändert",

	"old_password" => "Altes Passwort stimmt nicht überein",

	/*activity.js*/

	"you_sure" => "Sind Sie sicher ?", // +manage_template +listing

	"recover_activity" => "Dieser Tätigkeits-Satz kann nicht wiederhergestellt werden!",

	"delete_it" => "Ja, löschen!", // +manage_template +listing

	"deleted_successfully" => "Erfolgreich gelöscht", // +manage_template

	"activity_exists" => "Dieser Tätigkeits-Namen besteht bereits!",

	"updated_successfully" => "Erfolgreich aktualisiert", // +manage_template

	"reached_limit" => "Sie haben die Begrenzung von 20 Zeichen erreicht!",

	"limited_space" => "Diese Einschränkung besteht wegen der Bildschirmgröße der mobilen App",

	"deploy_it" => "Ausführen!", // +manage_template

	"deployed_successfully" => "Satz erfolgreich ausgeliefert", // +manage_template

	"priority_of_activity" => "Bitte die gewünschte Reheinfolge wählen",

	"priority_updated" => "Reihenfolge wurde aktualisiert",

	"number_not_zero" => "Keine Null erlaubt",

	"number_more_than_activity" => "Die Zahl darf nicht ausserhalb des Bereichs liegen, d.h.",

	"submit_button" => "Absenden",

	"select_delete" => "bitte den zu löschenden Satz wählen",

	"seq" =>"Folge",

	/*manage_template*/

	"reached_temp_limit" => "Sie haben die Begrenzung von 25 Zeichen erreicht!",

	"recover_template" => "Diese Vorlage kann nicht wiederhergestellt werden",

	"template_exists" => "Dieser Vorlagen-Namen ist bereits vorhanden!",

	/*listing*/

	"retrieve_employee" => "Dieser Mitarbeiter kann bis Ende der aktuellen Laufzeit wiederhergestellt werden",

	"cancel_it" => "Nein, abbrechen",

	"employee_deleted" => "Mitarbeiter wurde gelöscht",

	"cancelled" => "Abgebrochen",

	"not_deleted" => "Mitarbeiter wurde nicht gelöscht",

	/*subscribed*/

	"atleast_one" => "Bitte wählen Sie atleast einen Benutzer zu abonnieren.",

	"success"=>"Hat geklappt",

	"deleted"=>"Gelöscht",

	"error_occurred"=>"Es ist ein Fehler aufgetreten",

	"employee_archived"=>"Mitarbeiter wurde archiviert",

	"employee_not_archived"=>"Mitarbeiter wurde nicht archiviert",

	"error_"=>"Fehler",

	"archive_it"=>"Ja, bitte archivieren",

	"archived"=>"Archivieren abgeschlossen",

	"cancelled"=>"Abgebrochen",

	"great"=>"Achtung!",

	"saved_successfully"=>"Erfolgreich gespeichert",

	"fill_data"=>"Gesamtwerte erfassen",

	"dashboard_graph_title"=>"Sollzeit and Überstunden - Monatsdurchschnittswerte",

	"dashboard_graph_time"=>"Zeit (Stunden)",

	"dashboard_graph_overtime"=>"Durchschnittliche Überstunden",

	"dashboard_graph_averge_offset"=>"Durchschnittliche Sollzeit",

	"mark_read_successfully"=>"Als gelesen markieren",

	"yes_make_it_read"=>"Ja, als gelesen markieren",

	"no_new_notification_translation"=>"Keine neue Meldungen",

	"oops"=>"Ups!",

	"login_not_active_message"=>"Ihr Konto ist nicht aktiviert. Bitte prüfen Sie die Email um Ihr Konto zu aktivieren !.",

	"cancel"=>"Abbrechen",

	"email_not_registerd"=>"Email ist nicht registriert",

	"failure"=>"Fehler",

	"succes_message"=>"Bitte überprüfen sie ihre mail",

	"unarchive_msg"=>"Wollen Sie den Mitarbeiter wiederherstellen ?",

	"unarchive_msg1"=>"Wiederherstellen",

	"unarchive_msg2"=>"Wiederhergestellt",

	/*pricing*/

	"feature_one_a"=> "Bis zu 20 Mitarbeiter",

	"feature_two_a"=> "pro Jahr und Benutzer",

	"feature_three_a"=> "Präsenzzeit",

	"feature_four_a"=> "Gleitzeit",

	"feature_five_a"=> "Projektzeit",

	"feature_six_a"=> "Überstundenverwaltung",

	"feature_one_b"=> "von 21 bis 40 Mitarbeiter",

	"feature_two_b"=> "pro Jahr und Benutzer",

	"feature_three_b"=> "Präsenzzeit",

	"feature_four_b"=> "Gleitzeit",

	"feature_five_b"=> "Projektzeit",

	"feature_six_b"=> "Überstundenverwaltung",

	"feature_one_c"=> "ab 41 Mitarbeiter",

	"feature_two_c"=> "pro Jahr und Benutzer",

	"feature_three_c"=> "Präsenzzeit",

	"feature_four_c"=> "Gleitzeit",

	"feature_five_c"=> "Projektzeit",

	"feature_six_c"=> "Überstundenverwaltung",

	/*cookie */

	"message" => "Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden.",

	"agree" => "Verstanden",

	"cannot_delete" => "Dieser Satz kann nicht gelöscht werden weil er als Vorgabe verwendet wird. Bitte unter Vorgaben die Einstellung <Schnelles Einchecken> und <Überstunden Zeitraum> prüfen und gegebenenfalls anpassen.",
);