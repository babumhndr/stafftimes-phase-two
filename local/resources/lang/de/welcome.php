<?php

use App\Translation;

return Translation::where('translation_page','welcome')->lists('translation_de', 'translation_key')->toArray();