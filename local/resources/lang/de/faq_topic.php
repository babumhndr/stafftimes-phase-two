<?php

use App\FaqTitle;

return FaqTitle::where('translation_page','faq_topic')->lists('translation_de', 'translation_key')->toArray();