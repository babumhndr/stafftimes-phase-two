<?php

return array(

	"thank_you" => "Danke für Ihre Kontaktaufnahme bei Staff Times",

	"click_here" => "Unser Team wird Sie in Kürze kontaktieren",

	"regards" => "Grüße",

	"team" => "Staff Times",
);