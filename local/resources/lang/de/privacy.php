<?php

return array(

	"privacy" => "Datenschutz bei Staff Times",

	"first_para" => "Staff Times nimmt den Schutz Ihrer persönlichen Daten sehr ernst und hält sich strikt an die Regeln der schweizerischen Datenschutzgesetze. Personenbezogene Daten werden durch Staff Times nur im erforderlichen Umfang erhoben, verarbeitet und genutzt. Eine Übermittlung von Daten an Dritte erfolgt lediglich zur Abwicklung der notwendigen Bezahlvorgänge und auch nur im jeweils erforderlichen Umfang. In keinem Fall werden die erhobenen Daten oder Kundenadressen verkauft, verleast oder in sonstiger Weise gewerblich veräußert.",

	"second_para" => "Die nachfolgende Erklärung gibt Ihnen einen Überblick darüber, wie wir diesen Schutz gewährleisten und welche Art von Daten zu welchem Zweck erhoben werden.",

	"Data_processing" => "Datenverarbeitung",

	"Data_body_one" => "Staff Times erhebt und speichert automatisch in seinen Server-Log-Files Informationen, die Ihr Browser an uns übermittelt. Dies sind:",  

	"Data_one" => "verwendetes Betriebssystem",

	"Data_two" => "Browsertyp/ -version",

	"Data_three" => "Referrer-URL (die zuvor besuchte Seite)",

	"Data_four" => "Hostname des zugreifenden Rechners (IP-Adresse)",

	"Data_five" => "Uhrzeit der Serveranfrage.",

	"Data_body_two" => "Staff Times kann diese Daten nicht bestimmten Personen zuordnen. Eine Zusammenführung dieser Daten mit anderen Daten wird nicht vorgenommen. Die IP-Adressen werden spätestens nach 72 Stunden gelöscht.",

	"Data_body_three" => "Um den Service weiter zu verbessern, werten wir einige Ihrer Konto Kennzahlen zu statistischen Zwecken aus. Dies beinhaltet die reine Anzahl Ihrer Zeiteinträge, erfassten Stunden, Projekte, Kunden, Benutzer und Logins.",

	"Personal_info" => "Sammlung persönlicher Informationen",

	"Personal_info_one" => "Einige persönliche Informationen werden gesammelt, wenn Sie ein Konto erstellen. Während der Testphase sind dies: Ihr Konto-Namen, Ihr Name und Ihre E-Mail-Adresse. Um Staff Times auch nach der Testphase nutzen zu können, müssen Sie außerdem eine Rechnungsanschrift angeben.",

	"Personal_info_two" => "Wenn Sie Staff Times per E-Mail oder Instant Messaging kontaktieren, speichern wir einen Datensatz dieser Korrespondenz. Wenn Sie auf dem Staff Times blog einen Kommentar abgeben, speichern wir Ihre IP-Adresse.",

	"Information_sharing" => "Weitergabe von Informationen", 

	"Information_sharing_one" => "Staff Times veröffentlicht keine persönlichen Informationen der Nutzer, wenn wir nicht schriftlich oder in Textform (beispielsweise per E-Mail) die ausdrückliche Genehmigung dazu erhalten haben oder gesetzliche Auskunftspflichten dazu bestehen.", 

	"Cookies" => "Cookies", 

	"Cookies_one" => "Staff Times verwendet an mehreren Stellen Cookies. Sie dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Die meisten der von uns verwendeten Cookies sind so genannte Session-Cookies. Sie werden nach Ende Ihren Besuchs automatisch gelöscht. Staff Times verwendet außerdem Cookies, um einen Nutzer während des gesamten Nutzungsvorgangs zu identifizieren. Dadurch muss sich der Nutzer nicht auf jeder aufgerufenen personalisierten Seite neu einloggen.", 

	"Cookies_two" => "Das Setzen von Cookies kann durch eine entsprechende Einstellung in eurem Browser verhindert werden. Allerdings ist Staff Times dann möglicherweise nicht mehr voll funktionsfähig.",  

	"Google_analytics" => "Google Analytics",

	"Google_analytics_one" => "Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (“Google”). Google Analytics verwendet sog. “Cookies”, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt.",

	"Google_analytics_two" => "Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.",

	"Safety_precautions" => "Sicherheitsmaßnahmen",

	"Safety_precautions_one" => "Ihr Staff Times Konto ist passwortgeschützt, sodass nur der jeweilige Nutzer Zugriff auf seine persönlichen Informationen hat. Wir empfehlen allen Nutzern, ihr Passwort in regelmäßigen Abständen zu wechseln",

	"Safety_precautions_two" => "Wenn Sie Staff Times über öffentliche Netze oder an öffentlichen Orten wie Internetcafés nutzen, rufen Sie Staff Times bitte unbedingt über eine verschlüsselte Verbindung (HTTPS) auf. Melden Sie sich von Ihre Staff Times Konto ab (Ausloggen) und schließen Sie das Browserfenster, wenn Sie Ihre Arbeit beendet haben. Damit stellen Sie sicher, dass andere nicht auf Ihre persönlichen Informationen zugreifen können. In enger Zusammenarbeit mit unseren Hosting-Providern und Entwicklungspartner bemühen wir uns, die Datenbanken so gut wie möglich vor fremden Zugriffen, Verlusten, Missbrauch oder vor Fälschung zu schützen.",

	"Changes" => "Änderungen",

	"Changes_one" => "Ändern sich die Datenschutzbestimmungen grundlegend, verständigen wir alle Anwender von Staff Times per E-Mail. Eine aktuelle Version der Datenschutzbestimmungen findest Sie stets an dieser Stelle.",

	"More_info" => "Weitere Informationen",

	"More_info_one" => "Wenn Sie Fragen haben, die Ihnen diese Datenschutzerklärung nicht beantworten kann oder wenn Sie zu einem Punkt ausführlichere Informationen wünschen, können Sie sich jederzeit per E-Mail an support@stafftimes.com an uns wenden.",

	"download" => "Herunterladen As"	
);