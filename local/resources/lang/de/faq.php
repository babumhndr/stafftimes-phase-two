<?php

use App\Translation;

return Translation::where('translation_page','faq')->lists('translation_de', 'translation_key')->toArray();