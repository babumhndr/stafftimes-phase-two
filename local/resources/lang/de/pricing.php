<?php

use App\Translation;

return Translation::where('translation_page','pricing')->lists('translation_de', 'translation_key')->toArray();