<?php

use App\Translation;

return Translation::where('translation_page','dashboard')->lists('translation_de', 'translation_key')->toArray();