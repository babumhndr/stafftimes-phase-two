<?php

use App\Translation;

return Translation::where('translation_page','main_page_header_nav')->lists('translation_de', 'translation_key')->toArray();