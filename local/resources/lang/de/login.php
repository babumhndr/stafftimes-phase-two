<?php

use App\Translation;

return Translation::where('translation_page','login')->lists('translation_de', 'translation_key')->toArray();