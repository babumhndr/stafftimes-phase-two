<?php

use App\Translation;

return Translation::where('translation_page','add_employee')->lists('translation_de', 'translation_key')->toArray();