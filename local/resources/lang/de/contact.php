<?php

use App\Translation;

return Translation::where('translation_page','contact')->lists('translation_de', 'translation_key')->toArray();