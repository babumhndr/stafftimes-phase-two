<?php

use App\Faq;

return Faq::where('translation_page','faq_main')->lists('question_de', 'answer_de', 'translation_key')->toArray();