<?php

return array(

	"security" => "1. Sicherheit & Datenschutz",

	"question_one" => "1.1. Ist es möglich, Staff Times über eine gesicherte Verbindung zu benutzen ?",

	"answer_one" => "Ja, die Verbindung zwischen dir und unseren Servern erfolgt grundsätzlich verschlüsselt. HTTPS ist – ähnlich wie beim Online-Banking – immer aktiv.",

	"question_two" => "1.2. Sind meine Daten wirklich sicher? Erstellt ihr Backups?",

	"answer_two" => "Deine Daten werden von uns täglich gesichert, und zwar mehrfach redundant. Jede Nacht kopieren wir deine Daten komplett auf zwei physisch unterschiedliche Server. Darüberhinaus kannst du jederzeit unter Account → Backup eine vollständige Sicherungskopie deiner Daten im XML-Format herunterladen.",  

	"question_three" => "1.3. Wo werden meine Daten gesichert ?",

	"answer_three" => "Die Hauptserver von Staff Times stehen im Sicherheits-Rechenzentrum unseres Partners in Zürich CH.",

	"question_four" => "1.4. Meine Daten sind vertraulich. Was macht ihr genau damit ?",

	"answer_four" => "Deine Daten sind und bleiben deine Daten. Punkt. Deine Daten werden weder für werbe oder sonstige Zwecke personalisiert ausgewertet noch an Dritte weitergegeben. Unter keinen Umständen – solange uns nicht das Gesetz im Falle eines Kapitaldelikts dazu zwingt. Um den Service weiter zu verbessern, werten wir einige Daten anonymisiert zu statistischen Zwecken aus. Dies beinhaltet beispielsweise die Anzahl der Zeiteinträge oder Projekte.\n\nAußerdem erheben und speichern wir in unseren Server-Log-Files Informationen, die dein Browser automatisch an uns übermittelt, wie das von dir verwendete Betriebssystem oder deinen Browsertyp. Diese Daten werden nach einer statistischen Auswertung gelöscht. Für detailliertere Informationen kannst du gerne einen Blick in unsere Datenschutzerklärung werfen. Siehe Link beiliegend.",

	"technical" => "2. Technisches",

	"question_five" => "2.1. Welche Desktop-Browser werden unterstützt ?",

	"answer_five" => "Staff Times unterstützt aktuelle Versionen der gängigsten Browser auf Windows, Mac OS und Linux:<br/><br/>\n\n•\tMozilla Firefox ab Version 17.0<br/>\n•\tChrome ab Version 21.0<br/>\n•\tSafari ab Version 5.0<br/>\n•\tInternet Explorer ab Version 8<br/>",

	"question_six" => "2.2. Kann ich Staff Times auf dem Smartphone oder Tablet nutzen?",

	"answer_six" => "Als Administrator steht Ihnen Staff Times auch auf Ihrem Smartphone oder Tablet zur Seite.\n\nAuf deinem Smartphone oder Tablet kannst du deine reguläre Login-Adresse – bspw. www.stafftimes.de – einfach über den systemeigenen Browser ansteuern. Staff Times erkennt Ihr Gerät und passt die Darstellung für das kleinere Display an. Generell empfehlen wir jedoch den Desktop-Browser da von der Bildschirmgrösse bequemer zum bedienen.<br/><br/>\n\nAls Mitarbeiter für die Zeiterfassung müssen Sie die native Apps verwenden. Wenn Sie als Mitarbeiter die Zeiterfassung führen möchten, bitte die App installieren und eine Einladung vom Administrator anfordern. Die Einladung enthält Ihre Zugangsdaten. ",

	"question_seven" => "2.3. Kann ich mite auf meinem eigenen Server installieren?",

	"answer_seven" => "Nein, Staff Times wird zur Zeit und in absehbarer Zukunft ausschließlich als Version auf unserem Server angeboten. Wir glauben an die Vorteile von zentral betreuten Webapplikationen – aus Entwickler- wie auch aus Benutzersicht. Dennoch wenn Sie einen langfristigen Einsatz dieser Anwendung beabsichtigen und eigene IT Resourcen für die Integration aufbringen, kann eine Lizensierung in Betracht gezogen werden. Bitte kontaktieren Sie uns damit wir Ihre Bedürfnisse besser verstehen.",

	"question_eight" => "2.4. Kann ich Staff Times offline nutzen ?",

	"answer_eight" => "Das Administratoren Konto benötigt eine Internet Verbindung. Hingegen die Mitarbeiter-Apps benötigen keine Internet-Verbindung für die Zeiterfassung. Die erfasste Zeit wird bei der nächsten Internet Verbindung an den Administrator übertragen. Der Mitarbeiter benutzt dazu die Taste SPEICHERN im Zeiteingabebild. ", 

	"trail" => "3. Probephase & Vertrag", 

	"question_nine" => "3.1. Wie funktioniert die 30-tägige kostenlose Probephase?", 

	"answer_nine" => "Das Probe-Abo ist unverbindlich, kostenlos und ohne funktionale Einschränkungen irgendeiner Art. Das gibt Ihnen die Möglichkeit, das Produkt genauestens zu prüfen und wirklich herauszufinden, ob Staff Times zu Ihnen passt. Falls Sie nach 30 Tagen Staff Times nicht mehr weiter gebrauchen möchten, lassen Sie das Probe-Abo einfach auslaufen. Wir würden Ihr Konto anschliessend innert 30 Tagen löschen. Wenn Sie weiterhin die Zeiterfassung mit Staff Times betreiben möchten, steuern Sie bitte auf den Menupunkt 'Pläne & Preise'. Klicken Sie auf den enstprechenden Plan und Staff Times listet die Mitarbeiter für welche ein Abonnement gelöst werden kann. Wählen Sie die Mitarbeiter aus und klicken auf 'Abonnieren'. Nun können Sie Ihre Zahlungsangaben erfassen und die Rechnungssumme bestätigen. Anschliessend erhalten Sie eine Quittung der abgerechneten Mitarbeiter an Ihre Email-Adresse. Sie können nach dem 30-tägigen Probe-Abo jederzeit weitere Mitarbeiter hinzufügen. Der anwendbare Plan berechnet sich aufgrund der Anzahl aktiven Benutzer zum Zeitpunkt des Kaufes.", 

	"question_ten" => "3.2. Existiert eine Mindestvertragslaufzeit ? Oder Kündigungsfristen ?",  

	"answer_ten" => "Staff Times funktioniert im Abo-Modell (SaaS) daher gibt es weder eine Einrichtungsgebühr noch Kündigungskosten. Die Laufzeit des Abonnements ist jeweils ein Jahr. Sie können das Jahresabo jederzeit kündigen, jedoch verfallt die nicht genutzte Laufzeit. Beispiel: Wenn Ihr Jahresabo am 1. Februar gestartet ist (nach der 30-tägigen Probephase) and Sie kündigen Ihr Abo nach 3 Monaten führt dies dazu, dass die verbleibenden 9 Monate nicht zurückerstattet werden. Darüberhinaus wenn Ihr Abonnement abläuft, gibt es keine automatische Erneuerung was heissen will, dass für die folgende Laufzeit keine Belastung geschieht ohne Ihre Bestätigung.",

	"question_eleven" => "3.3. Wie kündige ich mein Konto ?",

	"answer_eleven" => "Da die Jahresabonnemente der angeschlossenen Mitarbeiter nicht erneuert werden, ist keine Kündigung nötig. Als Administrator können Sie auf den auf den Menüpunkt 'Pläne & Preise' > 'Abonnierte Benutzer' steuern um eine Übersicht der Mitarbeiter und das jeweilige Ablauf-Datum der Abonnements prüfen. Bitte daran denken, Ihre Daten zuvor zu sichern da Staff Times ca. 30 Tage nach Ablauf diese Daten löschen kann. Mitarbeiter Daten können im XLS or CSV Dateiformat aus dem Detailbericht exportiert werden. Wenn Sie das Abonnement eines bestimmten Mitarbeiter von den Berichten ausschliessen möchten, bitte im Menüpunkt 'Mitarbeiter & Berichte' den entsprechenden Mitarbeiter auswählen und auf Archivieren klicken. Dieser Vorgang setzt diesen Benutzer unter die Rubrik 'Archiviert'. Die Archivierung kann während eines laufenden Abonnements wiederhergestellt werden. ",

	"question_twelve" => "3.4. Wie kann ich mein Abonnement erneuern ?",

	"answer_twelve" => "Das Abo kann frühesten 3 Monate vor dem Ablaufdatum erneuert werden. Staff Times sendet eine Erinnerung 3 Monate vor dem Ablauf des Abonnements für jeden Mitarbeiter. Es gibt keine automatische Erneuerung. Falls das Abonnement verfällt und das betreffende Abo nicht erneuert wird, werden die Daten des Mitarbeiters schreibgeschützt bzw. die Zeiterfassung wird gesperrt. Um den Erneuerungsvorgang  einzuleiten, steuern Sie auf den Menüpunkt 'Abonnierte Benutzer' und wählen den entsprechenden Mitarbeiter aus der Abonnements-Liste und klicken auf 'Abonnieren'",

	"question_thirteen" => "3.5.\tWie kann ich ein bestehendes Abo auf einen neuen Mitarbeiter übertragen ?",

	"answer_thirteen" => "Das geht leider nicht weil jeder Mitarbeiter eine einmalige Token ID besitzt mit zugeordneter Datenhistorie. Sie müssten das bestehende Abo kündigen oder auslaufen lassen und für den neuen Mitarbeiter ein Abo abschliessen und ihn einladen.",

	"payment" => "4. Zahlungen",

	"question_fourteen" => "4.1. Wie viel kostet Staff Times ?",

	"answer_fourteen" => "Um die Preise abzufragen, bitte loggen Sie sich ein und prüfen den Menupunkt 'Pläne & Preise'. Es werden Abschläge angeboten wenn Ihr Konto eine bestimmte Anzahl aktive Benutzer überschreitet. Staff Times is in der Schweiz registriert. Aufgrund den Standort-Angaben Ihres Kontos und ob Sie Staff Times als Firma oder Privatperson verwenden, sind MwSt fällig. Der Jahresbeitrag wird berechnet aufgrund der Anzahl Mitarbeiter-Abo welches Sie einleiten.",

	"question_fifteen" => "4.2. Welche Zahlungsvarianten akzeptiert ihr?",

	"answer_fifteen" => "Ihre Zahlungen können Sie per Kreditkarte leisten (Visa oder MasterCard). Derzeit können wir leider keine American Express oder Paypal akzeptieren. Eine Zahlung auf Rechnung ist leider nicht möglich.",

	"question_sixteen" => "4.3. Wo kann ich den Zahlungsstand abfragen ?",

	"answer_sixteen" => "Wenn Sie den Zahlungsvorgang abgeschlossen haben, erhalten Sie eine Quittung von Braintree, unseren Zahlungsabwicklungspartner. Darüberhinaus können Sie den Stand des Abos und den Status unter .... abfragen.",

	"login" => "5. Einloggen",

	"question_seventeen" => "5.1. Ich habe meine Zugangsdaten vergessen. Und jetzt ?",

	"answer_seventeen" => "Ihre persönliche Anmeldedaten finden Sie in der allerersten Mail, die Sie von uns nach Erstellung Ihres Kontos oder Benutzerzugangs erhalten haben. Sie enthält Ihre Email und ein Passwort (Für App Benutzer das Token-Passwort)\nFalls Sie diese Mail nicht finden sollten, ist auch dies kein Grund zur Verzweiflung: Ihr Passwort liegt verschlüsselt in der Datenbank und kann nicht einmal vom Staff Times team ausgelesen werden. Beim Anmeldevorgang (Einloggen) können Sie aber ganz einfach ein neues Passwort anfordern. Nach der Angabe Ihrer E-Mail-Adresse wird es generiert und Ihnen kurz darauf per E-Mail zugeschickt. Dieser Vorgang dauert in der Regel nur wenige Sekunden.Falls das nicht weiter hilft, schicken Sie uns eine kurze E-Mail an support@stafftimes.com mit Ihrem Benutzernamen, Ihre verwendeten E-Mail-Adresse oder dem Titel bzw. Firmennamen des Kontos. Wir helfen, so schnell es geht.", 

	"question_eighteen" => "5.2. Wie ändere ich mein Konto-Passwort ?",

	"answer_eighteen" => "Ihr Passwort können Sie per Klick auf Ihren Firmennamen, rechts oben, um in die Ansicht 'Mein Profil' zu wechseln. Klicken Sie dort auf Ändern (Stift-Symbol) um von dort aus auf Passwort ändern zu klicken. Wählen Sie ein neues Passwort, wiederholen Sie die Eingabe und bestätigen Sie die Passwortänderung durch Klick auf die Taste 'Sichern'.",

	"question_nineteen" => "5.3. Kann ich meine Anmeldungs-Adresse ändern?",

	"answer_nineteen" => "Leider nein weil die Email für die Konto Kennzeichnung verwendet wird welches die Basis für die Mitarbeiter Abo bildet. Falls Sie eine Kontoinhaber Übertragung auf eine andere Person wünschen, bitte nehmen Sie Kontakt mit uns auf. Wir finden da eine Lösung.", 

	"team" => "6. Team",

	"question_twenty" => "6.1. Ist Staff Times mehrbenutzerfähig?",

	"answer_twenty" => "Ja, Sie können mit mehrere Mitarbeiter auf einem Konto arbeiten. Das Konto wird im Web verwaltet und die Zeiterfassung erfolgt vom Team über mobile Apps.",

	"question_twentyone" => "6.2. Wieviele Benutzer können mit einem Staff Times Konto arbeiten?",

	"answer_twentyone" => "Die Anzahl der Benutzer pro Konto ist technisch nicht begrenzt. Sie können so viele Teilnehmer bzw. Mitarbeiter einladen wie Sie wünschen.",

	"question_twentytwo" => "6.3. Ich bin Ersteller des Kontos. Was bedeutet das ? Lässt sich das ändern ?",

	"answer_twentytwo" => "Der Konto-Ersteller ist der Konto-Besitzer und erhält gleichzeitig die Administrator-Rechte welche das Einrichten der Standardwerte des Betriebs ermöglicht und weitere Benutzer hinzufügen bzw. einladen kann. Darüberhinaus ist der Ersteller für die Verwaltung des Abonnements zuständig. Der Konto-Ersteller kann diese Berechtigung auf einen anderen Benutzer übertragen. Leider gibt es dazu keine automatische Funktion. Falls Sie eine Übertragung wünschen, bitte melden Sie sich bei uns unter support@stafftimes.com. Bitte beachten, dass nur einen Benutzer der Konto-Besitzer sein kann.",

	"question_twentythree" => "6.4. Lassen sich die Rechte eines Benutzers einschränken?",

	"answer_twentythree" => "Staff Times kennt zwei Arten von Rollen; der \"Administrator\" und der \"Mitarbeiter\".  Der Administrator richtit die Standardwerte ein und verwaltet das Konto-Abonnement, der Mitarbeiter ist der Zeiterfasser bzw. erfasst seine Arbeitszeit unter Anwendung der vordefinierten Zeitkonten ein. Er hat die Möglichkeit bestimmte Einstellungen anzupassen (z.B. Sprachauswahl, Zeitformate, Vorlagen anpassen).",	

	"question_twentyfour" => "6.5. Kann ich Benutzer löschen ?",

	"answer_twentyfour" => "Nein. Wir bieten jedoch eine Alternative namens 'Archivieren'. Wenn Sie ein Benutzer archivieren, wird dieser unter der Rubrik 'Archiviert' eingeordnet und von allen Berichten entfernt. Archivierte Benutzer werden gesperrt und können Ihre Daten nicht mehr aktualisieren. Archivierte Benutzer können wieder hergestellt werden solange das entsprechende Abonnement nicht abgelaufen ist. Um den Benutzer zu archivieren, bitte auf den Menüpunkt 'Mitarbeiter & Berichte' wechseln und auf den Mitarbeiter Namen klicken und steuern auf die Funktion 'Archivieren'. ",

	"Time_Tracking" => "7. Zeiterfassung",

	"question_twentyfive" => "7.1. Wie kann ich ein Anfangssaldo bzw. mein Saldoübertrag erfassen ?",

	"answer_twentyfive" => "Auf der Zeiterfassungsapp gehen Sie wie folgt vor:<br/><br/>\n1.  Gehen Sie zu dem Termin (Datum) an dem sie die Zeiterfassung starten wollen bzw. den Tag davor<br/>\n2.  Tippen Sie auf die Taste 'Meine Tätigkeiten' und wählen Sie 'Startsaldo'* aus der Liste. <br/>\n3.  Aktualisieren Sie den vorgeschlagenen Pauschalstundenwert mit Ihrem gewünschten Startsaldo<br/>\n4.  Nun sind Sie Startklar und können im nächsten Termin mit dem Erfassen Ihrer Arbeitszeiten beginnen<br/><br/>\n\n* Falls der Satz nicht vorhanden ist, bitten den Administrator kontaktieren, um diesen auszuliefern. Administrator: in der Web-App gehen Sie auf Zeitkonten' und erstellen Sie einen neuen Tätigkeitssatz und richten Sie diesen mit 'Sollzeit = abziehen' (Häkchen) ein und mit 'Pauschal = EIN und klicken auf Ausliefern.",

	"question_twentysix" => "7.2. Wie kann ich die gesammelten Überstunden zurücksetzen ?",

	"answer_twentysix" => "Wenn Sie im Rahmen eines flexiblen Arbeitszeitmodells Ihre Überstunden reduzieren können in dem sie später zur Arbeit kommen oder sich früher abmelden, braucht es keine besonderen Zeiteingaben weil der Tages-Saldo somit negative Stunden ausweist und ihre Überstunden gesamthaft reduziert werden.<br/><br/>\n\nWenn Sie ganze Tage kompensieren möchten und diese eventuell auszahlen lassen, gibt es folgende Möglichkeiten:<br/><br/>\n\nVariante A: Freier Tag als Zeitausgleich<br/><br/>\n\n1.  Gehen Sie zum abzugeltenden Termin\n2.  Tippen Sie auf '+' um eine Zeiteingabe an zulegen<br/>\n3.  Stellen Sie sicher, dass die Sollzeit den Arbeitstag entspricht (z.B. 8,00)<br/>\n4.  Wählen Sie den Satz 'Ausgleich'* oder 'Gleitzeit-Abbau' aus den vordefinierten Tätigkeiten<br/>\n5.  Nun wird das Tagestotal auf 0 stehen und der Saldo einen negative Sollzeitwert ergeben (z.B. -8,00)<br/>\n6.  Wenn Sie nun Ihre Daten abfragen, (gehen Sie zur Berichtsübersicht) können Sie feststellen, dass ihre Gesamtüberstunden um diesen Wert reduziert wurden.<br/><br/>\n\nVariante B: Überschusssaldo zurücksetzen durch „Auflösung Überstundensaldo“<br/><br/>\n\n1.  Falls Sie Ihren Überschusssaldo per heutigem Stand, per Vormonat bzw. per einem bestimmten Stand auflösen möchten, so wird diese Funktion Ihren Saldo zum genannten Datum ausrechnen und in einem weiteren Schritt den ermittelten Saldo als Zeiteingabe mit umgekehrtem Vorzeichen einfügen um den Saldo entsprechend auszugleichen.<br/>\n2.  Gehen Sie auf Einstellungen und „Auflösung Überstundensaldo“. Als erstes bestimmen Sie das Datum des Saldostandes bzw. bis zu welchem Datum soll der Überstundensaldo ermittelt werden.<br/>\n3.  Ermitteln Sie den Überstundensaldo in dem Sie auf „Los“ tippen.<br/> \n4.  Wenn Sie nun diesen Saldo auflösen möchten, haben Sie die Möglichkeit eine Zeiteingabe mit einem vordefinierten Tätigkeitssatz* zum gewünschten Datum mit dem Umkehrwert einzufügen (wiederum auf „Los“ tippen).<br/> \n5.  Nun prüfen Sie bitten die erstellte Zeiteingabe. Falls eine Auszahlung zu diesem Vorgang gehört, können Sie die entsprechende Geldsumme im Betragsfeld erfassen (es finden keine Berechnungen statt). <br/>\n6.  Prüfen Sie nun den Saldo in dem Sie die Berichte ausführen. Nun müssten Ihre Gesamtüberstunden um den entsprechenden Wert reduziert sein bzw. zurückgesetzt sein. Der Geldbetrag kann im Detailbericht abgefragt werden.<br/><br/>\n\n* Falls die Vorlage oder der entsprechende Tätigkeitssatz nicht vorhanden ist, kann Ihr Administrator diesen erstellen. Administrator: gehen Sie zu 'Zeitkonten' und erstellen Sie einen neue Tätigkeitssatz. Eine Saldosenkung bzw. ein entsprechenden Satz richten Sie mit folgenden Parameter ein: 'Sollzeit = abziehen', 'Pauschal = EIN' und 'Saldo reduzieren = EIN'. <br/><br/>\n\n**Der Saldosenkungssatz kann auch zu einem bestehenden Eintrag (z.B. „Bei der Arbeit“) hinzugefügt werden (z.B. „Bei der Arbeit“ mit „Saldosenkung“ inklusive) hinzugefügt werden.\n",

	"question_twentyseven" => "7.3. Welche Möglichkeiten habe ich um Überstunden bzw. Stunden zu Sondersätzen zu verwalten ?",

	"answer_twentyseven" => "In Zusammenhang mit dieser App sind Überstunden die Summe der Arbeitsstunden welche die tägliche Sollzeit übertrifft. Der Wert wird jeweils am unteren Rande der Zeiteingabe als 'Saldo' aufgeführt. <br/><br/>\n\nEin Beispiel: Sie haben 8,00 Stunden Sollzeit vereinbart und ihre Arbeitszeit (abzüglich Pausen) ergibt 9,15 Stunden was bedeutet, dass Sie 1 Stunde und 15 Minuten Überstunden bzw. Überzeit geleistet haben. Bei Firmen mit Gleitzeitmodellen wird auch von Gleitzeit 'Überschuss' gesprochen. Diese Überstunden möchten Sie eventuell zu einem späteren Zeitpunkt in Form von Freizeit oder Geld beziehen bzw. ausgleichen. Wenn Sie mit einem flexiblen Arbeitszeitmodell arbeiten, können Sie diese Zeit in Form eines kürzeren Arbeitstages (später zur Arbeit kommen bzw. sich früher abmelden) oder eines freien Tages ausgleichen (siehe Frage 2). In bestimmten Industrien ist es üblich, dass die Arbeitszeit welche die 40-Stunden-Woche überschreitet, ausbezahlt wird. Im Weiteren kann Wochenendarbeit oder Nachtarbeit mit einem höheren Stundensatz berechnet werden (z.B. eine Stunde zählt 1,5-fache oder 2-fache einer einfachen Stunde).<br/><br/>\n\nWenn Sie Ihre Überstunden mit Ihrem Arbeitgeber abrechnen wollen, empfiehlt es sich die Stunden wie auch der geschuldete Betrag mit einem speziell eingerichteten Tätigkeitssatz zu erfassen.<br/><br/>\n\nTagen an denen spezielle Stundenfaktoren gelten<br/><br/>\n\nEs bieten sich folgende Varianten um Stundenzuschläge anzurechnen;<br/><br/>\n\na) Anwendung eines Tageszuschlages<br/>\nb) Anwendung eines Tätigkeitssatzes mit Stundensatz<br/>\nc) oder eine Mischung aus beiden<br/><br/>\n\nEin Beispiel wie dies in der App abgewickelt werden kann: Am Samstag wurde eine 7 Stündige Schicht geleistet wofür die Stunden zu 1,5-fach gelten was einen Zuschlag von 3,30 Stunden ergibt. Dazu wird folgende Zeiteingabe empfohlen:<br/><br/>\n\nVariante A: Anwendung eines Tageszuschlages<br/><br/>\n \n1.  Auf einen neuen Termin wechseln und oben rechts auf das Symbol tippen<br/>\n2.  Wählen Sie den Zuschlagtyp “Zeit” ab 9:00 Uhr mit dem Faktor 1,50, Zuschlag anrechnen auf EIN stellen<br/>\n3.  Auf dem Grundbild wählen Sie \"Schnell einchecken\" oder aus \"Meine Tätigkeitssätze\" den Satz “Bei der Arbeit”<br/>\n4.  Ändern Sie nun den Satz auf \"Im Büro\" (drauf tippen und auswählen) da dieser den Pausenabzug macht<br/>\n5.  Bitte den Startwert auf 9:00 ändern und Ende auf 17:00 mit 1,00 Std Pause einrichten <br/>\n6.  Setzen Sie die Sollzeit auf 7,00 Std.<br/>\n<br/> \ndiese Eingaben führen zu folgendem Ergebnis:<br/><br/>\n \n-    Arbeitsstunden: 7.00<br/>\n-    Stundenzuschlag: 3.30<br/>\n-    Stundentotal: 10.30<br/>\n-    Tagessaldo: 3.30<br/>\n<br/> \nVariante B: Tätigkeitssatz mit Stundensatz-Multiplikator<br/><br/>\n \n1.  Auf einen neuen Termin wechseln und bestehende Tageszuschläge entfernen<br/>\n2.  Auf dem Grundbild wählen Sie mittels Taste “Meine Tätigkeitssätze” den Satz  “Arbeit zu 1,5-fach”<br/>\n3.  Den Startwert auf 9:00 ändern und Ende auf 17:00 mit 1,00 Std Pause einrichten<br/> \n4.  Setzen Sie die Sollzeit auf 7,00 Std.<br/>\n<br/> \nBitte prüfen Sie die Darstellung der Saldi im jeweiligen Bericht. \n<br/> \n-    Falls für diese Stunden eine Entschädigung zutrifft, haben Sie die Möglichkeit die entsprechende Summe im Betragsfeld zu erfassen. Die Summe kann aufgrund der Stundensumme durch die App automatisch berechnet werden. Um das zu ermöglichen tippen Sie auf das Einstellungs-Symbol um in die Detailansicht des Satzes zu gelangen von wo Sie die Stundenlohn Berechnung einschalten (Tätigkeitsatz einrichten). <br/><br/>\n \n-    Am Zahltag bzw. am entsprechenden Termin kann die Tätigkeit 'Auszahlungskonto' angefügt werden womit alle abgegoltenen Überstunden und die entsprechende Geldsumme erfasst werden und der Überstundensaldo bis Dato um diesen Wert reduziert wird (führen Sie die Berichte aus um die Auswirkung zu überprüfen).<br/><br/>\n\nZeitraum bei dem spezielle Zuschläge gelten<br/><br/>\n\nEin Beispiel: Sie haben eine 40 Stundenwoche mit täglich 8 Stunden Sollzeit. Laut Betriebsreglement fällt ein Stundenfaktor von 1,5 an bei allen Stunden, die eine 40 Stundenwoche übersteigen. Es wurden insgesamt 45 Stunden (abzgl. Pausen) in dieser Woche gearbeitet. In diesem Fall würde der tägliche Saldo bis Ende der Woche 5 Stunden ansammeln (prüfe das Resultat im Bericht \"Tagesverlauf\" per Ende der Woche). Wenn nun der 1,5-Faktor (5 Stunden * 1,5 = 7,30) angewendet wird, werden daraus 2,30 Zusatzstunden ermittelt. Diese 2,30 Stunden können durch die Funktion „Überstunden im Zeitraum“ ausgerechnet und mit einer Zeiteingabe für die entsprechende Woche ergänzt werden.",

	"question_twentyeight" => "7.4. Wofür ist die Funktion “Überstunden im Zeitraum” ?",

	"answer_twentyeight" => "Hiermit können diejenigen Stunden ausgerechnet werden welche einen bestimmten Schwellenwert überschreiten und diese anschliessend, zu einem Sonderfaktor hochzurechnen. Daraus ergeben sich die „Bonus“ Stunden welche mit einer Zeiteingabe hinzugefügt werden und damit den Überstundensaldo entsprechend erhöhen. Diese Funktion kann speziell bei Überstunden welche auf Wochen- oder Monats- Basis ermittelt werden, nützlich sein.<br/><br/>\r\n\r\nEin Beispiel: bei einer Sollzeit von 8,00 Stunden vom Montag bis Freitag wurden 9-Stündige Tagen gearbeitet. Laut Betriebsreglement wird ab 40,00 Wochenstunden ein Stundenfaktor von 1,5 berechnet. Demnach ergibt ein Total von 40,00 Sollstunden gegenüber 45,00 Arbeitsstunden ein Überstundensaldo von 5,00 Stunden. Nun diese 5,00 Stunden werden mit dem Faktor 1,5 hochgerechnet was die 7,5 Stunden ergeben. Somit sind 2,30 Stunden (2 Std und 30 Min.) als „Bonus“ ermittelt worden. Dieser Wert kann mit dem vordefinierten Tätigkeitssatz zum erwünschten Termin hinzugefügt werden was der Gesamtsaldo entsprechend erhöht. Es empfiehlt sich die Berichte anschliessend auszuführen um den aktualisierten Saldo zu prüfen.",

	"question_twentynine" => "7.5. Was wird unter Sollzeit verstanden ?",

	"answer_twentynine" => "Damit versteht sich die mit dem Arbeitgeber vereinbarte tägliche Arbeitszeit. In der Regel wird die Mittagspause nicht bezahlt und somit nicht eingerechnet (z.B. ein Arbeitstag von 9:00 bis 17:00 mit 1 Std Pause ergibt 7 Std Arbeitszeit bzw. Sollzeit). Diese App wurde auf die Zeiterfassung mit Tagessoll eingerichtet. Es gibt jedoch Anstellungsverhältnisse bei welchen eine wöchentliche bzw. einen Monatssoll vereinbart wird. Womöglich kann in solchen Fällen keine tägliche Arbeitszeit berechnet werden. Es besteht in diesem Fällen die Möglichkeit am Anfang der Rechnungsperiode den Tagessoll mit dem Wochen- bzw. Monatssoll einzurichten. Zur Abhilfe wurden zwei vordefinierte Vorlagen für die Wochenarbeitszeit bereitgestellt.",

	"question_thirty" => "7.6. Was ist unter 'Tätigkeit' zu verstehen?",

	"answer_thirty" => "Darunter versteht sich im Allgemeinen eine Einheit an die Sie Ihre Stunden zuordnen, meist wird dies als \"Zeitkonto\" bezeichnet. Je nach dem wie Sie diese App einsetzen, gibt es eventuell andere Bezeichnungen, die zutreffender wären. In verschiedenen Stellen dürfte von 'Aufgaben', 'Projekte' oder 'Schichten' die Rede sein. Des weiteren könnte unter Tätigkeit auch ein 'Konto' verstanden werden (bspw. zur Verwaltung spezieller Überstunden).<br/><br/>\n\nFür den Administrator: Es wurden vordefinierte Tätigkeiten (auch als Vorlagen unter 'Meine Vorlagen') bei der Kontoeinrichtung bereitgestellt, um Ihnen die Möglichkeiten aufzuzeigen. Es ist Ihnen überlassen, diese zu mehr detaillierten Aufgaben aufzuteilen (z.B. 'im Büro' kann auf 'Büro Aufgabe A', 'Büro Aufgabe B' aufgeteilt werden, usw.), Konten zu erstellen (z.B. Überstunden Sondersatz, Sondersatz Auszahlungen) und andere Einheiten zu bilden.",

	"question_thirtyone" => "7.7.  Wie kann ich meine Arbeiten bzw. Tätigkeiten einrichten ?",

	"answer_thirtyone" => "Diese Möglichkeit besteht nur für den Administrator. Gehen Sie wie folgt vor:<br/><br/>\n\n1.  Gehen Sie zu 'Zeitkonten' und auf 'Tätigkeiten' um die vordefinierte Liste einzusehen.<br/>\n2.  Sie können die Tätigkeitssätze beliebig anpassen bzw. umbenennen, löschen, deren Reihenfolge ändern oder neue anlegen.<br/>\n3.  Clicken Sie auf ein Tätigkeitssatz, um in die Detailansicht zu gelangen. Die verschiedenen Einstellungen bestimmen wie die Eingabefelder erscheinen und deren Berechnung.<br/><br/>\n\n-    Gegenüber Sollzeit: bestimmt ob die Arbeitsstunden mit dem Tagessoll verrechnet werden soll oder nicht. Keine Verrechnung erfolgt beispielsweise beim Gleitzeitausgleich.<br/>\n-    Pauschalabzug Pause: bestimmt ob einen Pauschalwert von der Arbeitszeit abgezogen. Wenn Sie es vorziehen, in Echtzeit Ihre Pausen zu erfassen, kann diese Einstellung ausgeschaltet werden.<br/>\n-    Eigenkonto-Modus: bedeutet, dass diese Stunden unter einer separaten Spalte im Tätigkeitsbericht aufgeführt werden und für den Überstundensaldo nicht mitzählen.<br/>\n-    Betrag: es können Spesen und Ausgleichszahlungen als Gesamtbetrag ohne Berechnung erfasst werden oder Erfassung eines Stundenlohnes welche den Gesamtbetrag aufgrund der Stundensumme automatisch bei der Zeiteingabe berechnet. <br/>\n-    Stundensatz (Multiplikator): rechnet die Stunden abzüglich Pause um den gewählten Faktor hoch. Wir vorwiegend für Wochenendarbeit und Bereitschaftsdienst verwendet<br/> \n-    Pauschalmodus: bedeutet, dass anstelle der Eingabefelder für Start und Ende einen einzigen Total-Stundenwert erwartet wird. Als Vorgabe wird der Sollzeitwert direkt übernommen. Es können aber auch vordefinierte Stundenwerte festgelegt werden. Die Urlaubs-Tätigkeit wurde als pauschal vordefiniert.<br/>\n-    Jahressoll: das für ein Jahr geplante Guthaben bzw. Leistung. Dieser Wert wird für die Restguthaben- \nKontrolle verwendet (z.B. Urlaub) und mit dem Restguthabenbericht ausgewertet. <br/>\n\n-     Saldo reduzieren: die Saldosenkung ist im Pauschalmodus möglich. Die erfassten Stunden unter dieser Tätigkeit werden für die Senkung des Tagessaldos verwendet. Es empfiehlt sich diese im Falle einer Auszahlung zu verwenden. <br/>\n-    Die Tätigkeitssätze werden an die Mitarbeiter-App ausgeliefert wenn Sie auf die entsprechende Taste \"Ausliefern\" klicken. Was eine entsprechende Meldung in der App auslöst. Es besteht auch die Möglichkeit den Satz nachträglich d.h. nach der Auslieferung anzupassen oder zu löschen. Dies wird nicht auf bestehenden Zeiteingaben angewendet. Um die Anpassung auf bestehenden Zeiteingaben anzuwenden, muss der Benutzer zum betroffenen historische Termin steuern und den entsprechenden Tätigkeitssatz erneut auswählen.",

	"question_thirtytwo" => "7.8.\tWenn ich die Tätigkeit ändere, warum werden die Zeiteingaben nicht angepasst ?",

	"answer_thirtytwo" => "Änderungen (z.B. bei Namensänderung, Pauschalmodus EIN/AUS, Jahressollwert, usw.) werden nur auf neuen Zeiteingaben angewendet womit bestehende historische Zeiteingaben unverändert bleiben. Um die Änderungen rückwirkend anzuwenden, muss der Mitarbeiter bei jeder bestehenden Zeiteingabe die Tätigkeit jeweils neu selektiert werden. Um die betroffenen Termine ausfindig zu machen, verwenden Sie bitte den Tätigkeiten-Filter des Detailberichtes.\r\n\r\n",

	"question_thirtythree" => "7.9. Wie verwende ich \"Vorlagen\" ?",

	"answer_thirtythree" => "Durch Vorlagen können Sie typische Tätigkeiten, die sich in einem verändernden Zeitplan wiederholen als ‚vorgefertigte’ Zeiteingabe speichern. Als Beispiel könnten bestimmte wöchentliche Zeiteingaben oder Zeiteingaben, die Sie monatlich zu besonderen Anlässen benötigen zu einer Vorlage zusammengefasst werden. Sie können die unter 'Meine Vorlagen' bereitgestellten Vorlagen nutzen bzw. an Ihre Bedürfnisse anpassen. Darüber hinaus können Sie eine Vorlage für jeden Tag der Woche im Bereich 'Arbeitstage einrichten' erstellen.<br/><br/>\nDer Administrator kann Vorlagen an die Mitarbeiter ausliefern per Taste \"Ausliefern\" wonach eine Meldung in der Mitarbeiter-App erscheint. Die Mitarbeiter können diese Vorlagen anpassen und eigene erstellen und löschen. ",

	"question_thirtyfour" => "7.10. Wie kann ich meine Daten auf ein neues Gerät übertragen ?",

	"answer_thirtyfour" => "Bitte folgende Schritte beachten:<br/> <br/> \r\n1. Laden Sie die App auf Ihr neues Gerät herunter. Das geht direkt von Google Play (Android) und App Store (Apple).<br/> \r\n2. Loggen Sie ein mit Ihren persönlichen Zugangsdaten (siehe Original-Einladungsmail) und Ihre Daten werden aus der Cloud wiederhergestellt. Während diesem Vorgang wird die app neu geladen und nun sollten alle bisherigen Zeiteingaben auf dem Gerät erscheinen. <br/> \r\n3. Prüfen Sie Ihre Einstellungen und lassen Sie Ihre Berichte laufen um kontrollieren Ihren Saldo.<br/> <br/> \r\nDarüberhinaus beachten Sie bitte, dass die App für das gleichzeitige erfassen aus mehreren Geräten nicht optimiert wurde. Wir arbeiten an dieser Möglichkeit. Die neusten Meldungen bitte aus unserem Blog entnehmen.",

	"Reports" => "8. Berichte",

	"question_thirtyfive" => "8.1. Welche Möglichkeit habe ich, um meine Daten weiter auszuwerten ?",

	"answer_thirtyfive" => "Tabellenkalkulations-Programm zu überführen wo weitere Formatierungen, Text und grafische Elemente hinzugefügt werden können. Der Detailbericht enthält sämtliche Einzelheiten der Zeiteingaben und kann ins MS-Excel Format exportiert und von dort weiterbearbeitet werden (siehe Einstellung „Bericht Sendeoptionen“). Falls eine andere Tabellenkalkulations-Software verwendet wird welche das .XLS Format nicht erkennt, kann als Alternative die .CSV (Comma Separated Value)  oder HTML Datei importiert bzw. dorthin kopiert werden. Weitere Auswertungsvarianten bieten die Summenspalten (Stundentotal, Saldo) welche zusätzlich in Minutenwerte ausgegeben werden (siehe Detailberichtsfilter wo Spalten ein- und ausgeschlossen werden können).",

	"question_thirtysix" => "8.2. Wofür soll der Restguthabenbericht gut sein ?",

	"answer_thirtysix" => "Dieser Bericht ermöglicht das auswerten von Tätigkeiten, die mit einem Jahressoll bzw. Leistungsziel ausgestattet sind. Meist wird damit das Urlaubs-Restguthaben ausgewertet.<br/><br/>\r\n\r\nAdministrator: Um diesen Bericht kennenzulernen, legen Sie den vordefinierten Urlaubssatz als Zeiteingabe an und schauen anschliessend das Berichtsresultat an. Grundsätzlich können Sie für jeden Tätigkeitssatz ein Jahressoll setzen. Gehen Sie dazu unter Einstellungen auf „Jahressoll Einrichten“. Ein Beispiel: Wenn Ihr Urlaubs-Jahressoll 20 Tagen beträgt, dann rechnen Sie diese in Stunden um (8 Std * 20 Urlaubstage = 160.00 Stunden) und erfassen diesen als Jahressoll. Dieser Bericht berücksichtigt nur diejenigen Tätigkeiten bei denen einen Jahressoll gesetzt wurde. Um die Berichtssummen in Tagen darzustellen, prüfen Sie bitte die Angaben unter der Einstellung \"Arbeitstag Vollzeit\" (App: „Umrechnung in Tagen“)",

	"question_thirtyseven" => "8.3. Wie kann ich meine Berichte per Mobilgerät ausdrucken ?",

	"answer_thirtyseven" => "Sie führen den gewünschten Bericht aus und senden die Daten per Email. Aus dem Email heraus haben Sie folgende Möglichkeit:<br/><br/>\n\na.  Öffnen Sie den Datei-Anhang auf einem Computer mit Druckeranschluss<br/>\nb.  Öffnen Sie den Datei-Anhang auf Ihrem Gerät wenn Sie einen Wireless Drucker eingerichtet haben<br/>",

);