<?php

return array(

	"welcome" => "Lieber Staff Timer",

	"dear" => "Dear",

	"body" => "Und schon geht die Zeit im Nu vorbei. Bitte melden Sie sich wenn Sie mehr Zeit brauchen und weitere Abklärungen treffen möchten. Nach 30 Tagen kann das Konto mittels eines Abonnements freigeschalftet werden.
Falls dieses Produkt nicht Ihren Bedürfnisse gerecht wird bzw. Nachbesserungen nötig sind, wären wir um Ihren Input dankbar um unser Produkt weiter zu entwickeln. Wir führen eine Wunschliste.",

	"body_next" => "Bitte nicht auf diese Email antworten sondern die oben erwähnten Möglichkeiten benutzen.",

	

	"regards" => "Mit freundlichem Gruss",
    
    "team_by" => "Daniel Gubler",

	"team" => "Staff Times",
);