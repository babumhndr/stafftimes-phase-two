<?php

use App\Translation;

return Translation::where('translation_page','listing')->lists('translation_de', 'translation_key')->toArray();