<?php

use App\CountryNameTranslation;

return CountryNameTranslation::lists('translation_de', 'translation_key')->toArray();