<?php

use App\Translation;

return Translation::where('translation_page','blog')->lists('translation_de', 'translation_key')->toArray();