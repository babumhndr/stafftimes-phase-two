<?php

return array(

	"employee_subscribed" => "Abonnierte Mitarbeiter",

	"employee_name" => "Mitarbeitername",

	"employee_email" => "Mitarbeiter E-Mail",

	"employee_date" => "Startdatum",

	"employee_renewal" => "Ablaufdatum",

	"employee_verified" => "Bestätigung",

	"employee_subscribed" => "Abonnement-Status",

	/*index payment*/

	"payment_gateway" => "Staff Times Zahlungs-Gateway",

	"company_name" => "Name der Firma",

	"placeholder_name" => "Name",

	"email" => "Email",

	"placeholder_id" => "merchantId",

	"amount" => "Menge",

	"placeholder_amount" => "Menge",

	"pay_now" => "Bezahlen",

	"subscribe"=>"Abonnieren",

	"employee_subscribed_status"=>"Abonniert",

	"not_yet"=>"Noch nicht",

	"active"=>"Aktiv",

	"inactive"=>"Inaktiv",

	"archived"=>"Archiviert",

	"all_subscribed"=>"Alle Abonniert",

	"expired"=>"Abgelaufen",
);