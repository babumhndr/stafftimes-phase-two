<?php

use App\Translation;

return Translation::where('translation_page','header')->lists('translation_de', 'translation_key')->toArray();