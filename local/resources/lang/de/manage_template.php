<?php

use App\Translation;

return Translation::where('translation_page','manage_template')->lists('translation_de', 'translation_key')->toArray();