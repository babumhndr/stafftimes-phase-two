<?php

return array(

 "dear" => "Liebe(r)",

 "welcome" => "Willkommen bei Staff Times",

 "mobileapp" => "Staff Times ist eine mobile App, die es Ihnen ermöglicht, Ihre Präsenzzeiten und Überstunden jederzeit zu erfassen. Ihre Zeiterfassung wird mit der Cloud synchronisiert was Ihrem  Administrator eine einheitliche und aktuelle Berichterstattung ermöglicht",

 "iPhones" => "Die App ist für Android und Apple Geräte erhältlich. Bitte drücken Sie auf den Link unten, um die entsprechende App herunterzuladen, sich anzumelden und mit der Zeiterfassung auf Ihrem Smartphone zu starten",

 "credentials" => "Ihre Staff Times Anmeldeinformationen sind",

 "id" => "Email-ID",

 "token" => "Token-Passwort",

 "questions" => "Wenn Sie Fragen haben oder Hilfe benötigen, bitte Fragen Sie Ihren Administrator oder senden Sie uns eine E-Mail an",

 "regards" => "Mit freundlichen Grüßen",

 "team" => "Das Staff Times Team",

 "subject" => "Sie wurden eingeladen, Staff Times einzusetzen",
);