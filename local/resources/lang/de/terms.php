<?php

return array(

	"terms" => "Allgemeine Geschäftsbedingungen (AGB)",

	"validity" => "1. Geltung der Bedingungen",

	"validity_body" => "Diese allgemeinen Geschäftsbedingungen (AGB) gelten für alle Geschäftsbeziehungen zwischen Nutzern und der Staff Times - Daniel Gubler (nachfolgend DG genannt) für die Erbringung des Dienstes Staff Times. Maßgeblich ist die jeweils zum Zeitpunkt des Vertragsschlusses aktuelle Fassung. Die ergänzenden Kurzbeschreibungen der einzelnen Regelungen dienen nur der besseren Verständlichkeit und sind nicht verbindlicher Gegenstand dieser AGB. Etwaige abweichende Geschäftsbedingungen des Nutzers finden keine Anwendung, es sei denn, diesen wurde durch DG ausdrücklich schriftlich oder in Textform zugestimmt. Spätestens mit der Accounterstellung bei Staff Times erkennt der Nutzer die Geltung dieser Geschäftsbedingungen an.",

	"DGs_services" => "2. Leistungen von DG",

	"DGs_services_body_one" => "(1) DG bietet mit dem Dienst Staff Times die Möglichkeit an, Arbeitszeit zu erfassen und auszuwerten.",  

	"DGs_services_body_two" => "(2) Staff Times ist ein Dienst, der über das Internet zugänglich gemacht wird. Der Internetzugang, der für die Nutzung von Staff Times benötigt wird, ist nicht Gegenstand der Leistungen von DG.",

	"DGs_services_body_three" => "(3) DG behält sich das Recht vor, die Leistungen jederzeit zu erweitern und zu verbessern. Ein Anspruch auf das Anbieten weiterer Funktionalitäten über die in Absatz 1 beschriebene Leistung besteht nicht.",

	"Free_of_charge" => "3. Kostenlose Testphase und Vertragsschluss",

	"Free_of_charge_one" => "(1) Mit erfolgreich abgeschlossener Kontoerstellung kommt ein Nutzungsverhältnis zustande. Die Kontoerstellung ist erfolgreich abgeschlossen, wenn der Nutzer die Erstellung des Kontos z.B. durch Aufruf eines Aktivierungslinks in einer E-Mail bestätigt.",

	"Free_of_charge_two" => "(2) Der Nutzer kann Staff Times 30 Tage lang kostenlos testen. Gibt der Nutzer während dieser Testphase nicht seine Zahlungsdaten unter dem Menüpunkt Abonnieren ein, wird das Konto des Nutzers zunächst mit einer Nur-Lesen-Sperre versehen. Nach Ablauf der Testphase wird das Konto deaktiviert und das Nutzungsverhältnis beendet. Spätestens 28 Tage nach Ablauf der Testphase behält sich DG vor, alle Daten des Kontos zu löschen.",

	"Free_of_charge_three" => "(3) Gibt der Nutzer seine Zahlungsdaten unter dem Menüpunkt Abonnieren ein und klickt auf den Button „Abonnement abschliessen“, kommt ein kostenpflichtiges Vertragsverhältnis zustande. Die Kosten zur Nutzung von Staff Times kostet pro Nutzer pro Jahr werden im Menupunkt “Pläne und Preise” publiziert inkl. MwSt.",

	"Free_of_charge_four" => "(4) Das Vertragsverhältnis läuft auf unbestimmte Zeit.",

	"Payment_Invoice" => "4. Zahlung / Rechnung",

	"Payment_Invoice_body_one" => "(1) Die jährliche Vergütung für die Nutzung von Staff Times wird jeweils zum Ende des Abrechnungsjahres fällig. Die Zahlung der jährlichen Vergütung erfolgt durch Einzug per Kreditkartenzahlung.",

	"Payment_Invoice_body_two" => "(2) Der Nutzer hat dafür Sorge zu tragen, dass das Kreditkarten-Konto, von dem der Betrag abgebucht wird, über die erforderliche Deckung verfügt. Für den Fall, dass die Zahlung aufgrund von Umständen, die vom Nutzer zu vertreten sind, nicht erfolgt, kann DG die entstandenen Mehrkosten (z.B. Kosten der Rücklastschrift) dem Nutzer in der jeweils angefallenen Höhe berechnen.",

	"Payment_Invoice_body_three" => "(3) Nutzern von Staff Times werden jährliche Rechnungen durch unseren Abwicklungspartner “Braintree”  zur Verfügung gestellt",

	"Termination" => "5. Beendigung / Kündigung", 

	"Termination_body_one" => "(1) Der Nutzer kann den Vertrag für die Nutzung von Staff Times jederzeit zum Ende des laufenden Abrechnungsjahres unter dem Menüpunkt »Abonnement« kündigen. Sofern der Nutzer diese Möglichkeit nicht nutzen kann, kann die Kündigung auch schriftlich oder in Textform gegenüber DG erklärt werden.", 

	"Termination_body_two" => "(2) Der Nutzer kann jederzeit Sicherungskopien seiner Daten in einem weiterverarbeitbaren Format in seinem Staff Times Konto herunterladen. Nach einer Frist von 14 Tagen nach Kündigung des Kontos oder Ablauf des Abonnements wird das Konto gelöscht.", 

	"Termination_body_three" => "(3) DG hat das Recht, das Vertragsverhältnis mit einem Nutzer ohne Angabe von Gründen mit einer Frist von drei Monaten zum Ende eines Abrechnungsjahres zu kündigen.", 

	"Termination_body_four" => "(4) Sollte der Nutzer mit der Zahlung des jährlichen Betrages mehr als 8 Wochen in Verzug sein, behält DG sich das Recht vor, das Vertragsverhältnis zum Ende des laufenden Abrechnungsjahres zu kündigen. Ansprüche von DG, die durch die bisherige Nutzung von Staff Times durch den Nutzer entstanden sind, bleiben hiervon unberührt.",  

	"Termination_body_five" => "(5) Im Falle einer missbräuchlichen Nutzung (vgl. Ziff. 7 Absatz 3) von Staff Times, die zu erheblichen Beeinträchtigungen der Leistungen von DG für Dritte führt, behält DG sich die außerordentliche Kündigung des Vertragsverhältnisses vor.",

	"Termination_body_six" => "(6) Ein etwaiges außerordentliches Kündigungsrecht des Nutzers oder DG bleibt unberührt.",

	"User_obligations" => "6. Pflichten der Nutzer",

	"User_obligations_one" => "(1) Der Nutzer ist verpflichtet, Informationen zu seinen Zahlungsdaten (einschließlich der Rechnungsanschrift), die im Rahmen der Konto-Erstellung bzw. -Änderung angegeben werden, wahrheitsgemäß anzugeben, sofern Staff Times über die kostenlose Testphase hinaus genutzt werden soll.",

	"User_obligations_two" => "(2) Der Nutzer ist verpflichtet, die Zugangsdaten für seinen Staff Times Konto vor der unberechtigten Kenntnisnahme Dritter zu schützen.",

	"User_obligations_three" => "(3) Dem Nutzer ist es untersagt, die Leistungen von DG, insbesondere den Dienst Staff Times, missbräuchlich in Anspruch zu nehmen. Eine missbräuchliche Nutzung liegt insbesondere in folgenden Fällen vor:
",

	"User_obligations_sub_one" => "die Veröffentlichung oder Verbreitung rechtswidriger oder diffamierender Inhalte",

	"User_obligations_sub_two" => "die Nutzung von technischen Hilfsmitteln oder Methoden, die die Funktionsfähigkeit der Dienste des Anbieters beeinträchtigen oder beeinträchtigen können (Software, Skripte, Bots etc)",

	"User_obligations_four" => "(4) DG ist berechtigt, vom Nutzer erstellte Inhalte, die rechtswidrig und/oder missbräuchlich sind, unverzüglich zu löschen.",

	"Usage_rights" => "7. Nutzungsrechte",

	"Usage_rights_one" => "(1) DG räumt jedem Nutzer ein einfaches, nicht ausschließliches Recht ein, Staff Times für die Dauer des Vertrages für eigene Zwecke zu nutzen. Dieses Nutzungsrecht ist nicht übertragbar.",

	"Usage_rights_two" => "(2) Staff Times ist ein Webservice, der über den Zugriff zum Server bzw. zu Servern von DG erfolgt. Eine Überlassung der Software an den Nutzer erfolgt nicht.",

	"Usage_rights_three" => "(3) Sofern während der Laufzeit des Vertrages neue Versionen, Updates, Upgrades oder andere Änderungen an Staff Times vorgenommen werden, gelten die vorstehenden Regelungen.",

	"Availability" => "8. Verfügbarkeit der Leistungen",

	"Availability_one" => "DG gewährleistet eine Verfügbarkeit der Leistungen i.S.d Ziff. 2 Abs. 1 in Höhe von 95% im Jahresmittel. Von der Gewährleistung ausgeschlossen sind Leistungsmängel, die nicht von DG zu vertreten sind.",

	"Data" => "9. Datenschutz", 

	"Data_one" => "(1) Eine Weitergabe von personenbezogenen Daten des Nutzers an Dritte findet grundsätzlich nicht statt.",

	"Data_two" => "(2) DG verarbeitet nur die personenbezogenen Daten von Nutzern, die für die Erbringung der Leistungen für Staff Times erforderlich sind.",

	"Data_three" => "(3) Für die Abwicklung der Bezahlvorgänge ist eine Übermittlung personenbezogener Daten an Dritte (Zahlungsanbieter, Bankinstitute, Kreditkartenanbieter) zwingend erforderlich. Hier werden jedoch nur die Daten übermittelt, die für die Durchführung der Bezahlvorgänge absolut notwendig sind. Eine Übermittlung von Daten aus Zeiterfassungen des Nutzers erfolgt zu keiner Zeit. Für die Zahlungsabwicklung arbeiten wir mit unserem Vertragspartner “Braintree” zusammen.", 

	"Data_four" => "(4) Nach Beendigung des Vertragsverhältnisses erfolgt eine Löschung der personenbezogenen Daten, soweit keine gesetzlichen Aufbewahrungspflichten bestehen. In diesen Fällen erfolgt eine Sperrung der Daten. Daten von Accounts, die nur innerhalb der Testphase genutzt wurden, werden spätestens 28 Tage nach Ablauf der Testphase gelöscht.", 

	"Data_five" => "(5) Weitere Hinweise zum Datenschutz und zu Zweck, Art und Umfang der Erhebung, Verarbeitung und Nutzung personenbezogener Daten sind der Datenschutzerklärung zu entnehmen, die im Abschnitt Datenschutz einsehbar ist.",

	"Changes" => "10. Änderungen dieser AGB",

	"Changes_one" => "(1) DG behält sich vor, Änderungen an diesen AGB vorzunehmen, um z.B. Anpassungen an geänderte rechtliche Vorschriften vorzunehmen oder neue Leistungen einzuführen.",

	"Changes_two" => "(2) Der Nutzer wird per E-Mail über Änderungen an den AGB informiert. Die Änderungen werden wirksam, wenn der Nutzer nicht binnen 6 Wochen nach Zugang der Information den Änderungen widerspricht. Der Nutzer wird in der E-Mail gesondert auf die Widerspruchsmöglichkeit und deren Frist hingewiesen werden.",

	"Changes_three" => "(3) Widerspricht der Nutzer den Änderungen an den AGB, hat DG das Recht, das Vertragsverhältnis zum Ende des aktuellen Abrechnungsjahres zu kündigen und zu beenden.",

	"Final" => "11. Schlussbestimmungen",

	"Final_one" => "Sollten einzelne Teile dieser Bestimmungen ungültig sein oder werden, bleibt die Gültigkeit der übrigen Bestimmungen unberührt. In einem solchen Fall werden die Vertragsparteien ungültige Bestimmungen durch solche Bestimmungen ersetzen, die in ihrem wirtschaftlichen Zweck den ungültigen Bestimmungen möglichst nahekommen. Die Rechtsbeziehungen mit  DG unterstehen ausschliesslich schweizerischem Recht. Die Anwendung des Übereinkommens der Vereinten Nationen über Verträge über den internationalen Warenkauf („Wiener Kaufrecht“) ist ausgeschlossen. Gerichtsstand ist Winterthur als Sitz von DG. Diese behält sich vor, den Vertragspartner nach ihrer Wahl auch an dessen Domizil oder einem anderen zuständigen Gericht zu belangen.",

	"download" => "Herunterladen As"	
);