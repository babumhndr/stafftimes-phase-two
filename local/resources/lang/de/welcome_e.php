<?php

return array(

 "welcome" => "Willkommen bei Staff Times",

 "dear" => "Liebe(r)",

 "enterprise" => "Mit der Aktivierung erhalten Sie als “Administrator” den Zugang zur Steuerzentrale in der Cloud von wo Sie Ihre Zeitkonten und Erfassungsvorlagen einrichten und Ihren Mitarbeitern die Smartphone Tracker-App per Einladung ausliefern können. Bitte ",

 "click_here" => "hier klicken",

 "activate" => "um Ihr Konto zu aktivieren",

 "admin_app" => "Als Administrator können Sie zudem auf einfache Weise die Arbeitszeiten, Überstunden und Absenzen Ihrer Mitarbeiter verwalten und erhalten eine umfassende Berichterstattung. Falls Sie Fragen haben oder Hilfe brauchen, bitte senden Sie uns eine Email auf",

 "setup_guide" => "und beachten Sie die “Setup Anleitung”, wo Lernprogramme und weitere Dokumente bereitstehen",

 "regards" => "Mit freundlichem Gruss",

 "team" => "Staff Times",

);