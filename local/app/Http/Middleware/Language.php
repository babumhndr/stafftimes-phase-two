<?php

namespace App\Http\Middleware;

use Closure;
use App\LanguageUse;//language model
use Illuminate\Foundation\Application;
use Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Auth;
use App\Lang;
use App\UserBrowserLanguage;

class Language
{
    public function handle($request, Closure $next)
    {
        $getLanguage=Auth::user();
        $UserLanguage="";
            if (!empty($getLanguage)) {
               $getLanguage=$getLanguage['language'];
               App::setLocale($getLanguage);
            }
            else {
                $UserLanguage = UserBrowserLanguage::browserLanguage();
                App::setLocale($UserLanguage);
            }
/*        if (Session::has('applocale') AND array_key_exists(Session::get('applocale'), Config::get('languages'))) {
            App::setLocale(Session::get('applocale'));
        }
        else { 
            $getLanguage=Auth::user();
            if (!empty($getLanguage)) {
               $getLanguage=$getLanguage['language'];
               App::setLocale($getLanguage);
            }
            else
            {
            App::setLocale(Config::get('app.fallback_locale'));
            }
        }*/
        return $next($request);
    }
}