<?php
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);//language change api
Route::get('langChangeForBrowser/{lang}','LanguageController@langChangeForBrowser');//language change api for browser language

Route::get('/',['as' => '/', 'uses' => 'PageController@landing_page']);//landing page display api
Route::get('faq',['as' => 'faq', 'uses' => 'PageController@faq']);//faq page display api
Route::get('faqForMobileView',['as' => 'faqForMobileView', 'uses' => 'PageController@faqForMobileView']);//faq page display api
Route::get('tour',['as' => 'tour', 'uses' => 'PageController@tour']);//tour page display api
Route::get('price',['as' => 'price', 'uses' => 'PageController@price']);//price page display api
Route::get('termsandcondition',['as' => 'termsandcondition', 'uses' => 'PageController@termsandcondition']);//terms and condtion
Route::get('privacy',['as' => 'privacy', 'uses' => 'PageController@privacy']);//terms and condtion
/*Route::get('manage_template','PageController@manage_template');//manage template page display api
Route::get('manage_activity','PageController@manage_activity');//manage activity page display api*/
/*Route::get('profile','PageController@profile');//profile page display api*/
Route::get('login',  [ 'as' => 'login', 'uses' => 'PageController@login']);//login page display api
Route::get('login_ca',  [ 'as' => 'login_ca', 'uses' => 'PageController@caLogin']);
Route::get('companies',  [ 'as' => 'companies', 'uses' => 'PageController@companies']);
Route::get('add_company',  [ 'as' => 'add_company', 'uses' => 'PageController@add_company']);
Route::get('manage_users',  [ 'as' => 'manage_users', 'uses' => 'PageController@manage_users']);
Route::get('add_role',  [ 'as' => 'add_role', 'uses' => 'PageController@add_role']);
Route::get('manage_groups',  [ 'as' => 'manage_groups', 'uses' => 'PageController@manage_groups']);
Route::get('add_staff',  [ 'as' => 'add_staff', 'uses' => 'PageController@add_staff']);

Route::get('blog',['as' => 'blog', 'uses' => 'PageController@blog']);//blog page display api
Route::get('bloglanding/{id}',['as' => 'bloglanding', 'uses' => 'PageController@bloglanding']);//blog landing page display api
Route::get('contactus',['as' => 'contactus', 'uses' => 'PageController@contactus']);//contactus page display api

Route::post('companyRegister',['as' => 'companyRegister', 'uses' => 'CompanyController@companyRegister']);//registeration of company api
Route::get('activation/{token}/{email}/{language}','CompanyController@emailVerification');//email verification api
Route::post('/emailVerified','CompanyController@emailVerified');//email verified api
Route::post('invitePeople','EmployeeController@invitePeople');//invite employee by company api
Route::post('companyLogin', ['as' => 'companyLogin', 'uses' => 'CompanyController@companyLogin']);//login api
Route::post('companyUpdate','CompanyController@companyUpdate');//company information update api
Route::post('sendContactUs',['as' => 'sendContactUs', 'uses' => 'CompanyController@sendContactUs']);//send mail on contact us
Route::post('newsletterSend',['as' => 'newsletterSend', 'uses' => 'CompanyController@newsletterSend']);//send mail on contact us

Route::post('passwordChange','CompanyController@passwordChange');//company password change api

Route::post('forgotPassword','CompanyController@forgotPassword');
Route::get('resetPassword/{email}','CompanyController@resetPassword');
Route::post('resetPassword/updatePassword','CompanyController@updatePassword');
Route::get('timeout', ['as' => 'timeout','uses' =>'PageController@timeout']);
Route::get('logout1', ['as' => 'logout1','uses' =>'CompanyController@logout1']);//logout for meta refresh

Route::group(['middleware' => 'auth'], function() 
{
    
    Route::get('logout', ['as' => 'logout','uses' =>'CompanyController@logout']);//logout api
    Route::get('employee_profile/{id}','PageController@employeeprofile');//employee profile page display api
    Route::get('employee_profile1/{id}','PageController@employeeprofile1');//employee profile page display api test
    Route::get('profile/{id}','PageController@profile');//employer profile page display api
   	Route::get('listing','PageController@listing');//listing page display

	Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'PageController@dashboard']);//dashboard page display api
	Route::get('dashboard1', ['as' => 'dashboard1', 'uses' => 'PageController@dashboard1']);//dashboard page display api test1
	Route::get('manage_activity','PageController@manage_activity');//manage activity page display api
    Route::get('manage_template','PageController@manage_template');//manage template page display api
	Route::get('add_employee','PageController@add_employee');//add employee page display api
	Route::get('create_user','PageController@create_user');//add employee page display api 
	Route::get('settings',['as' => 'settings', 'uses' => 'PageController@settings']);//settings page display api
	Route::get('planner_report','PageController@planner_report');//planner report page display api
  	Route::get('reports_1/{id}','PageController@reports_1');//report page display api
  	Route::get('activity_graph_report/{id}','PageController@activity_graph_report');//activity graph report page display api
	Route::get('graph_report/{id}','PageController@graph_report');//graph report page display api
	Route::get('stats_reports/{id}','PageController@statistics_reports');//statistics report page display api
	Route::get('allowance_report/{id}','PageController@allowance_report');//allowance report page display api
	Route::get('reports/{id}','PageController@reports');//report page display api
	Route::get('plansandprice',['as' => 'plansandprice', 'uses' => 'PageController@plansandprice']);//plansandprice page display api
});
Route::get('backupPage','PageController@backupPage');
Route::get('backupListing','PageController@backupListing');
Route::get('restoreView','PageController@restoreView'); 
Route::post('importDate','BackUpController@importDate');
Route::post('employee_listing_active/{skip}','EmployeeController@employee_listing_active');//active employee listing api
Route::post('employee_listing_pending/{skip}','EmployeeController@employee_listing_pending');//pending employee listing api
Route::post('employee_listing_archive/{skip}','EmployeeController@employee_listing_archive');//archive employee listing api
Route::post('employee_listing_active_sort/{skip}','EmployeeController@employee_listing_active_sort');//active employee listing api
Route::post('employee_listing_pending_sort/{skip}','EmployeeController@employee_listing_pending_sort');//pending employee listing api
Route::post('employee_listing_archive_sort/{skip}','EmployeeController@employee_listing_archive_sort');//archive employee listing api
Route::post('employee_listing_active_search/{keyword}','EmployeeController@employee_listing_active_search');//active employee listing api
Route::post('employee_listing_pending_search/{keyword}','EmployeeController@employee_listing_pending_search');//pending employee listing api
Route::post('employee_listing_archive_search/{keyword}','EmployeeController@employee_listing_archive_search');//archive employee listing api

Route::post('employeeArchive','CompanyController@employeeArchive');//archive a employee api
Route::post('employeeUnArchive','CompanyController@employeeUnArchive');//un archive a employee api
Route::post('deleteEmployee','CompanyController@deleteEmployee');//delete a employee api

Route::post('profile/{id}/image1','CompanyController@uploadImage');//employer profile image upload api

Route::post('activityListForTemplate/{id}','CompanyController@activityListForTemplate');//activity list for company template api
Route::post('addCompanyTemplate','CompanyController@addCompanyTemplate');//adding company template api
Route::post('updateCompanyTemplate','CompanyController@updateCompanyTemplate');//updating company template api
Route::post('company_template_listing/{skip}','CompanyController@company_template_listing');//company template listing api
Route::post('company_template_edit/{id}','CompanyController@company_template_edit');//company template editing api
Route::post('company_template_delete/{id}','CompanyController@company_template_delete');//company template deleting api
Route::post('mot_template_listing/{skip}','CompanyController@mot_template_listing');//my overtime template listing api
Route::post('mot_template/{id}','CompanyController@mot_template');//my overtime template
Route::post('mot_activity/{id}','CompanyController@mot_activity');//my overtime activity
Route::post('mot_admin_template/{id}','CompanyController@mot_admin_template');//mot overtime template
Route::post('mot_admin_activity/{id}','CompanyController@mot_admin_activity');//mot overtime 
Route::post('mot_admin_template_edit/{id}','CompanyController@mot_admin_template_edit');//mot overtime template
Route::post('changeStatusDeploy','CompanyController@changeStatusDeploy');//change status deploy
Route::post('changeStatusDeployActivity','CompanyController@changeStatusDeployActivity');//change status deploy
Route::post('changeStatusDeployForSuperAdmin','CompanyController@changeStatusDeployForSuperAdmin');//change status deploy
Route::post('changeStatusDeployActivityForSuperAdmin','CompanyController@changeStatusDeployActivityForSuperAdmin');//change status deploy
Route::post('changePriority','CompanyController@changePriority');
Route::get('makePriorityZero','CompanyController@makePriorityZero');
Route::get('makePriorityZeroAdmin','CompanyController@makePriorityZeroAdmin');

Route::post('firstVisit','CompanyController@firstVisit');//massage notification for first vist of user

Route::post('addCompanyActivity','CompanyController@addCompanyActivity');//adding company activity api
Route::post('company_activity_listing/{skip}','CompanyController@company_activity_listing');//company activity listing api
Route::post('company_activity_edit/{id}','CompanyController@company_activity_edit');//company activity editing api
Route::post('company_activity_copy/{id}','CompanyController@company_activity_copy');//company activity editing api
Route::post('company_activity_delete/{id}','CompanyController@company_activity_delete');//company activity deleting api
Route::post('update_activity_color/{id}/{color}','CompanyController@update_activity_color');//color activity editing api
Route::post('companyActivityBulkDelete','CompanyController@companyActivityBulkDelete');//company activity deleting api
Route::post('updateCompanyActivity','CompanyController@updateCompanyActivity');//updating company activity api
//planner report
Route::post('getPlannerData/{id}/{from}/{to}','CompanyController@getPlannerData');
Route::post('selectSyncDates/{start}/{end}','CompanyController@selectSyncDates');
//ADMIN PANEL
Route::get('admin_login',function(){return view('admin.admin_login');});//admin login page display api
Route::post('adminLogin','CompanyController@adminLogin');
Route::group(['middleware' => ['auth', 'admin']], function() 
{
Route::get('admin_dashboard','CompanyController@adminDashboard');//admin dashboard api
Route::get('companyProfile/{id}','CompanyController@companyProfile');//admin access company view api
Route::get('companyEmployeeList/{id}','CompanyController@companyEmployeeList');//admin access company employee api 
Route::get('companyEmployeeList/companyEmployeeProfile/{id}','CompanyController@companyEmployeeProfile');
Route::get('adminManageTemplate','PageController@adminManageTemplate');
Route::get('adminManageActivity','PageController@adminManageActivity');
Route::get('adminSettings','PageController@adminSettings');
Route::get('admin_logout', ['as' => 'admin_logout','uses' =>'CompanyController@admin_logout']);//logout api
Route::post('companyListing','CompanyController@companyListing');//company listing for super admin
Route::post('companyEmployeeList/companyEmployeeListing/{id}','CompanyController@companyEmployeeListing');
Route::post('companyProfile/blockCompany/{id}','CompanyController@blockCompany');//block a company
Route::post('companyProfile/unblockCompany/{id}','CompanyController@unblockCompany');//block a company
Route::post('companyProfile/deleteCompany','CompanyController@deleteCompany');//block a company
Route::post('defaultTemplateListing/{skip}','CompanyController@defaultTemplateListing');//default template listing api
Route::post('defaultTemplateEdit/{id}','CompanyController@defaultTemplateEdit');//default template editing api
Route::post('defaultTemplateDelete/{id}','CompanyController@defaultTemplateDelete');//default template editing api
Route::post('addDefaultTemplate','CompanyController@addDefaultTemplate');//adding default template api
Route::post('updateDefaultTemplate','CompanyController@updateDefaultTemplate');//updating default template api
Route::post('activityListForDefaultTemplate','CompanyController@activityListForDefaultTemplate');//list of activity for template
Route::post('defaultActivityListing/{skip}','CompanyController@defaultActivityListing');//default template listing api
Route::post('defaultActivityEdit/{id}','CompanyController@defaultActivityEdit');//default template editing api
Route::post('defaultActivityDelete/{id}','CompanyController@defaultActivityDelete');//default template editing api
Route::post('addDefaultActivity','CompanyController@addDefaultActivity');//adding default template api
Route::post('updateDefaultActivity','CompanyController@updateDefaultActivity');//updating default template api
Route::get('adminHomeCms','PageController@adminHomeCms');
Route::get('adminHeaderCms','PageController@adminHeaderCms');
Route::get('adminTourCms','PageController@adminTourCms');
Route::get('adminFaqCms','PageController@adminFaqCms');
Route::get('adminLoginCms','PageController@adminLoginCms');
Route::get('adminSidebarCms','PageController@adminSidebarCms');
Route::get('adminFooterCms','PageController@adminFooterCms');
Route::get('adminDashboardCms','PageController@adminDashboardCms');
Route::get('adminAddEmployeeCms','PageController@adminAddEmployeeCms');
Route::get('adminManageTemplateCms','PageController@adminManageTemplateCms');
Route::get('adminManageActivityCms','PageController@adminManageActivityCms');
Route::get('adminSettingsCms','PageController@adminSettingsCms');
Route::get('adminBackupCms','PageController@adminBackupCms');
Route::get('adminPricingCms','PageController@adminPricingCms');
Route::get('adminListingCms','PageController@adminListingCms');
Route::get('adminBlogCms','PageController@adminBlogCms');
Route::get('adminTileCms','PageController@adminTileCms');
Route::get('adminReportsFirstCms','PageController@adminReportsFirstCms');
Route::get('adminStatCms','PageController@adminStatCms');
Route::get('adminReportsCms','PageController@adminReportsCms');
Route::get('adminAllowanceCms','PageController@adminAllowanceCms');
Route::get('adminPriceCms','PageController@adminPriceCms');
Route::get('adminContactCms','PageController@adminContactCms');
Route::get('adminEmployerCms','PageController@adminEmployerCms');
Route::get('allEmployeeListing','PageController@allEmployeeListing');
Route::get('adminCountryPriceCms','PageController@adminCountryPriceCms');
Route::post('cmsSend','CompanyController@cmsSend');//cms data
Route::post('cmsSendHome','CompanyController@cmsSendHome');//cms home data
Route::post('cmsSendFooter','CompanyController@cmsSendFooter');//cms footer data
Route::post('cmsSendLogin','CompanyController@cmsSendLogin');//cms login data
Route::post('cmsSendDashboard','CompanyController@cmsSendDashboard');//cms dashboard data
Route::post('cmsSendFaq','CompanyController@cmsSendFaq');//cms faq data
Route::post('cmsSendSidebar','CompanyController@cmsSendSidebar');//cms Sidebar data
Route::post('cmsSendTour','CompanyController@cmsSendTour');//cms Tour data
Route::post('cmsSendAdd','CompanyController@cmsSendAdd');//cms Add Employee data
Route::post('cmsSendTemplate','CompanyController@cmsSendTemplate');//cms Manage Template data
Route::post('cmsSendActivity','CompanyController@cmsSendActivity');//cms Manage Activity data
Route::post('cmsSendSettings','CompanyController@cmsSendSettings');//cms Settings data
Route::post('cmsSendBackup','CompanyController@cmsSendBackup');//cms Backup data
Route::post('cmsSendPricing','CompanyController@cmsSendPricing');//cms Pricing data
Route::post('cmsSendListing','CompanyController@cmsSendListing');//cms Listing data
Route::post('cmsSendBlog','CompanyController@cmsSendBlog');//cms Blog data
Route::post('cmsSendTile','CompanyController@cmsSendTile');//cms Report Tile data
Route::post('cmsSendReportsFirst','CompanyController@cmsSendReportsFirst');//cms Report First Tile data
Route::post('cmsSendStat','CompanyController@cmsSendStat');//cms Stat Report Tile data
Route::post('cmsSendReports','CompanyController@cmsSendReports');//cms Detailed Report Tile data
Route::post('cmsSendAllowance','CompanyController@cmsSendAllowance');//cms Allowance Report Tile data
Route::post('cmsSendPrice','CompanyController@cmsSendPrice');//cms Allowance Report Tile data
Route::post('cmsSendContact','CompanyController@cmsSendContact');//cms Contact data
Route::post('cmsSendEmployer','CompanyController@cmsSendEmployer');//cms Contact data
Route::post('setSetAnnualAllowanceInAdmin','CompanyController@setSetAnnualAllowanceInAdmin');//set annual allowance value
Route::post('updateSetAnnualAllowanceInAdmin','CompanyController@updateSetAnnualAllowanceInAdmin');//update annual allowance value
Route::post('setGeneralSettingInAdmin','CompanyController@setGeneralSettingInAdmin');//set general setting values
Route::post('updateGeneralSettingInAdmin','CompanyController@updateGeneralSettingInAdmin');//update general setting values
Route::post('uploadBlog','CompanyController@uploadBlog');//upload blog contents
Route::get('editBlog/{id}','CompanyController@editBlog');//edit blog contents
Route::post('updateBlog','CompanyController@updateBlog');//update blog contents
Route::get('deleteBlog/{id}','CompanyController@deleteBlog');//delete blog contents
Route::post('addTopic','CompanyController@addTopic');//add topic name
Route::get('deleteTopic/{id}','CompanyController@deleteTopic');//delete topic contents
Route::get('editTopic/{id}','CompanyController@editTopic');//edit topic contents
Route::post('updateFaqTopic','CompanyController@updateFaqTopic');//update topic contents
Route::post('addFaq','CompanyController@addFaq');//add Faq name
Route::get('editFaq/{id}','CompanyController@editFaq');//edit Faq contents
Route::post('updateFaq','CompanyController@updateFaq');//update topic contents
Route::get('deletefaq/{id}','CompanyController@deletefaq');//delete faq contents
Route::get('editPrice/{code}','CompanyController@editPrice');//edit price contents
Route::post('updatePrice','CompanyController@updatePrice');//update price contents
Route::post('extendTrial/{id}','CompanyController@extendTrial');//admin access company extend trail
});

/*Route::get('datatable',function(){
	return view('table');
});*/
/*Route::get('langauge',function(){
	$locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
	echo $locale;
});
*/





Route::post('addLanguage',['as'=>'addLanguage','uses' => 'CompanyController@addLanguage']);
Route::post('addTranslation',['as'=>'addTranslation','uses' => 'CompanyController@addTranslation']);
Route::post('getTranslation/{lang}',['as'=>'getTranslation','uses' => 'CompanyController@getTranslation']);
Route::get('demo',function(){
	return view('demo');
});
Route::get('demo2',function(){
	return view('demo2');
});
Route::get('test','PageController@test');
Route::get('test1','CompanyController@getDomain');
Route::get('test2','PageController@test2');

Route::group(array('prefix' => 'app'),function(){
	Route::get('testAPI',function(){
		return array('status' => 'sss','resposne' => 'test apis');
	});
	Route::post('tokenVerification','EmployeeController@tokenVerification');//token verification api
	Route::post('employeeRegister','EmployeeController@employeeRegister');//employee register api
	Route::post('employeeLogin','EmployeeController@employeeLogin');//employee login api
	Route::post('employeeDetails','EmployeeController@employeeDetails');//employee detail api
	Route::post('editEmployeeDetails','EmployeeController@editEmployeeDetails');//edit employee detail api

	//list of templates 
	Route::post('listOfTemplates','EmployeeController@listOfTemplates');

	//list of activities
	Route::post('listOfActivities','EmployeeController@listOfActivities');

	//add activities
	Route::post('addActivity','EmployeeController@addActivity');
	Route::post('addTemplate','EmployeeController@addTemplate');
	Route::post('addActivityMappedToTemplate','EmployeeController@addActivityMappedToTemplate');

	//set working setting values
	Route::post('workSetSettingValues','EmployeeController@workSetSettingValues');
	//general setting values
	Route::post('generalSettingValues','EmployeeController@generalSettingValues');
	//over handling setting values
	Route::post('overtimeHandlingSettingValues','EmployeeController@overtimeHandlingSettingValues');
	//set annual allowance values
	Route::post('setAnnualAllowanceSettingValues','EmployeeController@setAnnualAllowanceSettingValues');

	//timesheet update api
	Route::post('insertTimesheet','EmployeeController@insertTimesheet');

	//insertTimeSheet for 
	Route::post('insertTimesheetUpdated','EmployeeController@insertTimesheetUpdated');
	//delete timesheet api
	Route::post('deleteTimesheet','EmployeeController@deleteTimesheet');
	//lisitng of timesheet api
	Route::post('listTimesheet','EmployeeController@listTimesheet');
	Route::post('listTimesheetIos','EmployeeController@listTimesheetIos');
	
/*	//timesheet update  with new value api
	Route::post('updateTimesheet','EmployeeController@updateTimesheet');*/
	//insert fcm id for employee
	Route::post('insertFcmId','EmployeeController@insertFcmId');
	Route::get('faqForMobile/{lang}','LanguageController@faqForMobileWithLanguageChange');

	//test api
	Route::post('testApi','EmployeeController@testApi');

	//timesheet restore from mobile 
	Route::post('timesheetRestoreFromMobile','EmployeeController@timesheetRestoreFromMobile');
});


//settings
Route::post('setWorkingData','CompanyController@setWorkingData');//set working values
Route::post('updateWorkingData','CompanyController@updateWorkingData');//updated set working values
Route::post('setGeneralSetting','CompanyController@setGeneralSetting');//set general setting values
Route::post('updateGeneralSetting','CompanyController@updateGeneralSetting');//update general setting values
Route::post('setOvertimeHandling','CompanyController@setOvertimeHandling');//set over handling setting values
Route::post('updateOvertimeHandling','CompanyController@updateOvertimeHandling');//update over handling setting values
Route::post('setSetAnnualAllowance','CompanyController@setSetAnnualAllowance');//set annual allowance value
Route::post('updateSetAnnualAllowance','CompanyController@updateSetAnnualAllowance');//update annual allowance value

//reports
Route::post('listingTimesheetWithRangeForOvertimeReport/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetWithRangeForOvertimeReport');//timesheet values for overtime report
Route::post('listingTimesheetWithRange/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetWithRange');//timesheet values for report
Route::post('listingTimesheetWithRangeForStatistics/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetWithRangeForStatistics');//timesheet values for statistics weekly
Route::post('listingTimesheetForStatisticsMonths/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetForStatisticsMonths');//timesheet values for statistics monthly
Route::post('listingTimesheetForStatisticsYearly/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetForStatisticsYearly');//timesheet values for statistics yearly
Route::post('listingTimesheetWithRangeForGraph/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetWithRangeForGraph');//timesheet values for graph weekly
Route::post('listingTimesheetForGraphMonths/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetForGraphMonths');//timesheet values for graph monthly
Route::post('listingTimesheetForGraphYearly/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetForGraphYearly');//timesheet values for graph yearly
Route::post('listingTimesheetForActivityGraph/{cid}/{eid}/{from}/{to}','CompanyController@listingTimesheetForActivityGraph');//timesheet values for activity graph 
Route::post('listingTimesheetForActivity','CompanyController@listingTimesheetForActivity');//timesheet values for detailed report for array of activity



Route::post('lang/{code}','LanguageController@lang');

//testing api
/*Route::get('add','CompanyController@add');*/
Route::get('changeStatus','CompanyController@changeStatus');

Route::post('addPlans','CompanyController@addPlans');
Route::get('sendNotification/{id}','CompanyController@sendFCMNotification');
Route::get('sendAppleNotification/{id}','CompanyController@sendAppleNotification');

//keerthi api
Route::post('paymentInfo','SubscriptionController@paymentInfo');//update payment details to subscription
Route::get('subscribedemployees',['as' => 'subscribedemployees', 'uses' => 'PageController@subscribedemployees']);//subscribed employees page display 
Route::get('subscribedusers',['as' => 'subscribedusers', 'uses' => 'PageController@subscribedusers']);//subscribed employees page display 
Route::post('subscribedemployeeslist', 'PageController@subscribedemployeeslist');//subscribed employees page display api
//Notification Api
Route::post('myNotification','PageController@myNotification');//list all my notifications
Route::post('makeItRead','PageController@makeItRead');//make all notification read
Route::post('makeClikedRead','PageController@makeClikedRead');//make clicked notification read
Route::post('paymentRecipt','PageController@paymentRecipt');//make all notification read
Route::post('mailNow','PageController@mailNow');//make all notification read
Route::post('checkAuth',function(){
	if(Auth::check()){
      return "true";
    }
    else
    {
      return "false";
    } 
});
Route::get('redirectToLogin',function(){
\Session::flash('message',trans('popup.login_again'));
         return redirect()->route('login');
});
Route::get('activationLogin/{email}/{language}','CompanyController@activationLogin');

Route::get('importExport', 'BackUpController@importExport');
Route::get('downloadToJson', 'BackUpController@downloadToJson');
Route::get('cronTest', 'BackUpController@cronTest');
Route::get('importExcel', 'BackUpController@importExcel');
Route::get('upload', 'BackUpController@upload');
Route::get('import', 'BackUpController@import');
Route::get('companyid', 'BackUpController@companyid');

Route::post('getPreviousDayTemplate','CompanyController@getPreviousDayTemplate');

Route::get('datatable',function(){
	return View::make('datatable');
});

Route::post('myovertime/getuserinfo','PageController@getuserinfo');
 