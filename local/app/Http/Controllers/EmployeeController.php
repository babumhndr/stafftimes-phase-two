<?php

namespace App\Http\Controllers;
use App\Employee;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use View;
use Illuminate\Support\Facades\Auth;
use App\MotTemplates;
use App\CompanyTemplates;
use App\MotActivity;
use App\WorkingDaysSetting;//model
use App\ConditionForWorkingDaysSetting;//model
use App\GeneralSetting;//model
use App\OvertimeHandling;//model
use App\SetAnnualAllowance;//model
use App\ActivitiesMappedToTemplate;//model
use App\Timesheet;
use App\Companies;
use App\FileUpload;
use App\MailSend;
class EmployeeController extends Controller
{
    //invite employee by company
    public static function invitePeople()
    {   
        $company_details= Auth::user();
        $details = array('email' => strtolower(Input::get('email')),
                         'name' => Input::get('name'),
                         'company_id' => $company_details['_id'],
                         'profile_image'=> url('image/dummy.png'),
                         'token' => (string)rand(10000,100000),
                         'invite_verified'=>false,
                         'status'=>'pending',
                         'payment_status'=>'pending',
                         'trail_status'=>'pending',
                         'total_overtime'=>0,
                         'created_on' => Input::get('time'),
                         'updated_on'=> Input::get('time')
            );
            $results=Employee::invitePeople($details);
            return $results;
    }
    //employee token verification
    public static function tokenVerification()
    {
        $details=array('email'=>strtolower(Input::get('email')),
                       'token'=>Input::get('token') 
                       );
        $results=Employee::inviteVerification($details);
        return $results;
    }
    //employee register update
    public static function employeeRegister()
    {
        $details=array(
                       'email'=>Input::get('email'),
                       'password'=>md5(Input::get('password')),
                       'department'=>Input::get('department'),
                       'employee_id'=>Input::get('employee_id'),
                       'phone_number'=>Input::get('phone_number'),
                       'status'=>'active',
                       'company_name'=>'',
                       'fax_number'=>'',
                       'address'=>'',
                       );
        $results=Employee::employeeRegister($details);
        return $results;
    }
    //employee login
    public static function employeeLogin()
    {
        $details=array('email'=>Input::get('email'),
                        'password'=>md5(Input::get('password')),
                        );
        $results=Employee::employeeLogin($details);
        return $results;
    }
    //employee details
    public static function employeeDetails()
    {
        $details = array('authKey' => Input::get('authKey'));
      $authKey = explode("#",base64_decode($details['authKey']));
       $userId = $authKey[0];
        $employeeDetail =Employee::validateUser($userId);
        $count = count($employeeDetail);
        if($count>0)
        {
             return array('status'=> 'success','code'=>200,'response'=>$employeeDetail);
        }
        else
        {
            return array('status'=> 'failure','code'=>300,'response'=>'Details not found'); 
        }
    }
    //edit an employee detail 
    public static function editEmployeeDetails()
    {
        $imagePath=url('assets/images/dummy.png');
        $authKey = Input::get('authKey');
        $userId = explode("#",base64_decode($authKey));
        $userId = $userId[0];
        $res=Employee::where('_id',$userId)->get();
        if (count($res)>0 && $res[0]['profile_image'] != null) {
          $imagePath=$res[0]['profile_image'];
        }
        if (Input::hasFile('profile_image')) 
        {
        $imagePath = FileUpload::uploadEmployeeImage('employee_profile_images');
        }
        $details = array('name' => Input::get('name'),
                          'email' => strtolower(Input::get('email')),
                          'phone_number'=> Input::get('phone_number'),
                          'profile_image'=>$imagePath,
                          'company_name' =>Input::get('company_name'),
                          'fax_number' => Input::get('fax_number'),
                          'address' => Input::get('address'),
                          'department'=>Input::get('department'),
                          'employee_id'=>Input::get('employee_id'),
                          );
       /* return $details;*/
        $employeeDetail =Employee::editEmployeeDetails($authKey,$details);
        return $employeeDetail;
    }
    //active employee listing 
    public function employee_listing_active($skip)
    { 
        $data=Auth::user();
        $res=(int)$skip;
        $employee =Employee::where('status','active')->where('company_id',$data['id'])->orderBy('created_on', 'desc')->skip($res)->take(16)->get();
        return  $employee ;
    }
    //pending employee listing 
    public function employee_listing_pending($skip)
    {   
        $data=Auth::user();
        $res=(int)$skip;
        $employee =Employee::where('status','pending')->where('company_id',$data['id'])->orderBy('created_on', 'desc')->skip($res)->take(16)->get();
        return  $employee ;
    }
    //archive employee listing 
    public function employee_listing_archive($skip)
    {   
        $data=Auth::user();
        $res=(int)$skip;
        $employee =Employee::where('status','archive')->where('company_id',$data['id'])->orderBy('created_on', 'desc')->skip($res)->take(16)->get();
        return  $employee ;
    }

        //active employee listing 
    public function employee_listing_active_sort($skip)
    { 
        $data=Auth::user();
        $res=(int)$skip;
        $employee =Employee::where('status','active')->where('company_id',$data['id'])->orderBy('name', 'asc')->skip($res)->take(16)->get();
        return  $employee ;
    }
    //pending employee listing 
    public function employee_listing_pending_sort($skip)
    {   
        $data=Auth::user();
        $res=(int)$skip;
        $employee =Employee::where('status','pending')->where('company_id',$data['id'])->orderBy('name', 'asc')->skip($res)->take(16)->get();
        return  $employee ;
    }
    //archive employee listing 
    public function employee_listing_archive_sort($skip)
    {   
        $data=Auth::user();
        $res=(int)$skip;
        $employee =Employee::where('status','archive')->where('company_id',$data['id'])->orderBy('name', 'asc')->skip($res)->take(16)->get();
        return  $employee ;
    }
        //active employee listing 
    public function employee_listing_active_search($keyword)
    { 
        $data=Auth::user();
        $search = '%'.$keyword.'%';
        $employee =Employee::where('status','active')->where('company_id',$data['id'])->where('name', 'LIKE', $search)->get();
        return  $employee ;
    }
    //pending employee listing 
    public function employee_listing_pending_search($keyword)
    {   
        $data=Auth::user();
        $search = '%'.$keyword.'%';
        $employee =Employee::where('status','pending')->where('company_id',$data['id'])->where('name', 'LIKE', $search)->get();
        return  $employee ;
    }
    //archive employee listing 
    public function employee_listing_archive_search($keyword)
    {   
        $data=Auth::user();
        $search = '%'.$keyword.'%';
        $employee =Employee::where('status','archive')->where('company_id',$data['id'])->where('name', 'LIKE', $search)->get();
        return  $employee ;
    }

    //listOfTemplates
    public function listOfTemplates(){
        $details = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($details['authKey']));
        $userId = $authKey[0];
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
        if($employeeExist['status'] == 'success'){
          $companyId = $employeeExist['response'][0]['company_id'];
          $superadminDetails = Companies :: where('admin',1)
                      ->get();
          $details = array('user_id' => $userId,
                          'superadmin_id' => $superadminDetails[0]['_id'],
                          'company_id' => $companyId);

          //default MOT tempalte
          $motTemplateList = MotTemplates :: getListOfTemplateForApp($details);  
          if(count($motTemplateList) > 0){
            $response = array('status' => 'success','status_code' => '654','response' => $motTemplateList);
          }
          else{
            $response = array('status' => 'failure','status_code' => '754','response' => 'no template found');
          }
        } 
        else{
          $response = array('status' => 'failure','status_code' => '854','response' => 'not a valid user');
        }
        return $response;

    }

    //listOfActivities
    public function listOfActivities(){
      $details = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($details['authKey']));
        $userId = $authKey[0];
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
        if($employeeExist['status'] == 'success'){
          $companyId = $employeeExist['response'][0]['company_id'];
          $superadminDetails = Companies :: where('admin',1)
                      ->get();
          $details = array('user_id' => $userId,
                          'superadmin_id' => $superadminDetails[0]['_id'],
                          'company_id' => $companyId);
          //default MOT activities
          $motActivityList = MotActivity :: getListOfActivityForApp($details);
          
          if(count($motActivityList) > 0){
            $response = array('status' => 'success','status_code' => '229','response' => $motActivityList);
          }
          else{
            $response = array('status' => 'failure','status_code' => '329','response' => 'no activity found');
          }
          
        } 
        else{
          $response = array('status' => 'failure','status_code' => '420','response' => 'not a valid user');
        }
        return $response;
    }

    //addActivity
    public function addActivity(){
      $details = array('authKey' => Input::get('authKey'),
                        'activity_name' => Input::get('activity_name'),
                        'against_offset' => Input::get('against_offset'),
                        'flat_break_deduction'=> Input::get('flat_break_deduction'),
                        'own_account'=> Input::get('own_account'),
                        'amount' => Input::get('amount'),
                        'amount_type'=> Input::get('amount_type'),
                        'amount_value'=> Input::get('amount_value'),
                        'activity_hours' =>Input::get('activity_hour'),
                        'activity_days' => Input::get('activity_day'),
                        'hourly_rate_multiplier' => Input::get('hourly_rate_multiplier'),
                        'flat_time_mode' =>   Input::get('flat_time_mode'),
                        'pre_populated_with' => Input::get('pre_populated_with'),
                        'pre_populated_value' => Input::get('pre_populated_value'),
                        'overtime_reducer' => Input::get('overtime_reducer'),
                        'created_on'=> Input::get('created_on'),
                        'created_by' => 'user');
      
      $authKey = explode("#",base64_decode($details['authKey']));
      $userId = $authKey[0];
      //validate the empolyee
      $details['created_user_id'] = $userId;
      $employeeExist = Employee :: validateEmployee($userId);
      if($employeeExist['status'] == 'success'){
          $response = MotActivity :: addActivity($details);
      }
      else{
          $response = array('status' => 'failure','status_code' => '420','response' => 'not a valid user');
      }
      return $response;
    }

    //addTempalte
    public function addTemplate(){
      $details = array('authKey' => Input::get('authKey'),
                        'template_name' => Input::get('template_name'),
                        'template_offset_time' => Input::get('template_offset_time'),
                        'created_on' => Input::get('created_on'),
                        'created_by' => 'user');
      
      $authKey = explode("#",base64_decode($details['authKey']));
      $userId = $authKey[0];
      //validate the empolyee
      $details['created_user_id'] = $userId;
      $employeeExist = Employee :: validateEmployee($userId);
      if($employeeExist['status'] == 'success'){
        $details['company_id'] = $employeeExist['response'][0]['company_id'];;
        $response = MotTemplates::addTemplateByApp($details);
      }
      else{
          $response = array('status' => 'failure','status_code' => '420','response' => 'not a valid user');
      }
      return $response;
    }

    public function addActivityMappedToTemplate()
    {
       $activityDetails = array('activity_id' => Input::get('activity_id'),
                                'activity_flat_hours' =>Input::get('activity_flat_hours'),
                                'activity_amount' => Input::get('activity_amount'),
                                'activity_start' =>  Input::get('activity_start'),
                                'activity_end' =>  Input::get('activity_end'),
                                'activity_break' =>Input::get('activity_break'),
                                'template_id' => Input::get('template_id')
                                );
       $result = ActivitiesMappedToTemplate :: addActivityMappedToTempalte($activityDetails);
       $response = array('status' => 'success','status_code' => '585','response' => 'Acitivty mapped to tempalte added');
       return $response;
    }

    //workset setting value
    public function workSetSettingValues()
    {
      $details = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($details['authKey']));
        /*return  $authKey;*/
        $userId = $authKey[0];
     /*   return $userId;*/
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
        $companyId='';
        if($employeeExist['status'] == 'success')
        {
          $companyId = $employeeExist['response'][0]['company_id'];
          /*return $companyId;*/
          $working_days_settings=WorkingDaysSetting::workSetSettingValues($companyId);
        return $working_days_settings;
        }
        else
        {
           return array('status' => 'failure','status_code' => '650', 'response'=>'User detail not valid');
        }
        
    }
    //general setting values
    public function generalSettingValues()
    {
      $details = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($details['authKey']));
        $userId = $authKey[0];
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
         $companyId='';
        if($employeeExist['status'] == 'success')
        {
          $companyId = $employeeExist['response'][0]['company_id'];
          /*return $companyId;*/
           $general_setting_details=GeneralSetting::generalSettingValues($companyId);
        return $general_setting_details;
        }
        else
        {
           return array('status' => 'failure','status_code' => '347', 'response'=>'User detail not valid');
        }
       
    }
    //over handling setting values
   public function overtimeHandlingSettingValues()
    {
      $details = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($details['authKey']));
        $userId = $authKey[0];
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
          $companyId='';
        if($employeeExist['status'] == 'success')
        {
          $companyId = $employeeExist['response'][0]['company_id'];
          /*return $companyId;*/
          $overtime_handling_setting_details=OvertimeHandling::overtimeHandlingSettingValues($companyId);
        return $overtime_handling_setting_details;
        }
        else
        {
           return array('status' => 'failure','status_code' => '192', 'response'=>'User detail not valid');
        }
    }
    //set annual allowance setting values
   public function setAnnualAllowanceSettingValues()
    {
      $details = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($details['authKey']));
        $userId = $authKey[0];
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
          $companyId ='';
        if($employeeExist['status'] == 'success')
        {
          $companyId = $employeeExist['response'][0]['company_id'];
          /*return $companyId;*/
          $set_annual_allowance_setting_details=SetAnnualAllowance::setAnnualAllowanceSettingValues($companyId);
        return $set_annual_allowance_setting_details;
        }
        else
        {
           return array('status' => 'failure','status_code' => '152', 'response'=>'User detail not valid');
        }
        
    }

    //update timesheet
     public function insertTimesheet()
    {
      $user = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($user['authKey']));
        $userId = $authKey[0];
        $companyId='';
        $monthFirstDate=Input::get('monthFirstDate');
          $monthLastDate=Input::get('monthLastDate');
       /* return $userId;*/
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId); 
        if($employeeExist['status'] == 'success')
        {
          $companyId = $employeeExist['response'][0]['company_id'];
          $activity=Input::get('activity');
          $getActivityDetails=MotActivity::where('_id',$activity)->get();
          $activityDetails='';
          $activityName='';
          if (count($getActivityDetails)>0) {
           $activityDetails=json_decode($getActivityDetails[0], true); 
           $activityName=$activityDetails['activity_name'];
          }
          else
          {
             return array('status' => 'failure','status_code' => '239', 'response'=>'Invaild activity id');
          }
          /*return $companyId;*/
                $details=array('employee_id'=>$userId,
                    'company_id'=>$companyId,
                    'date'=>Input::get('date'),
                    'day_name'=>Input::get('day_name'),
                    'offset'=>Input::get('offset'),
                    'activity'=>$activity,
                    'activity_detail'=>$activityDetails,
                    'activity_name'=>$activityName,
                    'start_time'=>Input::get('start_time'),
                    'end_time'=>Input::get('end_time'),
                    'break_time'=>Input::get('break_time'),
                    'total_hours'=>Input::get('total_hours'),
                    'total_hours1'=>Input::get('total_hours1'),
                    'day_balance'=>Input::get('day_balance'),
                    'created_on'=>Input::get('created_on'),
                    'schedule_id'=>Input::get('schedule_id'),
                    'condition1'=>Input::get('condition1'),
                    'condition1_from'=>Input::get('condition1_from'),
                    'condition1_factor'=>Input::get('condition1_factor'),
                    'condition2'=>Input::get('condition2'),
                    'condition2_from'=>Input::get('condition2_from'),
                    'condition2_factor'=>Input::get('condition2_factor'),
                    'bonus_hours'=>Input::get('bonus_hours'),
                    'apply_bonus'=>Input::get('apply_bonus'),
                    'rate'=>Input::get('rate'),
                    'own_account'=>Input::get('own_account'),
                    'amount'=>Input::get('amount'),
                    'break_min'=>Input::get('break_min'),
                    'total_hours_min'=>Input::get('total_hours_min'),
                    'total_hours_min1'=>Input::get('total_hours_min1'),
                    'balance_min'=>Input::get('balance_min'),
                    'offset_min'=>Input::get('offset_min'),
                    'notes'=>Input::get('notes'),
                    'input_type'=>Input::get('input_type'),
                    'timesheet_id'=>Input::get('timesheet_id'),
                    'updated_time'=>Input::get('updated_time'),
                    'TSHrsAccounted'=>Input::get('TSHrsAccounted'),
                    'TSBonusHrs'=>Input::get('TSBonusHrs'),
                    'TSHrsTotal'=>Input::get('TSHrsTotal'),
                    'TSDailyBalnce'=>Input::get('TSDailyBalnce'),
                    'TimeSheetSchedule'=>Input::get('TimeSheetSchedule')
                     );         
      $result=Timesheet::insertTimesheet($details,$monthFirstDate,$monthLastDate);
      return $result;
        }
        else
        {
           return array('status' => 'failure','status_code' => '139', 'response'=>'User detail not valid');
        }
    }

    //insertTimesheetUpdated
    public function insertTimesheetUpdated(Request $request){
      dd(json_decode($request->getContent(), true));
      //return $details = array('data' => Input::get('data'));
      //return json_decode($details);
    }

    //delete time sheet
    public function deleteTimesheet()
    {
      $timesheet_id=Input::get('timesheet_id');
      $user = array('authKey' => Input::get('auth_key'));
      $authKey = explode("#",base64_decode($user['authKey']));
      $userId = $authKey[0];
      $totalOvertimeOfEmployee=array('total_overtime'=>Input::get('total_overtime'));
      $addOvertimeToEmployee=Employee::where('_id',$userId)->update($totalOvertimeOfEmployee);
      $updateTS=array('TSHrsAccounted'=>Input::get('TSHrsAccounted'),
                    'TSBonusHrs'=>Input::get('TSBonusHrs'),
                    'TSHrsTotal'=>Input::get('TSHrsTotal'),
                    'TSDailyBalnce'=>Input::get('TSDailyBalnce')
                    );
      $date=Input::get('date');
      $getTimesheet=Timesheet::where('_id',$timesheet_id)->get();
      if (count($getTimesheet)>0) {
        $updateTsFinal=Timesheet::where('date',$date)->where('employee_id',$userId)->update($updateTS);
        $result=Timesheet::deleteTimesheet($timesheet_id);
        return $result;
      }
      else
      {
         $response = array('status' => 'failure','status_code' => '902', 'delete_manually'=>true);
         return $response;
      }
      
    }

    //listing time sheet
    public function listTimesheet()
    {
      $user = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($user['authKey']));
        $userId = $authKey[0];
        $companyId='';
       /* return $userId;*/
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
        if($employeeExist['status'] == 'success')
        {
          $companyId = $employeeExist['response'][0]['company_id'];
          /*return $companyId;*/
                $details=array('employee_id'=>$userId,
                     'company_id'=>$companyId,
                     );         
      $result=Timesheet::listTimesheet($details);
      return $result;
        }
        else
        {
           return array('status' => 'failure','status_code' => '459', 'response'=>'User detail not valid');
        }
    }
    //listing time sheet
    public function listTimesheetIos()
    {
      $user = array('authKey' => Input::get('authKey'));
        $authKey = explode("#",base64_decode($user['authKey']));
        $userId = $authKey[0];
        $companyId='';
       /* return $userId;*/
        //validate the empolyee
        $employeeExist = Employee :: validateEmployee($userId);
        if($employeeExist['status'] == 'success')
        {
          $companyId = $employeeExist['response'][0]['company_id'];
          
                $details=array('employee_id'=>$userId,
                     'company_id'=>$companyId,
                     );         
          $result=Timesheet::listTimesheetIos($details);
          return $result;
        }
        else
        {
           return array('status' => 'failure','status_code' => '459', 'response'=>'User detail not valid');
        }
    }
/*    public function updateTimesheet()
    {
                $timesheet_id=Input::get('timesheet_id');
                $details=array('total_overtime'=>Input::get('total_overtime'),
                     'rate'=>Input::get('rate'),
                     'own_account'=>Input::get('own_account'),
                     'amount'=>Input::get('amount'),
                     'break_min'=>Input::get('break_min'),
                     'total_hours_min'=>Input::get('total_hours_min'),
                     'balance_min'=>Input::get('balance_min'),
                     'offset_min'=>Input::get('offset_min'),
                     'notes'=>Input::get('notes'),
                     );         
      $result=Timesheet::updateTimesheet($details,$timesheet_id);
      return $result;
    }*/

    //insert Fcm id 
    public function insertFcmId()
    {
      $details = array('authKey' => Input::get('authKey'));
      $authKey = explode("#",base64_decode($details['authKey']));
      $userId = $authKey[0];
      $fcmId=Input::get('fcm_id');
      $results=Employee::insertFcmId($userId,$fcmId);
      return $results;
    }

    public function testApi()
    {
      $results=Employee::insert(array("babin"=>"dsdfd"));
      $mail=MailSend::testApi();
      return "success";
    }
    public function timesheetRestoreFromMobile()
    {
      $details = array('authKey' => Input::get('authKey'));
      $authKey = explode("#",base64_decode($details['authKey']));
      $id = $authKey[0];
      $newlyUpdatedTimesheet=Timesheet::where('employee_id',$id)->orderBy('updated_time','desc')->first(['updated_time']);
      if (count($newlyUpdatedTimesheet)>0)
        {
             return array('status'=> 'success','code'=>267,'response'=>$newlyUpdatedTimesheet);
        }
        else
        {
            return array('status'=> 'failure','code'=>302,'response'=>'Details not found'); 
        }
    }
}

