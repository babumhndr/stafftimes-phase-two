<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;
use App\Lang;

class LanguageController extends Controller
{
    public function switchLang($lang)
    {
        session()->put('UserLanguage', $lang);
        if (array_key_exists($lang, Config::get('languages'))) {
            Session::set('applocale', $lang);
        }
        return Redirect::back();
       /* return 'jdj';*/
    }
    public function faqForMobileWithLanguageChange($lang)
    {
        session()->put('UserLanguage', $lang);
        if (array_key_exists($lang, Config::get('languages'))) {
            Session::set('applocale', $lang);
            App::setLocale($lang);
        }
        /*return App::getLocale();*/
        return redirect()->route('faqForMobileView');
       /* return 'jdj';*/
    }

    public function lang($code)
    {
    	/*return $code;*/
    	$result=Lang::all();
    	return $result['0'][$code];
    }
}
