<?php

namespace App\Http\Controllers;
use App\Companies; //including model.
use App\FileUpload;//including model.
use App\CompanyTemplates;//including model.
use App\CompanyActivity;//including model.
use App\MotTemplates;//including model.
use App\Employee;//including model.
use App\MotActivity;//including model.
use App\Language;//including model.
use App\Translation;//including model.
use App\WorkingDaysSetting;//model
use App\ConditionForWorkingDaysSetting;//model
use App\GeneralSetting;//model
use App\OvertimeHandling;//model
use App\SetAnnualAllowance;//model
use App\ActivitiesMappedToTemplate;//model
use App\Timesheet;//model
use App\MotAdminActivity;
use App\WebNotifications;
use App\MotAdminTemplates;
use App\AdminActivitiesMappedToTemplate;
use App\PlanAndPricing;
use App\Notification;
use App\BlogListing;//including model.
use App\FaqTitle;//including model.
use App\Faq;//including model.
/*use App\SubscriptionDetail;*/
use App\Http\Requests;
use App\MailSend;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
/*use Illuminate\Support\Facades\Request;*/
use View;
use Illuminate\Http\Request;
/*use Illuminate\Support\Facades\Auth;*/
use Auth;
Use App\User;
use Hash;
use Session;
use Cache;
use DateTime;
use DateInterval;
use DatePeriod;
use App\LanguageCode;
use App\Country;
class CompanyController extends Controller
{
  //for admin login
  public function adminLogin()
  {
    $userdata = array('email' => Input::get('email'),
                'password'  =>Input::get('password'));
    $isAuth = Auth::attempt($userdata, true);
    $res=Auth::user();
    if($isAuth)
    {
      $isAdmin=$res['admin'];
      if($isAdmin)
        { 
          //on success
          return redirect('admin_dashboard');
        }
      else
        {
          //on failure
         \Session::flash('message','Your are not a admin');
         return redirect()->to('admin_login');
        }
    } 
    else 
    {     
      //on failure   
      \Session::flash('message','Enter the correct login details!');
      return redirect()->to('admin_login');
    } 
  }
  //for registration of company
  public function companyRegister()
  {
    $details = array('company_name' => Input::get('name'),
              'email' => strtolower(Input::get('email')),
              'password' => Hash::make(Input::get('password')),
              'emailverified'=>false,
              'profile_image'=> '',
              'created_on' => Input::get('time'),
              'updated_on'=> Input::get('time'),
              'country'=>Input::get('country'),
              'language'=>Input::get('language'),
              'token' => str_random(30),
              'first_login'=>false,
              'admin' => 0,
              'status'=> "active",
              'trail_status'=>"pending",
              'payment_status'=>"pending",
              );
    //insert data to the collection
    $results=Companies::companyRegister($details);
   return $results;
  }

  //get trial days
  public static function getTrialDays()
  {
    $user=Auth::user();
    $date1 = $user['trail_start_date'];
     $date2 = date('Y-m-d');
    $datetime1 = new DateTime($date1);
    $datetime2 = new DateTime($date2);
    $datetime1= $datetime1->format('Y-m-d');
    $datetime2= $datetime2->format('Y-m-d');
    $datetime1 = new DateTime($datetime1);
    $datetime2 = new DateTime($datetime2);
    $interval = $datetime1->diff($datetime2);
    $days=$interval->format('%a');
    $returnDetails=array('days'=>$days,
                          'payment_status'=>$user['payment_status']);
    return $returnDetails;
   /* return $datetime1 .'/'.$datetime2 ;*/
  }
  //contact us mail
  public function sendContactUs()
  {
    $phoneCode=Input::get('phone-code');
    $phoneNumber=Input::get('phone-number');
    $phone=$phoneCode.'-'.$phoneNumber;
    $details = array('purpose' => Input::get('purpose'),
              'phone'=>$phone,
              'name'=>Input::get('name'),
              'email'=>Input::get('email'),
              'message'=>Input::get('message'),
              );
    /*return $details;*/
    $sendMailTo=MailSend::sendContactUsMail($details);
    if ($sendMailTo) 
     {
      return array('status'=> 'failure','response'=>'Error occured, Please try again');
    }
    else
    {
       return array('status'=> 'success','response'=>'Mail sent');
    }
  }
   //newsletter mail
  public function newsletterSend()
  {
    $mail=Input::get('mail');
    $details = array('mail'=>$mail,);        
    $sendMailTo=MailSend::newsletterMail($details);
    if ($sendMailTo) 
     {
      return array('status'=> 'failure','response'=>'Error occured, Please try again');
    }
    else
    {
       return array('status'=> 'success','response'=>'Mail sent');
    }
  }
  //for login of company
  public function companyLogin()
  {
    $userdata = array('email' => strtolower(Input::get('email')),
                'password'  =>Input::get('password'),
                'admin' => 0,
                'status'=>'active');
    $isAuth = Auth::attempt($userdata, true);
    if($isAuth)
    {
      $res=Auth::user();
      if($res['emailverified']==true)
        { 
          //on success
          return redirect()->route('dashboard');
        }
      else
        {
          //on failure
         \Session::flash('message',trans('popup.login_not_active_message'));
         return redirect()->route('login');
        }
    } 
    else 
    {     
      //on failure   
      \Session::flash('message',trans('popup.invalid'));
      return redirect()->route('login');
    } 
  }

  //for logout of company
  public function logout() 
  {
    Auth::logout();
    Session::flush();
    Cache::flush();
    return redirect(\URL::previous());
    /*return Redirect::route('login');*/
       /* Auth::logout(); // logout user
        return redirect(\URL::previous());*/
  }

  //for meta refresh or for inactive
   public function logout1() 
  {
    Auth::logout();
    Session::flush();
    Cache::flush();
    \Session::flash('message',trans('popup.logged_out'));
    return redirect()->route('login');
    /*return redirect(\URL::previous());*/
  }
  public function admin_logout()
  {
    Auth::logout();
    Session::flush();
    return Redirect('admin_login');  
  }
  public static function emailVerification($token,$email,$language)
  {
    return View::make('email_verification', array('token' => base64_decode($token),
                    'email' => base64_decode($email),
                    'language' => base64_decode($language)));
  }
  //for email verification of company
  public static function emailVerified()
  {
    $email=$_POST['email'];
    $details = array('email' =>$email,
               'emailverified'=>true,
               'start_date'=>$_POST['start_date'],
               'end_date'=>$_POST['end_date']);
    $results=Companies::emailVerification($details);
    /*return $results;*/
    if ($results) {
      return "success";
    }
  }
  //for update of company information
  public static function companyUpdate()
  {
    $companycountry=Input::get('companycountry');
    $companylangugage=Input::get('companylangugage');
    if($companycountry=='')
    {
      $companycountry="--";
    }
    if($companylangugage=='')
    {
      $companylangugage="--";
    }
    $countryName=Country::where('code',$companycountry)->get();
          $countryNameFinal=$countryName[0]['name'];
          $languageName=LanguageCode::where('code',$companylangugage)->get();
          $languageNameFinal=$languageName[0]['name'];
    $details = array('company_name' => Input::get('companyname'),
              'email' => strtolower(Input::get('companyemail')),
              'company_id' => Input::get('companyid'),
              'phone' => Input::get('companyphone'),
              'fax'=> Input::get('companyfax'),
              'country'=> $companycountry,
              'language'=>$companylangugage ,
              'country_name'=>$countryNameFinal,
              'language_name'=>$languageNameFinal,
              'updated_on'=> Input::get('time'),
              'skype'=> Input::get('companyskype'),
              );
   /* return $details;*/
    $results=Companies::companyUpdate($details);
      return $results;
  }
  //for change company password
  public static function passwordChange()
  { 
    $user=Auth::user();
    $userpassword=$user['password'];
    $useremail=$user['email'];
    $old_password=Input::get('old_password');
    $new_password=array('password' => Hash::make(Input::get('new_password')));
    if(Hash::check( $old_password,$user['password']))
    {
      $results=Companies::passwordChange($useremail,$new_password);
      return $results;
    }
     return array('status'=> 'failure','response'=>'Password is invaild');
  }

  //Archive a employee
  public static function employeeArchive()
  {
    $employee_id=Input::get('id');
    $details=array('status'=>'archive');
    $result=Employee::where('_id',$employee_id)->update($details);
    if($result)
    {
      $response=array('status'=>'success');
    }
    else
    {
      $response=array('status'=>'failure');
    }
    return $response;
  }

    //Archive a employee
  public static function employeeUnArchive()
  {
    $employee_id=Input::get('id');
    $details=array('status'=>'active');
    $result=Employee::where('_id',$employee_id)->update($details);
    if($result)
    {
      $response=array('status'=>'success');
    }
    else
    {
      $response=array('status'=>'failure');
    }
    return $response;
  }

//Delete a employee
  public static function deleteEmployee()
  {
    $employee_id=Input::get('id');
    $result=Employee::where('_id',$employee_id)->delete();
    if($result)
    {
      $response=array('status'=>'success');
    }
    else
    {
      $response=array('status'=>'failure');
    }
    return $response;
  }
  //for update of company profileimage
  public static function uploadImage()
  {
    $res=Auth::user();
  if (Input::hasFile('image')) 
      {
       $imagePath = FileUpload::upload('profile_images');
       $details=array('profile_image'=>$imagePath,
                      'email'=>$res['email']);
       $results=Companies::upload($details);
       return $results;
      }
  else{
        return 'no';
      }
  }
   //for adding template
  public static function addCompanyTemplate()
  {
   $company=Auth::user();
   /*$details=Input::get('number_of_activity');
    return $details;*/
    //replaceDotToColon
    $offsetVal=FileUpload::replaceDotToColon(Input::get('template_offset_time'));
    $details = array(
              'company_id' =>  $company['-id'],
              'template_name' => Input::get('template_name'),
              'template_offset_time' => $offsetVal,
              'number_of_activity' => Input::get('number_of_activity'),
              'deploy_status'=> 'pending',
              // 'template_activity' => Input::get('template_activity'),
              // 'template_flat_hours'=>Input::get('template_flat_hours'),
              // 'template_amount'=> Input::get('template_amount'),
              // 'template_start' => Input::get('template_start'),
              // 'template_end'=> Input::get('template_end'),
              // 'template_break' => Input::get('template_break'),
              'created_on'=> Input::get('time'),
              'created_by' => 'company',
              'created_user_id' => $company['-id']
              );

    /*return $details;*/

     $results=MotTemplates::addTemplate($details);
   return $results;
  }
  //for lisitng template
  public static function company_template_listing($skip)
  { 
      $data=Auth::user();
      $res=(int)$skip;
      $template =CompanyTemplates::where('company_id',$data['id'])->orderBy('created_on', 'desc')->skip($res)->take(16)->get();
      return  $template;
  }
  //for editing template
  public static function company_template_edit($id)
  {/*
    $template_details=MotTemplates::where('_id',$id)->get();
    return $template_details;*/
    $userDetails = Auth::user();
    $templateList = MotTemplates::getListOfTemplates($userDetails['_id'],$id);
    return $templateList;
    /* return sizeof($templateList['0']['activity']);*/
  }
  //mot template
  public static function mot_template($id)
  {
    $templateList = MotTemplates::getListOfMotTemplates($id);
    return $templateList;
  }
  //mot activity
  public static function mot_activity($id)
  {
    $activityList = MotActivity::getListOfMotActivitys($id);
    return $activityList;
  }
    //mot template
  public static function mot_admin_template($id)
  {
    $templateList = MotAdminTemplates::getListOfMotTemplates($id);
    return $templateList;
  }
  //mot activity
  public static function mot_admin_activity($id)
  {
    $activityList = MotAdminActivity::getListOfMotActivitys($id);
    return $activityList;
  }
   public static function mot_admin_template_edit($id)
  {/*
    $template_details=MotTemplates::where('_id',$id)->get();
    return $template_details;*/
    $userDetails = Auth::user();
    $templateList = MotAdminTemplates::getListOfTemplates($userDetails['_id'],$id);
    return $templateList;
    /* return sizeof($templateList['0']['activity']);*/
  }
  //for deleting template
  public static function company_template_delete($id)
  {
    $template_details=MotTemplates::where('_id',$id)->delete();
    $results1=ActivitiesMappedToTemplate::where('template_id',$id)->delete();
    /*if ($template_details==1) {
      $responseToSend=array('template_deleted'=>$id,
                            'message'=>'template_deleted');
      Employee::sendNotificationByFcmId($responseToSend);
    }*/
    return $template_details;
  }
  //change status deploy
  public static function changeStatusDeploy()
  {
    $id=Input::get('id');
    $status=array('deploy_status' =>"deployed");
    $changeStatus=MotTemplates::changeStatusDeploy($id,$status);
    return $changeStatus;
  }

//change status deploy
  public static function changeStatusDeployActivity()
  {
    $id=Input::get('id');
    $status=array('deploy_status' =>"deployed");
    $changeStatus=MotActivity::changeStatusDeployActivity($id,$status);
    return $changeStatus;
  }

  //change priority
  public static function changePriority()
  {
    $id=Input::get('id');
    $companyId = Input::get('companyId');
    $priorityNumber=(int)Input::get('priority_number');
    $companyActivityList = MotActivity::where('company_id',$companyId)->orderBy('priority_number', 'asc')->get();
    $iteration = 0 ;
    $changeStatus = null;
    for ($i=0; $i <sizeof($companyActivityList) ; $i++) { 
      if ($companyActivityList[$i]['_id']!= $id) {
        $iteration=$iteration+1;
        if ($iteration==$priorityNumber) {
           $iteration=$iteration+1;
        }
        $data=array('priority_number'=>$iteration);
        $changeStatus = MotActivity::where('_id',$companyActivityList[$i]['_id'])->update($data);
      }else{
        $data=array('priority_number'=>$priorityNumber);
        $changeStatus = MotActivity::where('_id',$companyActivityList[$i]['_id'])->update($data);
      }
    }
   if ($changeStatus) {
      return array('status'=>'success');
    }else{
       return array('status'=>'failure');
    }
  }

  //makePriorityZero
  public static function makePriorityZero(){
    $company = Companies::all();
    for ($i=0; $i < sizeof($company) ; $i++) { 
      $activity = MotActivity :: where('created_user_id',$company[$i]['_id'])->orderBy('activity_name', 'asc')->get();  
      if (sizeof($activity)>0) {
        for ($j=0; $j < sizeof($activity) ; $j++) { 
          $data=array('priority_number'=>$j+1);
          $priority = MotActivity::where('_id',$activity[$j]['_id'])->update($data);
        }
      }
    }
    return 'yes';
  }

  //makePriorityZeroAdmin
  public static function makePriorityZeroAdmin(){
      $activity = MotAdminActivity::orderBy('activity_name', 'asc')->get(); 
      if (sizeof($activity)>0) {
        for ($j=0; $j < sizeof($activity) ; $j++) { 
          $data=array('priority_number'=>$j+1);
          $priority = MotAdminActivity::where('_id',$activity[$j]['_id'])->update($data);
        }
      }
    return 'yes';
  }

  //change status deploy
  public static function changeStatusDeployForSuperAdmin()
  {
    $id=Input::get('id');
  $admin=Auth::user();
    $status=array('deploy_status' =>"deployed");
    $companyList = Companies :: where('status','active')->get();
  $details=array('notification_type' => "template_deployed",'notification_content' => "deployed new template.",'content_landing_id' => $id,'reciever_id' => null,'sender_id' => $admin['_id'],'notification_for' => "individual",'deployed_time' => Input::get('deployed_time'));
    $addNotification=WebNotifications::addIntoNotification($details,$companyList);
  $changeStatus=MotAdminTemplates::changeStatusDeploy($id,$status);
    return $changeStatus;
  }

//change status deploy
  public static function changeStatusDeployActivityForSuperAdmin()
  {
    $id=Input::get('id');
  $admin=Auth::user();
    $status=array('deploy_status' =>"deployed");
    $companyList = Companies :: where('status','active')->get();
  $details=array('notification_type' => "activity_deployed",'notification_content' => "deployed new activity.",'content_landing_id' => $id,'reciever_id' => null,'sender_id' => $admin['_id'],'notification_for' => "individual",'deployed_time' => Input::get('deployed_time'));
    $addNotification=WebNotifications::addIntoNotification($details,$companyList);
    $changeStatus=MotAdminActivity::changeStatusDeployActivity($id,$status);
    return $changeStatus;
  }

  //for updating template
  public static function updateCompanyTemplate()
  {
     $company=Auth::user();
   /*$details=Input::get('number_of_activity');
    return $details;*/
    $template_id=Input::get('template_id');
    $number_of_activity= Input::get('number_of_activity');
     $offsetVal=FileUpload::replaceDotToColon(Input::get('template_offset_time'));
   /* $results2=MotTemplates::where('_id',$template_id)->delete();*/
    /*$results1=ActivitiesMappedToTemplate::where('template_id',$template_id)->delete();*/
  /*  return $template_id;*/
    $details = array(
              'company_id' =>  $company['-id'],
              'template_name' => Input::get('template_name'),
              'template_offset_time' => $offsetVal,
              // 'template_activity' => Input::get('template_activity'),
              // 'template_flat_hours'=>Input::get('template_flat_hours'),
              // 'template_amount'=> Input::get('template_amount'),
              // 'template_start' => Input::get('template_start'),
              // 'template_end'=> Input::get('template_end'),
              // 'template_break' => Input::get('template_break'),
              'created_on'=> Input::get('time'),
              'created_by' => 'company',
              'created_user_id' => $company['-id']
              );
     $results=MotTemplates::updateTemplate($details,$template_id,$number_of_activity);
   return $results;
   /* $company=Auth::user();
    $details = array(
              'company_id' =>  $company['-id'],
              'template_name' => Input::get('template_name1'),
              'template_offset_time' => Input::get('template_offset_time1'),
              'template_activity_id' => Input::get('template_activity1'),
              'template_flat_hours'=>Input::get('template_flat_hours1'),
              'template_amount'=> Input::get('template_amount1'),
              'template_start' => Input::get('template_start1'),
              'template_end'=> Input::get('template_end1'),
              'template_break' => Input::get('template_break1'),
              );
    $template_id = Input::get('template_id');
    $results=MotTemplates::updateCompanyTemplate($details,$template_id);
    return $results;*/
  }
  //activity list for template
  public static function activityListForTemplate($id)
  {
   /* return 'dhdhfd';*/
   $activity_for_template=CompanyActivity::activityListForTemplate($id);
   return $activity_for_template;
  }
  //for listing mot_template
  public static function mot_template_listing($skip)
  {
      /*$data=Auth::user();*/
     /* return $skip;*/
      $res=(int)$skip;
      $template =MotTemplates::orderBy('created_on', 'desc')->skip($res)->take(16)->get();
      return  $template;
  }
  //for popup msg for first visiting of admin
  public static function firstVisit()
  {
      $res =Auth::user();
      $user_email=$res['email'];
      $first_login=$res['first_login'];
      if ($first_login==false) 
      {
        $res=Companies::firstVisit($user_email);
        return $res;
      }
  }
  //for forget password
  public function forgotPassword()
  {
    $email=strtolower(Input::get('email'));
    /*return $email;*/
    $res=Companies::forgotPassword($email);
    return $res;
  }
  //for reseting of password
  public function resetPassword($email)
  {
    $emailid=base64_decode($email);
    $res=Companies::checkPassword($emailid);
    $data=$res[0]['email'];

    return View('reset_password')->with('data',$data);
  }
  // for updating of password
  public function updatePassword()
  {
    $email=strtolower(Input::get('email'));
    /*return $email;*/
    $password=Hash::make(Input::get('password'));
/*    return $password;*/
    $res=Companies::updatePassword($email,$password);
    return $res;
  }
  //for adding of activity
  public static function addCompanyActivity()
  {  
    $flat_break_deduction=true;
    $own_account=true;
    $flat_time_mode=true;
    $overtime_reducer=true;
    $amount = true;
    $amounttype=Input::get('amount_type');
    $amount_value=Input::get('amount_value');
    $pre_populated_with=Input::get('pre_populated_with');
    $pre_populated_value=Input::get('pre_populated_value');
    $allowance_bank_hours=Input::get('activity_hour');
    if ($allowance_bank_hours=="0") {
      $allowance_bank_hours="00:00";
    }
    if(Input::get('flat_break_deduction')== null) 
    {
      $flat_break_deduction=false;
    }
    if(Input::get('own_account')== null)
    {
      $own_account=false;
    } 
    if(Input::get('amount') == null){
      $amount = false;
      $amounttype = '--';
      $amount_value='00.00';
    }
    if(Input::get('flat_time_mode') == null)
    {
      $flat_time_mode=false;
      $pre_populated_with=null;
      $pre_populated_value=null;
      $overtime_reducer=null;
    }
    if(Input::get('overtime_reducer')==null)
    {
       $overtime_reducer =false;
    }
    if (Input::get('hourly_rate_multiplier')==null) {
      $hourly_rate_multiplier='1.000';
    }
    else
    {
      $hourly_rate_multiplier=FileUpload::replaceDotToComma(Input::get('hourly_rate_multiplier'));
    }
    $company=Auth::user();
    $priorityNumber = 0;
    $companyActivityList = MotActivity :: where('company_id',$company['-id'])->orderBy('priority_number', 'desc')->get();
    if (sizeof($companyActivityList)) {
      $priorityNumber = (int)$companyActivityList[0]['priority_number'];
      $priorityNumber = $priorityNumber+1;
    }
    $details = array(
                    'company_id' =>  $company['-id'],
                    'activity_name' => Input::get('activity_name'),
                    'activity_color' => Input::get('activity_color'),
                   /* 'priority_number' => Input::get('activity_priority'),*/
                    'against_offset' => Input::get('against_offset'),
                    'flat_break_deduction'=> $flat_break_deduction,
                    'own_account'=>   $own_account,
                    'amount' => $amount,
                    'amount_type'=> $amounttype,
                    'amount_value'=> FileUpload::replaceDotToComma($amount_value),
                    'allowance_bank_hours' =>$allowance_bank_hours,
                    'allowance_bank_days' =>FileUpload::replaceDotToComma(Input::get('activity_day')),
                    'hourly_rate_multiplier' => $hourly_rate_multiplier,
                    'flat_time_mode' =>   $flat_time_mode,
                    'pre_populated_with' => $pre_populated_with,
                    'pre_populated_value' =>$pre_populated_value,
                    'overtime_reducer' => $overtime_reducer,
                    'created_on'=> Input::get('time'),
                    'created_by' => 'company',
                    'deploy_status'=> 'pending',
                    'created_user_id' => $company['-id'],
                    'priority_number' => $priorityNumber
                 );
    // return $details;
    $results= MotActivity :: addActivity($details);
    return $results;
  }
  //for listing of activity
  public static function company_activity_listing($skip)
  { 
      $data=Auth::user();
      $res=(int)$skip;
      $template =CompanyActivity::where('company_id',$data['id'])->orderBy('created_on', 'desc')->skip($res)->take(16)->get();
      return  $template;
  }
  //for editing of activity
  public static function company_activity_edit($id)
  {
   /* $template_details=CompanyActivity::where('_id',$id)->get();
    return $template_details;*/
    $userDetails = Auth::user();
    $activityList = MotActivity :: getListOfActivitys($userDetails['_id'],$id);
    return $activityList;
  }
   //for copy of activity
  public static function company_activity_copy($id)
  {
   /* $template_details=CompanyActivity::where('_id',$id)->get();
    return $template_details;*/
    $isValid = false;
    $userDetails = Auth::user();
    $activityList = MotActivity :: getListOfActivitys($userDetails['_id'],$id);
    if(count($activityList)>0){
        $name=$activityList[0]['activity_name'];
        $i=0;
        while ($isValid==false) {
        $result = MotActivity::where('activity_name',$name)->where('company_id',$activityList[0]['company_id'])->get();
        if (count($result)>0) {
        $i++;
        if(strpos($name, '(')){
            $name = substr($name, 0, strpos($name, '('));
        }else{
            $name = $name;
        }
        $name=$name.'('.$i.')';
        }else{
        $isValid=true;
        if(strpos($name, '(')){
            $name = substr($name, 0, strpos($name, '('));
        }else{
            $name = $name;
        }
        $name=$name.'('.$i.')';
        }
        }
        $activityList[0]['copyName']=$name;
        return $activityList;
    }else{
      return 'failure';
    }
  }
  //for deleting of activity
  public static function company_activity_delete($id)
  {
    $activityLinkedToGeneralSettings =GeneralSetting::where('general_setting_activity',$id)->get();
    $activityLinkedToOvertimeHandlePeriod =OvertimeHandling::where('overtime_for_period',$id)->get();
    $activityLinkedToOvertimeHandleClear =OvertimeHandling::where('clear_overtime_balance',$id)->get();
    $activityLinkedToAnnualAllowance =SetAnnualAllowance::where('set_annual_allowance_activity',$id)->get();
    //return sizeof($activityLinkedToAnnualAllowance);
    if(sizeof($activityLinkedToGeneralSettings)>0 || sizeof($activityLinkedToOvertimeHandlePeriod)>0 || sizeof($activityLinkedToOvertimeHandleClear)>0 || sizeof($activityLinkedToAnnualAllowance)>0 ) {
     $response = array('status' => 'failure');
      return $response;
    }
    else {
    $activity_details=MotActivity::where('_id',$id)->delete();
    $activityMappedToTemplate=ActivitiesMappedToTemplate::where('activity_id',$id)->get();
    for ($i=0; $i <sizeof($activityMappedToTemplate) ; $i++) 
      { 
        $deleteActivityMappedToTemplate=MotTemplates::where('_id',$activityMappedToTemplate[$i]['template_id'])->delete();
        $deleteActivityMappedToTemplateData=ActivitiesMappedToTemplate::where('template_id',$activityMappedToTemplate[$i]['template_id'])->delete();
      }
    }
    if ($activity_details==1) {
      $responseToSend=array('activity_deleted'=>$id,
                            'message'=>'activity_deleted');
      Employee::sendNotificationByFcmId($responseToSend);
    }
    return $activity_details;
  }
  //for updating of activity
  public static function updateCompanyActivity()
  {
    $flat_break_deduction=true;
    $own_account=true;
    $flat_time_mode=true;
    $overtime_reducer=true;
    $amount = true;
    $amounttype=Input::get('amount_type');
    $amount_value=Input::get('amount_value');
    /*return Input::get('amount');*/
    $activity_color= Input::get('activity_color');
    $pre_populated_with=Input::get('pre_populated_with');
    $pre_populated_value=Input::get('pre_populated_value');
     $allowance_bank_hours=Input::get('activity_hour');
    if ($allowance_bank_hours=="0") {
      $allowance_bank_hours="00:00";
    }
    if(Input::get('flat_break_deduction')== null) 
    {
      $flat_break_deduction=false;
    }
    if(Input::get('own_account')== null)
    {
      $own_account=false;
    } 
    if(Input::get('amount') == null){
      $amount = false;
      $amounttype = '--';
      $amount_value='00.00';
    }
    if(Input::get('flat_time_mode') == null)
    {
      $flat_time_mode=false;
      $pre_populated_with=null;
      $pre_populated_value=null;
      $overtime_reducer=null;
    }
    if(Input::get('overtime_reducer')==null)
    {
       $overtime_reducer =false;
    }
    if (Input::get('hourly_rate_multiplier')==null) {
      $hourly_rate_multiplier='1.000';
    }
    else
    {
        $hourly_rate_multiplier=FileUpload::replaceDotToComma(Input::get('hourly_rate_multiplier'));
    }
    $company=Auth::user();
    $details = array(
                    'company_id' =>  $company['-id'],
                    'activity_name' => Input::get('activity_name'),
                   /* 'priority_number' => Input::get('activity_priority'),*/
                    'activity_color' => Input::get('activity_color'),
                    'against_offset' => Input::get('against_offset'),
                    'flat_break_deduction'=> $flat_break_deduction,
                    'own_account'=>   $own_account,
                    'amount' => $amount,
                    'amount_type'=> $amounttype,
                    'amount_value'=> FileUpload::replaceDotToComma($amount_value),
                    'allowance_bank_hours' =>$allowance_bank_hours,
                    'allowance_bank_days' =>FileUpload::replaceDotToComma(Input::get('activity_day')),
                    'hourly_rate_multiplier' => $hourly_rate_multiplier,
                    'flat_time_mode' =>   $flat_time_mode,
                    'pre_populated_with' => $pre_populated_with,
                    'pre_populated_value' =>$pre_populated_value,
                    'overtime_reducer' => $overtime_reducer,
                    'created_on'=> Input::get('time'),
                    'created_by' => 'company',
                    'created_user_id' => $company['-id']
                 );
    /*return $details;*/
    $activity_id = Input::get('activity_id');
    $results=MotActivity::updateCompanyActivity($details,$activity_id);
    return $results;    
  }
  //for updating of activity color
  public static function update_activity_color($id,$color)
  {
    $activity_details=MotActivity::where('_id',$id)->get();
    //return $activity_details;
    $color_array = array('activity_color' => '#'.$color);
    $result=MotActivity::where('_id',$id)->update($color_array);
    return $result;

  }

  public static function adminDashboard()
  {
    $company_list=Companies::where('admin',0)->get();
    /*return $company_list;*/
    return view('admin.admin_dashboard')->with('data',$company_list);
  }
/*  public static function companyListing()
  {
    $result=Companies::where('admin',0)->get();
    return $result;
  }*/
  public static function companyProfile($id)
  {
      $companyDetails =Companies::getCompanyDetail($id);
    /* return $companyDetails[0];*/
      return view('admin.company_profile')->with('data', $companyDetails);
  }
  public static function companyEmployeeList($id)
  {
    $employee =Employee::where('company_id',$id)->get();
    //return $employee;
    return view('admin.company_employee_list')->with('data',$employee);
  }
/*  public static function companyEmployeeListing($id)
  {
    $employee =Employee::where('company_id',$id)->get();
    return  $employee ;
  }*/
  public static function companyEmployeeProfile($id)
  {
    $employee =Employee::where('_id',$id)->get();
    return view('admin.company_employee_profile')->with('data', $employee);
  }
  public static function blockCompany($id)
  {
    $status=array('status'=>'block');
    $block=Companies::where('_id',$id)->update($status);
    return $block;
  }
   public static function unblockCompany($id)
  {
    $status=array('status'=>'active');
    $block=Companies::where('_id',$id)->update($status);
    return $block;
  }
  public static function deleteCompany()
  {
    $companyid=Input::get('id');
    $deleteInCompanies=Companies::where('_id',$companyid)->delete();
    $deleteInConditionForWorkingDaysSetting=ConditionForWorkingDaysSetting::where('companyid',$companyid)->delete();
    $deleteInEmployee=Employee::where('company_id',$companyid)->delete();
    $deleteInGeneralSetting=GeneralSetting::where('companyid',$companyid)->delete();
    $deleteMotActivity=MotActivity::where('company_id',$companyid)->delete();
    $deleteMotTemplates=MotTemplates::where('company_id',$companyid)->get();
    for ($i=0; $i <sizeof($deleteMotTemplates) ; $i++) { 
       $deleteActivitiesMappedToTemplate=ActivitiesMappedToTemplate::where('template_id',$deleteMotTemplates[$i]['id'])->delete();
    }
    $deleteMotTemplates=MotTemplates::where('company_id',$companyid)->delete();
    $deleteOvertimeHandling=OvertimeHandling::where('companyid',$companyid)->delete();
    $deleteSetAnnualAllowance=SetAnnualAllowance::where('companyid',$companyid)->delete();
    $deleteTimesheet=Timesheet::where('company_id',$companyid)->delete();
    $deleteWorkingDaysSetting=WorkingDaysSetting::where('companyid',$companyid)->delete();
    $deleteWebNotifications=WebNotifications::where('reciever_id',$companyid)->delete();
    if ($deleteInCompanies) {
      return "success";
    }
    else
    {
      return "failure";
    }
  }
  public static function defaultTemplateListing($skip)
  {
    $res=(int)$skip;
    $template =MotAdminTemplates::orderBy('created_on', 'desc')->skip($res)->take(16)->get();
    return  $template;
  }
  public static function defaultTemplateEdit($id)
  {
    $template_details=MotAdminTemplates::where('_id',$id)->get();
    return $template_details;
  }
  public static function defaultTemplateDelete($id)
  {
    $template_details=MotAdminTemplates::where('_id',$id)->delete();
    $results1=AdminActivitiesMappedToTemplate::where('template_id',$id)->delete();
    return $template_details;
  }
  public static function addDefaultTemplate()
  {
    $superAdmin=Auth::user();
    $offsetVal=FileUpload::replaceDotToColon(Input::get('template_offset_time'));
    $details = array(
              'company_id' =>  $superAdmin['-id'],
              'template_name' => Input::get('template_name'),
              'template_name_german' => Input::get('template_name_german'),
              'template_offset_time' =>  $offsetVal,
              'number_of_activity' => Input::get('number_of_activity'),
              'deploy_status'=> 'pending',
              // 'template_activity' => Input::get('template_activity'),
              // 'template_flat_hours'=>Input::get('template_flat_hours'),
              // 'template_amount'=> Input::get('template_amount'),
              // 'template_start' => Input::get('template_start'),
              // 'template_end'=> Input::get('template_end'),
              // 'template_break' => Input::get('template_break'),
              'created_on'=> Input::get('time'),
              'created_by' => 'MOT',
              'created_user_id' => $superAdmin['_id']

              );
  /*  return $details;*/
  $results=MotAdminTemplates::addTemplate($details);
   return $results;
  }
  public static function updateDefaultTemplate()
  {
    /*$company=Auth::user();
    $details = array(
              'template_name' => Input::get('template_name1'),
              'template_offset_time' => Input::get('template_offset_time1'),
              'template_activity' => Input::get('template_activity1'),
              'template_flat_hours'=>Input::get('template_flat_hours1'),
              'template_amount'=> Input::get('template_amount1'),
              'template_start' => Input::get('template_start1'),
              'template_end'=> Input::get('template_end1'),
              'template_break' => Input::get('template_break1'),
              );
    $template_id = Input::get('template_id');
    $results=MotAdminTemplates::updateTemplate($details,$template_id);
    return $results;*/
    $company=Auth::user();
   /*$details=Input::get('number_of_activity');
    return $details;*/
    $template_id=Input::get('template_id');
    $number_of_activity= Input::get('number_of_activity');
   /* $results2=MotTemplates::where('_id',$template_id)->delete();*/
  /*  return $template_id;*/
   $offsetVal=FileUpload::replaceDotToColon(Input::get('template_offset_time'));
    $details = array(
              'company_id' =>  $company['-id'],
              'template_name' => Input::get('template_name'),
              'template_name_german' => Input::get('template_name_german'),
              'template_offset_time' => $offsetVal,
              // 'template_activity' => Input::get('template_activity'),
              // 'template_flat_hours'=>Input::get('template_flat_hours'),
              // 'template_amount'=> Input::get('template_amount'),
              // 'template_start' => Input::get('template_start'),
              // 'template_end'=> Input::get('template_end'),
              // 'template_break' => Input::get('template_break'),
              'created_on'=> Input::get('time'),
              'created_by' => 'company',
              'created_user_id' => $company['-id']
              );
     $results=MotAdminTemplates::updateTemplate($details,$template_id,$number_of_activity);
   return $results;
  }
  public static function activityListForDefaultTemplate()
  {
    $result= MotAdminActivity::all();
    return $result;
  }
  public static function defaultActivityListing($skip)
  {
    $res=(int)$skip;
    $activity_details=MotAdminActivity::orderBy('created_on', 'desc')->skip($res)->take(16)->get();
    return  $activity_details;
  }
    public static function defaultActivityEdit($id)
  {
    $activity_details=MotAdminActivity::where('_id',$id)->get();
    return $activity_details;
  }
  public static function defaultActivityDelete($id)
  {
    $activity_details=MotAdminActivity::where('_id',$id)->delete();
    $activityMappedToTemplate=AdminActivitiesMappedToTemplate::where('activity_id',$id)->get();
    for ($i=0; $i <sizeof($activityMappedToTemplate) ; $i++) 
      { 
        $deleteActivityMappedToTemplate=MotAdminTemplates::where('_id',$activityMappedToTemplate[$i]['template_id'])->delete();
        $deleteActivityMappedToTemplateData=AdminActivitiesMappedToTemplate::where('template_id',$activityMappedToTemplate[$i]['template_id'])->delete();
      }
    return $activity_details;
  }
  public static function addDefaultActivity()
  {  
    $flat_break_deduction=true;
    $own_account=true;
    $flat_time_mode=true;
    $overtime_reducer=true;
    $amount = true;
    $amounttype=Input::get('amount_type');
    $amount_value=Input::get('amount_value');
    $pre_populated_with=Input::get('pre_populated_with');
    $pre_populated_value=Input::get('pre_populated_value');
     $allowance_bank_hours=Input::get('activity_hour');
    if ($allowance_bank_hours=="0") {
      $allowance_bank_hours="00:00";
    }
    if(Input::get('flat_break_deduction') == null) 
    {
      $flat_break_deduction=false;
    }
    if(Input::get('own_account') == null)
    {
      $own_account=false;
    }
    if(Input::get('amount') == null){
      $amount = false;
      $amounttype = '--';
      $amount_value='00.00';
    } 
    if(Input::get('flat_time_mode') == null)
    {
      $flat_time_mode=false;
      $pre_populated_with=null;
      $pre_populated_value=null;
      $overtime_reducer=null;
    }
    if(Input::get('overtime_reducer')==null)
    {
       $overtime_reducer =false;
    }
    if (Input::get('hourly_rate_multiplier')==null) {
      $hourly_rate_multiplier='1.000';
    }
    else
    {
      $hourly_rate_multiplier=Input::get('hourly_rate_multiplier');
    }
    $superAdmin=Auth::user();
    $priorityNumber = 0;
    $companyActivityList = MotAdminActivity :: where('company_id',$superAdmin['-id'])->orderBy('priority_number', 'desc')->get();
    if (sizeof($companyActivityList)) {
      $priorityNumber = (int)$companyActivityList[0]['priority_number'];
      $priorityNumber = $priorityNumber+1;
    }
    $details = array(
                    'company_id' =>  $superAdmin['-id'],
                    'activity_name' => Input::get('activity_name'),
                    'activity_german_name' => Input::get('activity_german_name'),
                    'against_offset' => Input::get('against_offset'),
                    'flat_break_deduction'=> $flat_break_deduction,
                    'own_account'=>   $own_account,
                    'amount' => $amount,
                    'amount_type'=> $amounttype,
                    'amount_value'=> $amount_value,
                    'allowance_bank_hours' =>$allowance_bank_hours,
                    'allowance_bank_days' => Input::get('activity_day'),
                    'hourly_rate_multiplier' => $hourly_rate_multiplier,
                    'flat_time_mode' =>   $flat_time_mode,
                    'pre_populated_with' => $pre_populated_with,
                    'pre_populated_value' =>$pre_populated_value,
                    'overtime_reducer' => $overtime_reducer,
                    'created_on'=> Input::get('time'),
                    'deploy_status'=> 'pending',
                    'created_by' => 'MOT',
                    'created_user_id' => $superAdmin['_id'],
                    'priority_number' => $priorityNumber
   );
    /*return  $details;*/
    $results=MotAdminActivity::addActivity($details);
    return $results;
  }
  public static function updateDefaultActivity()
  {
    $flat_break_deduction=true;
    $own_account=true;
    $flat_time_mode=true;
    $overtime_reducer=true;
    $amount = true;
    $amounttype=Input::get('amount_type');
    $amount_value=Input::get('amount_value');
    $pre_populated_with=Input::get('pre_populated_with');
    $pre_populated_value=Input::get('pre_populated_value');
    $allowance_bank_hours=Input::get('activity_hour');
    if ($allowance_bank_hours=="0") {
      $allowance_bank_hours="00:00";
    }
    if(Input::get('flat_break_deduction')== null) 
    {
      $flat_break_deduction=false;
    }
    if(Input::get('own_account')== null)
    {
      $own_account=false;
    }
    if(Input::get('amount') == null){
      $amount = false;
      $amounttype = '--';
      $amount_value='00.00';
    } 
    if(Input::get('flat_time_mode') == null)
    {
      $flat_time_mode=false;
      $pre_populated_with=null;
      $pre_populated_value=null;
      $overtime_reducer=null;
    }
    if(Input::get('overtime_reducer')==null)
    {
       $overtime_reducer =false;
    }
    if (Input::get('hourly_rate_multiplier')==null) {
      $hourly_rate_multiplier='1.000';
    }
    else
    {
      $hourly_rate_multiplier=Input::get('hourly_rate_multiplier');
    }
    $superAdmin=Auth::user();
    $details = array(
                    'company_id' =>  $superAdmin['-id'],
                    'activity_name' => Input::get('activity_name'),
                    'activity_german_name' => Input::get('activity_german_name'),
                    'against_offset' => Input::get('against_offset'),
                    'flat_break_deduction'=> $flat_break_deduction,
                    'own_account'=>   $own_account,
                    'amount' => $amount,
                    'amount_type'=> $amounttype,
                    'amount_value'=> $amount_value,
                    'allowance_bank_hours' =>$allowance_bank_hours,
                    'allowance_bank_days' => Input::get('activity_day'),
                    'hourly_rate_multiplier' => $hourly_rate_multiplier,
                    'flat_time_mode' =>   $flat_time_mode,
                    'pre_populated_with' => $pre_populated_with,
                    'pre_populated_value' =>$pre_populated_value,
                    'overtime_reducer' => $overtime_reducer,
                    'created_on'=> Input::get('time'),
                    'created_by' => 'MOT',
                    'created_user_id' => $superAdmin['_id']
   );
    $activity_id = Input::get('activity_id');
    $results=MotAdminActivity::updateDefaultActivity($details,$activity_id);
    return $results;    
  }
  public static function addLanguage()
  {
    $details=array('language_code'=>Input::get('language_code'),
                    'language_name'=>Input::get('language_name'),
                    'country_code'=>Input::get('country_code'),
                    );
    $result=Language::addLanguage($details);
    return $result;
  }
  public static function addTranslation()
  {
    $details=array( 'translation_page'=>Input::get('translation_page'),
                    'translation_key'=>Input::get('translation_key'),
                    'translation_en'=>Input::get('translation_en'),
                    'translation_de'=>Input::get('translation_de'),
                    );
    $result=Translation::addTranslation($details);
    return $result;
  }
  public static function getTranslation($lang)
  {
    $result=Translation::where('language_code',$lang)->get();
    return $result;
  }
  //set working data
public static function setWorkingData()
  {
    /*return 'ff';*/
   $monday_isset=true;
   $tuesday_isset=true;
   $wednesday_isset=true;
   $thursday_isset=true;
   $friday_isset=true;
   $saturday_isset=true;
   $sunday_isset=true;
   if(Input::get('monday_isset') == null)
   {
    $monday_isset=false;
   }
   if(Input::get('tuesday_isset')==null)
   {
    $tuesday_isset=false;
   }
   if(Input::get('wednesday_isset')==null)
   {
    $wednesday_isset=false;
   }
   if(Input::get('thursday_isset')==null)
   {
    $thursday_isset=false;
   }
   if(Input::get('friday_isset')==null)
   {
    $friday_isset=false;
   }
   if(Input::get('saturday_isset')==null)
   {
    $saturday_isset=false;
   }
   if(Input::get('sunday_isset')==null)
   {
    $sunday_isset=false;
   }

   $ctb_monday=true;
   $ctb_tuesday=true;
   $ctb_wednesday=true;
   $ctb_thursday=true;
   $ctb_friday=true;
   $ctb_saturday=true;
   $ctb_sunday=true;
   if(Input::get('ctb_monday') == null)
   {
    $ctb_monday=false;
   }
   if(Input::get('ctb_tuesday')==null)
   {
    $ctb_tuesday=false;
   }
   if(Input::get('ctb_wednesday')==null)
   {
    $ctb_wednesday=false;
   }
   if(Input::get('ctb_thursday')==null)
   {
    $ctb_thursday=false;
   }
   if(Input::get('ctb_friday')==null)
   {
    $ctb_friday=false;
   }
   if(Input::get('ctb_saturday')==null)
   {
    $ctb_saturday=false;
   }
   if(Input::get('ctb_sunday')==null)
   {
    $ctb_sunday=false;
   }
   /*$res=Input::get('monday_isset');*/
  /* return $res;*/
  $company=Auth::user();
    $monday_details=array(
                          'day'=>'Monday',
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>Input::get('work_days_offset_monday'),
                          'ctb'=>$ctb_monday,
                          'break_default'=>Input::get('break_default_monday'),
                          'template_id'=>Input::get('template_monday'),
                          'isset'=>$monday_isset,
                         );
    $tuesday_details=array(
                          'day'=>'Tuesday',
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>Input::get('work_days_offset_tuesday'),
                          'ctb'=>$ctb_tuesday,
                          'break_default'=>Input::get('break_default_tuesday'),
                          'template_id'=>Input::get('template_tuesday'),
                          'isset'=>$tuesday_isset,
                         );
    $wednesday_details=array(
                          'day'=>'Wednesday',
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>Input::get('work_days_offset_wednesday'),
                          'ctb'=>$ctb_wednesday,
                          'break_default'=>Input::get('break_default_wednesday'),
                          'template_id'=>Input::get('template_wednesday'),
                          'isset'=>$wednesday_isset,
                         );
    $thursday_details=array(
                          'day'=>'Thursday',
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>Input::get('work_days_offset_thursday'),
                          'ctb'=>$ctb_thursday,
                          'break_default'=>Input::get('break_default_thursday'),
                          'template_id'=>Input::get('template_thursday'),
                          'isset'=>$thursday_isset,
                         );
    $friday_details=array(
                          'day'=>'Friday',
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>Input::get('work_days_offset_friday'),
                          'ctb'=>$ctb_friday,
                          'break_default'=>Input::get('break_default_friday'),
                          'template_id'=>Input::get('template_friday'),
                          'isset'=>$friday_isset,
                         );
    $saturday_details=array(
                          'day'=>'Saturday',
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>Input::get('work_days_offset_saturday'),
                          'ctb'=>$ctb_saturday,
                          'break_default'=>Input::get('break_default_saturday'),
                          'template_id'=>Input::get('template_saturday'),
                          'isset'=>$saturday_isset,
                         );
    $sunday_details=array(
                          'day'=>'Sunday',
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>Input::get('work_days_offset_sunday'),
                          'ctb'=>$ctb_sunday,
                          'break_default'=>Input::get('break_default_sunday'),
                          'template_id'=>Input::get('template_sunday'),
                          'isset'=>$sunday_isset,
                         );
    $set_working_data= array('0'=>$monday_details,
                 '1'=>$tuesday_details,
                 '2'=>$wednesday_details,
                 '3'=>$thursday_details,
                 '4'=>$friday_details,
                 '5'=>$saturday_details,
                 '6'=>$sunday_details
                  );
    /*return $data;*/
    $from_monday=Input::get('conditions_monday');
    $from_tuesday=Input::get('conditions_tuesday');
    $from_wednesday=Input::get('conditions_wednesday');
    $from_thursday=Input::get('conditions_thursday');
    $from_friday=Input::get('conditions_friday');
    $from_saturday=Input::get('conditions_saturday');
    $from_sunday=Input::get('conditions_sunday');
    $from_monday1=Input::get('conditions_monday1');
    $from_tuesday1=Input::get('conditions_tuesday1');
    $from_wednesday1=Input::get('conditions_wednesday1');
    $from_thursday1=Input::get('conditions_thursday1');
    $from_friday1=Input::get('conditions_friday1');
    $from_saturday1=Input::get('conditions_saturday1');
    $from_sunday1=Input::get('conditions_sunday1');

    //factor
    $factor_monday= FileUpload::replaceDotToComma(Input::get('factor_monday'));
    $factor_monday1= FileUpload::replaceDotToComma(Input::get('factor_monday1'));
    $factor_tuesday= FileUpload::replaceDotToComma(Input::get('factor_tuesday'));
    $factor_tuesday1= FileUpload::replaceDotToComma(Input::get('factor_tuesday1'));
    $factor_wednesday= FileUpload::replaceDotToComma(Input::get('factor_wednesday'));
    $factor_wednesday1= FileUpload::replaceDotToComma(Input::get('factor_wednesday1'));
    $factor_thursday= FileUpload::replaceDotToComma(Input::get('factor_thursday'));
    $factor_thursday1= FileUpload::replaceDotToComma(Input::get('factor_thursday1'));
    $factor_friday= FileUpload::replaceDotToComma(Input::get('factor_friday'));
    $factor_friday1= FileUpload::replaceDotToComma(Input::get('factor_friday1'));
    $factor_saturday= FileUpload::replaceDotToComma(Input::get('factor_saturday'));
    $factor_saturday1= FileUpload::replaceDotToComma(Input::get('factor_saturday1'));
    $factor_sunday= FileUpload::replaceDotToComma(Input::get('factor_sunday'));
    $factor_sunday1= FileUpload::replaceDotToComma(Input::get('factor_sunday1'));

    //from_monday
    if($from_monday=='Time')
     {
        $from_monday_val=Input::get('from_monday');
     }
     elseif ($from_monday=='Hours') {
       $from_monday_val=Input::get('from_monday11');
     }
     elseif ($from_monday=="No Condition") {
       $from_monday_val="00:00";
       $factor_monday=00;
     }
     //from_tuesday
     if($from_tuesday=='Time')
     {
        $from_tuesday_val=Input::get('from_tuesday');
     }
     elseif ($from_tuesday=='Hours') {
       $from_tuesday_val=Input::get('from_tuesday11');
     }
     elseif ($from_tuesday=="No Condition") {
       $from_tuesday_val="00:00";
       $factor_tuesday=00;
     }
     //from_wednesday
     if($from_wednesday=='Time')
     {
        $from_wednesday_val=Input::get('from_wednesday');
     }
     elseif ($from_wednesday=='Hours') {
       $from_wednesday_val=Input::get('from_wednesday11');
     }
     elseif ($from_wednesday=="No Condition") {
       $from_wednesday_val="00:00";
       $factor_wednesday=00;
     }
     //from_thursday
     if($from_thursday=='Time')
     {
        $from_thursday_val=Input::get('from_thursday');
     }
     elseif ($from_thursday=='Hours') {
       $from_thursday_val=Input::get('from_thursday11');
     }
     elseif ($from_thursday=="No Condition") {
       $from_thursday_val="00:00";
       $factor_thursday=00;
     }
     //from_friday
     if($from_friday=='Time')
     {
        $from_friday_val=Input::get('from_friday');
     }
     elseif ($from_friday=='Hours') {
       $from_friday_val=Input::get('from_friday11');
     }
     elseif ($from_friday=="No Condition") {
       $from_friday_val="00:00";
       $factor_friday=00;
     }
     //from_saturday
     if($from_saturday=='Time')
     {
        $from_saturday_val=Input::get('from_saturday');
     }
     elseif ($from_saturday=='Hours') {
       $from_saturday_val=Input::get('from_saturday11');
     }
     elseif ($from_saturday=="No Condition") {
       $from_saturday_val="00:00";
       $factor_saturday=00;
     }
     //from_sunday
     if($from_sunday=='Time')
     {
        $from_sunday_val=Input::get('from_sunday');
     }
     elseif ($from_sunday=='Hours') {
       $from_sunday_val=Input::get('from_sunday11');
     }
     elseif ($from_sunday=="No Condition") {
       $from_sunday_val="00:00";
       $factor_sunday=00;
     }
     //form_mondy1
     if($from_monday1=='Time')
     {
        $from_monday_val1=Input::get('from_monday1');
     }
     elseif ($from_monday1=='Hours') {
       $from_monday_val1=Input::get('from_monday111');
     }
     elseif ($from_monday1=="No Condition") {
       $from_monday_val1="00:00";
       $factor_monday1=00;
     }
     //from_tuesday1
     if($from_tuesday1=='Time')
     {
        $from_tuesday_val1=Input::get('from_tuesday1');
     }
     elseif ($from_tuesday1=='Hours') {
       $from_tuesday_val1=Input::get('from_tuesday111');
     }
     elseif ($from_tuesday1=="No Condition") {
       $from_tuesday_val1="00:00";
       $factor_tuesday1=00;
     }
     //from_wednesday1
     if($from_wednesday1=='Time')
     {
        $from_wednesday_val1=Input::get('from_wednesday1');
     }
     elseif ($from_wednesday1=='Hours') {
       $from_wednesday_val1=Input::get('from_wednesday111');
     }
     elseif ($from_wednesday1=="No Condition") {
       $from_wednesday_val1="00:00";
       $factor_wednesday1=00;
     }
     //from_thursday
     if($from_thursday1=='Time')
     {
        $from_thursday_val1=Input::get('from_thursday1');
     }
     elseif ($from_thursday1=='Hours') {
       $from_thursday_val1=Input::get('from_thursday111');
     }
     elseif ($from_thursday1=="No Condition") {
       $from_thursday_val1="00:00";
       $factor_thursday1=00;
     }
     //from_friday
     if($from_friday1=='Time')
     {
        $from_friday_val1=Input::get('from_friday1');
     }
     elseif ($from_friday1=='Hours') {
       $from_friday_val1=Input::get('from_friday111');
     }
     elseif ($from_friday1=="No Condition") {
       $from_friday_val1="00:00";
       $factor_friday1=00;
     }
     //from_saturday
     if($from_saturday1=='Time')
     {
        $from_saturday_val1=Input::get('from_saturday1');
     }
     elseif ($from_saturday1=='Hours') {
       $from_saturday_val1=Input::get('from_saturday111');
     }
     elseif ($from_saturday1=="No Condition") {
       $from_saturday_val1="00:00";
       $factor_saturday1=00;
     }
     //from_sunday
     if($from_sunday1=='Time')
     {
        $from_sunday_val1=Input::get('from_sunday1');
     }
     elseif ($from_sunday1=='Hours') {
       $from_sunday_val1=Input::get('from_sunday111');
     }
     elseif ($from_sunday1=="No Condition") {
       $from_sunday_val1="00:00";
       $factor_sunday1=00;
     }

    $monday_condtion_details=array( 
                             'companyid'=>$company['-id'],
                             'condtion'=>Input::get('conditions_monday'),
                             'from'=>$from_monday_val,
                             'factor'=>$factor_monday,
                          );
  /*  return $monday_condtion_details;*/
    $monday_condtion_details1=array( 'companyid'=>$company['-id'],
                              'condtion'=>Input::get('conditions_monday1'),
                             'from'=>$from_monday_val1,
                             'factor'=>$factor_monday1,
                          );
    $tuesday_condtion_details=array( 'condtion'=>Input::get('conditions_tuesday'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_tuesday_val,
                             'factor'=>$factor_tuesday,
                          );
    $tuesday_condtion_details1=array( 'condtion'=>Input::get('conditions_tuesday1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_tuesday_val1,
                             'factor'=>$factor_tuesday1,
                          );
    $wednesday_condtion_details=array( 'condtion'=>Input::get('conditions_wednesday'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_wednesday_val,
                             'factor'=>$factor_wednesday,
                          );
    $wednesday_condtion_details1=array( 'condtion'=>Input::get('conditions_wednesday1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_wednesday_val1,
                             'factor'=>$factor_wednesday1,
                          );
    $thursday_condtion_details=array( 'condtion'=>Input::get('conditions_thursday'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_thursday_val,
                             'factor'=>$factor_thursday,
                          );
    $thursday_condtion_details1=array( 'condtion'=>Input::get('conditions_thursday1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_thursday_val1,
                             'factor'=>$factor_thursday1,
                          );
    $friday_condtion_details=array( 'condtion'=>Input::get('conditions_friday'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_friday_val,
                             'factor'=>$factor_friday,
                          );
    $friday_condtion_details1=array( 'condtion'=>Input::get('conditions_friday1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_friday_val1,
                             'factor'=>$factor_friday1,
                          );
    $saturday_condtion_details=array( 'condtion'=>Input::get('conditions_saturday'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_saturday_val,
                             'factor'=>$factor_saturday,
                          );
    $saturday_condtion_details1=array( 'condtion'=>Input::get('conditions_saturday1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_saturday_val1,
                             'factor'=>$factor_saturday1,
                          );
    $sunday_condtion_details=array( 'condtion'=>Input::get('conditions_sunday'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_sunday_val,
                             'factor'=>$factor_sunday,
                          );
    $sunday_condtion_details1=array( 'condtion'=>Input::get('conditions_sunday1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_sunday_val1,
                             'factor'=>$factor_sunday1,
                          );
    $condtion=array(
                '0'=>array('0'=>$monday_condtion_details,
                           '1'=>$monday_condtion_details1,),
                '1'=>array('0'=>$tuesday_condtion_details,
                           '1'=>$tuesday_condtion_details1,),
                '2'=>array('0'=>$wednesday_condtion_details,
                           '1'=>$wednesday_condtion_details1,),
                '3'=>array('0'=>$thursday_condtion_details,
                           '1'=>$thursday_condtion_details1,),
                '4'=>array('0'=>$friday_condtion_details,
                           '1'=>$friday_condtion_details1,),
                '5'=>array('0'=>$saturday_condtion_details,
                           '1'=>$saturday_condtion_details1,),
                '6'=>array('0'=>$sunday_condtion_details,
                           '1'=>$sunday_condtion_details1,),
                );
   /* return $condtion;*/
    $results=WorkingDaysSetting::setWorkingData($set_working_data,$condtion);
    return $results;
  }
  public static function updateWorkingData()
  {

    /*return 'ff';*/
   $monday_isset=true;
   $tuesday_isset=true;
   $wednesday_isset=true;
   $thursday_isset=true;
   $friday_isset=true;
   $saturday_isset=true;
   $sunday_isset=true;
   if(Input::get('monday_isset') == null)
   {
    $monday_isset=false;
   }
   if(Input::get('tuesday_isset')==null)
   {
    $tuesday_isset=false;
   }
   if(Input::get('wednesday_isset')==null)
   {
    $wednesday_isset=false;
   }
   if(Input::get('thursday_isset')==null)
   {
    $thursday_isset=false;
   }
   if(Input::get('friday_isset')==null)
   {
    $friday_isset=false;
   }
   if(Input::get('saturday_isset')==null)
   {
    $saturday_isset=false;
   }
   if(Input::get('sunday_isset')==null)
   {
    $sunday_isset=false;
   }

   $ctb_monday=true;
   $ctb_tuesday=true;
   $ctb_wednesday=true;
   $ctb_thursday=true;
   $ctb_friday=true;
   $ctb_saturday=true;
   $ctb_sunday=true;
   if(Input::get('ctb_monday') == null)
   {
    $ctb_monday=false;
   }
   if(Input::get('ctb_tuesday')==null)
   {
    $ctb_tuesday=false;
   }
   if(Input::get('ctb_wednesday')==null)
   {
    $ctb_wednesday=false;
   }
   if(Input::get('ctb_thursday')==null)
   {
    $ctb_thursday=false;
   }
   if(Input::get('ctb_friday')==null)
   {
    $ctb_friday=false;
   }
   if(Input::get('ctb_saturday')==null)
   {
    $ctb_saturday=false;
   }
   if(Input::get('ctb_sunday')==null)
   {
    $ctb_sunday=false;
   }
   /*$res=Input::get('monday_isset');*/
  /* return $res;*/
/*  function replaceDotToColon($val)
    {
    if (strpos($val, '.') !== FALSE)
    {
      return str_replace('.',':',$val);
    }
    elseif(strpos($val, ',') !== FALSE)
    {
      return str_replace(',',':',$val);
    }
    else
    {
      return $val;
    }
    }*/
  $company=Auth::user();
    $monday_details=array(
                          'day'=>'Monday',
                          'id'=>Input::get('mondayid'),
                          'ctb'=>$ctb_monday,
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>FileUpload::replaceDotToColon(Input::get('work_days_offset_monday')),
                          'break_default'=>FileUpload::replaceDotToColon(Input::get('break_default_monday')),
                          'template_id'=>Input::get('template_monday'),
                          'isset'=>$monday_isset,
                         );
    $tuesday_details=array(
                          'day'=>'Tuesday',
                          'id'=>Input::get('tuesdayid'),
                          'ctb'=>$ctb_tuesday,
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>FileUpload::replaceDotToColon(Input::get('work_days_offset_tuesday')),
                          'break_default'=>FileUpload::replaceDotToColon(Input::get('break_default_tuesday')),
                          'template_id'=>Input::get('template_tuesday'),
                          'isset'=>$tuesday_isset,
                         );
    $wednesday_details=array(
                          'day'=>'Wednesday',
                          'id'=>Input::get('wednesdayid'),
                          'ctb'=>$ctb_wednesday,
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>FileUpload::replaceDotToColon(Input::get('work_days_offset_wednesday')),
                          'break_default'=>FileUpload::replaceDotToColon(Input::get('break_default_wednesday')),
                          'template_id'=>Input::get('template_wednesday'),
                          'isset'=>$wednesday_isset,
                         );
    $thursday_details=array(
                          'day'=>'Thursday',
                          'id'=>Input::get('thursdayid'),
                          'ctb'=>$ctb_thursday,
                          'companyid'=>$company['-id'],
                          'work_days_offset'=>FileUpload::replaceDotToColon(Input::get('work_days_offset_thursday')),
                          'break_default'=>FileUpload::replaceDotToColon(Input::get('break_default_thursday')),
                          'template_id'=>Input::get('template_thursday'),
                          'isset'=>$thursday_isset,
                         );
    $friday_details=array(
                          'day'=>'Friday',
                          'companyid'=>$company['-id'],
                          'id'=>Input::get('fridayid'),
                          'ctb'=>$ctb_friday,
                          'work_days_offset'=>FileUpload::replaceDotToColon(Input::get('work_days_offset_friday')),
                          'break_default'=>FileUpload::replaceDotToColon(Input::get('break_default_friday')),
                          'template_id'=>Input::get('template_friday'),
                          'isset'=>$friday_isset,
                         );
    $saturday_details=array(
                          'day'=>'Saturday',
                          'companyid'=>$company['-id'],
                          'id'=>Input::get('saturdayid'),
                          'ctb'=>$ctb_saturday,
                          'work_days_offset'=>FileUpload::replaceDotToColon(Input::get('work_days_offset_saturday')),
                          'break_default'=>FileUpload::replaceDotToColon(Input::get('break_default_saturday')),
                          'template_id'=>Input::get('template_saturday'),
                          'isset'=>$saturday_isset,
                         );
    $sunday_details=array(
                          'day'=>'Sunday',
                          'companyid'=>$company['-id'],
                          'id'=>Input::get('sundayid'),
                          'ctb'=>$ctb_sunday,
                          'work_days_offset'=>FileUpload::replaceDotToColon(Input::get('work_days_offset_sunday')),
                          'break_default'=>FileUpload::replaceDotToColon(Input::get('break_default_sunday')),
                          'template_id'=>Input::get('template_sunday'),
                          'isset'=>$sunday_isset,
                         );
    /*return $sunday_details;*/
    $set_working_data= array('0'=>$monday_details,
                 '1'=>$tuesday_details,
                 '2'=>$wednesday_details,
                 '3'=>$thursday_details,
                 '4'=>$friday_details,
                 '5'=>$saturday_details,
                 '6'=>$sunday_details
                  );
    /*return $set_working_data['0']['id'];*/
    $from_monday=Input::get('conditions_monday');
    $from_tuesday=Input::get('conditions_tuesday');
    $from_wednesday=Input::get('conditions_wednesday');
    $from_thursday=Input::get('conditions_thursday');
    $from_friday=Input::get('conditions_friday');
    $from_saturday=Input::get('conditions_saturday');
    $from_sunday=Input::get('conditions_sunday');
    $from_monday1=Input::get('conditions_monday1');
    $from_tuesday1=Input::get('conditions_tuesday1');
    $from_wednesday1=Input::get('conditions_wednesday1');
    $from_thursday1=Input::get('conditions_thursday1');
    $from_friday1=Input::get('conditions_friday1');
    $from_saturday1=Input::get('conditions_saturday1');
    $from_sunday1=Input::get('conditions_sunday1');

    //factor
    $factor_monday= FileUpload::replaceDotToComma(Input::get('factor_monday'));
    $factor_monday1= FileUpload::replaceDotToComma(Input::get('factor_monday1'));
    $factor_tuesday= FileUpload::replaceDotToComma(Input::get('factor_tuesday'));
    $factor_tuesday1= FileUpload::replaceDotToComma(Input::get('factor_tuesday1'));
    $factor_wednesday= FileUpload::replaceDotToComma(Input::get('factor_wednesday'));
    $factor_wednesday1= FileUpload::replaceDotToComma(Input::get('factor_wednesday1'));
    $factor_thursday= FileUpload::replaceDotToComma(Input::get('factor_thursday'));
    $factor_thursday1= FileUpload::replaceDotToComma(Input::get('factor_thursday1'));
    $factor_friday= FileUpload::replaceDotToComma(Input::get('factor_friday'));
    $factor_friday1= FileUpload::replaceDotToComma(Input::get('factor_friday1'));
    $factor_saturday= FileUpload::replaceDotToComma(Input::get('factor_saturday'));
    $factor_saturday1= FileUpload::replaceDotToComma(Input::get('factor_saturday1'));
    $factor_sunday= FileUpload::replaceDotToComma(Input::get('factor_sunday'));
    $factor_sunday1= FileUpload::replaceDotToComma(Input::get('factor_sunday1'));

    /*function replaceDotToColon($val)
    {
    if (strpos('.', $val) == false) {
    return str_replace('.',':',$val);
    }
    }*/
    //from_monday
    if($from_monday=='Time')
     {
        $from_monday_val=Input::get('from_monday');
     }
     elseif ($from_monday=='Hours') {
       $from_monday_val=Input::get('from_monday11');
       $from_monday_val=FileUpload::replaceDotToColon($from_monday_val);
     }
     elseif ($from_monday=="No Condition") {
       $from_monday_val="00:00";
       $factor_monday=00;
     }
     //from_tuesday
     if($from_tuesday=='Time')
     {
        $from_tuesday_val=Input::get('from_tuesday');
     }
     elseif ($from_tuesday=='Hours') {
       $from_tuesday_val=Input::get('from_tuesday11');
       $from_tuesday_val=FileUpload::replaceDotToColon($from_tuesday_val);
     }
     elseif ($from_tuesday=="No Condition") {
       $from_tuesday_val="00:00";
       $factor_tuesday=00;
     }
     //from_wednesday
     if($from_wednesday=='Time')
     {
        $from_wednesday_val=Input::get('from_wednesday');
     }
     elseif ($from_wednesday=='Hours') {
       $from_wednesday_val=Input::get('from_wednesday11');
       $from_wednesday_val=FileUpload::replaceDotToColon($from_wednesday_val);
     }
     elseif ($from_wednesday=="No Condition") {
       $from_wednesday_val="00:00";
       $factor_wednesday=00;
     }
     //from_thursday
     if($from_thursday=='Time')
     {
        $from_thursday_val=Input::get('from_thursday');
     }
     elseif ($from_thursday=='Hours') {
       $from_thursday_val=Input::get('from_thursday11');
       $from_thursday_val=FileUpload::replaceDotToColon($from_thursday_val);
     }
     elseif ($from_thursday=="No Condition") {
       $from_thursday_val="00:00";
       $factor_thursday=00;
     }
     //from_friday
     if($from_friday=='Time')
     {
        $from_friday_val=Input::get('from_friday');
     }
     elseif ($from_friday=='Hours') {
       $from_friday_val=Input::get('from_friday11');
       $from_friday_val=FileUpload::replaceDotToColon($from_friday_val);
     }
     elseif ($from_friday=="No Condition") {
       $from_friday_val="00:00";
       $factor_friday=00;
     }
     //from_saturday
     if($from_saturday=='Time')
     {
        $from_saturday_val=Input::get('from_saturday');
     }
     elseif ($from_saturday=='Hours') {
       $from_saturday_val=Input::get('from_saturday11');
       $from_saturday_val=FileUpload::replaceDotToColon($from_saturday_val);
     }
     elseif ($from_saturday=="No Condition") {
       $from_saturday_val="00:00";
       $factor_saturday=00;
     }
     //from_sunday
     if($from_sunday=='Time')
     {
        $from_sunday_val=Input::get('from_sunday');
     }
     elseif ($from_sunday=='Hours') {
       $from_sunday_val=Input::get('from_sunday11');
       $from_sunday_val=FileUpload::replaceDotToColon($from_sunday_val);
     }
     elseif ($from_sunday=="No Condition") {
       $from_sunday_val="00:00";
       $factor_sunday=00;
     }
     //form_mondy1
     if($from_monday1=='Time')
     {
        $from_monday_val1=Input::get('from_monday1');
     }
     elseif ($from_monday1=='Hours') {
       $from_monday_val1=Input::get('from_monday111');
       $from_monday_val1=FileUpload::replaceDotToColon($from_monday_val1);
     }
     elseif ($from_monday1=="No Condition") {
       $from_monday_val1="00:00";
       $factor_monday1=00;
     }
     //from_tuesday1
     if($from_tuesday1=='Time')
     {
        $from_tuesday_val1=Input::get('from_tuesday1');
     }
     elseif ($from_tuesday1=='Hours') {
       $from_tuesday_val1=Input::get('from_tuesday111');
       $from_tuesday_val1=FileUpload::replaceDotToColon($from_tuesday_val1);
     }
     elseif ($from_tuesday1=="No Condition") {
       $from_tuesday_val1="00:00";
       $factor_tuesday1=00;
     }
     //from_wednesday1
     if($from_wednesday1=='Time')
     {
        $from_wednesday_val1=Input::get('from_wednesday1');
     }
     elseif ($from_wednesday1=='Hours') {
       $from_wednesday_val1=Input::get('from_wednesday111');
       $from_wednesday_val1=FileUpload::replaceDotToColon($from_wednesday_val1);
     }
     elseif ($from_wednesday1=="No Condition") {
       $from_wednesday_val1="00:00";
       $factor_wednesday1=00;
     }
     //from_thursday
     if($from_thursday1=='Time')
     {
        $from_thursday_val1=Input::get('from_thursday1');
     }
     elseif ($from_thursday1=='Hours') {
       $from_thursday_val1=Input::get('from_thursday111');
       $from_thursday_val1=FileUpload::replaceDotToColon($from_thursday_val1);
     }
     elseif ($from_thursday1=="No Condition") {
       $from_thursday_val1="00:00";
       $factor_thursday1=00;
     }
     //from_friday
     if($from_friday1=='Time')
     {
        $from_friday_val1=Input::get('from_friday1');
     }
     elseif ($from_friday1=='Hours') {
       $from_friday_val1=Input::get('from_friday111');
       $from_friday_val1=FileUpload::replaceDotToColon($from_friday_val1);
     }
     elseif ($from_friday1=="No Condition") {
       $from_friday_val1="00:00";
       $factor_friday1=00;
     }
     //from_saturday
     if($from_saturday1=='Time')
     {
        $from_saturday_val1=Input::get('from_saturday1');
     }
     elseif ($from_saturday1=='Hours') {
       $from_saturday_val1=Input::get('from_saturday111');
       $from_saturday_val1=FileUpload::replaceDotToColon($from_saturday_val1);
     }
     elseif ($from_saturday1=="No Condition") {
       $from_saturday_val1="00:00";
       $factor_saturday1=00;
     }
     //from_sunday
     if($from_sunday1=='Time')
     {
        $from_sunday_val1=Input::get('from_sunday1');
     }
     elseif ($from_sunday1=='Hours') {
       $from_sunday_val1=Input::get('from_sunday111');
       $from_sunday_val1=FileUpload::replaceDotToColon($from_sunday_val1);
     }
     elseif ($from_sunday1=="No Condition") {
       $from_sunday_val1="00:00";
       $factor_sunday1=00;
     }

    $monday_condtion_details=array( 
                             'companyid'=>$company['-id'],
                             'id'=>Input::get('monday_condtion_id'),
                             'condtion'=>Input::get('conditions_monday'),
                             'from'=>$from_monday_val,
                             'factor'=>$factor_monday,
                          );
  /*  return $monday_condtion_details;*/
    $monday_condtion_details1=array( 'companyid'=>$company['-id'],
                              'id'=>Input::get('monday_condtion_id1'),
                              'condtion'=>Input::get('conditions_monday1'),
                             'from'=>$from_monday_val1,
                             'factor'=>$factor_monday1,
                          );
    $tuesday_condtion_details=array( 'condtion'=>Input::get('conditions_tuesday'),
                             'id'=>Input::get('tuesday_condtion_id'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_tuesday_val,
                             'factor'=>$factor_tuesday,
                          );
    $tuesday_condtion_details1=array( 'condtion'=>Input::get('conditions_tuesday1'),
                             'id'=>Input::get('tuesday_condtion_id1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_tuesday_val1,
                             'factor'=>$factor_tuesday1,
                          );
    $wednesday_condtion_details=array( 'condtion'=>Input::get('conditions_wednesday'),
                             'id'=>Input::get('wednesday_condtion_id'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_wednesday_val,
                             'factor'=>$factor_wednesday,
                          );
    $wednesday_condtion_details1=array( 'condtion'=>Input::get('conditions_wednesday1'),
                             'id'=>Input::get('wednesday_condtion_id1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_wednesday_val1,
                             'factor'=>$factor_wednesday1,
                          );
    $thursday_condtion_details=array( 'condtion'=>Input::get('conditions_thursday'),
                             'id'=>Input::get('thursday_condtion_id'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_thursday_val,
                             'factor'=>$factor_thursday,
                          );
    $thursday_condtion_details1=array( 'condtion'=>Input::get('conditions_thursday1'),
                             'id'=>Input::get('thursday_condtion_id1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_thursday_val1,
                             'factor'=>$factor_thursday1,
                          );
    $friday_condtion_details=array( 'condtion'=>Input::get('conditions_friday'),
                             'id'=>Input::get('friday_condtion_id'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_friday_val,
                             'factor'=>$factor_friday,
                          );
    $friday_condtion_details1=array( 'condtion'=>Input::get('conditions_friday1'),
                             'id'=>Input::get('friday_condtion_id1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_friday_val1,
                             'factor'=>$factor_friday1,
                          );
    $saturday_condtion_details=array( 'condtion'=>Input::get('conditions_saturday'),
                             'id'=>Input::get('saturday_condtion_id'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_saturday_val,
                             'factor'=>$factor_saturday,
                          );
    $saturday_condtion_details1=array( 'condtion'=>Input::get('conditions_saturday1'),
                             'id'=>Input::get('saturday_condtion_id1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_saturday_val1,
                             'factor'=>$factor_saturday1,
                          );
    $sunday_condtion_details=array( 'condtion'=>Input::get('conditions_sunday'),
                             'id'=>Input::get('sunday_condtion_id'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_sunday_val,
                             'factor'=>$factor_sunday,
                          );
    $sunday_condtion_details1=array( 'condtion'=>Input::get('conditions_sunday1'),
                             'id'=>Input::get('sunday_condtion_id1'),
                             'companyid'=>$company['-id'],
                             'from'=>$from_sunday_val1,
                             'factor'=>$factor_sunday1,
                          );
    $condtion=array(
                '0'=>array('0'=>$monday_condtion_details,
                           '1'=>$monday_condtion_details1,),
                '1'=>array('0'=>$tuesday_condtion_details,
                           '1'=>$tuesday_condtion_details1,),
                '2'=>array('0'=>$wednesday_condtion_details,
                           '1'=>$wednesday_condtion_details1,),
                '3'=>array('0'=>$thursday_condtion_details,
                           '1'=>$thursday_condtion_details1,),
                '4'=>array('0'=>$friday_condtion_details,
                           '1'=>$friday_condtion_details1,),
                '5'=>array('0'=>$saturday_condtion_details,
                           '1'=>$saturday_condtion_details1,),
                '6'=>array('0'=>$sunday_condtion_details,
                           '1'=>$sunday_condtion_details1,),
                );
  /*  return $condtion;
*/    $companyid=$company['-id'];
  /*  return $set_working_data;*/
    $result=WorkingDaysSetting::updateWorkingData($set_working_data,$condtion,$companyid);
      return $result;
  }

  //set general setting value
  public static function setGeneralSetting()
  {
     $company=Auth::user();
/*     function replaceDotToColon($val)
    {
    if (strpos($val, '.') !== FALSE)
    {
      return str_replace('.',':',$val);
    }
    elseif(strpos($val, ',') !== FALSE)
    {
      return str_replace(',',':',$val);
    }
    else
    {
      return $val;
    }
    }*/
    $updateAnnualAllowance=Input::get('update_annual_allowance');
    if ($updateAnnualAllowance==true) 
    {
      $setWorkingHours=FileUpload::replaceDotToColon(Input::get('set_working_hours'));
      $user=$company['-id'];
      $updateAnnualAllowanceInActivity=MotActivity::updateAnnualAllowanceInActivity($setWorkingHours,$user);
    }

   $general_setting_details = array('timpiker_increments'=>Input::get('timpiker'),
                      'time_style'=>Input::get('time_style'),
                      'companyid'=>$company['-id'],
                      'general_setting_activity'=>Input::get('general_setting_activity'),
                      'value_format'=>Input::get('value_format'),
                      'set_working_hours'=>FileUpload::replaceDotToColon(Input::get('set_working_hours')),
                      );
   $result=GeneralSetting::setGeneralSetting($general_setting_details);
   return $result;
  }

  // update updateGeneralSetting
  public static function updateGeneralSetting()
  {
    $company=Auth::user();
   /*  function replaceDotToColon($val)
    {
    if (strpos($val, '.') !== FALSE)
    {
      return str_replace('.',':',$val);
    }
    elseif(strpos($val, ',') !== FALSE)
    {
      return str_replace(',',':',$val);
    }
    else
    {
      return $val;
    }
    }*/
    $updateAnnualAllowance=Input::get('update_annual_allowance');
    if ($updateAnnualAllowance==true) 
    {
      $setWorkingHours=FileUpload::replaceDotToColon(Input::get('set_working_hours'));
      $user=$company['-id'];

     $updateAnnualAllowanceInActivity=MotActivity::updateAnnualAllowanceInActivity($setWorkingHours,$user);
    }

    $general_setting_details = array('start_interval'=>Input::get('start_interval'),
                      'end_interval'=>Input::get('end_interval'),
                      'start_rounding_option'=>Input::get('start_rounding_option'),
                      'end_rounding_option'=>Input::get('end_rounding_option'),
                      'time_style'=>Input::get('time_style'),
                      'companyid'=>$company['-id'],
                      'general_setting_activity'=>Input::get('general_setting_activity'),
                      'value_format'=>Input::get('value_format'),
                      'set_working_hours'=>FileUpload::replaceDotToColon(Input::get('set_working_hours')),
                      );
   $result=GeneralSetting::updateGeneralSetting($general_setting_details);
   return $result;
  }

  //setOvertimeHandling
  public static function setOvertimeHandling()
  {
    $company=Auth::user();
   $overtime_handling_setting_details = array('overtime_for_period'=>Input::get('overtime_for_period'),
                      'threshold_hours'=>Input::get('threshold_hours'),
                      'hourly_rate'=> FileUpload::replaceDotToComma(Input::get('hourly_rate')),
                      'clear_overtime_balance'=>Input::get('clear_overtime_balance'),
                      'companyid'=>$company['-id']
                      );
   /*return $overtime_handling_setting_details;*/
   $result=OvertimeHandling::setOvertimeHandling($overtime_handling_setting_details);
   return $result;
  }

  //updateOvertimeHandling
  public static function updateOvertimeHandling()
  {
    $company=Auth::user();
   $overtime_handling_setting_details = array('overtime_for_period'=>Input::get('overtime_for_period'),
                      'threshold_hours'=>Input::get('threshold_hours'),
                      'hourly_rate'=> FileUpload::replaceDotToComma(Input::get('hourly_rate')),
                      'clear_overtime_balance'=>Input::get('clear_overtime_balance'),
                      'companyid'=>$company['-id']
                      );
   /*return $overtime_handling_setting_details;*/
   $result=OvertimeHandling::updateOvertimeHandling($overtime_handling_setting_details);
   return $result;
  }

  //setSetAnnualAllowance
  public static function setSetAnnualAllowance()
  {
    $company=Auth::user();
    $valueType=Input::get('total_in_type');
    $activityId=Input::get('set_annual_allowance_activity');
    $totalInValue='';
    $totalInCalculatedValue='';
    $totalInDays='';
    $totalInHours='';
    if ($valueType=='Hours') {
      $totalInValue=Input::get('total_in_value1');
      $totalInCalculatedValue= FileUpload::replaceDotToComma(Input::get('total_in_calculated_value'));
      $totalInDays=$totalInCalculatedValue;
      $totalInHours=$totalInValue;
    }
    else
    {
      $totalInValue= FileUpload::replaceDotToComma(Input::get('total_in_value'));
      $totalInCalculatedValue=Input::get('total_in_calculated_value');
      $totalInDays=$totalInValue;
      $totalInHours=$totalInCalculatedValue;
    }
    $annualAllowanceForActivity=array('allowance_bank_hours'=>$totalInHours,
                                      'allowance_bank_days'=>number_format((float)$totalInDays, 2, '.', ''));
    //update allowance for activity
    $updateAnnualAllowanceByActivityId=MotActivity::updateAnnualAllowanceByActivityId($activityId,$annualAllowanceForActivity);
    if ($updateAnnualAllowanceByActivityId['status']=='success') {
        $set_annual_allowance_setting_details = array('set_annual_allowance_activity'=>$activityId,
                        'total_in_type'=>$valueType,
                        'total_in_days'=>$totalInDays,
                        'total_in_hours'=>$totalInHours,
                        'companyid'=>$company['-id'],
                        'total_in_value'=>$totalInValue,
                        'total_in_calculated_value'=>$totalInCalculatedValue
                        );
     $result=SetAnnualAllowance::setSetAnnualAllowance($set_annual_allowance_setting_details);
     return $result;
    }
    else
    {
      $response = array('status' => 'failure');
      return $response;
    }
  }

  //updateSetAnnualAllowance
  public static function updateSetAnnualAllowance()
  {
    $company=Auth::user();
    $valueType=Input::get('total_in_type');
    $activityId=Input::get('set_annual_allowance_activity');
    $totalInValue='';
    $totalInCalculatedValue='';
    $totalInDays='';
    $totalInHours='';
    if ($valueType=='Hours') {
      $totalInValue=Input::get('total_in_value1');
      $totalInCalculatedValue= FileUpload::replaceDotToComma(Input::get('total_in_calculated_value'));
      $totalInDays=$totalInCalculatedValue;
      $totalInHours=$totalInValue;
    }
    else
    {
      $totalInValue= FileUpload::replaceDotToComma(Input::get('total_in_value'));
      $totalInCalculatedValue=Input::get('total_in_calculated_value');
      $totalInDays=$totalInValue;
      $totalInHours=$totalInCalculatedValue;
    }
    $annualAllowanceForActivity=array('allowance_bank_hours'=>$totalInHours,
                                      'allowance_bank_days'=>number_format((float)$totalInDays, 2, '.', ''));
    //update allowance for activity
    $updateAnnualAllowanceByActivityId=MotActivity::updateAnnualAllowanceByActivityId($activityId,$annualAllowanceForActivity);
    if ($updateAnnualAllowanceByActivityId['status']=='success') {
        $set_annual_allowance_setting_details = array('set_annual_allowance_activity'=>$activityId,
                        'total_in_type'=>$valueType,
                        'total_in_days'=>$totalInDays,
                        'total_in_hours'=>$totalInHours,
                        'companyid'=>$company['-id'],
                        'total_in_value'=>$totalInValue,
                        'total_in_calculated_value'=>$totalInCalculatedValue
                        );
     $result=SetAnnualAllowance::updateSetAnnualAllowance($set_annual_allowance_setting_details);
     return $result;
    }
    else
    {
      $response = array('status' => 'failure');
      return $response;
    }
  }

 //set general setting value in Admin
  public static function setGeneralSettingInAdmin()
  {
     $company=Auth::user();
/*     function replaceDotToColon($val)
    {
    if (strpos($val, '.') !== FALSE)
    {
      return str_replace('.',':',$val);
    }
    elseif(strpos($val, ',') !== FALSE)
    {
      return str_replace(',',':',$val);
    }
    else
    {
      return $val;
    }
    }*/
    $updateAnnualAllowance=Input::get('update_annual_allowance');
    if ($updateAnnualAllowance==true) 
    {
      $setWorkingHours=FileUpload::replaceDotToColon(Input::get('set_working_hours'));
      $user=$company['-id'];
      $updateAnnualAllowanceInActivity=MotAdminActivity::updateAnnualAllowanceInActivity($setWorkingHours,$user);
    }

   $general_setting_details = array('timpiker_increments'=>Input::get('timpiker'),
                      'time_style'=>Input::get('time_style'),
                      'companyid'=>$company['-id'],
                      'general_setting_activity'=>Input::get('general_setting_activity'),
                      'value_format'=>Input::get('value_format'),
                      'set_working_hours'=>FileUpload::replaceDotToColon(Input::get('set_working_hours')),
                      );
   $result=GeneralSetting::setGeneralSetting($general_setting_details);
   return $result;
  }

  // update updateGeneralSetting in Admin
  public static function updateGeneralSettingInAdmin()
  {
    $company=Auth::user();
   /*  function replaceDotToColon($val)
    {
    if (strpos($val, '.') !== FALSE)
    {
      return str_replace('.',':',$val);
    }
    elseif(strpos($val, ',') !== FALSE)
    {
      return str_replace(',',':',$val);
    }
    else
    {
      return $val;
    }
    }*/
    $updateAnnualAllowance=Input::get('update_annual_allowance');
    if ($updateAnnualAllowance==true) 
    {
      $setWorkingHours=FileUpload::replaceDotToColon(Input::get('set_working_hours'));
      $user=$company['-id'];
      $updateAnnualAllowanceInActivity=MotAdminActivity::updateAnnualAllowanceInActivity($setWorkingHours,$user);
      return $updateAnnualAllowanceInActivity;
    }
   $general_setting_details = array('timpiker_increments'=>Input::get('timpiker'),
                      'time_style'=>Input::get('time_style'),
                      'companyid'=>$company['-id'],
                      'general_setting_activity'=>Input::get('general_setting_activity'),
                      'value_format'=>Input::get('value_format'),
                      'set_working_hours'=>FileUpload::replaceDotToColon(Input::get('set_working_hours')),
                      );
   $result=GeneralSetting::updateGeneralSetting($general_setting_details);
   return $result;
  }
  //setSetAnnualAllowance for super admin
  public static function setSetAnnualAllowanceInAdmin()
  {
    $company=Auth::user();
    $valueType=Input::get('total_in_type');
    $activityId=Input::get('set_annual_allowance_activity');
    $totalInValue='';
    $totalInCalculatedValue='';
    $totalInDays='';
    $totalInHours='';
    if ($valueType=='Hours') {
      $totalInValue=Input::get('total_in_value1');
      $totalInCalculatedValue= FileUpload::replaceDotToComma(Input::get('total_in_calculated_value'));
      $totalInDays=$totalInCalculatedValue;
      $totalInHours=$totalInValue;
    }
    else
    {
      $totalInValue= FileUpload::replaceDotToComma(Input::get('total_in_value'));
      $totalInCalculatedValue=Input::get('total_in_calculated_value');
      $totalInDays=$totalInValue;
      $totalInHours=$totalInCalculatedValue;
    }
    $annualAllowanceForActivity=array('allowance_bank_hours'=>$totalInHours,
                                      'allowance_bank_days'=>number_format((float)$totalInDays, 2, '.', ''));
    //update allowance for activity
    $updateAnnualAllowanceByActivityId=MotAdminActivity::updateAnnualAllowanceByActivityId($activityId,$annualAllowanceForActivity);
    if ($updateAnnualAllowanceByActivityId['status']=='success') {
        $set_annual_allowance_setting_details = array('set_annual_allowance_activity'=>$activityId,
                        'total_in_type'=>$valueType,
                        'total_in_days'=>$totalInDays,
                        'total_in_hours'=>$totalInHours,
                        'companyid'=>$company['-id'],
                        'total_in_value'=>$totalInValue,
                        'total_in_calculated_value'=>$totalInCalculatedValue
                        );
     $result=SetAnnualAllowance::setSetAnnualAllowance($set_annual_allowance_setting_details);
     return $result;
    }
    else
    {
      $response = array('status' => 'failure');
      return $response;
    }
  }

  //updateSetAnnualAllowance in super admin
  public static function updateSetAnnualAllowanceInAdmin()
  {
    $company=Auth::user();
    $valueType=Input::get('total_in_type');
    $activityId=Input::get('set_annual_allowance_activity');
    $totalInValue='';
    $totalInCalculatedValue='';
    $totalInDays='';
    $totalInHours='';
    if ($valueType=='Hours') {
      $totalInValue=Input::get('total_in_value1');
      $totalInCalculatedValue= FileUpload::replaceDotToComma(Input::get('total_in_calculated_value'));
      $totalInDays=$totalInCalculatedValue;
      $totalInHours=$totalInValue;
    }
    else
    {
      $totalInValue= FileUpload::replaceDotToComma(Input::get('total_in_value'));
      $totalInCalculatedValue=Input::get('total_in_calculated_value');
      $totalInDays=$totalInValue;
      $totalInHours=$totalInCalculatedValue;
    }
    $annualAllowanceForActivity=array('allowance_bank_hours'=>$totalInHours,
                                      'allowance_bank_days'=>number_format((float)$totalInDays, 2, '.', ''));
    //update allowance for activity
    $updateAnnualAllowanceByActivityId=MotAdminActivity::updateAnnualAllowanceByActivityId($activityId,$annualAllowanceForActivity);
    if ($updateAnnualAllowanceByActivityId['status']=='success') {
        $set_annual_allowance_setting_details = array('set_annual_allowance_activity'=>$activityId,
                        'total_in_type'=>$valueType,
                        'total_in_days'=>$totalInDays,
                        'total_in_hours'=>$totalInHours,
                        'companyid'=>$company['-id'],
                        'total_in_value'=>$totalInValue,
                        'total_in_calculated_value'=>$totalInCalculatedValue
                        );
     $result=SetAnnualAllowance::setSetAnnualAllowance($set_annual_allowance_setting_details);
     return $result;
    }
    else
    {
      $response = array('status' => 'failure');
      return $response;
    }
  }

  //lisitng timesheet by dates for overtime report
  public static function listingTimesheetWithRangeForOvertimeReport($cid,$eid,$from,$to)
  {
     $balanceAtRangeStart=0;
    $balanceAtRangeStart=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->orderBy('date', 'asc')->get();
     //get balanceAtRangeStart
  if(count($balanceAtRangeStart)>0)
      {
      $from1=date("Y-m-d", strtotime($from));
      $balanceAtRangeStartDate=$balanceAtRangeStart[0]['date'];
      $balanceAtRangeStartDate=date("Y-m-d", strtotime($balanceAtRangeStartDate));
      $balanceAtRangeStartDateEnd=date_sub(date_create($from),date_interval_create_from_date_string("1 days"));
      $balanceAtRangeStartDateEnd=date_format($balanceAtRangeStartDateEnd,"Y-m-d");
      if($from1<$balanceAtRangeStart[0]['date'] || $from1==$balanceAtRangeStart[0]['date'])
        {
          $balanceAtRangeStart=0;
        }
      else
        {
          $range=Timesheet::groupBy('date')->where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date',[$balanceAtRangeStartDate,$balanceAtRangeStartDateEnd])->get();
          $statistics='';
          $balance='';
          $offset='';
          for ($i=0; $i <sizeof($range); $i++) { 
          $statistics=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->where('date',$range[$i]['date'])->get();
          $result=array();
          $total_hours_min='';
          for ($j=0; $j < sizeof($statistics); $j++) 
          { 
          $total_hours_min=$statistics[0]['TSHrsTotal'];
          $offset=$statistics[$j]['offset_min'];
          }
          $balance=$balance+$statistics[0]['TSDailyBalnce'];
          }      
          $balanceAtRangeStart= $balance;
        }
      }
      else
      {
          $balanceAtRangeStart=0;
      }
   $fromNew=date("Y-m-d", strtotime($from));
   $toNew=date("Y-m-d", strtotime($to));
    $range=Timesheet::listingTimesheetWithRangeForOvertimeReport($cid,$eid,$fromNew,$toNew);
    $result=array();
    $myovertime=0;
    $finalTotalHours=0;
    $finalOffset=0;
    $balanceAtRangeEnd=0;
    for ($i=0; $i < sizeof($range) ; $i++) { 
      $totalHours=0;
      $offset=0;
      $balance=0;
      for ($j=0; $j < sizeof($range[$i]); $j++) { 
         $totalHours=$range[$i][0]['TSHrsTotal'];
         $offset=$range[$i][$j]['offset_min'];
         $date=$range[$i][$j]['date'];
         $balance=$range[$i][0]['TSDailyBalnce'];
        }
        $totalHoursfinal=$totalHours;
        $offsetfinal=$offset;
        $myovertime=$myovertime+$balance;
        $myovertime1=$balanceAtRangeStart+$myovertime;
        $result[$i][]=$date;
        $finalTotalHours=$finalTotalHours+$totalHours;//total hours final value
        $finalOffset=$finalOffset+$offset;//total offset final value
      array_push($result[$i],$totalHoursfinal,$offsetfinal,$balance,$myovertime1);
      }
      $balanceAtRangeEnd=$balanceAtRangeStart+$myovertime;
      $total=array('finalTotalHours' =>$finalTotalHours,
                   'finalOffset' =>$finalOffset,
                   'finalBalance' =>$myovertime,
                   'balanceAtRangeEnd'=>$balanceAtRangeEnd,
                   'balanceAtRangeStart'=>$balanceAtRangeStart
                  );
      $response = array('result' =>$result,
                        'total' =>$total);
      return $response;
  }

   //lisitng timesheet by dates
  public static function listingTimesheetWithRange($cid,$eid,$from,$to)
  {
    $balanceAtRangeStart=0;
    $balanceAtRangeStart=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->orderBy('date', 'asc')->get();

     //get balanceAtRangeStart
  if(count($balanceAtRangeStart)>0)
      {
      $from1=date("Y-m-d", strtotime($from));
      $balanceAtRangeStartDate=$balanceAtRangeStart[0]['date'];
      $balanceAtRangeStartDate=date("Y-m-d", strtotime($balanceAtRangeStartDate));
      $balanceAtRangeStartDateEnd=date_sub(date_create($from),date_interval_create_from_date_string("1 days"));
      $balanceAtRangeStartDateEnd=date_format($balanceAtRangeStartDateEnd,"Y-m-d");
      if($from1<$balanceAtRangeStart[0]['date'] || $from1==$balanceAtRangeStart[0]['date'])
        {
          $balanceAtRangeStart=0;
        }
      else
        {
          $range=Timesheet::groupBy('date')->where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date',[$balanceAtRangeStartDate,$balanceAtRangeStartDateEnd])->get();
          $statistics='';
          $balance='';
          $offset='';
          for ($i=0; $i <sizeof($range); $i++) { 
          $statistics=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->where('date',$range[$i]['date'])->get();;
          $result=array();
          $total_hours_min='';
          for ($j=0; $j < sizeof($statistics); $j++) 
          { 
          $total_hours_min=$statistics[0]['TSHrsTotal'];
          $offset=$statistics[$j]['offset_min'];
          }
          $balance=$balance+$statistics[0]['TSDailyBalnce'];
          }      
          $balanceAtRangeStart= $balance;
        }
      }
      else
      {
          $balanceAtRangeStart=0;
      }
   $timeStyle='';
    $offsetOfCompany='';
    $companySetting=GeneralSetting::where('companyid',$cid)->get();
    if(!empty($companySetting))
    {
    $timeStyle=$companySetting[0]['time_style'];
    /*return $timeStyle;*/
    function hoursToMinutes($hours,$timeStyle) 
    { 
    $minutes = 0;
    if (strpos($hours, ':') !== false) 
    { 
    // Split hours and minutes. 
    list($hours, $minutes) = explode(':', $hours); 
    } 
    return $hours * 60 + $minutes; 
    }  
    $offsetOfCompany=hoursToMinutes($companySetting[0]['set_working_hours'],$timeStyle);
    }
   $fromNew=date("Y-m-d", strtotime($from));
   $toNew=date("Y-m-d", strtotime($to));
   /*return $offsetOfCompany;*/
    $range=Timesheet::listingTimesheetWithRange($cid,$eid,$fromNew,$toNew);
   /* return $range;*/
   $array=array();
    for ($i=0; $i <sizeof($range) ; $i++) { 
      $range1=Timesheet::listingTimesheetWithRangeDate($range[$i]['date'],$cid,$eid);
      array_push($array, $range1);
    }
/*    return $array;*/
$babin=array();
    $finalTotalHoursInDay=0;
    $finalOffsetInDay=0;
    $balanceInDay=0;
    $finalTotalHoursInDay1=0;
    $finalOffsetInDay1=0;
    $balanceInDay1=0;
    $finalAmount=0;
    $balanceAtRangeEnd=0;
    $ownAccountTotal=0;
    for ($j=0; $j <sizeof($array) ; $j++) 
    { 

    $finalTotalHours=0;
    $finalOffset=0;
    $balance=0;
    $amount=0;
    $ownAccountSubTotal=0;
      for ($k=0; $k <sizeof($array[$j]) ; $k++) { 
        /*echo $array[$j][$k]['date']."/";*/
        $finalTotalHours=$array[$j][0]['TSHrsTotal'];
        $finalOffset=$array[$j][$k]['offset_min'];
        $amount=$amount+$array[$j][$k]['amount'];
        $ownAccountSubTotal=$ownAccountSubTotal+str_replace(":",".",$array[$j][$k]['own_account']);
        /*$balance=$balance+$array[$j][$k]['total_hours_min']-$array[$j][$k]['offset_min'];*/
      }
      $balance=$finalTotalHours-$finalOffset;
    $total=array( 'timesheetDetail'=> $array[$j],
                  'finalTotalHours' =>$finalTotalHours,
                   'finalOffset' =>$finalOffset,
                   'finalBalance' =>$balance,
                   'amount'=>$amount,
                   'sub_ownaccount'=>$ownAccountSubTotal,
                  );
    array_push($babin,$total);
    $finalTotalHoursInDay= $finalTotalHoursInDay+$finalTotalHours;
    $finalOffsetInDay=$finalOffsetInDay+$finalOffset;
    $balanceInDay= $balanceInDay+$balance;
    $finalAmount=$finalAmount+$amount;
    $ownAccountTotal=$ownAccountTotal+$ownAccountSubTotal;
    }
    $balanceAtRangeEnd=$balanceAtRangeStart+$balanceInDay;
    $finalTotalHoursInDay1= $finalTotalHoursInDay/$offsetOfCompany;
    $finalOffsetInDay1=$finalOffsetInDay/$offsetOfCompany;
    $ownAccountTotalFinal=$ownAccountTotal/$offsetOfCompany;
    $balanceInDay1= $balanceInDay/$offsetOfCompany;
    $total1=array('finalTotalHours' =>$finalTotalHoursInDay,
               'finalOffset' =>$finalOffsetInDay,
               'finalBalance' =>$balanceInDay,
                'finalTotalHoursInDay' =>$finalTotalHoursInDay1,
                'ownAccountTotalFinal'=>$ownAccountTotalFinal,
               'finalOffsetInDay' =>$finalOffsetInDay1,
               'finalBalanceInDay' =>$balanceInDay1,
               'finalAmount'=>$finalAmount,
               'balanceAtRangeStart'=>$balanceAtRangeStart,
               'balanceAtRangeEnd'=>$balanceAtRangeEnd,
               'ownAccountTotal'=>$ownAccountTotal,
              );
    $res=array('TotalTimesheetDetail'=>$babin,
            'whole_total'=>$total1);
    return $res;
  }

  //lisitng timesheet for activity in detailed report
  public static function listingTimesheetForActivity()
  {
    $timeStyle='';
    $offsetOfCompany='';
    $cid=Auth::user();
    $cid=$cid['-id'];
    $companySetting=GeneralSetting::where('companyid',$cid)->get();
    if(!empty($companySetting))
    {
    $timeStyle=$companySetting[0]['time_style'];
    /*return $timeStyle;*/
    function hoursToMinutes($hours) 
    { 
    $minutes = 0;
    if (strpos($hours, ':') !== false) 
    { 
    // Split hours and minutes. 
    list($hours, $minutes) = explode(':', $hours); 
    } 
    return $hours * 60 + $minutes; 
    } 
    $offsetOfCompany=hoursToMinutes($companySetting[0]['set_working_hours']);
    }
    $array=[];
    $activity=Input::get('data');
    $date=Input::get('date');
    $array=array();
    for ($i=0; $i < sizeof($activity) ; $i++) 
    { 
      $timesheet=Timesheet::listingTimesheetForActivity($activity[$i],$date);
      array_push($array,$timesheet);
    }
   $response=array_flatten($array);
    $finalTotalHours=0;
    $finalOffset=0;
    $finalOwnAccount=0;
    $balance=0;
    $amount=0;
    $own_account=0;
    for ($i=0; $i <sizeof($response) ; $i++) { 
        $finalTotalHours=$finalTotalHours+$response[$i]['total_hours_min'];
        $finalOffset=$finalOffset+$response[$i]['offset_min'];
        $balance=$balance+$response[$i]['total_hours_min']-$response[$i]['offset_min'];
        $amount=$amount+$response[$i]['amount'];
        $own_account=$own_account+$response[$i]['own_account'];
    }
    $finalTotalHoursInDay=$finalTotalHours/$offsetOfCompany;
    $finalOffsetInDay=$finalOffset/$offsetOfCompany;
    $balanceInDay=$balance/$offsetOfCompany;
    $finalOffsetInDay=$finalOffset/$offsetOfCompany;
    $finalOwnAccount=$own_account/$offsetOfCompany;
    $total=array('finalTotalHours' =>$finalTotalHours,
                  'own_account'=>$own_account,
                   'finalOffset' =>$finalOffset,
                   'finalBalance' =>$balance,
                   'finalTotalHoursInDay' =>$finalTotalHoursInDay,
                   'finalOffsetInDay' =>$finalOffsetInDay,
                   'finalBalanceInDay' =>$balanceInDay,
                   'finalOwnAccount' =>$finalOwnAccount,
                   'amount'=>$amount,
                  );
    $response1 = array('result' =>$response,
                        'total' =>$total);
      return $response1;
    //return multidimensional into single dimensional array
    function array_flatten($array) { 
      if (!is_array($array)) { 
        return FALSE; 
      } 
      $result = array(); 
      foreach ($array as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, array_flatten($value)); 
        } 
        else { 
          $result[$key] = $value; 
        } 
      } 
      return $result; 
    } 

  }
  //lisitng timesheet by dates for statistics
  public static function listingTimesheetWithRangeForStatistics($cid,$eid,$from,$to)
  {
    /*$fromNew = date("Y-m-d", strtotime($from));
    $toNew = date("Y-m-d", strtotime($to));*/
    /*return $fromNew;*/
    $selectedFromDate=$from;
    $selectedToDate=$to;
    $start_date = $from;
    $last_date = $to;
    $timestamp=strtotime($start_date);
    $startDate='';
    if(date('D', $timestamp) === 'Mon') 
    {
      $startDate=strtotime($start_date);
    }
    else
    {
      $startDate=strtotime('previous monday',$timestamp);
    }
    $dates = range($startDate, strtotime($last_date),86400);
    $days = array('monday' => array());

    array_map(function($v)use(&$days){
        if(date('D', $v) == 'Mon'){
            $days['monday'][] = date('Y-m-d', $v);
        }
    }, $dates); // Requires PHP 5.3+
    $statistics="";
    for($i=0;$i<sizeof($days['monday']);$i++)
    {
      $from=$days['monday'][$i];
      $to = strtotime($from);
      $to = strtotime("+6 day", $to);
      $to = date('Y-m-d', $to);
      $week = new DateTime($from);
      $week_number = $week->format("W");
      $dispalyFrom = strtotime($from);
      $dispalyFrom = date('d-m-Y', $dispalyFrom);
      $dispalyTo = strtotime($to);
      $dispalyTo = date('d-m-Y', $dispalyTo);
      $from_to=trans('translate.week')." : ".$week_number." (".$dispalyFrom." ".trans('translate.to')." ".$dispalyTo.") ";//Week : 11 (1/1/2014 to 7/1/2014)
      $range_time1=Timesheet::listingTimesheetWithRangeForStatistics($cid,$eid,$from,$to,$selectedFromDate,$selectedToDate);
      $statistics[$i][]=$range_time1;
      array_push($statistics[$i],$from_to);
    }
    /*return $from_to;*/
    return $statistics;
  }

  //lisitng timesheet by dates for statistics
  public static function listingTimesheetForStatisticsMonths($cid,$eid,$from,$to)
  {
    /*$start_date = $from;
    $last_date = $to;*/
    $selectedFromDate=$from;
    $selectedToDate=$to;
    $fromNew = date("Y-m-d", strtotime($from));
    $toNew = date("Y-m-d", strtotime($to));
    $start    = new DateTime($fromNew );
    $start->modify('first day of this month');
    $end      = new DateTime($toNew);
    $end->modify('first day of next month');
    $interval = DateInterval::createFromDateString('1 month');
    $period   = new DatePeriod($start, $interval, $end);
    $month = ['months'];
    foreach ($period as $dt) {
      /*$month['months'][]=*/
      $month['months'][]=$dt->format("M,Y");
    }
    /*return $month;*/
    for($i=0;$i<sizeof($month['months']);$i++)
    {
     /* echo $month['months'][$i];*/
      $monthData= $month['months'][$i];
      $monthName = explode(",", $monthData);
      $monthTrans='translate.'.$monthName[0];
      $monthYear=$monthName[1];
      $from_to =trans($monthTrans).', '.$monthYear;
      // First day of the month.
      $start_date = date('Y-m-01', strtotime($from_to));
      // Last day of the month.
      $end_date = date('Y-m-t', strtotime($from_to));
      /*echo $start_date."/".$end_date."||";*/
      $range_time1=Timesheet::listingTimesheetWithRangeForStatistics($cid,$eid,$start_date,$end_date,$selectedFromDate,$selectedToDate);
      $statistics[$i][]=$range_time1;
      array_push($statistics[$i],$from_to);
    }
    return $statistics;
  }

  //lisitng timesheet by dates for statistics
  public static function listingTimesheetForStatisticsYearly($cid,$eid,$from,$to)
  {
    /*$start_date = $from;
    $last_date = $to;*/
    $selectedFromDate=$from;
    $selectedToDate=$to;
    $fromNew = date("Y-m-d", strtotime($from));
    $toNew = date("Y-m-d", strtotime($to));
    $year=date("Y",strtotime($fromNew));
    $year1=date("Y",strtotime($toNew));
    $array=array();
    while($year!=$year1)
    {
    array_push($array,$year);
    $year++;
    }
    array_push($array,$year1);
    for($i=0;$i<sizeof($array);$i++)
    {
      // First day of the month.
      $start_date = date("$array[$i]-01-01",strtotime($array[$i]));
      // Last day of the month.
      $end_date = date("$array[$i]-12-31",strtotime($array[$i]));
      $range_time1=Timesheet::listingTimesheetWithRangeForStatistics($cid,$eid,$start_date,$end_date,$selectedFromDate,$selectedToDate);
      $statistics[$i][]=$range_time1;
      array_push($statistics[$i],$array[$i]);
    }
    return $statistics;
  }

  //lisitng timesheet by dates for graoh report
  public static function listingTimesheetWithRangeForGraph($cid,$eid,$from,$to)
  {
    /*$fromNew = date("Y-m-d", strtotime($from));
    $toNew = date("Y-m-d", strtotime($to));*/
    /*return $fromNew;*/
    $selectedFromDate=$from;
    $selectedToDate=$to;
    $start_date = $from;
    $last_date = $to;
    $timestamp=strtotime($start_date);
    $startDate='';
    if(date('D', $timestamp) === 'Mon') 
    {
      $startDate=strtotime($start_date);
    }
    else
    {
      $startDate=strtotime('previous monday',$timestamp);
    }
    $dates = range($startDate, strtotime($last_date),86400);
    $days = array('monday' => array());

    array_map(function($v)use(&$days){
        if(date('D', $v) == 'Mon'){
            $days['monday'][] = date('Y-m-d', $v);
        }
    }, $dates); // Requires PHP 5.3+
    $statistics="";
    for($i=0;$i<sizeof($days['monday']);$i++)
    {
      $from=$days['monday'][$i];
      $to = strtotime($from);
      $to = strtotime("+6 day", $to);
      $to = date('Y-m-d', $to);
      $week = new DateTime($from);
      $week_number = $week->format("W");
      $dispalyFrom = strtotime($from);
      $dispalyFrom = date('d-m-Y', $dispalyFrom);
      $dispalyTo = strtotime($to);
      $dispalyTo = date('d-m-Y', $dispalyTo);
      $from_to=trans('translate.week')." : ".$week_number." (".$dispalyFrom." ".trans('translate.to')." ".$dispalyTo.")";//Week : 11 (1/1/2014 to 7/1/2014)
      $range_time1=Timesheet::listingTimesheetWithRangeForStatistics($cid,$eid,$from,$to,$selectedFromDate,$selectedToDate);
      $statistics[$i][]=$range_time1;
      array_push($statistics[$i],$from_to);
    }
    /*return $from_to;*/
    return $statistics;
  }

  //lisitng timesheet by dates for graph report
  public static function listingTimesheetForGraphMonths($cid,$eid,$from,$to)
  {
    /*$start_date = $from;
    $last_date = $to;*/
    $selectedFromDate=$from;
    $selectedToDate=$to;
    $fromNew = date("Y-m-d", strtotime($from));
    $toNew = date("Y-m-d", strtotime($to));
    $start    = new DateTime($fromNew );
    $start->modify('first day of this month');
    $end      = new DateTime($toNew);
    $end->modify('first day of next month');
    $interval = DateInterval::createFromDateString('1 month');
    $period   = new DatePeriod($start, $interval, $end);
    $month = ['months'];
    foreach ($period as $dt) {
      $month['months'][]=$dt->format("M,Y");
    }
    /*return $month;*/
    for($i=0;$i<sizeof($month['months']);$i++)
    {
     /* echo $month['months'][$i];*/
      $monthData= $month['months'][$i];
      $monthName = explode(",", $monthData);
      $monthTrans='translate.'.$monthName[0];
      $monthYear=$monthName[1];
      $from_to =trans($monthTrans).', '.$monthYear;
      // First day of the month.
      $start_date = date('Y-m-01', strtotime($from_to));
      // Last day of the month.
      $end_date = date('Y-m-t', strtotime($from_to));
      /*echo $start_date."/".$end_date."||";*/
      $range_time1=Timesheet::listingTimesheetWithRangeForStatistics($cid,$eid,$start_date,$end_date,$selectedFromDate,$selectedToDate);
      $statistics[$i][]=$range_time1;
      array_push($statistics[$i],$from_to);
    }
    return $statistics;
  }

  //lisitng timesheet by dates for graph report
  public static function listingTimesheetForGraphYearly($cid,$eid,$from,$to)
  {
    /*$start_date = $from;
    $last_date = $to;*/
    $selectedFromDate=$from;
    $selectedToDate=$to;
    $fromNew = date("Y-m-d", strtotime($from));
    $toNew = date("Y-m-d", strtotime($to));
    $start    = new DateTime($fromNew );
    $start->modify('first day of this year');
    $end      = new DateTime($toNew);
    $end->modify('first day of next year');
    $interval = DateInterval::createFromDateString('1 year');
    $period   = new DatePeriod($start, $interval, $end);
    $years = ['years'];
    foreach ($period as $dt) {
      $years['years'][]=$dt->format("Y-m-d");
    }
    /*return $month;*/
    for($i=0;$i<sizeof($years['years']);$i++)
    {
     /* echo $years['years'][$i];*/
      $year_final = $years['years'][$i];
      $from_to = date('Y', strtotime($year_final));
      // First day of the month.
      $start_date = date('Y-m-01', strtotime($year_final));
      // Last day of the month.
      $end_date = date('Y-m-t', strtotime($year_final));
      /*echo $start_date."/".$end_date."||";*/
      $range_time1=Timesheet::listingTimesheetWithRangeForStatistics($cid,$eid,$start_date,$end_date,$selectedFromDate,$selectedToDate);
      $statistics[$i][]=$range_time1;
      array_push($statistics[$i],$from_to);
    }
    return $statistics;
  }

  //lisitng timesheet by dates for activity graph
  public static function listingTimesheetForActivityGraph($cid,$eid,$from,$to)
  {
    $from = date('Y-m-d', strtotime($from));
    $to = date('Y-m-d', strtotime($to));
    $activity_graph=Timesheet::listingTimesheetWithRangeForActivityGraph($cid,$eid,$from,$to);
   /* return $activity_graph;*/
    $result=array();
    for ($i=0; $i < sizeof($activity_graph) ; $i++){ 
      $balance_min='';
      for ($j=0; $j < sizeof($activity_graph[$i]); $j++) { 
         $balance_min=$balance_min+$activity_graph[$i][$j]['total_hours_min']+$activity_graph[$i][$j]['own_account'];
         $activity=$activity_graph[$i][$j]['timesheetActivity']['activity_name'];
        }
        $balance_min_final=$balance_min;
        $result[$i][]=$balance_min_final;
      array_push($result[$i],$activity);
      }
      return $result;
  }

 

//cms data from super admin

//cms header data from super admin
  public static function cmsSend()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10']);
    for($i=0;$i<sizeof($array);$i++)
    {
     /* $ID = ' id'.($i+1);*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $ID;*/
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms home data from super admin
  public static function cmsSendHome()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24'],'key25'=>$data['key25'],'key26'=>$data['key26'],'key27'=>$data['key27'],'key28'=>$data['key28'],'key29'=>$data['key29'],'key30'=>$data['key30']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24'],'24'=>$data['id25'],'25'=>$data['id26'],'26'=>$data['id27'],'27'=>$data['id28'],'28'=>$data['id29'],'29'=>$data['id30']);
    for($i=0;$i<sizeof($array);$i++)
    {
     /* $ID = ' id'.($i+1);*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms footer data from super admin
  public static function cmsSendFooter()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms login data from super admin
  public static function cmsSendLogin()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms dashboard data from super admin
  public static function cmsSendDashboard()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms faq data from super admin
  public static function cmsSendFaq()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }
  // cms Sidebar data from super admin
  public static function cmsSendSidebar()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms Tour data from super admin
  public static function cmsSendTour()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms Add employee data from super admin
  public static function cmsSendAdd()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms Manage Template data from super admin
  public static function cmsSendTemplate()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24'],'key25'=>$data['key25']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24'],'24'=>$data['id25']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms Manage Activity data from super admin
  public static function cmsSendActivity()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24'],'key25'=>$data['key25'],'key26'=>$data['key26'],'key27'=>$data['key27'],'key28'=>$data['key28'],'key29'=>$data['key29'],'key30'=>$data['key30'],'key31'=>$data['key31'],'key32'=>$data['key32'],'key33'=>$data['key33'],'key34'=>$data['key34'],'key35'=>$data['key35'],'key36'=>$data['key36'],'key37'=>$data['key37'],'key38'=>$data['key38'],'key39'=>$data['key39'],'key40'=>$data['key40'],'key41'=>$data['key41']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24'],'24'=>$data['id25'],'25'=>$data['id26'],'26'=>$data['id27'],'27'=>$data['id28'],'28'=>$data['id29'],'29'=>$data['id30'],'30'=>$data['id31'],'31'=>$data['id32'],'32'=>$data['id33'],'33'=>$data['id34'],'34'=>$data['id35'],'35'=>$data['id36'],'36'=>$data['id37'],'37'=>$data['id38'],'38'=>$data['id39'],'39'=>$data['id40'],'40'=>$data['id41']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms Settings data from super admin
  public static function cmsSendSettings()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24'],'key25'=>$data['key25'],'key26'=>$data['key26'],'key27'=>$data['key27'],'key28'=>$data['key28'],'key29'=>$data['key29'],'key30'=>$data['key30'],'key31'=>$data['key31'],'key32'=>$data['key32'],'key33'=>$data['key33'],'key34'=>$data['key34'],'key35'=>$data['key35'],'key36'=>$data['key36'],'key37'=>$data['key37'],'key38'=>$data['key38'],'key39'=>$data['key39'],'key40'=>$data['key40'],'key41'=>$data['key41'],'key42'=>$data['key42'],'key43'=>$data['key43'],'key44'=>$data['key44'],'key45'=>$data['key45'],'key46'=>$data['key46'],'key47'=>$data['key47'],'key48'=>$data['key48'],'key49'=>$data['key49'],'key50'=>$data['key50'],'key51'=>$data['key51'],'key52'=>$data['key52'],'key53'=>$data['key53'],'key54'=>$data['key54'],'key55'=>$data['key55'],'key56'=>$data['key56'],'key57'=>$data['key57'],'key58'=>$data['key58'],'key59'=>$data['key59'],'key60'=>$data['key60'],'key61'=>$data['key61'],'key62'=>$data['key62'],'key63'=>$data['key63'],'key64'=>$data['key64'],'key65'=>$data['key65'],'key66'=>$data['key66'],'key67'=>$data['key67'],'key68'=>$data['key68'],'key69'=>$data['key69'],'key70'=>$data['key70'],'key71'=>$data['key71'],'key72'=>$data['key72'],'key73'=>$data['key73'],'key74'=>$data['key74'],'key75'=>$data['key75'],'key76'=>$data['key76'],'key77'=>$data['key77'],'key78'=>$data['key78'],'key79'=>$data['key79'],'key80'=>$data['key80'],'key81'=>$data['key81'],'key82'=>$data['key82'],'key83'=>$data['key83'],'key84'=>$data['key84'],'key85'=>$data['key85'],'key86'=>$data['key86'],'key87'=>$data['key87'],'key88'=>$data['key88'],'key89'=>$data['key89'],'key90'=>$data['key90'],'key91'=>$data['key91'],'key92'=>$data['key92'],'key93'=>$data['key93'],'key94'=>$data['key94'],'key95'=>$data['key95'],'key96'=>$data['key96'],'key97'=>$data['key97'],'key98'=>$data['key98']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24'],'24'=>$data['id25'],'25'=>$data['id26'],'26'=>$data['id27'],'27'=>$data['id28'],'28'=>$data['id29'],'29'=>$data['id30'],'30'=>$data['id31'],'31'=>$data['id32'],'32'=>$data['id33'],'33'=>$data['id34'],'34'=>$data['id35'],'35'=>$data['id36'],'36'=>$data['id37'],'37'=>$data['id38'],'38'=>$data['id39'],'39'=>$data['id40'],'40'=>$data['id41'],'41'=>$data['id42'],'42'=>$data['id43'],'43'=>$data['id44'],'44'=>$data['id45'],'45'=>$data['id46'],'46'=>$data['id47'],'47'=>$data['id48'],'48'=>$data['id49'],'49'=>$data['id50'],'50'=>$data['id51'],'51'=>$data['id52'],'52'=>$data['id53'],'53'=>$data['id54'],'54'=>$data['id55'],'55'=>$data['id56'],'56'=>$data['id57'],'57'=>$data['id58'],'58'=>$data['id59'],'59'=>$data['id60'],'60'=>$data['id61'],'61'=>$data['id62'],'62'=>$data['id63'],'63'=>$data['id64'],'64'=>$data['id65'],'65'=>$data['id66'],'66'=>$data['id67'],'67'=>$data['id68'],'68'=>$data['id69'],'69'=>$data['id70'],'70'=>$data['id71'],'71'=>$data['id72'],'72'=>$data['id73'],'73'=>$data['id74'],'74'=>$data['id75'],'75'=>$data['id76'],'76'=>$data['id77'],'77'=>$data['id78'],'78'=>$data['id79'],'79'=>$data['id80'],'80'=>$data['id81'],'81'=>$data['id82'],'82'=>$data['id83'],'83'=>$data['id84'],'84'=>$data['id85'],'85'=>$data['id86'],'86'=>$data['id87'],'87'=>$data['id88'],'88'=>$data['id89'],'89'=>$data['id90'],'90'=>$data['id91'],'91'=>$data['id92'],'92'=>$data['id93'],'93'=>$data['id94'],'94'=>$data['id95'],'95'=>$data['id96'],'96'=>$data['id97'],'97'=>$data['id98']);
    for($i=0;$i<sizeof($array);$i++)
    {
     /* $ID = ' id'.($i+1);*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

    //cms Backup data from super admin
  public static function cmsSendBackup()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
     $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7']);
    for($i=0;$i<sizeof($array);$i++)
    {
     /* $ID = ' id'.($i+1);*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  // cms Pricing data from super admin
  public static function cmsSendPricing()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }
 //test
/*  public function add()
  {
  }*/
  // cms Listing data from super admin
  public static function cmsSendListing()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms Blog data from super admin
  public static function cmsSendBlog()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  // cms Report Tile data from super admin
  public static function cmsSendTile()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms report first data from super admin
  public static function cmsSendReportsFirst()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms stat report data from super admin
  public static function cmsSendStat()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms detailed report data from super admin
  public static function cmsSendReports()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24'],'key25'=>$data['key25']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24'],'24'=>$data['id25']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

    //cms Allowance report data from super admin
  public static function cmsSendAllowance()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }


  //cms Price data from super admin
  public static function cmsSendPrice()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24'],'key25'=>$data['key25'],'key26'=>$data['key26'],'key27'=>$data['key27'],'key28'=>$data['key28'],'key29'=>$data['key29'],'key30'=>$data['key30'],'key31'=>$data['key31'],'key32'=>$data['key32'],'key33'=>$data['key33'],'key34'=>$data['key34'],'key35'=>$data['key35'],'key36'=>$data['key36'],'key37'=>$data['key37'],'key38'=>$data['key38'],'key39'=>$data['key39'],'key40'=>$data['key40'],'key41'=>$data['key41'],'key42'=>$data['key42'],'key43'=>$data['key43'],'key44'=>$data['key44']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24'],'24'=>$data['id25'],'25'=>$data['id26'],'26'=>$data['id27'],'27'=>$data['id28'],'28'=>$data['id29'],'29'=>$data['id30'],'30'=>$data['id31'],'31'=>$data['id32'],'32'=>$data['id33'],'33'=>$data['id34'],'34'=>$data['id35'],'35'=>$data['id36'],'36'=>$data['id37'],'37'=>$data['id38'],'38'=>$data['id39'],'39'=>$data['id40'],'40'=>$data['id41'],'41'=>$data['id42'],'42'=>$data['id43'],'43'=>$data['id44']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  //cms Employer data from super admin
  public static function cmsSendEmployer()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23'],'key24'=>$data['key24'],'key25'=>$data['key25'],'key26'=>$data['key26'],'key27'=>$data['key27']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23'],'23'=>$data['id24'],'24'=>$data['id25'],'25'=>$data['id26'],'26'=>$data['id27']);
    for($i=0;$i<sizeof($array);$i++)
    {
     /* $ID = ' id'.($i+1);*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }

  public static function changeStatus()
  {
    $up=array("trail_status" =>"active",
    "payment_status" => "pending",
    "trail_start_date" =>"2016-10-05 00:00:00",
    "trail_end_date" =>"2016-11-06 00:00:00");
    $company=Companies::all();
    for ($i=0; $i < sizeof($company); $i++) { 
      $update=Companies::where('_id',$company[$i]['_id'])->update($up);
    }
    return Companies::all();
  }

  //cms contact data from super admin
  public static function cmsSendContact()
  {
    $data=Input::all();
    $lang=Input::get('languag');
    /*return $data;*/
    $array=array('key1'=>$data['key1'],'key2'=>$data['key2'],'key3'=>$data['key3'],'key4'=>$data['key4'],'key5'=>$data['key5'],'key6'=>$data['key6'],'key7'=>$data['key7'],'key8'=>$data['key8'],'key9'=>$data['key9'],'key10'=>$data['key10'],'key11'=>$data['key11'],'key12'=>$data['key12'],'key13'=>$data['key13'],'key14'=>$data['key14'],'key15'=>$data['key15'],'key16'=>$data['key16'],'key17'=>$data['key17'],'key18'=>$data['key18'],'key19'=>$data['key19'],'key20'=>$data['key20'],'key21'=>$data['key21'],'key22'=>$data['key22'],'key23'=>$data['key23']);
    $array2=array('0'=>$data['id1'],'1'=>$data['id2'],'2'=>$data['id3'],'3'=>$data['id4'],'4'=>$data['id5'],'5'=>$data['id6'],'6'=>$data['id7'],'7'=>$data['id8'],'8'=>$data['id9'],'9'=>$data['id10'],'10'=>$data['id11'],'11'=>$data['id12'],'12'=>$data['id13'],'13'=>$data['id14'],'14'=>$data['id15'],'15'=>$data['id16'],'16'=>$data['id17'],'17'=>$data['id18'],'18'=>$data['id19'],'19'=>$data['id20'],'20'=>$data['id21'],'21'=>$data['id22'],'22'=>$data['id23']);
    for($i=0;$i<sizeof($array);$i++)
    {
      /*return $lang;*/
     if ($lang=='en') {
        $language="translation_en";
     }
     else if($lang=='ge') {
      $language="translation_de";
     }
    /* return $language;*/
      $KEY = 'key'.($i+1);
      $update = array($language=>$array[$KEY]);
      /*return $array2[$ID1];*/
      $result=Translation::where('_id',$array2[$i])->update($update); 
    }
    return  array('status' => 'success','response'=>'Content updated');
  }
  public function addPlans()
  {
    /*$response=array('general_setting_sync'=>true);
    Employee::sendNotificationByFcmId($response);*/
    $update=array("country_name" =>Input::get('country_name'),
    "country_code" =>Input::get('country_code'),
    "currency_code" =>Input::get('currency_code'),
    "plan"=>Input::get('plan'),
    "plan_price"=>Input::get('plan_price'));
    $result=PlanAndPricing::addPlans($update);
    return $result;
  }

  public function uploadBlog() {
   // return "ssd";
  /* $data=Input::all();*/
    
      if(Input::hasFile('image'))
      {
        $imagePath = FileUpload::uploadBlogImage('fileUpload');
      }
      else {
        $imagePath = 'null';
      }
      
      
       $details=array("title_en"=>Input::get('title_en'),
        "body_en"=>Input::get('body_en'),
        "title_de"=>Input::get('title_de'),
        "body_de"=>Input::get('body_de'),
        "link"=>Input::get('link'),
        "blog_image"=>$imagePath,
        "created_on"=>Input::get('created_on'),
        "created_on_format"=>Input::get('created_on_format'));
      //return $details;

      $result=BlogListing::uploadBlog($details);
      if($result['status']=='success')
        {
        return array('status'=> 'success','response'=>"great");
        }
      else
        {
           return array('status'=> 'error','response'=>"Try again");
        }
    }
    public function editBlog($id) {
      $result=BlogListing::editBlog($id);
      return $result['0'];
    }


    public function updateBlog() {
      if(Input::hasFile('image'))
      {
        $imagePath = FileUpload::uploadBlogImage('fileUpload');
        /*return $imagePath;*/
      }
      else {
        $imagePath = Input::get('image1');
      }
      
      $blog_id=Input::get('blogId');
      $update=array('title_en'=>Input::get('title_en'),
      'body_en'=>Input::get('body_en'),
      'title_de'=>Input::get('title_de'),
      'body_de'=>Input::get('body_de'),
      'link'=>Input::get('link'),
      'blog_image'=>$imagePath);
      //return $update;

    $result=BlogListing::where('_id',$blog_id)->update($update);
    if($result)
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
        return array('status'=> 'error','response'=>"Try again");
      }
    }
  public function deleteBlog($id) {
    $result=BlogListing::deleteBlog($id);
    if($result['status']=='success')
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
       return array('status'=> 'error','response'=>"Try again");
      }
    }
    public function addTopic() {
      $details=array("translation_page"=>'faq',
                    "translation_key"=>Input::get('translation_key'),
                    "translation_en"=>Input::get('translation_en'),
                    "translation_de"=>Input::get('translation_de'));
      /*return $details;*/
      $result=FaqTitle::addTopic($details);
      if($result['status']=='success')
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
       return array('status'=> 'error','response'=>"Try again");
      }
    }
    public function editTopic($id) {
    $result=FaqTitle::editTopic($id);
    return $result['0'];
    }
    public function updateFaqTopic() {
      $topic_id=Input::get('id');
      $update=array('topic_en'=>Input::get('englishtopic'),
      'topic_de'=>Input::get('germantopic'));
      /*return $update;*/

    $result=FaqTitle::where('_id',$topic_id)->update($update);
    if($result)
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
        return array('status'=> 'error','response'=>"Try again");
      }
    }
    public function deleteTopic($id) {
    $result=FaqTitle::deleteTopic($id);
    if($result['status']=='success')
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
       return array('status'=> 'error','response'=>"Try again");
      }
    }
    public function addFaq() {
   // return "ssd";
  /* $data=Input::all();*/
    
      if(Input::hasFile('image'))
      {
        $imagePath = FileUpload::uploadFaqImage('FaqImage');
      }
      else {
        $imagePath = null;
      }
       $details=array("topic_id"=>Input::get('topic_id'),
        "question_en"=>Input::get('question_en'),
        "answer_en"=>Input::get('answer_en'),
        "question_de"=>Input::get('question_de'),
        "answer_de"=>Input::get('answer_de'),
        "link"=>Input::get('link'),
        "faq_image"=>$imagePath);

      $result=Faq::addFaq($details);
      if($result['status']=='success')
        {
        return array('status'=> 'success','response'=>"great");
        }
      else
        {
           return array('status'=> 'error','response'=>"Try again");
        }
    }
    public function editFaq($id) {
    $result=Faq::editFaq($id);
    return $result['0'];
/*if($result['status']=='success')
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
       return array('status'=> 'error','response'=>"Try again");
      }*/
    }
    public function updateFaq() {
      $faq_id=Input::get('id');
      $update=array('question_en'=>Input::get('question_en'),
      'answer_en'=>Input::get('answer_en'),
      'question_de'=>Input::get('question_de'),
      'answer_de'=>Input::get('answer_de'),
      'link'=>Input::get('link'));
      /*return $update;*/

    $result=Faq::where('_id',$faq_id)->update($update);
    if($result)
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
        return array('status'=> 'error','response'=>"Try again");
      }
    }
    public function deletefaq($id) {
    $result=Faq::deletefaq($id);
    if($result['status']=='success')
      {
        return array('status'=> 'success','response'=>"great");
      }
    else
      {
       return array('status'=> 'error','response'=>"Try again");
      }
    }
     public function editPrice($code) {
      $result=PlanAndPricing::editPrice($code);
    return $result;

     }
     public function updatePrice() {
      $data=Input::all();

       $array2=array('0'=>$data['plan_id1'],'1'=>$data['plan_id2'],'2'=>$data['plan_id3']);
     /* return $array;*/

       for($i=0;$i<sizeof($array2);$i++)
    {

       $price =array('plan_price'=>$data[$i]);
      $result=PlanAndPricing::where('_id',$array2[$i])->update($price); 
    }
     return  array('status' => 'success','response'=>'Content updated');
     }
   //sendFCMNotification
  public function sendFCMNotification($id){
   $fields = array(
       'app_id' => "99e99848-9ad6-4fb4-97e7-81cde5da78cf",
       'registration_ids' => array (
                 $id
        ),
        'data' => array (
                  "name" => 'Nagesh',
                  "message" => 'Hi' )
       
     );
  $fields = json_encode($fields);

     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
           'Authorization: key=AIzaSyADzAzLnzF9CLgyRdLwXp7KxOYFqBINR4A'));
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
     curl_setopt($ch, CURLOPT_HEADER, FALSE);
     curl_setopt($ch, CURLOPT_POST, TRUE);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
     
     $response = curl_exec($ch);
     return $response;
  }
  public function activationLogin($email,$language)
  {
    \Session::flash('email',$email);
    \Session::flash('message1',trans('translate.activatin_login_msg'));
    session()->put('UserLanguage', $language);
      return redirect()->route('login');
  }

  //getPreviousDayTemplate
  public function getPreviousDayTemplate()
  {
    $date=Input::get('date');
    $eid=Input::get('id');
    $from=Input::get('start_date');
    $to=Input::get('end_date');
    $range=Timesheet::groupBy('date')->orderBy('date','asc')->where('employee_id',$eid)->whereBetween('date', [$from, $to])->get();
    $array=array();
    for ($i=0; $i <sizeof($range) ; $i++) { 
      $range1=Timesheet::orderBy('date','desc')->where('date',$range[$i]['date'])->where('employee_id',$eid)->get();
      array_push($array, $range1);
    }
    /*return $array;*/
    $list=array();
    for ($j=0; $j < sizeof($array) ; $j++) 
    { 
    $timestamp=strtotime($array[$j][0]['date']);
    $list[$j]['date']=date('D', $timestamp);
    $offset=$array[$j][0]['offset_min'];
    $totalHours=0;
    $balanceHours=0;
    for ($k=0; $k <sizeof($array[$j]) ; $k++) { 
    $totalHours=$array[$j][$k]['TSHrsTotal'];
    }
    $balanceHours=$totalHours-$offset;
    $list[$j]['overtime']=$balanceHours;
    }
    /*return $list;*/
    $details=array('week'=>$list);
    return array('status'=>"success",
                  'data'=>$details);
  }


//sendAppleNotification
  public function sendAppleNotification($id){
 // Put your device token here (without spaces):
$deviceToken = $id;

// Put your private key's passphrase here:
$passphrase = 'codewave';

// Put your alert message here:
$message = 'push notification';
$response = array('general_setting_change'=>true,
                    'message'=>'general_setting_changed');
////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert',  app_path().'/certs/pushcert-stafftime.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
  'alert' => array(
        'body' => $message,
        'action-loc-key' => 'stafftimes',
    ),
    'response' => $response,
    'badge' => 2,
  'sound' => 'oven.caf',
  );

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
  echo 'Message not delivered' . PHP_EOL;
else
  echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
  

  

  }

  public static function getPlannerData($id,$from,$to)
  {
    $selectedFromDate=$from;
    $selectedToDate=$to;
    $start_date = $from;
    $last_date = $to;
    $timestamp=strtotime($start_date);
    $startDate='';
    if(date('D', $timestamp) === 'Mon') 
    {
      $startDate=strtotime($start_date);
    }
    else
    {
      $startDate=strtotime('previous monday',$timestamp);
    }
    $dates = range($startDate, strtotime($last_date),86400);
    $days = array('monday' => array());

    array_map(function($v)use(&$days){
        if(date('D', $v) == 'Mon'){
            $days['monday'][] = date('Y-m-d', $v);
        }
    }, $dates); // Requires PHP 5.3+
    $statistics="";
    $dataArray=[];
    $dataArray1=[];
    for($i=0;$i<sizeof($days['monday']);$i++)
    {
      $from=$days['monday'][$i];
      $to = strtotime($from);
      $to = strtotime("+6 day", $to);
      $to = date('Y-m-d', $to);
      $month_name = strtotime($from);
      $month_name = date('M', $month_name);
      $week = new DateTime($from);
      $week_number = $week->format("W");
      $dispalyFrom = strtotime($from);
      $dispalyFrom = date('Y-m-d', $dispalyFrom);
      $dispalyTo = strtotime($to);
      $dispalyTo = date('Y-m-d', $dispalyTo);
      $datesPerWeek =FileUpload::getAllDates($dispalyFrom,$dispalyTo);
      $resArray=[];
    foreach($datesPerWeek as $date) {
      $datesWeekly = $date->format('Y-m-d');
          array_push($resArray, $datesWeekly);
      }
     // $from_to=trans('translate.week')." : ".$week_number." (".$dispalyFrom." ".trans('translate.to')." ".$dispalyTo.") ";
      $from_to= array('from' =>$dispalyFrom ,
                      'to'=> $dispalyTo,
                      'week_number'=>$week_number,
                      'month_name' => $month_name,
                      'datesInAWeek'=>$resArray);
      array_push($dataArray,$from_to);
    }
    
    $planner=Timesheet::getPlannerData($id,$from,$to,$selectedFromDate,$selectedToDate);
    /*return $from_to;*/
    //return $planner;
    $details=array('range_details' =>$dataArray,
                   'planner_details' =>$planner);
    return $details;
  }

  //sync data button
  public static function selectSyncDates($start,$end) {
    $selectedFromDate=$start;
    $selectedToDate=$end;
    $fromDate = strtotime($selectedFromDate);
    $fromDate = date('Y-m-d H:i:s', $fromDate);
    $endDate = strtotime($selectedToDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    return array('start_date' => $fromDate , 'end_date' => $endDate );
    $responseToSend=array('start_date'=>$selectedFromDate,
                          'end_date'=>$selectedToDate,
                            'message'=>'back up');
      Employee::sendNotificationByFcmId($responseToSend);
      
    return  array('status' => 'success', 'response'=> 'sent notification');
  }
  public function extendTrial($id) {
    $companyDetails =Companies::getCompanyDetail($id);
    //return $companyDetails[0];
    //$employeeDetails =Employee::where('company_id',$id)->get();
    $trail_status="active";
    $todayDate= date("Y-m-d H:i:s");
    $trailEndDateUpdated = strtotime($todayDate);
    $trailEndDateUpdated = strtotime("+30 day", $trailEndDateUpdated);
    $trailEndDateUpdated = date("Y-m-d H:i:s", $trailEndDateUpdated);
    $update = array('trail_status'=>$trail_status,
                    'trail_start_date'=>$todayDate,
                    'trail_end_date'=>$trailEndDateUpdated);
    $update1 = array('status'=>$trail_status);
    $result = Companies::where('_id',$id)->update($update);
    $result2 = Employee::where('company_id',$id)->update($update1);
    return  array('status' => 'success','response'=>'Content updated');
    

  }
}


