<?php

namespace App\Http\Controllers;
use App\Employee; //including model.  
use App\Companies; //including model.
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\MotTemplates;
use App\MotActivity;
use App\WorkingDaysSetting;//model
use App\ConditionForWorkingDaysSetting;//model
use App\GeneralSetting;//model
use App\OvertimeHandling;//model
use App\SetAnnualAllowance;//model
use App\BlogListing;//model
use App\FaqTitle;//including model.
use App\Faq;//including model.
use App\Timesheet;
use App\Translation;
use App\LanguageCode; 
use App\WebNotifications;
use App\Country;
use Illuminate\Support\Facades\Input;
use App\MotAdminActivity;
use App\MotAdminTemplates;
use App\PlanAndPricing;
use DateTime;
use App\MailSend;
use App\UserBrowserLanguage;
use App\CountryNameTranslation;
use App\RestoreLog;
use App\FileUpload;
use App\MyovertimeUsers;
use App\Domain;
class PageController extends Controller
{	//landing page display
    public function landing_page()
	{
		$domain = FileUpload::getDomain();
		$metaTag=Domain::where('domain',$domain)->get(['welcome']);
		$metaTag=$metaTag[0]['welcome'];
		return view('welcome')->with('data',$metaTag);
	}
	//login page display
    public function login()
	{

		return view('login');
	}
	public function caLogin()
	{

		return view('login_ca');
	}
	public function companies()
	{

		return view('companies');
	}
	public function add_company()
	{

		return view('add_company');
	}

	public function manage_users()
	{

		return view('manage_users');
	}
	public function add_role()
	{

		return view('add_role');
	}
	public function manage_groups()
	{

		return view('manage_groups');
	}
	public function add_staff()
	{

		return view('add_staff');
	}


	//faq page display
	public function faq()
	{
		$result = FaqTitle::GetDetails();
		$domain = FileUpload::getDomain();
		$metaTag=Domain::where('domain',$domain)->get(['faq']);
		$metaTag=$metaTag[0]['faq'];
		$data=array('faqResult'=>$result,
					'metaTag'=>$metaTag);
		return view('faq')->with('faqData',$data);
	}
	public function faqForMobileView()
	{
		$result = FaqTitle::GetDetails();
		$domain = FileUpload::getDomain();
		$metaTag=Domain::where('domain',$domain)->get(['faq']);
		$metaTag=$metaTag[0]['faq'];
		$data=array('faqResult'=>$result,
					'metaTag'=>$metaTag);
		return view('faq1')->with('faqData',$data);
	}
	//tour page display
	public function tour()
	{
		$domain = FileUpload::getDomain();
		$metaTag=Domain::where('domain',$domain)->get(['faq']);
		$metaTag=$metaTag[0]['faq'];
		$data=array('metaTag'=>$metaTag);
		return view('tour')->with('faqData',$data);
	}
	//blog page display
	public function blog()
	{
		$result = BlogListing::orderBy('_id','desc')->get();
		$domain = FileUpload::getDomain();
		$metaTag=Domain::where('domain',$domain)->get(['blog']);
		//return $result;
		$metaTag=$metaTag[0]['blog'];
		$data=array('blogResult'=>$result,
					'metaTag'=>$metaTag);
		return view('blog')->with('blogData',$data);
	}
	public function bloglanding($id) 
	{
		$result =BlogListing::where('_id',$id)->get();
		$data=$result[0];
		/*return $data;*/
		return view('bloglanding')->with('data',$data);

	}
	// blog comments section
	/*public function blogComment()
	{
		$result = BlogComment::all();
		return $result;
		return view('blog')->with('data',$result) ;
	}*/

	//contactus page display
	public function contactus()
	{
		$domain = FileUpload::getDomain();
		$metaTag=Domain::where('domain',$domain)->get(['contactus']);
		$metaTag=$metaTag[0]['contactus'];
		return view('contactus')->with('data',$metaTag);
	}
	//price page display
	public function price()
	{
		$plans=PlanAndPricing::all();
		$defaultPlans=PlanAndPricing::where('country_code','US')->where('plan','A')->get();
		$domain = FileUpload::getDomain();
		$metaTag=Domain::where('domain',$domain)->get(['price']);
		$metaTag=$metaTag[0]['price'];
		$data=array('plans'=>$plans,
					'defaultPlans'=>$defaultPlans,
					'metaTag'=>$metaTag);
		return view('price')->with('data', $data);
	}
	//employee profile page display
	public function employeeprofile($id)
	{   
		$companyDetail = Auth::user();
		$employee =Employee::where('company_id',$companyDetail['_id'])->where('_id',$id)->get();
		$eid=$employee[0]['_id'];
		$range=Timesheet::groupBy('date')->orderBy('date','asc')->where('employee_id',$eid)->get();
		$array=array();
		for ($i=0; $i <sizeof($range) ; $i++) { 
		$range1=Timesheet::orderBy('date','desc')->where('date',$range[$i]['date'])->where('employee_id',$eid)->get();
		array_push($array, $range1);
		}
		$timesheets = Timesheet :: allTimeSheetsForEmployee($eid);
		$data1['date'] = array();
		$data1['total_overtime'] = array();
		$data1['avg_offset'] = array();
		for($i=0;$i<count($timesheets);$i++){
			// $result[$i]['date'] = $timesheets[$i]['date'];
			// $data = Timesheet :: totalOvertimeForCompany($timesheets[$i]['date'],$companyId);
			// $result[$i]['total_overtime'] = $data['total_overtime'];
			// $result[$i]['avg_offset'] = $data['avg_offset'];
			$dataTimesheet = Timesheet :: totalOvertimeForEmployee($timesheets[$i]['date'],$eid);
			array_push($data1['date'],date('d', strtotime($timesheets[$i]['date'])));
			array_push($data1['total_overtime'],$dataTimesheet['total_overtime']);
			array_push($data1['avg_offset'],$dataTimesheet['avg_offset']);
		}
		$data=array('employee'=>$employee[0],
				'timesheet'=>$array,
				'chart_data'=>$data1);
		/*array_push($employee[1], $array);*/
		/*return $data1;*/
		return view('profile')->with('data', $data);
	}

	//employee profile page display test
	public function employeeprofile1($id)
	{   
		$companyDetail = Auth::user();
		$employee =Employee::where('company_id',$companyDetail['_id'])->where('_id',$id)->get();
		$eid=$employee[0]['_id'];
		$range=Timesheet::groupBy('date')->orderBy('date','asc')->where('employee_id',$eid)->get();
		$array=array();
		for ($i=0; $i <sizeof($range) ; $i++) { 
		$range1=Timesheet::orderBy('date','desc')->where('date',$range[$i]['date'])->where('employee_id',$eid)->get();
		array_push($array, $range1);
		}
		$timesheets = Timesheet :: allTimeSheetsForEmployee($eid);
		$data1['date'] = array();
		$data1['total_overtime'] = array();
		$data1['avg_offset'] = array();
		for($i=0;$i<count($timesheets);$i++){
			// $result[$i]['date'] = $timesheets[$i]['date'];
			// $data = Timesheet :: totalOvertimeForCompany($timesheets[$i]['date'],$companyId);
			// $result[$i]['total_overtime'] = $data['total_overtime'];
			// $result[$i]['avg_offset'] = $data['avg_offset'];
			$dataTimesheet = Timesheet :: totalOvertimeForEmployee($timesheets[$i]['date'],$eid);
			array_push($data1['date'],date('d', strtotime($timesheets[$i]['date'])));
			array_push($data1['total_overtime'],$dataTimesheet['total_overtime']);
			array_push($data1['avg_offset'],$dataTimesheet['avg_offset']);
		}
		$data=array('employee'=>$employee[0],
				'timesheet'=>$array,
				'chart_data'=>$data1);
		/*array_push($employee[1], $array);*/
		return $data;
		return view('profile')->with('data', $data);
	}

	//my notifications
	public function myNotification()
	{   
	    $details = array('id' => Input::get('id'));
		$notification=WebNotifications::myNotification($details);
		return $notification;
	}

	//mailNow
	public function paymentRecipt()
	{   
	    $details = array('name' => Input::get('name'),'email' => Input::get('email'),'currency' => Input::get('currency'),'amount' => Input::get('amount'),'order_no' => Input::get('order_no'),'plan' => Input::get('plan'),'date' => Input::get('date'));
		$mailnow=MailSend::paymentRecipt($details);
		return $mailnow;
	}
    
    //make all read notifications
	public function makeItRead()
	{   
	    $details = array('company_id' => Input::get('company_id'));
		$notification=WebNotifications::makeItRead($details);
		return $notification;
	}

	 //make clicked read notifications
	public function makeClikedRead()
	{   
	    $details = array('company_id' => Input::get('company_id'),'notification_id'=>Input::get('notification_id'));
		$notification=WebNotifications::makeClikedRead($details);
		return $notification;
	}
	 
	//pricing page display
	public function plansandprice()
	{
		$companyCountryDetail = Auth::user();
		$companyMembers =Employee::where('company_id',$companyCountryDetail['_id'])->get();
		$countryCode=$companyCountryDetail['country'];
		$plans=PlanAndPricing::where('country_code',$countryCode)->get();
		$defaultPlans=PlanAndPricing::where('country_code','US')->get();
		$data=array('plans'=>$plans,
					'defaultPlans'=>$defaultPlans,
					'company_details' => $companyCountryDetail,
					 'employee_list'=>$companyMembers);
		/*return $data;*/

		return view('pricing')->with('data', $data);
	}
	//company details with all the employees
	public function subscribedemployees()
	{   
		$companyDetail = Auth::user(); 
		$companyMembers =Employee::where('company_id',$companyDetail['_id'])->get();
		$details = array('company_details' => $companyDetail,
					     'employee_list'=>$companyMembers);

		return view('subscribed_employee')->with('data', $details);
	}
    
    //company details with all the employees
	public function subscribedusers()
	{   
		$companyDetail = Auth::user(); 
		$companyMembers =Employee::where('company_id',$companyDetail['_id'])->get();
		$details = array('company_details' => $companyDetail,
					     'employee_list'=>$companyMembers);

		return view('subscribed_users')->with('data', $details);
	}

	public function subscribedemployeeslist()
	{   
		$companyDetail = Auth::user(); 
		$companyMembers =Employee::where('company_id',$companyDetail['_id'])->get();
		$details = array('company_details' => $companyDetail,
					     'employee_list'=>$companyMembers);
		return $details;
	}
	//empolyer profile page display
	public function profile($id) 
	{
		$employerDetail =Companies::getCompanyDetail($id);
		$countryTranslation=CountryNameTranslation::orderBy('translation_en','asc')->get();
		$employerDetail[]=array($countryTranslation);
		$employee=Employee::where('company_id',$id)->orderBy('subscription_end_date','desc')->get();
		$employeeActiveSubcription=Employee::where('company_id',$id)->where('payment_status','active')->get();
		$employerDetail[]=array($employee);
		$employerDetail[]=count($employeeActiveSubcription);
		/*return $employerDetail[2][0][0];*/
		return view('employer')->with('data', $employerDetail);
	}
	//add employee page disply
	public function add_employee() 
	{
		$userDetails = Auth::user();
     	return view('add_employee')->with('data',$userDetails);
	}

	public function create_user() 
	{
		$userDetails = Auth::user();
     	return view('create_user')->with('data',$userDetails);
	}

	//report page display
	public function reports_1($id) 
	{
		$employee =Employee::where('_id',$id)->get();
		$data=$employee[0];
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		$data1= array('employee_details' =>$details,
					  'employee'=>$employee);
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		Timesheet::where('date',"1970-01-01")->delete();//delete junk data in timesheet
		$data = array('user_id' => $data['company_id'],);//for general settings
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$separator=Country::Separator();
		$data1= array('employee_details' =>$details,
					  'general_setting' => $general_setting,
					  'employee'=>$employee,
					  'separator'=>$separator);
	/*	return $data1;*/
		return view('reports-1')->with('data',$data1);
	}
		//report page display
	public function reports($id) 
	{
		$employee =Employee::where('_id',$id)->get();
		$data=$employee[0];
		$superadminDetails = Companies :: where('admin',1)->get();
		$data2 = array('user_id' => $data['company_id'],
					 'superadmin_id' => $superadminDetails[0]['_id']);//for activity
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		Timesheet::where('date',"1970-01-01")->delete();//delete junk data in timesheet
		$data = array('user_id' => $data['company_id'],);//for general settings
		$activity=MotActivity::getListOfActivity($data2);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$separator=Country::Separator();
		$data1= array('employee_details' =>$details,
					  'general_setting' => $general_setting,
					  'activity'=>$activity,
					  'employee'=>$employee,
					  'separator'=>$separator);
		/*return $data1;*/
		return view('reports')->with('data', $data1);
	}
	//stats reports page display
	public function statistics_reports($id) 
	{

		$employee =Employee::where('_id',$id)->get();
		$data=$employee[0];
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		$data1= array('employee_details' =>$details,
					  'employee'=>$employee);
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		$data = array('user_id' => $data['company_id'],);//for general settings
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$separator=Country::Separator();
		$data1= array('employee_details' =>$details,
					  'general_setting' => $general_setting,
					  'employee'=>$employee,
					  'separator'=>$separator);

		/*return $data1;*/
		return view('stats_reports')->with('data', $data1);
	}
	//activity graph report page display
	public function activity_graph_report($id) 
	{
		$employee =Employee::where('_id',$id)->get();
		$data=$employee[0];
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		$data1= array('employee_details' =>$details,
					  'employee'=>$employee);
		return view('activity_graph_report')->with('data', $data1);
	}
	//activity graph report page display
	public function allowance_report($id) 
	{
		$employee =Employee::where('_id',$id)->get();
		$data=$employee[0];
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		$data1= array('employee_details' =>$details,
					  'employee'=>$employee);
		/*$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);*/
		$data = array('user_id' => $data['company_id'],);//for general settings
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$separator=Country::Separator();
		$allowanceActivity=MotActivity::where('allowance_bank_days', '!=','0')->where('allowance_bank_days', '!=','0.00')->where('allowance_bank_hours', '!=','')->where('company_id',$data['user_id'])->get();
		/*return $allowanceActivity;*/
		$allownaceFinal=[];
		for ($i=0; $i <sizeof($allowanceActivity); $i++) { 
			$activityId=$allowanceActivity[$i]['_id'];
			$userDetail=$details;
			$allownaceValue=Timesheet::allownaceTimesheet($activityId,$userDetail);
			if (count($allownaceValue) > 0) {
				array_push($allownaceFinal,$allownaceValue);
			}
		}
		$data1= array('employee_details' =>$details,
					  'general_setting' => $general_setting,
					  'employee'=>$employee,
					  'separator'=>$separator,
					  'allownace'=>$allownaceFinal);
		/*return $data1;*/

		return view('allowance_report')->with('data', $data1);
	}
	//graph report page display
	public function graph_report($id) 
	{
		$employee =Employee::where('_id',$id)->get();
		$data=$employee[0];
		$details=array('company_id'=>$data['company_id'],
						'employee_id'=>$data['_id']);
		$data1= array('employee_details' =>$details,
					  'employee'=>$employee);

		return view('graph_report')->with('data', $data1);
	}
	//graph report page display
	
	public function test() 
	{
		$timesheet=Timesheet::groupBy('employee_id','date')->get();
		for ($i=0; $i < sizeof($timesheet) ; $i++) 
		{ 
			$timesheet1=Timesheet::where('employee_id',$timesheet[$i]['employee_id'])->where('date',$timesheet[$i]['date'])->get();
			$TSHrsTotal=0;
			$TSHrsAccounted=0;
			$TSBonusHrs=0;
			$TSDailyBalnce=0;
			$offset=0;
			for ($j=0; $j <sizeof($timesheet1) ; $j++) 
			{ 
				$TSHrsTotal=$TSHrsTotal+$timesheet1[$j]['total_hours_min'];
				$TSBonusHrs=$TSBonusHrs+FileUpload::hoursToMinutes($timesheet1[$j]['bonus_hours']);
				$offset=$timesheet1[$j]['offset_min'];
			}
			$TSHrsAccounted=$TSHrsTotal-$TSBonusHrs;
			$TSDailyBalnce=$TSHrsTotal-$offset;
			$updateTS=array('TSHrsAccounted' => $TSHrsAccounted,
							'TSBonusHrs' => $TSBonusHrs,
							'TSHrsTotal' => $TSHrsTotal,
							'TSDailyBalnce' =>$TSDailyBalnce);
			$timesheet1=Timesheet::where('employee_id',$timesheet[$i]['employee_id'])->where('date',$timesheet[$i]['date'])->update($updateTS);
		}
/*	return view('emails.payment');	*/
		/*$userDetails = Auth::user();
		$templateList = MotTemplates :: getListOfCompanyTemplate($userDetails['_id']);
		$MotTemplateList = MotTemplates :: getListOfMotTemplate();

		//get super admin details 
		$superadminDetails = Companies :: where('admin',1)
										  ->get();

		//separator
		$separator=Country::Separator();

		$data = array('user_id' => $userDetails['_id'],
					 'superadmin_id' => $superadminDetails[0]['_id']);
		$activityList = MotActivity :: getListOfActivity($data);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$details = array('template_list' => $templateList,
						 'activity_list' => $activityList,
						 'mot_template_list' => $MotTemplateList,
						 'general_setting'=>$general_setting,
						 'separator'=>$separator,
						 'user'=>$userDetails,); 
			return view('demo')->with('details',$details);*/
	/*	return $details;*/
		/*return $details['mot_template_list']['response'][0]['activity'][0]['activityDetails']['activity_name'];*/
     
		/*$update = array('own_account' =>"", );
		Timesheet::where('own_account',"false")->update($update);*/
/*		 $activity=MotAdminActivity::where('deploy_status','!=','pending')->get();
		 $array=array('deploy_status'=>'deployed');
		 for ($i=0; $i <sizeof($activity) ; $i++) { 
		 	MotAdminActivity::where('_id',$activity[$i]['_id'])->update($array);
		 }
		 $activity=MotAdminActivity::where('deploy_status','!=','pending')->get();
		 return $activity;*/
/*		$countryTranslation=CountryNameTranslation::orderBy('translation_en','asc')->get();
		$res='';
		for ($i=0; $i <sizeof($countryTranslation) ; $i++) 
		{ 
			$string=".trans('country_translation.".$countryTranslation[$i]['translation_key']."').";

			echo "<option value='".$countryTranslation[$i]['translation_key']."'>".$string."</option>\n";
echo "<option data-countryCode='".$countryTranslation[$i]['translation_key']."' value='".$countryTranslation[$i]['FIELD9']."'>".$string."(+".$countryTranslation[$i]['FIELD9'].")</option>\n";

		}*/
		/*
		<option data-countryCode="UA" value="380"><?php echo trans('country_translation.CH');?> (+380)</option>
		$changeLanguageForUserLanguage=UserBrowserLanguage::userLanguage();
		$changeLanguageForUserLanguage1=UserBrowserLanguage::browserLanguage();
		return 'user-'.$changeLanguageForUserLanguage.' browser-'.$changeLanguageForUserLanguage1;*/
	}
	public function test1() 
	{
	return 'fdfddf';
/*			$timesheet1=Timesheet::all();
			for ($j=0;$j<sizeof($timesheet1);$j++) 
			{ 
				$bonus=FileUpload::hoursToMinutes($timesheet1[$j]['bonus_hours']);
				$totalhoursmin=$timesheet1[$j]['total_hours_min']-$bonus;
				$totalhours=str_replace('.',':',FileUpload::MinToHours($totalhoursmin));
				$updateTS=array('total_hours1' => (string)$totalhours,
							'total_hours_min1' => (string)$totalhoursmin);
				Timesheet::where('_id',$timesheet1[$j]['_id'])->update($updateTS);		
			}
			return 'f';*/
/*		$userDetails = Auth::user();
		$templateList = MotTemplates :: getListOfCompanyTemplate($userDetails['_id']);
		$MotTemplateList = MotTemplates :: getListOfMotTemplate();

		//get super admin details 
		$superadminDetails = Companies :: where('admin',1)
										  ->get();

		//separator
		$separator=Country::Separator();

		$data = array('user_id' => $userDetails['_id'],
					 'superadmin_id' => $superadminDetails[0]['_id']);
		$activityList = MotActivity :: getListOfActivity($data);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$details = array('template_list' => $templateList,
						 'activity_list' => $activityList,
						 'mot_template_list' => $MotTemplateList,
						 'general_setting'=>$general_setting,
						 'separator'=>$separator,
						 'user'=>$userDetails,); 
   		return view('demo1')->with('details',$details);*/
	}
	public function test2() 
	{
		$userDetails = Auth::user();
		$templateList = MotTemplates :: getListOfCompanyTemplate($userDetails['_id']);
		$MotTemplateList = MotTemplates :: getListOfMotTemplate();

		//get super admin details 
		$superadminDetails = Companies :: where('admin',1)
										  ->get();

		//separator
		$separator=Country::Separator();

		$data = array('user_id' => $userDetails['_id'],
					 'superadmin_id' => $superadminDetails[0]['_id']);
		$activityList = MotActivity :: getListOfActivity($data);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$details = array('template_list' => $templateList,
						 'activity_list' => $activityList,
						 'mot_template_list' => $MotTemplateList,
						 'general_setting'=>$general_setting,
						 'separator'=>$separator,
						 'user'=>$userDetails,); 
   		return view('demo2')->with('details',$details);
	}
	//dashboard page display
	public function dashboard()
	{	
		$userDetails = Auth::user();
		$companyId = $userDetails['_id'];
		$numberOfActiveEmployee = Employee :: activeEmployee($companyId);
		$totalOvertimeByMonth = Timesheet :: totalOvertime($companyId);
		$todaysAvgOvertime = Timesheet :: todaysAvgOvertime($companyId);
		$topEmployee = Employee :: topEmployee($companyId);
		$timesheets = Timesheet :: allTimeSheets($companyId);
		$data['date'] = array();
		$data['total_overtime'] = array();
		$data['avg_offset'] = array();
		for($i=0;$i<count($timesheets);$i++){
			// $result[$i]['date'] = $timesheets[$i]['date'];
			// $data = Timesheet :: totalOvertimeForCompany($timesheets[$i]['date'],$companyId);
			// $result[$i]['total_overtime'] = $data['total_overtime'];
			// $result[$i]['avg_offset'] = $data['avg_offset'];
			$dataTimesheet = Timesheet :: totalOvertimeForCompany($timesheets[$i]['date'],$companyId);
			array_push($data['date'],date('d', strtotime($timesheets[$i]['date'])));
			array_push($data['total_overtime'],$dataTimesheet['total_overtime']);
			array_push($data['avg_offset'],$dataTimesheet['avg_offset']);
		}
		$details = array('data' => $userDetails,
						'active_users' => $numberOfActiveEmployee,
						'total_overtime_by_month' => $totalOvertimeByMonth,
						'todays_avg_overtime' => $todaysAvgOvertime,
						'top_employee' => $topEmployee,
						'chart_data' => $data);
		/*return $details;*/
     	return view('dashboard')->with('data',$details);
    }
    public function dashboard1()
	{	
		$userDetails = Auth::user();
		$companyId = $userDetails['_id'];
		$numberOfActiveEmployee = Employee :: activeEmployee($companyId);
		$totalOvertimeByMonth = Timesheet :: totalOvertime($companyId);
		$todaysAvgOvertime = Timesheet :: todaysAvgOvertime($companyId);
		$topEmployee = Employee :: topEmployee($companyId);
		$timesheets = Timesheet :: allTimeSheets($companyId);
		$data['date'] = array();
		$data['total_overtime'] = array();
		$data['avg_offset'] = array();
		for($i=0;$i<count($timesheets);$i++){
			// $result[$i]['date'] = $timesheets[$i]['date'];
			// $data = Timesheet :: totalOvertimeForCompany($timesheets[$i]['date'],$companyId);
			// $result[$i]['total_overtime'] = $data['total_overtime'];
			// $result[$i]['avg_offset'] = $data['avg_offset'];
			$dataTimesheet = Timesheet :: totalOvertimeForCompany($timesheets[$i]['date'],$companyId);
			array_push($data['date'],date('d', strtotime($timesheets[$i]['date'])));
			array_push($data['total_overtime'],$dataTimesheet['total_overtime']);
			array_push($data['avg_offset'],$dataTimesheet['avg_offset']);
		}
		$details = array('data' => $userDetails,
						'active_users' => $numberOfActiveEmployee,
						'total_overtime_by_month' => $totalOvertimeByMonth,
						'todays_avg_overtime' => $todaysAvgOvertime,
						'top_employee' => $topEmployee,
						'chart_data' => $data);
		return $details;
     	return view('dashboard')->with('data',$details);
    }
    //setting page display
	public function settings()
	{
		$userDetails = Auth::user();
		//get super admin details 
		$superadminDetails = Companies :: where('admin',1)
										  ->get();
	/*	dd($superadminDetails[0]['_id']);*/
		$data = array('user_id' => $userDetails['_id'],
					 'superadmin_id' => $superadminDetails[0]['_id']);
		$separator=Country::Separator();
		$templateList = MotTemplates::getListTemplate($data);
		$activityList = MotActivity::getListOfActivity($data);
		$activityListForSetAllowance = MotActivity::getListOfActivityForSetting($data);
		$working_day_setting=WorkingDaysSetting::workingDaySetting($data);
		$working_day_setting_condtion=ConditionForWorkingDaysSetting::workingDaySettingCondtion($data);
		$general_setting=GeneralSetting::generalSettingList($data);
		$overtime_handling=OvertimeHandling::overtimeHandlingList($data);
		$set_annual_allowance=SetAnnualAllowance::setAnnualAllowanceList($data);
	/*	return $overtime_handling;*/
		$details = array('template_list' => $templateList,
						 'working_day_setting' => $working_day_setting,
						 'activity_list' => $activityList,
						 'activity_list1'=>$activityListForSetAllowance,
						 'general_setting'=>$general_setting,
						 'condtion'=>$working_day_setting_condtion,
						 'overtime_handling'=>$overtime_handling,
						 'set_annual_allowance'=>$set_annual_allowance,
						 'separator'=>$separator);
		/*return $details;*/
		return view('settings')->with('details',$details);
	}
    //manage template page display
    public function manage_template()
	{
		$userDetails = Auth::user();
		$templateList = MotTemplates :: getListOfCompanyTemplate($userDetails['_id']);
		$MotTemplateList = MotTemplates :: getListOfMotTemplate();

		//get super admin details 
		$superadminDetails = Companies :: where('admin',1)
										  ->get();

		//separator
		$separator=Country::Separator();

		$data = array('user_id' => $userDetails['_id'],
					 'superadmin_id' => $superadminDetails[0]['_id']);
		$activityList = MotActivity :: getListOfActivity($data);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$details = array('template_list' => $templateList,
						 'activity_list' => $activityList,
						 'mot_template_list' => $MotTemplateList,
						 'general_setting'=>$general_setting,
						 'separator'=>$separator,
						 'user'=>$userDetails,); 
	
		/*return $details['mot_template_list']['response'][0]['activity'][0]['activityDetails']['activity_name'];*/
     	return view('manage_template')->with('details',$details);
    }
    //manage activity page display
    public function manage_activity()
	{	
		$userDetails = Auth::user();
		$activityList = MotActivity :: getListOfCompanyActivity($userDetails['_id']);
		//separator
		$separator=Country::Separator();
		$data = array('user_id' => $userDetails['_id']);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$overtime_handling=OvertimeHandling::overtimeHandlingList($data);//overtime handling
		$annual_allowance=SetAnnualAllowance::setAnnualAllowanceList($data);//annual allowance
		$details = array('activity_list' => $activityList,
						'general_setting'=>$general_setting,
						'overtime_handling'=>$overtime_handling,
						'annual_allowance'=>$annual_allowance,
						 'separator'=>$separator); 
		// return $details;
     	return view('manage_activity')->with('details',$details);
    }
    //listing page display
    public function listing()
    {
    	return view('listing');
	}
	//planner report
	public function planner_report() 
	{
		return view('planner_report');
	}
	public function backupPage() 
	{
		return view('backup');
	}
	public function backupListing() 
	{
		$user=Auth::user();
		$data=BackUpController::import($user['_id']);
		return view('backup_listing')->with('data',$data);
	}
	public function restoreView()
	{
		$user=Auth::user();
		$data=RestoreLog::where('company_id',$user['_id'])->get();
		if(count($data) > 0){
            $response = array('status' => 'success','status_code' => '323','response' => $data);
          }
          else{
            $response = array('status' => 'failure','status_code' => '433','response' => 'no data');
          }
		return view('restoreview')->with('data',$response);
	}
	public function adminManageTemplate() 
	{
		$userDetails = Auth::user();
		//get super admin details 
		$superadminDetails = Companies :: where('admin',1)
										  ->get();
		$MotTemplateList = MotAdminTemplates :: getListOfCompanyTemplate($superadminDetails[0]['_id']);
		$data = array('user_id' => $userDetails['_id'],
					 'superadmin_id' => $superadminDetails[0]['_id']);
		$activityList = MotAdminActivity :: getListOfActivitysforPage($data);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$details = array(
						 'activity_list' => $activityList,
						 'mot_template_list' => $MotTemplateList,
						 'general_setting'=>$general_setting,);
		/*return $details;*/
		return view('admin.manage_default_template')->with('details',$details);
	}
	public function adminManageActivity() 
	{
		$userDetails = Auth::user();
		$activityList = MotAdminActivity :: getListOfMotActivityForAdmin($userDetails['_id']);
		$MotactivityList = MotAdminActivity :: getListOfMotActivity();
		//separator
		$separator=Country::Separator();
		$data = array('user_id' => $userDetails['_id']);
		$general_setting=GeneralSetting::generalSettingList($data);//general setting
		$details = array('activity_list' => $activityList,
						'mot_activity_list' => $MotactivityList,
						'general_setting'=>$general_setting,
						 'separator'=>$separator); 
		/*return $details;*/
		return view('admin.manage_default_activity')->with('details',$details);
	}
	public function adminHomeCms() 
	{
		$Translation = Translation::where('translation_page','welcome')->orderBy('_id', 'asc')->get();
		$details = array('Translation' => $Translation); 
		/*return $details['Translation']['translation_en'];*/
		return view('admin.admin_home_cms')->with('details',$details);
	}
	public function adminHeaderCms() 
	{
		$Translation = Translation::where('translation_page','main_page_header_nav')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_header_cms')->with('details',$details);
	}
	public function adminTourCms() 
	{
		$Translation = Translation::where('translation_page','tour')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_tour_cms')->with('details',$details);
	}
	public function adminFaqCms() 
	{
		$Translation = Translation::where('translation_page','faq')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		$data = FaqTitle::orderBy('id','asc')->get();
		$faq = Faq::orderBy('id','asc')->get();
		$result= array('details'=>$details, 'data'=>$data, 'faq'=>$faq);
		//return $details; 
		return view('admin.admin_faq_cms')->with('result',$result);
	}
	public function adminLoginCms() 
	{
		$Translation = Translation::where('translation_page','login')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_login_cms')->with('details',$details);
	}
	public function adminSidebarCms() 
	{
		$Translation = Translation::where('translation_page','header')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_sidebar_cms')->with('details',$details);
	}
	public function adminFooterCms() 
	{
		$Translation = Translation::where('translation_page','main_page_footer')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_footer_cms')->with('details',$details);
	}
	public function adminDashboardCms() 
	{
		$Translation = Translation::where('translation_page','dashboard')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_dashboard_cms')->with('details',$details);
	}
	public function adminAddEmployeeCms() 
	{
		$Translation = Translation::where('translation_page','add_employee')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_add_cms')->with('details',$details);
	}
	public function adminManageTemplateCms() 
	{
		$Translation = Translation::where('translation_page','manage_template')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_template_cms')->with('details',$details);
	}
	public function adminManageActivityCms() 
	{
		$Translation = Translation::where('translation_page','manage_activity')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_activity_cms')->with('details',$details);
	}
	public function adminSettingsCms() 
	{
		$Translation = Translation::where('translation_page','settings')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_settings_cms')->with('details',$details);
	}
	public function adminBackupCms() 
	{
		$Translation = Translation::where('translation_page','backup')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_backup_cms')->with('details',$details);
	}
	public function adminPricingCms() 
	{
		$Translation = Translation::where('translation_page','pricing')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_pricing_cms')->with('details',$details);
	}
	public function adminListingCms() 
	{
		$Translation = Translation::where('translation_page','listing')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_listing_cms')->with('details',$details);
	}
	public function adminBlogCms() 
	{
		$Translation = Translation::where('translation_page','blog')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		$data = BlogListing::orderBy('id','asc')->get();
		//return $details; 
		$result = array('details' => $details, 'data' => $data);
		return view('admin.admin_blog_cms')->with('result',$result);
	}
	public function adminTileCms() 
	{
		$Translation = Translation::where('translation_page','report_links')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_tile_cms')->with('details',$details);
	}
	public function adminReportsFirstCms() 
	{
		$Translation = Translation::where('translation_page','reports-1')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_reports-1_cms')->with('details',$details);
	}
	public function adminStatCms() 
	{
		$Translation = Translation::where('translation_page','stats_reports')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_stat_cms')->with('details',$details);
	}
	public function adminReportsCms() 
	{
		$Translation = Translation::where('translation_page','reports')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_reports_cms')->with('details',$details);
	}
	public function adminAllowanceCms() 
	{
		$Translation = Translation::where('translation_page','allowance_report')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_allowance_cms')->with('details',$details);
	}
	public function adminPriceCms() 
	{
		$Translation = Translation::where('translation_page','price')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_price_cms')->with('details',$details);
	}
	public function adminContactCms() 
	{
		$Translation = Translation::where('translation_page','contact')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_contact_cms')->with('details',$details);
	}
	public function adminEmployerCms() 
	{
		$Translation = Translation::where('translation_page','employer')->orderBy('id','asc')->get();
		$details = array('Translation' => $Translation); 
		//return $details; 
		return view('admin.admin_employer_cms')->with('details',$details);
	}
	public function adminCountryPriceCms() 
	{
		$details = PlanAndPricing::groupBy('country_name','country_code')->get();
		/*$details = array('German' => $Country);*/
		/*return $details;*/
		return view('admin.admin_german_cms')->with('details',$details);
	}
	/*//timeout
	public function timeout()
	{
		return view('timeout');
	}*/

	//admin setting
	public function adminSettings()
	{
		$userDetails = Auth::user();
		$data = array('user_id' => $userDetails['_id']);
		$separator=Country::Separator();
		$activityList = MotAdminActivity::getListOfActivity($data);
		$general_setting=GeneralSetting::generalSettingList($data);
		$set_annual_allowance=SetAnnualAllowance::setAnnualAllowanceList($data);
	/*	return $overtime_handling;*/
		$details = array('activity_list' => $activityList,
						 'general_setting'=>$general_setting,
						 'set_annual_allowance'=>$set_annual_allowance,);
	/*	return $details;*/
		return view('admin.setting')->with('details',$details);
	}
	public function termsandcondition()
	{
		return view('termsandcondition');
	}

	public function privacy()
	{
		return view('privacy');
	}

	//all employee listing for super admin
	public function allEmployeeListing()
	{
		$employeeList=Employee::companyWithEmployeeListing();
		/*return $employeeList;*/
		return view('admin.all_employee_listing')->with('data',$employeeList);
	}

	//getuserinfo
	public function getuserinfo(){
		$details = array('email_id' => Input::get('email_id'),
						  'name' => Input::get('name'),
						  'language' => Input::get('language'));
		$result = MyovertimeUsers :: insertUser($details);
		return $result;
	}
}





//$result=Translation::where('translation_page','welcome')->get();\
//function(){return view('admin.admin_home_cms');}