<?php
namespace App\Http\Controllers;
use App\Companies; //including model.
use App\FileUpload;//including model.
use App\CompanyTemplates;//including model.
use App\CompanyActivity;//including model.
use App\MotTemplates;//including model.
use App\Employee;//including model.
use App\MotActivity;//including model.
use App\Language;//including model.
use App\Translation;//including model.
use App\WorkingDaysSetting;//model
use App\ConditionForWorkingDaysSetting;//model
use App\GeneralSetting;//model
use App\OvertimeHandling;//model
use App\SetAnnualAllowance;//model
use App\ActivitiesMappedToTemplate;//model
use App\Timesheet;//model
use App\MotAdminActivity;
use App\WebNotifications;
use App\MotAdminTemplates;
use App\AdminActivitiesMappedToTemplate;
use App\PlanAndPricing;
use App\Notification;
use App\BlogListing;//including model.
use App\FaqTitle;//including model.
use App\Faq;//including model.
use App\BackUp;
use App\RestoreLog;
use \File;
use DB;
use Excel;
use Zipper;
use AWS;
use Aws\S3\S3Client;
use GuzzleHttp;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use View;
use Illuminate\Support\Facades\Auth;
use DateTime;

class BackUpController extends Controller
{
	public function importExport()
	{
		return view('importExport');
	}
	public function downloadToJson()
	{	
		$totalCompanies = Companies::get();
		$timstamp=date("Y-m-d h:s",time());
		$allExportPath=storage_path('app/daily-bkp/exports');
		File::cleanDirectory($allExportPath);
		for($i=0;$i<count($totalCompanies);$i++){
			$xlsPath = storage_path('../../local/storage/app/daily-bkp/exports/'.$totalCompanies[$i]['_id']);
			//delete if any data inside the folder
			$jsonPath = storage_path('app/daily-bkp/exports/'.$totalCompanies[$i]['_id']);
			File::makeDirectory($jsonPath);
			//save Company data
			$data = Companies::where('_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_detail.json',json_encode($data));
			//save Company data

			//save Employee data
			$data = Employee::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_employee_detail.json',json_encode($data));
			//save Employee data

			//save GeneralSetting data
			$data = GeneralSetting::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_general_settings_detail.json',json_encode($data));
			//save GeneralSetting data

			//save OvertimeHandling data
			$data = OvertimeHandling::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_overtime_handling_detail.json',json_encode($data));
			//save OvertimeHandling data

			//save SetAnnualAllowance data
			$data = SetAnnualAllowance::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_set_annual_allowance_detail.json',json_encode($data));
			//save SetAnnualAllowance data

			//save WorkingDaysSetting data
			$data = WorkingDaysSetting::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_working_days_settings_detail.json',json_encode($data));
			//save WorkingDaysSetting data

			//save ConditionForWorkingDaysSetting data
			$data = ConditionForWorkingDaysSetting::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_condition_mapped_to_working_days_settings_detail.json',json_encode($data));
			//save ConditionForWorkingDaysSetting data

			//save Timesheet data
			$data = Timesheet::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_timesheet_detail.json',json_encode($data));
			//save Timesheet data

			//save MotActivity data
			$data = MotActivity::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_activity_detail.json',json_encode($data));
			//save MotActivity data

			//save ActivitiesMappedToTemplate data
			$data = ActivitiesMappedToTemplate::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_activities_mapped_to_template_detail.json',json_encode($data));
			//save ActivitiesMappedToTemplate data

			//save MotTemplates data
			$data = MotTemplates::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_templates_detail.json',json_encode($data));
			//save MotTemplates data

			//create a zip file
			$files = glob(storage_path('../../local/storage/app/daily-bkp/exports/'.$totalCompanies[$i]['_id'].'/*'));
   			Zipper::make(storage_path('../../local/storage/app/daily-bkp/exports/'.$totalCompanies[$i]['_id'].'_.zip'))->add($files)->close();

   			//upload
   			Storage::disk('s3')->makeDirectory($totalCompanies[$i]['_id']);
			$path='daily-bkp/exports/'.$totalCompanies[$i]['_id'].'_.zip';
			$contents = Storage::disk('local')->get($path);
			$storage=Storage::disk('s3')->put($totalCompanies[$i]['_id'].'/'.$timstamp.'.zip', $contents,'public');
			var_dump($storage);
		}
	}
	public function importExcel()
	{
		Zipper::make(storage_path('../../local/storage/app/daily-bkp/exports/574ef2cef4619d67dd529e12_.zip'))->folder('tiles')->extractTo(storage_path('../../local/storage/app/daily-bkp/imports/'));
                return "Extracted";
		/*if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = ['title' => $value->title, 'description' => $value->description];
				}
				if(!empty($insert)){
					DB::table('items')->insert($insert);
					dd('Insert Record successfully.');
				}
			}
		}
		return back();*/
	}

	public function importDate()
	{
		$fileNameCsv=array('0' =>'company_detail.json',
						'1'=>'company_employee_detail.json',
						'2'=>'company_activities_mapped_to_template_detail.json',
						'3'=>'company_activity_detail.json',
						'4'=>'company_condition_mapped_to_working_days_settings_detail.json',
						'5'=>'company_general_settings_detail.json',
						'6'=>'company_overtime_handling_detail.json',
						'7'=>'company_set_annual_allowance_detail.json',
						'8'=>'company_templates_detail.json',
						'9'=>'company_timesheet_detail.json',
						'10'=>'company_working_days_settings_detail.json');
		$fileName=Auth::user();
		$log=Input::get('time_log');
		$restore_log=array('company_id' => $fileName['_id'],
							'log'=> $log);
		$insert='';
		if (Input::hasFile('zip_file')) 
        {
        	$xlsPath = storage_path('../../local/storage/app/daily-bkp/imports/'.$fileName['_id']);
        	File::deleteDirectory($xlsPath);
        	File::makeDirectory($xlsPath);
        	Zipper::make(Input::file('zip_file'))->extractTo(storage_path('../../local/storage/app/daily-bkp/imports/'.$fileName['_id']));
        	$files = \File::files(storage_path('../../local/storage/app/daily-bkp/imports/'.$fileName['_id']));
        	$filesExported=array();
        	if (sizeof($files)>0) {
        	for ($i=0; $i <sizeof($files) ; $i++) { 
        		$filenameExported=explode('/', $files[$i]);
        		array_push($filesExported, $filenameExported[sizeof($filenameExported)-1]);
        	}
			sort($filesExported);
			sort($fileNameCsv);
            $arraysAreEqual = ($filesExported == $fileNameCsv);
			/*var_dump($arraysAreEqual);
            return 'ffd';*/
            if ($arraysAreEqual==true) 
            {
				for ($i=0; $i <sizeof($fileNameCsv) ; $i++) 
				{ 
				$path=$xlsPath.'/'.$fileNameCsv[$i];
				$data=json_decode(file_get_contents($path), true); 
				$data= FileUpload::De($data);
				if ($data=="DecryptException") {
				return array('status'=> 'failure','response'=> 'Please upload the correct folder');
				}
				}
				for ($i=0; $i <sizeof($fileNameCsv) ; $i++) 
				{ 
				$path=$xlsPath.'/'.$fileNameCsv[$i];
				$data=json_decode(file_get_contents($path), true); 
				$data= FileUpload::De($data);
				$insert=BackUp::uploadCsvToDatabase($data,$fileNameCsv[$i]);
				if ($insert['status']=="failure") {
				return $insert;
				}
				}
				$insert_log=RestoreLog::insert($restore_log);
				if ($insert_log) {
					return array('status'=> 'success','response'=> 'Uploaded');
				}
            }
            else
            {
            	return array('status'=> 'failure','response'=> 'Please upload the correct folder');
            }        		# code...
        	}
        	else
        	{
        		return array('status'=> 'failure','response'=> 'Please try again');
        	}
        }
        else
        {
        	return array('status'=> 'failure','response'=> 'Please Upload the zip file');
        }
        
	}

	//upload
/*	public function upload()
	{
		$totalCompanies = Companies::get();
		$timstamp=data("Y-m-d h:s",time());
		for($i=0;$i<count($totalCompanies);$i++)
		{
			Storage::disk('s3')->makeDirectory($totalCompanies[$i]['_id']);
			$path='daily-bkp/exports/'.$totalCompanies[$i]['_id'].'_.zip';
			$contents = Storage::disk('local')->get($path);
			$storage=Storage::disk('s3')->put($totalCompanies[$i]['_id'].'/'.$timstamp.'.zip', $contents,'public');
			var_dump($storage);
		}	
	}*/
	public static function import($id)
	{
		$files = Storage::disk('s3')->allFiles($id);
		/*var_dump($files);*/
		/*return $files;*/
		$backUp=array();
		for ($i=0; $i <sizeof($files) ; $i++) { 
			$time = Storage::lastModified($files[$i]);
			$backUp[$i]['link']="https://s3.eu-central-1.amazonaws.com/stafftimes-central/".$files[$i];
			$backUp[$i]['last_modified']=date("Y-m-d h:s",$time);

		}
		 $response='';
		if(count($backUp) > 0){
            $response = array('status' => 'success','status_code' => '323','response' => $backUp);
          }
          else{
            $response = array('status' => 'failure','status_code' => '433','response' => 'no data');
          }
          return $response;
	}

	public static function companyid()
	{
		return ActivitiesMappedToTemplate::cmp();
	}
	public static function cronTest()
	{
		$test=BackUp::cronTest();
	}
}

/*https://s3.eu-central-1.amazonaws.com/stafftimes-central/574ef2cef4619d67dd529e12/2017-02-08+01:22.zip*/