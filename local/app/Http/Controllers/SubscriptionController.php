<?php

namespace App\Http\Controllers;
use App\Employee; //including model.  
use App\Subscriptions; //including model.
use App\Companies; //including model.
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\MotTemplates;
use App\MotActivity;
use DateTime;

class SubscriptionController extends Controller
{	
	public function paymentInfo()
    {   
        $details = array('company_email' => Input::get('company_email'),
                         'company_name' => Input::get('company_name'),
                         'company_id' => Input::get('company_id'),
                         'braintree_id' => Input::get('braintree_id'),
                         'subscription_plan' => Input::get('subscription_plan'),
                         'employeeIds' => Input::get('employeeIds'),
                         'payment_status' => Input::get('payment_status'),   
                         'payment_amount' => Input::get('payment_amount'),    
                         'created_on' => date("Y-m-d 00:00:00"),
                         'end_date' => Input::get('end_date'),
                         'updated_on' => date("Y-m-d 00:00:00")
            );
        $compDetails = Companies::updatepaymentCompany($details); 
        if($compDetails){
            $empDetails = Employee::updatepaymentEmployee($details);
        if($empDetails){
            $results=Subscriptions::paymentInfo($details);
            return $results;
        }
        else{
        	return array('status'=> 'failure','status_code'=> '401','response'=>'Payment Failure!');
        }
        }  
        else{
        	return array('status'=> 'failure','status_code'=> '401','response'=>'Payment Failure!');
        }
        
    }

	
	
}
