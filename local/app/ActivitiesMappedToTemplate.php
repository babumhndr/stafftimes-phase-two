<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
class ActivitiesMappedToTemplate extends Eloquent
{
  protected $collection='mot_activities_mapped_to_template';

    //activity
 /* public function activity(){
    return $this->hasOne('App\MotActivity','_id','template_activity_id');
  }*/
  //activityDetails
  public function activityDetails(){
    return $this->hasOne('App\MotActivity','_id','activity_id');
  }

  //addActivityMappedToTempalte
  public static function addActivityMappedToTempalte($activityDetails){
    $start=$activityDetails['activity_start'];
    $end=$activityDetails['activity_end'];
    /*if ($activityDetails['activity_flat_hours']!='00:00') {
      $start="00:00";
      $end="00:00";
    }*/
  	$activity = new ActivitiesMappedToTemplate;
  	$activity->activity_id = $activityDetails['activity_id'];
    $activity->company_id = $activityDetails['company_id'];
  	$activity->activity_flat_hours = $activityDetails['activity_flat_hours'];
  	$activity->activity_amount = $activityDetails['activity_amount'];
  	$activity->activity_start = $start;
  	$activity->activity_end = $end;
  	$activity->activity_break = $activityDetails['activity_break'];
  	$activity->template_id = $activityDetails['template_id'];
  	$activity->save();

  }

  public static function cmp()
  {
    $data=ActivitiesMappedToTemplate::all();
    for ($i=0; $i <sizeof($data) ; $i++) { 
      $company_id=MotActivity::where('_id',$data[$i]['activity_id'])->get(['company_id']);
      $array=array('company_id' =>$company_id[0]['company_id']);
      $update=ActivitiesMappedToTemplate::where('_id',$data[$i]['_id'])->update($array);
      /*return $array;*/
    }
    
  }

}