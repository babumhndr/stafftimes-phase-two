<?php

namespace App\Console;

use DB;
use App\Companies;
use App\MailSend;
use App\BackUp;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
   protected function schedule(Schedule $schedule)
    {
      $schedule->call(function () {
          $dailyBackup=BackUp::downloadToJson();
          //$test=BackUp::cronTest();
          //$backupDB = BackUp::takeADBBackupForTestServer();
          $backupDB = BackUp::takeADBBackupofLiveServer();

          //$backupDB = BackUp::takeADBBackupForTestServer();


          //$backupDB = BackUp::takeADBBackupofLiveServer();
      })->cron('00 18 * * *'); 
      
   $schedule->call(function () {
   
   //Trail status to expired after expiry date
   $companyList = Companies::all();

   for($m=0; $m < count($companyList); $m++){  
   $companyId = $companyList[$m]['_id'];
   $companyEmail = $companyList[$m]['email'];
   $companyName = $companyList[$m]['company_name'];
   $paymentStatus =$companyList[$m]['payment_status'];
   $companyLanguage = $companyList[$m]['language_name'];         
   $companyEmployee = DB::table('mot_employees')->where('company_id', $companyId)->get();
   $companyTrailStatus = $companyList[$m]['trail_status'];
   if($companyTrailStatus == 'active' && $paymentStatus == 'pending'){
   $trailStartDate = $companyList[$m]['trail_start_date'];
   $trailEndDate = $companyList[$m]['trail_end_date'];
         $paymentDate = strtotime(date("Y-m-d 00:00:00"));
         $contractDateBegin = strtotime($trailStartDate); 
         $contractDateEnd = strtotime($trailEndDate);
   if($paymentDate <= $contractDateEnd) {
               
   } else {
      DB::table('mot_employees')->where('company_id',$companyId)->update(array('trail_status' => 'expired'));
      DB::table('mot_companies')->where('_id',$companyId)->update(array('trail_status' => 'expired'));
   }

    //trail period reminder
    $trailStart = str_replace(" 00: 00:  00", "", $trailStartDate);
    $trailEnd = str_replace(" 00: 00: 00", "", $trailEndDate);

    $paymentDate = date("Y-m-d 00:00:00");
    $bodytag = str_replace(" 00:00:00", "", $paymentDate);
    $paymentDateNew = strtotime($bodytag);
    $contractDateBegin = strtotime($trailStart); 
    $contractDateEnd = strtotime($trailEnd);

    $days_between = abs($contractDateEnd - $paymentDateNew);
    $numberDays = $days_between/86400;
    $numberDays = intval($numberDays);  

    if($numberDays == 15){
    if($companyLanguage == 'English'){ 
      $companySubject = '15 days since you joined';
    } 
    else{
      $companySubject = 'Schon 15 Tage dabei';
    }
    $details = array('name'=>$companyName,
              'email'=>$companyEmail,
              'subject'=>$companySubject,
              'lang'=>$companyLanguage,
              );
   $sendMailTo=MailSend::FifteenDaysReminder($details);
    }  
    if($numberDays == 7){
    if($companyLanguage == 'English'){
      $companySubject = 'Your 30 Day Trial is expiring';
    }
    else{
      $companySubject = 'Ihre 30 tägige Testphase läuft ab';
    }
    $details = array('name'=>$companyName,
              'email'=>$companyEmail,
              'subject'=>$companySubject,
              'lang'=>$companyLanguage,
              );
    $sendMailTo=MailSend::SevenDaysReminder($details);
    }
    if($numberDays == 1){
    if($companyLanguage == 'English'){
      $companySubject = 'Your 30 Day Trial is expiring';
    }
    else{
      $companySubject = 'Ihre 30 tägige Testphase läuft ab';
    }
    $details = array('name'=>$companyName,
              'email'=>$companyEmail,
              'subject'=>$companySubject,
              'lang'=>$companyLanguage,
              );
    $sendMailTo=MailSend::OneDayReminder($details);
    }
     
   } 
      }
   
   
         })->cron('30 18 * * *'); 
    }
}
