<?php
namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class CountryNameTranslation extends Eloquent
{ 
  protected $collection='country_for_translation';
}
