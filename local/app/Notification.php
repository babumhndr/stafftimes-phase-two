<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
 //sendFCMNotification
  public static function sendFCMNotification($id,$response)
  {
   $fields = array(
       'app_id' => "99e99848-9ad6-4fb4-97e7-81cde5da78cf",
       'registration_ids' => array (
                 $id
        ),
        'data' => $response
       
     );
  $fields = json_encode($fields);
 /*    $headers = array (
             'Authorization: key=' . "AIzaSyC6BNfk_sFn9rbu737Bc08NH909cxA7l-w",
             'Content-Type: application/json'
     );*/

/*     $ch = curl_init ();
     curl_setopt ( $ch, CURLOPT_URL, $url );
     curl_setopt ( $ch, CURLOPT_POST, true );
     curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
     curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
     curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );*/

     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
           'Authorization: key=AIzaSyCV20AHs_c3eR_Ih84wf88rRfvGiiBvLlk'));
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
     curl_setopt($ch, CURLOPT_HEADER, FALSE);
     curl_setopt($ch, CURLOPT_POST, TRUE);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
     
     $response = curl_exec($ch);
     return $response;
  }

  //sendAppleNotification
  public static function sendAppleNotification($id,$response){
 // Put your device token here (without spaces):
$deviceToken = $id;

// Put your private key's passphrase here:
$passphrase = 'codewave';

// Put your alert message here:

$msgText = str_replace('_', ' ', $response['message']);
$msgText = ucfirst($msgText);
$message = $msgText.'. Tap to update';
///////////////////////////////////////////////////////////////////////
$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert',  app_path().'/certs/pushcert.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

//echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
/*$body['aps'] = array(
  'alert' => array(
        'body' => $message,
        'action-loc-key' => 'stafftimes App',
    ),
    'response' => $response,
    'badge' => 2,
  'sound' => 'oven.caf',
  );*/
  $body['aps'] = array(
  'alert' => array(
        'loc-key' => $message,
    ),
    'response' => $response,
    'badge' => 2,
  'sound' => 'oven.caf',
  );

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result){
  //echo 'Message not delivered' . PHP_EOL;
}
  
else{
  //echo 'Message successfully delivered' . PHP_EOL;
}
  

// Close the connection to the server
fclose($fp);
}
}
