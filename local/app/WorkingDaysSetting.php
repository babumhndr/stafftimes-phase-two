<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use App\ConditionForWorkingDaysSetting;//model

class WorkingDaysSetting extends Eloquent
{ 
  protected $collection='mot_working_days_settings';

//condtion
  public function condtion(){
    return $this->hasMany('App\ConditionForWorkingDaysSetting','working_days_settings_id','_id');
  }
  //setWorkingData
  public static function setWorkingData($set_working_data,$condtion)
  {
    function replaceDotToColon($val)
    {
    if (strpos($val, '.') !== FALSE)
    {
      return str_replace('.',':',$val);
    }
    elseif(strpos($val, ',') !== FALSE)
    {
      return str_replace(',',':',$val);
    }
    else
    {
      return $val;
    }
    }
   for($i=0;$i<count($set_working_data);$i++)
   {
    $setWorkingOffset=replaceDotToColon($set_working_data[$i]['work_days_offset']);

    $setWorkingBreak=replaceDotToColon($set_working_data[$i]['break_default']);

     $result = new WorkingDaysSetting;

     $result->dayname= $set_working_data[$i]['day'];
     $result->companyid=$set_working_data[$i]['companyid'];
     $result->ctb=$set_working_data[$i]['ctb'];
     $result->work_days_offset=$setWorkingOffset;
     $result->break_default=$setWorkingBreak;
     $result->template_id=$set_working_data[$i]['template_id'];
     $result->isset=$set_working_data[$i]['isset'];
     $result->save();

     $set_working_data_id=$result->id;

     $result1=ConditionForWorkingDaysSetting::setWorkingData($set_working_data_id,$condtion,$i);

   }
     if($result)
     {
       $response = array('status' => 'success','status_code' => '119');
       $responseToSend=array('working_day_setting_change'=>true,
                            'message'=>'working_day_setting_changed');
       Employee::sendNotificationByFcmId($responseToSend);
     }
     else{
        $response = array('status' => 'failure','status_code' => '129');
      }
      return $response;
  }

  //update setWorkingData

   public static function updateWorkingData($set_working_data,$condtion,$companyid)
  {
    for($i=0;$i<count($set_working_data);$i++)
   {
     /*$set_working_data[$i];*/
     $result=WorkingDaysSetting::where('companyid',$companyid)->where('_id',$set_working_data[$i]['id'])->update($set_working_data[$i]);
     $result1=ConditionForWorkingDaysSetting::updateWorkingData($condtion,$companyid,$i);
   }
    if($result)
     {
       $response = array('status' => 'success','status_code' => '119');
       $responseToSend=array('working_day_setting_change'=>true,
                            'message'=>'working_day_setting_changed');
       Employee::sendNotificationByFcmId($responseToSend);
     }
     else{
        $response = array('status' => 'failure','status_code' => '129');
      }
      return $response;
 /*     return $result1;*/
  }

  //list of workingDaySetting
  public static function workingDaySetting($data)
  {
    $details=WorkingDaysSetting::where('companyid',$data['user_id'])->orderBy('_id', 'asc')->get();
    if(count($details) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $details);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;
  }

  //list of workSetSettingValues for mobile
  public static function workSetSettingValues($companyid)
  {
    $result=WorkingDaysSetting::with('condtion')->where('companyid',$companyid)->get();
    if(count($result) > 0){
          $response = array('status' => 'success','status_code' => '456','response' => $result);

      }
      else{
        $response = array('status' => 'failure','status_code' => '876','response' =>[]);
      }
      return $response;
  }
}


      