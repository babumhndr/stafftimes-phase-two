<?php

namespace App;
use App\Companies; //including model.
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class WebNotifications extends Eloquent implements Authenticatable
{
	use AuthenticableTrait; 
    protected $collection='web_notifications';
	
    public function senderInfo(){
        return $this->hasOne('App\Companies','_id','sender_id');
    }
	public function recieverInfo(){
        return $this->hasOne('App\Companies','_id','reciever_id');
    }
    public function templateInfo(){
        return $this->hasOne('App\MotAdminTemplates','_id','content_landing_id');
    }



    public static function addIntoNotification($details,$companyList){
       
       for($i=0;$i<count($companyList);$i++){
    	$notification  = new WebNotifications;
    	$notification->reciever_id = $companyList[$i]['_id'];
    	$notification->sender_id = $details['sender_id'];
        $notification->notification_type = $details['notification_type'];
        $notification->notification_content = $details['notification_content'];
        $notification->content_landing_id = $details['content_landing_id'];
        $notification->notification_sent_at = $details['deployed_time'];
        $notification->seen_status = false;
        $notification->status = 'pending';
		$notification->notification_for = $details['notification_for'];
    	$notification->save();
		}
        return "success";
        
    }
	
	//make it read all messages
    public static function makeItRead($details){
        $result = WebNotifications :: where('reciever_id',$details['company_id'])
                                    ->update(array('seen_status' => true,'viewed_by' => $details['company_id']));
        if($result){
            $response = array('status' => 'success', 'status_code' => '220','response' => 'successfully');
        }
        else{
            $response = array('status' => 'failure', 'status_code' => '420','response' => 'unsuccessful');   
        }
        return $response;
    }

    //make it read all messages
    public static function makeClikedRead($details){
        $result = WebNotifications :: where('reciever_id',$details['company_id'])->where('_id',$details['notification_id'])
                                    ->update(array('seen_status' => true,'viewed_by' => $details['company_id']));
        if($result){
            $response = array('status' => 'success', 'status_code' => '221','response' => 'successfully');
        }
        else{
            $response = array('status' => 'failure', 'status_code' => '421','response' => 'unsuccessful');   
        }
        return $response;
    }
    
	//myNotification
    public static function myNotification($details){
        $result = WebNotifications :: with('senderInfo')->with('recieverInfo')->with('templateInfo')
                                    ->where('seen_status',false)
                                    ->Where('reciever_id',$details['id'])
									->orderBy('created_at','desc')
									->take(6)
                                    ->get();
        if(count($result) > 0){
            $response = array('status_code' => '276','status' => 'success', 'notification_list' => $result);
        }
        else{
            $response = array('status_code' => '477','status' => 'failure', 'notification_list' => $result);   
        }
        return $response;
    }
	
	//viewAllNotification
    public static function viewAllNotification($details){
		$skip = 50 * $details['page_number'];
        $result = WebNotifications :: with('senderInfo')->with('firmInfo')
                                    ->where('reciever_id',$details['company_id'])
									->orderBy('created_at','desc')
									->take(50)->skip($skip)
                                    ->get(); 
        if(count($result) > 0){
            $response = array('status_code' => '276','status' => 'success', 'notification_list' => $result);
        }
        else{
            $response = array('status_code' => '476','status' => 'failure');   
        }
        return $response;
    }

	 //deleteNotification
    public static function deleteNotification($details){
        $result = WebNotifications :: where('_id',$details['notification_id'])
                                 ->delete();
    }


	//clearAllNotification
    public static function clearAllNotification($details){
        $result = WebNotifications :: where('reciever_id',$details['company_id'])
                                   ->update(array('seen_status' => true));
        if(count($result) > 0){
            $response = array('status_code' => '276','status' => 'success');
        }
        else{
            $response = array('status_code' => '477','status' => 'failure');   
        }
        return $response;
    }
	
	//clearAllNotification
    public static function clearAllJobNotification($details){
        $result = Notifications :: where('reciever_id',$details['user_id'])
		                           ->where('content_landing_id',$details['job_id'])
                                   ->update(array('seen_status' => true));
        if(count($result) > 0){
            $response = array('status_code' => '876','status' => 'success');
        }
        else{
            $response = array('status_code' => '877','status' => 'failure');   
        }
        return $response;
    } 
	
	//clearAllNotification
    public static function clearAllChatNotification($details){
        $result = Notifications :: where('reciever_id',$details['user_id'])
                                    ->where(function($query){
                                            $query->orWhere('notification_type',"private_chat");
                                            $query->orWhere('notification_type',"group_chat");
                                    })
                                    ->update(array('seen_status' => true));
        if(count($result) > 0){
            $response = array('status_code' => '876','status' => 'success');
        }
        else{
            $response = array('status_code' => '877','status' => 'failure');   
        }
        return $response;
    }
	
    //makeStatusTrue
    public static function makeStatusTrue($details){
        $result = Notifications :: where('_id',$details['notification_id'])
                                    ->update(array('seen_status' => true));

    }

    //allMyNotification
    public static function allMyNotification(){
        $result = Notifications :: where('reciever_id',$details['user_id'])
                                    ->get();
        if(count($result) > 0){
            $response = array('status_code' => '876','status' => 'success', 'notification_list' => $result);
        }
        else{
            $response = array('status_code' => '877','status' => 'failure');   
        }
        return $response;
    }

    //rejectLawFirmRequest
    public static function rejectLawFirmRequest($details){
        $result = Notifications :: where('_id',$details['notification_id'])
                                    ->update(array('seen_status' => true,'status' => 'rejected'));
        if($result){
            $response = array('status' => 'success', 'status_code' => '6543','response' => 'rejected the request');
        }
        else{
            $response = array('status' => 'failure', 'status_code' => '6541','response' => 'not able to reject');   
        }
        return $response;
    }

    //updateStatus
    public static function updateStatus($details){
        $result = Notifications :: where('_id',$details['notification_id'])
                                    ->update(array('seen_status' => true,'status' => $details['status']));
        if($result){
            $response = array('status' => 'success', 'status_code' => '6543','response' => 'accpeted the request');
        }
        else{
            $response = array('status' => 'failure', 'status_code' => '6541','response' => 'not able to reject');   
        }
        return $response;
    }

    //clearAParticularNotification
    public static function clearAParticularNotification($details){
         $result = Notifications :: where('_id',$details['notification_id'])
                                    ->update(array('seen_status' => true));
        if($result){
            $response = array('status' => 'success', 'status_code' => '6543','response' => 'notification made as read');
        }
        else{
            $response = array('status' => 'failure', 'status_code' => '6541','response' => 'not able to update');   
        }
        return $response;
    }
}