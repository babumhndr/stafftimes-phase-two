<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use App\seconds;
class  MotAdminActivity extends Eloquent
{
    protected $collection='mot_admin_activity';
    public static function addActivity($details)
	   {
    	$result = MotAdminActivity::where('activity_name',$details['activity_name'])->where('company_id',$details['company_id'])->get();
    	$count = count($result);
    	//check if activity name already exists
    	if ($count>0)
   		{
      		return array('status'=> 'failure','response'=>'Activity name already exists !');
    	}
    	else
    	{
			$res=MotAdminActivity::insert($details);
			
			if($res)
   			{
      			return array('status'=> 'success','response'=>'Activity Added');
    		}
    		else
    		{
      			return array('status'=> 'failure','response'=>'Error occured');
    		}
    	}
	}
	//updating of  default activity
    public static function updateDefaultActivity($details,$activity_id )
  	{
      $result = MotAdminActivity::where('activity_name',$details['activity_name'])->where('company_id',$details['company_id'])->get();
     /* return $result;*/
      $count = count($result);
      //check if activity name already exists
      if ($count>0 && $activity_id!=$result[0]['_id'])
      {
       return array('status'=> 'failure','response'=>'Activity name already exists !');
      }
      else
      {
           $res = MotAdminActivity::where('_id',$activity_id)->update($details);
      if($res)
      {
        return array('status'=> 'success','response'=>'Activity Updated');
      }
      else
      {
        return array('status'=> 'failure','response'=>'Error occured');
      } 
    }
  	}

    //updating of activity
    public static function updateCompanyActivity($details,$activity_id )
    {
     /* return $details;*/
      $res = MotAdminActivity::where('_id',$activity_id)->update($details);
      if($res)
      {
        return array('status'=> 'success','response'=>'Activity Updated');
      }
      else
      {
        return array('status'=> 'failure','response'=>'Error occured');
      } 
    }

    //getListOfActivity
    public static function getListOfActivity($details){
      $result = MotAdminActivity :: where('created_user_id',$details['user_id'])
                               ->get();
 /*     if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        $response = array('status' => 'failure','status_code' => '119','response' => []);
      }
*/
      return $result;
    }
//getListOfActivity
    public static function getListOfActivitysforPage($details){
      $result = MotAdminActivity :: where('created_user_id',$details['user_id'])->where( "deploy_status","!=","pending")->orderBy('activity_name', 'asc')->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        $response = array('status' => 'failure','status_code' => '119','response' => []);
      }

      return $response;
    }


    //getListOfMotActivity
    public static function getListOfMotActivity(){
       $result = MotAdminActivity :: where('created_by','MOT')
                                ->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;
    }

    //get list of activity for edit
    public static function getListOfActivitys($companyId,$id){
      $result = MotAdminActivity :: where('company_id',$companyId)->where('_id',$id)
                                    ->get();
      return $result;
  }
//getListOfMotActivitys
  public static function getListOfMotActivitys($id){
    $result = MotAdminActivity :: where('_id',$id)->get();
      return $result;
  }

  //for admin
  public static function getListOfMotActivityForAdmin($id){
    $result = MotAdminActivity :: where('company_id',$id)->get();
     if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '556','response' => $result);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '545','response' => []);
      }
      return $response;
  }
  public static function addActivityForRegisterUser($res,$language)
    {
      $activity=MotAdminActivity::where('deploy_status','!=','pending')->get();
      if ($language=="de") {
        for ($i=0; $i < sizeof($activity) ; $i++) { 
        $activity[$i]['activity_name']=$activity[$i]['activity_german_name'];
        $activity[$i]['company_id']=$res;
        $activity[$i]['created_user_id']=$res;
        $activity[$i]['created_by']='company';
        $activity[$i]['duplicate']=$activity[$i]['_id'];
        unset($activity[$i]['_id']);
        $array = json_decode(json_encode($activity[$i]), true);
        $insert=MotActivity::insert($array);
        }
       }
       else
       {
            for ($i=0; $i < sizeof($activity) ; $i++) { 
            $activity[$i]['activity_name']=$activity[$i]['activity_name'];
            $activity[$i]['company_id']=$res;
            $activity[$i]['created_user_id']=$res;
            $activity[$i]['created_by']='company';
            $activity[$i]['duplicate']=$activity[$i]['_id'];
            unset($activity[$i]['_id']);
            $array = json_decode(json_encode($activity[$i]), true);
            $insert=MotActivity::insert($array);
            }
       }
    }

      //update Annual Allowance InActivity
  public static function updateAnnualAllowanceInActivity($setWorkingHours,$user)
  {

    $allowanceActivity=MotAdminActivity::where('allowance_bank_days', '!=','0')->where('allowance_bank_days', '!=','0.00')->where('allowance_bank_hours', '!=','')->where('company_id',$user)->get();
  /*  return $allowanceActivity;*/
    function hrsToMin($time)
        {
          $time = explode(':', $time);
          return ($time[0]*60) + $time[1];
        }
        function minToHrs($time)
        {
          return gmdate("H:i", ($time * 60));
        }
    if (count($allowanceActivity)>0) 
    {
        for ($i=0; $i < sizeof($allowanceActivity); $i++) 
          { 
            $allowanceBankHours=$allowanceActivity[$i]['allowance_bank_hours'];
            $allowanceBankDays=hrsToMin($allowanceBankHours)/hrsToMin($setWorkingHours);
            $allowanceBankDays=number_format((float)$allowanceBankDays, 2, '.', '');
            $allowance= array('allowance_bank_days' => $allowanceBankDays);
            $updateAllowance=MotActivity::where('_id',$allowanceActivity[$i]['_id'])->update($allowance);
          }
    }
  }

  //update annual allowance by settings 
  public static function updateAnnualAllowanceByActivityId($activityId,$annualAllowanceForActivity)
  {
      $updateAnnualAllowance=MotAdminActivity::where('_id',$activityId)->update($annualAllowanceForActivity);
      if($updateAnnualAllowance){
        $response = array('status' => 'success');
      }
      else{
        $response = array('status' => 'failure');
      }
      return $response;
  }
    //change deploy status
  public static function changeStatusDeployActivity($id,$status)
  {
    $result=MotAdminActivity::where('_id',$id)->update($status);
    $company=Companies::where('admin','!=',1)->select('_id', 'language')->get();
    $activityToAdd=MotAdminActivity::where('_id',$id)->get();
    $activityId=$activityToAdd[0]['_id'];
    if (count($company)>0) 
    {
      for ($i=0; $i<sizeof($company); $i++) 
      { 
        $activityName="";
        if ($company[$i]['language']=='de') 
        {
          $activityName=$activityToAdd[0]['activity_german_name'];
        }
        else
        {
          $activityName=$activityToAdd[0]['activity_name'];
        }
          $activityToAdd[0]['activity_name']=$activityName;
          $activityToAdd[0]['company_id']=$company[$i]['_id'];
          $activityToAdd[0]['created_user_id']=$company[$i]['_id'];
          $activityToAdd[0]['created_by']='company';
          $activityToAdd[0]['deploy_status']='pending';
          $activityToAdd[0]['duplicate']=$activityId;
          unset($activityToAdd[0]['_id']);
          $array = json_decode(json_encode($activityToAdd[0]), true);
          $insert=MotActivity::insert($array);
      }
    }
    if ($result) 
      {
        $response = array('status' => 'success','status_code' => '998');
      }
    else{ 
        $response = array('status' => 'failure','status_code' => '888');
      }
      return $response;
  }
}
