<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
class MotTemplates extends Eloquent
{
  protected $collection='mot_templates';

    //activity
  public function activity(){
    return $this->hasMany('App\ActivitiesMappedToTemplate','template_id','_id');
  }

        //adding template
	public static function addTemplate($details)
	{
    	$result = MotTemplates::where('template_name',$details['template_name'])->where('company_id',$details['company_id'])->get();
    	$count = count($result);
    	//check if template name already exists
    	if ($count>0)
   		{
      		return array('status'=> 'failure','response'=>'Template name already exists !');
    	}
    	else
    	{
			   //insert to mottemplates table
          $template = new MotTemplates;
          $template->template_name = $details['template_name'];
          $template->company_id = $details['company_id'];
          $template->template_offset_time = $details['template_offset_time'];
          $template->created_on = $details['created_on'];
          $template->created_by = $details['created_by'];
          $template->created_user_id = $details['created_user_id'];
          $template->deploy_status = $details['deploy_status'];
          $template->save();
          $tempalteId = $template->id;
    /*   return $templateId;*/
			if($tempalteId)
   			{
            $details['template_id'] = $tempalteId;

            $totalNumberOfActivity = $details['number_of_activity'];
            for($i=0;$i<$totalNumberOfActivity;$i++){
              //insert to activity mapped to templates table
              $activityId=Input::get('activity_id_'.$i.'');
              $activityFlatHoursVal=FileUpload::replaceDotToColon(Input::get('activity_flat_hours_'.$i.''));
              $activityBreakVal=FileUpload::replaceDotToColon(Input::get('activity_break_'.$i.''));
              $activityAmount=FileUpload::replaceDotToComma(Input::get('activity_amount_'.$i.''));
              $activityDetails = array('activity_id' => $activityId,
                                        'company_id' => $details['company_id'],
                                        'activity_flat_hours' => $activityFlatHoursVal,
                                        'activity_amount' => $activityAmount ,
                                        'activity_start' =>  FileUpload::hoursToAmPm(Input::get('activity_start_'.$i.'')),
                                        'activity_end' =>  FileUpload::hoursToAmPm(Input::get('activity_end_'.$i.'')),
                                        'activity_break' =>$activityBreakVal,
                                        'template_id' => $details['template_id']);
              if ($activityId != null) 
              {
                $result = ActivitiesMappedToTemplate :: addActivityMappedToTempalte($activityDetails);
              }
              
            }
            return array('status'=> 'success','response'=>'Template Added');
      			

    		}
    		else
    		{
      			return array('status'=> 'failure','response'=>'Error occured');
    		}
    	}
	}
//updating template
  public static function updateTemplate($details,$template_id,$number_of_activity)
  {
    $result = MotTemplates::where('template_name',$details['template_name'])->where('company_id',$details['company_id'])->get();
      $count = count($result);
      //check if template name already exists
     if ($count>0 && $template_id!=$result[0]['_id'])
      {
        return array('status'=> 'failure','response'=>'Template name already exists !');
      }
      else
      {
         $result1 = MotTemplates::where('_id',$template_id)->update($details);
         $deletePreviousMappedActivity=ActivitiesMappedToTemplate::where('template_id',$template_id)->delete();
    if($result1)
        {
           /* $totalNumberOfActivity = $details['number_of_activity'];*/
            for($i=0;$i<$number_of_activity;$i++){
              //insert to activity mapped to templates table

         /*     $activityDetails = array('activity_id' => Input::get('activity_id_'.$i.''),
                                        'activity_flat_hours' =>  Input::get('activity_flat_hours_'.$i.''),
                                        'activity_amount' =>  Input::get('activity_amount_'.$i.''),
                                        'activity_start' =>  Input::get('activity_start_'.$i.''),
                                        'activity_end' =>  Input::get('activity_end_'.$i.''),
                                        'activity_break' =>  Input::get('activity_break_'.$i.''),
                                        'template_id' => $template_id);*/
                $activityId=Input::get('activity_id_'.$i.'');
               $activityFlatHoursVal=FileUpload::replaceDotToColon(Input::get('activity_flat_hours_'.$i.''));
              $activityBreakVal=FileUpload::replaceDotToColon(Input::get('activity_break_'.$i.''));
                $activityAmount=FileUpload::replaceDotToComma(Input::get('activity_amount_'.$i.''));
              $activityDetails = array('activity_id' => $activityId,
                                        'company_id' => $details['company_id'],
                                        'activity_flat_hours' => $activityFlatHoursVal,
                                        'activity_amount' => $activityAmount,
                                        'activity_start' =>  FileUpload::hoursToAmPm(Input::get('activity_start_'.$i.'')),
                                        'activity_end' =>  FileUpload::hoursToAmPm(Input::get('activity_end_'.$i.'')),
                                        'activity_break' =>$activityBreakVal,
                                         'template_id' => $template_id);
             /* return $activityDetails;*/
           if ($activityId != null) 
              {
                $result = ActivitiesMappedToTemplate :: addActivityMappedToTempalte($activityDetails);
              }
              
            }
           /* $responseToSend=array('template_added_change'=>true,
                                  'message'=>'Template Updated');
            Employee::sendNotificationByFcmId($responseToSend);*/
            return array('status'=> 'success','response'=>'Template Updated');
        }
        else
        {
            return array('status'=> 'failure','response'=>'Error occured');
        }
      }
  }
  //update company template
   public static function updateCompanyTemplate($details,$template_id )
  {
    $result = MotTemplates::where('_id',$template_id)->update($details);
    if($result)
    {
      return array('status'=> 'success','response'=>'Template Updated');
    }
    else
    {
      return array('status'=> 'failure','response'=>'Error occured');
    } 
  }

  //update deafult template
  public static function updateDefaultTemplate($details,$template_id )
  {
   	$result = MotTemplates::where('_id',$template_id)->update($details);
    if($result)
    {
      return array('status'=> 'success','response'=>'Template Updated');
    }
    else
    {
      return array('status'=> 'failure','response'=>'Error occured');
    } 
  }

  //getListOfTemplate
  public static function getListOfCompanyTemplate($companyId){
      $result = MotTemplates :: with('activity.activityDetails')
                                ->where('created_user_id',$companyId)
                                ->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;

  }

  //getListOfTemplate
  public static function getListOfTemplates($companyId,$id){
      $result = MotTemplates :: with('activity.activityDetails')->where('company_id',$companyId)->where('_id',$id)->get();
      return $result;

  }

  //getListOfMotTemplates
  public static function getListOfMotTemplates($id){
    $result = MotTemplates :: with('activity.activityDetails')->where('_id',$id)->where('created_by','MOT')
                                    
                                    ->get();
      return $result;
  }

  //getListOfMotTemplate
  public static function getListOfMotTemplate(){
      $result = MotTemplates :: with('activity.activityDetails')
                                ->where('created_by','MOT')
                                ->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;

  }

  //getListOfTemplateNew
  public static function getListOfTemplateForApp($details){
      $result = MotTemplates :: with('activity.activityDetails')
                                ->whereIn('created_user_id',array($details['user_id'],$details['superadmin_id'],$details['company_id']))
                                ->where( "deploy_status","deployed")->get();
      return $result;

  }

  //get list of template for settings
  public static function getListTemplate($details)
  {  
    $result = MotTemplates :: where('created_user_id',$details['user_id'])->where( "deploy_status","!=","pending")->get();
    return $result;
    /*$result = MotTemplates :: where('created_user_id',$id)
                                ->get();
    $result1=MotTemplates::where('created_by','MOT')->get();
    $combined = array_merge(array($result),array($result1));
    return $combined;*/ 
  }

  //change deploy status
  public static function changeStatusDeploy($id,$status)
  { 
    $getActivityMappedToTemplate=ActivitiesMappedToTemplate::where('template_id',$id)->get();
    for ($i=0; $i <sizeof($getActivityMappedToTemplate) ; $i++) { 
      $activityID=$getActivityMappedToTemplate[$i]['activity_id'];
      $activity=MotActivity::where('_id',$activityID)->get();
      if ($activity[0]['deploy_status'] == "pending") {
        $response = array('status' => 'failure','status_code' => '800','data'=>"deploy activity");
        return $response;
      }
    }
    $result=MotTemplates::where('_id',$id)->update($status);
    if ($result) 
      {
        $response = array('status' => 'success','status_code' => '998');
         $responseToSend=array('template_added_change'=>true,
                                  'message'=>'Template Added');
        Employee::sendNotificationByFcmId($responseToSend);
      }
    else{ 
        $response = array('status' => 'failure','status_code' => '888');
      }
      return $response;
  }

    //add template in app
  public static function addTemplateByApp($details)
  {
      $result = MotTemplates::where('template_name',$details['template_name'])->where('company_id',$details['company_id'])->get();
      $result1= MotTemplates::where('template_name',$details['template_name'])->where('created_user_id',$details['created_user_id'])->get();
      /*return $result;*/
      $count = count($result);
      $count1 = count($result1);
      //check if template name already exists
      if ($count>0 || $count1>0)
      {
          return array('status'=> 'failure','response'=>'Template name already exists !','status_code' => '899');
      }
      else
      {
         //insert to mottemplates table
          $template = new MotTemplates;
          $template->template_name = $details['template_name'];
          $template->template_offset_time = $details['template_offset_time'];
          $template->company_id = $details['company_id'];
          $template->created_on = $details['created_on'];
          $template->created_by = $details['created_by'];
          $template->created_user_id = $details['created_user_id'];
          $template->save();
          $tempalteId = $template->id;
          /*return $tempalteId;*/
          $result = MotTemplates::where('_id',$tempalteId)->get();
          return array('status'=> 'success','response'=>'Template added !','status_code' => '444','data'=>$result[0]);
      }
  }
}
