<?php
namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Mail;
use Illuminate\Http\Response;
use Illuminate\Mail\Message;

class MailSend extends Eloquent
{ 
  //email verification mail sender for company
  public static function sendMail($details)
  {
    $data = ['from'=>'support@stafftimes.com',
             'to' => $details['email'],
             'name'=>$details['company_name']
            ];

    Mail::send('emails.welcome',array('name' => $details['company_name'],
                                      'token' => base64_encode ($details['token']),
                                      'email'=>base64_encode ($details['email']),
                                      'language'=>base64_encode ($details['language'])
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject(trans('welcome_e.welcome'));
        });  
  }
  //invite mail sender
  public static function sendInviteMail($details)
  {
    $data = ['from'=>'support@stafftimes.com',
             'to' => $details['email'],
             'name'=>$details['name']
            ];

    Mail::send('emails.invite',array('name' => $details['name'],
                                      'token' => base64_encode ($details['token']),
                                      'email'=>base64_encode ($details['email'])
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject(trans('invite_e.subject')); 
        });  
  }

  //payment reciept mail sender
  public static function paymentRecipt($details)
  {
    $data = ['from'=>'support@stafftimes.com',
             'to' => $details['email'],
             'name'=>$details['name']
            ];

    Mail::send('emails.payment',array('name' => $details['name'],
                                      'amount' => $details['amount'],
                                      'currency' => $details['currency'],
                                      'order_no' => $details['order_no'],
                                      'date' => $details['date'],
                                      'plan' => $details['plan'],
                                      'email'=>$details['email']
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject(trans("translate.payment")); 
        });  
  }
   public static function resetMail($res)
  {
 /*   return $res;*/
    $data = ['from'=>'support@stafftimes.com',
             'to' => $res[0]['email'],
             'name'=>$res[0]['company_name']
            ];
       /*     return $res[0]['email'];*/
    Mail::send('emails.passwordReset',array('name' => $res[0]['company_name'],
                                      'email'=>base64_encode ($res[0]['email'])
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject(trans('translate.reset_password'));
        });  
  }

  //send contact us mail
  public static function sendContactUsMail($details)
  {
    $data = ['from'=>$details['email'],
             'to' => 'support@stafftimes.com',
             'name'=>$details['name']
            ];
   $data1 = ['from'=>'support@stafftimes.com',
             'to' => $details['email'],
             'name'=>$details['name']
            ];
    Mail::send('emails.contactus',array('name' => $details['name'],
                                      'phone' => $details['phone'],
                                      'purpose' => $details['purpose'],
                                      'usermessage' => $details['message'],
                                      'email'=> $details['email']
                                     ),function($message) use ($data)
        {
         $message->from($data['from'],$data['name']);
         $message->to($data['to'])->subject('Contact Us');
        }); 
    Mail::send('emails.contactusto_sender',array('name' => $details['name']
                                     ),function($message) use ($data1)
        {
         $message->from($data1['from'],'Stafftimes');
         $message->to($data1['to'])->subject('Staff Times Support');
        }); 
  }
    //send contact us mail
  public static function newsletterMail($details)
  {
    $data = ['from'=>$details['mail'],
             'to' =>'support@stafftimes.com',
            ];
   $data1 = ['from'=>'support@stafftimes.com',
             'to' => $details['mail'],
            ];
    Mail::send('emails.newsletter',array('mail'=> $details['mail']
                                     ),function($message) use ($data)
        {
         $message->from($data['from']);
         $message->to($data['to'])->subject(trans('translate.book_a_demo_sub'));
        }); 
    Mail::send('emails.newsletter_sender',array('mail' => $details['mail']
                                     ),function($message) use ($data1)
        {
         $message->from($data1['from'],'Stafftimes');
         $message->to($data1['to'])->subject('Staff Times Support');
        }); 
  }

  //15days reminder 
  public static function FifteenDaysReminder($details)
  {
    $data = ['from'=>'support@stafftimes.com',
             'to' => $details['email'],
             'name'=>$details['name'],
             'subject'=>$details['subject'],
            ];
     if($details['lang'] == 'English'){
      Mail::send('emails.15days_trail_mail_english',array('name' => $details['name'],
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject($data['subject']);
        });  
    } 
    else{
      Mail::send('emails.15days_trail_mail_german',array('name' => $details['name'],
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject($data['subject']);
        });  
    }
    
  } 
  //15days reminder 
  public static function SevenDaysReminder($details) 
  {
    $data = ['from'=>'support@stafftimes.com',
             'to' => $details['email'],
             'name'=>$details['name'],
             'subject'=>$details['subject'],
            ];
    if($details['lang'] == 'English'){
      Mail::send('emails.7days_trail_mail_english',array('name' => $details['name'],
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject($data['subject']);
        });  
    } 
    else{
      Mail::send('emails.7days_trail_mail_german',array('name' => $details['name'],
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject($data['subject']);
        });  
    }
    
  }
  //15days reminder 
  public static function OneDayReminder($details)
  {
    $data = ['from'=>'support@stafftimes.com',
             'to' => $details['email'],
             'name'=>$details['name'],
             'subject'=>$details['subject'],
            ];
    if($details['lang'] == 'English'){
      Mail::send('emails.1day_trail_mail_english',array('name' => $details['name'],
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject($data['subject']);
        }); 
    } 
    else{
      Mail::send('emails.1day_trail_mail_german',array('name' => $details['name'],
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject($data['subject']);
        }); 
    }

  }

  //email verification mail sender for company
  public static function testApi()
  {
    $data = ['from'=>'support@stafftimes.com',
             'to' => 'babin@codewave.in',
             'name'=>'Babin'
            ];

    Mail::send('emails.welcome',array('name' => "BABin",
                                      'token' => base64_encode ('fggfgfgf'),
                                      'email'=>base64_encode ('fdgfgfgd'),
                                      'language'=>base64_encode ('en')
                                     ),function($message) use ($data)
        {
         $message->from($data['from'], 'Staff Times');
         $message->to($data['to'],$data['name'])->subject(trans('welcome_e.welcome'));
        });  
  }
}

