<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoId;
use Auth;
use File;
use Zipper;
use Storage;
class BackUp extends Eloquent
{
	public static function uploadCsvToDatabase($data,$filename)
	{ 
		/*$data=$data->toArray();*/
		if ($filename=='company_detail.json') 
		{
			if (sizeof($data)>0) 
			{
				$user =Auth::user();
				/*return $user['id'];*/
				for ($i=0; $i <sizeof($data) ; $i++) 
				{ 
					if ($user['id']!=$data[$i]['_id']) 
					{
						return array('status'=> 'failure','response'=> 'Please upload the correct folder');
					}
					$result=Companies::where('_id',$data[$i]['_id'])->get();
					if (count($result)>0) 
					{	
						$id=$data[$i]['_id'];
						unset($data[$i]['_id']);
						$result=Companies::where('_id',$id)->update($data[$i]);
					}
					/*else
					{
						 return array('status'=> 'failure','response'=> 'Upload Correct file');
					}*/
					/*$data[$i]['_id']=new MongoId($data[$i]['_id']);
					Companies::insert($data[$i]);*/
					/*return $data[$i]['_id'];*/
				}
			}
		}
		else if ($filename=='company_employee_detail.json') 
		{
			
			if (sizeof($data)>0) 
			{
				for ($i=0; $i <sizeof($data) ; $i++) 
				{ 
					$result=Employee::where('_id',$data[$i]['_id'])->delete();
					$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
					Employee::insert($data[$i]);
				}
			}
		}
		else if ($filename=='company_activities_mapped_to_template_detail.json') 
		{

			if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=ActivitiesMappedToTemplate::where('_id',$data[$i]['_id'])->delete();
				$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				ActivitiesMappedToTemplate::insert($data[$i]);
			}
			}
		}
		else if ($filename=='company_activity_detail.json') {
			
			if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=MotActivity::where('_id',$data[$i]['_id'])->delete();
				$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				MotActivity::insert($data[$i]);
			}
			}
			
		}
		else if ($filename=='company_condition_mapped_to_working_days_settings_detail.json') {
			
			if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) {
				$result=ConditionForWorkingDaysSetting::where('_id',$data[$i]['_id'])->delete(); 
				$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				ConditionForWorkingDaysSetting::insert($data[$i]);
			}
			}
			
		}
		else if ($filename=='company_general_settings_detail.json') {
		
			if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=GeneralSetting::where('_id',$data[$i]['_id'])->delete();
					$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				GeneralSetting::insert($data[$i]);
			}
			}
			
		}
		else if ($filename=='company_overtime_handling_detail.json') {
			
			if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=OvertimeHandling::where('_id',$data[$i]['_id'])->delete();
					$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				OvertimeHandling::insert($data[$i]);
			}
			}
			
		}
		else if ($filename=='company_set_annual_allowance_detail.json') {
			
			if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=SetAnnualAllowance::where('_id',$data[$i]['_id'])->delete();
					$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				SetAnnualAllowance::insert($data[$i]);
			}
			}
			
		}
		else if ($filename=='company_templates_detail.json') {
			
			if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=MotTemplates::where('_id',$data[$i]['_id'])->delete();
					$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				MotTemplates::insert($data[$i]);
			}
			}
			
		}
		else if ($filename=='company_timesheet_detail.json') {
		
				if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=Timesheet::where('_id',$data[$i]['_id'])->delete();
					$data[$i]['_id']=new MongoId($data[$i]['_id']);
					$data[$i]['rate']=number_format($data[$i]['rate'],3);
					unset($data[$i]['id']);
				Timesheet::insert($data[$i]);
			}
			}
			
		}
		else if ($filename=='company_working_days_settings_detail.json') {
			
				if (sizeof($data)>0) {
				for ($i=0; $i <sizeof($data) ; $i++) { 
					$result=WorkingDaysSetting::where('_id',$data[$i]['_id'])->delete();
					$data[$i]['_id']=new MongoId($data[$i]['_id']);
					unset($data[$i]['id']);
				WorkingDaysSetting::insert($data[$i]);
			}
			}
		return array('status'=> 'success','response'=>'Imported');
		}
		else
		{
			return false;
		}
	}
	public static function downloadToJson(){	
		$totalCompanies = Companies::get();
		$timstamp=date("Y-m-d H:i",time());
		$allExportPath=storage_path('app/daily-bkp/exports');
		File::cleanDirectory($allExportPath);
		for($i=0;$i<count($totalCompanies);$i++)
		{
			$xlsPath = storage_path('../../local/storage/app/daily-bkp/exports/'.$totalCompanies[$i]['_id']);
			//delete if any data inside the folder
			$jsonPath = storage_path('app/daily-bkp/exports/'.$totalCompanies[$i]['_id']);
			File::makeDirectory($jsonPath);
			//save Company data
			$data = Companies::where('_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_detail.json',json_encode($data));
			//save Company data

			//save Employee data
			$data = Employee::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_employee_detail.json',json_encode($data));
			//save Employee data

			//save GeneralSetting data
			$data = GeneralSetting::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_general_settings_detail.json',json_encode($data));
			//save GeneralSetting data

			//save OvertimeHandling data
			$data = OvertimeHandling::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_overtime_handling_detail.json',json_encode($data));
			//save OvertimeHandling data

			//save SetAnnualAllowance data
			$data = SetAnnualAllowance::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_set_annual_allowance_detail.json',json_encode($data));
			//save SetAnnualAllowance data

			//save WorkingDaysSetting data
			$data = WorkingDaysSetting::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_working_days_settings_detail.json',json_encode($data));
			//save WorkingDaysSetting data

			//save ConditionForWorkingDaysSetting data
			$data = ConditionForWorkingDaysSetting::where('companyid',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_condition_mapped_to_working_days_settings_detail.json',json_encode($data));
			//save ConditionForWorkingDaysSetting data

			//save Timesheet data
			$data = Timesheet::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_timesheet_detail.json',json_encode($data));
			//save Timesheet data

			//save MotActivity data
			$data = MotActivity::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_activity_detail.json',json_encode($data));
			//save MotActivity data

			//save ActivitiesMappedToTemplate data
			$data = ActivitiesMappedToTemplate::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_activities_mapped_to_template_detail.json',json_encode($data));
			//save ActivitiesMappedToTemplate data

			//save MotTemplates data
			$data = MotTemplates::where('company_id',$totalCompanies[$i]['_id'])->get()->toArray();
			$data= FileUpload::En($data);
			$file=File::put($jsonPath.'/company_templates_detail.json',json_encode($data));
			//save MotTemplates data

			//create a zip file
			$files = glob(storage_path('../../local/storage/app/daily-bkp/exports/'.$totalCompanies[$i]['_id'].'/*'));
   			Zipper::make(storage_path('../../local/storage/app/daily-bkp/exports/'.$totalCompanies[$i]['_id'].'_.zip'))->add($files)->close();

   			//upload
   			Storage::disk('s3')->makeDirectory($totalCompanies[$i]['_id']);
			$path='daily-bkp/exports/'.$totalCompanies[$i]['_id'].'_.zip';
			$contents = Storage::disk('local')->get($path);
			$storage=Storage::disk('s3')->put($totalCompanies[$i]['_id'].'/'.$timstamp.'.zip', $contents,'public');
			/*var_dump($storage);*/
		}
	}
	public static function cronTest()
	{
		$array = array('test' => 'test');
		WebNotifications::insert($array);
	}

	/*public static function takeADBBackupofLiveServer(){
			$date = date('d-m-Y');
			Storage::disk('s3')->makeDirectory('staffimes-live-DB');
			$path='/database_bkp/live_server/compressed_database_'.$date.'.zip';
			$contents = Storage::disk('local')->get($path);
			$storage=Storage::disk('s3')->put('staffimes-live-DB/compressed_database_'.$date.'.zip', $contents,'public');
	}*/

	public static function takeADBBackupofLiveServer(){
			$date = date('d-m-Y');
			Storage::disk('s3')->makeDirectory('staffimes-live-DB');
			$path='/database_bkp/live_server/compressed_database_'.$date.'.zip';
			$contents = Storage::disk('local')->get($path);
			$storage=Storage::disk('s3')->put('staffimes-live-DB/compressed_database_'.$date.'.zip', $contents,'public');

		}
	
}
/*

$fileNameCsv=array('0' =>'company_detail.json',
						'1'=>'company_employee_detail.json',
						'2'=>'company_activities_mapped_to_template_detail.json',
						'3'=>'company_activity_detail.json',
						'4'=>'company_condition_mapped_to_working_days_settings_detail.json',
						'5'=>'company_general_settings_detail.json',
						'6'=>'company_overtime_handling_detail.json',
						'7'=>'company_set_annual_allowance_detail.json',
						'8'=>'company_templates_detail.json',
						'9'=>'company_timesheet_detail.json',
						'10'=>'company_working_days_settings_detail.json');*/