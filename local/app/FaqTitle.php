<?php

namespace App;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class FaqTitle extends Eloquent 
{
	protected $collection='mot_faq_topic';

	 public static function addTopic($details){
    	$res=FaqTitle::insert($details);
    		return array('status'=> 'success','response'=>'great');
    }
     public static function deleteTopic($id){
        $res=FaqTitle::where('_id',$id)->delete();
        /*return $res;*/
            return array('status'=> 'success','response'=>"great");
    }
    public static function editTopic($id){
        $res=FaqTitle::where('_id',$id)->get();
        return $res;
    }

    public function FaqDetails(){
    	return $this->hasMany('App\Faq','topic_id');
    }

    public static function GetDetails(){
    	$res=FaqTitle::with('FaqDetails')->get();
    	return $res;
    }
	/*FAQ Listing*/
	/*public function faq(){
    return $this->hasMany('App\faqListMappedToFaqTitle','faq_id','_id');*/
	
}