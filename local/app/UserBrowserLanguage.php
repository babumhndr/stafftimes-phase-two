<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests;
use Config;
use Illuminate\Support\Facades\Session;
use Auth;

class UserBrowserLanguage extends Model
{
   public static function browserLanguage()
	{
		$UserLanguage = session()->get('UserLanguage');
		if ($UserLanguage != "") 
		{
			return UserBrowserLanguage::correctLangToDeOrEn($UserLanguage);
		}
		else 
		{
			$countryCodeFromIp='';
			//Get user ip address
			$ip_address=$_SERVER['REMOTE_ADDR'];
			//echo $ip_address;

			//Get user ip address details with geoplugin.net
			/*$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
			$addrDetailsArr = unserialize(file_get_contents($geopluginURL));
			$countryCodeFromIp=$addrDetailsArr['geoplugin_countryCode'];*/
			$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip_address));    
		    if($ip_data && $ip_data->geoplugin_countryCode != null){
		        $countryCodeFromIp = $ip_data->geoplugin_countryCode;
		    }
			if ($countryCodeFromIp=="") {
				$countryCodeFromIp="en";
			}
			$countryCodeFromIp=strtolower($countryCodeFromIp);
			$countryCode=Lang::get([$countryCodeFromIp]);
			$countryCode=explode(",",$countryCode[0][$countryCodeFromIp]);
			session()->put('UserLanguage', $countryCode[0]);
			$UserLanguage = session()->get('UserLanguage');
			return UserBrowserLanguage::correctLangToDeOrEn($UserLanguage);
		}

	}
	//change if language other than en or de
	public static function correctLangToDeOrEn($lang)
	{
		if ($lang=="en") {
			return $lang;
		}
		else if ( $lang=="de") {
			return $lang;
		}
		else
		{
			return 'en';
		}
	}
	public static function userLanguage()
	{
		$user=Auth::user();
		$adminUserLanguage=$user['language'];
		if ($adminUserLanguage=="") {
				$adminUserLanguage="en";
			}
		return $adminUserLanguage;
	}

}
