<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use Excel;
use File;
use Storage;
class MyovertimeUsers extends Eloquent
{
  protected $collection='myovertime_users';

    //adding template
	public static function insertUser($details)
	{
    	//check if email is already there
    	$userExist = MyovertimeUsers :: where('email_id',$details['email_id'])->get();
    	if(count($userExist) > 0){
    		return array('status' => 'failure','status_code' => '225','response' => 'already exist');
    	}
    	else{
    		$user = new MyovertimeUsers;
    		$user->email_id = $details['email_id'];
    		$user->name = $details['name'];
    		$user->language = $details['language'];
    		$user->save();
    		//save in a excel sheet
    		$file_path = storage_path('app/myovertimeusers/myovertimeuser.xls');
    		$filename = "myovertimeuser";
    		$ext = 'xls';
    		$data = $details;
			if(! File::exists($file_path)){
				
            $excelFile = Excel::create($filename, function ($excel) use($data)
            {
                $excel->setTitle('Export');
                $excel->setCreator('Meta System')->setCompany('Company Sdn Bhd.');
                $excel->setDescription('Export Master Data');
                $excel->sheet('Sheet1', function ($sheet) use($data)
                {	
                	$headers = array('Email Id','Name','Language');
                    $sheet->fromArray($data, $headers, 'A1', false, false);
                });
            })->store($ext, storage_path('app/myovertimeusers'), true);
        }else{
        	$rows = \Excel::load($file_path, function($reader) {})->get();

			$totalRows = $rows->count();
			$row = $totalRows + 2;
            $excelFile = Excel::load($file_path,  function ($reader) use($data,$row) {
                $reader->sheet('Sheet1', function($sheet) use($data,$row){
                    $sheet->appendRow($row,$data);
                });
            })->store($ext, storage_path('app/myovertimeusers'), true);
        }
        Storage::disk('s3')->makeDirectory('MyOvertime Users');
			$path='myovertimeusers/myovertimeuser.xls';
			$contents = Storage::disk('local')->get($path);
			$storage=Storage::disk('s3')->put('MyOvertime Users/myovertimeuser.xls', $contents,'public');
        	

		
    		return array('status' => 'success','status_code' => '215','response' => 'added');
    	}
	}

}
