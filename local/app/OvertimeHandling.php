<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use App\ConditionForWorkingDaysSetting;//model

class OvertimeHandling extends Eloquent
{ 
  protected $collection='mot_overtime_handling';

  //insert setOvertimeHandling
  public static function setOvertimeHandling($overtime_handling_setting_details)
  {
    $result=OvertimeHandling::insert($overtime_handling_setting_details);
   if($result)
      {
      $response = array('status' => 'success','status_code' => '119','response' =>'Saved');
      $responseToSend=array('overtime_handling_setting_change'=>true,
                            'message'=>'overtime_handling_setting_changed');
       Employee::sendNotificationByFcmId($responseToSend);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => 'Error');
      }
      return $response;
  }
 
  //update setOvertimeHandling
  public static function updateOvertimeHandling($overtime_handling_setting_details)
  {
    $result=OvertimeHandling::where('companyid',$overtime_handling_setting_details['companyid'])->update($overtime_handling_setting_details);
   if($result)
      {
      $response = array('status' => 'success','status_code' => '119','response' =>'Saved');
      $responseToSend=array('overtime_handling_setting_change'=>true,
                            'message'=>'overtime_handling_setting_changed');
       Employee::sendNotificationByFcmId($responseToSend);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => 'Error');
      }
      return $response;
  }

  //setting listing
  public static function overtimeHandlingList($data)
  {
    $details=OvertimeHandling::where('companyid',$data['user_id'])->get();
    if(count($details) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $details);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;
  }

  //list for mobile
  public static function overtimeHandlingSettingValues($companyid)
  {
    $result=OvertimeHandling::where('companyid',$companyid)->get();
    if(count($result) > 0){
          $response = array('status' => 'success','status_code' => '234','response' => $result);

      }
      else{
        $response = array('status' => 'failure','status_code' => '564','response' =>[]);
      }
      return $response;
  }
}


      