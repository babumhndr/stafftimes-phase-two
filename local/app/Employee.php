<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Auth;
use Redirect;

class Employee extends Eloquent
{ 
  protected $collection='mot_employees';
  //check user
  public static function validateUser($userId)
  {
    /*return $userId;*/
    $employeeDetails=Employee::where('_id',$userId)->get();
    return $employeeDetails;
  }
  //get employee by auth 
  public static function getUser()
  {
    /*return $userId;*/
    $companyId=Auth::user();
   /* return $companyId['_id'];*/
    $employeeDetails=Employee::where('company_id',$companyId['_id'])->get();
    return $employeeDetails;
  }
  //invite employee from dashboard
  public static function invitePeople($details)
  {
    $result = Employee::where('email',$details['email'])->get();
    $count = count($result);
    if ($count>0)
    {
      return array('status'=> 'failure','response'=> trans('popup.already_registered'));
    }
    else
    {
      $res=Employee::insert($details);
      if($res)
      {
        $sendMailTo=MailSend::sendInviteMail($details);
        return array('status'=> 'success','response'=>'Invite has been sent!');
      }
    }
  }

   //payment status update
  public static function updatepaymentEmployee($details)
  {
    $detail=array('payment_status'=> 'active','subscription_start_date'=> $details['created_on'],'subscription_end_date'=> $details['end_date']);
     $employeeIds = explode(',', $details['employeeIds']);
      //return count($specializationAreas);
      for($i=0;$i<count($employeeIds);$i++){
          $finalemployeeId[$i] = $employeeIds[$i];  
          
          $result=Employee::where('_id', $finalemployeeId[$i])->update($detail); 
      }
    
    if($result)
    {
      return 'success';
    }
    else
    {
      return 'failure';
    }
  }
  //verification of invite
  public static function inviteVerification($details)
  {
    $result = Employee::where('email',$details['email'])->where('token',$details['token'])->get();
    $count = count($result);
    if ($count>0)
    {
      $userDetails=Companies::where('_id',$result[0]['company_id'])->get();
      $paymentStatus=$userDetails[0]['payment_status'];
      $trailStatus=$userDetails[0]['trail_status'];
      if ($paymentStatus=="pending") 
      {
        if ($trailStatus=="expired")
          {
              return array('status'=> 'failure','code'=>330,'response'=>'Trial expired'); 
          }
      }
      else if ($paymentStatus=="expired")  
        {
          return array('status'=> 'failure','code'=>340,'response'=>'Payment status expired'); 
        }
      $authKey='';
      $res=Employee::where('email',$details['email'])->where('token',$details['token'])
      ->update(array('invite_verified' => true,
                      'trail_status'=>'active',
                      'status'=>'active'));
      $employeeStatusDetail=Employee::where('email',$details['email'])->where('token',$details['token'])
      ->get();
      $employeeStatusDetail[0]['company_name']=$trailStatus=$userDetails[0]['company_name'];
      if ($employeeStatusDetail[0]['status']=='active') 
      {
        $authKey=$employeeStatusDetail[0]['_id']."#myovertime#employee";
        $employeeStatusDetail[0]['authKey']=base64_encode($authKey);
      }
      else
      {
        $employeeStatusDetail[0]['authKey']=$authKey;
      }
      return array('status'=> 'success','code'=>200 ,'response'=>'Invite token is Verified','employeeData'=> $employeeStatusDetail); 
    }
    else
    {
      return array('status'=> 'failure','code'=>300,'response'=>'Invalid invite token'); 
    }
  }
  //update of employee detail
  public static function employeeRegister($details)
  {
    $res=Employee::where('email',$details['email'])->update($details);
/*    return $res1;*/
    if($res)
    {
      $employeeDetail=Employee::where('email',$details['email'])->get();
      $authKey=$employeeDetail[0]['_id']."#myovertime#employee";
      $employeeDetail[0]['authKey']=base64_encode($authKey);
      return array('status'=> 'success','code'=>200,'response'=>'Register successful','employee_data'=>$employeeDetail);
    }
    else
    {
      return array('status'=> 'failure','code'=>300,'response'=>'Invalid details'); 
    }
  }
  public static function employeeLogin($details)
  {
    $result=Employee::where('email',$details['email'])->where('password',$details['password'])->get();
    $count=sizeof($result);
    if($count>0)
    {
      $authKey=$result[0]['_id']."#myovertime#employee";
      $result[0]['authKey']=base64_encode($authKey);
      return array('status'=> 'success','code'=>200,'response'=>'Login successful','employee_data'=>$result);
    }
    else
    {
      return array('status'=> 'failure','code'=>300,'response'=>'Invalid login details'); 
    }
  }
  public static function editEmployeeDetails($authKey,$details)
  {
    /*return $details;*/
       $authKey = explode("#",base64_decode($authKey));
       $userId = $authKey[0];
     $res=Employee::where('_id',$userId)->update($details);
    /*    return $res1;*/
    if($res)
    {
       $employeeDetail=Employee::where('_id',$userId)->get();
      return array('status'=> 'success','code'=>200,'response'=>'Details updated successful','employee_data'=>$employeeDetail);
    }
    else
    {
      return array('status'=> 'failure','code'=>300,'response'=>'Invalid details'); 
    }
  }

  //validateEmployee
  public static function validateEmployee($userId){
      $result = Employee :: where('_id',$userId)
                            ->where('status','active')
                            ->get();
      if(count($result) > 0){
          $response = array('status' => 'success','status_code' => '228','response' => $result);

      }
      else{
        $response = array('status' => 'failure','status_code' => '828','response' => 'not a valid user');
      }
      return $response;
  }

  //insert fcm id
  public static function insertFcmId($userId,$fcmId)
  {
      $fcmIdArray=array('fcm_id' =>$fcmId);
      $result=Employee::where('_id',$userId)->update($fcmIdArray);
      $paymentStatus=Employee::where('_id',$userId)->get(['status','payment_status','trail_status','company_id']);
      $data='';
      if (count($paymentStatus)>0) {
        $data=$paymentStatus[0];
        $company=Companies::where('_id',$data['company_id'])->get();
        $data1='';
        if (count($company)>0) {
          $data1= $company[0]['status'];
        }
      }
      if($result)
        {
          return array('status'=> 'success','status_code'=>666,'response'=>'Fcm Id updated successful','data'=>$data,'company_data'=>$data1);
        }
        else
        {
          return array('status'=> 'failure','status_code'=>888,'response'=>'Fcm Id update failed'); 
        }
  }
  //send notification
  public static function sendNotificationByFcmId($response)
  {
    $user=Employee::getUser();
    for ($i=0; $i < sizeof($user); $i++) { 
      if ($user[$i]['fcm_id']!='') {
        if(strlen($user[$i]['fcm_id']) == 152){
          //send notification to android
          $id=$user[$i]['fcm_id'];
          $result=Notification::sendFCMNotification($id,$response);
        }
        else if(strlen($user[$i]['fcm_id']) == 64){
          //send notification to iOS
          $id=$user[$i]['fcm_id'];
          $result=Notification::sendAppleNotification($id,$response);
        }
        
      }
    }
  }

  //activeEmployee
  public static function activeEmployee($companyId){
    $result = Employee :: where('status','active')
                          ->where('company_id',$companyId)
                          ->get();
    return count($result);
  }

  //topEmployee
  public static function topEmployee($companyId){
    $result = Employee :: where('company_id',$companyId)
                          ->orderBy('total_overtime','desc')
                          ->where('status','active')
                          ->get();
    return $result;
  }


  //for super admin
  public static function companyWithEmployeeListing()
  {
    $employee=Employee::all();
    $details=array();
    for ($i=0; $i < sizeof($employee) ; $i++) { 
      $id=$employee[$i]['company_id'];
      $company=Companies::where('_id',$id)->get();
      $list=array('employee'=>$employee[$i],
                  'company'=>$company[0]);
      array_push($details, $list);
    }
    return  $details;
  }
}


      