<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Auth;
class Country extends Eloquent
{
	  protected $collection='country';

	  public static function Separator()
	  {
		$user=Auth::user();
		$user=$user['country'];
		$res=Country::where('code',$user)->get();
		if (count($res)>0) 
		{
			$res=$res[0];
			return  array('status' => 'success','data'=>$res['separator']);
			/*return $res['separator'];*/
		}
		else
		{
			return  array('status' => 'failure');
		}
	  }
}
