<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class BlogListing extends Eloquent 
{
	protected $collection='mot_blog';
	//list blogs
	
    public static function uploadBlog($details){
    	$res=BlogListing::insert($details);
    		return array('status'=> 'success','response'=>"great");
    }
    public static function deleteBlog($id){
        $res=BlogListing::where('_id',$id)->delete();
            return array('status'=> 'success','response'=>"great");
    }
    public static function editBlog($id){
        $res=BlogListing::where('_id',$id)->get();
            return $res;
    }
}
