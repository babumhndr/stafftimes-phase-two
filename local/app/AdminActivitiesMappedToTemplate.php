<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
class AdminActivitiesMappedToTemplate extends Eloquent
{
  protected $collection='mot_admin_activities_mapped_to_mot_admin_template';

    //activity
 /* public function activity(){
    return $this->hasOne('App\MotActivity','_id','template_activity_id');
  }*/
  //activityDetails
  public function activityDetails(){
    return $this->hasOne('App\MotAdminActivity','_id','activity_id');
  }

  //addActivityMappedToTempalte
  public static function addActivityMappedToTempalte($activityDetails){
    $start=$activityDetails['activity_start'];
    $end=$activityDetails['activity_end'];
/*    if ($activityDetails['activity_flat_hours']!='0') {
      $start="00:00";
      $end="00:00";
    }*/
  	$activity = new AdminActivitiesMappedToTemplate;
  	$activity->activity_id = $activityDetails['activity_id'];
  	$activity->activity_flat_hours = $activityDetails['activity_flat_hours'];
  	$activity->activity_amount = $activityDetails['activity_amount'];
  	$activity->activity_start = $start;
  	$activity->activity_end = $end;
  	$activity->activity_break = $activityDetails['activity_break'];
  	$activity->template_id = $activityDetails['template_id'];
  	$activity->save();

  }

  //activity map to template for register
  public static function addActivityForTemplatesForRegisterUser($duplicate,$res,$tempalteId)
  {
    $activityToUpdate=AdminActivitiesMappedToTemplate::where('template_id',$duplicate)->get();
    for ($i=0; $i < sizeof($activityToUpdate) ; $i++) { 
    $start=$activityToUpdate[$i]['activity_start'];
    $end=$activityToUpdate[$i]['activity_end'];
    $activityIdForResigter='';
    $activityIdForResigter=MotActivity::where('duplicate',$activityToUpdate[$i]['activity_id'])->where('company_id',$res)->get();
    if (count($activityIdForResigter)>0) {
      $activityIdForResigter=$activityIdForResigter[0]['_id'];
      $activity = new ActivitiesMappedToTemplate;
      $activity->activity_id = $activityIdForResigter;
      $activity->company_id = $res;
      $activity->activity_flat_hours = $activityToUpdate[$i]['activity_flat_hours'];
      $activity->activity_amount = $activityToUpdate[$i]['activity_amount'];
      $activity->activity_start = $start;
      $activity->activity_end = $end;
      $activity->activity_break = $activityToUpdate[$i]['activity_break'];
      $activity->template_id = $tempalteId;
      $activity->save();
    }
    else
    {
    $activityToUpdate=MotTemplates::where('_id',$tempalteId)->delete();
    }
    }
  }

    //activity map to template for register
  public static function addActivityForTemplatesForExistUser($duplicate,$res,$tempalteId)
  {
    $activityToUpdate=AdminActivitiesMappedToTemplate::where('template_id',$duplicate)->get();
    for ($i=0; $i < sizeof($activityToUpdate) ; $i++) { 
    $start=$activityToUpdate[$i]['activity_start'];
    $end=$activityToUpdate[$i]['activity_end'];
    $activityIdForResigter='';
    $activityIdForResigter=MotActivity::where('duplicate',$activityToUpdate[$i]['activity_id'])->where('company_id',$res)->get();
    if (count($activityIdForResigter)>0) {
      /*for ($i=0; $i < sizeof($activityIdForResigter); $i++) 
      {*/ 
        $activityIdForResigter=$activityIdForResigter[0]['_id'];
        $activity = new ActivitiesMappedToTemplate;
        $activity->activity_id = $activityIdForResigter;
        $activity->company_id = $res;
        $activity->activity_flat_hours = $activityToUpdate[$i]['activity_flat_hours'];
        $activity->activity_amount = $activityToUpdate[$i]['activity_amount'];
        $activity->activity_start = $start;
        $activity->activity_end = $end;
        $activity->activity_break = $activityToUpdate[$i]['activity_break'];
        $activity->template_id = $tempalteId;
        $activity->save();
     /* }*/
  }
  else
  {
    $activityToUpdate=MotTemplates::where('_id',$tempalteId)->delete();
  }
    }
  }

}