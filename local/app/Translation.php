<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class Translation extends Eloquent
{
    protected $collection='mot_translation';

    public static function addTranslation($details)
    {
    	$result=Translation::insert($details);
    	if($result)
    	{
    		return 'yes';
    	}
    	return 'no';
    }
}
