<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Companies extends Eloquent implements Authenticatable
{
  use AuthenticableTrait; 
  protected $collection='mot_companies';
  //admin login
  public function isAdmin()
  {
    return $this->admin;
  }
  //for registration of company
  public static function companyRegister($details)
  {
    $result = Companies::where('email',$details['email'])->get();
    $count = count($result);
    //check if email already exists
    if ($count>0)
    {
      return array('status'=> 'failure','response'=>'Email already exists !');
    }
    else
    {
    //set a unique company id 
      $lastInsertedCompanyDetails = Companies :: latest()->first();
      if($lastInsertedCompanyDetails['company_unique_id'] != null)
      {
        $lastCompanyId = $lastInsertedCompanyDetails['company_unique_id'];
        $lastId = substr($lastCompanyId, -6);
        $lastId = (int)$lastId;
        $newId = str_pad($lastId + 1, 6, 0, STR_PAD_LEFT);
        $details['company_unique_id'] = 'MOT'.$newId;
      }
      else
      {
        $details['company_unique_id'] = 'MOT000001';
      }
      //insert data into company collection
   /*   $res=Companies::insert($details);*/
      //insert to mottemplates table
          $countryName=Country::where('code',$details['country'])->get();
          $countryNameFinal=$countryName[0]['name'];
          $languageName=LanguageCode::where('code',$details['language'])->get();
          $languageNameFinal=$languageName[0]['name'];
          $res = new Companies;
          $res->company_name = $details['company_name'];
          $res->email = $details['email'];
          $res->password = $details['password'];
          $res->emailverified = $details['emailverified'];
          $res->profile_image = $details['profile_image'];
          $res->created_on = $details['created_on'];
          $res->updated_on = $details['updated_on'];
          $res->password = $details['password'];
          $res->country = $details['country'];
          $res->country_name = $countryNameFinal;
          $res->language = $details['language'];
          $res->language_name = $languageNameFinal;
          $res->token = $details['token'];
          $res->first_login = $details['first_login'];
          $res->admin = $details['admin'];
          $res->status = $details['status'];
          $res->company_unique_id = $details['company_unique_id'];
          $res->trail_status = $details['trail_status'];
          $res->payment_status = $details['payment_status'];
          $res->save();
          $res = $res->id;
          /*return $res;*/
          $language=$details['language'];
          $addActivity=MotAdminActivity::addActivityForRegisterUser($res,$language);
          $addTemplete=MotAdminTemplates::addTemplatesForRegisterUser($res,$language);
          $settings=GeneralSetting::firstLoginSettingValues($res);
      /*    return $addTemplete;*/
      if($res)
      {
        session_start();
        /*session is started if you don't write this line can't use $_Session  global variable*/
        $_SESSION["newsession"]=$details['token'];
        $_SESSION['start'] = time();
        $_SESSION['expire'] = $_SESSION['start'] + (3600);
        $sendMailTo=MailSend::sendMail($details);
        return array('status'=> 'success','response'=>'Thank you for registering.Check your email!');
      }
    } 
  }
  //first visit
  public static function firstVisit($user_email)
  {
    $detail=array('first_login'=>true);
    $result=Companies::where('email',$user_email)->update($detail);
    if($result)
    {
      return 'success';
    }
    else
    {
      return 'failure';
    }
  }
  //payment status Update
  public static function updatepaymentCompany($details)
  {
    $detail=array('payment_status'=> 'active');
    $result=Companies::where('_id',$details['company_id'])->update($detail);
    if($result)
    {
      return 'success';
    }
    else
    {
      return 'failure';
    }
  }
  //email verification
  public static function emailVerification($details)
  {
    $result= Companies::where('email',$details['email'])->get();
    $count = count($result);
    if ($count>0)
    {
      $updateStatus=array('emailverified' => true,
                          'trail_status'=>"active",
                          'trail_start_date'=>$details['start_date'],
                          'trail_end_date'=>$details['end_date']);
      $res=Companies::where('email',$details['email'])->update($updateStatus);
    }
    else
    {
      return array('status'=> 'failure'); 
    }
  }
  public static function companyUpdate($details)
  {
  //update company information
    $res=Companies::where('email',$details['email'])->update($details);
    $companyName=array('company_name'=>$details['company_name']);
    if($res)
    {
      $updateInEmployee=Companies::where('email',$details['email'])->get();
      $updateInEmployee=Employee::where('company_id',$updateInEmployee[0]['id'])->update($companyName);
      return array('status'=> 'success','response'=>'Profile updated');
    }
    else
    {
      return array('status'=> 'failure','response'=>'Error occured');
    }
  }
  //password change
  public static function passwordChange($useremail,$new_password)
  {
    $res=Companies::where('email',$useremail)->update($new_password);
    if($res)
    {
      return array('status'=> 'success','response'=>'Password changed');
    }
    else
    {
      return array('status'=> 'failure','response'=>'Error occured');
    }
  }
  //get company details by id
  public static function getCompanyDetail($id)
  {
    $details=Companies::where('_id','=',$id)->get();
    return $details;
  }
  public static function upload($details)
  {  
   /* return $details;*/
    $result=Companies::where("email",$details['email'])->update($details);
    if($result)
    {
      return 'success';
    }
    else
    {
      return 'failure';
    }
  }
  public static function forgotPassword($email) 
  {
    $res=Companies::where('email',$email)->get();
    /*return $res;*/
    $count=count($res);
    if($count > 0)
    {
      $resetPasswordMail=MailSend::resetMail($res);
     /* return $resetPasswordMail;*/
      if ($resetPasswordMail== '')
      {
        return array('status'=> 'success','response'=>trans('popup.check_email')); 
      }
      else
      {
        return array('status' => 'failure' , 'response'=>trans('popup.try_again') );
      }
    }
    else
    {
      return array('status' => 'failure' , 'response'=>'the email id is not registered' );
    }
  } 
  public static function checkPassword($emailid)
  {
    $res=Companies::where('email',$emailid)->get();
    return $res;
  }
   public static function updatePassword($email,$password)
  {
 /*   return $email;*/
 $data=array('password'=>$password);
    $res=Companies::where('email',$email)->update($data);
    /*return $res;*/
    if ($res) 
    {
      return array('status'=> 'success','response'=>trans('popup.password_changed'));
    }
    else
    {
      return array('status' => 'failure' , 'response'=>trans('popup.try_again') );
    }
  }
}