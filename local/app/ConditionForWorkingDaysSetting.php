<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class ConditionForWorkingDaysSetting extends Eloquent
{ 
  protected $collection='condition_mapped_to_working_days_settings';
  //setWorkingData
  function replaceDotToColon($val)
    {
    if (strpos($val, '.') !== FALSE)
    {
      return str_replace('.',':',$val);
    }
    elseif(strpos($val, ',') !== FALSE)
    {
      return str_replace(',',':',$val);
    }
    else
    {
      return $val;
    }
    }
  public static function setWorkingData($set_working_data_id,$condtion,$i)
  {
    for($j=0;$j<2;$j++)
   {
    $condtionFrom=replaceDotToColon($condtion[$i][$j]['from']);
/*    if (strpos('.', $condtionFrom) == false) {
      $condtionFrom=str_replace('.',':',$condtionFrom);
    }*/
     $result = new ConditionForWorkingDaysSetting;
     $result->working_days_settings_id=$set_working_data_id;
     $result->condtion= $condtion[$i][$j]['condtion'];
     $result->companyid= $condtion[$i][$j]['companyid'];
     $result->from = $condtionFrom;
     $result->factor=$condtion[$i][$j]['factor'];
     $result->save();
   }
    if($result)
     {
       $response = array('status' => 'success','status_code' => '119');
     }
     else{
        $response = array('status' => 'failure','status_code' => '129');
      }
      return $response;
  }
  //updateupdateWorkingData
  public static function updateWorkingData($condtion,$companyid,$i)
  {
    for($j=0;$j<2;$j++)
   {
     $result=ConditionForWorkingDaysSetting::where('companyid',$companyid)->where('_id',$condtion[$i][$j]['id'])->update($condtion[$i][$j]);
   }
  }
  //list of workingDaySettingCondtion
  public static function workingDaySettingCondtion($data)
  {
    $details=ConditionForWorkingDaysSetting::where('companyid',$data['user_id'])->orderBy('_id', 'asc')->get();
    if(count($details) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $details);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;
  }
}


      //test