<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use Intervention\Image\ImageManager;
use Image;
use DateTime;
use DateInterval;
use DatePeriod;
use Illuminate\Contracts\Encryption\DecryptException;
class FileUpload extends Eloquent
{
    public static function upload($type)
    {
		$url=url('');
		$file = Input::file('image');
		$file1 = Image::make($file)->resize(100, 100);
		$file1->save('uploads/'.$type.'/'.time().'-'.$file->getClientOriginalName());
		$path = $url.'/uploads/'.$type.'/'.time().'-'.$file->getClientOriginalName();
		return $path;
	}
	public static function uploadEmployeeImage($type)
    {
		$url=url('');
		$file = Input::file('profile_image');
		$file->move('uploads/'.$type, time().'-'.$file->getClientOriginalName());
		$path = $url.'/uploads/'.$type.'/'.time().'-'.$file->getClientOriginalName();
		return $path;
	}
	public static function uploadBlogImage($type)
    {
		$url=url('');
		$file = Input::file('image');
		$file1 = Image::make($file)->resize(1200, 650);
		$file1->save('uploads/'.$type.'/'.time().'-'.$file->getClientOriginalName());
		$path = $url.'/uploads/'.$type.'/'.time().'-'.$file->getClientOriginalName();
		return $path;
	}
	public static function uploadFaqImage($type)
    {
		$url=url('');
		$file = Input::file('image');
		$file->move('uploads/'.$type, time().'-'.$file->getClientOriginalName());
		$path = $url.'/uploads/'.$type.'/'.time().'-'.$file->getClientOriginalName();
		return $path;
	}
	 public static function replaceDotToColon($val)
    {
	    if (strpos($val, '.') !== FALSE)
	    {
	      return str_replace('.',':',$val);
	    }
	    elseif(strpos($val, ',') !== FALSE)
	    {
	      return str_replace(',',':',$val);
	    }
	    else
	    {
	      return $val;
	    }
    }
     public static function replaceDotToComma($val)
    {
	    if (strpos($val, '.') !== FALSE)
	    {
	      return $val;
	    }
	    elseif(strpos($val, ',') !== FALSE)
	    {
	      return str_replace(',','.',$val);
	    }
	    else
	    {
	      return $val;
	    }
    }
    public static function hoursToAmPm($time)
    {
		$res=FileUpload::verify_time_format($time);
		if ($res==1) 
		{
			return $time;
		}
		else
		{
			return date('h:i a ', strtotime($time));
		}
    }
    public static function verify_time_format($value) 
    {
		$pattern2 = '/^(0?\d|1[0-2]):[0-5]\d\s(am|pm)$/i';
		return preg_match($pattern2, $value);
    }
    public static function HoursToMin($hours)
	{
		$t = EXPLODE(":", $hours); 
		return (int)($t[0]*60)+(int)($t[1]);
	}
	/* public static function MinToHours($time)
	{
    	$val= intval($time/60).'.'.abs($time % 60);
    	return $val;
	}*/
	public static function MinToHours($Minutes)
	{
    	/*return  date('G.i', mktime(0,$time));*/
    $s='';
	    if ($Minutes < 0)
	    {
	        $Min = Abs($Minutes);
            $s='-';
	    }
	    else
	    {
	        $Min = $Minutes;
	    }
	    $iHours = Floor($Min / 60);
	    $Minutes = ($Min - ($iHours * 60)) / 100;
	    $tHours = $iHours + $Minutes;
	    if ($Minutes < 0)
	    {
	        $tHours = $tHours * (-1);
	    }
	    $aHours = explode(".", $tHours);
	    $iHours = $aHours[0];
	    if (empty($aHours[1]))
	    {
	        $aHours[1] = "00";
	    }
	    $Minutes = $aHours[1];
	    if (strlen($Minutes) < 2)
	    {
	        $Minutes = $Minutes ."0";
	    }
	    $tHours = $s.$iHours .".". $Minutes;
	    return $tHours;
	}
	public static function datesBetweenRanges($sStartDate, $sEndDate){  
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = gmdate("Y-m-d", strtotime($sStartDate));  
	$sEndDate = gmdate("Y-m-d", strtotime($sEndDate));  

	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  

	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  

	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate && $sCurrentDate != $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  

	// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}  

	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;  
	}
	public static function encrypt($text)
	{
	return base64_encode($text);
	}

	public static function decrypt($text)
	{
	/*return base64_decode($text);*/
		try {
		$decrypted = decrypt($encryptedValue);
		} 
		catch (DecryptException $e) 
		{
		return "dsddf";
		}
		return $decrypted;
	}
	public static function En($arr_value)
	{
		return array_map("encrypt", $arr_value);
	}
	public static function De($arr_value)
	{
		try {
		$decrypted = array_map("decrypt", $arr_value);
		} catch (DecryptException $e) {
		return 'DecryptException';
		}
		return $decrypted;
	}
	public static function hoursToMinutes($hours)
	{
	if (strstr($hours, ':'))
	{
	# Split hours and minutes.
	$separatedData = explode(':', $hours);

	$minutesInHours    = $separatedData[0] * 60;
	$minutesInDecimals = $separatedData[1];

	$totalMinutes = $minutesInHours + $minutesInDecimals;
	}
	else
	{
	$totalMinutes = $hours * 60;
	}

	return $totalMinutes;
	}

	public static function getDomain()
	{
		$domain=$_SERVER['HTTP_HOST'];
		$domainArray=explode(".",$domain);
		if(!empty($domainArray[2])){
		  return $domainArray[2];
		}else{
		  return 'com';
		}
	}


	public static function getAllDates($fromdate,$todate) {
		$fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
    	$todate = \DateTime::createFromFormat('Y-m-d', $todate);
    	return new \DatePeriod($fromdate,
    							new \DateInterval('P1D'),
    							$todate->modify('+1 day'));
	}

	public static function dateRangeArray($datesPerWeek ){
		
      return $resArray;
	}
}
