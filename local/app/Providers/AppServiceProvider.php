<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\BackUp;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        // Please note the different namespace
        // and please add a \ in front of your classes in the global namespace
        \Event::listen('cron.collectJobs', function() {

            \Cron::add('example1', '* * * * *', function() {
                BackUp::cronTest();
                // Do some crazy things unsuccessfully every minute
                return 'yes';
            });

            \Cron::add('example2', '*/2 * * * *', function() {
                // Do some crazy things successfully every two minute
                return null;
            });

            \Cron::add('disabled job', '0 * * * *', function() {
                // Do some crazy things successfully every hour
            }, false);

            \Cron::setDisablePreventOverlapping();
            \Cron::setDatabaseLogging(false);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
