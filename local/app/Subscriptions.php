<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
class Subscriptions extends Eloquent
{
  
  protected $collection = 'subscriptions';
  //invite employee from dashboard
  public static function paymentInfo($details)
  {
     
      $employeeIds = explode(',', $details['employeeIds']);
      //return count($specializationAreas);
      for($i=0;$i<count($employeeIds);$i++){
          $finalemployeeId[$i] = $employeeIds[$i];  
          
          $res = new Subscriptions;
          $res->company_name = $details['company_name'];
          $res->company_email = $details['company_email'];
          $res->company_id = $details['company_id'];
          $res->payment_amount = $details['payment_amount'];
          $res->created_on = $details['created_on'];
          $res->updated_on = $details['updated_on'];
          $res->employee_id = $finalemployeeId[$i];
          $res->braintree_id = $details['braintree_id'];
          $res->subscription_plan = $details['subscription_plan'];
          $res->subscription_start_date = $details['created_on'];
          $res->subscription_end_date = $details['end_date'];
          $res->payment_status = $details['payment_status'];
          $res->save();            
      }
      
      
        return array('status'=> 'success','status_code'=> '200','response'=>'Payment Successful!');
    

  }

}
