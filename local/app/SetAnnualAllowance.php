<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use App\ConditionForWorkingDaysSetting;//model

class SetAnnualAllowance extends Eloquent
{ 
  protected $collection='mot_set_annual_allowance';

  //insert setOvertimeHandling
  public static function setSetAnnualAllowance($set_annual_allowance_setting_details)
  {
    $result=SetAnnualAllowance::insert($set_annual_allowance_setting_details);
   if($result)
      {
      $response = array('status' => 'success','status_code' => '119','response' =>'Saved');
      $responseToSend=array('set_annual_allowance_setting_change'=>true,
                            'message'=>'set_annual_allowance_setting_changed');
       Employee::sendNotificationByFcmId($responseToSend);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => 'Error');
      }
      return $response;
  }
 
  //update setOvertimeHandling
  public static function updateSetAnnualAllowance($set_annual_allowance_setting_details)
  {
    $result=SetAnnualAllowance::where('companyid',$set_annual_allowance_setting_details['companyid'])->update($set_annual_allowance_setting_details);
   if($result)
      {
      $response = array('status' => 'success','status_code' => '119','response' =>'Saved');
       $responseToSend=array('set_annual_allowance_setting_change'=>true,
                            'message'=>'set_annual_allowance_setting_changed');
       Employee::sendNotificationByFcmId($responseToSend);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => 'Error');
      }
      return $response;
  }

  //setting listing
  public static function setAnnualAllowanceList($data)
  {
    $details=SetAnnualAllowance::where('companyid',$data['user_id'])->get();
    if(count($details) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $details);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;
  }

  //list for mobile
  public static function setAnnualAllowanceSettingValues($companyid)
  {
    $result=SetAnnualAllowance::where('companyid',$companyid)->get();
    if(count($result) > 0){
          $response = array('status' => 'success','status_code' => '876','response' => $result);

      }
      else{
        $response = array('status' => 'failure','status_code' => '834','response' =>[]);
      }
      return $response;
  }
}


      