<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class Timesheet extends Eloquent
{ 
  protected $collection='mot_timesheet';

  //activity
  public function timesheetActivity(){
    return $this->hasOne('App\MotActivity','_id','activity');
  } 
   public function employeeDetails(){
    return $this->hasOne('App\Employee','_id','employee_id');
  } 

  public static function insertTimesheet($details,$monthFirstDate,$monthLastDate)
  {
   /* $result=Timesheet::insert($details);*/
    //insert to timesheet table
   if($details['input_type']=='insert')
   {
        $timesheet = new Timesheet;
          $timesheet->employee_id = $details['employee_id'];
          $timesheet->company_id = $details['company_id'];
          $timesheet->date = $details['date'];
          $timesheet->day_name = $details['day_name'];
          $timesheet->offset = $details['offset'];
          $timesheet->activity = $details['activity'];
          $timesheet->activity_detail = $details['activity_detail'];
          $timesheet->activity_name = $details['activity_name'];
          $timesheet->start_time = $details['start_time'];
          $timesheet->end_time = $details['end_time'];
          $timesheet->break_time = $details['break_time'];
          $timesheet->total_hours = $details['total_hours'];
          $timesheet->total_hours1 = $details['total_hours1'];
          $timesheet->day_balance = $details['day_balance'];
          $timesheet->condition1 = $details['condition1'];
          $timesheet->condition1_from = $details['condition1_from'];
          $timesheet->condition1_factor = $details['condition1_factor'];
          $timesheet->condition2 = $details['condition2'];
          $timesheet->condition2_from = $details['condition2_from'];
          $timesheet->condition2_factor = $details['condition2_factor'];
          $timesheet->bonus_hours = $details['bonus_hours'];
          $timesheet->apply_bonus = $details['apply_bonus'];
          $timesheet->rate = $details['rate'];
          $timesheet->own_account = $details['own_account'];
          $timesheet->amount = $details['amount'];
          $timesheet->break_min = $details['break_min'];
          $timesheet->total_hours_min = $details['total_hours_min'];
          $timesheet->total_hours_min1 = $details['total_hours_min1'];
          $timesheet->balance_min = $details['balance_min'];
          $timesheet->offset_min = $details['offset_min'];
          $timesheet->notes = $details['notes'];
          $timesheet->schedule_id = $details['schedule_id'];
          $timesheet->created_on = $details['created_on'];
          $timesheet->updated_time = $details['updated_time'];
          $timesheet->TSHrsAccounted = $details['TSHrsAccounted'];
          $timesheet->TSBonusHrs = $details['TSBonusHrs'];
          $timesheet->TSHrsTotal = $details['TSHrsTotal'];
          $timesheet->TSDailyBalnce = $details['TSDailyBalnce'];
          $timesheet->TimeSheetSchedule = $details['TimeSheetSchedule'];
          $timesheet->save();
          $timesheet = $timesheet->id;

          $updateTS=array('TSHrsAccounted' => $details['TSHrsAccounted'],
          'TSBonusHrs' => $details['TSBonusHrs'],
          'TSHrsTotal' => $details['TSHrsTotal'],
          'TSDailyBalnce' =>$details['TSDailyBalnce'],
          'TimeSheetSchedule'=>$details['TimeSheetSchedule']);
          $updateTsFinal=Timesheet::where('date',$details['date'])->where('employee_id',$details['employee_id'])->update($updateTS);
          /*return $timesheet;*/
          $data=Timesheet::where('_id',$timesheet)->get();
          /*return $data;*/
          /*$data1=gettype($data['0']['date'])*/;
          $totalOvertimeOfEmployeeValue=0;
          $timesheet1=Timesheet::groupBy('date','TSDailyBalnce')->where('employee_id', $details['employee_id'])->whereBetween('date', [$monthFirstDate, $monthLastDate])->get();
            if (count($timesheet1)>0) {
            for ($i=0; $i <sizeof($timesheet1) ; $i++) { 
              $totalOvertimeOfEmployeeValue=$totalOvertimeOfEmployeeValue+$timesheet1[$i]['TSDailyBalnce'];
            }
            }
            //insert monthly overtime
            $totalOvertimeOfEmployee=array('total_overtime'=>$totalOvertimeOfEmployeeValue);
            $addOvertimeToEmployee=Employee::where('_id',$details['employee_id'])->update($totalOvertimeOfEmployee);
    if($timesheet){
        $response = array('status' => 'success','status_code' => '109','response'=>'Added successfully','timesheet_details'=>$data);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '139', 'response'=>'Error Occurred');
      }
      return $response;
    }
    else if($details['input_type']=='update')
    {
             /*return $timesheet;*/
      $update_details= array('employee_id' =>$details['employee_id'], 
                                'company_id' =>$details['company_id'], 
                                'date' =>$details['date'], 
                                'day_name' =>$details['day_name'], 
                                'offset' =>$details['offset'], 
                                'activity' =>$details['activity'],
                                'activity_detail'=>$details['activity_detail'],
                                'activity_name' =>$details['activity_name'],
                                'start_time' =>$details['start_time'],
                                'end_time' =>$details['end_time'],
                                'break_time' =>$details['break_time'],
                                'total_hours' =>$details['total_hours'],
                                'total_hours1' =>$details['total_hours1'],
                                'day_balance' =>$details['day_balance'],
                                'condition1'=>$details['condition1'],
                                'condition1_from'=>$details['condition1_from'],
                                'condition1_factor'=>$details['condition1_factor'],
                                'condition2'=>$details['condition2'],
                                'condition2_from'=>$details['condition2_from'],
                                'condition2_factor'=>$details['condition2_factor'],
                                'bonus_hours'=>$details['bonus_hours'],
                                'apply_bonus' => $details['apply_bonus'],
                                'rate' => $details['rate'],
                                'own_account' => $details['own_account'],
                                'amount' => $details['amount'],
                                'break_min' => $details['break_min'],
                                'total_hours_min' => $details['total_hours_min'],
                                'total_hours_min1' => $details['total_hours_min1'],
                                'balance_min' => $details['balance_min'],
                                'offset_min' => $details['offset_min'],
                                'notes' => $details['notes'],
                                'schedule_id' =>$details['schedule_id'],
                                'created_on' =>$details['created_on'],
                                'updated_time' =>$details['updated_time'],
                                 );
    $timesheet=Timesheet::where('_id',$details['timesheet_id'])->update($update_details);
    $updateTS=array('TSHrsAccounted' => $details['TSHrsAccounted'],
                    'TSBonusHrs' => $details['TSBonusHrs'],
                    'TSHrsTotal' => $details['TSHrsTotal'],
                    'TSDailyBalnce' =>$details['TSDailyBalnce'],
                    'TimeSheetSchedule'=>$details['TimeSheetSchedule']);
    $updateTsFinal=Timesheet::where('date',$details['date'])->where('employee_id',$details['employee_id'])->update($updateTS);
    $data=Timesheet::where('_id',$details['timesheet_id'])->get();
    $totalOvertimeOfEmployeeValue=0;
          $timesheet1=Timesheet::groupBy('date','TSDailyBalnce')->where('employee_id', $details['employee_id'])->whereBetween('date', [$monthFirstDate, $monthLastDate])->get();
            if (count($timesheet1)>0) {
            for ($i=0; $i <sizeof($timesheet1) ; $i++) { 
              $totalOvertimeOfEmployeeValue=$totalOvertimeOfEmployeeValue+$timesheet1[$i]['TSDailyBalnce'];
            }
            }
            //insert monthly overtime
            $totalOvertimeOfEmployee=array('total_overtime'=>$totalOvertimeOfEmployeeValue);
            $addOvertimeToEmployee=Employee::where('_id',$details['employee_id'])->update($totalOvertimeOfEmployee);
          /*return $data;*/
    if($timesheet){
        $response = array('status' => 'success','status_code' => '609','response'=>'Updated successfully','timesheet_details'=>$data);
      }
      else{
        $timesheet = new Timesheet;
          $timesheet->employee_id = $details['employee_id'];
          $timesheet->company_id = $details['company_id'];
          $timesheet->date = $details['date'];
          $timesheet->day_name = $details['day_name'];
          $timesheet->offset = $details['offset'];
          $timesheet->activity = $details['activity'];
          $timesheet->activity_detail = $details['activity_detail'];
          $timesheet->activity_name = $details['activity_name'];
          $timesheet->start_time = $details['start_time'];
          $timesheet->end_time = $details['end_time'];
          $timesheet->break_time = $details['break_time'];
          $timesheet->total_hours = $details['total_hours'];
          $timesheet->total_hours1 = $details['total_hours1'];
          $timesheet->day_balance = $details['day_balance'];
          $timesheet->condition1 = $details['condition1'];
          $timesheet->condition1_from = $details['condition1_from'];
          $timesheet->condition1_factor = $details['condition1_factor'];
          $timesheet->condition2 = $details['condition2'];
          $timesheet->condition2_from = $details['condition2_from'];
          $timesheet->condition2_factor = $details['condition2_factor'];
          $timesheet->bonus_hours = $details['bonus_hours'];
          $timesheet->apply_bonus = $details['apply_bonus'];
          $timesheet->rate = $details['rate'];
          $timesheet->own_account = $details['own_account'];
          $timesheet->amount = $details['amount'];
          $timesheet->break_min = $details['break_min'];
          $timesheet->total_hours_min = $details['total_hours_min'];
          $timesheet->total_hours_min1 = $details['total_hours_min1'];
          $timesheet->balance_min = $details['balance_min'];
          $timesheet->offset_min = $details['offset_min'];
          $timesheet->notes = $details['notes'];
          $timesheet->schedule_id = $details['schedule_id'];
          $timesheet->created_on = $details['created_on'];
          $timesheet->updated_time = $details['updated_time'];
          $timesheet->TSHrsAccounted = $details['TSHrsAccounted'];
          $timesheet->TSBonusHrs = $details['TSBonusHrs'];
          $timesheet->TSHrsTotal = $details['TSHrsTotal'];
          $timesheet->TSDailyBalnce = $details['TSDailyBalnce'];
          $timesheet->TimeSheetSchedule = $details['TimeSheetSchedule']; 
          $timesheet->save();
          $timesheetid = $timesheet->id;

          $updateTS=array('TSHrsAccounted' => $details['TSHrsAccounted'],
          'TSBonusHrs' => $details['TSBonusHrs'],
          'TSHrsTotal' => $details['TSHrsTotal'],
          'TSDailyBalnce' =>$details['TSDailyBalnce'],
          'TimeSheetSchedule'=>$details['TimeSheetSchedule']);
          $updateTsFinal=Timesheet::where('date',$details['date'])->where('employee_id',$details['employee_id'])->update($updateTS);
          /*return $timesheet;*/
          $data=Timesheet::where('_id',$timesheetid)->get();
          $totalOvertimeOfEmployeeValue=0;
          $timesheet1=Timesheet::groupBy('date','TSDailyBalnce')->where('employee_id', $details['employee_id'])->whereBetween('date', [$monthFirstDate, $monthLastDate])->get();
            if (count($timesheet1)>0) {
            for ($i=0; $i <sizeof($timesheet1) ; $i++) { 
              $totalOvertimeOfEmployeeValue=$totalOvertimeOfEmployeeValue+$timesheet1[$i]['TSDailyBalnce'];
            }
            }
            //insert monthly overtime
          $totalOvertimeOfEmployee=array('total_overtime'=>$totalOvertimeOfEmployeeValue);
          $addOvertimeToEmployee=Employee::where('_id',$details['employee_id'])->update($totalOvertimeOfEmployee);
        $response = array('status' => 'success','status_code' => '609','response'=>'Updated successfully','timesheet_details'=>$data);
      }
      return $response;
    }
  }

  //update new values into timesheet
/*  public static function updateTimesheet($details,$id)
  {
    $timesheet=Timesheet::where('_id',$id)->update($details);
    $data=Timesheet::where('_id',$id)->get();
     
    if($timesheet){
        $response = array('status' => 'success','status_code' => '534','response'=>'Updated successfully','timesheet_details'=>$data);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '453', 'response'=>'Error Occurred');
      }
      return $response;
  }*/
  //delete time sheet api
  public static function deleteTimesheet($id)
  {
    $result=Timesheet::where('_id',$id)->delete();
    if($result)
    {
        $response = array('status' => 'success','status_code' => '199','response'=>'Deleted successfully');
        $responseToSend=array('timesheet_deleted'=>$id,
                            'message'=>'timesheet_deleted');
        Employee::sendNotificationByFcmId($responseToSend);
    }
    else
    {
        $response = array('status' => 'failure','status_code' => '149', 'response'=>'Error Occurred');
    }
     return $response;
  }

  //list time sheet api
  public static function listTimesheet($details)
  {
    $result=Timesheet::with('timesheetActivity')->where('company_id',$details['company_id'])->where('employee_id',$details['employee_id'])->get();
    for ($i=0; $i <sizeof($result) ; $i++) 
    { 
      if ($result[$i]['timesheetActivity']==null) 
      {
        $result[$i]['activity_status']="deleted";
      }
      else
      {
        $result[$i]['activity_status']="active";
      }
    }
    /*return $result;*/
    if(count($result)>0)
    {
        $response = array('status' => 'success','status_code' => '769','response'=>'Data found','data'=>$result);
    }
    else
    {
        $response = array('status' => 'failure','status_code' => '358', 'response'=>'Data not found','data'=>[]);
    }
     return $response;
  }

  public static function listTimesheetIos($details)
  {
   $result=Timesheet::with('timesheetActivity')->groupBy('date')->where('company_id',$details['company_id'])->where('employee_id',$details['employee_id'])->get();
    $apiArray=array();
    if(count($result) > 0){
        for ($i=0; $i <sizeof($result) ; $i++) { 
        $data=Timesheet::with('timesheetActivity')->where('date',$result[$i]['date'])->where('company_id',$details['company_id'])->where('employee_id',$details['employee_id'])->get();

        $myarray[]=array("data" => $data, "date" => $result[$i]['date']);
    }
    for ($i=0; $i <sizeof($myarray) ; $i++) 
    { 
        for ($j=0; $j <sizeof($myarray[$i]['data']); $j++) 
        { 
        if ($myarray[$i]['data'][$j]['timesheetActivity']==null) 
        {
          $myarray[$i]['data'][$j]['activity_status']="deleted";
        }
        else
        {
          $myarray[$i]['data'][$j]['activity_status']="active";
        }
      }
    }
    if(count($myarray)>0)
    {
        $response = array('status' => 'success','status_code' => '769','response'=>'Data found','data'=>$myarray);
    }
    else
    {
        $response = array('status' => 'failure','status_code' => '358', 'response'=>'Data not found','data'=>[]);
    }
    }
    else{
      $response = array('status' => 'failure','status_code' => '358', 'response'=>'Data not found','data'=>[]);
    }
    
     return $response;
  }

   //list time sheet api for statistics
  public static function listTimesheetForStatistics($details)
  {
    $result=Timesheet::with('timesheetActivity')->where('company_id',$details['company_id'])->where('employee_id',$details['employee_id'])->orderBy('date', 'asc')->get();
    /*return $result;*/
    if(count($result)>0)
    {
        $response = array('status' => 'success','status_code' => '769','response'=>'Data found','data'=>$result);
    }
    else
    {
        $response = array('status' => 'failure','status_code' => '358', 'response'=>'Data not found','data'=>[]);
    }
     return $response;
  }

  //lisiting of time sheet with ranges
  public static function listingTimesheetWithRangeForOvertimeReport($cid,$eid,$from,$to)
  {
    $range=Timesheet::groupBy('date')->orderBy('date','asc')->where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date', [$from, $to])->get();
    $array=array();
    for ($i=0; $i <sizeof($range) ; $i++) { 
      $range1=Timesheet::orderBy('date','desc')->where('date',$range[$i]['date'])->where('company_id',$cid)->where('employee_id',$eid)->get();
      array_push($array, $range1);
    }
    return $array;
  }

 //lisiting of time sheet with ranges for detail report
  public static function listingTimesheetWithRange($cid,$eid,$from,$to)
  {
   /* return gettype($from);*/
    $range=Timesheet::groupBy('date')->orderBy('date','asc')->where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date', [$from, $to])->get();
    return $range;
  }

  //detailed report ################### activity change#############################
  public static function listingTimesheetWithRangeDate($date,$cid,$eid)
  {
       $range1=Timesheet::with('timesheetActivity')->where('company_id',$cid)->where('employee_id',$eid)->where('date',$date)->get();
       return $range1;
  } 

     //lisiting of time sheet with ranges for detail report ################### activity change#############################
  public static function listingTimesheetForActivity($activity,$date)
  {
    $result=array();
    $from=$date['0'];
    $to=$date['1'];
    $list=Timesheet::with('timesheetActivity')->orderBy('date')->where('activity',$activity)->whereBetween('date', [$from, $to])->get();
    return $list;
  }

/*  //lisiting of time sheet with ranges
  public static function listingTimesheetWithRangeForStatistics($cid,$eid,$from,$to)
  {
    $range=Timesheet::groupBy('date')->where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date', [$from, $to])->get();
    $statistics='';
    $balance='';
    $offset='';
    for ($i=0; $i <sizeof($range); $i++) { 
      $statistics=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->where('date',$range[$i]['date'])->get();
      $result=array();
       $total_hours_min='';
      for ($j=0; $j < sizeof($statistics); $j++) 
      { 
        $total_hours_min=$total_hours_min+$statistics[$j]['total_hours_min'];
        $offset=$statistics[$j]['offset_min'];
      }
      $balance=$balance+$total_hours_min-$offset;
    }
    return $balance;
  }*/
    //lisiting of time sheet with ranges
  public static function listingTimesheetWithRangeForStatistics($cid,$eid,$from,$to,$selectedFromDate,$selectedToDate)
  {
   /* return $to;*/
  /* $range=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date', [$from, $to])->get(['total_hours_min','offset_min','date']);
   return $range;*/
   $firstDayOfWeek=$from;
   $dateRangeOfParticulatWeek='';
   if(strtotime($from) < strtotime($selectedFromDate))
   {
    $from = strtotime($selectedFromDate);
    $from = date('Y-m-d', $from);
    $fromRange = strtotime($from);
    $fromRange = strtotime("-1 day", $fromRange);
    $fromRange = date('Y-m-d', $fromRange);
    $dateRangeOfParticulatWeek=FileUpload::datesBetweenRanges($firstDayOfWeek,$fromRange);
   }
   if(strtotime($to) > strtotime($selectedToDate))
   {
    $to = strtotime($selectedToDate);
    $to = date('Y-m-d', $to);
   }
   /*return $dateRangeOfParticulatWeek;*/
    $range=Timesheet::groupBy('date')->where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date', [$from, $to])->get();
    $statistics='';
    $balance='';
    $offset='';
    $statistics1='';
    $offset1='';
    $balance1='';
    $returnArray=array('start_balance'=>0,
                        'range_balance'=>0);
    for ($i=0; $i <sizeof($range); $i++) { 
      $statistics=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->where('date',$range[$i]['date'])->get();
      $result=array();
       $total_hours_min='';
      for ($j=0; $j < sizeof($statistics); $j++) 
      { 
        $total_hours_min=$statistics[0]['TSHrsTotal'];
        $offset=$statistics[$j]['offset_min'];
      }
      $balance=$balance+$total_hours_min-$offset;
    }
    if ($dateRangeOfParticulatWeek!='') {
       for ($i=0; $i <sizeof($dateRangeOfParticulatWeek); $i++) { 
      $statistics1=Timesheet::where('company_id',$cid)->where('employee_id',$eid)->where('date',$dateRangeOfParticulatWeek[$i])->get();
      $result1=array();
       $total_hours_min1='';
      for ($j=0; $j < sizeof($statistics1); $j++) 
      { 
        $total_hours_min1=$statistics1[0]['TSHrsTotal'];
        $offset1=$statistics1[$j]['offset_min'];
      }
      $balance1=$balance1+$total_hours_min1-$offset1;
    }
    }
    else
    {
      $balance1=0;
    }
    $returnArray=array('start_balance'=>$balance1,
                        'range_balance'=>$balance);
    return $returnArray;
  }

  //lisiting of time sheet with ranges for activity graph
  public static function listingTimesheetWithRangeForActivityGraph($cid,$eid,$from,$to)
  {
    $range=Timesheet::groupBy('activity')->where('company_id',$cid)->where('employee_id',$eid)->whereBetween('date', [$from, $to])->get();
   /* return $range;*/
    $array=array();
    for ($i=0; $i < sizeof($range) ; $i++) { 
      $range1=Timesheet::with('timesheetActivity')->where('company_id',$cid)->where('employee_id',$eid)->where('activity',$range[$i]['activity'])->whereBetween('date', [$from, $to])->get();
      array_push($array, $range1);
    }
    return $array;
  }

   //lisiting of time sheet for allwance report
  public static function allownaceTimesheet($activityId,$userDetail)
  {
   /* return $to;*/
    $allownaceTimesheet=Timesheet::with('timesheetActivity')->groupBy('date','activity','offset','offset_min','total_hours','total_hours_min','TSHrsTotal','TSDailyBalnce','bonus_hours','apply_bonus')->where('company_id',$userDetail['company_id'])->where('employee_id',$userDetail['employee_id'])->where('activity',$activityId)->get();
/*    $array=array();
    for ($i=0; $i < sizeof($range) ; $i++) { 
      $range1=Timesheet::with('timesheetActivity')->where('activity',$range[$i]['activity'])->get();
      array_push($array, $range1);
    }*/
    return $allownaceTimesheet;
  }

  //totalOvertime
  public static function totalOvertime($companyId){
/*    $month = date("m");
    $year = date("Y");
    $startDate = $year.'-'.$month.'-01';
    $endDate = $year.'-'.$month.'-31';
    
    $result = Timesheet :: where('company_id',$companyId)
                          ->whereBetween('date',[$startDate,$endDate])
                          ->get();
    $totalOvertime = 0;
    for($i=0;$i<count($result);$i++){
      $totalOvertime = $totalOvertime + $result[$i]['balance_min'];
    }
  return floatval(number_format(FileUpload::MinToHours($totalOvertime), 2));*/
    $result=Employee::where('company_id',$companyId)->get();
    $totalOvertime = 0;
    for($i=0;$i<count($result);$i++){
      $totalOvertime = $totalOvertime + $result[$i]['total_overtime'];
    }
    return number_format(FileUpload::MinToHours($totalOvertime), 2);
  }

  //todaysAvgOvertime
  public static function todaysAvgOvertime($companyId){
    $date = date("Y-m-d");
    $result = Timesheet :: where('company_id',$companyId)
                          ->where('date',$date)
                          ->get();
    if(count($result) > 0){
    $totalHours = 0;
    $totalOffset = 0;
    $totalOvertime= 0;
    for($i=0;$i<count($result);$i++){
      $totalHours = $result[$i]['TSHrsTotal'];
      $totalOffset =$result[$i]['offset_min'];
    }
    $totalOvertime=$totalHours-$totalOffset;
      return number_format(FileUpload::MinToHours($totalOvertime), 2);
    }
    else{
      return 0;
    }
    
  }

  //topEmplyee
  public static function topEmployee($companyId){
    $month = date("m");
    $year = date("Y");
    $startDate = $year.'-'.$month.'-01';
    $endDate = $year.'-'.$month.'-31';
    // $startDate = '2016-10-01';
    // $endDate = '2016-10-31';
    
   $result = Timesheet :: where('company_id',$companyId)
                          ->whereBetween('date',[$startDate,$endDate])
                          ->with('employeeDetails')
                         // ->groupBy('employee_id')
                          ->get();
   return $result;
  }

  //allTimeSheets
  public static function allTimeSheets($companyId){
     $month = date("m");
    $year = date("Y");
    $startDate = $year.'-'.$month.'-01';
    $endDate = $year.'-'.$month.'-31';
    // $startDate = '2016-10-01';
    // $endDate = '2016-10-31';

    $result = Timesheet::groupBy('date')
              ->whereBetween('date',[$startDate,$endDate])
          ->orderBy('date','asc')
          ->where('company_id',$companyId)
          ->get();
    return $result;
    // $result = DB::table('mot_timesheet')
    //              ->select('date',DB::raw('SUM(balance_min) as total'))
    //              ->groupBy('date')
    //              ->where('company_id',$companyId)
    //              ->orderBy('date','ASC')
    //              ->get();


    // $finalArray = array();
    // if($i=0;$i<count($result);$i++){
    //   $j=0;
    //   $finalArray[$j]['date'] = $result[$i]['date'];

    //   if($result[$i]['date'] == $result[$i++]['date']){
    //     $finalArray[$j]['total'] = 
    //   }
    // }
  }

  //totalOvertime
  public static function totalOvertimeForCompany($date,$companyId)
  {
    $employee=Employee::where('company_id',$companyId)->get();
    $data = array();
    if(count($employee) > 0)
      {
        $total = 0;
        $total_hours_min = 0;
        $totalOffset = 0;
        for ($j=0; $j <count($employee) ; $j++) 
        {
          $result = Timesheet :: where('company_id',$companyId)
                                  ->where('employee_id',$employee[$j]['_id'])
                                  ->where('date',$date)
                                  ->get();
          if(count($result) > 0){
            for($i=0;$i<count($result);$i++){
              $total_hours_min = $result[$i]['TSHrsTotal'];
              $totalOffset = $result[$i]['offset_min'];
            }
          }
          $total= $total_hours_min-$totalOffset;
        }
      /*$data['total_overtime'] = floatval(number_format(($total/60), 2));
      $data['avg_offset'] = floatval(number_format(($totalOffset/count($result)/60), 2));*/
      $data['total_overtime'] = floatval(number_format(FileUpload::MinToHours($total), 2));
      $data['avg_offset'] = floatval(number_format(FileUpload::MinToHours($totalOffset), 2));
      }
    return $data;
  }

 //allTimeSheets
  public static function allTimeSheetsForEmployee($eid){
     $month = date("m");
    $year = date("Y");
    $startDate = $year.'-'.$month.'-01';
    $endDate = $year.'-'.$month.'-31';
    // $startDate = '2016-10-01';
    // $endDate = '2016-10-31';

    $result = Timesheet::groupBy('date')
              ->whereBetween('date',[$startDate,$endDate])
          ->orderBy('date','asc')
          ->where('employee_id',$eid)
          ->get();
    return $result;
  }
   //totalOvertime
  public static function totalOvertimeForEmployee($date,$eid){

    $result = Timesheet :: where('employee_id',$eid)
                            ->where('date',$date)
                            ->get();
    if(count($result) > 0){
      $total = 0;
      $total_hours_min = 0;
      $totalOffset = 0;
      $data = array();
      for($i=0;$i<count($result);$i++){
        $total_hours_min = $result[$i]['TSHrsTotal'];
         $totalOffset = $result[$i]['offset_min'];
      }
    }
    $total= $total_hours_min-$totalOffset;
      $data['total_overtime'] = floatval(number_format(FileUpload::MinToHours($total), 2));
      $data['avg_offset'] = floatval(number_format(FileUpload::MinToHours($totalOffset), 2));

    return $data;
  }

  //lisiting of time sheet with ranges
  public static function getPlannerData($id,$from,$to,$selectedFromDate,$selectedToDate)
  {
   //return array('1'=>$selectedFromDate,'2'=>$selectedToDate);
    $selectedFromDate = strtotime($selectedFromDate);
    $selectedFromDate = date('Y-m-d', $selectedFromDate);
    $selectedToDate = strtotime($selectedToDate);
    $selectedToDate = date('Y-m-d', $selectedToDate);
   // return array('1'=>$selectedFromDate,'2'=>$selectedToDate);
    $employee=array();
    $range=Timesheet::where('company_id',$id)->whereBetween('date', [$selectedFromDate, $selectedToDate])->groupBy('employee_id')->get(['employee_id']);

    for ($i=0; $i <sizeof($range) ; $i++) { 
      $timesheet_detail=Timesheet:: with('timesheetActivity')->where('employee_id',$range[$i]['employee_id'])->whereBetween('date', [$selectedFromDate, $selectedToDate])->orderBy('date','asc')->get();
      $employee_detail=Employee::where('_id',$range[$i]['employee_id'])->get();
      $details=array('employee_detail'=>$employee_detail,
                     'timesheet_detail'=>$timesheet_detail);
    array_push($employee, $details);
    }
   return $employee;
  }
}


      