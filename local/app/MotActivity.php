<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use App\seconds;
class  MotActivity extends Eloquent
{
    protected $collection='mot_activity';
    public static function addActivity($details)
     {
      $result = MotActivity::where('activity_name',$details['activity_name'])->where('company_id',$details['company_id'])->get();
      $count = count($result);
      //check if activity name already exists
      if ($count>0)
      {
          return array('status'=> 'failure','response'=>'Activity name already exists !');
      }
      else
      {
      $res=MotActivity::insert($details);
      
      if($res)
        {
            return array('status'=> 'success','response'=>'Activity Added');
        }
        else
        {
            return array('status'=> 'failure','response'=>'Error occured');
        }
      }
  }
  //updating of  default activity
    public static function updateDefaultActivity($details,$activity_id )
    {
      $result = MotActivity::where('activity_name',$details['activity_name'])->where('company_id',$details['company_id'])->get();
      $count = count($result);
     //check if activity name already exists
      if ($count>0 || $activity_id!=$result[0]['_id'])
      {
        return array('status'=> 'failure','response'=>'Activity name already exists !');
      }
      else
      {
        $res = MotActivity::where('_id',$activity_id)->update($details);
        if($res)
        {
          $responseToSend=array('activity_added_change'=>true,
                                  'message'=>'Activity Updated');
            Employee::sendNotificationByFcmId($responseToSend);
        return array('status'=> 'success','response'=>'Activity Updated');
        }
        else
        {
        return array('status'=> 'failure','response'=>'Error occured');
        } 
      }
    //
    }

    //updating of activity
    public static function updateCompanyActivity($details,$activity_id )
    {
      $result = MotActivity::where('activity_name',$details['activity_name'])->where('company_id',$details['company_id'])->get();
      $count = count($result);
           //check if activity name already exists
      if ($count>0 && $activity_id!=$result[0]['_id'])
      {
        return array('status'=> 'failure','response'=>'Activity name already exists !');
      }
      else
      {
        $res = MotActivity::where('_id',$activity_id)->update($details);
        if($res)
        {
            $responseToSend=array('activity_added_change'=>true,
                                  'message'=>'Activity Updated');
            Employee::sendNotificationByFcmId($responseToSend);
        return array('status'=> 'success','response'=>'Activity Updated');
        }
        else
        {
        return array('status'=> 'failure','response'=>'Error occured');
        } 
      }
    //
    }
    //getListOfActivityForApp
    public static function getListOfActivityForApp($details){
      $result = MotActivity :: whereIn('created_user_id',array($details['user_id'],$details['company_id']))->where( "deploy_status","!=","pending")->orderBy('activity_name', 'asc')->get();
      return $result;
    }

    //getListOfActivity
    public static function getListOfActivity($details){
      $result = MotActivity :: where('created_user_id',$details['user_id'])
                               ->where( "deploy_status","!=","pending")->orderBy('activity_name', 'asc')->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        $response = array('status' => 'failure','status_code' => '119','response' => []);
      }

      return $response;
    }

        //getListOfActivity
    public static function getListOfActivityForSetting($details){
      $result = MotActivity :: where('created_user_id',$details['user_id'])
                               ->orderBy('activity_name', 'asc')->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        $response = array('status' => 'failure','status_code' => '119','response' => []);
      }

      return $response;
    }

    //getListOfCompanyActivity
    public static function getListOfCompanyActivity($companyId){  
      //$mainArray = array();
      $mainArray = MotActivity :: where('created_user_id',$companyId)
                              ->orderBy('priority_number', 'asc')
                               ->get();
     /* $result = MotActivity :: where('created_user_id',$companyId)
                              ->where('priority_number','!=',0)
                              ->where('priority_number','!=',null)
                              ->orderBy('priority_number', 'desc')
                               ->get();

      $result1 = MotActivity :: where('created_user_id',$companyId)
                              ->where('priority_number','<',1)
                              ->orderBy('activity_name', 'asc')
                               ->get();
      if (count($result)>0) {
        for($i=0;$i<count($result);$i++){
          array_push($mainArray , $result[$i]);
        }
      }
      if (count($result1)>0) {
        for($i=0;$i<count($result1);$i++){
          array_push($mainArray , $result1[$i]);
        }
      }*/
       if(count($mainArray)>0){
        $response = array('status' => 'success','status_code' => '119','response' => $mainArray);
      }
      else{
        $response = array('status' => 'failure','status_code' => '119','response' => []);
      }
     /* $result = MotActivity :: where('created_user_id',$companyId)
                               ->orderBy('priority_number', 'desc')
                               ->get();
         if(count($result) > 0){
          $response = array('status' => 'success','status_code' => '119','response' => $result);
        }
        else{
          $response = array('status' => 'failure','status_code' => '119','response' => []);
        }*/

      return $response;
    }

    //getListOfMotActivity
    public static function getListOfMotActivity(){
       $result = MotActivity :: where('created_by','MOT')
                                ->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;
    }

    //get list of activity for edit
    public static function getListOfActivitys($companyId,$id){
      $result = MotActivity :: where('company_id',$companyId)->where('_id',$id)->orderBy('activity_name', 'asc')->get();
      return $result;
  }

  //getListOfMotActivitys
  public static function getListOfMotActivitys($id){
    $result = MotActivity :: where('_id',$id)->get();
      return $result;
  }

  //update Annual Allowance InActivity
  public static function updateAnnualAllowanceInActivity($setWorkingHours,$user)
  {

   $allowanceActivity=MotActivity::where('allowance_bank_days', '!=','0')->where('allowance_bank_days', '!=','0.00')->where('allowance_bank_hours', '!=','')->where('company_id',$user)->get();
  /*  return $allowanceActivity;*/
    function hrsToMin($time)
        {
          $time = explode(':', $time);
          return ($time[0]*60) + $time[1];
        }
        function minToHrs($time)
        {
          return gmdate("H:i", ($time * 60));
        }
    if (count($allowanceActivity)>0) 
    { 

        for ($i=0; $i < count($allowanceActivity); $i++) 
          { 
            $allowanceBankHours=$allowanceActivity[$i]['allowance_bank_hours'];
            $allowanceBankDays=hrsToMin($allowanceBankHours)/hrsToMin($setWorkingHours);
            $allowanceBankDays=number_format((float)$allowanceBankDays, 2, '.', '');
            $allowance= array('allowance_bank_days' => $allowanceBankDays);
            $updateAllowance=MotActivity::where('_id',$allowanceActivity[$i]['_id'])->update($allowance);
          }
    }
  }

  //update annual allowance by settings 
  public static function updateAnnualAllowanceByActivityId($activityId,$annualAllowanceForActivity)
  {
      $updateAnnualAllowance=MotActivity::where('_id',$activityId)->update($annualAllowanceForActivity);
      if($updateAnnualAllowance){
        $response = array('status' => 'success');
      }
      else{
        $response = array('status' => 'failure');
      }
      return $response;
  }

   //change deploy status
  public static function changeStatusDeployActivity($id,$status)
  {
    $result=MotActivity::where('_id',$id)->update($status);
    if ($result) 
      {
        $response = array('status' => 'success','status_code' => '998');
         $responseToSend=array('activity_added_change'=>true,
                                  'message'=>'Activity Added');
         Employee::sendNotificationByFcmId($responseToSend);
      }
    else{ 
        $response = array('status' => 'failure','status_code' => '888');
      }
      return $response;
  }
}
