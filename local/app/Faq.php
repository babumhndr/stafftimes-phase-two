<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class Faq extends Eloquent 
{
	protected $collection='mot_faq';

    public function FaqDetails(){
        return $this->hasMany('App\FaqTitle','_id','topic_id');
    }

	 public static function addFaq($details){
    	$res=Faq::insert($details);
    		return array('status'=> 'success','response'=>'great');
    }

    public static function deletefaq($id){
        $res=Faq::where('_id',$id)->delete();
        /*return $res;*/
            return array('status'=> 'success','response'=>"great");
    }
    public static function editFaq($id){
        $res=Faq::with('FaqDetails')->where('_id',$id)->get();
        return $res;
    }
    


	/*FAQ Listing*/
	/*public function faq(){
    return $this->hasMany('App\faqListMappedToFaqTitle','faq_id','_id');*/
	
}