<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;

class LanguageUse extends Eloquent
{
    protected $collection='mot_language';

    public static function addLanguage($details)
    {
    	$result = LanguageUse::where('language_code',$details['language_code'])->get();
    	$count = count($result);
    	/*return $count;*/
    	//check if email already exists
    	if ($count>0)
    	{
    		return 'Language already exist';
    	}
    	else
    	{
    		$result=LanguageUse::insert($details);
    		$result1=LanguageUse::where('language_code',$details['language_code'])->get();
    		if($result)
    		{
    			return array('status'=>'Success','reposnse'=>$result1);
    		}
    		return 'no';
    	}
	}
}
