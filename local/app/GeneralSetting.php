<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
use App\ConditionForWorkingDaysSetting;//model

class GeneralSetting extends Eloquent
{ 
  protected $collection='mot_general_settings';

  //insert general setting
  public static function setGeneralSetting($general_setting_details)
  {
    $result=GeneralSetting::insert($general_setting_details);
   if($result)
      {
      $response = array('status' => 'success','status_code' => '119','response' =>'Saved');
      $responseToSend=array('general_setting_change'=>true,
                            'message'=>'general_setting_changed');
       Employee::sendNotificationByFcmId($responseToSend);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => 'Error');
      }
      return $response;
  }

  //update general setting
  public static function updateGeneralSetting($general_setting_details)
  {
    $checkForChangeOfValuesBefore=GeneralSetting::where('companyid',$general_setting_details['companyid'])->get();
    $result=GeneralSetting::where('companyid',$general_setting_details['companyid'])->update($general_setting_details);
    $checkForChangeOfValuesAfter=GeneralSetting::where('companyid',$general_setting_details['companyid'])->get();
    //notfication on change of timpicker and total hours
    if (!empty($checkForChangeOfValuesBefore) && !empty($checkForChangeOfValuesAfter)) {
      if ($checkForChangeOfValuesBefore[0]['set_working_hours']!=$checkForChangeOfValuesAfter[0]['set_working_hours'] || $checkForChangeOfValuesBefore[0]['start_interval']!=$checkForChangeOfValuesAfter[0]['start_interval']|| $checkForChangeOfValuesBefore[0]['end_interval']!=$checkForChangeOfValuesAfter[0]['end_interval']|| $checkForChangeOfValuesBefore[0]['start_rounding_option']!=$checkForChangeOfValuesAfter[0]['start_rounding_option']|| $checkForChangeOfValuesBefore[0]['end_rounding_option']!=$checkForChangeOfValuesAfter[0]['end_rounding_option']) 
      {

        $responseToSend=array('general_setting_change'=>true,
                    'message'=>'general_setting_changed');
        Employee::sendNotificationByFcmId($responseToSend);
      }
    }
   if($result)
      {
      $response = array('status' => 'success','status_code' => '119','response' =>'Saved');
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => 'Error');
      }
      return $response;
  }
  
  //setting listing
  public static function generalSettingList($data)
  {
    $details=GeneralSetting::where('companyid',$data['user_id'])->get();
    if(count($details) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $details);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;
  }

  //list for mobile
  public static function generalSettingValues($companyid)
  {
    $result=GeneralSetting::where('companyid',$companyid)->get();
    if(count($result) > 0){
          $response = array('status' => 'success','status_code' => '228','response' => $result);

      }
      else{
        $response = array('status' => 'failure','status_code' => '828','response' =>[]);
      }
      return $response;
  }

  //setting values on first login
  public static function firstLoginSettingValues($res)
  {
     $userActivity=MotActivity::where('fast_check_in_default',1)->where('company_id',$res)->get(['_id']);
     $overtime_for_period=MotActivity::where('overtime_for_period',1)->where('company_id',$res)->get(['_id']);
     $clear_overtime_balance=MotActivity::where('clear_overtime',1)->where('company_id',$res)->get(['_id']);
     $generalSettingActivity=$userActivity[0]['_id'];
    $insertArray= array('companyid' => $res,
                        'set_working_hours'=>'08:00',
                        'start_interval'=>'1',
                        'end_interval'=>'1',
                        'start_rounding_option'=>'near',
                        'end_rounding_option'=>'near',
                        "time_style" => "24 hours",
                        "value_format" => "Device region",
                        "general_setting_activity"=>$generalSettingActivity);
    $overtimeHandling=array("overtime_for_period" => $overtime_for_period[0]['_id'],
                        "threshold_hours" => "40:00",
                        "hourly_rate" => "1.500",
                        "clear_overtime_balance" => $clear_overtime_balance[0]['_id'],
                        "companyid" =>$res);
    $result=GeneralSetting::insert($insertArray);
    $result1=OvertimeHandling::insert($overtimeHandling);
  }
}


      