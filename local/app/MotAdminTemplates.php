<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Redirect;
class MotAdminTemplates extends Eloquent
{
  protected $collection='mot_admin_templates';

    //activity
  public function activity(){
    return $this->hasMany('App\AdminActivitiesMappedToTemplate','template_id','_id');
  }

        //adding template
	public static function addTemplate($details)
	{
    	$result = MotAdminTemplates::where('template_name',$details['template_name'])->where('company_id',$details['company_id'])->get();
    	$count = count($result);
    	//check if template name already exists
    	if ($count>0)
   		{
      		return array('status'=> 'failure','response'=>'Template name already exists !');
    	}
    	else
    	{
			   //insert to MotAdminTemplates table
          $template = new MotAdminTemplates;
          $template->template_name = $details['template_name'];
          $template->template_name_german = $details['template_name_german'];
          $template->company_id = $details['company_id'];
          $template->template_offset_time = $details['template_offset_time'];
          $template->created_on = $details['created_on'];
          $template->created_by = $details['created_by'];
          $template->created_user_id = $details['created_user_id'];
          $template->deploy_status = $details['deploy_status'];
          $template->save();
          $tempalteId = $template->id;
    /*   return $templateId;*/
			if($tempalteId)
   			{
            $details['template_id'] = $tempalteId;

            $totalNumberOfActivity = $details['number_of_activity'];
            for($i=0;$i<$totalNumberOfActivity;$i++){
              //insert to activity mapped to templates table
              $activityId=Input::get('activity_id_'.$i.'');
              $activityFlatHoursVal=FileUpload::replaceDotToColon(Input::get('activity_flat_hours_'.$i.''));
              $activityBreakVal=FileUpload::replaceDotToColon(Input::get('activity_break_'.$i.''));
              $activityAmount=FileUpload::replaceDotToComma(Input::get('activity_amount_'.$i.''));
              $activityDetails = array('activity_id' => $activityId,
                                        'activity_flat_hours' => $activityFlatHoursVal,
                                        'activity_amount' => $activityAmount ,
                                        'activity_start' =>  FileUpload::hoursToAmPm(Input::get('activity_start_'.$i.'')),
                                        'activity_end' =>  FileUpload::hoursToAmPm(Input::get('activity_end_'.$i.'')),
                                        'activity_break' =>$activityBreakVal,
                                        'template_id' => $details['template_id']);
              if ($activityId != null) 
              {
                $result = AdminActivitiesMappedToTemplate :: addActivityMappedToTempalte($activityDetails);
              }
              
            }
            return array('status'=> 'success','response'=>'Template Added');
      			

    		}
    		else
    		{
      			return array('status'=> 'failure','response'=>'Error occured');
    		}
    	}
	}
//updating template
  public static function updateTemplate($details,$template_id,$number_of_activity)
  {
    $result = MotAdminTemplates::where('template_name',$details['template_name'])->where('company_id',$details['company_id'])->get();
      $count = count($result);
      //check if template name already exists
      if ($count>0 && $template_id!=$result[0]['_id'])
      { 
          return array('status'=> 'failure','response'=>'Template name already exists !');
      }
      else
      {
         $result1 = MotAdminTemplates::where('_id',$template_id)->update($details);
               $results1=AdminActivitiesMappedToTemplate::where('template_id',$template_id)->delete();
    if($result1)
        {
           /* $totalNumberOfActivity = $details['number_of_activity'];*/
            for($i=0;$i<$number_of_activity;$i++){
              //insert to activity mapped to templates table
              $activityId=Input::get('activity_id_'.$i.'');
            $activityFlatHoursVal=FileUpload::replaceDotToColon(Input::get('activity_flat_hours_'.$i.''));
              $activityBreakVal=FileUpload::replaceDotToColon(Input::get('activity_break_'.$i.''));
                $activityAmount=FileUpload::replaceDotToComma(Input::get('activity_amount_'.$i.''));
              $activityDetails = array('activity_id' => $activityId,
                                        'activity_flat_hours' => $activityFlatHoursVal,
                                        'activity_amount' => $activityAmount,
                                        'activity_start' =>  FileUpload::hoursToAmPm(Input::get('activity_start_'.$i.'')),
                                        'activity_end' =>  FileUpload::hoursToAmPm(Input::get('activity_end_'.$i.'')),
                                        'activity_break' =>$activityBreakVal,
                                         'template_id' => $template_id);
             /* return $activityDetails;*/
           if ($activityId != null) 
              {
                $result = AdminActivitiesMappedToTemplate :: addActivityMappedToTempalte($activityDetails);
              }
              
            }
            return array('status'=> 'success','response'=>'Template Updated');
        }
        else
        {
            return array('status'=> 'failure','response'=>'Error occured');
        }
      }
  }
  //update company template
   public static function updateCompanyTemplate($details,$template_id )
  {
    $result = MotAdminTemplates::where('_id',$template_id)->update($details);
    if($result)
    {
      return array('status'=> 'success','response'=>'Template Updated');
    }
    else
    {
      return array('status'=> 'failure','response'=>'Error occured');
    } 
  }

  //update deafult template
  public static function updateDefaultTemplate($details,$template_id )
  {
   	$result = MotAdminTemplates::where('_id',$template_id)->update($details);
    if($result)
    {
      return array('status'=> 'success','response'=>'Template Updated');
    }
    else
    {
      return array('status'=> 'failure','response'=>'Error occured');
    } 
  }

  //getListOfTemplate
  public static function getListOfCompanyTemplate($companyId){
      $result = MotAdminTemplates :: with('activity.activityDetails')
                                ->where('created_user_id',$companyId)
                                ->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;

  }

  //getListOfTemplate
  public static function getListOfTemplates($companyId,$id){
      $result = MotAdminTemplates :: with('activity.activityDetails')->where('company_id',$companyId)->where('_id',$id)
                                    
                                    ->get();
      return $result;

  }

  //getListOfMotTemplates
  public static function getListOfMotTemplates($id){
    $result = MotAdminTemplates :: with('activity.activityDetails')->where('_id',$id)->where('created_by','MOT')
                                    
                                    ->get();
      return $result;
  }

  //getListOfMotTemplate
  public static function getListOfMotTemplate(){
      $result = MotAdminTemplates :: with('activity.activityDetails')
                                ->where('created_by','MOT')
                                ->get();
      if(count($result) > 0){
        $response = array('status' => 'success','status_code' => '119','response' => $result);
      }
      else{
        
        $response = array('status' => 'failure','status_code' => '129','response' => []);
      }
      return $response;

  }

  //getListOfTemplateNew
  public static function getListOfTemplateForApp($details){
      $result = MotAdminTemplates :: with('activity.activityDetails')
                                ->whereIn('created_user_id',array($details['user_id'],$details['superadmin_id'],$details['company_id']))
                                ->get();
      return $result;

  }

  //get list of template for settings
  public static function getListTemplate($details){
    
    $result = MotAdminTemplates :: where('created_user_id',$details['user_id'])
                               ->get();
    return $result;
    /*$result = MotAdminTemplates :: where('created_user_id',$id)
                                ->get();
    $result1=MotAdminTemplates::where('created_by','MOT')->get();
    $combined = array_merge(array($result),array($result1));
    return $combined;*/ 
  }

  public static function addTemplatesForRegisterUser($res,$language)
    {
      $template=MotAdminTemplates::where('deploy_status','!=','pending')->get();
      for ($i=0; $i < sizeof($template) ; $i++) { 
   /*     $template[$i]['company_id']=$res;
        $template[$i]['created_user_id']=$res;
        $template[$i]['created_by']='company';
        $template[$i]['duplicate']=$template[$i]['_id'];
        unset($template[$i]['_id']);
        $array = json_decode(json_encode($template[$i]), true);
        $insert=MotTemplates::insert($array);*/
        if ($language=="de") {
         $template[$i]['template_name']=$template[$i]['template_name_german'];
        }
          $duplicate=$template[$i]['_id'];
          $templateInsert = new MotTemplates;
          $templateInsert->template_name = $template[$i]['template_name'];
          $templateInsert->company_id = $res;
          $templateInsert->template_offset_time = $template[$i]['template_offset_time'];
          $templateInsert->created_on = $template[$i]['created_on'];
          $templateInsert->created_by = 'company';
          $templateInsert->created_user_id =$res;
          $templateInsert->deploy_status ='pending';
          $templateInsert->duplicate =$template[$i]['_id'];
          $templateInsert->save();
          $tempalteId = $templateInsert->id;
          $activityForTemplate=AdminActivitiesMappedToTemplate::addActivityForTemplatesForRegisterUser($duplicate,$res,$tempalteId);
    /*      return $activityForTemplate;*/
      }
    }


  //change deploy status
  public static function changeStatusDeploy($id,$status)
  {
    $result=MotAdminTemplates::where('_id',$id)->update($status);
    $company=Companies::where('admin','!=',1)->select('_id', 'language')->get();
    $templateToAdd=MotAdminTemplates::where('_id',$id)->get();
    if (count($company)>0) 
    {
      for ($i=0; $i<sizeof($company); $i++) 
      { 
        if ($company[0]['language']=='de') 
        {
          $templateToAdd[0]['template_name']=$templateToAdd[0]['template_name_german'];
        }
        $duplicate=$templateToAdd[0]['_id'];
        $templateInsert = new MotTemplates;
        $templateInsert->template_name = $templateToAdd[0]['template_name'];
        $templateInsert->company_id = $company[$i]['_id'];
        $templateInsert->template_offset_time = $templateToAdd[0]['template_offset_time'];
        $templateInsert->created_on = $templateToAdd[0]['created_on'];
        $templateInsert->created_by = 'company';
        $templateInsert->deploy_status='pending';
        $templateInsert->created_user_id =$company[$i]['_id'];
        $templateInsert->duplicate =$templateToAdd[0]['_id'];
        $templateInsert->save();
        $tempalteId = $templateInsert->id;
        $activityForTemplate=AdminActivitiesMappedToTemplate::addActivityForTemplatesForExistUser($duplicate,$company[$i]['_id'],$tempalteId);
      }
    }
    if ($result) 
      {
        $response = array('status' => 'success','status_code' => '998');
      }
    else{ 
        $response = array('status' => 'failure','status_code' => '888');
      }
      return $response;
  }

}
