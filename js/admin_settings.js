
 //form of general setting 
  (function() {
$('#general_setting').ajaxForm({
    beforeSend: function () {
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
     console.log(data);
 if(data.status=='success')
 {
     swal({  
                         title: "Success", 
                         text: "Saved successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             location.reload();  
                        });

 }
 else if(data.status=='failure'){
    swal("Error!", "Error Occured", "error");
}

    },
    complete: function() {
    
    }
}); 

})();


//form set annual allowance
  (function() {
$('#set_annual_allowance_form').ajaxForm({
    beforeSend: function () {
    /* console.log('sasdd');
     return false;*/
     var val=$('#total_in_calculated_value').val();
     if (val == '') {
        swal({  
                         title: "Error", 
                         text: "Fill total in values",   
                         type: "error",   
                         confirmButtonText : "Ok"
                        });  
        return false;
     }
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
     console.log(data);
 if(data.status=='success')
 {
     swal({  
                         title: "Success", 
                         text: "Saved successfully",   
                         type: "success",   
                         confirmButtonText : "Ok"
                        },
                        function(){

                             location.reload();  
                        });

 }
 else if(data.status=='failure'){
    swal("Error!", "Error Occured", "error");
}

    },
    complete: function() {
    
    }
}); 

})();

//set annual allowance total in

function hrsToMin(time)
        {
          time = time.split(':');
          return parseInt(time[0]*60)+parseInt(time[1]);
          /*return (time[0]*60) + time[1];*/
        }

function minToHrs(minutes) {
            var sign ='';
            if(minutes < 0){
            sign = '-';
            }

            var hours = Math.floor(Math.abs(minutes) / 60);
            var minutes = leftPad(Math.abs(minutes) % 60);

            return sign + hours +':'+minutes;

    }
function leftPad(number) {  
         return ((number < 10 && number >= 0) ? '0' : '') + number;
    }


     $( function() {
     Globalize.culture('en');
     $( ".decimal" ).spinner({
     step: 1.00,
     numberFormat: "n"
     });
     } );

$("#total_in_value1").timeEntry({unlimitedHours: true,spinnerImage: '',defaultTime: null});


var total='';
var goVal='';
var activitySelected='';
var allowanceDaysForActivity='';
var allowanceHoursForActivity='';

//onload activity select
   activitySelected=$('#setting_selection').val();
var allowanceDaysForActivity=$('#day'+activitySelected).val();
var allowanceHoursForActivity=$('#hour'+activitySelected).val();
console.log(allowanceDaysForActivity+'fgfgfgfgf'+allowanceHoursForActivity);
 $('#total_in_value').val($('#total_in_value').html() + allowanceDaysForActivity);
 $('#total_in_value1').val($('#total_in_value1').html() + allowanceHoursForActivity);  

//on change of activity select
$('#setting_selection').change(function()
{
     activitySelected=$('#setting_selection').val();
     var allowanceDaysForActivity=$('#day'+activitySelected).val();
     var allowanceHoursForActivity=$('#hour'+activitySelected).val();
     $('#total_in_value').val($('#total_in_value').html() + allowanceDaysForActivity);
     $('#total_in_value1').val($('#total_in_value1').html() + allowanceHoursForActivity);
});

//onload hours and day select

total=$('#total_in_type').val();
offset=hrsToMin(offset);
if(total=='Hours'){
     $('#type_display').html('Days');
     $('.decimal').hide();
     $('.hours_val').show();
     $('#go').click(function()
     {
          goVal=hrsToMin($('#total_in_value1').val());
          console.log(offset);
          goVal=goVal/offset;
          goVal=(goVal).toFixed(2);
          $('#total_in_calculated_value').val($('#total_in_calculated_value').html() + goVal);
     });
}
else if (total=='Days')
{
     $('#type_display').html('Hours');
     $('.decimal').show();
     $('.hours_val').hide();
     $('#go').click(function()
     {
          goVal=Math.round(changeToDot($('#total_in_value').val())*offset);
          console.log(goVal);
          goVal=minToHrs(goVal);
          $('#total_in_calculated_value').val($('#total_in_calculated_value').html() + goVal);
     });
}


//on change hours and day select
$('#total_in_type').change(function(){
     total=$('#total_in_type').val();
     console.log(total);
if(total=='Hours'){
      $('#type_display').html('Days');
     $('.decimal').hide();
     $('.hours_val').show();
     $('#go').click(function()
     {
          goVal=hrsToMin($('#total_in_value1').val());
          console.log(goVal);
          goVal=goVal/offset;
          goVal=(goVal).toFixed(2);
          $('#total_in_calculated_value').val($('#total_in_calculated_value').html() + goVal);
     });
}
else if(total=='Days')
{    
     $('#type_display').html('Hours');
     $('.decimal').show();
     $('.hours_val').hide();
     $('#go').click(function()
     {
          goVal=Math.round(changeToDot($('#total_in_value').val())*offset);
          console.log(goVal);
          goVal=minToHrs(goVal);
          $('#total_in_calculated_value').val($('#total_in_calculated_value').html() + goVal);
     });
}
return false;
});



var tab_active=localStorage.getItem("value");
/*alert(tab_active);*/

if(tab_active=='general_setting_btn')
{
     $("#set_annual_id").removeClass("active");
     $("#general_settings_id").addClass("active");
     $("#set_annual").removeClass("active");
     $("#general_settings").addClass("active");
}
else if(tab_active=='set_annual_allowance_btn')
{
     $("#general_settings_id").removeClass("active");
     $("#set_annual_id").addClass("active");
     $("#general_settings").removeClass("active");
     $("#set_annual").addClass("active");
}
else{
     $("#set_annual_id").removeClass("active");
     $("#general_settings_id").addClass("active");
     $("#set_annual").removeClass("active");
     $("#general_settings").addClass("active");
}
function changeToDot(str)
{
     if(str.indexOf(',') !== -1)
          {
               return str.replace(',','.');
          }
     else
          {
               return str;
          }
}

$('#set_working_hours').timeDropper1();