var code=$('#phone-code').val();
	finalcode='+'+code;
	$('#phone-code-input').html($('#phone-code-input').val(finalcode));
$('#phone-code').change(function()
{
	var code=$('#phone-code').val();
	finalcode='+'+code;
	$('#phone-code-input').html($('#phone-code-input').val(finalcode));
});


(function() {
$('form').ajaxForm({
    beforeSend: function () {
    var name = $('#name').val();
    var email = $('#email').val();
    var phone = $("#phone").val();
    var isValid = true;
    var message;
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    console.log(regex.test(email));
    if(name == null || email == null || phone == null || 
        name == '' || email == '' || phone == '' ){
        isValid = false;
        swal(error_, fields, "error");
    }
    else if(regex.test(email) == false){
        isValid = false;
        swal(error_, check, "error");
    }
    if(isValid == false){
        return false;
    }
    else{
        $('#btn2').hide();
        $('#loader').show();
    }
 },
    uploadProgress: function() {
      
    },
    success: function(data) {
        console.log(data);
 if(data.status=='success')
 {
    swal(success, back, "success");
    $("#form")[0].reset();
     $('#loader').hide();
     $('#btn2').show();
 }
 else if(data.status=='failure'){
    $('#btn2').show();
    $('#loader').hide();
    swal(error_, try_again, "error");
}

    },
    complete: function() {
    
    }
}); 

})();