<?php
require_once("../includes/braintree_init.php");

$amount = $_POST["amount"];
$email = $_POST["emailId"];
$name = $_POST["companyName"];
$companyLangId = $_POST["companyLangId"];
if($companyLangId == 'English'){
$cardholder_name = $_POST["cardholder_name_eng"];
}
else{
$cardholder_name = $_POST["cardholder_name_ger"];
}

$merchantId = $_POST["merchantId"];
$nonce = $_POST["payment_method_nonce"]; 

$result = Braintree\Transaction::sale([
    'amount' => $amount,
	'merchantAccountId' => $merchantId,
    'paymentMethodNonce' => $nonce,
    'creditCard' => [
        'cardholderName' => $cardholder_name
    ],
    'customer' => [
        'email' => $email,
        'company' => $name
    ],
    'options' => [
        'submitForSettlement' => true
    ]
]); 

//echo $result;die;

if ($result->success || !is_null($result->transaction)) {
    $transaction = $result->transaction;
    header("Location: transaction.php?id=" . $transaction->id);
} else {
    $errorString = "";

    foreach($result->errors->deepAll() as $error) {
        $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
    }

    $_SESSION["errors"] = $errorString;
    header("Location: index.php");
}
