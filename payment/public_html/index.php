<?php require_once("../includes/braintree_init.php"); ?>

<html>
<?php require_once("../includes/head.php"); ?>
<body>
<?php require_once("../includes/header.php"); ?> 
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
function validateForm() {
    if(companyLangNew == 'English'){
    var x = document.forms["myForm"]["cardholder_name_eng"].value;
    if (x == "") {
        $('#loading').hide();
        $('.buttonEng').show();
        $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
        return false;
    }
    }
    else {
    var x = document.forms["myForm"]["cardholder_name_ger"].value;
    if (x == "") {
        $('#loading').hide();
        $('.buttonGer').show();
        $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
        return false;
    }
    }

}
</script>
    <div class="wrapper">
        <div class="checkout container">

            <header>
                <h1 class="english_div">Staff Times Subscription Payment</h1>
                <h1 class="german_div">Staff Times Abonnementszahlung</h1>
            </header>

            <form name="myForm" id="payment-form" onsubmit="validateForm()" method="post" action="checkout.php">
                <section>
                    <label for="company" style="border-top:none;">
                        <span class="input-label english_div" style="opacity:1;">Company Name</span>
                         <span class="input-label german_div" style="opacity:1;">Name der Firma</span>
                        <div class="input-wrapper">
                            <input id="company" type="text" name="company" placeholder="Name" value="" disabled>
                            <input id="companyName" type="hidden" name="companyName" placeholder="companyName" value="">
                        </div> 
                    </label>
                    <label for="email" style="border-top:none;">
                        <span class="input-label english_div" style="opacity:1;">Email</span>
                        <span class="input-label german_div" style="opacity:1;">E-Mail-Adresse</span>
                        <div class="input-wrapper">
                            <input id="email" type="text" name="email" placeholder="Email" value="" disabled>
                            <input id="emailId" type="hidden" name="emailId" placeholder="Email" value="">
							              <input id="merchantId" type="hidden" name="merchantId" placeholder="merchantId" value="">
                            <input id="companyLangId" type="hidden" name="companyLangId" placeholder="companyLangId" value="">
                        </div>
                    </label>
                   <!--  <label for="cardholder_name" class="cardholder_name_label cardholder" style="border-top:none;border-bottom: solid 1px #DEE2E5;margin-bottom: 2px;" class="">
                        <span class="input-label english_div" style="opacity: 1; display: none;">Card Holder Name</span>
                         <span class="input-label german_div" style="opacity:1;">Name des Karteninhabers</span>
                        <div class="input-wrapper english_div">
                            <input type="text" class="cardholder_name_eng cardholder" name="cardholder_name_eng" placeholder="Card Holder Name" value="">
                        </div>
                        <div class="input-wrapper german_div">
                        <input type="text" class="cardholder_name_ger cardholder" name="cardholder_name_ger" placeholder="Name des Karteninhabers" value="">
                        </div>
                    </label> -->
                    <div class="bt-drop-in-wrapper" style="margin-top: 16px;">
                        <div id="bt-dropin"></div>
                    </div>
                    <label for="cardholder_name" class="cardholder_name_label cardholder" style="border-top:none;border-bottom: solid 1px #DEE2E5;margin-bottom: 2px;" class="">
                       <!--  <span class="input-label english_div" style="opacity: 1; display: none;">Card Holder Name</span>
                         <span class="input-label german_div" style="opacity:1;">Name des Karteninhabers</span> -->
                        <div class="input-wrapper english_div">
                            <input type="text" class="cardholder_name_eng cardholder" name="cardholder_name_eng" placeholder="Card Holder Name" value="" style="height: 35px;">
                        </div>
                        <div class="input-wrapper german_div">
                        <input type="text" class="cardholder_name_ger cardholder" name="cardholder_name_ger" placeholder="Name des Karteninhabers" value="" style="height: 35px;">
                        </div>
                    </label>
                    <label for="amount">
                        <span class="input-label english_div">Amount</span>
                        <span class="input-label german_div">Betrag</span>
                        <div class="input-wrapper">
						   <span id="currency"> </span>
                            <input id="amount" name="amount" type="tel" min="1" placeholder="Amount" value="10" readonly>
                        </div>
                    </label>
                </section>

                <div><button class="button english_div buttonEng" type="submit"><span>PAY NOW</span></button>
                <button class="button german_div buttonGer" type="submit"><span>BEZAHLEN</span></button></div>
                <img src="../../image/myovertime-loader.gif" id="loading" style="display:none;"> 
                <table class="sep merchant_accounts">
                  <tbody>
                  <tr style="border: none;">
                  <td style="padding-top: 20px;" class="english_div"><a href="https://stafftimes.com/faq" style="color: #242424;">FAQ</a> <small style="color: #2392ec;">AND</small> <a href="https://stafftimes.com/termsandcondition" style="color: #242424;">TERMS AND CONDITIONS</a></td>
                   <td style="padding-top: 20px;" class="german_div"><a href="https://stafftimes.com/faq" style="color: #242424;">FAQ</a> <small style="color: #2392ec;">UND</small> <a href="https://stafftimes.com/termsandcondition" style="color: #242424;">AGB</a></td>
                  <td class="payment_instrument_logos">
                    <img alt="Paypal" class="" src="images/paypal_account.png">
                    <img alt="Discover" class="" src="images/discover.png">
                    <img alt="Maestro" class="" src="images/maestro.png">
                    <img alt="UK Maestro" class="" src="images/uk_maestro.png">
                    <img alt="MasterCard" class="" src="images/mastercard.png">
                    <img alt="Visa" class="" src="images/visa.png"></td>
                  </tr>
                  </tbody>  
                </table>
            </form>
        </div>
    </div>

<script src="https://js.braintreegateway.com/js/braintree-2.27.0.min.js"></script>

<script>
$( ".cardholder" ).focusin(function() {
  $('.cardholder_name_label').css("border-bottom", "solid 2px #DEE2E5");
});
$( ".cardholder" ).focusout(function() { 
  if(companyLangNew == 'English'){
          if($('.cardholder_name_eng').val() == ''){
               $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
            }
            else{
               $('.cardholder_name_label').css("border-bottom", "solid 1px #DEE2E5");
            }
          }
          else{
            if($('.cardholder_name_ger').val() == ''){
               $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
            }
            else{
               $('.cardholder_name_label').css("border-bottom", "solid 1px #DEE2E5");
            }
          }
});
$('#payment-form').submit(function() {
    /*if($('.cardholder_name').val() == ''){
      $('.cardholder_name').css("border-color", "red");
      alert('Cardholder Name required');
    }
    else{
    }*/
  $('#loading').show();
  $('.button').hide();
  return false;
});
  var companyId = localStorage.getItem("companyId");
	var merchantAccountId = localStorage.getItem("merchantAccountId");
  //var merchantAccountId = "danstafftimescom";
	$('#merchantId').val(merchantAccountId);
  var companyLangNew = localStorage.getItem("companyLang");
  $('#companyLangId').val(companyLangNew);
  var companyName = localStorage.getItem("companyName");
	$('#company').val(companyName);
  $('#companyName').val(companyName);
	var companyCurrency = localStorage.getItem("companyCurrency");
  $('#currency').html(companyCurrency);
  var companyEmail = localStorage.getItem("companyEmail");
  $('#email').val(companyEmail);
  $('#emailId').val(companyEmail);
  var totalAmount = localStorage.getItem("totalAmount");
  //var totalAmount = 1;
  localStorage.setItem("totalAmount", totalAmount);
  $('#amount').val(totalAmount); 
  var companyPlan = localStorage.getItem("companyPlan");

  //console.log(companyId+'---'+companyName+'---'+companyEmail+'---'+totalAmount+'---'+merchantAccountId);
  var checkout = new Demo({
      formID: 'payment-form'
  })
  var client_token = "<?php echo(Braintree\ClientToken::generate()); ?>";
  braintree.setup(client_token, "dropin", {
      container: "bt-dropin",
      paypal: {
        button: {
          type: 'checkout'
        }
      },
      onReady: function () {
       
      },
      /*onPaymentMethodReceived: function (obj){
        console.log(obj.type);
        //console.log($('.cardholder_name').value());
        if(obj.type == "CreditCard"){
          if(companyLangNew == 'English'){
            if($('.cardholder_name_eng').val() == ''){
               $('#loading').hide();
               $('.buttonEng').show();
               $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
            }
            else{
               $('.cardholder_name_label').css("border-bottom", "solid 1px #DEE2E5");
                if(companyLangNew == 'English'){
                  $('#loading').hide();
                  $('.buttonEng').show();
                  window.location.href = 'checkout.php'; 
                }
                else{
                  $('#loading').hide();
                  $('.buttonGer').show();
                  window.location.href = 'checkout.php'; 
                }
            }
          }
          else{
            if($('.cardholder_name_ger').val() == ''){
               $('#loading').hide();
               $('.buttonGer').show();
               $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
            }
            else{
               $('.cardholder_name_label').css("border-bottom", "solid 1px #DEE2E5");
                if(companyLangNew == 'English'){
                  $('#loading').hide();
                  $('.buttonEng').show();
                  window.location.href = 'checkout.php'; 
                }
                else{
                  $('#loading').hide();
                  $('.buttonGer').show();
                  window.location.href = 'checkout.php'; 
                }
            }
          }
        }
        else{
          var companyLangNew = "----";
          $('.cardholder_name_eng').val(companyLangNew);
          $('.cardholder_name_ger').val(companyLangNew);
        }
      },*/
      onError: function(obj) {
        //console.log(obj.message);
        if(obj.type == "VALIDATION"){
          if(companyLangNew == 'English'){
            var x = document.forms["myForm"]["cardholder_name_eng"].value;
            if (x == "") {
                $('#loading').hide();
                $('.buttonEng').show();
                $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
                return false;
            }
            $('#loading').hide();
            $('.buttonEng').show();
          }
          else{
            var x = document.forms["myForm"]["cardholder_name_ger"].value;
            if (x == "") {
                $('#loading').hide();
                $('.buttonEng').show();
                $('.cardholder_name_label').css("border-bottom", "solid 2px #D0021B");
                return false;
            }
            $('#loading').hide();
            $('.buttonGer').show();
          }
        }
        
        return false;
        // console.log("error");
        // $('#submit-payment').removeClass('disabled');
      }
      /*paypal: {
      singleUse: true, // Required
      amount: totalAmount, // Required
      currency: companyCurrency, // Required
      }*/
  });
       
	   
var company_lang = localStorage.getItem("companyLang");
if(company_lang == 'English'){
   $('.english_div').show();
   $('.german_div').hide();
}
else{
   $('.german_div').show();
   $('.english_div').hide();
}
</script>
<style>
#loading{
  width: 100px;
  text-align: center;
  margin: 0px auto;
}
#container, .add-payment-method-view .payment-method-options, .checkbox-label, .clearfix, .grid, .grid .row {
    zoom: 1;
    margin: 7px 0px -6px 0px;
}
.payment-method-options{
  display:none;
}
.merchant_accounts{
    margin-top: 15px;
}
table.sep tr td.payment_instrument_logos > img {
    margin-right: 5px;
    width: 35px;
    float: left;
}
h1 {
    font-size: 36px;
    line-height: 1.1em;
    margin: 0 0 0 -0.05em;
}
table tbody td {
    padding: 15px;
    vertical-align: middle;
}
table.merchant_accounts tr.default td {
    font-weight: 500;
}
#amount{
	width: 90%!important;
    padding: 2px 0px 0px 0px!important;
}
#currency{
	float: left!important;
    margin: 0px 8px 0px 0px!important;
}
.payment-method-options{
    display:none;
}
.button {
    background-color: #2392ec;
}
.button:hover, .button:focus {
    background-color: #2392ec;
    color: #fff;
}
</style>
</body>
</html>
