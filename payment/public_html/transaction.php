<html>
<?php require_once("../includes/head.php"); ?>
<body>

<?php
    require_once("../includes/braintree_init.php");
    require_once("../includes/header.php");
    if (isset($_GET["id"])) {
        $transaction = Braintree\Transaction::find($_GET["id"]);
        //print_r($transaction);
        
        // $fields = array(
        //     'amount' => $transaction->amount,
        //     'company_email' => $transaction->email,
        //     'company_name' => Input::get('company_name'),
        //     'company_id' => Input::get('company_id'),
        //     'braintree_id' => Input::get('braintree_id'),
        //     'subscription_plan' => Input::get('subscription_plan'),
        //     'emaployeeIds' => Input::get('emaployeeIds'),
        //     'payment_status' => Input::get('payment_status')
        // );

        // $curl = curl_init();
        // curl_setopt($curl, CURLOPT_URL, "http://codewave.co.in/payment/public_html/checkout.php");
        // curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // curl_exec($curl);

        // $httpStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // $redirectUrl = curl_getinfo($curl, CURLINFO_REDIRECT_URL);
        // curl_close($curl);


        $transactionSuccessStatuses = [
            Braintree\Transaction::AUTHORIZED,
            Braintree\Transaction::AUTHORIZING,
            Braintree\Transaction::SETTLED,
            Braintree\Transaction::SETTLING,
            Braintree\Transaction::SETTLEMENT_CONFIRMED,
            Braintree\Transaction::SETTLEMENT_PENDING,
            Braintree\Transaction::SUBMITTED_FOR_SETTLEMENT
        ];
        
        if (in_array($transaction->status, $transactionSuccessStatuses)) {
            $header = "Transaction Success!";
            $icon = "success";
            $message = "Your transaction has been successfully processed. A receipt will be sent to your email address shortly.";
        } else {
            $header = "Transaction Failed";
            $icon = "fail";
            $message = "Your transaction has been failed. Try again later.";
        }
    }
?>

<div class="wrapper" id="langGerman" style="display:none;">
    <div class="container" id="transuccess">
        <div class="content">
            <div class="icon">
            <img src="images/<?php echo($icon)?>.svg" alt="">
            </div>

            <h1>Verarbeitung erfolgreich !</h1>
            <section>
                <p><span class="clColor">Firmennamen :</span> <t id="company_name"></t></p>
                <p><span class="clColor">Firmen-Email :</span> <t id="company_email"></t></p>
                <p><span class="clColor">Gesamtsumme :</span> <t id="company_total_amount"><?php echo $transaction->amount; ?></t></p>
                <p>Die Transaktion wurde erfolgreich abgewickelt. Sie erhalten in kürze eine Quittung an Ihre Email-Adresse.</p>
            </section>
            <section>
                <a class="button primary back" style="background-color:#2392ec;" href="https://stafftimes.com/subscribedemployees">
                    <span>Zurück zu Staff Times</span>
                </a>
            </section>
        </div>
    </div>
     <div class="container" id="tranfailure">
        <div class="content">
            <div class="icon">
            <img src="images/<?php echo($icon)?>.svg" alt="">
            </div>

            <h1><?php echo($header)?></h1>
            <section>
                <p><span class="clColor">Company Name :</span> <t id="company_name1"></t></p>
                <p><span class="clColor">Company Email :</span> <t id="company_email1"></t></p>
                <p><span class="clColor">Total Amount :</span> <t id="company_total_amount1"><?php echo $transaction->amount; ?></t></p>
                <p><?php echo($message)?></p>
            </section>
            <section>
                <a class="button primary back" style="background-color:#2392ec;" href="https://stafftimes.com/subscribedemployees">
                    <span>Back To Staff Times</span>
                </a>
            </section>
        </div>
    </div>
</div>

<div class="wrapper" id="langEnglish" style="display:none;">
    <div class="container">
        <div class="content">
            <div class="icon">
            <img src="images/<?php echo($icon)?>.svg" alt="">
            </div>

            <h1><?php echo($header)?></h1>
            <section>
                <p><span class="clColor">Company Name :</span> <t id="company_name2"></t></p>
                <p><span class="clColor">Company Email :</span> <t id="company_email2"></t></p>
                <p><span class="clColor">Total Amount :</span> <t id="company_total_amount1"><?php echo $transaction->amount; ?></t></p>
                <p><?php echo($message)?></p>
            </section>
            <section>
                <a class="button primary back" style="background-color:#2392ec;" href="https://stafftimes.com/subscribedemployees">
                    <span>Back To Staff Times</span>
                </a>
            </section>
        </div>
    </div>
</div>


<style>
img {
    margin: 0px auto!important;
    max-width: 100%;
}
.clColor{
    color:#333;
    font-weight:600;
}
</style>
<script src="https://stafftimes.com/js/moment-with-locales.js"></script>
<script>
var status = '<?php echo $transaction->status; ?>';
var amount = '<?php echo $transaction->amount; ?>';
var company_email = localStorage.getItem("companyEmail");
var company_lang = localStorage.getItem("companyLang");
if(company_lang == 'English'){
   $('#langEnglish').show();
   $('#langGerman').hide();
}
else{
   $('#langGerman').show();
   $('#langEnglish').hide();
   if(status == 'submitted_for_settlement' || status == 'settling'){
       $('#transuccess').show();
       $('#tranfailure').hide();
   }
   else{
       $('#tranfailure').show();
       $('#transuccess').hide();
   }
}
//console.log(company_email);
$('#company_email').html(company_email);
$('#company_email1').html(company_email);
$('#company_email2').html(company_email);
var company_name = localStorage.getItem("companyName");
$('#company_name').html(company_name);
$('#company_name1').html(company_name);
$('#company_name2').html(company_name);
var company_id = localStorage.getItem("companyId");
var braintree_id = '<?php echo $transaction->id; ?>';
var subscription_plan = localStorage.getItem("companyPlanMain");
var employeeIds = localStorage.getItem("memberIds");
var status = '<?php echo $transaction->status; ?>';
 var companyLangNew = localStorage.getItem("companyLang");
if(companyLangNew == 'English'){
    var date = moment().format("MM-DD-YYYY");
}
else{
    var date = moment().format("DD.MM.YYYY");
}
var date = moment().format("DD-MM-YYYY");
var companyCurrency = localStorage.getItem("companyCurrency");
var end_date = moment().add(365, 'days').format("YYYY-MM-DD 00:00:00");
//console.log(date);
//console.log(end_date);
if(status == 'submitted_for_settlement' || status == 'settling'){
var payment_status = "success";

   $.ajax({ 
         method: 'POST',
         url: 'https://stafftimes.com/paymentRecipt',
         data:{'amount' : amount, 'email' : company_email, 'name' : company_name, 'currency' : companyCurrency, 'order_no' : braintree_id, 'plan' : subscription_plan,'date' : date},
        success: function(data){
          //console.log(data);
        if(data.status == 'success' && data.status_code == '200'){
          
        }
      }});

}
else{
var payment_status = "failure";
}

function donationInfo() {
   $.ajax({ 
         method: 'POST',
         url: 'https://stafftimes.com/paymentInfo',
         data:{'payment_amount' : amount, 'company_email' : company_email, 'company_name' : company_name, 'company_id' : company_id, 'braintree_id' : braintree_id, 'subscription_plan' : subscription_plan, 'employeeIds' : employeeIds,'payment_status' : payment_status,'end_date' : end_date},
        success: function(data){
          //console.log(data);
        if(data.status == 'success' && data.status_code == '200'){
          
        }
      }});
}

 donationInfo();

</script>

</body>
</html>
