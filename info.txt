# Contact info submission url: https://www.stafftimes.com/

site_owner: Staff Times

address1:  Zur Kesselschmiede 29
8400 Winterthur
Schweiz

display_email: support@stafftimes.com

site_name: stafftimes.com

site_description: If you're looking for a quickly achievable, easy to handle time & attendance and project tracking solution for tracking your staff's working hours, overtimes and absences from a single place on the web. You see the benefits of having your staff check-in and track conveniently from their smartphones.

